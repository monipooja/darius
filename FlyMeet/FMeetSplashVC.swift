//
//  FMeetSplashVC.swift
//  Darious
//
//  Created by Apple on 23/01/20.
//  Copyright © 2020 Personal. All rights reserved.
// 2225777914

import UIKit

var msg_count : NSNumber = 0
var intentionArray : [NSDictionary] = []
var interestArray : [NSDictionary] = []
var studiesArray : [String] = []
var filterDict : NSMutableDictionary = [:]

class FMeetSplashVC: UIViewController {

    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = true
        
        if let _ = userDefault.value(forKey: "filter_dict")
        {
            let data = userDefault.value(forKey: "filter_dict")
            filterDict = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as! NSMutableDictionary
        }
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.Fmeet_Category, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func gotoNext()
    {
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
        self.callService(urlstr: Api.Fmeet_Profile, parameters: params, check: "profile")
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                if (check == "get") {
                    intentionArray = result["intention"] as! [NSDictionary]
                    interestArray = result["interest"] as! [NSDictionary]
                    studiesArray = result["studies"] as! [String]
                    
                    self.timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.gotoNext), userInfo: nil, repeats: false)
                } else {
                   // print("profile result = \(result)")
                    let dict = result["data"] as! NSDictionary
                    msg_count = dict["message"] as! NSNumber
                    if (filterDict == [:]) {
                        self.setFilterdict(valueDict: dict)
                    }
                    if (dict != [:]) {
                        let homevc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetHomeVC") as! FMeetHomeVC
                        self.navigationController?.pushViewController(homevc, animated: false)
                    } else {
                        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetProfileVC") as! FMeetProfileVC
                        nextvc.check = "launch"
                        self.navigationController?.pushViewController(nextvc, animated: false)
                    }
                }
            }
        }
    }
    
    func setFilterdict(valueDict : NSDictionary)
    {
        filterDict.setValue(valueDict["i_am"] as! String, forKey: "iam")
        filterDict.setValue(valueDict["looking_for"] as! String, forKey: "looking")
        filterDict.setValue("\(valueDict["intention"] as! NSNumber)", forKey: "intentions")
        filterDict.setValue(valueDict["studies_level"] as! String, forKey: "studies")
        filterDict.setValue(valueDict["interested_on"] as! String, forKey: "interest")
        filterDict.setValue(valueDict["age_range"] as! String, forKey: "age_range")
        filterDict.setValue("\(valueDict["distance"] as! NSNumber)", forKey: "distance")
        
        let archive = NSKeyedArchiver.archivedData(withRootObject: filterDict)
        userDefault.setValue(archive, forKey: "filter_dict")
        userDefault.synchronize()
    }
}

public func getAgeFromDOB(dob_str : String) -> String
{
    var age_str = ""
    if (dob_str != "0000-00-00") && (dob_str != "")
    {
        let passDateStr = dob_str
        let datestr = passDateStr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd-MM-yyyy")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let convert_date = dateFormatter.date(from: datestr!)
        let components = Set<Calendar.Component>([.year])
        let ageComponents = Calendar.current.dateComponents(components, from: (convert_date)!, to: Date())
        age_str = "\(ageComponents.year!)"
    }
    return age_str
}

public func flyMeetTopButtonAction(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 0:
        // Around me
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetSearchAroundVC") as! FMeetSearchAroundVC
        nextvc.check = "around"
        controller.navigationController?.pushViewController(nextvc, animated: false)
        break
    case 1:
        // My Matches
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetMatchesVC") as! FMeetMatchesVC
        controller.navigationController?.pushViewController(nextvc, animated: false)
        break
    case 2:
        // Filter
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetFilterVC") as! FMeetFilterVC
        controller.navigationController?.pushViewController(nextvc, animated: false)
        break
    case 3:
        // My Profile
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetProfileVC") as! FMeetProfileVC
        controller.navigationController?.pushViewController(nextvc, animated: false)
        break
    case 4:
        // My Rejected
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetRejectedVC") as! FMeetRejectedVC
        controller.navigationController?.pushViewController(nextvc, animated: false)
        break
    default:
        break
    }
}

public func flyMeetBottomViewAction(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 1:
        // My Likes
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetLikesVC") as! FMeetLikesVC
        controller.navigationController?.pushViewController(nextvc, animated: false)
        break
    case 2:
        // My Chat
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetChatListVC") as! FMeetChatListVC
        controller.navigationController?.pushViewController(nextvc, animated: false)
        break
    case 3:
        // Darious
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
        break
    case 4:
        // Wallet
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetWalletVC") as! FMeetWalletVC
        controller.navigationController?.pushViewController(nextvc, animated: false)
        break
    case 5:
        // Notification
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetNotificationVC") as! FMeetNotificationVC
        controller.navigationController?.pushViewController(nextvc, animated: false)
        break
    default:
        break
    }
}

/*Remains points :-
1. Age to show on every list but not in response except search.
2. my pics section.
3. edit profile option in profile.
4. what to do on like or reject on details screen.
5. user around me.
6. online/offline feature.
 */
