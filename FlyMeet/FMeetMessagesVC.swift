//
//  FMeetMessagesVC.swift
//  Darious
//
//  Created by Apple on 28/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit
import UserNotifications
import SocketIO

class FMeetMessagesVC: UIViewController {

    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var userImgVw : UIImageView!
    @IBOutlet weak var userNameLbl : UILabel!
    @IBOutlet weak var sendBtn : UIButton!
    @IBOutlet weak var BtmView : UIView!
    @IBOutlet weak var textF : UITextField!
    @IBOutlet weak var msgTextVw : UITextView!
    
    @IBOutlet weak var btmHtConst : NSLayoutConstraint!
    @IBOutlet weak var VwBtmConst : NSLayoutConstraint!
    
    @IBOutlet weak var chatTableView : UITableView!
    
    var socket_local : SocketIOClient = socket
    
    var pass_id : String = ""
    var cc : CGFloat = 40
    var timer : Timer?
    var timer_socket : Timer?
    var old_total : Int = 0
    var show_btm_view = false
    var join_value = false
    
    var messageArray : [NSDictionary] = []
    var messageArray2 : [[NSDictionary]] = []
    var matchDateArray : [String] = []
    var insertDateArray : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        current_screen = "chat"
        
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FMeetThemeClr
        topHeaderImgVw.tintColor = ColorCode.FMeetThemeClr
        sendBtn.layer.cornerRadius = sendBtn.frame.width/2
        sendBtn.isEnabled = false
        
        chatTableView.rowHeight = UITableView.automaticDimension
        chatTableView.estimatedRowHeight = 70
        
        let data : NSDictionary = ["sender_id" : user_id, "reciever_id" : "\(self.pass_id)"]
        socket_local.emit("join_flymeet", data)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FMeetMessagesVC.keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FMeetMessagesVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
      
        let tapgesture = UITapGestureRecognizer.init(target: self, action: #selector(hideOnTap))
        self.chatTableView.addGestureRecognizer(tapgesture)
        
        timer_socket = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(checkScoketConnection), userInfo: nil, repeats: true)
        
        textF.placeholder = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fm_type_message"))......"
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FMeetDarkTheme.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.pass_id)"
            self.callService(urlstr: Api.Fmeet_Chat_messages, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to black
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    ///  MARK : Swipe Gesture on view
    @objc func hideOnTap()
    {
        self.view.endEditing(true)
        self.VwBtmConst.constant = 0
    }
    @objc func appMovedToForeground() {
      //  print("foreground ......")
        socket_local.removeAllHandlers()
        join_value = true
        viewDidAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       // print("disappear ......")
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidAppear(_ animated: Bool)
    {
        socket_local.on("joined_flymeet") { (dataArray, socketAck) in
           // print("joineddd......")
            self.join_value = false
        }
  
        socket_local.on("chat_recieve_flymeet") { (dataArray, socketAck) in
            
            self.textF.placeholder = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fm_type_message"))......"
            self.msgTextVw.text = ""
            self.sendBtn.isEnabled = false
            
            let jsonDict = dataArray[0] as? NSDictionary
            //print("dddddd = \(jsonDict!)")
            if (("\(jsonDict!["sender_id"] as! NSNumber)" == self.pass_id) && (jsonDict!["reciever_id"] as! String == "\(user_id)")) || ((jsonDict!["sender_id"] as! NSNumber == user_id) && ("\(jsonDict!["reciever_id"] as! String)" == self.pass_id))
            {
                if (self.messageArray.contains(jsonDict!) == false)
                {
                    var arr : [NSDictionary] = []
                    
                    let date_str = "\(jsonDict!["time"] as! NSNumber)"
                    let dateVar = Date(timeIntervalSince1970: TimeInterval(date_str)!/1000)
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    let matchDate = dateFormatter.string(from: dateVar)
                   
                    if (self.matchDateArray.count == 0) {
                        self.matchDateArray.append(matchDate)
                        arr.append((jsonDict)!)
                        self.messageArray2.append(arr)
                    } else {
                        if (self.matchDateArray.contains(matchDate)) {
                            let idx = self.matchDateArray.index(of: matchDate)
                            self.messageArray2[idx!].append(jsonDict!)
                        } else {
                            self.matchDateArray.append(matchDate)
                            arr.append((jsonDict)!)
                            self.messageArray2.append(arr)
                        }
                    }
                    self.messageArray.append(jsonDict!)
                }
                self.chatTableView.isHidden = false
                self.chatTableView.reloadData()
                self.tableViewScrollToBottom(true)
                
            } else {
            }
        }
    }
    
    @IBAction func sendMessage(_ sender: Any)
    {
        old_total = messageArray.count
        
        if(join_value == true){
            socket_local = socket
            let data : NSDictionary = ["sender_id" : user_id, "reciever_id" : "\(self.pass_id)"]
            socket_local.emit("join_flymeet", data)
        }
        let data : NSDictionary = ["sender_id" : user_id, "reciever_id" : pass_id, "message" : "\(msgTextVw.text!)"]
        socket_local.emit("chat_send_flymeet", data)
        
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(checkSocket), userInfo: nil, repeats: false)
    }
    
    @objc func checkSocket()
    {
        if (old_total == messageArray.count)
        {
            timer?.invalidate()
            self.view.endEditing(true)
            self.VwBtmConst.constant = 0
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet"), controller: self, fontFamliy: UIFont.systemFont(ofSize: 12), bgcolor: ColorCode.FMeetDarkTheme, txtcolor: UIColor.white, wd: (self.view.frame.width-40), ht: 45)
            join_value = true
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        socket_local.removeAllHandlers()
        current_screen = ""
        
        let homevc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetHomeVC") as! FMeetHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        socket_local.removeAllHandlers()
        current_screen = ""
        
        navigationController?.popViewController(animated: false)
    }
    
    // Mark : Call service
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
           // print("messages result = \(result)")
            OperationQueue.main.addOperation {
                if let _ = result["userDetails"] as? NSDictionary
                {
                    let userDict = result["userDetails"] as! NSDictionary
                    if (userDict["profile_pic"] as! String).contains("https")
                    {
                        self.userImgVw.sd_setImage(with: URL(string: userDict["profile_pic"] as! String), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                    } else {
                        self.userImgVw.sd_setImage(with: URL(string: "\(Api.Profile_BaseURL)\(userDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                    }
                    self.userNameLbl.text = "\(userDict["first_name"] as! String) \(userDict["last_name"] as! String)"
                }
                
                self.messageArray = result["data"] as! [NSDictionary]
                
                var arr : [NSDictionary] = []
                for dict in self.messageArray
                {
                    let date_str = dict["created_on"] as! String
                    
                    let tttt = self.parseDuration(timeString: date_str.replacingOccurrences(of: "-", with: ":"))
                    
                    let dateVar = Date(timeIntervalSince1970: tttt/1000)
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    let matchDate = dateFormatter.string(from: dateVar)
                    
                    if(self.matchDateArray.count == 0) {
                        self.matchDateArray.append(matchDate)
                        arr.append(dict as NSDictionary)
                    } else {
                        if (self.matchDateArray.contains(matchDate)) {
                            arr.append(dict as NSDictionary)
                        } else {
                            self.messageArray2.append(arr)
                            arr = []
                            self.matchDateArray.append(matchDate)
                            arr.append(dict as NSDictionary)
                        }
                    }
                   self.messageArray2.append(arr)
                }
                
                if (self.messageArray2.count > 0)
                {
                    self.chatTableView.reloadData()
                    self.chatTableView.isHidden = false
                    self.tableViewScrollToBottom(true)
                } else {
                    self.chatTableView.isHidden = true
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    func parseDuration(timeString:String) -> TimeInterval {
        guard !timeString.isEmpty else {
            return 0
        }
        
        var interval:Double = 0
        
        let parts = timeString.components(separatedBy: ":")
        for (index, part) in parts.reversed().enumerated() {
            interval += (Double(part) ?? 0) * pow(Double(60), Double(index))
        }
        
        return interval
    }
    
    ///  MARK : Keyboard will show
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            //   print("notification: Keyboard will show - \(keyboardSize.height)")
            if self.view.frame.origin.y == 0 {
                self.chatTableView.frame = CGRect(x: self.chatTableView.frame.origin.x, y: self.chatTableView.frame.origin.y, width: self.chatTableView.frame.width, height: self.chatTableView.frame.height - keyboardSize.height)
                self.VwBtmConst.constant -= keyboardSize.height
            }
        }
    }
    
    ///  MARK : Keyboard will hide
    @objc func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            //   print("notification: Keyboard will Hide-  \(keyboardSize.height)")
            if self.view.frame.origin.y == 0 {
                self.chatTableView.frame = CGRect(x: self.chatTableView.frame.origin.x, y: self.chatTableView.frame.origin.y, width: self.chatTableView.frame.width, height: self.chatTableView.frame.height + keyboardSize.height)
                self.BtmView.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    // MARK : Show Local Notification
    func showLocalNotification(titleStr : String, subtitleStr : String, bodyStr : String)
    {
        let content = UNMutableNotificationContent()
        content.title = titleStr
        content.subtitle = subtitleStr
        content.body = bodyStr
        content.categoryIdentifier = "dakefly"
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 2, repeats: false)
        
        let requestIdentifier = "dakefly"
        
        let request = UNNotificationRequest.init(identifier: requestIdentifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            //print("handle error")
        }
    }
    
    @objc func checkScoketConnection()
    {
        let socketConnectionStatus = socket_local.status
        
        switch socketConnectionStatus {
        case SocketIOClientStatus.connected:
           // print("socket connected")
            break
        case SocketIOClientStatus.connecting:
           // print("socket connecting")
            break
        case SocketIOClientStatus.disconnected:
          // print("socket disconnected")
            break
        case SocketIOClientStatus.notConnected:
           // print("socket not connected")
            break
        }
    }
}

// MARK : table view methods
extension FMeetMessagesVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return matchDateArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (messageArray2.count > 0) {
            return messageArray2[section].count + 1
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == 0)
        {
            let cell = chatTableView.dequeueReusableCell(withIdentifier: "ChatDateCell") as! ChatDateCell
            
            if (matchDateArray.count > 0) {
                
                let str1 = matchDateArray[indexPath.section]
                
                let today = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let datestr = dateFormatter.string(from: today)
               
                let convert_date = dateFormatter.date(from: str1)
                
                if (str1 == datestr){
                    cell.dateLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fm_today")
                } else if (Calendar.current.isDateInYesterday(convert_date!))
                {
                     cell.dateLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fm_yesterday")
                }
                else {
                    cell.dateLabel.text = matchDateArray[indexPath.section]
                }
            }
            
            return cell
        }
        else {
            let dict = (messageArray2[indexPath.section])[indexPath.row-1]
            
            var my_id = ""
            if let _ = dict["uid"] as? String
            {
                my_id = dict["uid"] as! String
            } else {
                my_id = "\(dict["sender_id"] as! NSNumber)"
            }
            
            if(my_id == "\(user_id)")
            {
                let cell = chatTableView.dequeueReusableCell(withIdentifier: "fmeetrightcell") as! FMeetChatRightCell
                
                let msgStr = dict["message"] as! String
                cell.msgLabel.text = msgStr
                
                var date_str = ""
                if let _ = dict["created_on"] as? String
                {
                    date_str = dict["created_on"] as! String
                } else {
                    date_str = "\(dict["time"] as! NSNumber)"
                }
                
                if let _ = TimeInterval(date_str)
                {
                    let dateVar = Date(timeIntervalSince1970: TimeInterval(date_str)!/1000)
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    cell.timeLabel.text = dateFormatter.string(from: dateVar)
                }
                else {
                    let ttt = parseDuration(timeString: date_str.replacingOccurrences(of: "-", with: ":"))
                    let dateVar = Date(timeIntervalSince1970: ttt/1000)
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    cell.timeLabel.text = dateFormatter.string(from: dateVar)
                }

                let sz = ApiResponse.calculateSizeForString(msgStr, sz: 13, maxWidth: CGFloat.greatestFiniteMagnitude, controller: self)
                let wd = sz.width + 25

                let maxWidth = self.view.frame.width - 100
                if (wd < maxWidth) {
                    cell.msgwdConstraint.constant = wd
                    let ht = ApiResponse.calculateSizeForString(msgStr, sz: 13, maxWidth: wd, controller: self).height
                    cell.msghtConstraint.constant = ht + 20
                } else {
                    cell.msgwdConstraint.constant = maxWidth
                    let ht = ApiResponse.calculateSizeForString(msgStr, sz: 13, maxWidth: maxWidth, controller: self).height
                    cell.msghtConstraint.constant = ht + 20
                }
                return cell
            }
            else {
                let cell = chatTableView.dequeueReusableCell(withIdentifier: "fmeetleftcell") as! FMeetChatLeftCell
                
                let msgStr = dict["message"] as! String
                cell.msgLabel.text = msgStr
                
                var date_str = ""
                if let _ = dict["created_on"] as? String
                {
                    date_str = dict["created_on"] as! String
                } else {
                    date_str = "\(dict["time"] as! NSNumber)"
                }
                
                if let _ = TimeInterval(date_str)
                {
                    let dateVar = Date(timeIntervalSince1970: TimeInterval(date_str)!/1000)
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    cell.timeLabel.text = dateFormatter.string(from: dateVar)
                }
                else {
                    let ttt = parseDuration(timeString: date_str.replacingOccurrences(of: "-", with: ":"))
                    let dateVar = Date(timeIntervalSince1970: ttt/1000)
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    cell.timeLabel.text = dateFormatter.string(from: dateVar)
                }
                
                let sz = ApiResponse.calculateSizeForString(msgStr, sz: 13, maxWidth: CGFloat.greatestFiniteMagnitude, controller: self)
                let wd = sz.width + 25
                
                let maxWidth = self.view.frame.width - 100
                if (wd < maxWidth) {
                    cell.msgwdConstraint.constant = wd
                    let ht = ApiResponse.calculateSizeForString(msgStr, sz: 13, maxWidth: wd, controller: self).height
                    cell.msghtConstraint.constant = ht + 20
                } else {
                    cell.msgwdConstraint.constant = maxWidth
                    let ht = ApiResponse.calculateSizeForString(msgStr, sz: 13, maxWidth: maxWidth, controller: self).height
                    cell.msghtConstraint.constant = ht + 20
                }
                return cell
            }
        }
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let vw = UIView.init(frame: CGRect(x: 0, y : 0, width : chatTableView.frame.width, height : 30))
//        vw.backgroundColor = UIColor.clear
//
//        vw.addSubview(dateLabel)
//
//        return vw
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 20
//    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    // MARK : Scroll to bottom
    func tableViewScrollToBottom(_ animated: Bool)
    {
        let numberOfSections = self.chatTableView.numberOfSections
        if (numberOfSections > 0) {
            let numberOfRows = chatTableView.numberOfRows(inSection: numberOfSections-1)
            
            if (numberOfRows > 0) {
                let indexPath = IndexPath(row: numberOfRows-1 , section: numberOfSections-1)
                chatTableView.scrollToRow(at: indexPath,
                                          at: UITableView.ScrollPosition.bottom, animated: false)
            }
        }
    }
    
    func dayDifference(from interval : TimeInterval) -> String
    {
        let calendar = Calendar.current
        let date = Date(timeIntervalSince1970: interval)
        let startOfNow = calendar.startOfDay(for: Date())
        let startOfTimeStamp = calendar.startOfDay(for: date)
        let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
        let day = components.day!
        if abs(day) < 2 {
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            formatter.timeStyle = .none
            formatter.doesRelativeDateFormatting = true
            return formatter.string(from: date)
        } else if day > 1 {
            return "In \(day) days"
        } else {
            return "\(-day) days ago"
        }
    }
}

class FMeetChatLeftCell: UITableViewCell {
    
    @IBOutlet weak var msgLabel : UILabel!
    @IBOutlet weak var timeLabel : UILabel!
    
    @IBOutlet weak var msgwdConstraint : NSLayoutConstraint!
    @IBOutlet weak var msghtConstraint : NSLayoutConstraint!
}

class FMeetChatRightCell: UITableViewCell {
    
    @IBOutlet weak var msgLabel : UILabel!
    @IBOutlet weak var timeLabel : UILabel!
    
    @IBOutlet weak var msgwdConstraint : NSLayoutConstraint!
    @IBOutlet weak var msghtConstraint : NSLayoutConstraint!
}

class ChatDateCell: UITableViewCell {
    @IBOutlet weak var dateLabel : UILabel!
}

// MARK : Textview Delegates
extension FMeetMessagesVC : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textF.placeholder = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fm_type_message"))......"
        sendBtn.isEnabled = false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
      //  print("texttt = \(textView.text.count)----\(text)")
        if  textView.text.count == 1 && text == "" {
            let newPosition = textView.beginningOfDocument
            textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
            textF.placeholder = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fm_type_message"))......"
            textView.text = ""
            sendBtn.isEnabled = false
        }
        else if textView.text != "" {
            textF.placeholder = ""
            sendBtn.isEnabled = true
        }
        else {
            textF.placeholder = ""
            sendBtn.isEnabled = true
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        let fixedWidth = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
       // print("zzzzzzzzzz = \(newSize.height) --- \(cc)")
        
        if (newSize.height > cc) {
            
            if (newSize.height < 100)
            {
                textView.isScrollEnabled = false
                btmHtConst.constant = newSize.height + 10
            } else {
                textView.isScrollEnabled = true
                btmHtConst.constant = 110
            }
        }
    }
}

