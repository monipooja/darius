//
//  FMeetPhotosVC.swift
//  Darious
//
//  Created by Apple on 27/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class FMeetPhotosVC: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet var btnCollection: [UIButton]!
    @IBOutlet var imgCollection: [UIImageView]!
    
    @IBOutlet weak var btmMsgVw : CustomUIView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var ageLabel : UILabel!
    @IBOutlet weak var photoImgView : UIImageView!
    @IBOutlet weak var leftButton : UIButton!
    @IBOutlet weak var rightButton : UIButton!
    
    var checkStr : String = ""
    var userId : String = ""
    var passDict : NSDictionary = [:]
    var pass_tag : String = ""
    
    var xCenter: CGFloat = 0.0
    var yCenter: CGFloat = 0.0
    let theresoldMargin = (UIScreen.main.bounds.size.width/2) * 0.75
    let stength : CGFloat = 4
    let range : CGFloat = 0.90
    var originalPoint : CGPoint = CGPoint(x: 0, y: 0)
    var center : CGPoint = CGPoint(x: 0, y: 0)
    
    var timer : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FMeetThemeClr
        btmImageView.tintColor = ColorCode.FMeetThemeClr
        topHeaderImgVw.tintColor = ColorCode.FMeetThemeClr
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if ("\(btn.tag)" == pass_tag) {
                btn.backgroundColor = ColorCode.FMeetDarkTheme
                imgCollection[btn.tag].tintColor = UIColor.white
            }
            else {
                btn.backgroundColor = ColorCode.FMeetThemeClr
                imgCollection[btn.tag].tintColor = ColorCode.FMeetDarkTheme
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setDataOnLoad()
        
        if (checkStr == "search") || (checkStr == "around")
        {
            userId = "\(passDict["uid"] as! NSNumber)"
        }else {
            userId = "\(passDict["user_id"] as! NSNumber)"
        }
        setBottomView()
    }
    
    // MARK : Status bar content's color set to black
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setDataOnLoad()
    {
        nameLabel.text = "\(passDict["first_name"] as! String) \(passDict["last_name"] as! String)"
        placeLabel.text = passDict["address"] as? String
        
        let age = getAgeFromDOB(dob_str: passDict["dateOfBirth"] as! String)
        ageLabel.text = "\(age) year old"
        
        photoImgView.layer.cornerRadius = 12
        photoImgView.layer.masksToBounds = true
        if (passDict["profile_pic"] as! String).contains("https")
        {
            photoImgView.sd_setImage(with: URL(string: passDict["profile_pic"] as! String), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        } else {
            photoImgView.sd_setImage(with: URL(string: "\(Api.Profile_BaseURL)\(passDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        }
        
        if (checkStr == "search") || (checkStr == "around")
        {
            if (checkStr == "search"){
                titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "search")
            }else {
                titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fm_around_me")
            }
            leftButton.isHidden = false
            rightButton.isHidden = false
            rightButton.backgroundColor = UIColor.lightGray
        }
        else if (checkStr == "likes")
        {
            leftButton.isHidden = false
            rightButton.isHidden = true
            titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fm_my_likes")
        }
        else if (checkStr == "matches")
        {
            rightButton.isHidden = true
            leftButton.isHidden = false
            leftButton.setImage(#imageLiteral(resourceName: "fmeet_btm_chat"), for: .normal)
            titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fm_my_matches")
        }
    }
    
    @IBAction func likeUser(_ sender: Any)
    {
//        UIView.animate(withDuration: 0.6,
//                       animations: {
//                        self.rightButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
//        },
//                       completion: { _ in
//                        UIView.animate(withDuration: 0.6) {
//                            self.rightButton.transform = CGAffineTransform.identity
//                            self.rightButton.backgroundColor = ColorCode.FMeetDarkTheme
//                            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.onBack(_:)), userInfo: nil, repeats: false)
//                        }
//        })
        customLoader.showActivityIndicator(showColor: ColorCode.FMeetDarkTheme.toHexString(), controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(userId)"
        self.callService(urlstr: Api.Fmeet_Like_User, parameters: params, check: "like")
    }
    
    @IBAction func rejectUser(_ sender: Any)
    {
        if (checkStr == "matches")
        {
            let vc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetMessagesVC") as! FMeetMessagesVC
            vc.pass_id = "\(userId)"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FMeetDarkTheme.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(userId)"
            self.callService(urlstr: Api.Fmeet_Reject_User, parameters: params, check: "reject")
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetHomeVC") as! FMeetHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if let _ = timer
        {
            timer!.invalidate()
        }
        let transition = CATransition.init()
        transition.duration = 0.5
        transition.type = .fade
        self.navigationController?.view.layer.add(transition, forKey: "kCATransition")
        navigationController?.popViewController(animated: false)
    }
    
    // Mark : Call service
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            //   print("likes result = \(result)")
            OperationQueue.main.addOperation {
                customLoader.hideIndicator()
                
                if (check == "like")
                {
                    UIView.animate(withDuration: 0.6,
                                   animations: {
                                    self.rightButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                    },
                                   completion: { _ in
                                    UIView.animate(withDuration: 0.6) {
                                        self.rightButton.transform = CGAffineTransform.identity
                                        self.rightButton.backgroundColor = ColorCode.FMeetDarkTheme
                                        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.onBack(_:)), userInfo: nil, repeats: false)
                                    }
                    })
                }
                else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyMeetTopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyMeetBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    func addSwipablePansGesture() {
        let panRecognizer = UIPanGestureRecognizer(target:self, action: #selector(self.beingDragged(_:)))
        panRecognizer.delegate = self
        self.photoImgView.addGestureRecognizer(panRecognizer)
        
    }
    
    @objc func beingDragged(_ gestureRecognizer: UIPanGestureRecognizer) {
        
        xCenter = gestureRecognizer.translation(in: self.photoImgView).x
        yCenter = gestureRecognizer.translation(in: self.photoImgView).y
        switch gestureRecognizer.state {
        // Keep swiping
        case .began:
            originalPoint = self.photoImgView.center
            break;
        //in the middle of a swipe
        case .changed:
            let rotationStrength = min(xCenter / UIScreen.main.bounds.size.width, 1)
            let rotationAngel = .pi/8 * rotationStrength
            let scale = max(1 - abs(rotationStrength) / stength, range)
            center = CGPoint(x: originalPoint.x + xCenter, y: originalPoint.y + yCenter)
            let transforms = CGAffineTransform(rotationAngle: rotationAngel)
            let scaleTransform: CGAffineTransform = transforms.scaledBy(x: scale, y: scale)
            self.photoImgView.transform = scaleTransform
            break;
            
        // swipe ended
        case .ended:
            afterSwipeAction()
            break;
        case .possible:break
        case .cancelled:break
        case .failed:break
        default:
            fatalError()
            break
        }
    }
    
    fileprivate func afterSwipeAction() {
        
        if xCenter > theresoldMargin {
            cardGoesRight()
        }
        else if xCenter < -theresoldMargin {
            cardGoesLeft()
        }
        else {
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: [], animations: {
                self.photoImgView.center = self.originalPoint
                self.photoImgView.transform = CGAffineTransform(rotationAngle: 0)
            })
        }
    }
    
    func cardGoesRight() {
        
        let finishPoint = CGPoint(x: self.photoImgView.frame.size.width*2, y: 2 * yCenter + originalPoint.y)
        UIView.animate(withDuration: 0.5, animations: {
            self.photoImgView.center = finishPoint
        }, completion: {(_) in
            print("remove card in right")
            //self.delegate?.didRemoveCard(card: self)
        })
    }
    
    func cardGoesLeft() {
        
        let finishPoint = CGPoint(x: -self.photoImgView.frame.size.width*2, y: 2 * yCenter + originalPoint.y)
        UIView.animate(withDuration: 0.5, animations: {
            self.photoImgView.center = finishPoint
        }, completion: {(_) in
            print("remove card in left")
            //self.delegate?.didRemoveCard(card: self)
        })
    }
}
