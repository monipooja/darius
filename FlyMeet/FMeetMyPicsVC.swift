//
//  FMeetMyPicsVC.swift
//  Darious
//
//  Created by Apple on 28/07/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit
import OpalImagePicker
import Photos
import Alamofire

class FMeetMyPicsVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet var btnCollection: [UIButton]!
    @IBOutlet var imgCollection: [UIImageView]!
    
    @IBOutlet weak var btmMsgVw : CustomUIView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var notfound : UILabel!
    
    @IBOutlet weak var picsCollectionView : UICollectionView!
    
    var profile_pic : String = ""
    
    var pics_array : [NSDictionary] = []
    var selected_imgs : [UIImage] = []
    var final_images_arr : [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FMeetThemeClr
        btmImageView.tintColor = ColorCode.FMeetThemeClr
        topHeaderImgVw.tintColor = ColorCode.FMeetThemeClr
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            btn.backgroundColor = ColorCode.FMeetThemeClr
            imgCollection[btn.tag].tintColor = ColorCode.FMeetDarkTheme
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        notfound.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "My Pics")
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FMeetDarkTheme.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.Fmeet_My_pics, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to black
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }

    func showActionSheet()
    {
        let actionSheet = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
        
        let subview = actionSheet.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.backgroundColor = UIColor.init(hexString: "212121")
        actionSheet.view.tintColor = UIColor.init(hexString: "212121")
        alertContentView.layer.cornerRadius = 15
        
        //to change font of title and message.
        let titleFont = [NSAttributedString.Key.font: UIFont(name: "OpenSans-Semibold", size: 20.0)!]
        let titleAttrString = NSMutableAttributedString(string: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_upload_photos"), attributes: titleFont)
        actionSheet.setValue(titleAttrString, forKey: "attributedTitle")
        
        let action1 = UIAlertAction.init(title: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_choose_photos"), style: .default) { (action) in
            
            let imagePicker = OpalImagePickerController()
            imagePicker.imagePickerDelegate = self
            //Only allow image media type assets
            imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
            imagePicker.selectionImageTintColor = UIColor.white
            self.present(imagePicker, animated: true, completion: nil)
        }
        let icon1 = #imageLiteral(resourceName: "fp_gallery").imageWithSize(scaledToSize: CGSize(width: 28, height: 28))
        action1.setValue(icon1, forKey: "image")
        
        let action2 = UIAlertAction.init(title: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_take_photo"), style: .default) { (action) in
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerController.SourceType.camera;
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        let icon2 = #imageLiteral(resourceName: "fp_graycamera").imageWithSize(scaledToSize: CGSize(width: 32, height: 32))
        action2.setValue(icon2, forKey: "image")
        
        let action3 = UIAlertAction.init(title: ApiResponse.getLanguageFromUserDefaults(inputString: "cancel"), style: .default) { (action) in
        }
        action1.setValue(UIColor.init(hexString: "212121"), forKey: "titleTextColor")
        action2.setValue(UIColor.init(hexString: "212121"), forKey: "titleTextColor")
        //  action2.setValue(#imageLiteral(resourceName: "SelectCircle").withRenderingMode(.alwaysOriginal), forKey: "image")
        actionSheet.addAction(action1)
        actionSheet.addAction(action2)
        actionSheet.addAction(action3)
        present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var image = info[.originalImage] as! UIImage
        if let _ = fixedOrientation(passImg: image)
        {
            image = fixedOrientation(passImg: image)!
        }
        selected_imgs = []
        selected_imgs.append(image)
        final_images_arr.append(image)
        self.picsCollectionView.reloadData()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func uploadPics(_ sender: Any)
    {
        if (selected_imgs.count > 0)
        {
            uploadMultiplePhotos()
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetHomeVC") as! FMeetHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    // Mark : Call service
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            print("picssss \(check) result = \(result)")
            OperationQueue.main.addOperation {
                if (check == "get") {
                    
                    customLoader.hideIndicator()
                    self.pics_array = result["images"] as! [NSDictionary]
                    self.profile_pic = result["profile_pic"] as! String
                    for dd in self.pics_array
                    {
                        let img = "\(Api.Base_URL)public/flymeet/\(dd["pic_name"] as! String)".toImage()
                       // print("\(Api.Base_URL)public/flymeet/\(dd["pic_name"] as! String)")
                        if let _ = img
                        {
                          self.final_images_arr.append(img!)
                        }
                        else {
                            self.final_images_arr.append(#imageLiteral(resourceName: "Fm_Placeholder"))
                        }
                    }
                    self.picsCollectionView.reloadData()
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    /// MARK : Multipart (file format)
    func uploadMultiplePhotos()
    {
        let urlString = Api.Fmeet_upload_pics
    
        var imagesData : [Data] = []
        for img in selected_imgs
        {
            let image_data = img.jpegData(compressionQuality: 0.2)
            imagesData.append(image_data!)
        }
        
        let parameters : [String : String] = ["access_token":"\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)"]
        
        let ddd = Date()
        let dateformt = DateFormatter.init(format: "ddMMyyHHmmss")
        let dd_str = dateformt.string(from: ddd)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            for imageData in imagesData {
                multipartFormData.append(imageData, withName: "image[", fileName: "\(dd_str).jpeg", mimeType: "image/jpeg")
            }
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: urlString,
           
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    let ddict = try! JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    
                    let status = ddict["status"] as! NSNumber
                    
                    if (status == 1) {
                        customLoader.hideIndicator()
                        self.final_images_arr = []
                        self.selected_imgs = []
                        self.viewDidLoad()
                    }
                    else if(status == 3){
                        customLoader.hideIndicator()
                        Access_token = ""
                        userDefault.set(Access_token, forKey: "token")
                        userDefault.synchronize()
                        let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.navigationController?.pushViewController(VC, animated: true)
                    }
                    else {
                        if let str = ddict["message"] {
                            let msgStr = str as! String
                            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                            if(showMsg != "") {
                                ApiResponse.alert(title: "", message: showMsg, controller: self)
                            }
                            else {
                                ApiResponse.alert(title: "", message: msgStr, controller: self)
                            }
                        }
                        else {
                            customLoader.hideIndicator()
                        }
                    }
                    
                }
            case .failure(let error):
                //print(error.localizedDescription)
                ApiResponse.alert(title: "", message: error.localizedDescription, controller: self)
                customLoader.hideIndicator()
            }
        })
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyMeetTopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyMeetBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    /////////////
    func fixedOrientation(passImg : UIImage) -> UIImage? {
        
        guard passImg.imageOrientation != UIImage.Orientation.up else {
            //This is default orientation, don't need to do anything
            return passImg.copy() as? UIImage
        }
        
        guard let cgImage = passImg.cgImage else {
            //CGImage is not available
            return nil
        }
        // let colorSpace = cgImage.colorSpace,
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(passImg.size.width), height: Int(passImg.size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            
            return nil //Not able to create CGContext
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch passImg.imageOrientation {
            
        case .down, .downMirrored:
            
            transform = transform.translatedBy(x: passImg.size.width, y: passImg.size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
            
        case .left, .leftMirrored:
            
            transform = transform.translatedBy(x: passImg.size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
            
        case .right, .rightMirrored:
            
            transform = transform.translatedBy(x: 0, y: passImg.size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
            
        case .up, .upMirrored:
            break
            
        }
        
        //Flip image one more time if needed to, this is to prevent flipped image
        
        switch passImg.imageOrientation {
            
        case .upMirrored, .downMirrored:
            
            transform.translatedBy(x: passImg.size.width, y: 0)
            
            transform.scaledBy(x: -1, y: 1)
            
            break
            
        case .leftMirrored, .rightMirrored:
            
            transform.translatedBy(x: passImg.size.height, y: 0)
            
            transform.scaledBy(x: -1, y: 1)
            
        case .up, .down, .left, .right:
            
            break
        }
        
        ctx.concatenate(transform)
        
        switch passImg.imageOrientation {
            
        case .left, .leftMirrored, .right, .rightMirrored:
            
            ctx.draw(passImg.cgImage!, in: CGRect(x: 0, y: 0, width: passImg.size.height, height: passImg.size.width))
            
        default:
            
            ctx.draw(passImg.cgImage!, in: CGRect(x: 0, y: 0, width: passImg.size.width, height: passImg.size.height))
            
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
    
}

/// MAKR : CollectionView Methods
extension FMeetMyPicsVC : UICollectionViewDataSource, UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return final_images_arr.count + 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = picsCollectionView.dequeueReusableCell(withReuseIdentifier: "FMeetPicsCell", for: indexPath) as! FMeetPicsCell
        
        if (indexPath.item == 0)
        {
            if (profile_pic.contains("https") == true)
            {
                cell.imageVw.sd_setImage(with: URL(string: profile_pic), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            } else {
                cell.imageVw.sd_setImage(with: URL(string: "\(Api.Profile_BaseURL)\(profile_pic)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            }
            cell.plusButton.isHidden = true
        }
        else if (indexPath.item <= final_images_arr.count) && (indexPath.item != 0)
        {
            cell.plusButton.isHidden = false
            cell.imageVw.image = final_images_arr[indexPath.item-1]
            cell.plusButton.setImage(#imageLiteral(resourceName: "fmeet_cross"), for: .normal)
            cell.plusButton.backgroundColor = UIColor.white
            cell.plusButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            cell.plusButton.setTitle("", for: .normal)
            cell.plusButton.addTarget(self, action: #selector(addImages(_:)), for: .touchUpInside)
        }
        else if (indexPath.item > final_images_arr.count) && (indexPath.item != 0) {
            cell.imageVw.image = #imageLiteral(resourceName: "Placeholder")
            cell.plusButton.backgroundColor = ColorCode.FMeetDarkTheme
            cell.plusButton.setImage(nil, for: .normal)
            cell.plusButton.setTitle("+", for: .normal)
            cell.plusButton.addTarget(self, action: #selector(addImages(_:)), for: .touchUpInside)
            cell.plusButton.isHidden = false
            cell.plusButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }else{}
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            return CGSize(width: 130, height: 130)
        }
        else if (self.view.frame.width == 375)
        {
            return CGSize(width: 150, height: 150)
        }
        else {
            return CGSize(width: 170, height: 170)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10
    }
    
    @objc func addImages(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: picsCollectionView)
        let idxPath = picsCollectionView.indexPathForItem(at: point)!
        
        if (sender.backgroundColor == UIColor.white)
        {
            let transition = CATransition.init()
            transition.duration = 0.5
            transition.type = .fade
            self.picsCollectionView.layer.add(transition, forKey: "kCATransition")

            self.picsCollectionView.performBatchUpdates({
                self.picsCollectionView.deleteItems(at: [idxPath])
                self.final_images_arr.remove(at: idxPath.item-1)
                self.picsCollectionView.reloadData()
            }, completion:nil)
            
            if (idxPath.item <= pics_array.count)
            {
                let dict = self.pics_array[idxPath.item-1]
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&pic_id=\(dict["id"] as! NSNumber)"
                self.callService(urlstr: Api.Fmeet_Delete_pics, parameters: params, check: "delete")
            }
        }
        else {
            showActionSheet()
        }
    }
}

class FMeetPicsCell: UICollectionViewCell {
    
    @IBOutlet weak var imageVw : UIImageView!
    @IBOutlet weak var plusButton : KGHighLightedButton!
}

extension FMeetMyPicsVC : OpalImagePickerControllerDelegate
{
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    {
        selected_imgs = selected_imgs + images
        final_images_arr = final_images_arr + images
        self.picsCollectionView.reloadData()
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController)
    {
    }
}
