//
//  FMeetFilterVC.swift
//  Darious
//
//  Created by Apple on 27/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit
import TTRangeSlider

class FMeetFilterVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet var btnCollection: [UIButton]!
    @IBOutlet var imgCollection: [UIImageView]!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    @IBOutlet weak var btmMsgVw : CustomUIView!
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var distanceText : UILabel!
    @IBOutlet weak var distanceLbl : UILabel!
    @IBOutlet weak var ageText : UILabel!
    @IBOutlet weak var ageRangeLbl : UILabel!
    @IBOutlet weak var distSlider : UISlider!
    @IBOutlet weak var applyBtn: KGHighLightedButton!
    
    @IBOutlet weak var rangeSlider : TTRangeSlider!
    
    @IBOutlet weak var filterTableVw : UITableView!
    
    var heightArray : [CGFloat] = [0, 140, 165, 225, 40, 32]
    var section_titles : [String] = ["fm_i_am_a", "fm_looking_for", "fm_intentions", "fm_studies_level", "fm_interested_on"]
    var valueDict : NSMutableDictionary = ["iam" : "", "looking" : "", "intentions" : "", "studies" : "", "interest" : ""]
    
    var check : String = ""
    var distValue : String = "0"
    var ageValue : String = ""
    var idArray : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FMeetThemeClr
        btmImageView.tintColor = ColorCode.FMeetThemeClr
        topHeaderImgVw.tintColor = ColorCode.FMeetThemeClr
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 2) {
                btn.backgroundColor = ColorCode.FMeetDarkTheme
                imgCollection[btn.tag].tintColor = UIColor.white
            }
            else {
                btn.backgroundColor = ColorCode.FMeetThemeClr
                imgCollection[btn.tag].tintColor = ColorCode.FMeetDarkTheme
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        distanceText.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fm_distance")
        ageText.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fm_age_range")
        titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fm_filter")
        applyBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fm_apply"), for: .normal)
        
        rangeSlider.handleDiameter = 30
        rangeSlider.handleColor = UIColor.white
        rangeSlider.handleImage = #imageLiteral(resourceName: "Slider_thumb")

        showFilterData()
    }
    
    // MARK : Status bar content's color set to black
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func showFilterData()
    {
      //  print("filter dict = \(filterDict)")
        self.valueDict.setValue(filterDict["iam"] as! String, forKey: "iam")
        self.valueDict.setValue(filterDict["looking"] as! String, forKey: "looking")
        self.valueDict.setValue(filterDict["studies"] as! String, forKey: "studies")
        self.valueDict.setValue("\(filterDict["intentions"] as! String)", forKey: "intentions")
        self.valueDict.setValue(filterDict["interest"] as! String, forKey: "interest")
        self.valueDict.setValue(filterDict["distance"] as! String, forKey: "distance")
        
        distValue = filterDict["distance"] as! String
        self.distanceLbl.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fm_up_to")) \(distValue) mi"
        self.distSlider.value = Float(distValue) ?? 0
        
        ageValue = filterDict.value(forKey: "age_range") as! String
        ageRangeLbl.text = filterDict.value(forKey: "age_range") as? String
        
        if (ageValue != "") && (ageValue.contains("-"))
        {
            let arr_age = ageValue.split(separator: "-")
            self.rangeSlider.selectedMinimum = Float(arr_age[0]) ?? 17
            self.rangeSlider.selectedMaximum = Float(arr_age[1]) ?? 100
        }
        
        let arr = (filterDict["interest"] as! String).split(separator: ",")
        for ar in arr
        {
            self.idArray.add(ar)
        }
        self.filterTableVw.reloadData()
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        distanceLbl.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fm_up_to")) \(String(format: "%.2f", distSlider.value)) mi"
        distValue = String(format: "%.2f", distSlider.value)
    }
    
    @IBAction func onApply(_ sender: Any)
    {
        filterDict.setValue(valueDict["iam"] as! String, forKey: "iam")
        filterDict.setValue(valueDict["looking"] as! String, forKey: "looking")
        filterDict.setValue(valueDict["intentions"] as! String, forKey: "intentions")
        filterDict.setValue(valueDict["studies"] as! String, forKey: "studies")
        filterDict.setValue(valueDict["interest"] as! String, forKey: "interest")
        filterDict.setValue(distValue, forKey: "distance")
        filterDict.setValue(ageValue, forKey: "age_range")

        let archive = NSKeyedArchiver.archivedData(withRootObject: filterDict)
        userDefault.setValue(archive, forKey: "filter_dict")
        userDefault.synchronize()
        
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetSearchAroundVC") as! FMeetSearchAroundVC
        nextvc.check = "search"
        navigationController?.pushViewController(nextvc, animated: false)
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetHomeVC") as! FMeetHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    // Mark : Call service
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
          //  print("filter result = \(result)")
            OperationQueue.main.addOperation {
                
                customLoader.hideIndicator()
                ApiResponse.alert(title: "", message: (result["message"] as? String)!, controller: self)
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyMeetTopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyMeetBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FMeetFilterVC : TTRangeSliderDelegate
{
    func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float) {
       // print("min = \(Int(selectedMinimum))-----max = \(Int(selectedMaximum))")
        ageValue = "\(Int(selectedMinimum))" + "-" + "\(Int(selectedMaximum))"
        ageRangeLbl.text = "\(Int(selectedMinimum))-\(Int(selectedMaximum))"
    }
    

}

// MARK : table view methods
extension FMeetFilterVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5 + interestArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.section == 0)
        {
            let cell = filterTableVw.dequeueReusableCell(withIdentifier: "fmeetfilter1") as! FMeetFilterFirstCell
            
            cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: section_titles[indexPath.section])
            
            if (valueDict["iam"] as! String == "0") {
                cell.btnCollection[0].backgroundColor = ColorCode.FMeetDarkTheme
            }else if (valueDict["iam"] as! String == "1") {
                cell.btnCollection[1].backgroundColor = ColorCode.FMeetDarkTheme
            }else {
                cell.btnCollection[0].backgroundColor = UIColor.white
                cell.btnCollection[1].backgroundColor = UIColor.white
            }
            
            for btn in cell.btnCollection
            {
                btn.addTarget(self, action: #selector(iam(_:)), for: .touchUpInside)
            }
            
            return cell
        }
        else if (indexPath.section == 1)
        {
            let cell = filterTableVw.dequeueReusableCell(withIdentifier: "fmeetfilter2") as! FMeetFilterSecondCell
            
            cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: section_titles[indexPath.section])
            
            if (valueDict["looking"] as! String == "0") {
                cell.btnCollection[0].backgroundColor = ColorCode.FMeetDarkTheme
            }else if (valueDict["looking"] as! String == "1") {
                cell.btnCollection[1].backgroundColor = ColorCode.FMeetDarkTheme
            }else if (valueDict["looking"] as! String == "2") {
                cell.btnCollection[2].backgroundColor = ColorCode.FMeetDarkTheme
            }else {
                cell.btnCollection[0].backgroundColor = UIColor.white
                cell.btnCollection[1].backgroundColor = UIColor.white
                cell.btnCollection[2].backgroundColor = UIColor.white
            }
            
            for btn in cell.btnCollection
            {
                btn.addTarget(self, action: #selector(looikgFor(_:)), for: .touchUpInside)
            }
            
            return cell
        }
        else if (indexPath.section == 2) {
            let cell = filterTableVw.dequeueReusableCell(withIdentifier: "fmeetfilter3") as! FMeetFilterThirdCell
            
            cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: section_titles[indexPath.section])
            
            for btn in cell.btnCollection
            {
                let dict = intentionArray[btn.tag]
                cell.LblCollection[btn.tag].text = dict["name"] as? String
                
                if (valueDict["intentions"] as! String == "\(dict["id"] as! NSNumber)") {
                    btn.backgroundColor = ColorCode.FMeetDarkTheme
                } else {
                    btn.backgroundColor = UIColor.white
                }
                
                btn.addTarget(self, action: #selector(intentions(_:)), for: .touchUpInside)
            }
            
            return cell
        }
        else if (indexPath.section == 3) {
            let cell = filterTableVw.dequeueReusableCell(withIdentifier: "fmeetfilter4") as! FMeetFilterFourthCell
            
            cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: section_titles[indexPath.section])
            
            for btn in cell.btnCollection
            {
                cell.LblCollection[btn.tag].text = studiesArray[btn.tag]
                
                if (valueDict["studies"] as! String == "\(btn.tag)") {
                    btn.backgroundColor = ColorCode.FMeetDarkTheme
                } else {
                    btn.backgroundColor = UIColor.white
                }
                
                btn.addTarget(self, action: #selector(studiesLevel(_:)), for: .touchUpInside)
            }
            
            return cell
        }
        else if (indexPath.section == 4) {
            let cell = filterTableVw.dequeueReusableCell(withIdentifier: "fmeetfilter5") as! FMeetFilterFifthCell
            
            cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: section_titles[indexPath.section])
            
            return cell
        }
        else {
            let cell = filterTableVw.dequeueReusableCell(withIdentifier: "fmeetfilter6") as! FMeetFilterSixthCell
            
            let dict = interestArray[indexPath.section-5]
            cell.textLbl.text = dict["name"] as? String
            
            if (indexPath.section == (5+interestArray.count-1))
            {
                cell.btmConstraint.constant = 2
                if #available(iOS 11.0, *) {
                    cell.bgView.layer.cornerRadius = 12
                    cell.bgView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                } else {
                    // Fallback on earlier versions
                    cell.bgView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 12, szz: cell.bgView!.frame.size)
                }
            }else {
                cell.btmConstraint.constant = -5
            }
            
            if (idArray.count > 0) && (idArray.contains("\(dict["id"] as! NSNumber)")) {
                cell.checkButton.backgroundColor = ColorCode.FMeetDarkTheme
            } else {
                cell.checkButton.backgroundColor = UIColor.white
            }
            
            cell.checkButton.addTarget(self, action: #selector(interestedOn(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section <= 5)
        {
            return heightArray[indexPath.section]
        }
        return 32
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: filterTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (section <= 3)
        {
            return 10
        }
        return 0
    }
    
    @objc func iam(_ sender : UIButton)
    {
        let _ = setBtnColor(button: sender, check: "first")
        
        if (sender.tag == 0){
            valueDict.setValue("0", forKey: "iam")
        }else {
            valueDict.setValue("1", forKey: "iam")
        }
    }
    
    @objc func looikgFor(_ sender : UIButton)
    {
        let b_value = setBtnColor(button: sender, check: "second")
        
        if (b_value == true)
        {
            if (sender.tag == 0){
                valueDict.setValue("0", forKey: "looking")
            }else if (sender.tag == 1) {
                valueDict.setValue("1", forKey: "looking")
            }else {
                valueDict.setValue("2", forKey: "looking")
            }
        }
        else {
            valueDict.setValue("", forKey: "looking")
        }
    }
    
    @objc func intentions(_ sender : UIButton)
    {
        let b_value = setBtnColor(button: sender, check: "third")
        
        if (b_value == true)
        {
            let dict = intentionArray[sender.tag]
            valueDict.setValue("\(dict["id"] as! NSNumber)", forKey: "intentions")
        }
        else {
            valueDict.setValue("", forKey: "intentions")
        }
    }
    
    @objc func studiesLevel(_ sender : UIButton)
    {
        let b_value = setBtnColor(button: sender, check: "fourth")
        if (b_value == true)
        {
            valueDict.setValue("\(sender.tag)", forKey: "studies")
        }
        else {
            valueDict.setValue("", forKey: "studies")
        }
    }
    
    @objc func interestedOn(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: filterTableVw)
        let idxpath = filterTableVw.indexPathForRow(at: point)!
        let dict = interestArray[idxpath.section-5]
        
        if (sender.backgroundColor == ColorCode.FMeetDarkTheme)
        {
            sender.backgroundColor = UIColor.white
            if (idArray.contains("\(dict["id"] as! NSNumber)"))
            {
                idArray.remove("\(dict["id"] as! NSNumber)")
            }
        }
        else {
            sender.backgroundColor = ColorCode.FMeetDarkTheme
            idArray.add("\(dict["id"] as! NSNumber)")
        }
        var str = ""
        for ii in idArray
        {
            if (str == "")
            {
                str = "\(ii)"
            } else {
                str = "\(str),\(ii)"
            }
        }
        valueDict.setValue(str, forKey: "interest")
    }
    
    // common function
    func setBtnColor(button : UIButton, check : String) -> Bool
    {
        let point = button.convert(CGPoint.zero, to: filterTableVw)
        let idxpath = filterTableVw.indexPathForRow(at: point)!
        
        var btnarr : [UIButton] = []
        
        if (check == "first")
        {
            let cell = filterTableVw.cellForRow(at: idxpath) as! FMeetFilterFirstCell
            btnarr = cell.btnCollection
        }
        else if (check == "second")
        {
            let cell = filterTableVw.cellForRow(at: idxpath) as! FMeetFilterSecondCell
            btnarr = cell.btnCollection
        }
        else if (check == "third")
        {
            let cell = filterTableVw.cellForRow(at: idxpath) as! FMeetFilterThirdCell
            btnarr = cell.btnCollection
        }
        else
        {
            let cell = filterTableVw.cellForRow(at: idxpath) as! FMeetFilterFourthCell
            btnarr = cell.btnCollection
        }
        
        var retBool = false
        for btn in btnarr
        {
            if (btn.tag == button.tag) {
                if (btn.backgroundColor == ColorCode.FMeetDarkTheme)
                {
                    btn.backgroundColor = UIColor.white
                    retBool = false
                }
                else {
                    btn.backgroundColor = ColorCode.FMeetDarkTheme
                    retBool = true
                }
            } else {
                btn.backgroundColor = UIColor.white
            }
        }
        return retBool
    }
}

class FMeetFilterFirstCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var girlLabel : UILabel!
    @IBOutlet weak var boyLabel : UILabel!
    
    @IBOutlet var btnCollection : [UIButton]!
}

class FMeetFilterSecondCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var girlLabel : UILabel!
    @IBOutlet weak var boyLabel : UILabel!
    @IBOutlet weak var bothLabel : UILabel!
    
    @IBOutlet var btnCollection : [UIButton]!
}

class FMeetFilterThirdCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet var LblCollection : [UILabel]!
    
    @IBOutlet var btnCollection : [UIButton]!
}

class FMeetFilterFourthCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet var LblCollection : [UILabel]!
    
    @IBOutlet var btnCollection : [UIButton]!
}

class FMeetFilterFifthCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
}

class FMeetFilterSixthCell: UITableViewCell {
    
    @IBOutlet weak var textLbl : UILabel!
    @IBOutlet weak var checkButton : UIButton!
    @IBOutlet weak var bgView : CustomUIView!
    
    @IBOutlet weak var topConstraint : NSLayoutConstraint!
    @IBOutlet weak var btmConstraint : NSLayoutConstraint!
}
