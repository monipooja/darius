//
//  FMeetMatchesVC.swift
//  Darious
//
//  Created by Apple on 27/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class FMeetMatchesVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet var btnCollection: [UIButton]!
    @IBOutlet var imgCollection: [UIImageView]!
    
    @IBOutlet weak var btmMsgVw : CustomUIView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var notfound : UILabel!
    
    @IBOutlet weak var homeCollectionView : UICollectionView!
    
    var check : String = ""
    
    var userArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FMeetThemeClr
        btmImageView.tintColor = ColorCode.FMeetThemeClr
        topHeaderImgVw.tintColor = ColorCode.FMeetThemeClr
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 1) {
                btn.backgroundColor = ColorCode.FMeetDarkTheme
                imgCollection[btn.tag].tintColor = UIColor.white
            }
            else {
                btn.backgroundColor = ColorCode.FMeetThemeClr
                imgCollection[btn.tag].tintColor = ColorCode.FMeetDarkTheme
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        notfound.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fm_my_matches")
    }
    
    // MARK : Status bar content's color set to black
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FMeetDarkTheme.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.Fmeet_Matches, parameters: params, check: "get")
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetHomeVC") as! FMeetHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    // Mark : Call service
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
        //    print("matches result = \(result)")
            OperationQueue.main.addOperation {
                if (check == "get") {
                    self.userArray = result["data"] as! [NSDictionary]
                    if (self.userArray.count > 0)
                    {
                        self.homeCollectionView.reloadData()
                    } else {
                        self.homeCollectionView.isHidden = true
                    }
                    customLoader.hideIndicator()
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyMeetTopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyMeetBottomViewAction(tagValue: sender.tag, controller: self)
    }

}

/// MAKR : CollectionView Methods
extension FMeetMatchesVC : UICollectionViewDataSource, UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "fmeetmatchecell", for: indexPath) as! FMeetMatcheCell
        
        let dict = userArray[indexPath.item]
        
        cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        cell.placeLabel.text = dict["address"] as? String
        
        cell.imageVw.layer.cornerRadius = 8
        cell.imageVw.layer.masksToBounds = true
        if (dict["profile_pic"] as! String).contains("https")
        {
            cell.imageVw.sd_setImage(with: URL(string: dict["profile_pic"] as! String), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        } else {
            cell.imageVw.sd_setImage(with: URL(string: "\(Api.Profile_BaseURL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        }
        let age = getAgeFromDOB(dob_str: dict["dateOfBirth"] as! String)
        cell.ageLabel.text = "\(age) year old"
        
        cell.chatBtn.addTarget(self, action: #selector(gotochat(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = userArray[indexPath.item]
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetPhotosVC") as! FMeetPhotosVC
        nextvc.checkStr = "matches"
        nextvc.passDict = dict
        nextvc.pass_tag = "1"
        
        let transition = CATransition.init()
        transition.duration = 0.5
        transition.type = .fade
        transition.subtype = CATransitionSubtype.fromTop
        self.navigationController?.view.layer.add(transition, forKey: "kCATransition")
        navigationController?.pushViewController(nextvc, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10
    }
    
    @objc func gotochat(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: homeCollectionView)
        let idxPath = homeCollectionView.indexPathForItem(at: point)!
        let dict = userArray[idxPath.item]
        
        let vc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetMessagesVC") as! FMeetMessagesVC
        vc.pass_id = "\(dict["user_id"] as! NSNumber)"
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

class FMeetMatcheCell: UICollectionViewCell {
    
    @IBOutlet weak var imageVw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var ageLabel : UILabel!
    
    @IBOutlet weak var chatBtn : KGHighLightedButton!
}
