//
//  FMeetSearchAroundVC.swift
//  Darious
//
//  Created by Apple on 27/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit
import CoreLocation

class FMeetSearchAroundVC: UIViewController, CLLocationManagerDelegate, UIViewControllerTransitioningDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet var btnCollection: [UIButton]!
    @IBOutlet var imgCollection: [UIImageView]!
    
    @IBOutlet weak var btmMsgVw : CustomUIView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var notfound : UILabel!
    
    @IBOutlet weak var homeCollectionView : UICollectionView!
    
    var check : String = ""
    var locLattitude : CLLocationDegrees = 0
    var locLongitude : CLLocationDegrees = 0
    
    var userArray : [NSDictionary] = []
    
    var selectedCell : FMeetSearchCell?
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FMeetThemeClr
        btmImageView.tintColor = ColorCode.FMeetThemeClr
        topHeaderImgVw.tintColor = ColorCode.FMeetThemeClr
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 0) && (check == "around") {
                btn.backgroundColor = ColorCode.FMeetDarkTheme
                imgCollection[btn.tag].tintColor = UIColor.white
            }
            else {
                btn.backgroundColor = ColorCode.FMeetThemeClr
                imgCollection[btn.tag].tintColor = ColorCode.FMeetDarkTheme
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        notfound.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        
        if (check == "around") {
            titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fm_around_me")
        } else {
            titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "search")
        }
    }
    
    // MARK : Status bar content's color set to black
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        var age1 = ""
        var age2 = ""
        let age = filterDict.value(forKey: "age_range") as! String
        if (age != "") && (age.contains("-"))
        {
            let arr_age = age.split(separator: "-")
            age1 = String(arr_age[0])
            age2 = String(arr_age[1])
        }
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FMeetDarkTheme.toHexString(), controller: self.view)
            if (self.check == "search") {
                
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&am_i=\(filterDict["iam"] as! String)&looking_for=\(filterDict["looking"] as! String)&intention=\(filterDict["intentions"] as! String)&studies_level=\(filterDict["studies"] as! String)&interested_on=\(filterDict["interest"] as! String)&age_range_1=\(age1)&age_range_2=\(age2)"
                self.callService(urlstr: Api.Fmeet_Users, parameters: params, check: "get")
            } else {
                
                if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                    CLLocationManager.authorizationStatus() ==  .authorizedAlways)
                {
                    self.getLocationData()
                }else{
                    let alert = UIAlertController(title: "App Permission Denied", message: "To re-enable, please go to Settings and turn on Location Service for this app.", preferredStyle: UIAlertController.Style.alert)
                    
                    // add an action (button)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (act) in
                        self.navigationController?.popViewController(animated: false)
                    }))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetHomeVC") as! FMeetHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    // Mark : Call service
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
           // print("users result = \(result)")
            OperationQueue.main.addOperation {
                if (check == "get") {
                    self.userArray = result["data"] as! [NSDictionary]
                    if (self.userArray.count > 0)
                    {
                        self.homeCollectionView.reloadData()
                    } else {
                        self.homeCollectionView.isHidden = true
                    }
                    customLoader.hideIndicator()
                }
                else {
                    customLoader.hideIndicator()
                    self.viewDidAppear(true)
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyMeetTopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyMeetBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    // MARK : Location
    func getLocationData()
    {
        locationManager = CLLocationManager()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        else {
            let alert = UIAlertController(title: "App Permission Denied", message: "To re-enable, please go to Settings and turn on Location Service for this app.", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
         // print("locations = \(locValue.latitude) \(locValue.longitude)")
        locLattitude = locValue.latitude
        locLongitude = locValue.longitude
        
        locationManager.stopUpdatingLocation()
        
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&lat1=\(self.locLattitude)&lon1=\(self.locLongitude)"
        self.callService(urlstr: Api.Fmeet_Around_Me, parameters: params, check: "get")
    }
        
}

/// MAKR : CollectionView Methods
extension FMeetSearchAroundVC : UICollectionViewDataSource, UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "fmeetsearchcell", for: indexPath) as! FMeetSearchCell
        
        let dict = userArray[indexPath.item]
        
        cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        cell.placeLabel.text = dict["address"] as? String
        
        cell.imageVw.layer.cornerRadius = 8
        cell.imageVw.layer.masksToBounds = true
        if (dict["profile_pic"] as! String).contains("https")
        {
            cell.imageVw.sd_setImage(with: URL(string: dict["profile_pic"] as! String), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        } else {
            cell.imageVw.sd_setImage(with: URL(string: "\(Api.Profile_BaseURL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        }
        
        let age = getAgeFromDOB(dob_str: dict["dateOfBirth"] as! String)
        cell.ageLabel.text = "\(age) year old"
    
        cell.likeBtn.addTarget(self, action: #selector(likeUser(_:)), for: .touchUpInside)
        cell.rejectBtn.addTarget(self, action: #selector(rejectUser(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = userArray[indexPath.item]
        
        selectedCell = homeCollectionView.cellForItem(at: indexPath) as? FMeetSearchCell
        
        let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetPhotosVC") as! FMeetPhotosVC
        if (self.check == "search") {
            nextvc.checkStr = "search"
        } else {
            nextvc.checkStr = "around"
            nextvc.pass_tag = "0"
        }
        nextvc.passDict = dict
        
        let transition = CATransition.init()
        transition.duration = 0.5
        transition.type = .fade
        self.navigationController?.view.layer.add(transition, forKey: "kCATransition")
        self.navigationController?.pushViewController(nextvc, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            return CGSize(width: 130, height: 215)
        }
        else if (self.view.frame.width == 375)
        {
            return CGSize(width: 150, height: 215)
        }
        else {
            return CGSize(width: 170, height: 215)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10
    }
    
    @objc func likeUser(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: homeCollectionView)
        let idxPath = homeCollectionView.indexPathForItem(at: point)!
        let dict = userArray[idxPath.item]
        
        customLoader.showActivityIndicator(showColor: ColorCode.FMeetDarkTheme.toHexString(), controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(dict["uid"] as! NSNumber)"
        self.callService(urlstr: Api.Fmeet_Like_User, parameters: params, check: "like")
    }
    
    @objc func rejectUser(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: homeCollectionView)
        let idxPath = homeCollectionView.indexPathForItem(at: point)!
        let dict = userArray[idxPath.item]
        
        customLoader.showActivityIndicator(showColor: ColorCode.FMeetDarkTheme.toHexString(), controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(dict["uid"] as! NSNumber)"
        self.callService(urlstr: Api.Fmeet_Reject_User, parameters: params, check: "reject")
    }
}

class FMeetSearchCell: UICollectionViewCell {
  
    @IBOutlet weak var imageVw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var ageLabel : UILabel!
    
    @IBOutlet weak var rejectBtn : KGHighLightedButton!
    @IBOutlet weak var likeBtn : KGHighLightedButton!
}


