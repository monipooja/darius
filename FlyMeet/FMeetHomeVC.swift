//
//  FMeetHomeVC.swift
//  Darious
//
//  Created by Apple on 27/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class FMeetHomeVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var btmMsgVw : CustomUIView!
    
    @IBOutlet weak var homeCollectionView : UICollectionView!
    
    var titleArray : [String] = ["search", "fm_around_me", "fm_my_pics", "ff_my_profile", "fm_my_likes", "fm_messages", "fm_my_matches", "fm_rejected"]
    var imageArray : [UIImage] = [#imageLiteral(resourceName: "fmeet_search"), #imageLiteral(resourceName: "fmeet_aroundme"), #imageLiteral(resourceName: "fmeet_pics"), #imageLiteral(resourceName: "fmeet_profile"), #imageLiteral(resourceName: "fmeet_mylikes"), #imageLiteral(resourceName: "fmeet_messages"), #imageLiteral(resourceName: "fmeet_mymatches"), #imageLiteral(resourceName: "fmeet_rejected")]
    var wdArray : [CGFloat] = [76,70,70,48,44,49,66,45]
    var htArray : [CGFloat] = [84,84,50,56,56,56,56,56]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FMeetThemeClr
        btmImageView.tintColor = ColorCode.FMeetThemeClr
        topHeaderImgVw.tintColor = ColorCode.FMeetThemeClr
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        if (Int(truncating: msg_count) > 0)
        {
            btmMsgVw.isHidden = false
        }
        
        homeCollectionView.dataSource = self
        homeCollectionView.delegate = self
        homeCollectionView.reloadData()
    }
    
    // MARK : Status bar content's color set to black
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetHomeVC") as! FMeetHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyMeetBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

/// MAKR : CollectionView Methods
extension FMeetHomeVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (indexPath.item <= 1)
        {
            let cell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "fmeethomecell", for: indexPath) as! FMeetHomeCollCell
            
            cell.imageVw.image = imageArray[indexPath.item]
            cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: titleArray[indexPath.item])
            
            cell.wdConstraint.constant = wdArray[indexPath.item]
            cell.htConstraint.constant = htArray[indexPath.item]
            
            cell.mainVw.isHidden = false
            
            cell.mainVw.addshadow(top: false, left: true, bottom: true, right: true, shadowRadius: 2.0)
            
            return cell
        }
        else if (indexPath.item <= 7) {
            let cell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "fmeethomecell2", for: indexPath) as! FMeetHome2CollCell
            
            cell.imageVw.image = imageArray[indexPath.item]
            cell.titleLabel.text = titleArray[indexPath.item]
            
            cell.wdConstraint.constant = wdArray[indexPath.item]
            cell.htConstraint.constant = htArray[indexPath.item]
            
            cell.mainVw.isHidden = false
            
            cell.mainVw.addshadow(top: false, left: true, bottom: true, right: true, shadowRadius: 2.0)
            
            return cell
        } else {
            let cell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "fmeethomecell", for: indexPath) as! FMeetHomeCollCell
            
            cell.imageVw.image = nil
            cell.titleLabel.text = ""
            cell.mainVw.isHidden = true
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        switch indexPath.item {
        case 0:
            let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetSearchAroundVC") as! FMeetSearchAroundVC
            nextvc.check = "search"
            navigationController?.pushViewController(nextvc, animated: false)
        case 1:
            let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetSearchAroundVC") as! FMeetSearchAroundVC
            nextvc.check = "around"
            navigationController?.pushViewController(nextvc, animated: false)
        case 2:
            let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetMyPicsVC") as! FMeetMyPicsVC
            navigationController?.pushViewController(nextvc, animated: false)
        case 3:
            let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetProfileVC") as! FMeetProfileVC
            navigationController?.pushViewController(nextvc, animated: false)
        case 4:
            let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetLikesVC") as! FMeetLikesVC
            navigationController?.pushViewController(nextvc, animated: false)
        case 5:
            let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetChatListVC") as! FMeetChatListVC
            navigationController?.pushViewController(nextvc, animated: false)
        case 6:
            let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetMatchesVC") as! FMeetMatchesVC
            navigationController?.pushViewController(nextvc, animated: false)
        case 7:
            let nextvc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetRejectedVC") as! FMeetRejectedVC
            navigationController?.pushViewController(nextvc, animated: false)
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (indexPath.item <= 1)
        {
            return CGSize(width: 150, height: 140)
        }
        else
        {
            return CGSize(width: 150, height: 112)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10
    }
}

class FMeetHomeCollCell: UICollectionViewCell {
    
    @IBOutlet weak var mainVw : CustomUIView!
    @IBOutlet weak var imageVw : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    
    @IBOutlet weak var wdConstraint : NSLayoutConstraint!
    @IBOutlet weak var htConstraint : NSLayoutConstraint!
}

class FMeetHome2CollCell: UICollectionViewCell {
    
    @IBOutlet weak var mainVw : CustomUIView!
    @IBOutlet weak var imageVw : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    
    @IBOutlet weak var wdConstraint : NSLayoutConstraint!
    @IBOutlet weak var htConstraint : NSLayoutConstraint!
}

extension UIView {
    func addshadow(top: Bool,
                   left: Bool,
                   bottom: Bool,
                   right: Bool,
                   shadowRadius: CGFloat = 2.0) {
        
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = 1.0
        
        let path = UIBezierPath()
        var x: CGFloat = 0
        var y: CGFloat = 0
        var viewWidth = self.frame.width
        var viewHeight = self.frame.height
        
        // here x, y, viewWidth, and viewHeight can be changed in
        // order to play around with the shadow paths.
        if (!top) {
            y+=(shadowRadius+1)
        }
        if (!bottom) {
            viewHeight-=(shadowRadius+1)
        }
        if (!left) {
            x+=(shadowRadius+1)
        }
        if (!right) {
            viewWidth-=(shadowRadius+1)
        }
        // selecting top most point
        path.move(to: CGPoint(x: x, y: y))
        // Move to the Bottom Left Corner, this will cover left edges
        /*
         |☐
         */
        path.addLine(to: CGPoint(x: x, y: viewHeight))
        // Move to the Bottom Right Corner, this will cover bottom edge
        /*
         ☐
         -
         */
        path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
        // Move to the Top Right Corner, this will cover right edge
        /*
         ☐|
         */
        path.addLine(to: CGPoint(x: viewWidth, y: y))
        // Move back to the initial point, this will cover the top edge
        /*
         _
         ☐
         */
        path.close()
        self.layer.shadowPath = path.cgPath
    }
}
