//
//  FMeetProfileVC.swift
//  Darious
//
//  Created by Apple on 27/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit
import  CoreLocation

class FMeetProfileVC: UIViewController, CLLocationManagerDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet var btnCollection: [UIButton]!
    @IBOutlet var imgCollection: [UIImageView]!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var topView : UIView!
    @IBOutlet weak var vwHtConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var btmMsgVw : CustomUIView!
    @IBOutlet weak var bottomView : UIView!
    @IBOutlet weak var tableBtmConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var upperView : UIView!
    @IBOutlet weak var imgView : CustomUIView!
    @IBOutlet weak var userImageVw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var editBtn : UIButton!
    @IBOutlet weak var dobTF : UITextField!
    @IBOutlet weak var applyBtn: KGHighLightedButton!
    
    @IBOutlet weak var imgNameView : UIView!
    @IBOutlet weak var imgNameHtCons : NSLayoutConstraint!
    
    @IBOutlet weak var profileTableVw : UITableView!
    
    var heightArray : [CGFloat] = [110, 140, 165, 225, 40, 32]
    var section_titles : [String] = ["fm_i_am_a", "fm_looking_for", "fm_intentions", "fm_studies_level", "fm_interested_on"]
    var valueDict : NSMutableDictionary = ["iam" : "", "looking" : "", "intentions" : "", "studies" : "", "interest" : "", "age_range" : ""]
    
    var datePicker : UIDatePicker?
    var doneButton : UIButton = UIButton()
    var dob_str : String = ""
    var age_str : Int = 0
    var check : String = ""
    var distValue : String = "0"
    var locLattitude : CLLocationDegrees = 0
    var locLongitude : CLLocationDegrees = 0
    
    var idArray : NSMutableArray = []
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FMeetThemeClr
        btmImageView.tintColor = ColorCode.FMeetThemeClr
        topHeaderImgVw.tintColor = ColorCode.FMeetThemeClr
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 3) && (check != "launch") {
                btn.backgroundColor = ColorCode.FMeetDarkTheme
                imgCollection[btn.tag].tintColor = UIColor.white
            }
            else {
                btn.backgroundColor = ColorCode.FMeetThemeClr
                imgCollection[btn.tag].tintColor = ColorCode.FMeetDarkTheme
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        getLocationData()
        addDatePicker()
        
        if (userData["date_of_birth"] as! String != "0000-00-00") && (userData["date_of_birth"] as! String != "")
        {
            dobTF.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "dob")) :- \((userData["date_of_birth"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd-MM-yyyy")!)"
            let passDateStr = userData["date_of_birth"] as! String
            let datestr = passDateStr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd-MM-yyyy")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let convert_date = dateFormatter.date(from: datestr!)
            let components = Set<Calendar.Component>([.year])
            let ageComponents = Calendar.current.dateComponents(components, from: (convert_date)!, to: Date())
            age_str = ageComponents.year!
        }
        
        dobTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "dob")
        titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_profile")
        applyBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fm_apply"), for: .normal)
        
        if (check == "launch")
        {
            upperView.frame.size = CGSize(width: upperView.frame.width, height: 60)
            userImageVw.isHidden = false
            imgNameHtCons.constant = 0
            imgNameView.isHidden = true
            btmVwHt.constant = 0
            bottomView.isHidden = true
            tableBtmConstraint.constant = 0
            topView.isHidden = true
            vwHtConstraint.constant = 0
            
            editBtn.isHidden = true
            
            if (intentionArray.count > 0)
            {
                profileTableVw.reloadData()
            } else {
                profileTableVw.isHidden = true
            }
        } else {
            editBtn.isHidden = false
            vwHtConstraint.constant = 32
            DispatchQueue.main.async {
                customLoader.showActivityIndicator(showColor: ColorCode.FMeetDarkTheme.toHexString(), controller: self.view)
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
                self.callService(urlstr: Api.Fmeet_Profile, parameters: params, check: "get")
            }
        }
    }
    
    // MARK : Status bar content's color set to black
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @IBAction func onApply(_ sender: Any)
    {
        if (age_str < 17)
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fm_age_must_be_17"), controller: self)
        }
        else {
            filterDict.setValue(valueDict["iam"] as! String, forKey: "iam")
            
            let archive = NSKeyedArchiver.archivedData(withRootObject: filterDict)
            userDefault.setValue(archive, forKey: "filter_dict")
            userDefault.synchronize()
            
            customLoader.showActivityIndicator(showColor: ColorCode.FMeetDarkTheme.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&i_am=\(valueDict["iam"] as! String)&looking_for=\(valueDict["looking"] as! String)&intention=\(valueDict["intentions"] as! String)&studies_level=\(valueDict["studies"] as! String)&interest=\(valueDict["interest"] as! String)&distance=\(distValue)&age_range=\(age_str)&lattitude=\(locLattitude)&longitude=\(locLongitude)"
            if (check == "launch") {
                callService(urlstr: Api.Fmeet_Create_User, parameters: params, check: "create")
            } else {
                callService(urlstr: Api.Fmeet_Create_User, parameters: params, check: "update")
            }
        }
    }
    
    @IBAction func selectDOB(_ sender: Any)
    {
        if (check == "launch")
        {
            if (userData["date_of_birth"] as! String == "0000-00-00") || (userData["date_of_birth"] as! String == "")
            {
                datePicker?.isHidden = false
                doneButton.isHidden = false
            }
        }
    }
    
    @IBAction func editPic(_ sender: Any)
    {
        let homevc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetMyPicsVC") as! FMeetMyPicsVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        if (check != "launch")
        {
            let homevc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetHomeVC") as! FMeetHomeVC
            navigationController?.pushViewController(homevc, animated: false)
        }
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (check == "launch")
        {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
        }
        else {
            navigationController?.popViewController(animated: false)
        }
    }
    
    // Mark : Call service
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
        
            OperationQueue.main.addOperation {
               // print("\(check) result = \(result)")
                if (check == "get")
                {
                    let dict = result["data"] as! NSDictionary
                    
                    if ((dict["profile_pic"] as! String).contains("http")) {
                        self.userImageVw.sd_setImage(with: URL(string: dict["profile_pic"] as! String), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                    }
                    else {
                        self.userImageVw.sd_setImage(with: URL(string: "\(Api.Profile_BaseURL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                    }
                    self.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
                    self.saveToLocal(dict: dict)
                    
                    let arr = (dict["interested_on"] as! String).split(separator: ",")
                    for ar in arr
                    {
                        self.idArray.add(ar)
                    }
                    self.profileTableVw.reloadData()
                    customLoader.hideIndicator()
                }
                else if (check == "create") {
                    customLoader.hideIndicator()
                    let homevc = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetHomeVC") as! FMeetHomeVC
                    self.navigationController?.pushViewController(homevc, animated: false)
                }
                else {
                    let msgstr = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msgstr, actions: [OkText], controller: self, completion: { (str) in
                        
                        filterDict = NSMutableDictionary.init(dictionary: self.valueDict)
                        
                        let archive = NSKeyedArchiver.archivedData(withRootObject: self.valueDict)
                        userDefault.setValue(archive, forKey: "filter_dict")
                        userDefault.synchronize()
                    })
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    func saveToLocal(dict : NSDictionary)
    {
        self.valueDict.setValue(dict["i_am"] as! String, forKey: "iam")
        self.valueDict.setValue(dict["looking_for"] as! String, forKey: "looking")
        self.valueDict.setValue(dict["studies_level"] as! String, forKey: "studies")
        self.valueDict.setValue("\(dict["intention"] as! NSNumber)", forKey: "intentions")
        self.valueDict.setValue(dict["interested_on"] as! String, forKey: "interest")
        self.valueDict.setValue(dict["age_range"] as! String, forKey: "age_range")
        self.valueDict.setValue("\(dict["distance"] as! NSNumber)", forKey: "distance")
        
        filterDict = NSMutableDictionary.init(dictionary: self.valueDict)
        
        let archive = NSKeyedArchiver.archivedData(withRootObject: self.valueDict)
        userDefault.setValue(archive, forKey: "filter_dict")
        userDefault.synchronize()
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyMeetTopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyMeetBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    // MARK : Added DatePicker To View
    func addDatePicker()
    {
        datePicker = UIDatePicker.init(frame: CGRect(x: -30, y: self.view.frame.height-200, width: self.view.frame.width+30, height: 200))
        datePicker?.datePickerMode = .date
        datePicker?.backgroundColor = UIColor.white
        datePicker?.tintColor = UIColor.black
        datePicker?.maximumDate = Date()
        self.view.addSubview(datePicker!)
        datePicker?.isHidden = true
        
        datePicker?.locale = Locale.init(identifier: Lang_Locale)
        
        //Done Button
        doneButton = UIButton.init(frame: CGRect(x: -30, y: self.view.frame.height-240, width: (datePicker?.frame.width)!+30, height: 40))
        doneButton.titleLabel?.textAlignment = .center
        doneButton.setTitleColor(UIColor.white, for: .normal)
        doneButton.backgroundColor = ColorCode.FMeetDarkTheme
        doneButton.titleLabel?.font = UIFont.init(name: "ProximaNova-Regular", size: 15)
        doneButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "common_done_ios"), for: .normal)
        
        doneButton.addTarget(self, action: #selector(donedatePicker(_:)), for: .touchUpInside)
        self.view.addSubview(doneButton)
        doneButton.isHidden = true
        
        let topBorder = CALayer()
        topBorder.backgroundColor = UIColor.lightGray.cgColor
        topBorder.frame = CGRect(x:0, y:0, width:doneButton.frame.width, height:0.5)
        doneButton.layer.addSublayer(topBorder)
    }
    
    @objc func donedatePicker(_ sender : UIButton)
    {
        datePicker?.isHidden = true
        sender.isHidden = true
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dobTF.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "dob")) :- \(formatter.string(from: (datePicker?.date)!))"
        dob_str = formatter.string(from: (datePicker?.date)!).toDateString(inputFormat: "dd/MM/yyyy", outputFormat: "yyyy-MM-dd")!
        
        let components = Set<Calendar.Component>([.year])
        let ageComponents = Calendar.current.dateComponents(components, from: (datePicker?.date)!, to: Date())
        age_str = ageComponents.year!
    }
    
    // MARK : Location
    func getLocationData()
    {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
     //   print("locations = \(locValue.latitude) \(locValue.longitude)")
        locLattitude = locValue.latitude
        locLongitude = locValue.longitude
        locationManager.stopUpdatingLocation()
    }
}

// MARK : table view methods
extension FMeetProfileVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5 + interestArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.section == 0)
        {
            let cell = profileTableVw.dequeueReusableCell(withIdentifier: "fmeetprof1") as! FMeetProfFirstCell
            
            cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: section_titles[indexPath.section])
            
            if (valueDict["iam"] as! String == "0") {
                cell.btnCollection[0].backgroundColor = ColorCode.FMeetDarkTheme
            }else if (valueDict["iam"] as! String == "1") {
                cell.btnCollection[1].backgroundColor = ColorCode.FMeetDarkTheme
            }else {
                cell.btnCollection[0].backgroundColor = UIColor.white
                cell.btnCollection[1].backgroundColor = UIColor.white
            }
            
            for btn in cell.btnCollection
            {
                btn.addTarget(self, action: #selector(iam(_:)), for: .touchUpInside)
            }
            
            return cell
        }
        else if (indexPath.section == 1)
        {
            let cell = profileTableVw.dequeueReusableCell(withIdentifier: "fmeetprof2") as! FMeetProfSecondCell
            
            cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: section_titles[indexPath.section])
            
            if (valueDict["looking"] as! String == "0") {
                cell.btnCollection[0].backgroundColor = ColorCode.FMeetDarkTheme
            }else if (valueDict["looking"] as! String == "1") {
                cell.btnCollection[1].backgroundColor = ColorCode.FMeetDarkTheme
            }else if (valueDict["looking"] as! String == "2") {
                cell.btnCollection[2].backgroundColor = ColorCode.FMeetDarkTheme
            }else {
                cell.btnCollection[0].backgroundColor = UIColor.white
                cell.btnCollection[1].backgroundColor = UIColor.white
                cell.btnCollection[2].backgroundColor = UIColor.white
            }
            
            for btn in cell.btnCollection
            {
                btn.addTarget(self, action: #selector(looikgFor(_:)), for: .touchUpInside)
            }
            
            return cell
        }
        else if (indexPath.section == 2) {
            let cell = profileTableVw.dequeueReusableCell(withIdentifier: "fmeetprof3") as! FMeetProfThirdCell
            
            cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: section_titles[indexPath.section])
            
            for btn in cell.btnCollection
            {
                let dict = intentionArray[btn.tag]
                cell.LblCollection[btn.tag].text = dict["name"] as? String
                
                if (valueDict["intentions"] as! String == "\(dict["id"] as! NSNumber)") {
                    btn.backgroundColor = ColorCode.FMeetDarkTheme
                } else {
                    btn.backgroundColor = UIColor.white
                }
                
                btn.addTarget(self, action: #selector(intentions(_:)), for: .touchUpInside)
            }
            
            return cell
        }
        else if (indexPath.section == 3) {
            let cell = profileTableVw.dequeueReusableCell(withIdentifier: "fmeetprof4") as! FMeetProfFourthCell
            
            cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: section_titles[indexPath.section])
            
            for btn in cell.btnCollection
            {
                cell.LblCollection[btn.tag].text = studiesArray[btn.tag]
                
                if (valueDict["studies"] as! String == "\(btn.tag)") {
                    btn.backgroundColor = ColorCode.FMeetDarkTheme
                } else {
                    btn.backgroundColor = UIColor.white
                }
                
                btn.addTarget(self, action: #selector(studiesLevel(_:)), for: .touchUpInside)
            }
            
            return cell
        }
        else if (indexPath.section == 4) {
            let cell = profileTableVw.dequeueReusableCell(withIdentifier: "fmeetprof5") as! FMeetProfFifthCell
            
            cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: section_titles[indexPath.section])
            
            return cell
        }
        else {
            let cell = profileTableVw.dequeueReusableCell(withIdentifier: "fmeetprof6") as! FMeetProfSixthCell
            
            let dict = interestArray[indexPath.section-5]
            cell.textLbl.text = dict["name"] as? String
            
            if (indexPath.section == (5+interestArray.count-1))
            {
                cell.btmConstraint.constant = 2
                if #available(iOS 11.0, *) {
                    cell.bgView.layer.cornerRadius = 12
                    cell.bgView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                } else {
                    // Fallback on earlier versions
                    cell.bgView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 12, szz: cell.bgView!.frame.size)
                }
            }else {
                cell.btmConstraint.constant = -5
            }
            
            if (idArray.count > 0) && (idArray.contains("\(dict["id"] as! NSNumber)")) {
                cell.checkButton.backgroundColor = ColorCode.FMeetDarkTheme
            } else {
                cell.checkButton.backgroundColor = UIColor.white
            }
            
            cell.checkButton.addTarget(self, action: #selector(interestedOn(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section <= 5)
        {
          return heightArray[indexPath.section]
        }
        return 32
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: profileTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (section <= 3)
        {
            return 10
        }
        return 0
    }
    
    @objc func iam(_ sender : UIButton)
    {
        setBtnColor(button: sender, check: "first")
        
        if (sender.tag == 0){
            valueDict.setValue("0", forKey: "iam")
        }else {
            valueDict.setValue("1", forKey: "iam")
        }
    }
    
    @objc func looikgFor(_ sender : UIButton)
    {
        setBtnColor(button: sender, check: "second")
        
        if (sender.tag == 0){
            valueDict.setValue("0", forKey: "looking")
        }else if (sender.tag == 1) {
            valueDict.setValue("1", forKey: "looking")
        }else {
            valueDict.setValue("2", forKey: "looking")
        }
    }
    
    @objc func intentions(_ sender : UIButton)
    {
        setBtnColor(button: sender, check: "third")
        let dict = intentionArray[sender.tag]
        
        valueDict.setValue("\(dict["id"] as! NSNumber)", forKey: "intentions")
    }
    
    @objc func studiesLevel(_ sender : UIButton)
    {
        setBtnColor(button: sender, check: "fourth")
        
        valueDict.setValue("\(sender.tag)", forKey: "studies")
    }
    
    @objc func interestedOn(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: profileTableVw)
        let idxpath = profileTableVw.indexPathForRow(at: point)!
        let dict = interestArray[idxpath.section-5]
        
        if (sender.backgroundColor == ColorCode.FMeetDarkTheme)
        {
            sender.backgroundColor = UIColor.white
            if (idArray.contains("\(dict["id"] as! NSNumber)"))
            {
                idArray.remove("\(dict["id"] as! NSNumber)")
            }
        }
        else {
            sender.backgroundColor = ColorCode.FMeetDarkTheme
            idArray.add("\(dict["id"] as! NSNumber)")
        }
        var str = ""
        for ii in idArray
        {
            if (str == "")
            {
                str = "\(ii)"
            } else {
                str = "\(str),\(ii)"
            }
        }
        valueDict.setValue(str, forKey: "interest")
    }
    
    // common function
    func setBtnColor(button : UIButton, check : String)
    {
        let point = button.convert(CGPoint.zero, to: profileTableVw)
        let idxpath = profileTableVw.indexPathForRow(at: point)!
        
        var btnarr : [UIButton] = []
        
        if (check == "first")
        {
            let cell = profileTableVw.cellForRow(at: idxpath) as! FMeetProfFirstCell
            btnarr = cell.btnCollection
        }
        else if (check == "second")
        {
            let cell = profileTableVw.cellForRow(at: idxpath) as! FMeetProfSecondCell
            btnarr = cell.btnCollection
        }
        else if (check == "third")
        {
            let cell = profileTableVw.cellForRow(at: idxpath) as! FMeetProfThirdCell
            btnarr = cell.btnCollection
        }
        else
        {
            let cell = profileTableVw.cellForRow(at: idxpath) as! FMeetProfFourthCell
            btnarr = cell.btnCollection
        }
        
        for btn in btnarr
        {
            if (btn.tag == button.tag) {
                btn.backgroundColor = ColorCode.FMeetDarkTheme
            } else {
                btn.backgroundColor = UIColor.white
            }
        }
    }
}

class FMeetProfFirstCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var girlLabel : UILabel!
    @IBOutlet weak var boyLabel : UILabel!
    
    @IBOutlet var btnCollection : [UIButton]!
}

class FMeetProfSecondCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var girlLabel : UILabel!
    @IBOutlet weak var boyLabel : UILabel!
    @IBOutlet weak var bothLabel : UILabel!
    
    @IBOutlet var btnCollection : [UIButton]!
}

class FMeetProfThirdCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet var LblCollection : [UILabel]!
    
    @IBOutlet var btnCollection : [UIButton]!
}

class FMeetProfFourthCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet var LblCollection : [UILabel]!
    
    @IBOutlet var btnCollection : [UIButton]!
}

class FMeetProfFifthCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
}

class FMeetProfSixthCell: UITableViewCell {
    
    @IBOutlet weak var textLbl : UILabel!
    @IBOutlet weak var checkButton : UIButton!
    @IBOutlet weak var bgView : CustomUIView!
    
    @IBOutlet weak var topConstraint : NSLayoutConstraint!
    @IBOutlet weak var btmConstraint : NSLayoutConstraint!
}
