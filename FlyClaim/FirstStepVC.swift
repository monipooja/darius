//
//  FristStepVC.swift
//  Darious
//
//  Created by Dario Carrasco on 17/06/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FirstStepVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet var contentVw: UIView!
    @IBOutlet var contentVwHt: NSLayoutConstraint!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    // First View Outlets
    @IBOutlet var firstView: UIView!
    @IBOutlet var achieveTF: KGHighLightedField!
    @IBOutlet var signatureTF: KGHighLightedField!
    @IBOutlet var petitionTitleLbl: UILabel!
    @IBOutlet var petitionMsgLbl: UILabel!
    @IBOutlet var signatureLbl: UILabel!
    @IBOutlet var firstContinue: KGHighLightedButton!
    
    var contentFinalHt : CGFloat = 0
    var cc : CGFloat = 40
    var word_count : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        contentFinalHt = contentVwHt.constant
        setLanguage()
        setBottomView()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        gesture.delegate = self
        contentVw.addGestureRecognizer(gesture)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        contentVwHt.constant = contentFinalHt
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        petitionTitleLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "write_your_petition_title")
        petitionMsgLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "write_petition_title_sub_text")
        signatureLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "number_of_signature_to_achieve")
        achieveTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "what_do_you_want_achieve") + "?"
        signatureTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "how_many_signatures_do_you_want") + "?"
        
        firstContinue.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
    }
    
    // Mark : Gesture Action
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        contentVw.endEditing(true)
        contentVwHt.constant = contentFinalHt
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    @IBAction func onFirstContinue(_ sender: Any)
    {
        if (achieveTF.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_petition_title"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (signatureTF.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_petition_signatures"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            createDict.setValue("\(achieveTF.text!)", forKey: "title")
            createDict.setValue("\(signatureTF.text!)", forKey: "signature")
            let secondvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "SecondStepVC") as! SecondStepVC
            present(secondvc, animated: true, completion: nil)
        }
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        createDict.setValue("", forKey: "title")
        createDict.setValue("", forKey: "signature")
        self.dismiss(animated: false, completion: nil)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FirstStepVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        contentVwHt.constant = contentFinalHt
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        contentVwHt.constant = contentFinalHt + 100
    }
}
