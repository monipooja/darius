//
//  ShareViewController.swift
//  Darious
//
//  Created by Dario Carrasco on 27/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import FBSDKShareKit
import NetworkExtension
import Social
import Firebase

class ShareViewController: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    // Upper View Outlets
    @IBOutlet weak var shareOneBtn: KGHighLightedButton!
    @IBOutlet weak var sharetwoBtn: KGHighLightedButton!
    @IBOutlet weak var topLabel: UILabel!
    
    @IBOutlet weak var topCustomVw: CustomUIView!
    @IBOutlet weak var topLabel2: UILabel!
    
    // Middle View Outlets
    @IBOutlet weak var contributeLabel: UILabel!
    
    @IBOutlet weak var peopleTableView: UITableView!
    @IBOutlet weak var tableHtConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentvWHtConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentVw: UIView!
    
    var docController:UIDocumentInteractionController!
    
    //var listArray : [NSDictionary] = []
    var sharePetitionId : NSNumber = 0
    var descriptionStr : String = ""
    var youtubeUrl : String = ""
    var imageUrl : UIImage?
    var imageUrlStr : String = ""
    var comeFrom : String = ""
    var sendTextData : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FCBlackColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        if (comeFrom == "sign") {
            topCustomVw.isHidden = false
        }
        
        //for rounded corners
        peopleTableView.layer.cornerRadius = 10
        peopleTableView.layer.masksToBounds = true
        
        setLanguage()
        setBottomView()
        setData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
        
        socket.on("petition_message_emit") { (dataArray, socketAck) in
            
            self.setData()
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "time_to_share_and_help")
        topLabel2.text = ApiResponse.getLanguageFromUserDefaults(inputString: "time_to_share_and_help")
        shareOneBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_share_link"), for: .normal)
        sharetwoBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_share_link"), for: .normal)
    }
    
    func setData()
    {
        if (listArray.count == 0)
        {
            self.contentvWHtConstraint.constant = self.contentvWHtConstraint.constant - self.tableHtConstraint.constant
            self.tableHtConstraint.constant = 0
            self.contributeLabel.text = "0 \(ApiResponse.getLanguageFromUserDefaults(inputString: "people_have_already_made_contribution"))"
        }
        else {
            self.contributeLabel.text = "\(listArray.count) \(ApiResponse.getLanguageFromUserDefaults(inputString: "people_have_already_made_contribution"))"
            self.peopleTableView.rowHeight = UITableView.automaticDimension
            self.peopleTableView.estimatedRowHeight = 44
            self.peopleTableView.reloadData()
        }
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    @IBAction func shareOnWhatsapp(_ sender: Any)
    {
        // \(Api.Base_URL)sharePetition?petitionId=\(sharePetitionId)
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(sharePetitionId)&app_name=flyclaim") else { return }
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flyclaim")
        
        if(imageUrl != nil) {
            linkBuilder!.socialMetaTagParameters?.descriptionText = "\n"
            linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"\(imageUrlStr)")
        }
        else {
            linkBuilder!.socialMetaTagParameters?.descriptionText = "\(youtubeUrl)"
            linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        }
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
           
                let data_json : NSDictionary = ["petition_id" : "\(self.sharePetitionId)", "uid" : user_id]
                socket.emit("share_this_petition", data_json)
                
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flyclaim")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    func saveImageDocumentDirectory(){
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("apple.png")
        let imageData = imageUrl!.pngData()
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }
    
    func getImage() -> String {
        let fileManager = FileManager.default
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let imagePAth = (documentsDirectory as NSString).appendingPathComponent("apple.png")
        if fileManager.fileExists(atPath: imagePAth){
            //self.imageView.image = UIImage(contentsOfFile: imagePAth)
            return imagePAth as String
        }else{
            print("No Image")
            return ""
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
}

extension ShareViewController : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = peopleTableView.dequeueReusableCell(withIdentifier: "commentcell") as! CommentTableCell
        
        let dict = listArray[indexPath.row]
        
        cell.userImage.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        cell.userImage.layer.cornerRadius = cell.userImage.frame.width/2
        cell.userImage.layer.masksToBounds = true
        
        let days = retreiveDaysInterval(passDateStr: dict["created_on"] as! String)
        let dayStr = ApiResponse.getLanguageFromUserDefaults(inputString: "days_ago").replacingOccurrences(of: "[day]", with: days)
        if (ApiResponse.getLanguageFromUserDefaults(inputString: dict["message"] as! String) != "") {
            cell.msgLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: dict["message"] as! String)) \(dayStr)"
        }
        else {
            cell.msgLabel.text = "\(dict["message"] as! String) \(dayStr)"
        }
        cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        
        return cell
    }
    
    func retreiveDaysInterval(passDateStr : String) -> String
    {
        let datestr = passDateStr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "yyyy-MM-dd")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let formatedStartDate = dateFormatter.date(from: datestr!)
        
        let currentDate = Date()
        let components = Set<Calendar.Component>([.day])
        let differenceOfDate = Calendar.current.dateComponents(components, from: formatedStartDate!, to: currentDate)
        
        return "\(differenceOfDate.day!)"
    }
}

class CommentTableCell : UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var msgLabel: UILabel!
}

extension ShareViewController {
    /// This function will set all the required properties, and then provide a preview for the document
    func share(url: URL) {
        docController.url = url
        docController.uti = "net.whatsapp.image"
        //url.typeIdentifier ?? "public.data, public.content"
        docController.name = url.localizedName ?? url.lastPathComponent
        docController.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
    }
    
    /// This function will store your document to some temporary URL and then provide sharing, copying, printing, saving options to the user
    func storeAndShare(withURLString: String) {
        guard let url = URL(string: withURLString) else { return }
        /// START YOUR ACTIVITY INDICATOR HERE
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else { return }
            let tmpURL = FileManager.default.temporaryDirectory
                .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
            do {
                try data.write(to: tmpURL)
            } catch {
                print(error)
            }
            DispatchQueue.main.async {
                /// STOP YOUR ACTIVITY INDICATOR HERE
                //  print("tmp url  = \(tmpURL)")
                self.share(url: tmpURL)
            }
            }.resume()
    }
}

extension ShareViewController: UIDocumentInteractionControllerDelegate {
    /// If presenting atop a navigation stack, provide the navigation controller in order to animate in a manner consistent with the rest of the platform
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}

extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}
