//
//  FourthStepVC.swift
//  Darious
//
//  Created by Dario Carrasco on 21/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import Photos

class FourthStepVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet var contentVw: UIView!
    @IBOutlet var contentVwHt: NSLayoutConstraint!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet var uploadImageVw: UIImageView!
    @IBOutlet var deleteImgBtn: UIButton!
    @IBOutlet var youtubLinkTFF: UITextField!
    @IBOutlet var addphotoLbl: UILabel!
    @IBOutlet var msgLabel: UILabel!
    @IBOutlet var uploadLabel: UILabel!
    @IBOutlet var orLabel: UILabel!
    
    @IBOutlet var continueBtn: KGHighLightedButton!
    
    var imgPick = UIImagePickerController()
    
    var photoString : String = ""
    var videoString : String = ""
    var asset : PHAsset!
    
    var contentFinalHt : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        deleteImgBtn.layer.cornerRadius = deleteImgBtn.frame.width/2
        addBottomLayerWithColor(bgcolor: UIColor(hexString: ColorCode.themeColor), textF: youtubLinkTFF)
        
        contentFinalHt = contentVwHt.constant
        setLanguage()
        setBottomView()
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        gesture.delegate = self
        contentVw.addGestureRecognizer(gesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        addphotoLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "add_a_photo_or_link")
        msgLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "add_a_photo_or_link_sub_text")
        uploadLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "upload_picture")
        //orLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "")
        youtubLinkTFF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "enter_youtube_link_here")
        continueBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        contentVw.endEditing(true)
        contentVwHt.constant = contentFinalHt
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    @IBAction func openGalleryToUploadPic(_ sender: Any)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)
        {
            imgPick.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imgPick.delegate = self
            imgPick.allowsEditing = false
            self.present(imgPick, animated: true, completion: nil)
        }
    }
    
    // Image picker delegate
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let image = info[.originalImage] as! UIImage
        uploadImageVw.image = image
        deleteImgBtn.isHidden = false
        if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            asset = result.firstObject
        }
        if(asset==nil){
            photoString="FirstImg.png"
        }
        else{
            let fname = (asset.value(forKey: "filename"))!
            let words = (fname as! String).components(separatedBy: ".")
            let fnm=words[0]
            photoString = "\(fnm).png"
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteSelectImage(_ sender: Any)
    {
        photoString = ""
        uploadImageVw.sd_setImage(with: nil, placeholderImage: #imageLiteral(resourceName: "UploadImage"))
        deleteImgBtn.isHidden = true
    }
    
    @IBAction func onContinue(_ sender: Any)
    {
        youtubLinkTFF.resignFirstResponder()
        if (photoString == "") && (youtubLinkTFF.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_choose_image_or_url"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (youtubLinkTFF.text != "") && (validateYoutubeLink(youtubLinkTFF.text!) == false)
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_correct_url"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
            {
                customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
                savePetiton()
            }
            else {
                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
                ApiResponse.alert(title: "", message: showMsg, controller: self)
            }
        }
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // Mark : Check youtube valid URL
    func validateYoutubeLink(_ urlStr : String) -> Bool
    {
        let a = "(http(s)?:\\/\\/)?(www\\.|m\\.)?youtu(be\\.com|\\.be)(\\/watch\\?([&=a-z]{0,})(v=[\\d\\w]{1,}).+|\\/[\\d\\w]{1,})"
        let urlTest = NSPredicate(format: "SELF MATCHES %@", a)
        return urlTest.evaluate(with: urlStr)
    }
    
    // Mark : Add Bottom Layer
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:1)
        textF.layer.addSublayer(border)
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
                
                customLoader.hideIndicator()
                let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
                self.present(homevc, animated: false, completion: nil)
            }
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    /// MARK : Multipart (file format)
    func savePetiton(){
       
        let url = NSURL(string: Api.Save_Petition_URL)
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var body = Data()
        
        let mimetype = "image/png"
        let parameters : [String : String] = ["access_token":"\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "petition_title":"\(createDict["title"] as! String)", "number_of_signatures":"\(createDict["signature"] as! String)", "choose_a_decision_maker":"\(createDict["decision"] as! String)", "explain_the_problem":"\(createDict["problem"] as! String)", "link" : "\(youtubLinkTFF.text!)"]
        
        for (key, value) in parameters
        {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        let image_data = uploadImageVw.image!.jpegData(compressionQuality: 0.2)
        if(image_data == nil)
        {
            return
        }
        //define the data post parameter
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"photo_or_video\"; filename=\"\(photoString)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(image_data!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.httpBody = body as Data
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
        
            DispatchQueue.main.async
            {
                    guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                         ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "time_out"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "try_again"), controller: self)
                        return
                    }
                _ = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    
                    let dd = try! JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    
                    let status = dd["status"] as! NSNumber
                    
                    if (status == 1) {
                        customLoader.hideIndicator()
                        StaticFunctions.showAlert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fc_create_petition_msg"), actions: [OkText], controller: self, completion: { (str) in
                            
                            let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
                            self.present(homevc, animated: false, completion: nil)
                        })
                    }
                    else if(status == 3){
                        customLoader.hideIndicator()
                        Access_token = ""
                        userDefault.set(Access_token, forKey: "token")
                        userDefault.synchronize()
                        let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.present(VC, animated: true, completion: nil)
                    }
                    else {
                        if let str = dd["message"] {
                            let msgStr = str as! String
                            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                            if(showMsg != "") {
                                ApiResponse.alert(title: "", message: showMsg, controller: self)
                            }
                            else {
                                ApiResponse.alert(title: "", message: msgStr, controller: self)
                            }
                        }
                        else {
                            customLoader.hideIndicator()
                        }
                }
            }
        }
        task.resume()
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
}

extension FourthStepVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        contentVwHt.constant = contentFinalHt
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        contentVwHt.constant = contentFinalHt + 150
        contentVw.accessibilityScroll(.up)
    }
}
