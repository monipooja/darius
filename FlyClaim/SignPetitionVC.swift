//
//  SignPetitionVC.swift
//  Darious
//
//  Created by Dario Carrasco on 31/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class SignPetitionVC: UIViewController {
    
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var contentVw: UIView!
    
    // Upper View Outlets
    @IBOutlet weak var middleLabel: UILabel!
    @IBOutlet weak var donateBtn: KGHighLightedButton!
    @IBOutlet weak var shareBtn: KGHighLightedButton!
    @IBOutlet weak var topLabel: UILabel!
    
    // Middle View Outlets
    @IBOutlet weak var contributeLabel: UILabel!
    
    @IBOutlet weak var peopleTableView: UITableView!
    @IBOutlet weak var tableHtConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentvWHtConstraint: NSLayoutConstraint!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    
    //var listArray : [NSDictionary] = []
    var signPetitionId : NSNumber = 0
    var visibleValue : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        //for rounded corners
        peopleTableView.layer.cornerRadius = 10
        peopleTableView.layer.masksToBounds = true
        
        setLanguage()
        setBottomView()
        setData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
        
        socket.on("petition_message_emit") { (dataArray, socketAck) in
            
            self.setData()
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "can_you_contribute").replacingOccurrences(of: "€", with: Current_Currency)
        middleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "can_you_contribute_sub_text")
        donateBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "yes_i_will_help_with_more").replacingOccurrences(of: "€", with: Current_Currency), for: .normal)
        shareBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_i_will_share"), for: .normal)
    }
    
    func setData()
    {
        if (listArray.count == 0)
        {
            self.contentvWHtConstraint.constant = self.contentvWHtConstraint.constant - self.tableHtConstraint.constant
            self.tableHtConstraint.constant = 0
            self.contributeLabel.text = "0 \(ApiResponse.getLanguageFromUserDefaults(inputString: "people_have_already_made_contribution"))"
        }
        else {
            self.contributeLabel.text = "\(listArray.count) \(ApiResponse.getLanguageFromUserDefaults(inputString: "people_have_already_made_contribution"))"
            self.peopleTableView.rowHeight = UITableView.automaticDimension
            self.peopleTableView.estimatedRowHeight = 44
            self.peopleTableView.reloadData()
        }
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    @IBAction func goToDonate(_ sender: Any)
    {
        DispatchQueue.main.async {
            if let link = URL(string: "\(Api.Donate_URL)?uid=\(user_id)&petition_id=\(self.signPetitionId)&type=flyclaim&visible=\(self.visibleValue)") {
                UIApplication.shared.open(link)
            }
        }
    }
    
    @IBAction func goToShare(_ sender: Any)
    {
        let sharevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
        sharevc.sharePetitionId = self.signPetitionId
        sharevc.comeFrom = "sign"
        present(sharevc, animated: false, completion: nil)
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
}

extension SignPetitionVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = peopleTableView.dequeueReusableCell(withIdentifier: "signcell") as! SignTableCell
        
        let dict = listArray[indexPath.row]
        
        cell.userImage.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        cell.userImage.layer.cornerRadius = cell.userImage.frame.width/2
        cell.userImage.layer.masksToBounds = true
        
        let days = retreiveDaysInterval(passDateStr: dict["created_on"] as! String)
        let dayStr = ApiResponse.getLanguageFromUserDefaults(inputString: "days_ago").replacingOccurrences(of: "[days]", with: days)
        if (ApiResponse.getLanguageFromUserDefaults(inputString: dict["message"] as! String) != "") {
            cell.msgLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: dict["message"] as! String)) \(dayStr)"
        }
        else {
            cell.msgLabel.text = "\(dict["message"] as! String) \(dayStr)"
        }
        
        cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        
        return cell
    }
    
    func retreiveDaysInterval(passDateStr : String) -> String
    {
        let datestr = passDateStr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "yyyy-MM-dd")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let formatedStartDate = dateFormatter.date(from: datestr!)
        
        let currentDate = Date()
        let components = Set<Calendar.Component>([.day])
        let differenceOfDate = Calendar.current.dateComponents(components, from: formatedStartDate!, to: currentDate)
        
        return "\(differenceOfDate.day!)"
    }
}

class SignTableCell : UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var msgLabel: UILabel!
}
