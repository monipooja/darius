//
//  DonateViewController.swift
//  Darious
//
//  Created by Dario Carrasco on 27/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class DonateViewController: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var contentVw: UIView!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    // Upper View Outlets
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var donateBtn: KGHighLightedButton!
    @IBOutlet var priceBtnCollection: [KGHighLightedButton]!
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var imgVwHtConstraint: NSLayoutConstraint!
    @IBOutlet weak var topVwHtConstraint: NSLayoutConstraint!
    
    // Middle View Outlets
    @IBOutlet weak var contributeLabel: UILabel!
    
    @IBOutlet weak var peopleTableView: UITableView!
    @IBOutlet weak var tableHtConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentvWHtConstraint: NSLayoutConstraint!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    
    /// Payment View
    @IBOutlet weak var paymentView: CustomUIView!
    @IBOutlet weak var walletBtn: UIButton!
    @IBOutlet weak var creditBtn: UIButton!
    @IBOutlet weak var selectLabel: UILabel!
    
    /// Center Views
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var centerSuccessVw: CustomUIView!
    @IBOutlet weak var successLabel: UILabel! // thank_for_donate
    @IBOutlet weak var centerErrorVw: CustomUIView!
    @IBOutlet weak var failedLabel: UILabel!
    
    @IBOutlet weak var centerView: CustomUIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var commissionLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var subtotalValue: UILabel!
    @IBOutlet weak var commissionValue: UILabel!
    @IBOutlet weak var totalValue: UILabel!
    @IBOutlet weak var continueBtn: KGHighLightedButton!
    
 //   var listArray : [NSDictionary] = []
    var passPetitionId : NSNumber = 0
    var comeFrom : String = ""
    var transactionId : String = ""
    var enterAmount : Float = 0
    var total_comsn : Float = 0
    var amountStr : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in priceBtnCollection
        {
            btn.addTarget(self, action: #selector(selectPriceToDonate(_:)), for: .touchUpInside)
        }
        
        if (comeFrom == "details") {
            topVwHtConstraint.constant = topVwHtConstraint.constant - imgVwHtConstraint.constant
            imgVwHtConstraint.constant = 0
            imgVw.isHidden = true
        }
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        contentVw.addGestureRecognizer(gesture)
        let gesture1 = UITapGestureRecognizer(target: self, action:  #selector(hideSuccessView(sender:)))
        gesture1.numberOfTapsRequired = 1
        blurView.addGestureRecognizer(gesture1)
            
        addBottomLayer(ht: 0.6, color: UIColor.lightGray)
        
        //for rounded corners
        peopleTableView.layer.cornerRadius = 10
        peopleTableView.layer.masksToBounds = true
        
        setLanguage()
        setBottomView()
        setData()
        
        if let _ = userDefault.value(forKey: "popup") as? Bool
        {
            self.setViews()
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "you_are_contributing")
        amount.text = ApiResponse.getLanguageFromUserDefaults(inputString: "amount")
        donateBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "donate")), for: .normal)
        
        titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "card_method")
        subtotalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "subtotal")
        totalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "total")
        commissionLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "commission")
        successLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "thank_for_donate")
        failedLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "payment_not_done")
        selectLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "choose_payment_method")
        
        walletBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "darius_wallet_heading"), for: .normal)
        creditBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "card_method"), for: .normal)
        continueBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    
        socket.on("petition_message_emit") { (dataArray, socketAck) in
        
            self.setData()
            self.setViews()
        }
    }
    
    // Mark : Gesture Action on Content view
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        self.view.endEditing(true)
    }
    
    // Mark : Gesture Action On Blur view
    @objc func hideSuccessView(sender : UITapGestureRecognizer) {
        
        blurView.isHidden = true
        blurView.alpha = 1
        if (centerSuccessVw.isHidden == false) {
            centerSuccessVw.isHidden = true
        }
        if (centerErrorVw.isHidden == false) {
            centerErrorVw.isHidden = true
        }
        if (paymentView.isHidden == false) {
            paymentView.isHidden = true
        }
        if (centerView.isHidden == false) {
            centerView.isHidden = true
        }
    }
    
    func setViews()
    {
        if let _ = userDefault.value(forKey: "popup") as? Bool
        {
            if (userDefault.value(forKey: "popup") as! Bool == true)
            {
                self.successLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "thank_for_donate")) \n\(self.amountStr) €"
                
                self.blurView.isHidden = false
                self.centerSuccessVw.isHidden = false
            }
            else {
                self.blurView.isHidden = false
                self.centerErrorVw.isHidden = false
            }
            userDefault.set(nil, forKey: "popup")
        }
    }
    
    func setData()
    {
        if (listArray.count == 0)
        {
            self.contentvWHtConstraint.constant = self.contentvWHtConstraint.constant - self.tableHtConstraint.constant
            self.tableHtConstraint.constant = 0
            self.contributeLabel.text = "0 \(ApiResponse.getLanguageFromUserDefaults(inputString: "people_have_already_made_contribution"))"
        }
        else {
            self.contributeLabel.text = "\(listArray.count) \(ApiResponse.getLanguageFromUserDefaults(inputString: "people_have_already_made_contribution"))"
            self.peopleTableView.rowHeight = UITableView.automaticDimension
            self.peopleTableView.estimatedRowHeight = 44
            self.peopleTableView.reloadData()
        }
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    @IBAction func onDonate(_ sender: Any)
    {
        amountTF.resignFirstResponder()
        
        if (amountTF.text == "") {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_fill_the_amount"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            if (self.amountTF.text!.contains(",")) {
                amountStr = (self.amountTF.text?.replacingOccurrences(of: ",", with: "."))!
            }
            else {
                amountStr = "\(self.amountTF.text!)"
            }
            total_comsn = ((Float(amountStr)!) * (Float(commission_value)!/100)) + Float(commission_Card)!
            enterAmount = Float(amountStr)! + total_comsn
            enterAmount = Float(String(format: "%.2f", enterAmount))!
        
            if (Int(truncating: wallet_amount) < 1) {
                let aa = "\(enterAmount)".split(separator: ".")
                
                if (aa[1].count == 1) {
                    self.molliePayment(payment: "\(enterAmount)0", cid: "")
                }
                else {
                    self.molliePayment(payment: "\(enterAmount)", cid: "")
                }
            }
            else {
                paymentView.isHidden = false
                blurView.isHidden = false
                blurView.alpha = 0.6
            }
        }
    }
    
    @IBAction func onHideSuccessView(_ sender: Any)
    {
        blurView.isHidden = true
        centerSuccessVw.isHidden = true
    }
    
    @IBAction func onHideErrorView(_ sender: Any)
    {
        blurView.isHidden = true
        centerErrorVw.isHidden = true
    }
    
    @IBAction func onHidePaymentView(_ sender: Any)
    {
        blurView.isHidden = true
        blurView.alpha = 1
        paymentView.isHidden = true
    }
    
    @IBAction func onContinue(_ sender: Any)
    {
        let aa = "\(enterAmount)".split(separator: ".")

        if (aa[1].count == 1) {
            self.molliePayment(payment: "\(enterAmount)0", cid: "")
        }
        else {
            self.molliePayment(payment: "\(enterAmount)", cid: "")
        }
    }
    
    @IBAction func onhideCenterView(_ sender: Any)
    {
        //blurView.isHidden = true
        centerView.isHidden = true
    }
    
    @IBAction func onSelectWallet(_ sender: Any)
    {
        if (enterAmount > Float(truncating: wallet_amount))
        {
            let titlStr = ApiResponse.getLanguageFromUserDefaults(inputString: "invalid_wallet_amount")
            StaticFunctions.showAlert(title: titlStr, message: ApiResponse.getLanguageFromUserDefaults(inputString: "fc_continue_with_card"), actions: [YesText, NoText], controller: self) { (str) in
                if (str == YesText)
                {
                    self.blurView.isHidden = false
                    self.centerView.isHidden = false
                    
                    if (self.amountTF.text!.contains(".") == false)
                    {
                        self.subtotalValue.text = "\(StaticFunctions.priceSetWithoutDecimal(prcValue: self.amountTF.text!)) \(Current_Currency)"
                    }
                    else {
                        self.subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: self.amountTF.text!) + " \(Current_Currency)"
                    }
                    self.commissionValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", self.total_comsn)) + " \(Current_Currency)"
                    self.totalValue.text = StaticFunctions.priceFormatSet(prcValue: "\(self.enterAmount)") + " \(Current_Currency)"
                }
            }
        }
        else {
            /// Wallet api call
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&pid=\(passPetitionId)&amount=\(amountStr)"
            callService(urlstr: Api.Wallet_Donate_URL, parameters: params, check: "")
        }
    }
    
    @IBAction func onSelectCard(_ sender: Any)
    {
        blurView.isHidden = false
        centerView.isHidden = false
        
        if (amountTF.text!.contains(".") == false) && (amountTF.text!.contains(",") == false)
        {
            subtotalValue.text = "\(amountTF.text!).0 \(Current_Currency)"
        }
        else {
            if (amountTF.text!.contains(".") == true)
            {
                let sp = amountTF.text?.split(separator: ".")
                if (sp?.count == 1)
                {
                    subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", amount.text!)) + "0 \(Current_Currency)"
                }
                else {
                    subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", amount.text!)) + " \(Current_Currency)"
                }
            }
            else {
                let sp = amountTF.text?.split(separator: ",")
                if (sp?.count == 1)
                {
                    subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", amount.text!)) + "0 \(Current_Currency)"
                }
                else {
                    subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", amount.text!)) + " \(Current_Currency)"
                }
            }
        }
        
        commissionValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", String(format: "%.2f", total_comsn))) + " \(Current_Currency)"
        totalValue.text = StaticFunctions.priceFormatSet(prcValue: "\(enterAmount)") + " \(Current_Currency)"
    }
    
    // Mollie Payment Service Call
    func molliePayment(payment : String, cid : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        
        let amountDict : NSDictionary = ["currency" : "EUR", "value" : payment]
        let date = Date()
        let formatter = DateFormatter.init()
        formatter.dateFormat = "yyyyMMdd"
        let datestr = formatter.string(from: date)
        let metaDict : NSDictionary = ["uinique_id" : "\(datestr)\(user_id)"]
        
        let params : [String : Any] = ["metadata" : metaDict, "amount" : amountDict, "description" : "\(ApiResponse.getLanguageFromUserDefaults(inputString: "subscription_business"))", "customerId" : "\(cid)", "redirectUrl" : "donation://PaymentDone", "webhookUrl" : "\(Api.Base_URL)worker/webhook_for_mobile/\(user_id)"]
        // mollie-darius://payment-return
    
        ApiResponse.onResponsePostMollie2(url: "https://api.mollie.com/v2/payments", parms: params) { (result, error) in
            // print("mollie payment result = \(result)")
            
            OperationQueue.main.addOperation {
                if let _ = result["_links"] as? NSDictionary {
                    
                    let checkoutUrl = ((result["_links"] as! NSDictionary)["checkout"] as! NSDictionary)["href"] as! String
                  //  let callbackUrl = ((result["_links"] as! NSDictionary)["self"] as! NSDictionary)["href"] as! String
                    self.transactionId = result["id"] as! String
                    
                    DispatchQueue.main.async {
                        if let link = URL(string: checkoutUrl) {
                            userDefault.set(self.passPetitionId, forKey: "pid")
                            userDefault.set(self.transactionId, forKey: "trid")
                            userDefault.synchronize()
                            UIApplication.shared.open(link, options: [:], completionHandler: nil)
                        }
                    }
                    
                    customLoader.hideIndicator()
                    self.blurView.isHidden = true
                    self.paymentView.isHidden = true
                    self.centerView.isHidden = true
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (comeFrom == "details")
        {
            self.dismiss(animated: false, completion: nil)
        }
        else {
            let detailvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "PetitionDetailsVC") as! PetitionDetailsVC
            detailvc.petitionId = self.passPetitionId
            detailvc.checkScreen = "check"
            present(detailvc, animated: false, completion: nil)
        }
    }
    
    func addBottomLayer(ht : CGFloat, color : UIColor)
    {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:amountTF.frame.height+1, width:amountTF.frame.width, height:ht)
        amountTF.layer.addSublayer(border)
    }
    
    // Price Button Action
    @objc func selectPriceToDonate(_ sender : UIButton)
    {
        amountTF.text = "\((sender.titleLabel?.text)!).00".replacingOccurrences(of: "\(Current_Currency)", with: "").replacingOccurrences(of: " ", with: "")
        
        for btn in priceBtnCollection
        {
            btn.layer.borderColor = UIColor.init(hexString: ColorCode.progressColor).cgColor
            btn.layer.borderWidth = 1
            btn.backgroundColor = UIColor.white
            btn.setTitleColor(UIColor.init(hexString: ColorCode.progressColor), for: .normal)
        }
        sender.backgroundColor = UIColor.init(hexString: ColorCode.progressColor)
        sender.setTitleColor(UIColor.white, for: .normal)
    }
    
    // Mark : Call service
    func callService(urlstr : String, parameters : String, check : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
                customLoader.hideIndicator()
                wallet_amount = result["wallet_amount"] as! NSNumber
                userDefault.set(wallet_amount, forKey: "walletAmt")
                userDefault.synchronize()
                let data_json : NSDictionary = ["petition_id" : "\(self.passPetitionId)", "uid" : user_id]
                socket.emit("donate_this_petition", data_json)
                
                //  let total_comsn : Float = ((Float(self.amountStr)!) * (Float(commission_value)!/100)) + Float(commission_Card)!
                self.successLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "thank_for_donate")) \n\(self.amountStr) \(Current_Currency)"
                
                self.paymentView.isHidden = true
                self.blurView.isHidden = false
                self.centerSuccessVw.isHidden = false
            }
        }
    }

    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
}

extension DonateViewController : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        addBottomLayer(ht: 1, color: UIColor.init(hexString: ColorCode.FCBlackColor))
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        for btn in priceBtnCollection
        {
            btn.layer.borderColor = UIColor.init(hexString: ColorCode.progressColor).cgColor
            btn.layer.borderWidth = 1
            btn.backgroundColor = UIColor.white
            btn.setTitleColor(UIColor.init(hexString: ColorCode.progressColor), for: .normal)
        }
        
        return true
    }
}

extension DonateViewController : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = peopleTableView.dequeueReusableCell(withIdentifier: "peoplecell") as! PeopleTableCell
        
        let dict = listArray[indexPath.row]
        
        cell.userImage.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        cell.userImage.layer.cornerRadius = cell.userImage.frame.width/2
        cell.userImage.layer.masksToBounds = true
        
        let days = retreiveDaysInterval(passDateStr: dict["created_on"] as! String)
        let dayStr = ApiResponse.getLanguageFromUserDefaults(inputString: "days_ago").replacingOccurrences(of: "[days]", with: days)
        if (ApiResponse.getLanguageFromUserDefaults(inputString: dict["message"] as! String) != "") {
            cell.msgLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: dict["message"] as! String)) \(dayStr)"
        }
        else {
            cell.msgLabel.text = "\(dict["message"] as! String) \(dayStr)"
        }
        
        cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        
        return cell
    }
    
    func retreiveDaysInterval(passDateStr : String) -> String
    {
        let datestr = passDateStr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "yyyy-MM-dd")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let formatedStartDate = dateFormatter.date(from: datestr!)
        
        let currentDate = Date()
        let components = Set<Calendar.Component>([.day])
        let differenceOfDate = Calendar.current.dateComponents(components, from: formatedStartDate!, to: currentDate)
        
        return "\(differenceOfDate.day!)"
    }
}

class PeopleTableCell : UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var msgLabel: UILabel!
}


extension Double {
    
    func roundedDecimal(to scale: Int = 0, mode: NSDecimalNumber.RoundingMode = .plain) -> Decimal {
        var decimalValue = Decimal(self)
        var result = Decimal()
        NSDecimalRound(&result, &decimalValue, scale, mode)
        return result
    }
}
