//
//  PetitionDetailsVC.swift
//  Darious
//
//  Created by Dario Carrasco on 20/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import youtube_ios_player_helper_swift

var listArray : [NSDictionary] = []

class PetitionDetailsVC: UIViewController {

    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var imageVw : UIImageView!
    @IBOutlet weak var msgLabel : UILabel!
    @IBOutlet weak var checkBtnOne : UIButton!
    @IBOutlet weak var checkBtnTwo : UIButton!
    @IBOutlet weak var lowerLabel : UILabel!
    @IBOutlet weak var reportBtn : KGHighLightedButton!
    @IBOutlet weak var shareBtn : UIButton!
    
    @IBOutlet weak var signBtn : UIButton!
    @IBOutlet weak var donateBtn : UIButton!
    @IBOutlet weak var checkFirstBtn : UIButton!
    @IBOutlet weak var checkSecondBtn : UIButton!
    
    @IBOutlet weak var progressBar : UIProgressView!
    @IBOutlet weak var listTableview : UITableView!
//    @IBOutlet weak var youtubePlayerVw : YouTubePlayerView!
    @IBOutlet weak var youtubePlayerVw : UIView!
    @IBOutlet weak var viewOne : UIView!
    @IBOutlet weak var viewTwo : UIView!
    
    @IBOutlet weak var donateTopConstraint : NSLayoutConstraint!
    @IBOutlet weak var viewTwoTopConstraint : NSLayoutConstraint!
    @IBOutlet weak var tableHtConstraint : NSLayoutConstraint!
    @IBOutlet weak var viewOneHtConstraint : NSLayoutConstraint!
    @IBOutlet weak var viewTwoHtConstraint : NSLayoutConstraint!
    @IBOutlet weak var progressHtConstraint : NSLayoutConstraint!
    @IBOutlet weak var contentvwHtConstraint : NSLayoutConstraint!
    @IBOutlet weak var lowerlblHtConstraint : NSLayoutConstraint!
    @IBOutlet weak var titlelblHtConstraint : NSLayoutConstraint!
    
    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var contentVW : UIView!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    
    var petitionId : NSNumber = 0
    var visible : String = "yes"
    var passVid : String = ""
    var videoLoader : UIActivityIndicatorView!
    private var playerView: PlayerView!
    var currentSliderVlaue : Float = 0
    var timer : Timer?
    var checkScreen : String = ""
    var shareImg: UIImage?
    var shareImgUrl: String = ""
    var shareYTurl: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FCBlackColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        progressHtConstraint.constant = 20
        contentvwHtConstraint.constant = contentvwHtConstraint.constant + progressHtConstraint.constant
        progressBar.layer.cornerRadius = 8
        progressBar.clipsToBounds = true
        
        listTableview.rowHeight = UITableView.automaticDimension
        listTableview.estimatedRowHeight = 45
        
        setBottomView()
        setLanguage()
        setVideoLoader()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&petition_id=\(self.petitionId)"
            self.callService(urlstr: Api.Petition_Detail_URL, parameters: params, check: "get")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
        
        socket.on("petition_message_emit") { (dataArray, socketAck) in
            
            let jsonDict = dataArray[0] as? NSDictionary
            let datarr = jsonDict!["data"] as? [NSDictionary]
            let dict = datarr![0]
            if (listArray.contains(dict) == false)
            {
                let arr : NSMutableArray = NSMutableArray.init(array: listArray)
                arr.insert(dict, at: 0)
                listArray = arr as! [NSDictionary]
            }
            self.listTableview.isHidden = false
            self.listTableview.reloadData()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if (passVid != "")
        {
            playerView.ytPlayerView.delegate = nil
            playerView.ytPlayerView.stopVideo()
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        signBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "sign_this_petition")), for: .normal)
        donateBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "donate")), for: .normal)
        checkFirstBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "i_want_to_know")), for: .normal)
        checkSecondBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "i_dont_want_to_know")), for: .normal)
        reportBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "report")), for: .normal)
    }
    
    func setMMPlayer(passUrl : String, videoid : String)
    {
        passVid = videoid
        youtubePlayerVw.isHidden = false
        playerView = UINib(nibName: "PlayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PlayerView
        playerView.videoId = videoid
        addPlayerView()
    }
    
    private func addPlayerView(){
        contentVW.addSubview(playerView)
        contentVW.addSubview(shareBtn)
        playerView.frame.size = CGSize(width: youtubePlayerVw.frame.width, height: youtubePlayerVw.frame.height)
        let xValue = (self.contentVW.frame.width - playerView.frame.width)/2
        let yValue = titleLabel.frame.origin.y + titlelblHtConstraint.constant + 10
        playerView.frame.origin = CGPoint(x: xValue, y: yValue)
        playerView.autoresizingMask = .flexibleWidth
        videoLoader.stopAnimating()
        
        playerView.btnFullScreen.addTarget(self, action: #selector(enterFullScreen(_:)), for: .touchUpInside)
    }
    
    @objc func enterFullScreen(_ sender : UIButton)
    {
        if (playerView.isFullScreen == false)
        {
            let landscapevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "LandscapeVC") as! LandscapeVC
            landscapevc.vId = passVid
            landscapevc.sliderCurrentVlaue = self.playerView.timeSlider.value
            self.present(landscapevc, animated: false, completion: nil)
        }
    }
    
    func setVideoLoader()
    {
        videoLoader = UIActivityIndicatorView.init()
        videoLoader.frame.size = CGSize(width: 25, height: 25)
        videoLoader.center = youtubePlayerVw.center
        videoLoader.hidesWhenStopped = true
        contentVW.addSubview(videoLoader)
        videoLoader.isHidden = true
    }
    
    @IBAction func onReport(_ sender: Any)
    {
        let alertcont = UIAlertController.init(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_provide_reason"), preferredStyle: .alert)
        alertcont.addTextField { (msgTextField) in
            msgTextField.placeholder = ""
        }
        let yesaction = UIAlertAction.init(title: ApiResponse.getLanguageFromUserDefaults(inputString: "report"), style: .default) { (action) in
            customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
            self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.reportingUser), userInfo: nil, repeats: false)
            let _ = alertcont.textFields![0] as UITextField
        }
        let noaction = UIAlertAction.init(title: ApiResponse.getLanguageFromUserDefaults(inputString: "cancel"), style: .cancel, handler: nil)
        alertcont.addAction(yesaction)
        alertcont.addAction(noaction)
        present(alertcont, animated: true, completion: nil)
    }
    
    @objc func reportingUser()
    {
        customLoader.hideIndicator()
        timer?.invalidate()
        ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "thanks_for_report"), message: "", controller: self)
    }
    
    @IBAction func signPetition(_ sender: Any)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&petition_id=\(self.petitionId)&publicVisible=\(visible)"
        self.callService(urlstr: Api.Sign_Petition_URL, parameters: params, check: "sign")
    }
    
    @IBAction func onDonate(_ sender: Any)
    {
        if (petitionId != 0) {
            DispatchQueue.main.async {
                if let link = URL(string: "\(Api.Donate_URL)?uid=\(user_id)&petition_id=\(self.petitionId)&type=flyclaim&visible=\(self.visible)") {
                    UIApplication.shared.open(link)
                }
            }
        }
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        let sharevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
        sharevc.sharePetitionId = self.petitionId
        sharevc.descriptionStr = lowerLabel.text!
        sharevc.youtubeUrl = shareYTurl
        sharevc.imageUrl = shareImg
        sharevc.imageUrlStr = shareImgUrl
        sharevc.comeFrom = "details"
        present(sharevc, animated: false, completion: nil)
    }
    
    @IBAction func checkButtonOne(_ sender: Any)
    {
        checkBtnOne.setImage(#imageLiteral(resourceName: "SelectCircle"), for: .normal)
        checkBtnTwo.setImage(#imageLiteral(resourceName: "UnselectCircle"), for: .normal)
        visible = "yes"
    }
    
    @IBAction func checkButtonTwo(_ sender: Any)
    {
        checkBtnTwo.setImage(#imageLiteral(resourceName: "SelectCircle"), for: .normal)
        checkBtnOne.setImage(#imageLiteral(resourceName: "UnselectCircle"), for: .normal)
        visible = "no"
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (passVid != "") {
            if contentVW.subviews.contains(playerView) {
                playerView.playStop(sender: playerView.btnPlayPause)
            }
        }
        if (checkScreen != ""){
            let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
            present(homevc, animated: false, completion: nil)
        }
        else {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    // Mark : Get video id from You tube URL
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
    
    // Set Data
    func setDetails(dictPass : NSDictionary)
    {
        titleLabel.text = dictPass["petition_title"] as? String
        
        if dictPass["facebook_link"] as! String != ""  {
            // play you tube player
            let vid = getYoutubeId(youtubeUrl: dictPass["facebook_link"] as! String)
            videoLoader.isHidden = false
            videoLoader.startAnimating()
            setMMPlayer(passUrl: dictPass["facebook_link"] as! String, videoid: vid!)
            shareYTurl = dictPass["facebook_link"] as! String
        } else {
            imageVw.sd_setImage(with: URL(string: "\(Api.Petition_Image_URL)\(dictPass["photo_or_video"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            shareImg = imageVw.image
            shareImgUrl = "\(Api.Petition_Image_URL)\(dictPass["photo_or_video"] as! String)"
        }
        lowerLabel.text = dictPass["explain_the_problem"] as? String
        
        let petitionCount = (dictPass["signThisPetitionCount"] as? NSNumber)!
        let signature = (dictPass["number_of_signatures"] as? String)!
        
        let percentage : Float = (Float.init(exactly: petitionCount)!) / (Float(signature)!)
        progressBar.setProgress(percentage, animated: false)
        
        if (dictPass["u_sign"] as! String == "yes") {
            donateTopConstraint.constant = -32
            viewTwoTopConstraint.constant = -25
            viewTwoHtConstraint.constant = 0
            viewTwo.isHidden = true
            contentvwHtConstraint.constant = contentvwHtConstraint.constant - 75
        }
        
        if (Int(truncating: petitionCount) == Int(signature))
        {
            let totalht = viewOneHtConstraint.constant + viewTwoHtConstraint.constant + tableHtConstraint.constant
            contentvwHtConstraint.constant = contentvwHtConstraint.constant - totalht
            viewOneHtConstraint.constant = 0
            viewOne.isHidden = true
            viewTwoHtConstraint.constant = 0
            viewTwo.isHidden = true
            tableHtConstraint.constant = 0
            listTableview.isHidden = true
            msgLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "victory_we_have_achieved")) 1"
        }
        else {
            let largeNumber = Int(signature)
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            let formattedNumber = numberFormatter.string(from: NSNumber(value: largeNumber!))
            
            msgLabel.text = "\(petitionCount) \(ApiResponse.getLanguageFromUserDefaults(inputString: "people_has_signed")) \(formattedNumber!)"
        }
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
                
                if (check == "get") {
                    if let dict = result["petitionDetails"] as? NSDictionary
                    {
                        let dictArray = dict["signThisPetitionDetails"] as! [NSDictionary]
                        listArray = dictArray
                        if (dictArray.count > 0)
                        {
                            self.tableHtConstraint.constant = 128
                        } else {
                            self.contentvwHtConstraint.constant = self.contentvwHtConstraint.constant - self.tableHtConstraint.constant
                            //   self.tableHtConstraint.constant = 0
                            self.tableHtConstraint.isActive = false
                            self.view.addConstraint(NSLayoutConstraint(item: self.listTableview, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.lessThanOrEqual, toItem: self.listTableview, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1, constant: 0))
                        }
                        self.listTableview.isHidden = false
                        self.listTableview.reloadData()
                        self.setDetails(dictPass: dict)
                    }
                }
                else {
                    let data_json : NSDictionary = ["insertId" : "\(self.petitionId)", "uid" : user_id]
                    socket.emit("sign_this_petition", data_json)
                    let signvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "SignPetitionVC") as! SignPetitionVC
                    signvc.signPetitionId = self.petitionId
                    signvc.visibleValue = self.visible
                    self.present(signvc, animated: false, completion: nil)
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
}

// MARK : TableView Delegates
extension PetitionDetailsVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = listTableview.dequeueReusableCell(withIdentifier: "listcell") as! ListTableCell
        
        let dict = listArray[indexPath.row]
        cell.userImage.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        cell.userImage.layer.cornerRadius = cell.userImage.frame.width/2
        cell.userImage.layer.masksToBounds = true
        
        var daysStr : String = ""
        let timecomponent = retreiveDaysInterval(passDateStr: dict["created_on"] as! String)
        if (timecomponent.day! > 0)
        {
            daysStr = ApiResponse.getLanguageFromUserDefaults(inputString: "days_ago").replacingOccurrences(of: "[day]", with: "\(timecomponent.day!)")
        }
        else if (timecomponent.hour! > 0)
        {
            daysStr = ApiResponse.getLanguageFromUserDefaults(inputString: "hours_ago").replacingOccurrences(of: "[hour]", with: "\(timecomponent.hour!)")
        }
        else if (timecomponent.minute! > 0)
        {
            daysStr = ApiResponse.getLanguageFromUserDefaults(inputString: "minutes_ago").replacingOccurrences(of: "[minute]", with: "\(timecomponent.minute!)")
        }
        else {
            daysStr = ApiResponse.getLanguageFromUserDefaults(inputString: "few_seconds_ago")
        }
        
        if (ApiResponse.getLanguageFromUserDefaults(inputString: dict["message"] as! String) != "") {
            cell.msgLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: dict["message"] as! String)) \(daysStr)"
        }
        else {
            cell.msgLabel.text = "\(dict["message"] as! String) \(daysStr)"
        }
        
        cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        
        return cell
    }
    
    func retreiveDaysInterval(passDateStr : String) -> DateComponents
    {
        let datestr = passDateStr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "yyyy-MM-dd HH:mm:ss")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formatedStartDate = dateFormatter.date(from: datestr!)

        let currentDate = Date()
        let components = Set<Calendar.Component>([.day, .hour , .minute, .second])
        let differenceOfDate = Calendar.current.dateComponents(components, from: formatedStartDate!, to: currentDate)
      //  print("diffffff ====\(datestr)-------- \(formatedStartDate!)-----\(currentDate)------- \(differenceOfDate.day!)----- \(differenceOfDate.hour!)-----\(differenceOfDate.minute!)-------\(differenceOfDate.second!)")
        return differenceOfDate
    }
}

class ListTableCell : UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var msgLabel: UILabel!
}

