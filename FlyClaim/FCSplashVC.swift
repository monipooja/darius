//
//  FCSplashVC.swift
//  Darious
//
//  Created by Dario Carrasco on 20/05/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FCSplashVC: UIViewController {
    
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = true
   //     UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FCBlackColor)
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(gotoNext), userInfo: nil, repeats: false)
    }

    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func gotoNext()
    {
        timer.invalidate()
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
}

public func flyclaimBottomViewAction(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 1:
        // Browse
        let browsevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "BrowseViewController") as! BrowseViewController
        controller.present(browsevc, animated: false, completion: nil)
        break
    case 2:
        // My Petition
        let mypetvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "MyPetitionVC") as! MyPetitionVC
        controller.present(mypetvc, animated: false, completion: nil)
        break
    case 3:
        // Darious
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
        break
    case 4:
        // Wallet
        let walletvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCWalletVC") as! FCWalletVC
        controller.present(walletvc, animated: false, completion: nil)
        break
    case 5:
        // Notification
        let notifyvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCNotificationVC") as! FCNotificationVC
        controller.present(notifyvc, animated: false, completion: nil)
        break
    default:
        break
    }
}
