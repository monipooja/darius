//
//  SecondStepVC.swift
//  Darious
//
//  Created by Dario Carrasco on 21/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class SecondStepVC: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet var contentVw: UIView!
    @IBOutlet var contentVwHt: NSLayoutConstraint!
    @IBOutlet var scrollVwHt: NSLayoutConstraint!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet var happenTV: customTextView!
    @IBOutlet var chooseDecisionLbl: UILabel!
    @IBOutlet var decisionMsgLbl: UILabel!
    @IBOutlet var secondContinue: KGHighLightedButton!
    
    var contentFinalHt : CGFloat = 0
    var cc : CGFloat = 40
    var word_count : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        contentFinalHt = contentVwHt.constant
        
        setLanguage()
        setBottomView()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        gesture.delegate = self
        contentVw.addGestureRecognizer(gesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        contentVwHt.constant = contentFinalHt
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        chooseDecisionLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "choose_a_decision_maker")
        decisionMsgLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "choose_a_decision_maker_sub_text")
        happenTV.text = ApiResponse.getLanguageFromUserDefaults(inputString: "who_can_make_this_happen")
     secondContinue.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
    }
    
    // Mark : Gesture Action
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        contentVw.endEditing(true)
        contentVwHt.constant = contentFinalHt
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    @IBAction func onSecondContinue(_ sender: Any)
    {
        if (happenTV.text == ApiResponse.getLanguageFromUserDefaults(inputString: "who_can_make_this_happen"))
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_petition_decision_maker"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            createDict.setValue("\(happenTV.text!)", forKey: "decision")
            let thirdvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "ThirdStepVC") as! ThirdStepVC
            present(thirdvc, animated: true, completion: nil)
        }
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        createDict.setValue("", forKey: "decision")
        self.dismiss(animated: false, completion: nil)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
}

extension SecondStepVC : UITextViewDelegate, UITextFieldDelegate
{
    // MARK : TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        contentVwHt.constant = contentFinalHt + 100
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        contentVwHt.constant = contentFinalHt
    }
    
    // MARK : TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = UIColor.black
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "") && (textView.text.count == 1) {
            let newPosition = textView.beginningOfDocument
            textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
            textView.text = ApiResponse.getLanguageFromUserDefaults(inputString: "who_can_make_this_happen")
                //ApiResponse.getLanguageFromUserDefaults(inputString: "write_here")
            textView.textColor = UIColor.darkGray
            return false
        }
        else if textView.textColor == UIColor.darkGray {
            textView.text = ""
            textView.textColor = UIColor.black
            return true
        }
        else {
            return true
        }
    }
    
//    func textViewDidChange(_ textView: UITextView) {
//
//        let fixedWidth = textView.frame.size.width
//        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//        if (newSize.height < 105) && (newSize.height > cc)
//        {
//            // textView.isScrollEnabled = false
//            textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
//            if (cc < newSize.height)
//            {
//                textView.frame.origin.y = textView.frame.origin.y - 3 - (newSize.height - cc)
//                // print("YYYYYYYYYYY = \(textView.frame.origin.y)")
//                cc = newSize.height + 2
//                word_count = textView.text.count
//            }
//            else
//            {
//                if (word_count > textView.text.count) && (cc != newSize.height) {
//                    textView.frame.origin.y = textView.frame.origin.y + (cc - newSize.height)
//                    cc = newSize.height
//                }
//            }
//        }
//        else {
//            textView.isScrollEnabled = true
//            textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: cc)
//        }
//    }
}
