//
//  MyPetitionVC.swift
//  Darious
//
//  Created by Dario Carrasco on 23/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class MyPetitionVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet var userImgVw: UIImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var msgLabel: UILabel!
    @IBOutlet var myPetitionLbl: UILabel!
    @IBOutlet var btnBgView: CustomUIView!
    @IBOutlet var startedBtn: KGHighLightedButton!
    @IBOutlet var signedBtn: KGHighLightedButton!
    @IBOutlet var startBtn: KGHighLightedButton!
    
    @IBOutlet var myPetitionTableVw: UITableView!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    
    var petitionArray : [NSDictionary] = []
    var selectBtn : String = "started"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        myPetitionTableVw.rowHeight = UITableView.automaticDimension
        myPetitionTableVw.estimatedRowHeight = 170
        
        userImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(userData["profile_pic"] as! String)"), placeholderImage: UIImage.init(named: "demo_profile.png"))
        userImgVw.layer.cornerRadius = userImgVw.frame.width/2
        userImgVw.layer.masksToBounds = true
        userName.text = "\(userData["first_name"] as! String) \(userData["last_name"] as! String)"
        
        setLanguage()
        setBottomView()
        setCorners(passBtn: startedBtn)
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
            
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.MyStarted_Petition_URL, parameters: params, check: "start")
            
            let params2 = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.MySigned_Petition_URL, parameters: params2, check: "signed")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        myPetitionLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "my_petition")
        startBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "start_a_petition")), for: .normal)
        signedBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "signed")), for: .normal)
        startedBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "started")), for: .normal)
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func openStartedPetition(_ sender: Any)
    {
        selectBtn = "started"
        
        setCorners(passBtn: startedBtn)
        
        myPetitionTableVw.isHidden = true
        startedBtn.backgroundColor = UIColor.white
        startedBtn.setTitleColor(UIColor.black, for: .normal)
        signedBtn.backgroundColor = UIColor.clear
        signedBtn.setTitleColor(UIColor.white, for: .normal)
        
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
        self.callService(urlstr: Api.MyStarted_Petition_URL, parameters: params, check: "start")
    }
    
    @IBAction func openSignedPetition(_ sender: Any)
    {
        selectBtn = "signed"
        
        setCorners(passBtn: signedBtn)
        
        myPetitionTableVw.isHidden = true
        signedBtn.backgroundColor = UIColor.white
        signedBtn.setTitleColor(UIColor.black, for: .normal)
        startedBtn.backgroundColor = UIColor.clear
        startedBtn.setTitleColor(UIColor.white, for: .normal)
        
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
        self.callService(urlstr: Api.MySigned_Petition_URL, parameters: params, check: "signed")
    }
    
    @IBAction func startPetition(_ sender: Any)
    {
        let firststartvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FirstStartVC") as! FirstStartVC
        present(firststartvc, animated: false, completion: nil)
    }
    
    func setCorners(passBtn : KGHighLightedButton)
    {
        passBtn.layer.cornerRadius = 8
        btnBgView.layer.cornerRadius = 8
        if #available(iOS 11.0, *) {
            if (passBtn == startedBtn)
            {
                passBtn.layer.maskedCorners = [.layerMinXMinYCorner]
            }
            else {
                passBtn.layer.maskedCorners = [.layerMaxXMinYCorner]
            }
            btnBgView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
            var path : UIBezierPath!
            if (passBtn == startedBtn)
            {
                path = UIBezierPath(roundedRect: passBtn.bounds, byRoundingCorners: [.topLeft], cornerRadii: CGSize(width: 12, height: 12))
            }
            else {
                path = UIBezierPath(roundedRect: passBtn.bounds, byRoundingCorners: [.topRight], cornerRadii: CGSize(width: 12, height: 12))
            }
            path = UIBezierPath(roundedRect: passBtn.bounds, byRoundingCorners: [.topRight, .topLeft], cornerRadii: CGSize(width: 12, height: 12))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            passBtn.layer.mask = mask
            btnBgView.layer.mask = mask
          //  passBtn.layer.masksToBounds = true
        }
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
                
                if (check == "start") {
                    if let dictArray = result["petitionDetails"] as? [NSDictionary]
                    {
                        self.startedBtn.setTitle("\(ApiResponse.getLanguageFromUserDefaults(inputString: "started")) (\(result["startedPetitionCount"] as! NSNumber))", for: .normal)
                        
                        if (self.selectBtn == "started") {
                            self.petitionArray = dictArray
                            self.myPetitionTableVw.isHidden = false
                            self.myPetitionTableVw.reloadData()
                        }
                    }
                }
                else {
                    if let dictArray = result["petitionDetails"] as? [NSDictionary]
                    {
                        self.signedBtn.setTitle("\(ApiResponse.getLanguageFromUserDefaults(inputString: "signed")) (\(result["signedPetitionCount"] as! NSNumber))", for: .normal)
                        
                        if (self.selectBtn == "signed") {
                            self.petitionArray = dictArray
                            self.myPetitionTableVw.isHidden = false
                            self.myPetitionTableVw.reloadData()
                        }
                    }
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Get video id from You tube URL
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }

    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
}

extension MyPetitionVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return petitionArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(selectBtn == "started")
        {
            let cell = myPetitionTableVw.dequeueReusableCell(withIdentifier: "startedcell") as! StartedTableCell
            
            let dict = petitionArray[indexPath.section]
            cell.titleLabel.text = dict["petition_title"] as? String
            cell.descTextVw.text = dict["explain_the_problem"] as? String
            
            if let _ = dict["number_of_signatures"] as? String
            {
                let number = Int(dict["number_of_signatures"] as! String)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: number!))
                cell.supoorterLabel.text = "\(formattedNumber!) \(ApiResponse.getLanguageFromUserDefaults(inputString: "supporters"))"
            }
            
            if let _ = dict["photo_or_video"] as? String
            {
                if (dict["photo_or_video"] as! String != "")
                {
                    cell.petitionImageVw.sd_setImage(with: URL(string: "\(Api.Petition_Image_URL)\(dict["photo_or_video"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                    cell.playImgVw.isHidden = true
                }
                else if (dict["photo_or_video"] as! String == "") && (dict["facebook_link"] as! String != "") {
                    // show thumbnail
                    let vid = getYoutubeId(youtubeUrl: dict["facebook_link"] as! String)
                    cell.petitionImageVw.sd_setImage(with: URL(string: "http://img.youtube.com/vi/\(vid!)/0.jpg"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                    cell.playImgVw.isHidden = false
                }
                else {
                    cell.petitionImageVw.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                    cell.playImgVw.isHidden = true
                }
            }
            
         cell.editBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "edit"), for: .normal)
            cell.editBtn.addTarget(self, action: #selector(goToEditPetition(_:)), for: .touchUpInside)
            
            return cell
        }
        else {
            let cell = myPetitionTableVw.dequeueReusableCell(withIdentifier: "signedcell") as! SignedTableCell
            
            let dict = petitionArray[indexPath.section]
            cell.titleLabel.text = dict["petition_title"] as? String
            cell.descLabel.text = dict["explain_the_problem"] as? String
            
            if let _ = dict["number_of_signatures"] as? String
            {
                let number = Int(dict["number_of_signatures"] as! String)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value: number!))
                cell.supoorterLabel.text = "\(formattedNumber!) \(ApiResponse.getLanguageFromUserDefaults(inputString: "supporters"))"
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = petitionArray[indexPath.section]
        let detailvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "PetitionDetailsVC") as! PetitionDetailsVC
        if (selectBtn == "started")
        {
            detailvc.petitionId = dict["id"] as! NSNumber
        }
        else {
            detailvc.petitionId = dict["petition_id"] as! NSNumber
        }
        present(detailvc, animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 20))
        vw.backgroundColor = UIColor.clear
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    @objc func goToEditPetition(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: myPetitionTableVw)
        let idxpath = myPetitionTableVw.indexPathForRow(at: point)
        
        let editvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "EditPetitionVC") as! EditPetitionVC
        editvc.passDict = self.petitionArray[(idxpath?.section)!]
        present(editvc, animated: false, completion: nil)
    }
}

class StartedTableCell : UITableViewCell {
    
    @IBOutlet var petitionImageVw: customImageView!
    @IBOutlet var playImgVw: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descTextVw: UITextView!
    @IBOutlet var supoorterLabel: UILabel!
    
    @IBOutlet var editBtn: KGHighLightedButton!
}

class SignedTableCell : UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descLabel: UILabel!
    @IBOutlet var supoorterLabel: UILabel!
}
