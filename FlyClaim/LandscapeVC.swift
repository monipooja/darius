//
//  LandscapeVC.swift
//  Darious
//
//  Created by Dario Carrasco on 19/06/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class LandscapeVC: UIViewController {

    var playerVw : PlayerView!
    var vId : String = ""
    var label : UILabel!
    var sliderCurrentVlaue : Float = 0
    var videoLoader : UIActivityIndicatorView!
    
    override func viewDidLoad() {
        UIApplication.shared.statusBarView?.isHidden = false
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .landscape
        
        setVideoLoader()
        setMMPlayer(videoid: vId)
    }
    
    func setMMPlayer(videoid : String)
    {
        playerVw = UINib(nibName: "PlayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PlayerView
        playerVw.videoId = videoid
        addPlayerView()
    }
    
    private func addPlayerView(){
        self.view.addSubview(playerVw)
        playerVw.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
     //   playerVw.autoresizingMask = .flexibleWidth
        videoLoader.stopAnimating()
       playerVw.timeSlider.value = sliderCurrentVlaue
        playerVw.updateTime()
       playerVw.btnFullScreen.addTarget(self, action: #selector(exitFullScreen(_:)), for: .touchUpInside)
    }
    
    func setVideoLoader()
    {
        videoLoader = UIActivityIndicatorView.init()
        videoLoader.frame.size = CGSize(width: 25, height: 25)
        videoLoader.center = self.view.center
        videoLoader.hidesWhenStopped = true
        self.view.addSubview(videoLoader)
        videoLoader.isHidden = true
    }
    
    @objc func exitFullScreen(_ sender : UIButton)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .portrait
        self.dismiss(animated: false, completion: nil)
    }
}
