//
//  EditPetitionVC.swift
//  Darious
//
//  Created by Dario Carrasco on 23/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import Photos

class EditPetitionVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet var contentVw: UIView!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet var contentVwHt: NSLayoutConstraint!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    
    @IBOutlet var titleTextVw: customTextView!
    @IBOutlet var signatureTF: KGHighLightedField!
    @IBOutlet var decisionTextVw: customTextView!
    @IBOutlet var problemTextVw: customTextView!
    
    @IBOutlet var changeBtn: UIButton!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var signatureLabel: UILabel!
    @IBOutlet var decisionLabel: UILabel!
    @IBOutlet var problemLabel: UILabel!
    @IBOutlet var continueBtn: KGHighLightedButton!
    @IBOutlet var youtubeTF: UITextField!
    
    @IBOutlet var petitionImageVw: UIImageView!
    @IBOutlet weak var youtubePlayerVw : UIView!
    
    var contentFinalHt : CGFloat = 0
    var passDict : NSDictionary = [:]
    var imgPick = UIImagePickerController()
    
    var photoString : String = ""
    var asset : PHAsset!
    
    var passVid : String = ""
    var videoLoader : UIActivityIndicatorView!
    private var playerView: PlayerView!
    var currentSliderVlaue : Float = 0
    var timer : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
           // btn.setTitle(btmarr[btn.tag-1], for: .normal)
        }
        setLanguage()
        setVideoLoader()
        
        DispatchQueue.main.async {
            self.setData()
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if (passVid != "")
        {
            playerView.ytPlayerView.delegate = nil
            playerView.ytPlayerView.stopVideo()
        }
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "write_your_petition_title")
        signatureLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "number_of_signature_to_achieve")
        decisionLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "choose_a_decision_maker")
        problemLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "explain_the_problem_to_solve")
        continueBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
        changeBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "change_image")), for: .normal)
    }
    
    // Mark : Gesture Action
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        contentVw.endEditing(true)
        contentVwHt.constant = contentFinalHt
    }
    
    func setMMPlayer(passUrl : String, videoid : String)
    {
        passVid = videoid
        youtubePlayerVw.isHidden = false
        playerView = UINib(nibName: "PlayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PlayerView
        playerView.videoId = videoid
        addPlayerView()
    }
    
    private func addPlayerView(){
        contentVw.addSubview(playerView)
        playerView.frame.size = CGSize(width: youtubePlayerVw.frame.width, height: youtubePlayerVw.frame.height)
        playerView.center = youtubePlayerVw.center
        playerView.autoresizingMask = .flexibleWidth
        videoLoader.stopAnimating()
        
        playerView.btnFullScreen.addTarget(self, action: #selector(enterFullScreen(_:)), for: .touchUpInside)
    }
    
    @objc func enterFullScreen(_ sender : UIButton)
    {
        if (playerView.isFullScreen == false)
        {
            let landscapevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "LandscapeVC") as! LandscapeVC
            landscapevc.vId = passVid
            landscapevc.sliderCurrentVlaue = self.playerView.timeSlider.value
            self.present(landscapevc, animated: false, completion: nil)
        }
    }
    
    func setVideoLoader()
    {
        videoLoader = UIActivityIndicatorView.init()
        videoLoader.frame.size = CGSize(width: 25, height: 25)
        videoLoader.center = youtubePlayerVw.center
        videoLoader.hidesWhenStopped = true
        contentVw.addSubview(videoLoader)
        videoLoader.isHidden = true
    }
    
    // Mark : Get video id from You tube URL
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
    
    // Mark : Set data
    func setData()
    {
        contentFinalHt = contentVwHt.constant
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        gesture.delegate = self
        contentVw.addGestureRecognizer(gesture)
        
        titleTextVw.text = passDict["petition_title"] as? String
        signatureTF.text = passDict["number_of_signatures"] as? String
        decisionTextVw.text = passDict["choose_a_decision_maker"] as? String
        problemTextVw.text = passDict["explain_the_problem"] as? String
        
        if passDict["facebook_link"] as! String != ""  {
            // play you tube player
            let vid = getYoutubeId(youtubeUrl: passDict["facebook_link"] as! String)
            videoLoader.isHidden = false
            videoLoader.startAnimating()
            youtubeTF.isHidden = false
            youtubeTF.text = passDict["facebook_link"] as? String
            setMMPlayer(passUrl: passDict["facebook_link"] as! String, videoid: vid!)
        } else {
            petitionImageVw.sd_setImage(with: URL(string: "\(Api.Petition_Image_URL)\(passDict["photo_or_video"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        }
        
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    @IBAction func openGalleryToUploadPic(_ sender: Any)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)
        {
            imgPick.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imgPick.delegate = self
            imgPick.allowsEditing = false
            self.present(imgPick, animated: true, completion: nil)
        }
    }
    
    // Image picker delegate
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let image = info[.originalImage] as! UIImage
        petitionImageVw.image = image
        if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            asset = result.firstObject
        }
        if(asset==nil){
            photoString="FirstImg.png"
        }
        else{
            let fname = (asset.value(forKey: "filename"))!
            let words = (fname as! String).components(separatedBy: ".")
            let fnm=words[0]
            photoString = "\(fnm).png"
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // Mark : Check youtube valid URL
    func validateYoutubeLink(_ urlStr : String) -> Bool
    {
        let a = "(http(s)?:\\/\\/)?(www\\.|m\\.)?youtu(be\\.com|\\.be)(\\/watch\\?([&=a-z]{0,})(v=[\\d\\w]{1,}).+|\\/[\\d\\w]{1,})"
        let urlTest = NSPredicate(format: "SELF MATCHES %@", a)
        return urlTest.evaluate(with: urlStr)
    }
    
    @IBAction func onEditContinue(_ sender: Any)
    {
        if (titleTextVw.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_petition_title"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (youtubeTF.text == "") && (youtubeTF.isHidden == false)
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "you_can_only_add_image_or_url"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (youtubeTF.text != "") && (validateYoutubeLink(youtubeTF.text!) == false)
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_correct_url"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (signatureTF.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_petitio"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (decisionTextVw.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_petition_decision_maker"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (problemTextVw.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_explain_problem_first"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            editSavePetiton()
        }
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onGoBack(_ sender: Any)
    {
        if (passVid != "") {
            if contentVw.subviews.contains(playerView) {
                playerView.playStop(sender: playerView.btnPlayPause)
            }
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    /// MARK : Multipart (file format)
    func editSavePetiton(){
        
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        
        let url = NSURL(string: Api.Edit_Petition_URL)
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var body = Data()
        
        let mimetype = "image/png"
        let parameters : [String : String] = ["access_token":"\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "petition_title":"\(titleTextVw.text!)", "number_of_signatures":"\(signatureTF.text!)", "choose_a_decision_maker":"\(decisionTextVw.text!)", "explain_the_problem":"\(problemTextVw.text!)", "petitionId" : "\(passDict["id"] as! NSNumber)", "link" : "\(youtubeTF.text!)"]
        
        for (key, value) in parameters
        {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        let image_data = petitionImageVw.image!.jpegData(compressionQuality: 0.2)
        if(image_data == nil)
        {
            return
        }
        //define the data post parameter
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"photo_or_video\"; filename=\"\(photoString)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(image_data!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.httpBody = body as Data
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            DispatchQueue.main.async
                {
                    guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                        ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "time_out"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "try_again"), controller: self)
                        return
                    }
                    _ = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    
                    let dd = try! JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    
                    let status = dd["status"] as! NSNumber
                    
                    if (status == 1) {
                        customLoader.hideIndicator()
                        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
                        self.present(homevc, animated: false, completion: nil)
                    }
                    else if(status == 3) {
                        customLoader.hideIndicator()
                        Access_token = ""
                        userDefault.set(Access_token, forKey: "token")
                        userDefault.synchronize()
                        let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.present(VC, animated: true, completion: nil)
                    }
                    else {
                        if let str = dd["message"] {
                            let msgStr = str as! String
                            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                            if(showMsg != "") {
                                ApiResponse.alert(title: "", message: showMsg, controller: self)
                            }
                            else {
                                ApiResponse.alert(title: "", message: msgStr, controller: self)
                            }
                        }
                        else {
                            customLoader.hideIndicator()
                        }
                    }
            }
        }
        task.resume()
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}

extension EditPetitionVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        contentVwHt.constant = contentFinalHt
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        contentVwHt.constant = contentFinalHt + 100
    }
}

extension EditPetitionVC : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        contentVwHt.constant = contentFinalHt + 100
    }
}
