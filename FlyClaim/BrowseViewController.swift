//
//  BrowseViewController.swift
//  Darious
//
//  Created by Dario Carrasco on 23/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class BrowseViewController: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    
    @IBOutlet var searchTF: KGHighLightedField!
    @IBOutlet var msgLabel: UILabel!
    
    @IBOutlet var browseTableVw: UITableView!
    
    var petitionArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        searchTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "search")
        
        browseTableVw.rowHeight = UITableView.automaticDimension
        browseTableVw.estimatedRowHeight = 250
        
        setBottomView()
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&searchtext="
            self.callService(urlstr: Api.Browse_Petition_URL, parameters: params, check: "get")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onSearchPetition(_ sender: Any)
    {
        searchTF.resignFirstResponder()
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&searchtext=\(searchTF.text!)"
        self.callService(urlstr: Api.Browse_Petition_URL, parameters: params, check: "get")
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
                
                if let dictArray = result["petitionDetails"] as? [NSDictionary]
                {
                    self.petitionArray = dictArray
                    self.browseTableVw.isHidden = false
                    self.browseTableVw.reloadData()
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Get video id from You tube URL
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
}

extension BrowseViewController : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return petitionArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = browseTableVw.dequeueReusableCell(withIdentifier: "browsecell") as! BrowseTableCell
        
        let dict = petitionArray[indexPath.section]
        cell.titleLabel.text = dict["petition_title"] as? String
        cell.descTextVw.text = dict["explain_the_problem"] as? String
        
        if let _ = dict["number_of_signatures"] as? String
        {
            let number = Int(dict["number_of_signatures"] as! String)
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            let formattedNumber = numberFormatter.string(from: NSNumber(value: number!))
            cell.supoorterLabel.text = "\(formattedNumber!) \(ApiResponse.getLanguageFromUserDefaults(inputString: "supporters"))"
        }
        if let _ = dict["photo_or_video"] as? String
        {
            if (dict["photo_or_video"] as! String != "")
            {
                cell.petitionImageVw.sd_setImage(with: URL(string: "\(Api.Petition_Image_URL)\(dict["photo_or_video"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                cell.playImgVw.isHidden = true
            }
            else if (dict["photo_or_video"] as! String == "") && (dict["facebook_link"] as! String != "") {
                // show thumbnail
                let vid = getYoutubeId(youtubeUrl: dict["facebook_link"] as! String)
                cell.petitionImageVw.sd_setImage(with: URL(string: "http://img.youtube.com/vi/\(vid!)/0.jpg"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                cell.playImgVw.isHidden = false
            }
            else {
                cell.petitionImageVw.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                cell.playImgVw.isHidden = true
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = petitionArray[indexPath.section]
        let detailvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "PetitionDetailsVC") as! PetitionDetailsVC
        detailvc.petitionId = dict["id"] as! NSNumber
        present(detailvc, animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 20))
        vw.backgroundColor = UIColor.clear
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
}

class BrowseTableCell : UITableViewCell {
    
    @IBOutlet var petitionImageVw: customImageView!
    @IBOutlet var playImgVw: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descTextVw: UITextView!
    @IBOutlet var supoorterLabel: UILabel!
}
