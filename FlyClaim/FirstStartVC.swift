//
//  FirstStartVC.swift
//  Darious
//
//  Created by Dario Carrasco on 21/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

var createDict : NSMutableDictionary = [:]

class FirstStartVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet var topLabel: UILabel!
    @IBOutlet var shortLabel: UILabel!
    @IBOutlet var shortMsgLabel: UILabel!
    @IBOutlet var focusLabel: UILabel!
    @IBOutlet var focusMsgLabel: UILabel!
    @IBOutlet var communicateLbl: UILabel!
    @IBOutlet var communicateMsgLbl: UILabel!
    
    @IBOutlet var continueBtn: KGHighLightedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setLanguage()
        setBottomView()
        createDict = ["title" : "", "signature" : "", "decision" : "", "problem" : ""]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "start_a_petition")
        shortLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "keep_it_short_and_to_the_point")
        shortMsgLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "keep_it_short_and_to_the_point_sub_text")
        focusLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "focus_on_the_solution")
        focusMsgLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "focus_on_the_solution_sub_text")
        communicateLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "communicate_urgency")
        communicateMsgLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "communicate_urgency_sub_text")
        
        continueBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    @IBAction func onContinue(_ sender: Any)
    {
        let firstvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FirstStepVC") as! FirstStepVC
        present(firstvc, animated: true, completion: nil)
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
}
