//
//  ThirdStepVC.swift
//  Darious
//
//  Created by Dario Carrasco on 21/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class ThirdStepVC: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet var contentVw: UIView!
    @IBOutlet var contentVwHt: NSLayoutConstraint!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet var problemTV: customTextView!
    @IBOutlet var problemLbl: UILabel!
    @IBOutlet var msgLabel: UILabel!
    @IBOutlet var continueBtn: KGHighLightedButton!
    
    var contentFinalHt : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setLanguage()
        setBottomView()
        contentFinalHt = contentVwHt.constant
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        gesture.delegate = self
        contentVw.addGestureRecognizer(gesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        contentVwHt.constant = contentFinalHt - 100
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        problemLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "explain_the_problem_to_solve")
        msgLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "explain_the_problem_to_solve_sub_text")
        problemTV.text = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_here") + ".."
     continueBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
    }
    
    // Mark : Gesture Action
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        contentVw.endEditing(true)
        contentVwHt.constant = contentFinalHt
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    @IBAction func onContinue(_ sender: Any)
    {
        if (problemTV.text == (ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_here") + ".."))
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_explain_problem_first"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            createDict.setValue("\(problemTV.text!)", forKey: "problem")
            let fourthvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FourthStepVC") as! FourthStepVC
            present(fourthvc, animated: true, completion: nil)
        }
    }
    
    @IBAction func onFCHome(_ sender: Any)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        createDict.setValue("", forKey: "problem")
        self.dismiss(animated: false, completion: nil)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
}

extension ThirdStepVC : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = UIColor.black
        contentVwHt.constant = contentFinalHt + 100
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if(text == "") && (textView.text.count == 1)
        {
            let newPosition = textView.beginningOfDocument
            textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
            textView.text = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_here") + ".."
            //ApiResponse.getLanguageFromUserDefaults(inputString: "write_here")
            textView.textColor = UIColor.darkGray
            return false
        }
        else if textView.textColor == UIColor.darkGray {
            textView.text = ""
            textView.textColor = UIColor.black
            return true
        }
        else {
            return true
        }
    }
}
