//
//  FCHomeVC.swift
//  Darious
//
//  Created by Dario Carrasco on 20/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FCHomeVC: UIViewController {

    @IBOutlet weak var getTableview : UITableView!
    
    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    // Table Header Outlets
    @IBOutlet weak var headerVw : UIView!
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var takingLabel : UILabel!
    @IBOutlet weak var startBtn : KGHighLightedButton!
    @IBOutlet weak var backBtn : UIButton!
    @IBOutlet weak var fcHomeBtn : UIButton!
    @IBOutlet weak var petcollectionVw : UICollectionView!
    @IBOutlet weak var happenLabel : UILabel!
    
    var topArray : [NSDictionary] = []
    var bottomArray : [NSDictionary] = []
    var userCount : NSNumber = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FCBlackColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        getTableview.rowHeight = UITableView.automaticDimension
        getTableview.estimatedRowHeight = 180
    
        setBottomView()
        setHeaderViewData()
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.GetAll_Petition_URL, parameters: params, check: "get")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fc")
    }
    
    func setHeaderViewData()
    {
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "world_platform_for_change")
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            takingLabel.font = UIFont(name: "GandhiSans-Regular", size: 12)
        }
        else {
            takingLabel.font = UIFont(name: "GandhiSans-Regular", size: 13)
        }
        takingLabel.numberOfLines = 2
        happenLabel.text = "    \(ApiResponse.getLanguageFromUserDefaults(inputString: "whats_happening"))"
        startBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "start_a_petition")), for: .normal)
        
        startBtn.addTarget(self, action: #selector(startPetition(_:)), for: .touchUpInside)
        backBtn.addTarget(self, action: #selector(goDariusHome(_:)), for: .touchUpInside)
        fcHomeBtn.addTarget(self, action: #selector(reloadHome(_:)), for: .touchUpInside)
    }
    
    @objc func startPetition(_ sender : UIButton)
    {
        let firststartvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FirstStartVC") as! FirstStartVC
        present(firststartvc, animated: false, completion: nil)
    }
    
    @objc func goDariusHome(_ sender : UIButton)
    {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
    }
    
    @objc func reloadHome(_ sender : UIButton)
    {
        let homevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCHomeVC") as! FCHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
                
                self.userCount = result["userCount"] as! NSNumber
                self.takingLabel.text = "\(self.userCount) \(ApiResponse.getLanguageFromUserDefaults(inputString: "people_taking_action"))"
                if let bottomdata = result["dataTop"] as? [NSDictionary]
                {
                    self.bottomArray = bottomdata
                }
                self.getTableview.reloadData()
                
                if let topdata = result["data"] as? [NSDictionary]
                {
                    self.topArray = topdata
                    //  let fcell = self.getTableview.cellForRow(at: IndexPath(row: 0, section: 0)) as! GetFirstTableCell
                    self.petcollectionVw.dataSource = self
                    self.petcollectionVw.delegate = self
                    self.petcollectionVw.isHidden = false
                    self.petcollectionVw.reloadData()
                }
                customLoader.hideIndicator()
            }
        }
    }

    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyclaimBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    // Mark : Get video id from You tube URL
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
}

// MARK : TableView Delegates
extension FCHomeVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return bottomArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = getTableview.dequeueReusableCell(withIdentifier: "getsecondcell") as! GetSecondTableCell
        
        let dict = bottomArray[indexPath.section]
        
        cell.upperLabel.text = dict["petition_title"] as? String
        cell.descTextVw.text = dict["explain_the_problem"] as? String
        cell.LowerLabel.text = "\(dict["Total"] as! NSNumber) \(ApiResponse.getLanguageFromUserDefaults(inputString: "supportors"))"
        
        DispatchQueue.main.async {
            if (dict["photo_or_video"] as! String != "")
            {
                if (dict["photo_or_video"] as! String).contains(".mp4")
                {
                    cell.imageVw.sd_setImage(with: nil, placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                }
                else {
                    cell.imageVw.sd_setImage(with: URL(string: "\(Api.Petition_Image_URL)\(dict["photo_or_video"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                }
                cell.playImgVw.isHidden = true
                cell.blackVw.isHidden = true
            }
            else if (dict["photo_or_video"] as! String == "") && (dict["facebook_link"] as! String != "") {
                // show thumbnail
                let vid = self.getYoutubeId(youtubeUrl: dict["facebook_link"] as! String)
                cell.imageVw.sd_setImage(with: URL(string: "http://img.youtube.com/vi/\(vid!)/0.jpg"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                cell.playImgVw.isHidden = false
                cell.blackVw.isHidden = false
            }
            else {
                cell.imageVw.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                cell.playImgVw.isHidden = true
                cell.blackVw.isHidden = true
            }
        }
        cell.blackVw.layer.cornerRadius = 12
        cell.imageVw.layer.masksToBounds = true
        cell.imageVw.layer.cornerRadius = 12
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = bottomArray[indexPath.section]
        let detailvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "PetitionDetailsVC") as! PetitionDetailsVC
        detailvc.petitionId = dict["id"] as! NSNumber
        present(detailvc, animated: false, completion: nil)
    }
}

class GetSecondTableCell: UITableViewCell {
    
    @IBOutlet weak var imageVw : UIImageView!
    @IBOutlet weak var upperLabel : UILabel!
    @IBOutlet var descTextVw: UITextView!
    @IBOutlet weak var LowerLabel : UILabel!
    @IBOutlet weak var playImgVw : UIImageView!
    @IBOutlet weak var blackVw : UIView!
}

// MARK : CollectionView Delegates
extension FCHomeVC : UICollectionViewDataSource, UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return topArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = petcollectionVw.dequeueReusableCell(withReuseIdentifier: "gettopcell", for: indexPath) as! GetCollectionCell
        
        let dict = topArray[indexPath.item]
        cell.textLabel.text = dict["petition_title"] as? String
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            cell.textLabel.font = UIFont(name: "GandhiSans-Regular", size: 12)
        }
        else if (self.view.frame.width == 375) {
            cell.textLabel.font = UIFont(name: "GandhiSans-Regular", size: 14)
        }
        else {
            cell.textLabel.font = UIFont(name: "GandhiSans-Regular", size: 16)
        }
        
        DispatchQueue.main.async {
            if (dict["photo_or_video"] as! String != "")
            {
                if (dict["photo_or_video"] as! String).contains(".mp4")
                {
                    cell.imageVw.sd_setImage(with: nil, placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                }
                else {
                    cell.imageVw.sd_setImage(with: URL(string: "\(Api.Petition_Image_URL)\(dict["photo_or_video"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                }
                cell.playImgVw.isHidden = true
                cell.blackVw.isHidden = true
            }
            else if (dict["photo_or_video"] as! String == "") && (dict["facebook_link"] as! String != "") {
                // show thumbnail
                let vid = self.getYoutubeId(youtubeUrl: dict["facebook_link"] as! String)
                cell.imageVw.sd_setImage(with: URL(string: "http://img.youtube.com/vi/\(vid!)/0.jpg"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                cell.playImgVw.isHidden = false
                cell.blackVw.isHidden = false
            }
            else {
                cell.imageVw.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
                cell.playImgVw.isHidden = true
                cell.blackVw.isHidden = true
            }
        }
        cell.blackVw.layer.cornerRadius = 12
        cell.imageVw.layer.cornerRadius = 12
        cell.imageVw.layer.masksToBounds = true
        
        cell.imageVw.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        cell.imageVw.layer.shadowColor = UIColor.lightGray.cgColor
        cell.imageVw.layer.shadowOpacity = 0.8
        cell.imageVw.layer.shadowRadius = 4
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = topArray[indexPath.item]
        let detailvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "PetitionDetailsVC") as! PetitionDetailsVC
        detailvc.petitionId = dict["id"] as! NSNumber
        present(detailvc, animated: false, completion: nil)
    }
}
class GetCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageVw : UIImageView!
    @IBOutlet weak var textLabel : UILabel!
    @IBOutlet weak var playImgVw : UIImageView!
    @IBOutlet weak var blackVw : UIView!
}
