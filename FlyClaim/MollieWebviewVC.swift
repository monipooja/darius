//
//  MollieWebviewVC.swift
//  Darious
//
//  Created by Dario Carrasco on 28/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import WebKit

//MARK: step 1 Add Protocol here
protocol MollieWebVCDelegate: class {
    func callDidAppear()
}


class MollieWebviewVC : UIViewController , WKNavigationDelegate, WKUIDelegate {
    
    var webView: WKWebView!
    var checkouturl : String = ""
    var callbackUrl : String = ""
    var transid : String = ""
    var comeFrom : String = ""
    
    weak var delegate: MollieWebVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        LoadWkWebView()
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func LoadWkWebView()
    {
        webView = WKWebView.init(frame: view.bounds)
        let url = URL(string: checkouturl)!
        webView.load(URLRequest(url: url))
        
        webView.navigationDelegate = self
        webView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        view.addSubview(webView)
        webView.allowsBackForwardNavigationGestures = true
    }
    
    //// WKWeb View Delegates and Methods
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        
        if navigationAction.navigationType == .linkActivated
        {
            customLoader.hideIndicator()
            let urlCheck = navigationAction.request.url
          //  print("link....... ==== >\(urlCheck!)")
            decisionHandler(.cancel)
            
            if ("\(urlCheck!)" == "https://sandbox.safecharge.com/home")
            {
                if (comeFrom == "donate_petition")
                {
                   // donate_status = "done"
                }
                else {
                   // payment_status = "done"
                }
            }else {
                if (comeFrom == "donate_petition")
                {
                   // donate_status = "failed"
                }
                else {
                   // payment_status = "failed"
                }
            }
            
            if #available(iOS 13, *)
            {
                delegate?.callDidAppear()
                self.dismiss(animated: false, completion:nil)
            }
            else {
                self.dismiss(animated: false, completion: nil)
            }
        } else {
            let urlCheck = navigationAction.request.url
         //   print("checValue Url ==== >\(urlCheck!)")
            
            customLoader.hideIndicator()
//            payment_status = "failed"
//            donate_status = "failed"
            if "\(urlCheck!)" == "https://ppp-test.safecharge.com/ppp/back.do" {
                if #available(iOS 13, *)
                {
                    delegate?.callDidAppear()
                    self.dismiss(animated: false, completion:nil)
                }
                else {
                    self.dismiss(animated: false, completion: nil)
                }
            }
            decisionHandler(.allow)
        }
    }
}

