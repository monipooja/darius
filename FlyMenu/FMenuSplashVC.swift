//
//  FMenuSplashVC.swift
//  Darious
//
//  Created by Apple on 19/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

var customerPlan : String = ""
var trailValue : Bool!

class FMenuSplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        
        DispatchQueue.main.async {
            goToHome(controller: self)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}

public func goToHome(controller : UIViewController)
{
    let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
    ApiResponse.onPostPhp(url: Api.FMenu_Splash_URL, parms: params, controller: controller) { (result) in
        
        OperationQueue.main.addOperation {
          //  print("menu splash = \(result)")
            trailValue = result["trial"] as? Bool
            if let dict = result["data"] as? NSDictionary
            {
                if (dict == [:])
                {
                    let homevc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuHomeVC") as! FMenuHomeVC
                    controller.present(homevc, animated: false, completion: nil)
                }
                else {
                    customerPlan = dict["customerPlan"] as! String
                    let homevc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuTodayVC") as! FMenuTodayVC
                    
                    var secarray : [String] = []
                    var rowsarr : [[String]] = []
                    var lunch = dict["lunch"] as! String
                    if (lunch != "")
                    {
                        lunch = lunch.replacingOccurrences(of: "##", with: "#")
                        let disharr = lunch.split(separator: "#").map(String.init)
                        secarray.append("Lunch")
                        rowsarr.append(disharr)
                    }
                    var dinner = dict["dinner"] as! String
                    if (dinner != "")
                    {
                        dinner = dinner.replacingOccurrences(of: "##", with: "#")
                        let disharr = dinner.split(separator: "#").map(String.init)
                        secarray.append("Dinner")
                        rowsarr.append(disharr)
                    }
                    homevc.sectionArray = secarray
                    homevc.rowsArray = rowsarr
                    controller.present(homevc, animated: false, completion: nil)
                }
            }
        }
    }
}

extension UIImageView {

    func setDashBorderWithColor(colorStr : String, crad : Int)
    {
        let layer = CAShapeLayer()
        let bounds = self.bounds
        layer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: crad, height: crad)).cgPath
        layer.strokeColor = UIColor.init(hexString: colorStr).cgColor
        layer.fillColor = nil
        layer.lineDashPattern = [3, 3]
        self.layer.addSublayer(layer)
    }
}

class SubscriptionData : NSObject {
    var package : String = ""
    var members : String = ""
    var daysType : String = ""
    var foodType : String = ""
    var amount : String = ""
    var commission : String = ""
    var startDate : String = ""
    var address_id : String = ""
    var customer_type : String = ""
}
