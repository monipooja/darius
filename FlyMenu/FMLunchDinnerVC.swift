//
//  FMLunchDinnerVC.swift
//  Darious
//
//  Created by Apple on 18/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FMLunchDinnerVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var dashImgCollection : [UIImageView]!
    @IBOutlet weak var topImageView : UIImageView!
    @IBOutlet weak var topImgWd : NSLayoutConstraint!
    @IBOutlet weak var topImgHt : NSLayoutConstraint!
    
    @IBOutlet weak var topTitleLabel : UILabel!
    @IBOutlet weak var circleLabel : UILabel!
    @IBOutlet weak var packageLabel : UILabel!
    @IBOutlet weak var lunchLabel : UILabel!
    @IBOutlet weak var dinnerLabel : UILabel!
    @IBOutlet weak var lunchPrice : UILabel!
    @IBOutlet weak var dinnerPrice : UILabel!
    
    var layoutCount : Int = 0
    
    var titleStr : String = ""
    var packageStr : String = ""
    var titleColor : String = ""
    var firstText : String = ""
    var secondText : String = ""
    var cellbounds : CGSize = CGSize(width: 0, height: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        if (self.view.frame.width == 320) {
            topTitleLabel.font = UIFont.init(name: "OpenSans-Bold", size: 18)
        }
        
        for img in dashImgCollection
        {
            img.tintColor = UIColor.init(hexString: titleColor)
        }
        
        topTitleLabel.text = titleStr
        packageLabel.text = packageStr.capitalized
        lunchLabel.text = firstText
        dinnerLabel.text = secondText
        
        circleLabel.textColor = UIColor.init(hexString: titleColor)
        circleLabel.layer.cornerRadius = circleLabel.frame.width/2
        circleLabel.layer.borderWidth = 1.2
        circleLabel.layer.borderColor = UIColor.init(hexString: titleColor).cgColor
        
        setDataOnLaod(str: titleStr)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    override func viewDidLayoutSubviews() {
        
//        if (layoutCount == 1)
//        {
//            for img in dashImgCollection
//            {
//                img.image = nil
//                img.setDashBorderWithColor(colorStr : ColorCode.FMenuIndividColor, crad: 12)
//            }
//        }
//        layoutCount += 1
    }
    
    func setDataOnLaod(str : String)
    {
        switch str {
        case "Individual Menu":
            topImageView.image = #imageLiteral(resourceName: "IndividualMenu")
            topImgHt.constant = 48
            topImgWd.constant = 27
            if (packageStr == "1 Menu") {
                lunchPrice.text = "8€"
                dinnerPrice.text = "14€"
            } else {
                lunchPrice.text = "40€"
                dinnerPrice.text = "70€"
            }
            break
        case "Couple Menu":
            topImageView.image = #imageLiteral(resourceName: "CoupleMenu")
            topImgHt.constant = 47
            topImgWd.constant = 53
            if (packageStr == "1 Menu") {
                lunchPrice.text = "16€"
                dinnerPrice.text = "28€"
            } else {
                lunchPrice.text = "80€"
                dinnerPrice.text = "140€"
            }
            break
        case "Small Family Menu":
            topImageView.image = #imageLiteral(resourceName: "SmallFamily")
            topImgHt.constant = 47
            topImgWd.constant = 53
            if (packageStr == "1 Menu") {
                lunchPrice.text = "24€"
                dinnerPrice.text = "42€"
            } else {
                lunchPrice.text = "120€"
                dinnerPrice.text = "210€"
            }
            break
        case "Medium Family Menu":
            topImageView.image = #imageLiteral(resourceName: "MediumFamily")
            topImgHt.constant = 44
            topImgWd.constant = 71
            if (packageStr == "1 Menu") {
                lunchPrice.text = "32€"
                dinnerPrice.text = "56€"
            } else {
                lunchPrice.text = "160€"
                dinnerPrice.text = "280€"
            }
            break
        case "Big Family Menu":
            topImageView.image = #imageLiteral(resourceName: "ThemeGroup")
            topImgHt.constant = 44
            topImgWd.constant = 71
            if (packageStr == "1 Menu") {
                lunchPrice.text = "40€"
                dinnerPrice.text = "70€"
            } else {
                lunchPrice.text = "200€"
                dinnerPrice.text = "350€"
            }
            break
        default:
            break
        }
    }
    
    @IBAction func selectLunch(_ sender: Any)
    {
        subsData.daysType = "1"
        subsData.foodType = "1"
        subsData.amount = (lunchPrice.text?.replacingOccurrences(of: "\(Current_Currency)", with: ""))!
        proceedToPayment()
    }
    
    @IBAction func selectLunchDinner(_ sender: Any)
    {
        subsData.daysType = ""
        subsData.foodType = "2"
        subsData.amount = (dinnerPrice.text?.replacingOccurrences(of: "\(Current_Currency)", with: ""))!
        proceedToPayment()
    }
    
    func proceedToPayment()
    {
        let paymentvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuPaymentVC") as! FMenuPaymentVC
        present(paymentvc, animated: false, completion: nil)
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        goToHome(controller: self)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fmenu")
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Address
            let addressvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuAddressVC") as! FMenuAddressVC
            present(addressvc, animated: false, completion: nil)
            break
        case 2:
            // My Plan
            let myplanvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuMyPlanVC") as! FMenuMyPlanVC
            present(myplanvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuWalletVC") as! FMenuWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuNotificationVC") as! FMenuNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
    
}
