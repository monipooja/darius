//
//  FMenuMyPlanVC.swift
//  Darious
//
//  Created by Apple on 20/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

var next_payment_date : String = ""
var reuqest_plan_id : NSNumber = 0
var current_plan_price : Float = 0
var start_payment_date : String = ""

class FMenuMyPlanVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var contentVwHt : NSLayoutConstraint!
    @IBOutlet weak var planTableView : UITableView!
    @IBOutlet weak var listAddTableView : UITableView!
    
    @IBOutlet weak var healthyLabel : UILabel!
    @IBOutlet weak var changeLabel : UILabel!
    @IBOutlet weak var upperImg : UIImageView!
    
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var changeAddLabel : UILabel!
    
    var planArray : [NSDictionary] = []
    var rowsHtArray : [CGFloat] = []
    var addressArray : [NSDictionary] = []
    
    var selectedSection : Int?
    var finalContentHt : CGFloat = 0
    var btmVwBool : Bool = false
    var selectedIndex : IndexPath!
    var selectedAddress : String = ""
    var selectedPlanId : NSNumber = 0
    var planTableWd : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        
        finalContentHt = contentVwHt.constant
        next_payment_date = ""
        reuqest_plan_id = 0
        current_plan_price = 0
        start_payment_date = ""
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        if (btmVwBool == false)
        {
            setBottomView()
        }
        
        let headerNib = UINib.init(nibName: "PlanHeaderView", bundle: Bundle.main)
        planTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "PlanHeaderView")
        
        planTableView.rowHeight = UITableView.automaticDimension
        planTableView.estimatedRowHeight = 230
        
        listAddTableView.rowHeight = UITableView.automaticDimension
        listAddTableView.estimatedRowHeight = 75
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FMenuThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FMenu_PlanDetails_URL, params: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        planTableView.reloadData()
        planTableWd = planTableView.bounds.width
    }
    
    @IBAction func addPlan(_ sender: Any)
    {
        let homevc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuHomeVC") as! FMenuHomeVC
        present(homevc, animated: false, completion: nil)
    }

    @IBAction func onShare(_ sender: Any)
    {
    }
    
    @IBAction func hideListAddTable(_ sender: Any)
    {
        self.containerView.isHidden = true
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        goToHome(controller: self)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK : Call Service
    func callService(urlstr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: params, controller: self) { (result) in
            
         //   print("menu plan \(check) status = \(result)")
            OperationQueue.main.addOperation
            {
                var msg = result["message"] as! String
                msg = ApiResponse.getLanguageFromUserDefaults(inputString: msg)
                
                if (check == "get")
                {
                    if let dictArr = result["data"] as? [NSDictionary]
                    {
                        for dict in dictArr
                        {
                            self.planArray.append(dict)
                            self.rowsHtArray.append(0)
                        }
                        self.planTableView.isHidden = false
                        self.planTableView.reloadData()
                        self.contentVwHt.constant = (self.finalContentHt - self.planTableView.frame.height + 50) + CGFloat((dictArr.count * 50))
                        self.finalContentHt = (self.finalContentHt - self.planTableView.frame.height + 50) + CGFloat((dictArr.count * 50))
                    }
                    customLoader.hideIndicator()
                }
                else if (check == "add") {
                    if let dictArr = result["data"] as? [NSDictionary]
                    {
                        for dict in dictArr
                        {
                            self.addressArray.append(dict)
                        }
                        self.containerView.isHidden = false
                        self.listAddTableView.reloadData()
                    }
                    else {
                        ApiResponse.alert(title: "", message: "You have no address", controller: self)
                    }
                    customLoader.hideIndicator()
                }
                else if (check == "addactive") {
                    var message = result["message"] as! String
                    message = ApiResponse.getLanguageFromUserDefaults(inputString: message)
                    StaticFunctions.showAlert(title: "", message: message, actions: [OkText], controller: self, completion: { (str) in
                        
                        self.addressArray = []
                        self.containerView.isHidden = true
                        self.listAddTableView.reloadData()
                        
                        let cell = self.planTableView.cellForRow(at: self.selectedIndex) as! MenuPlanTableCell
                        cell.addressLabel.text = "Address Name : \(self.selectedAddress)"
                    })
                }
                else {
                    StaticFunctions.showAlert(title: "", message: msg, actions: [OkText], controller: self) { (str) in
                        if (check == "pause")
                        {
                            customerPlan = "pause"
                        }
                        else if (check == "active")
                        {
                            customerPlan = "active"
                            goToHome(controller: self)
                        }
                        else if (check == "cancel") {
                            customerPlan = ""
                            self.planArray = []
                            self.rowsHtArray = []
                            self.viewDidLoad()
                        }else{
                            customLoader.hideIndicator()
                        }
                    }
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        btmVwBool = true
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fmenu")
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Address
            let addressvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuAddressVC") as! FMenuAddressVC
            present(addressvc, animated: false, completion: nil)
            break
        case 2:
            // My Plan
            let myplanvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuMyPlanVC") as! FMenuMyPlanVC
            present(myplanvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuWalletVC") as! FMenuWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuNotificationVC") as! FMenuNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
    
    func getDaySuffix(day : Int) -> String {

        switch (day % 10) {
            
        case 1:  return "st";
        case 2:  return "nd";
        case 3:  return "rd";
        default: return "th";
        }
    }
    
    // MARK : Check plan status
    func checkPlanStatus(cell : MenuPlanTableCell, startDate : String, nextDate : String)
    {
        switch customerPlan {
        case "active":
            cell.pauseButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuLightYellow)
            cell.pauseButton.setTitle("Pause Plan", for: .normal)
            cell.msgLabel.text = "Your plan started on \(startDate).\nNext payment will be on \(nextDate)"
            break
        case "pause":
            cell.pauseButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
            cell.pauseButton.setTitle("Reactivate Plan", for: .normal)
            cell.msgLabel.text = "Your plan started on \(startDate).\nYour plan is paused"
            break
        default:
            break
        }
    }

}

extension FMenuMyPlanVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if (tableView == planTableView)
        {
            return planArray.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (tableView == planTableView)
        {
            return 1
        }
        return addressArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (tableView == planTableView)
        {
            let cell = planTableView.dequeueReusableCell(withIdentifier: "menuplancell", for: indexPath) as! MenuPlanTableCell
            
            let dict = planArray[indexPath.section]
            
            customerPlan = dict["status"] as! String
            
            let day1 = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd")!
            let dayWithSuffix1 = getDaySuffix(day: Int(day1)!)
            let start = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "MMMM")! + " \(day1)\(dayWithSuffix1) of " + (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "yyyy")!
            
            if (dict["next_payment_date"] as! String != "0000-00-00 00:00:00")
            {
                let day2 = (dict["next_payment_date"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd")!
                let dayWithSuffix2 = getDaySuffix(day: Int(day2)!)
                let next = (dict["next_payment_date"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "MMMM")! + " \(day2)\(dayWithSuffix2) of " + (dict["next_payment_date"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "yyyy")!
                
                checkPlanStatus(cell: cell, startDate: start, nextDate: next)
            }
            else {
                checkPlanStatus(cell: cell, startDate: start, nextDate: "")
            }
            
            cell.addressLabel.text = "Address Name : \(dict["address_title"] as! String)"
            
            cell.mainvw.layer.cornerRadius = 4
            
            cell.pauseButton.addTarget(self, action: #selector(pausePlan(_:)), for: .touchUpInside)
            cell.changeButton.addTarget(self, action: #selector(changePlan(_:)), for: .touchUpInside)
            cell.addressButton.addTarget(self, action: #selector(changeAddress(_:)), for: .touchUpInside)
            cell.cancelButton.addTarget(self, action: #selector(cancelPlan(_:)), for: .touchUpInside)
            
            return cell
        }
        else {
            let cell = listAddTableView.dequeueReusableCell(withIdentifier: "addplancell", for: indexPath) as! ListAddTableCell
            
            let dict = addressArray[indexPath.row]
            
            cell.titleLabel.text = "\(dict["address_title"] as! String)"
            cell.addressLabel.text = dict["address"] as? String
            
            let topBorder = CALayer()
            topBorder.backgroundColor = UIColor.lightGray.cgColor
            topBorder.frame = CGRect(x:5, y: cell.contentView.frame.height-1, width: listAddTableView.frame.width-5, height:0.5)
            cell.contentView.layer.addSublayer(topBorder)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (tableView == listAddTableView) {
            
            let dict = addressArray[indexPath.row]
            selectedAddress = dict["address_title"] as! String
            
            customLoader.showActivityIndicator(showColor: ColorCode.FMenuThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&address_id=\(dict["id"] as! NSNumber)&plan_id=\(selectedPlanId)"
            self.callService(urlstr: Api.FMenu_ActiveAdd_URL, params: params, check: "addactive")
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (tableView == planTableView) {
            let cell = cell as! MenuPlanTableCell
            if let layer = cell.dashBorderImg.layer.sublayers as? [CAShapeLayer]
            {
                layer[0].removeFromSuperlayer()
            }
            cell.dashBorderImg.bounds.size = CGSize(width: planTableWd-31, height: cell.dashBorderImg.bounds.height)
            cell.dashBorderImg.setDashBorderWithColor(colorStr: ColorCode.FMenuGray, crad: 0)
        }
        else {
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (tableView == planTableView) {
            return rowsHtArray[indexPath.section]
        }
        else {
            return tableView.estimatedRowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if (tableView == planTableView) {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "PlanHeaderView") as! PlanHeaderView
            
            let dict = planArray[section]
            
            headerView.menuTitleLbl.text = "\(dict["food_type"] as! String) \(dict["days_type"] as! String) (\(dict["package"] as! String))"
            headerView.priceLabel.text = "\(String(format: "%.2f", Double(dict["amount"] as! String)!)) \(Current_Currency)"
            
            if (dict["status"] as! String == "pause")
            {
                headerView.trayImg.image = #imageLiteral(resourceName: "PauseIcon")
            }
            else {
                headerView.trayImg.image = UIImage.init(imageLiteralResourceName: "tray")
            }
            headerView.mainView.layer.cornerRadius = 4
            
            if let layer = headerView.dashBorderImg.layer.sublayers as? [CAShapeLayer]
            {
                layer[0].removeFromSuperlayer()
                headerView.dashBorderImg.bounds.size = CGSize(width: planTableWd-10, height: headerView.dashBorderImg.bounds.height)
                headerView.dashBorderImg.setDashBorderWithColor(colorStr: ColorCode.FMenuGray, crad: 0)
            } else {
                headerView.dashBorderImg.bounds.size = CGSize(width: planTableWd-10, height: headerView.dashBorderImg.bounds.height)
                headerView.dashBorderImg.setDashBorderWithColor(colorStr: ColorCode.FMenuGray, crad: 0)
            }
            
            if let _ = selectedSection
            {
                if (rowsHtArray[selectedSection!] == 0) {
                    headerView.ddImg.tintColor = UIColor.white
                    headerView.trayImg.tintColor = UIColor.white
                }
                else {
                    headerView.ddImg.tintColor = UIColor.init(hexString: ColorCode.FMenuGray)
                    headerView.trayImg.tintColor = UIColor.init(hexString: ColorCode.FMenuGray)
                }
            }
            else {
                headerView.ddImg.tintColor = UIColor.init(hexString: ColorCode.FMenuGray)
                headerView.trayImg.tintColor = UIColor.init(hexString: ColorCode.FMenuGray)
            }
            headerView.dropdownBtn.tag = section
            headerView.clickBtn.tag = section
            headerView.dropdownBtn.addTarget(self, action: #selector(openRow(_:)), for: .touchUpInside)
            headerView.clickBtn.addTarget(self, action: #selector(openRow(_:)), for: .touchUpInside)
            
            return headerView
            
        }
        else {
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (tableView == planTableView) {
            return 50
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: planTableView.frame.width, height: 10))
        return vw
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (tableView == planTableView) {
            return 10
        }
        else {
            return 0
        }
    }
    
    @objc func openRow(_ sender : UIButton)
    {
        selectedSection = sender.tag
        
        let idxpath = IndexPath(row: 0, section: sender.tag)
        let cell = planTableView.cellForRow(at: idxpath) as! MenuPlanTableCell
        let headerView = planTableView.headerView(forSection: idxpath.section) as! PlanHeaderView
    
        if (rowsHtArray[selectedSection!] == 0)
        {
            rowsHtArray[selectedSection!] = cell.cellhtConstraint.constant + 5.5
            var addHt : CGFloat = 0
            for ht in rowsHtArray
            {
                addHt = addHt + ht
            }
            contentVwHt.constant = finalContentHt + addHt
            
            headerView.mainView.backgroundColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
            if let layer = headerView.dashBorderImg.layer.sublayers as? [CAShapeLayer]
            {
                layer[0].strokeColor = UIColor.init(hexString: "ffffff").cgColor
            }
            headerView.menuTitleLbl.textColor = UIColor.white
            headerView.priceLabel.textColor = UIColor.white
            headerView.ddImg.tintColor = UIColor.white
            headerView.trayImg.tintColor = UIColor.white
        }
        else {
            rowsHtArray[selectedSection!] = 0
            var addHt : CGFloat = 0
            for ht in rowsHtArray
            {
                addHt = addHt + ht
            }
            contentVwHt.constant = finalContentHt + addHt
            
            headerView.mainView.backgroundColor = UIColor.init(hexString: ColorCode.FMenuLightGreen)
            if let layer = headerView.dashBorderImg.layer.sublayers as? [CAShapeLayer]
            {
                layer[0].strokeColor = UIColor.init(hexString: ColorCode.FMenuGray).cgColor
            }
            headerView.menuTitleLbl.textColor = UIColor.init(hexString: ColorCode.FMenuGray)
            headerView.priceLabel.textColor = UIColor.init(hexString: ColorCode.FMenuGray)
            headerView.ddImg.tintColor = UIColor.init(hexString: ColorCode.FMenuGray)
            headerView.trayImg.tintColor = UIColor.init(hexString: ColorCode.FMenuGray)
        }
        planTableView.reloadRows(at: [IndexPath(row: 0, section: selectedSection!)], with: .none)
    }
    
    @objc func pausePlan(_ sender: UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: planTableView)
        let idxpath = planTableView.indexPathForRow(at: point)
        let cell = planTableView.cellForRow(at: idxpath!) as! MenuPlanTableCell
        
        let header = planTableView.headerView(forSection: sender.tag) as! PlanHeaderView
        
        let dict = planArray[(idxpath?.section)!]
        self.selectedIndex = idxpath!
        
        var statusStr : String = ""
        var msgStr : String = ""
        if (cell.pauseButton.backgroundColor == UIColor.init(hexString: ColorCode.FMenuThemeColor))
        {
            statusStr = "active"
            msgStr = "Do you want to reactivate this plan"
        }
        else {
            statusStr = "pause"
            msgStr = "Do you want to pause this plan"
        }
        StaticFunctions.showAlert(title: "", message: msgStr, actions: ["Yes", "No"], controller: self) { (str) in
            
            if (str == "Yes")
            {
                if (cell.pauseButton.backgroundColor == UIColor.init(hexString: ColorCode.FMenuThemeColor))
                {
                    header.trayImg.image = UIImage.init(imageLiteralResourceName: "tray")
                    header.mainView.backgroundColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
                    cell.pauseButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuLightYellow)
                    cell.pauseButton.setTitle("Pause Plan", for: .normal)
                    
                    let day1 = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd")!
                    let dayWithSuffix1 = self.getDaySuffix(day: Int(day1)!)
                    let start = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "MMMM")! + " \(day1)\(dayWithSuffix1) of " + (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "yyyy")!
                    
                    let day2 = (dict["next_payment_date"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd")!
                    let dayWithSuffix2 = self.getDaySuffix(day: Int(day2)!)
                    let next = (dict["next_payment_date"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "MMMM")! + " \(day2)\(dayWithSuffix2) of " + (dict["next_payment_date"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "yyyy")!
                    cell.msgLabel.text = "Your plan started on \(start).\nNext payment will be on \(next)"
                }
                else {
                    header.trayImg.image = #imageLiteral(resourceName: "PauseIcon")
                    header.mainView.backgroundColor = UIColor.init(hexString: ColorCode.FMenuPauseGreen)
                    cell.pauseButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
                    cell.pauseButton.setTitle("Reactivate Plan", for: .normal)
                    
                    let day1 = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd")!
                    let dayWithSuffix1 = self.getDaySuffix(day: Int(day1)!)
                    let start = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "MMMM")! + " \(day1)\(dayWithSuffix1) of " + (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "yyyy")!
                    cell.msgLabel.text = "Your plan started on \(start).\nYour plan is paused"
                }
                
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&status=\(statusStr)&plan_id=\(dict["id"] as! NSNumber)"
                self.callService(urlstr: Api.FMenu_StatusOfPlan_URL, params: params, check: statusStr)
            }
            else {
            }
        }
    }
    
    @objc func changePlan(_ sender: UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: planTableView)
        let idxpath = planTableView.indexPathForRow(at: point)
        let dict = planArray[(idxpath?.section)!]
        
        StaticFunctions.showAlert(title: "", message: "Do you want to change the plan", actions: ["Yes", "No"], controller: self) { (str) in
            if (str == "Yes")
            {
                next_payment_date = dict["next_payment_date"] as! String
                reuqest_plan_id = dict["id"] as! NSNumber
                current_plan_price = Float.init(dict["amount"] as! String)!
                start_payment_date = dict["created_on"] as! String

                let changevc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuChangePlanVC") as! FMenuChangePlanVC
                self.present(changevc, animated: false, completion: nil)
            }
        }
    }
    
    @objc func changeAddress(_ sender: UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: planTableView)
        let idxpath = planTableView.indexPathForRow(at: point)
        
        StaticFunctions.showAlert(title: "", message: "Do you want to change the address", actions: ["Yes", "No"], controller: self) { (str) in
            if (str == "Yes")
            {
                self.selectedIndex = idxpath!
                self.selectedPlanId = (self.planArray[(idxpath?.section)!])["id"] as! NSNumber
                
              //  customLoader.showActivityIndicator(showColor: ColorCode.FMenuThemeColor, controller: self.view)
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
                self.callService(urlstr: Api.FMenu_GetAddress_URL, params: params, check: "add")
            }
        }
    }
    
    @objc func cancelPlan(_ sender: UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: planTableView)
        let idxpath = planTableView.indexPathForRow(at: point)
        
        let dict = planArray[(idxpath?.section)!]
        
        StaticFunctions.showAlert(title: "", message: "Do you want to cancel this plan", actions: ["Yes", "No"], controller: self) { (str) in
            if (str == "Yes")
            {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&status=cancel&plan_id=\(dict["id"] as! NSNumber)"
                self.callService(urlstr: Api.FMenu_StatusOfPlan_URL, params: params, check: "cancel")
            }
        }
    }
}

class MenuPlanTableCell: UITableViewCell {
    
    @IBOutlet weak var mainvw : UIView!
    @IBOutlet weak var msgLabel : UILabel!
    @IBOutlet weak var addressLabel : UILabel!
    @IBOutlet weak var dashBorderImg : UIImageView!
    
    @IBOutlet weak var cellhtConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var changeButton : KGHighLightedButton!
    @IBOutlet weak var addressButton : KGHighLightedButton!
    @IBOutlet weak var pauseButton : KGHighLightedButton!
    @IBOutlet weak var cancelButton : KGHighLightedButton!
}

class ListAddTableCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var addressLabel : UILabel!
}
