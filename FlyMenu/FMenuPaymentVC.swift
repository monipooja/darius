//
//  FMenuPaymentVC.swift
//  Darious
//
//  Created by Apple on 25/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import CoreLocation

class FMenuPaymentVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var addressTableVw : UITableView!
    @IBOutlet weak var addressBtn : KGHighLightedButton!
    @IBOutlet weak var paymentBtn : KGHighLightedButton!
    
    //Show data View
    @IBOutlet weak var dataView : UIView!
    @IBOutlet var leftLabelCollection: [UILabel]!
    @IBOutlet var valueLabelCollection: [UILabel]!
    
    // Center view for adding address
    @IBOutlet var labelCollection: [UILabel]!
    @IBOutlet var TextFieldCollection: [UITextField]!
    @IBOutlet weak var centreVwHorizontal : NSLayoutConstraint!
    @IBOutlet weak var centreVwHt : NSLayoutConstraint!
    @IBOutlet weak var submitBtn : KGHighLightedButton!
    @IBOutlet weak var blurView : UIView!
    @IBOutlet weak var centerView : UIView!
    @IBOutlet weak var centreImgVw: UIImageView!
    
    var addressArray : [NSDictionary] = []
    var datePicker : UIDatePicker?
    var doneButton : UIButton = UIButton()
    var transactionId : String = ""
    var btmVwBool : Bool = false
    var addressStr : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        if (btmVwBool == false)
        {
            setBottomView()
            setDataOnViewDidLoad()
        }
        
        if next_payment_date != ""
        {
            if (Float(subsData.amount)! < current_plan_price)
            {
                paymentBtn.backgroundColor = UIColor.init(hexString: ColorCode.FMenuBigColor)
                paymentBtn.setTitle("Change Plan", for: .normal)
            }
            else {
                subsData.amount = "\(calculatePriceOnChangePlan())"
                paymentBtn.setTitle("Proceed To Payment", for: .normal)
            }
        }
        else {
            paymentBtn.setTitle("Proceed To Payment", for: .normal)
        }
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FMenuThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlStr: Api.FMenu_GetAddress_URL, params: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        for tf in TextFieldCollection
        {
            tf.borderStyle = .none
            addBottomLayerWithColor(label: nil, textF: tf)
        }
        centerView.layer.cornerRadius = 8
        if let layer = centreImgVw.layer.sublayers as? [CAShapeLayer]
        {
            layer[0].removeFromSuperlayer()
        }
        centreImgVw.setDashBorderWithColor(colorStr : ColorCode.FMenuGray, crad: 8)
        
        for lbl in valueLabelCollection
        {
            addBottomLayerWithColor(label: lbl, textF: nil)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
        
        if (transactionId != "")
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
            ApiResponse.onResponseGetMollie(url: "https://api.mollie.com/v2/payments/\(transactionId)") { (result, error) in
                // print("mollie trans result = \(result)")
                
                OperationQueue.main.addOperation {
                    customLoader.hideIndicator()
                    if (error == "")
                    {
                        let status = "\(result["status"] ?? "")"
                        self.transactionId = ""
                        if status == "canceled" || status == "failed" || status == "open"{
                            StaticFunctions.showAlert(title: "Payment error", message: "Subscription not done", actions: [OkText], controller: self, completion: { (str) in
                                goToHome(controller: self)
                            })
                        } else {
                            if (status == "paid") {
                                StaticFunctions.showAlert(title: "Payment done", message: "Successfully subscribed", actions: [OkText], controller: self, completion: { (str) in
                                    goToHome(controller: self)
                                })
                            }
                        }
                    }
                    else{}
                }
            }
        }
    }
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        centreVwHorizontal.constant = 0
        self.view.endEditing(true)
    }
    
    func setDataOnViewDidLoad()
    {
        addPickerView()
        
        addressTableVw.rowHeight = UITableView.automaticDimension
        addressTableVw.estimatedRowHeight = 70
        addressTableVw.layer.cornerRadius = 8
        addressTableVw.layer.borderColor = UIColor.lightGray.cgColor
        addressTableVw.layer.borderWidth = 0.8
        
        dataView.layer.cornerRadius = 4
        dataView.layer.shadowColor = UIColor.lightGray.cgColor
        dataView.layer.shadowRadius = 3
        dataView.layer.shadowOpacity = 0.6
        dataView.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        blurView.addGestureRecognizer(gesture)
    }
    
    // MARK : Calculate price after chnage plan
    func calculatePriceOnChangePlan() -> Float
    {
        let str1 = start_payment_date.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "yyyy MM dd")
        let dateA = str1!.toDate(format: "yyyy MM dd")
        let str2 = next_payment_date.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "yyyy MM dd")
        let dateB = str2!.toDate(format: "yyyy MM dd")
        
        let totalDays = Calendar.current.dateComponents([.day], from: dateA!, to: dateB!).day
        
        let todayDate = Date()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd"
        let currentDay = dateformatter.string(from: todayDate)
        
        let remainingday = totalDays! - Int(currentDay)!
      //  print("dayyyyy = \(totalDays!)--------\(currentDay)-------\(remainingday)")
        
        let minusPrc = (current_plan_price/Float(totalDays!)) * Float(remainingday)
        
        let payPrice = Float(subsData.amount)! - minusPrc
        
    //    print("pay price = \(payPrice)")
        return payPrice
    }
    
    @IBAction func addAddress(_ sender : Any)
    {
        blurView.isHidden = false
        centerView.isHidden = false
    }
    
    @IBAction func submitAddress(_ sender: Any)
    {
        if (TextFieldCollection[0].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter address home", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (TextFieldCollection[1].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter street", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (TextFieldCollection[1].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter postal code", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (TextFieldCollection[1].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter city", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (TextFieldCollection[1].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter province", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (TextFieldCollection[1].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter country", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            
            customLoader.showActivityIndicator(showColor: ColorCode.FMenuThemeColor, controller: self.view)
            self.getCoordinateFromAddress(address: "\(TextFieldCollection[1].text!) \(TextFieldCollection[3].text!), \(TextFieldCollection[4].text!) , \(TextFieldCollection[5].text!) \(TextFieldCollection[2].text!)")
        }
    }
    
    @IBAction func hideCenterView(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
        for tf in TextFieldCollection
        {
            tf.text = ""
        }
    }
    
    @IBAction func doPayment(_ sender : Any)
    {
        if (paymentBtn.titleLabel?.text == "Change Plan")
        {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&address=\(subsData.address_id)&request_id=\(reuqest_plan_id)&date=\(next_payment_date)&package=\(subsData.package)&members=\(subsData.members)&days_type=\(subsData.daysType)&food_type=\(subsData.foodType)&amount=\(subsData.amount)"
            callService(urlStr: Api.FMenu_ChangePlan_URL, params: params, check: "change")
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FMenuThemeColor, controller: self.view)
            
            if (subsData.customer_type == "")
            {
                let params : [String : Any] = ["name" :"\(userData["first_name"] as! String) \(userData["last_name"] as! String)", "email" : ""]
                ApiResponse.onResponsePostMollie2(url: "https://api.mollie.com/v2/customers", parms: params) { (result, error) in
                    // print("create mollie customer = \(result)")
                    OperationQueue.main.addOperation {
                        
                        let amtstr = self.valueLabelCollection[8].text?.replacingOccurrences(of: ",", with: ".").replacingOccurrences(of: "\(Current_Currency)", with: "").trimmingCharacters(in: .whitespacesAndNewlines)
                        self.molliePayment(payment: "\(amtstr!)", cid: result["id"] as! String)
                    }
                }
            }
            else {
                let amtstr = self.valueLabelCollection[8].text?.replacingOccurrences(of: ",", with: ".").replacingOccurrences(of: "\(Current_Currency)", with: "").trimmingCharacters(in: .whitespacesAndNewlines)
                self.molliePayment(payment: "\(amtstr!)", cid: "")
            }
        }
    }

    @IBAction func onFMenuHome(_ sender: Any)
    {
        goToHome(controller: self)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    func openDetailsView()
    {
        dataView.isHidden = false
        paymentBtn.isHidden = false
        topLabel.text = ""
        
        valueLabelCollection[0].text = subsData.package
        valueLabelCollection[1].text = subsData.members
        
        switch subsData.foodType {
        case "1":
            valueLabelCollection[2].text = "Lunch"
            break
        case "2":
            valueLabelCollection[2].text = "Lunch & Dinner"
            break
        default:
            break
        }
        
        switch subsData.daysType {
        case "1":
            valueLabelCollection[3].text = "Mon-Fri"
            break
        case "2":
            valueLabelCollection[3].text = "Mon-Sun"
            break
        default:
            break
        }
        
        if (next_payment_date == "")
        {
            valueLabelCollection[5].text = subsData.startDate
        } else {
            valueLabelCollection[5].text = next_payment_date.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM yyyy")
        }
        calculateAndSetCommission()
    }
    
    func calculateAndSetCommission()
    {
        var amountstr : String = ""
        if (subsData.amount.contains(",")) {
            amountstr = (subsData.amount.replacingOccurrences(of: ",", with: "."))
        }
        else {
            amountstr = subsData.amount
        }
        let total_comsn = ((Float(amountstr)!) * (Float(commission_value)!/100)) + Float(commission_Card)!
        subsData.commission = "\(total_comsn)"
        
        if (subsData.amount.contains(".") == true)
        {
            let sp = subsData.amount.split(separator: ".")
            if (sp.count == 1)
            {
                valueLabelCollection[6].text = StaticFunctions.priceFormatSet(prcValue: "\(subsData.amount)") + "0 \(Current_Currency)"
            }
            else {
                valueLabelCollection[6].text = String(format: "%.2f", Double(subsData.amount)!) + " \(Current_Currency)"
            }
        }
        else if (subsData.amount.contains(",") == true) {
            let sp = subsData.amount.split(separator: ",")
            if (sp.count == 1)
            {
                valueLabelCollection[6].text = StaticFunctions.priceFormatSet(prcValue: "\(subsData.amount)") + "0 \(Current_Currency)"
            }
            else {
                valueLabelCollection[6].text = StaticFunctions.priceFormatSet(prcValue: "\(subsData.amount)") + "0 \(Current_Currency)"
            }
        }
        else {
            
            valueLabelCollection[6].text = "\(subsData.amount).00 \(Current_Currency)"
            
//            if (subsData.amount.contains(".") == false) && (subsData.amount.contains(",") == false)
//            {
//                valueLabelCollection[6].text = "\(subsData.amount).00 \(Current_Currency)"
//            }
//            else if (subsData.amount.contains(".") == true)
//            {
//                valueLabelCollection[6].text = String(format: "%.2f", Double(subsData.amount)!) + " \(Current_Currency)"
//            }else {
//                valueLabelCollection[6].text = StaticFunctions.priceFormatSet(prcValue: "\(subsData.amount)") + " \(Current_Currency)"
//            }
        }
        valueLabelCollection[7].text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", total_comsn)) + " \(Current_Currency)"
        valueLabelCollection[8].text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", Float(amountstr)! + total_comsn)) + " \(Current_Currency)"
    }
    
    // MARK : Geocoding (address to coordinate)
    func getCoordinateFromAddress(address : String)
    {
        //        "Bholaram Ustad Marg, Indra Puri Colony, Indrapuri Colony, Bhanwar Kuwa, Indore, Madhya Pradesh 452014"
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            if let placemk = placemarks?.first {
                let location = placemk.location
                let lattitude = (location?.coordinate.latitude)!
                let longitude = (location?.coordinate.longitude)!
                //  print("location = \((location?.coordinate)!)")
                
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&address_title=\(self.TextFieldCollection[0].text!)&street=\(self.TextFieldCollection[1].text!)&postal_code=\(self.TextFieldCollection[2].text!)&city=\(self.TextFieldCollection[3].text!)&province=\(self.TextFieldCollection[4].text!)&country=\(self.TextFieldCollection[5].text!)&lat=\(lattitude)&long=\(longitude)"
                self.callService(urlStr: Api.FMenu_AddEditAdd_URL, params: params, check: "add")
            }
            else {
                // print("location = \(error)")
                let showmsg = ApiResponse.getLanguageFromUserDefaults(inputString: "invalid_address")
                ApiResponse.alert(title: "", message: showmsg, controller: self)
            }
        }
    }
    
    // Mollie Payment Service Call
    func molliePayment(payment : String, cid : String)
    {
        let amountDict : NSDictionary = ["currency" : "EUR", "value" : payment]
        let date = Date()
        let formatter = DateFormatter.init()
        formatter.dateFormat = "yyyyMMdd"
        let datestr = formatter.string(from: date)
        let metaDict : NSDictionary = ["uinique_id" : "\(datestr)\(user_id)", "package" : subsData.package, "members" : subsData.members, "days_type" : subsData.daysType, "food_type" : subsData.foodType, "amount" : subsData.amount, "start_date" : subsData.startDate, "commission" : subsData.commission, "address_id" : subsData.address_id, "customer_type" : subsData.customer_type]
        
        var params : [String : Any] = [:]
        if (cid == "")
        {
            params = ["metadata" : metaDict, "amount" : amountDict, "description" : "\(ApiResponse.getLanguageFromUserDefaults(inputString: "subscription_business"))", "customerId" : "\(cid)", "redirectUrl" : "http://www.myapp.com/", "webhookUrl" : "\(Api.Base_URL)flymenu/webhook_for_payment/\(user_id)"]
        }
        else {
            params = ["metadata" : metaDict, "amount" : amountDict, "description" : "\(ApiResponse.getLanguageFromUserDefaults(inputString: "subscription_business"))", "customerId" : "\(cid)", "sequenceType" : "first", "redirectUrl" : "http://www.myapp.com/", "webhookUrl" : "\(Api.Base_URL)flymenu/webhook_for_payment/\(user_id)"]
        }
       
        ApiResponse.onResponsePostMollie2(url: "https://api.mollie.com/v2/payments", parms: params) { (result, error) in
            // print("mollie payment result = \(result)")
            
            OperationQueue.main.addOperation {
                if let _ = result["_links"] as? NSDictionary {
                    
                    let checkoutUrl = ((result["_links"] as! NSDictionary)["checkout"] as! NSDictionary)["href"] as! String
                    //  let callbackUrl = ((result["_links"] as! NSDictionary)["self"] as! NSDictionary)["href"] as! String
                    self.transactionId = result["id"] as! String
                    
                    customLoader.hideIndicator()
                    
                    let webvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "MollieWebviewVC") as! MollieWebviewVC
                    webvc.checkouturl = checkoutUrl
                    webvc.transid = result["id"] as! String
                    webvc.comeFrom = "flymenu"
                    self.present(webvc, animated: false, completion: nil)
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    // MARK : Open picker for date
    func addPickerView()
    {
        datePicker = UIDatePicker.init(frame: CGRect(x: 0, y: self.view.frame.height-200, width: self.view.frame.width, height: 200))
        datePicker?.datePickerMode = .date
        datePicker?.backgroundColor = UIColor.init(hexString: ColorCode.AddBgColor)
        datePicker?.tintColor = UIColor.black
        datePicker?.minimumDate = Date().dayAfter
        self.view.addSubview(datePicker!)
        datePicker?.isHidden = true
        
        if (useLang == "spanish") {
            datePicker?.locale = Locale.init(identifier: "es")
        }
        
        //Done Button
        doneButton = UIButton.init(frame: CGRect(x: 0, y: self.view.frame.height-240, width: self.view.frame.width, height: 40))
        doneButton.titleLabel?.textAlignment = .center
        doneButton.setTitleColor(UIColor.init(hexString: ColorCode.FMenuThemeColor), for: .normal)
        doneButton.backgroundColor = UIColor.init(hexString: ColorCode.AddBgColor)
        doneButton.titleLabel?.font = UIFont.init(name: "OpenSans-Semibold", size: 17)
        doneButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "common_done_ios"), for: .normal)
        
        doneButton.addTarget(self, action: #selector(donedatePicker(_:)), for: .touchUpInside)
        self.view.addSubview(doneButton)
        doneButton.isHidden = true
        
        doneButton.layer.borderColor = UIColor.darkGray.cgColor
        doneButton.layer.borderWidth = 0.5
    }
    @objc func donedatePicker(_ sender : UIButton)
    {
        datePicker?.isHidden = true
        sender.isHidden = true
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let dobStr = formatter.string(from: (datePicker?.date)!).toDateString(inputFormat: "dd/MM/yyyy", outputFormat: "yyyy-MM-dd")!
        self.view.endEditing(true)
        subsData.startDate = "\(dobStr)"
        
        openDetailsView()
    }
    
    // Add Bottom Border on textfield
    func addBottomLayerWithColor(label : UILabel?, textF :UITextField?)
    {
        if let _ = label
        {
            let border = CALayer()
            border.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray).cgColor
            border.frame = CGRect(x:0, y:label!.frame.height-2, width:label!.frame.width, height:0.5)
            if let layer = label!.layer.sublayers
            {
                layer[0].removeFromSuperlayer()
            }
            label!.layer.addSublayer(border)
        }
        else {
            let border = CALayer()
            border.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray).cgColor
            border.frame = CGRect(x:0, y:textF!.frame.height-2, width:textF!.frame.width, height:0.5)
            if let layer = textF!.layer.sublayers
            {
                layer[0].removeFromSuperlayer()
            }
            textF!.layer.addSublayer(border)
        }
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
             //   print("menu \(check) address = \(result)")
                if (check == "get")
                {
                    if let dictArr = result["data"] as? [NSDictionary]
                    {
                        for dict in dictArr
                        {
                            self.addressArray.append(dict)
                        }
                        if (self.addressArray.count > 0){
                            self.topLabel.text = "Please choose the address for delivery"
                            self.addressBtn.isHidden = true
                            self.addressTableVw.isHidden = false
                            self.addressTableVw.reloadData()
                        }
                        else {
                            self.addressBtn.isHidden = false
                            self.addressTableVw.isHidden = true
                        }
                    }
                    else {
                    }
                    customLoader.hideIndicator()
                }
                else if (check == "change") {
                    var message = result["message"] as! String
                    message = ApiResponse.getLanguageFromUserDefaults(inputString: message)
                    StaticFunctions.showAlert(title: "", message: message, actions: [OkText], controller: self, completion: { (str) in
                       
                        goToHome(controller: self)
                    })
                }
                else
                {
                    var message = result["message"] as! String
                    message = ApiResponse.getLanguageFromUserDefaults(inputString: message)
                    StaticFunctions.showAlert(title: "", message: message, actions: [OkText], controller: self, completion: { (str) in
                        
                        self.blurView.isHidden = true
                        self.centerView.isHidden = true
                        self.viewDidLoad()
                    })
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fmenu")
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Address
            let addressvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuAddressVC") as! FMenuAddressVC
            present(addressvc, animated: false, completion: nil)
            break
        case 2:
            // My Plan
            let myplanvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuMyPlanVC") as! FMenuMyPlanVC
            present(myplanvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuWalletVC") as! FMenuWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuNotificationVC") as! FMenuNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }

}

extension FMenuPaymentVC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        centreVwHorizontal.constant = -25
    }
}

extension FMenuPaymentVC : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = addressTableVw.dequeueReusableCell(withIdentifier: "listaddcell", for: indexPath) as! ListAddressCell
        
        let dict = addressArray[indexPath.row]
        
        cell.titleLabel.text = "\(dict["address_title"] as! String)"
        cell.addressLabel.text = dict["address"] as? String
        
        let topBorder = CALayer()
        topBorder.backgroundColor = UIColor.lightGray.cgColor
        topBorder.frame = CGRect(x:5, y: cell.contentView.frame.height+1, width: addressTableVw.frame.width-5, height:0.5)
        cell.contentView.layer.addSublayer(topBorder)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = addressArray[indexPath.row]
        
        topLabel.text = "Please select date for start the payment"
        valueLabelCollection[4].text = dict["address_title"] as? String
        subsData.address_id = "\(dict["id"] as! NSNumber)"
        addressStr = (dict["address"] as? String)!
        addressTableVw.isHidden = true
        if (next_payment_date == "")
        {
            datePicker?.isHidden = false
            doneButton.isHidden = false
        }
        else {
            openDetailsView()
        }
    }
}

class ListAddressCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var addressLabel : UILabel!
    @IBOutlet weak var vw : UIView!
}
