//
//  FMenuAddressVC.swift
//  Darious
//
//  Created by Apple on 22/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import CoreLocation

class FMenuAddressVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var addressTableview : UITableView!
    @IBOutlet var labelCollection: [UILabel]!
    @IBOutlet var TextFieldCollection: [UITextField]!
    
    @IBOutlet weak var centreVwHorizontal : NSLayoutConstraint!
    @IBOutlet weak var myAddLabel : UILabel!
    @IBOutlet weak var addBtn : KGHighLightedButton!
    @IBOutlet weak var submitBtn : KGHighLightedButton!
    @IBOutlet weak var blurView : UIView!
    @IBOutlet weak var centerView : UIView!
    @IBOutlet weak var centreImgVw: UIImageView!
    
    var addressArray : [NSDictionary] = []
    var rowsHtArray : [CGFloat] = []
    var addOrEdit : String = ""
    var selectedSection : Int?
    var lattitude : Double = 0
    var longitude : Double = 0
    var editAddId : NSNumber = 0
    var checkbool : Bool = false
    var btmVwBool : Bool = false
    var addTableWd : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        if (btmVwBool == false)
        {
            setBottomView()
        }
        centerView.layer.cornerRadius = 8
    
        let headerNib = UINib.init(nibName: "headerView", bundle: Bundle.main)
        addressTableview.register(headerNib, forHeaderFooterViewReuseIdentifier: "headerView")
        addressTableview.rowHeight = UITableView.automaticDimension
        addressTableview.estimatedRowHeight = 262
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        blurView.addGestureRecognizer(gesture)
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FMenuThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlStr: Api.FMenu_GetAddress_URL, params: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addressTableview.reloadData()
        addTableWd = addressTableview.frame.width
        for tf in TextFieldCollection
        {
            tf.borderStyle = .none
            addBottomLayerWithColor(textF: tf)
        }
        if let layer = centreImgVw.layer.sublayers as? [CAShapeLayer]
        {
            layer[0].removeFromSuperlayer()
        }
        centreImgVw.setDashBorderWithColor(colorStr : ColorCode.FMenuGray, crad: 8)
    }
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        centreVwHorizontal.constant = 0
        self.view.endEditing(true)
    }
    
    @IBAction func onAddFirstAddress(_ sender: Any)
    {
        addOrEdit = "add"
        blurView.isHidden = false
        centerView.isHidden = false
    }
    
    @IBAction func submitAddress(_ sender: Any)
    {
        if (TextFieldCollection[0].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter address home", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (TextFieldCollection[1].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter street", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (TextFieldCollection[1].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter postal code", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (TextFieldCollection[1].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter city", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (TextFieldCollection[1].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter province", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (TextFieldCollection[1].text == "")
        {
            ApiResponse.bottomCustomAlert(message: "Please enter country", controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            
            customLoader.showActivityIndicator(showColor: ColorCode.FMenuThemeColor, controller: self.view)
            self.getCoordinateFromAddress(address: "\(TextFieldCollection[1].text!) \(TextFieldCollection[3].text!), \(TextFieldCollection[4].text!) , \(TextFieldCollection[5].text!) \(TextFieldCollection[2].text!)")
        }
    }
    
    @IBAction func hideCenterView(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
        for tf in TextFieldCollection
        {
            tf.text = ""
        }
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        goToHome(controller: self)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        btmVwBool = true
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fmenu")
    }
    
    // Add Bottom Border on textfield
    func addBottomLayerWithColor(textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray).cgColor
        border.frame = CGRect(x:0, y:textF.frame.height-2, width:textF.frame.width, height:0.5)
        if let layer = textF.layer.sublayers
        {
            layer[0].removeFromSuperlayer()
        }
        textF.layer.addSublayer(border)
    }
    
    // MARK : Geocoding (address to coordinate)
    func getCoordinateFromAddress(address : String)
    {
      //  "Bholaram Ustad Marg, Indra Puri Colony, Indrapuri Colony, Bhanwar Kuwa, Indore, Madhya Pradesh 452014"
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            if let placemk = placemarks?.first {
                let location = placemk.location
                self.lattitude = (location?.coordinate.latitude)!
                self.longitude = (location?.coordinate.longitude)!
                //  print("location = \((location?.coordinate)!)")
                
                var params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&address_title=\(self.TextFieldCollection[0].text!)&street=\(self.TextFieldCollection[1].text!)&postal_code=\(self.TextFieldCollection[2].text!)&city=\(self.TextFieldCollection[3].text!)&province=\(self.TextFieldCollection[4].text!)&country=\(self.TextFieldCollection[5].text!)&lat=\(self.lattitude)&long=\(self.longitude)"
                
                var checkStr = "add"
                let paramsStr = "&address_id=\(self.editAddId)"
                if (self.addOrEdit == "edit") {
                    params = params + paramsStr
                    checkStr = "edit"
                }
              //  print("params = \(params)")
                self.callService(urlStr: Api.FMenu_AddEditAdd_URL, params: params, check: checkStr)
            }
            else {
                // print("location = \(error)")
                let showmsg = ApiResponse.getLanguageFromUserDefaults(inputString: "invalid_address")
                ApiResponse.alert(title: "", message: showmsg, controller: self)
            }
        }
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
             //   print("menu \(check) address = \(result)")
                if (check == "get")
                {
                    if let dictArr = result["data"] as? [NSDictionary]
                    {
                        for dict in dictArr
                        {
                            self.addressArray.append(dict)
                            self.rowsHtArray.append(0)
                        }
                        self.addressTableview.reloadData()
                        self.addressTableview.isHidden = false
                    }
                    customLoader.hideIndicator()
                }
                else
                {
                    var message = result["message"] as! String
                    message = ApiResponse.getLanguageFromUserDefaults(inputString: message)
                    StaticFunctions.showAlert(title: "", message: message, actions: [OkText], controller: self, completion: { (str) in
                        
                        self.addressTableview.isHidden = true
                        self.addressArray = []
                        self.rowsHtArray = []
                        self.blurView.isHidden = true
                        self.centerView.isHidden = true
                        self.viewDidLoad()
                    })
                }
            }
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Address
            let addressvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuAddressVC") as! FMenuAddressVC
            present(addressvc, animated: false, completion: nil)
            break
        case 2:
            // My Plan
            let myplanvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuMyPlanVC") as! FMenuMyPlanVC
            present(myplanvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuWalletVC") as! FMenuWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuNotificationVC") as! FMenuNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMenuAddressVC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        centreVwHorizontal.constant = -25
    }
}

extension FMenuAddressVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return addressArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = addressTableview.dequeueReusableCell(withIdentifier: "menuaddcell", for: indexPath) as! MenuAddressTableCell
        
        let dict = addressArray[indexPath.section]
        
        cell.TextFieldCollection[0].text = dict["address_title"] as? String
        cell.TextFieldCollection[1].text = dict["street"] as? String
        cell.TextFieldCollection[2].text = dict["postal_code"] as? String
        cell.TextFieldCollection[3].text = dict["city"] as? String
        cell.TextFieldCollection[4].text = dict["province"] as? String
        cell.TextFieldCollection[5].text = dict["country"] as? String
        
        let foodarr = (dict["food_type"] as! String).split(separator: ",")
        let daysarr = (dict["days_type"] as! String).split(separator: ",")
        let packarr = (dict["package"] as! String).split(separator: ",")
        
        if !foodarr.isEmpty
        {
            var planstr : String = ""
            for i in 0...foodarr.count-1 {
                planstr = planstr + "\(foodarr[i]) \(daysarr[i]) (\(packarr[i]))"
                if (foodarr.count > 1) {
                    planstr = planstr + ", "
                }
            }
            cell.planLabel.text = planstr
        }
        else {
            cell.planLabel.text = "Not Assigned"
        }
        cell.mainvw.layer.cornerRadius = 4
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (indexPath.section >= 0)
        {
            let cell = cell as! MenuAddressTableCell
            
            for tf in cell.TextFieldCollection
            {
                tf.borderStyle = .none
                addBottomLayerWithColor(textF: tf)
            }
            
            if let layer = cell.dashBorderImg.layer.sublayers as? [CAShapeLayer]
            {
                layer[0].removeFromSuperlayer()
            }
            cell.dashBorderImg.bounds = CGRect(origin: cell.dashBorderImg.frame.origin, size: CGSize(width: cell.dashBorderImg.frame.width, height: cell.bounds.size.height-15))
            cell.dashBorderImg.setDashBorderWithColor(colorStr: ColorCode.FMenuGray, crad: 0)
            
            if let layer = cell.planLabel.layer.sublayers
            {
                layer[0].removeFromSuperlayer()
            }
            let border = CALayer()
            border.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray).cgColor
            border.frame = CGRect(x:0, y: cell.planLabel.frame.height-0.5, width:cell.planLabel.frame.width, height:0.5)
            if let layer = cell.planLabel.layer.sublayers
            {
                layer[0].removeFromSuperlayer()
            }
            cell.planLabel.layer.addSublayer(border)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowsHtArray[indexPath.section]
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let dict = addressArray[section]
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerView") as! headerView
        
        headerView.mainView.layer.cornerRadius = 4
        headerView.titleLabel.text = dict["address_title"] as? String
        
        if (dict["selected"] as! String == "0")
        {
            headerView.mainView.backgroundColor = UIColor.init(hexString: ColorCode.AddBgColor)
            if let layer = headerView.dashBorderImg.layer.sublayers as? [CAShapeLayer]
            {
                layer[0].removeFromSuperlayer()
                headerView.dashBorderImg.bounds.size = CGSize(width: addTableWd-10, height: headerView.dashBorderImg.bounds.height)
                headerView.dashBorderImg.setDashBorderWithColor(colorStr: ColorCode.FMenuBorderGreen, crad: 0)
            }
            else {
                headerView.dashBorderImg.bounds.size = CGSize(width: addTableWd-10, height: headerView.dashBorderImg.bounds.height)
                headerView.dashBorderImg.setDashBorderWithColor(colorStr: ColorCode.FMenuBorderGreen, crad: 0)
            }
            headerView.titleLabel.textColor = UIColor.init(hexString: ColorCode.FMenuGray)
            headerView.ddImage.tintColor = UIColor.init(hexString: ColorCode.FMenuGray)
            headerView.editIcon.tintColor = UIColor.init(hexString: ColorCode.FMenuGray)
            headerView.addIcon.image = UIImage.init(imageLiteralResourceName: "GrayMenuAdd")
        }
        else {
            headerView.mainView.backgroundColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
            if let layer = headerView.dashBorderImg.layer.sublayers as? [CAShapeLayer]
            {
                layer[0].removeFromSuperlayer()
                headerView.dashBorderImg.bounds.size = CGSize(width: addTableWd-10, height: headerView.dashBorderImg.bounds.height)
                headerView.dashBorderImg.setDashBorderWithColor(colorStr: "ffffff", crad: 0)
            }
            else {
                headerView.dashBorderImg.bounds.size = CGSize(width: addTableWd-10, height: headerView.dashBorderImg.bounds.height)
                headerView.dashBorderImg.setDashBorderWithColor(colorStr: "ffffff", crad: 0)
            }
            headerView.titleLabel.textColor = UIColor.white
            headerView.ddImage.tintColor = UIColor.white
            headerView.editIcon.tintColor = UIColor.white
            headerView.addIcon.image = UIImage.init(imageLiteralResourceName: "menuAddress")
        }
        headerView.clickBtn.tag = section
        headerView.clickBtn.addTarget(self, action: #selector(openRow(_:)), for: .touchUpInside)
        headerView.dropdownBtn.tag = section
        headerView.dropdownBtn.addTarget(self, action: #selector(openRow(_:)), for: .touchUpInside)
        headerView.editBtn.tag = section
        headerView.editBtn.addTarget(self, action: #selector(editAddress(_:)), for: .touchUpInside)
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: addressTableview.frame.width, height: 10))
        return vw
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func openRow(_ sender : UIButton)
    {
        selectedSection = sender.tag
        if (rowsHtArray[selectedSection!] == 0)
        {
            rowsHtArray[selectedSection!] = addressTableview.rowHeight
        }
        else {
            rowsHtArray[selectedSection!] = 0
        }
        addressTableview.reloadRows(at: [IndexPath(row: 0, section: selectedSection!)], with: .none)
    }
    
    @objc func editAddress(_ sender : UIButton)
    {
        addOrEdit = "edit"
        blurView.isHidden = false
        centerView.isHidden = false
        
        let dict = addressArray[sender.tag]
        
        editAddId = (dict["id"] as? NSNumber)!
        TextFieldCollection[0].text = dict["address_title"] as? String
        TextFieldCollection[1].text = dict["street"] as? String
        TextFieldCollection[2].text = dict["postal_code"] as? String
        TextFieldCollection[3].text = dict["city"] as? String
        TextFieldCollection[4].text = dict["province"] as? String
        TextFieldCollection[5].text = dict["country"] as? String
    }
}

class MenuAddressTableCell: UITableViewCell {
    
    @IBOutlet weak var mainvw : UIView!
    @IBOutlet weak var dashBorderImg : UIImageView!
    @IBOutlet weak var planLabel : UILabel!
    
    @IBOutlet var labelCollection: [UILabel]!
    @IBOutlet var TextFieldCollection: [UITextField]!
}

