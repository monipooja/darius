//
//  FMenuVC.swift
//  Darious
//
//  Created by Apple on 20/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FMenuVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var menucollectionVw : UICollectionView!
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var trayImageVw : UIImageView!
    
    var titleStr : String = ""
    var titleColor : String = ""
    var cellbounds : CGSize = CGSize(width: 0, height: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        titleLabel.text = titleStr
        titleLabel.textColor = UIColor.init(hexString: titleColor)
        trayImageVw.tintColor = UIColor.init(hexString: titleColor)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        goToHome(controller: self)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
       StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fmenu")
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Address
            let addressvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuAddressVC") as! FMenuAddressVC
            present(addressvc, animated: false, completion: nil)
            break
        case 2:
            // My Plan
            let myplanvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuMyPlanVC") as! FMenuMyPlanVC
            present(myplanvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuWalletVC") as! FMenuWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuNotificationVC") as! FMenuNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMenuVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = menucollectionVw.dequeueReusableCell(withReuseIdentifier: "menucell", for: indexPath) as! MenuCollectionCell
        
        switch indexPath.item {
        case 0:
            cell.titleLabel.text = "Individual\nMenu"
            cell.titleLabel.textColor = UIColor.init(hexString: ColorCode.FMenuIndividColor)
            cell.iconImageVw.image = #imageLiteral(resourceName: "IndividualMenu")
            cell.iconImgWd.constant = 17
            cell.iconImgHt.constant = 30
            cell.borderImageVw.bounds.size = CGSize(width: cellbounds.width-14, height: cellbounds.height-14)
            cell.borderImageVw.setDashBorderWithColor(colorStr: ColorCode.FMenuBorderGreen, crad: 12)
            break
        case 1:
            cell.titleLabel.text = "Couple\nMenu"
            cell.titleLabel.textColor = UIColor.init(hexString: ColorCode.FMenuCoupleColor)
            cell.iconImageVw.image = #imageLiteral(resourceName: "CoupleMenu")
            cell.iconImgWd.constant = 33
            cell.iconImgHt.constant = 30
            cell.borderImageVw.bounds.size = CGSize(width: cellbounds.width-14, height: cellbounds.height-14)
            cell.borderImageVw.setDashBorderWithColor(colorStr: ColorCode.FMenuCoupleColor, crad: 12)
            break
        case 2:
            if (self.view.frame.width == 320) {
                cell.titleLabel.text = "Small Family Menu"
                cell.titleLabel.font = UIFont.init(name: "OpenSans-Semibold", size: 11)
            }
            else {
                cell.titleLabel.text = "Small Family\nMenu"
            }
            cell.titleLabel.textColor = UIColor.init(hexString: ColorCode.FMenuSmallColor)
            cell.iconImageVw.image = #imageLiteral(resourceName: "SmallFamily")
            cell.iconImgWd.constant = 36
            cell.iconImgHt.constant = 32
            cell.borderImageVw.bounds.size = CGSize(width: cellbounds.width-14, height: cellbounds.height-14)
            cell.borderImageVw.setDashBorderWithColor(colorStr: ColorCode.FMenuSmallColor, crad: 12)
            break
        case 3:
            if (self.view.frame.width == 320) {
                cell.titleLabel.text = "Medium Family Menu"
                cell.titleLabel.font = UIFont.init(name: "OpenSans-Semibold", size: 10)
            }
            else {
                cell.titleLabel.text = "Medium Family\nMenu"
            }
            cell.titleLabel.textColor = UIColor.init(hexString: ColorCode.FMenuMediumColor)
            cell.iconImageVw.image = #imageLiteral(resourceName: "MediumFamily")
            cell.iconImgWd.constant = 51
            cell.iconImgHt.constant = 32
            cell.borderImageVw.bounds.size = CGSize(width: cellbounds.width-14, height: cellbounds.height-14)
            cell.borderImageVw.setDashBorderWithColor(colorStr: ColorCode.FMenuMediumColor, crad: 12)
            break
        case 4:
            cell.titleLabel.text = "Big Family\nMenu"
            cell.titleLabel.textColor = UIColor.init(hexString: ColorCode.FMenuBigColor)
            cell.iconImageVw.image = #imageLiteral(resourceName: "BigFamily")
            cell.iconImgWd.constant = 60
            cell.iconImgHt.constant = 35
            cell.mainVwLeading.constant = (menucollectionVw.frame.width - cellbounds.width)/2
            cell.mainVwTrailing.constant = (menucollectionVw.frame.width - cellbounds.width)/2
            cell.borderImageVw.bounds.size = CGSize(width: cellbounds.width-14, height: cellbounds.height-14)
            cell.borderImageVw.setDashBorderWithColor(colorStr: ColorCode.FMenuBigColor, crad: 12)
            break
        default:
            break
        }
        cell.borderImageVw.layer.cornerRadius = 8
        cell.borderImageVw.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            return 5
        }
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            let wd = (menucollectionVw.frame.width-5)/2
            cellbounds = CGSize(width: wd, height: 100)
            if indexPath.item == 4
            {
                return CGSize(width: menucollectionVw.frame.width, height: 100)
            }
            else {
                return CGSize(width: wd, height: 100)
            }
        }
        else {
            let wd = (menucollectionVw.frame.width-20)/2
            cellbounds = CGSize(width: wd, height: 108)
            if indexPath.item == 4
            {
                return CGSize(width: menucollectionVw.frame.width, height: 108)
            }
            else {
                return CGSize(width: wd, height: 108)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.item {
        case 0:
            if (titleStr == "1 Menu") || (titleStr == "5 Menu") {
                goToLunchdinner(passTitle: "Individual Menu")
            }
            else {
                let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuIndividualVC") as! FMenuIndividualVC
                subsData.members = "1"
                present(menuvc, animated: false, completion: nil)
            }
            break
        case 1:
            if (titleStr == "1 Menu") || (titleStr == "5 Menu") {
                goToLunchdinner(passTitle: "Couple Menu")
            }
            else {
                let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuCoupleVC") as! FMenuCoupleVC
                subsData.members = "2"
                present(menuvc, animated: false, completion: nil)
            }
            break
        case 2:
            if (titleStr == "1 Menu") || (titleStr == "5 Menu") {
                goToLunchdinner(passTitle: "Small Family Menu")
            }
            else {
                let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuSmallVC") as! FMenuSmallVC
                subsData.members = "3"
                present(menuvc, animated: false, completion: nil)
            }
            break
        case 3:
            if (titleStr == "1 Menu") || (titleStr == "5 Menu") {
                goToLunchdinner(passTitle: "Medium Family Menu")
            }
            else {
                let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuMediumVC") as! FMenuMediumVC
                subsData.members = "4"
                present(menuvc, animated: false, completion: nil)
            }
            break
        case 4:
            if (titleStr == "1 Menu") || (titleStr == "5 Menu") {
                goToLunchdinner(passTitle: "Big Family Menu")
            }
            else {
                let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuBigVC") as! FMenuBigVC
                subsData.members = "5"
                present(menuvc, animated: false, completion: nil)
            }
            break
        default:
            break
        }
    }
    
    func goToLunchdinner(passTitle : String)
    {
        let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMLunchDinnerVC") as! FMLunchDinnerVC
        menuvc.titleStr = passTitle
        menuvc.packageStr = self.titleLabel.text!
        menuvc.titleColor = titleColor
        if (titleStr == "1 Menu") {
            menuvc.firstText = "Lunch"
            menuvc.secondText = "Lunch & Dinner"
        }else {
            menuvc.firstText = "Lunch Mon-Fri"
            menuvc.secondText = "Lunch & Dinner Mon-Fri"
        }
        
        present(menuvc, animated: false, completion: nil)
    }
}

class MenuCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var iconImageVw : UIImageView!
    @IBOutlet weak var borderImageVw : UIImageView!
    
    @IBOutlet weak var mainVwLeading : NSLayoutConstraint!
    @IBOutlet weak var mainVwTrailing : NSLayoutConstraint!
    
    @IBOutlet weak var iconImgWd : NSLayoutConstraint!
    @IBOutlet weak var iconImgHt : NSLayoutConstraint!
    
}
