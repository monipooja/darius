//
//  PlanHeaderView.swift
//  Darious
//
//  Created by Apple on 24/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class PlanHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var menuTitleLbl : UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var trayImg : UIImageView!
    @IBOutlet weak var dashBorderImg : UIImageView!
    @IBOutlet weak var ddImg : UIImageView!
    @IBOutlet weak var dropdownBtn : UIButton!
    @IBOutlet weak var clickBtn : UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
