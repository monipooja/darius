

import UIKit


class headerView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var dashBorderImg : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var ddImage : UIImageView!
    @IBOutlet weak var addIcon : UIImageView!
    @IBOutlet weak var editIcon: UIImageView!
    @IBOutlet weak var clickBtn : UIButton!
    @IBOutlet weak var dropdownBtn : UIButton!
    @IBOutlet weak var editBtn : UIButton!
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
