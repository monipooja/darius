//
//  FMenuHomeVC.swift
//  Darious
//
//  Created by Apple on 19/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

var subsData : SubscriptionData!

class FMenuHomeVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var monFriVwHt : NSLayoutConstraint!
    @IBOutlet weak var monFriVw : CustomUIView!
    
    @IBOutlet var trayimgCollection : [UIImageView]!
    
    @IBOutlet weak var healthyLabel : UILabel!
    @IBOutlet weak var monFriLabel : UILabel!
    @IBOutlet weak var tomorrowLabel : UILabel!
    @IBOutlet weak var weeklyLabel : UILabel!
    @IBOutlet weak var monthlyLabel : UILabel!
    @IBOutlet weak var friImageVw : UIImageView!
    @IBOutlet weak var sunImageVw : UIImageView!
    @IBOutlet weak var weeklyImageVw : UIImageView!
    @IBOutlet weak var monthlyImageVw : UIImageView!
    
    @IBOutlet weak var workBtn : KGHighLightedButton!
    
    var layoutCount : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        
        subsData = SubscriptionData()
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        if (trailValue == false)
        {
            monFriVwHt.constant = 0
            monFriVw.isHidden = true
        }
        
        let traycolor = [ColorCode.FMenuCoupleColor, ColorCode.FMenuSmallColor, ColorCode.FMenuIndividColor, ColorCode.FMenuBigColor]
        var ii = 0
        for trayimg in trayimgCollection
        {
            trayimg.tintColor = UIColor.init(hexString: traycolor[ii])
            ii += 1
        }
        setBottomView()
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        if (layoutCount == 1)
        {
            friImageVw.setDashBorderWithColor(colorStr: ColorCode.FMenuCoupleColor, crad: 12)
            sunImageVw.setDashBorderWithColor(colorStr: ColorCode.FMenuSmallColor, crad: 12)
            weeklyImageVw.setDashBorderWithColor(colorStr: ColorCode.FMenuBorderGreen, crad: 12)
            monthlyImageVw.setDashBorderWithColor(colorStr: ColorCode.FMenuBigColor, crad: 12)
        }
        layoutCount += 1
    }
    
    @IBAction func howDoesItWork(_ sender: Any)
    {
        let workvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuHowWorkVC") as! FMenuHowWorkVC
        present(workvc, animated: false, completion: nil)
    }
    
    @IBAction func selectWeeklyMenu(_ sender: Any)
    {
        subsData.package = "weekly"
        let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuVC") as! FMenuVC
        menuvc.titleStr = "Weekly Menu"
        menuvc.titleColor = ColorCode.FMenuIndividColor
        present(menuvc, animated: false, completion: nil)
    }

    @IBAction func selectMonthlyMenu(_ sender: Any)
    {
        subsData.package = "monthly"
        let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuVC") as! FMenuVC
        menuvc.titleStr = "Monthly Menu"
        menuvc.titleColor = ColorCode.FMenuBigColor
        present(menuvc, animated: false, completion: nil)
    }
    
    @IBAction func tryMonFriMenu(_ sender: Any)
    {
        subsData.package = "1menu"
        let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuVC") as! FMenuVC
        menuvc.titleStr = "5 Menu"
        menuvc.titleColor = ColorCode.FMenuCoupleColor
        present(menuvc, animated: false, completion: nil)
    }
    
    @IBAction func tryMenuForTomorrow(_ sender: Any)
    {
        subsData.package = "5menu"
        let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuVC") as! FMenuVC
        menuvc.titleStr = "1 Menu"
        menuvc.titleColor = ColorCode.FMenuSmallColor
        present(menuvc, animated: false, completion: nil)
    }
    
    @IBAction func onShare(_ sender: Any)
    {
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        goToHome(controller: self)
    }
    
    @IBAction func ongoBackToDarius(_ sender: Any)
    {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fmenu")
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Address
            let addressvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuAddressVC") as! FMenuAddressVC
            present(addressvc, animated: false, completion: nil)
            break
        case 2:
            // My Plan
            let myplanvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuMyPlanVC") as! FMenuMyPlanVC
            present(myplanvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuWalletVC") as! FMenuWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuNotificationVC") as! FMenuNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}
