//
//  FMenuTodayVC.swift
//  Darious
//
//  Created by Apple on 22/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FMenuTodayVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var menuTableview : UITableView!
    @IBOutlet weak var tomorrowTableview : UITableView!
    
    @IBOutlet weak var healthyLabel : UILabel!
    @IBOutlet weak var menuTitleLbl : UILabel!
    @IBOutlet weak var menuTrayImg : UIImageView!
    
    @IBOutlet weak var todayButton : KGHighLightedButton!
    @IBOutlet weak var tomorrowButton : KGHighLightedButton!
    @IBOutlet weak var menuAButton : KGHighLightedButton!
    @IBOutlet weak var menuBButton : KGHighLightedButton!
    @IBOutlet weak var saveButton : KGHighLightedButton!
    
    @IBOutlet weak var circleNoLabel : UILabel!
    @IBOutlet weak var packageLbl : UILabel!
    @IBOutlet weak var dashImg : UIImageView!
    @IBOutlet weak var familyImg : UIImageView!
    @IBOutlet weak var tomTrayImg : UIImageView!
    
    @IBOutlet weak var menuDateLbl : UILabel!
    @IBOutlet weak var menuALbl : UILabel!
    @IBOutlet weak var menuBLbl : UILabel!
    @IBOutlet weak var countALbl : UILabel!
    @IBOutlet weak var countBLbl : UILabel!
    @IBOutlet weak var secondTitleLbl : UILabel!
    
    @IBOutlet weak var menuView : UIView!
    
    @IBOutlet weak var menuAWd : NSLayoutConstraint!
    @IBOutlet weak var menuBTraling : NSLayoutConstraint!
    @IBOutlet weak var menuViewHt : NSLayoutConstraint!
    @IBOutlet weak var familyImgWd : NSLayoutConstraint!
    @IBOutlet weak var familyImgHt : NSLayoutConstraint!
    
    var sectionArray : [String] = []
    var rowsArray : [[String]] = []
    
    var checkstr : String = "today"
    var countA : Int = 0
    var countB : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        menuTrayImg.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        menuTableview.rowHeight = UITableView.automaticDimension
        menuTableview.estimatedRowHeight = 25
        setBottomView()
        
        selectTodayMenu(todayButton)
        
        setDataOnLaod(str: "Individual Menu")
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    func setDataOnLaod(str : String)
    {
        switch str {
        case "Individual Menu":
            familyImg.image = #imageLiteral(resourceName: "IndividualMenu")
            familyImgHt.constant = 48
            familyImgWd.constant = 27
            circleNoLabel.layer.borderColor = UIColor.init(hexString: ColorCode.FMenuIndividColor).cgColor
            dashImg.tintColor = UIColor.init(hexString: ColorCode.FMenuIndividColor)
            tomTrayImg.tintColor = UIColor.init(hexString: ColorCode.FMenuIndividColor)
            packageLbl.text = str
            packageLbl.textColor = UIColor.init(hexString: ColorCode.FMenuIndividColor)
            break
        case "Couple Menu":
            familyImg.image = #imageLiteral(resourceName: "CoupleMenu")
            familyImgHt.constant = 47
            familyImgWd.constant = 53
            circleNoLabel.layer.borderColor = UIColor.init(hexString: ColorCode.FMenuCoupleColor).cgColor
            dashImg.tintColor = UIColor.init(hexString: ColorCode.FMenuCoupleColor)
            tomTrayImg.tintColor = UIColor.init(hexString: ColorCode.FMenuIndividColor)
            packageLbl.text = str
            packageLbl.textColor = UIColor.init(hexString: ColorCode.FMenuCoupleColor)
            break
        case "Small Family Menu":
            familyImg.image = #imageLiteral(resourceName: "SmallFamily")
            familyImgHt.constant = 47
            familyImgWd.constant = 53
            circleNoLabel.layer.borderColor = UIColor.init(hexString: ColorCode.FMenuSmallColor).cgColor
            dashImg.tintColor = UIColor.init(hexString: ColorCode.FMenuSmallColor)
            tomTrayImg.tintColor = UIColor.init(hexString: ColorCode.FMenuIndividColor)
            packageLbl.text = str
            packageLbl.textColor = UIColor.init(hexString: ColorCode.FMenuSmallColor)
            break
        case "Medium Family Menu":
            familyImg.image = #imageLiteral(resourceName: "MediumFamily")
            familyImgHt.constant = 44
            familyImgWd.constant = 71
            circleNoLabel.layer.borderColor = UIColor.init(hexString: ColorCode.FMenuMediumColor).cgColor
            dashImg.tintColor = UIColor.init(hexString: ColorCode.FMenuMediumColor)
            tomTrayImg.tintColor = UIColor.init(hexString: ColorCode.FMenuIndividColor)
            packageLbl.text = str
            packageLbl.textColor = UIColor.init(hexString: ColorCode.FMenuMediumColor)
            break
        case "Big Family Menu":
            familyImg.image = #imageLiteral(resourceName: "ThemeGroup")
            familyImgHt.constant = 44
            familyImgWd.constant = 71
            circleNoLabel.layer.borderColor = UIColor.init(hexString: ColorCode.FMenuBigColor).cgColor
            dashImg.tintColor = UIColor.init(hexString: ColorCode.FMenuBigColor)
            tomTrayImg.tintColor = UIColor.init(hexString: ColorCode.FMenuIndividColor)
            packageLbl.text = str
            packageLbl.textColor = UIColor.init(hexString: ColorCode.FMenuBigColor)
            break
        default:
            break
        }
    }

    @IBAction func onShare(_ sender: Any)
    {
    }
    
    @IBAction func selectTodayMenu(_ sender: Any)
    {
        todayButton.backgroundColor = UIColor.white
        todayButton.setTitleColor(UIColor.init(hexString: ColorCode.FMenuSmallColor), for: .normal)
        tomorrowButton.backgroundColor = UIColor.clear
        tomorrowButton.setTitleColor(UIColor.white, for: .normal)
        
        tomorrowTableview.isHidden = true
        menuTableview.reloadData()
    }
    
    @IBAction func selectTomorrowMenu(_ sender: Any)
    {
        tomorrowButton.backgroundColor = UIColor.white
        tomorrowButton.setTitleColor(UIColor.init(hexString: ColorCode.FMenuSmallColor), for: .normal)
        todayButton.backgroundColor = UIColor.clear
        todayButton.setTitleColor(UIColor.white, for: .normal)
        
        tomorrowTableview.isHidden = false
        tomorrowTableview.reloadData()
    }
    
    @IBAction func selectMenuA(_ sender: Any)
    {
        menuAButton.layer.borderWidth = 0
        menuAButton.setBackgroundImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
        menuBButton.layer.borderWidth = 1
        menuBButton.setBackgroundImage(nil, for: .normal)
    }
    
    @IBAction func selectMenuB(_ sender: Any)
    {
        menuBButton.layer.borderWidth = 0
        menuBButton.setBackgroundImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
        menuAButton.layer.borderWidth = 1
        menuAButton.setBackgroundImage(nil, for: .normal)
    }
    
    @IBAction func minusMenuA(_ sender: Any)
    {
        if (countA > 0) {
            countA -= 1
            countALbl.text = "\(countA)"
        }
    }
    
    @IBAction func plusMenuA(_ sender: Any)
    {
        countA += 1
        countALbl.text = "\(countA)"
    }
    
    @IBAction func minusMenuB(_ sender: Any)
    {
        if (countB > 0) {
            countB -= 1
            countBLbl.text = "\(countB)"
        }
    }
    
    @IBAction func plusMenuB(_ sender: Any)
    {
        countB += 1
        countBLbl.text = "\(countB)"
    }
    
    @IBAction func onSave(_ sender: Any)
    {
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        goToHome(controller: self)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fmenu")
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Address
            let addressvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuAddressVC") as! FMenuAddressVC
            present(addressvc, animated: false, completion: nil)
            break
        case 2:
            // My Plan
            let myplanvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuMyPlanVC") as! FMenuMyPlanVC
            present(myplanvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuWalletVC") as! FMenuWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuNotificationVC") as! FMenuNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMenuTodayVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if (checkstr == "today") {
            return sectionArray.count
        }
        else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (checkstr == "today") {
            return rowsArray[section].count
        }
        else {
            if (section <= 1) {
                return rowsArray[section].count
            }
            else if (section == 2) {
                return 1
            }
            else {
                return rowsArray[section].count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (checkstr == "today") {
            let cell = menuTableview.dequeueReusableCell(withIdentifier: "todaymenucell", for: indexPath) as! TodayMenuTableCell
            
            cell.dishLabel.text = rowsArray[indexPath.section][indexPath.row]
            
            if (indexPath.row == 0)
            {
                //            let topBorder = CAShapeLayer()
                //            topBorder.frame = CGRect(x: 0,y: 2, width:menuTableview.frame.width, height:0.5)
                //            addDashBorder(menucell: cell, borderLayer: topBorder)
            }
            else if (indexPath.row == rowsArray[indexPath.section].count-1)
            {
                let btmBorder = CAShapeLayer()
                btmBorder.frame = CGRect(x: 0,y: cell.contentView.frame.height-0.5, width:menuTableview.frame.width, height:0.5)
                addDashBorder(menucell: cell, borderLayer: btmBorder)
            }
            let rightBorder = CAShapeLayer()
            rightBorder.frame = CGRect(x: menuTableview.frame.width - 0.5,y: 0, width:0.5, height:cell.contentView.frame.height)
            addDashBorder(menucell: cell, borderLayer: rightBorder)
            
            let leftBorder = CAShapeLayer()
            leftBorder.frame = CGRect(x:0, y:2, width:0.5, height:cell.contentView.frame.size.height-2)
            addDashBorder(menucell: cell, borderLayer: leftBorder)
            
            cell.circleImg.layer.cornerRadius = cell.circleImg.frame.width/2
            cell.circleImg.layer.masksToBounds = true
            
            return cell
        }
        else {
            if (indexPath.section <= 1) {
                let cell = tomorrowTableview.dequeueReusableCell(withIdentifier: "tomorrowmenucell", for: indexPath) as! TomorrowMenuTableCell
                
                cell.dishLabel.text = rowsArray[indexPath.section][indexPath.row]
                
                if (indexPath.row == 0)
                {
                }
                else if (indexPath.row == rowsArray[indexPath.section].count-1)
                {
                    let btmBorder = CAShapeLayer()
                    btmBorder.frame = CGRect(x: 0,y: cell.contentView.frame.height-0.5, width:menuTableview.frame.width, height:0.5)
                    addDashBorderToOther(menucell: cell, borderLayer: btmBorder)
                }
                let rightBorder = CAShapeLayer()
                rightBorder.frame = CGRect(x: menuTableview.frame.width - 0.5,y: 0, width:0.5, height:cell.contentView.frame.height)
                addDashBorderToOther(menucell: cell, borderLayer: rightBorder)
                
                let leftBorder = CAShapeLayer()
                leftBorder.frame = CGRect(x:0, y:2, width:0.5, height:cell.contentView.frame.size.height-2)
                addDashBorderToOther(menucell: cell, borderLayer: leftBorder)
                
                cell.circleImg.layer.cornerRadius = cell.circleImg.frame.width/2
                cell.circleImg.layer.masksToBounds = true
                
                return cell
            }
            else if (indexPath.section == 2) {
                let cell : UITableViewCell = tomorrowTableview.dequeueReusableCell(withIdentifier: "menuBcell")!
                return cell
            }
            else {
                let cell = tomorrowTableview.dequeueReusableCell(withIdentifier: "tomorrowmenucell2", for: indexPath) as! TomorrowMenuTableCell
                
                cell.dishLabel.text = rowsArray[indexPath.section][indexPath.row]
                
                if (indexPath.row == 0)
                {
                }
                else if (indexPath.row == rowsArray[indexPath.section].count-1)
                {
                    let btmBorder = CAShapeLayer()
                    btmBorder.frame = CGRect(x: 0,y: cell.contentView.frame.height-0.5, width:menuTableview.frame.width, height:0.5)
                    addDashBorderToOther(menucell: cell, borderLayer: btmBorder)
                }
                let rightBorder = CAShapeLayer()
                rightBorder.frame = CGRect(x: menuTableview.frame.width - 0.5,y: 0, width:0.5, height:cell.contentView.frame.height)
                addDashBorderToOther(menucell: cell, borderLayer: rightBorder)
                
                let leftBorder = CAShapeLayer()
                leftBorder.frame = CGRect(x:0, y:2, width:0.5, height:cell.contentView.frame.size.height-2)
                addDashBorderToOther(menucell: cell, borderLayer: leftBorder)
                
                cell.circleImg.layer.cornerRadius = cell.circleImg.frame.width/2
                cell.circleImg.layer.masksToBounds = true
                
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if (section <= 1) || (section > 2) {
            let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 45))
            
            let borderLayer = CAShapeLayer()
            borderLayer.frame = CGRect(x: 0,y: vw.frame.height-1, width:vw.frame.width, height:0.5)
            borderLayer.path = UIBezierPath(roundedRect: borderLayer.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 2, height: 2)).cgPath
            borderLayer.strokeColor = UIColor.init(hexString: ColorCode.FMenuGray).cgColor
            borderLayer.fillColor = nil
            borderLayer.lineDashPattern = [3, 3]
            vw.layer.addSublayer(borderLayer)
            
            let label = UILabel.init(frame: CGRect(x: 0, y: 5, width: tableView.frame.width, height: 35))
            label.backgroundColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
            
            let textLabel = UILabel.init(frame: CGRect(x: (label.frame.width-50)/2 + 20, y: 5, width: 50, height: 35))
            textLabel.text = sectionArray[section]
            textLabel.textColor = UIColor.white
            textLabel.textAlignment = .left
            textLabel.font = UIFont(name: "SFProText-Regular", size: 15)
            
            let imgvw = UIImageView.init(frame: CGRect(x: (label.frame.width/2)-(25+20), y: (35-15)/2, width: 25, height: 25))
            imgvw.image = #imageLiteral(resourceName: "tray")
            imgvw.tintColor = UIColor.white
            
            vw.addSubview(label)
            vw.addSubview(textLabel)
            vw.addSubview(imgvw)
            
            return vw
        }
        else {
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 10))
        return vw
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func addDashBorder(menucell : TodayMenuTableCell, borderLayer : CAShapeLayer)
    {
        borderLayer.path = UIBezierPath(roundedRect: borderLayer.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 1, height: 1)).cgPath
        borderLayer.strokeColor = UIColor.init(hexString: ColorCode.FMenuGray).cgColor
        borderLayer.fillColor = nil
        borderLayer.lineDashPattern = [3, 3]
        menucell.contentView.layer.addSublayer(borderLayer)
    }
    
    func addDashBorderToOther(menucell : TomorrowMenuTableCell, borderLayer : CAShapeLayer)
    {
        borderLayer.path = UIBezierPath(roundedRect: borderLayer.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 1, height: 1)).cgPath
        borderLayer.strokeColor = UIColor.init(hexString: ColorCode.FMenuGray).cgColor
        borderLayer.fillColor = nil
        borderLayer.lineDashPattern = [3, 3]
        menucell.contentView.layer.addSublayer(borderLayer)
    }

}

class TodayMenuTableCell: UITableViewCell {
 
    @IBOutlet weak var dishLabel : UILabel!
    @IBOutlet weak var circleImg : UIImageView!
}

class TomorrowMenuTableCell: UITableViewCell {
    
    @IBOutlet weak var dishLabel : UILabel!
    @IBOutlet weak var circleImg : UIImageView!
}
