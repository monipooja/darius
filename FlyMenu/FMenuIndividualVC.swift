//
//  FMenuIndividualVC.swift
//  Darious
//
//  Created by Apple on 20/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FMenuIndividualVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var menuLabelCollection : [UILabel]!
    @IBOutlet var dashImgCollection : [UIImageView]!
    
    @IBOutlet weak var topTitleLabel : UILabel!
    @IBOutlet weak var circleLabel : UILabel!
    @IBOutlet weak var packageLabel : UILabel!
    @IBOutlet weak var lunchMonFri : UILabel!
    @IBOutlet weak var lunchMonSun : UILabel!
    @IBOutlet weak var lunchDinnerMonFri : UILabel!
    @IBOutlet weak var lunchDinnerMonSun : UILabel!
    
    var layoutCount : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        if (self.view.frame.width == 320) {
            topTitleLabel.font = UIFont.init(name: "OpenSans-Bold", size: 18)
        }
        
        packageLabel.text = subsData.package.capitalized
        circleLabel.layer.cornerRadius = circleLabel.frame.width/2
        circleLabel.layer.borderWidth = 1.2
        circleLabel.layer.borderColor = UIColor.init(hexString: ColorCode.FMenuIndividColor).cgColor
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    override func viewDidLayoutSubviews() {
        
        if (layoutCount == 1)
        {
            for img in dashImgCollection
            {
                img.image = nil
                img.setDashBorderWithColor(colorStr : ColorCode.FMenuIndividColor, crad: 12)
            }
        }
        layoutCount += 1
    }
    
    @IBAction func selectLunchMonFri(_ sender: Any)
    {
        subsData.daysType = "1"
        subsData.foodType = "1"
        subsData.amount = (lunchMonFri.text?.replacingOccurrences(of: "\(Current_Currency)", with: ""))!
        proceedToPayment()
    }
    
    @IBAction func selectLunchMonSun(_ sender: Any)
    {
        subsData.daysType = "2"
        subsData.foodType = "1"
        subsData.amount = (lunchMonSun.text?.replacingOccurrences(of: "\(Current_Currency)", with: ""))!
        proceedToPayment()
    }
    
    @IBAction func selectLunchDinnerMonFri(_ sender: Any)
    {
        subsData.daysType = "1"
        subsData.foodType = "2"
        subsData.amount = (lunchDinnerMonFri.text?.replacingOccurrences(of: "\(Current_Currency)", with: ""))!
        proceedToPayment()
    }
    
    @IBAction func selectLunchDinnerMonSun(_ sender: Any)
    {
        subsData.daysType = "2"
        subsData.foodType = "2"
        subsData.amount = (lunchDinnerMonSun.text?.replacingOccurrences(of: "\(Current_Currency)", with: ""))!
        proceedToPayment()
    }
    
    func proceedToPayment()
    {
        let paymentvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuPaymentVC") as! FMenuPaymentVC
        present(paymentvc, animated: false, completion: nil)
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        goToHome(controller: self)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fmenu")
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Address
            let addressvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuAddressVC") as! FMenuAddressVC
            present(addressvc, animated: false, completion: nil)
            break
        case 2:
            // My Plan
            let myplanvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuMyPlanVC") as! FMenuMyPlanVC
            present(myplanvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuWalletVC") as! FMenuWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuNotificationVC") as! FMenuNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }

}
