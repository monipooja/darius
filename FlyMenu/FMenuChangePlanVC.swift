//
//  FMenuChangePlanVC.swift
//  Darious
//
//  Created by Apple on 20/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FMenuChangePlanVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var healthyLabel : UILabel!
    @IBOutlet weak var weeklyLabel : UILabel!
    @IBOutlet weak var monthlyLabel : UILabel!
    @IBOutlet weak var ChangeLabel : UILabel!
    @IBOutlet weak var weeklyImgVw : UIImageView!
    @IBOutlet weak var monthlyImgVw : UIImageView!
    @IBOutlet weak var WMenuImageVw : UIImageView!
    @IBOutlet weak var MMenuImageVw : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FMenuThemeColor)
        
        subsData = SubscriptionData()
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        WMenuImageVw.tintColor = UIColor.init(hexString: ColorCode.FMenuIndividColor)
        MMenuImageVw.tintColor = UIColor.init(hexString: ColorCode.FMenuBigColor)
        weeklyImgVw.setDashBorderWithColor(colorStr: ColorCode.FMenuBorderGreen, crad: 12)
        monthlyImgVw.setDashBorderWithColor(colorStr: ColorCode.FMenuBigColor, crad: 12)
        
        setBottomView()
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    @IBAction func selectWeeklyMenu(_ sender: Any)
    {
        subsData.package = "weekly"
        let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuVC") as! FMenuVC
        menuvc.titleStr = "Weekly Menu"
        menuvc.titleColor = ColorCode.FMenuIndividColor
        present(menuvc, animated: false, completion: nil)
    }
    
    @IBAction func selectMonthlyMenu(_ sender: Any)
    {
        subsData.package = "monthly"
        let menuvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuVC") as! FMenuVC
        menuvc.titleStr = "Monthly Menu"
        menuvc.titleColor = ColorCode.FMenuBigColor
        present(menuvc, animated: false, completion: nil)
    }
    
    @IBAction func onShare(_ sender: Any)
    {
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        goToHome(controller: self)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK : Call Service
    func callService(urlstr : String, params : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: params, controller: self) { (result) in
            
            // print("menu splash = \(result)")
            OperationQueue.main.addOperation {
//                if let dict = result["data"] as? NSDictionary
//                {
//                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fmenu")
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Address
            let addressvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuAddressVC") as! FMenuAddressVC
            present(addressvc, animated: false, completion: nil)
            break
        case 2:
            // My Plan
            let myplanvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuMyPlanVC") as! FMenuMyPlanVC
            present(myplanvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuWalletVC") as! FMenuWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuNotificationVC") as! FMenuNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }

}
