//
//  FSRankingVC.swift
//  Darious
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import DropDown
import FirebaseDynamicLinks

class FSRankingVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var countryCenterX : NSLayoutConstraint!
    
    @IBOutlet var ddImgCollection: [UIImageView]!
    
    @IBOutlet weak var topFlagImg : UIImageView!
    @IBOutlet weak var flagImg : UIImageView!
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var tableContentVw : UIView!
    
    @IBOutlet weak var countryLabel : UILabel!
    @IBOutlet weak var provinceLabel : UILabel!
    @IBOutlet weak var categoryLabel : UILabel!
    @IBOutlet weak var positionLabel : UILabel!
    @IBOutlet weak var yearLabel : UILabel!
    @IBOutlet weak var periodLabel : UILabel!
    @IBOutlet weak var leagueLabel : UILabel!
    
    @IBOutlet weak var provView : CustomUIView!
    @IBOutlet weak var catgView : CustomUIView!
    @IBOutlet weak var posView : CustomUIView!
    @IBOutlet weak var yearView : CustomUIView!
    @IBOutlet weak var periodView : CustomUIView!
    @IBOutlet weak var leagueView : CustomUIView!
    
    @IBOutlet weak var sendBtn : KGHighLightedButton!
    
    @IBOutlet weak var provHt : NSLayoutConstraint!
    @IBOutlet weak var leagueHt : NSLayoutConstraint!
    @IBOutlet weak var leagueBtm : NSLayoutConstraint!
    @IBOutlet weak var provBtm : NSLayoutConstraint!
    
    let dropDown = DropDown()
    let searchBar : UISearchBar = UISearchBar.init()
    
    var yearIndex : Int = 0
    var country_id : String = ""
    var passid : String = ""
    var passname : String = ""
    var flagiso : String = ""
    var votedate : String = ""
    
    var nameArray : [String] = []
    var isoArray : [String] = []
    var idArray : [String] = []
    var dataFiltered: [String] = []
    var filterIsoArray : [String] = []
    var filterIdArray : [String] = []
    var yearArray : [String] = []
    var periodArray : [[String]] = []
    
    var provNameArray : [String] = []
    var provIdArray : [String] = []
    var catgNameArray : [String] = []
    var catgIdArray : [String] = []
    var posNameArray : [String] = []
    var leaguesArray : [String] = []
    
    var rankingDict : NSMutableDictionary = ["country_id":"", "country_name":"", "province_id":"", "prov_name":"", "catg_id":"", "catg_name":"", "pos":"", "year":"", "months":"", "league" : ""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FSThemeColor
        
        if (passid != "")
        {
            FS_SelectedCountry = passname
            FS_CountryID = passid
            FS_Flag_iso = flagiso
            FS_Vote_end_date = votedate.replacingOccurrences(of: "2", with: ", 2")
        }
        topFlagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FS_Flag_iso).png"))
        for img in ddImgCollection
        {
            img.tintColor = ColorCode.FSViewBG
        }
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        let gesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(touchGesture(_:)))
        gesture.numberOfTapsRequired = 1
        tableContentVw.addGestureRecognizer(gesture)
        
        searchBarIntegration()
        setBottomView()
        SetLanguage()
        
        leagueHt.constant = 0
        leagueBtm.constant = 0
        leagueView.isHidden = true
        
        DispatchQueue.main.async {
            let params2 = "api_key=\(api_key)"
            self.callService(urlstr: Api.STDCode_URL, parameters: params2, check: "country")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    @objc func touchGesture(_ gesture : UIGestureRecognizer)
    {
        self.view.endEditing(true)
        self.searchBar.isHidden = true
    }
    
    // MARK : Search bar create
    func searchBarIntegration()
    {
        searchBar.frame = CGRect(x: 20, y: 40, width: self.view.frame.width-40, height:50)
        searchBar.delegate = self
        self.view.addSubview(searchBar)
        searchBar.isHidden = true
    }
    
    func SetLanguage()
    {
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_ranking")
        countryLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_country")
        provinceLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_province")
        yearLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_year")
        periodLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_period")
        categoryLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_category")
        positionLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_position")
        leagueLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_league")
        
        sendBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "send"), for: .normal)
    }
    
    @IBAction func chooseCountry(_ sender: Any)
    {
        provinceLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_province")
        provinceLabel.textColor = ColorCode.FSDDColor
        provNameArray = []
        provIdArray = []
        catgNameArray = []
        catgIdArray = []
        if (nameArray == [])
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
            let params2 = "api_key=\(api_key)"
            self.callService(urlstr: Api.STDCode_URL, parameters: params2, check: "country_dd")
        }
        else {
            showDropDown()
        }
    }
    
    @IBAction func chooseProvince(_ sender: Any)
    {
        provNameArray = []
        provIdArray = []
        catgNameArray = []
        catgIdArray = []
        posNameArray = []
        leaguesArray = []
        customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&country_id=\(country_id)"
        self.callService(urlstr: Api.FS_Sports_List, parameters: params, check: "prov")
    }
    
    @IBAction func chooseCategory(_ sender: Any)
    {
        showSecondDropDown(anchorVw: catgView, passarr: catgNameArray, passLabel: categoryLabel)
    }
    
    @IBAction func chooseLeague(_ sender: Any)
    {
        if (leaguesArray == [])
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_no_league_available"), controller: self)
        }
        else {
            showSecondDropDown(anchorVw: leagueView, passarr: leaguesArray, passLabel: leagueLabel)
        }
    }
    
    @IBAction func choosePosition(_ sender: Any)
    {
        showSecondDropDown(anchorVw: posView, passarr: posNameArray, passLabel: positionLabel)
    }
    
    @IBAction func chooseYear(_ sender: Any)
    {
        yearArray = []
        periodArray = []
        customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&app_name=flysports"
        self.callService(urlstr: Api.FB_Session_URL, parameters: params, check: "session")
    }
    
    @IBAction func choosePeriod(_ sender: Any)
    {
        showSecondDropDown(anchorVw: periodView, passarr: periodArray[yearIndex], passLabel: periodLabel)
    }
    
    @IBAction func onSend(_ sender: Any)
    {
        if (countryLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_country")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_county"), controller: self)
        }
        else if (provinceLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_province")) && (provView.isHidden == false) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_select_province"), controller: self)
        }
        else if (categoryLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_category")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_select_category"), controller: self)
        }
        else if (leagueView.isHidden == false) && (leagueLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_league")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_select_league"), controller: self)
        }
        else if (positionLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_position")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_select_position"), controller: self)
        }
        else if (yearLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_year")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_year"), controller: self)
        }
        else if (periodLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_period")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_select_period"), controller: self)
        }
        else {
            let rankinglist = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSRankingListVC") as! FSRankingListVC
            rankinglist.passDict = rankingDict
            present(rankinglist, animated: false, completion: nil)
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (passname == "")
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            let homevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flysport&screen_name=fsranking&country_name=\(FS_SelectedCountry)&flag=\(FS_Flag_iso)&country=\(FS_CountryID)&province_id=&province=&category_id=&category=&position=&vote_date=\(FS_Vote_end_date.replacingOccurrences(of: ", ", with: ""))") else { return }
        
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flysport")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    func showDropDown()
    {
        searchBar.isHidden = false
        searchBar.text = ""
        
        dropDown.anchorView = searchBar
        dropDown.dataSource = nameArray
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            let codestr = (self.isoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
            cell.codeLabel.text = ""
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            self.countryLabel.text = self.nameArray[index]
            self.rankingDict["country_name"] = self.nameArray[index]
            self.country_id = self.idArray[index]
            self.rankingDict["country_id"] = self.idArray[index]
            self.countryCenterX.constant = 15
            
            let codestr = (self.isoArray[index]).lowercased()
            self.flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
            
            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
        }
        dropDown.width = searchBar.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
        
        self.countryLabel.textColor = ColorCode.FSDDColor
        self.countryLabel.font = UIFont(name: "CopperplateGothic-Bold", size: 13)
        searchBar.becomeFirstResponder()
    }
    
    func showSecondDropDown(anchorVw : CustomUIView, passarr : [String], passLabel : UILabel)
    {
        dropDown.anchorView = anchorVw
        dropDown.dataSource = passarr
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            cell.flagImage.image = nil
            cell.codeLabel.text = ""
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            passLabel.text = passarr[index]
            passLabel.textColor = ColorCode.FSDDColor
        
            if (anchorVw == self.yearView) {
                self.yearIndex = index
                self.rankingDict["year"] = passarr[index]
            }
            else if (anchorVw == self.periodView) {
                self.rankingDict["months"] = passarr[index]
            }
            else if (anchorVw == self.posView) {
                self.rankingDict["pos"] = passarr[index]
            }
            else if (anchorVw == self.catgView) {
                self.rankingDict["catg_id"] = self.catgIdArray[index]
                self.rankingDict["catg_name"] = passarr[index]
                if (passarr[index] == "Senior")
                {
                    self.provHt.constant = 0
                    self.provView.isHidden = true
                    self.provBtm.constant = 0
                    self.leagueHt.constant = 55
                    self.leagueView.isHidden = false
                    self.leagueBtm.constant = 10
                }
                else {
                    self.leagueHt.constant = 0
                    self.leagueView.isHidden = true
                    self.leagueBtm.constant = 0
                    self.provHt.constant = 55
                    self.provView.isHidden = false
                    self.provBtm.constant = 10
                }
            }
            else if (anchorVw == self.provView) {
                self.rankingDict["province_id"] = self.provIdArray[index]
                self.rankingDict["prov_name"] = passarr[index]
            }
            else if (passLabel == self.leagueLabel) {
                self.rankingDict["league"] = passarr[index]
            }
            else{}
        }
        dropDown.width = anchorVw.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    // MARK : Service call and fetching data
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("ranking \(check) = \(result)")
                if (check == "country") || (check == "country_dd")
                {
                    let data = result["data"] as! [NSDictionary]
                    self.nameArray = []
                    self.isoArray = []
                    self.idArray = []
                    for dict in data
                    {
                        self.nameArray.append(dict["nicename"] as! String)
                        self.isoArray.append(dict["iso"] as! String)
                        self.idArray.append("\(dict["id"] as! NSNumber)")
                        
                        if (check == "country")
                        {
                            if ("\(dict["id"] as! NSNumber)" == FS_CountryID)
                            {
                                self.countryLabel.text = dict["nicename"] as? String
                                self.countryLabel.textColor = ColorCode.FSDDColor
                                self.rankingDict["country_name"] = dict["nicename"] as! String
                                self.country_id = "\(dict["id"] as! NSNumber)"
                                self.rankingDict["country_id"] = "\(dict["id"] as! NSNumber)"
                                self.countryCenterX.constant = 15
                                
                                let codestr = (dict["iso"] as! String).lowercased()
                                self.flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
                            }
                        }
                    }
                    if (check == "country_dd"){
                        self.showDropDown()
                    }
                    customLoader.hideIndicator()
                }
                else if (check == "prov") {
                    let provArr = result["province"] as! [NSDictionary]
                    for dict in provArr
                    {
                        self.provNameArray.append(dict["name"] as! String)
                        self.provIdArray.append("\(dict["id"] as! NSNumber)")
                    }
                    let catgArr = result["category"] as! [NSDictionary]
                    for dict in catgArr
                    {
                        self.catgNameArray.append(dict["name"] as! String)
                        self.catgIdArray.append("\(dict["id"] as! NSNumber)")
                    }
                    let arr = result["playerPosition"] as! [String]
                    for value in arr
                    {
                        self.posNameArray.append(ApiResponse.getLanguageFromUserDefaults(inputString: value))
                    }
                    let leagArr = result["leagues"] as! [String]
                    for str in leagArr
                    {
                        let convertStr = ApiResponse.getLanguageFromUserDefaults(inputString: str)
                        self.leaguesArray.append(convertStr)
                    }
                    customLoader.hideIndicator()
                    
                    self.showSecondDropDown(anchorVw: self.provView, passarr: self.provNameArray, passLabel: self.provinceLabel)
                }
                else {
                    //print("ranking \(check) = \(result)")
                    let dict = result["data"] as! NSDictionary
                    for (key,value) in dict
                    {
                        if ((key as! String) != "active_session") {
                            self.yearArray.append(key as! String)
                            var arr : [String] = []
                            for str in (value as! [String]) {
                                arr.append(ApiResponse.getLanguageFromUserDefaults(inputString: "\(str)"))
                            }
                            self.periodArray.append(arr)
                        }
                    }
                    customLoader.hideIndicator()
                    self.showSecondDropDown(anchorVw: self.yearView, passarr: self.yearArray, passLabel: self.yearLabel)
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fs")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flysportsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}


extension FSRankingVC : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dataFiltered = searchText.isEmpty ? nameArray : nameArray.filter({ (dat) -> Bool in
            dat.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        dropDown.dataSource = dataFiltered
        filterIsoArray = []
        filterIdArray = []
        
        for str in dataFiltered {
            var i : Int = 0
            for str1 in nameArray {
                if (str == str1) {
                    filterIsoArray.append(isoArray[i])
                    filterIdArray.append(idArray[i])
                    break
                }
                i += 1
            }
        }
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            let codestr = (self.filterIsoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png"))
            cell.codeLabel.text = ""
        }
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            self.countryLabel.text = self.dataFiltered[index]
            self.rankingDict["country_name"] = self.dataFiltered[index]
            self.country_id = self.filterIdArray[index]
            self.rankingDict["country_id"] = self.filterIdArray[index]
            self.countryCenterX.constant = 15
            
            let codestr = (self.filterIsoArray[index]).lowercased()
            self.flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
            
            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
        }
        
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        dataFiltered = nameArray
        filterIsoArray = isoArray
        filterIdArray = idArray
        searchBar.isHidden = true
        dropDown.hide()
    }
}
