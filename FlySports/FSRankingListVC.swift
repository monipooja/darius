//
//  FSRankingListVC.swift
//  Darious
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FSRankingListVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet weak var rankingTableVw : UITableView!
    @IBOutlet weak var flagImg : UIImageView!
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var nodataLabel : UILabel!
    
    @IBOutlet weak var provinceLabel : UILabel!
    @IBOutlet weak var categoryLabel : UILabel!
    @IBOutlet weak var positionLabel : UILabel!
    
    var passDict : NSMutableDictionary = [:]
    var rankListarray : [NSDictionary] = []
    var heightArray : [CGFloat] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FSThemeColor
        
        flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FS_Flag_iso).png"))
        
        positionLabel.text = "\(passDict["country_name"] as! String) - \(passDict["prov_name"] as! String)"
        categoryLabel.text = "\(passDict["catg_name"] as! String) - \(passDict["pos"] as! String)"
        provinceLabel.text = "\(passDict["year"] as! String) - \(passDict["months"] as! String)"
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_ranking")
      
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&country_id=\(self.passDict["country_id"] as! String)&province=\(self.passDict["province_id"] as! String)&cat=\(self.passDict["catg_id"] as! String)&pos=\(self.passDict["pos"] as! String)&year=\(self.passDict["year"] as! String)&months=\(self.passDict["months"] as! String)&ledges=\(self.passDict["league"] as! String)"
            print("paraaa = \(params)")
            self.callService(urlstr: Api.FS_Sports_Ranking, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        dismiss(animated:false, completion: nil)
    }
    
    // MARK : Service call and fetching data
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                print("sport ranking list = \(result)")
                self.rankListarray = result["data"] as! [NSDictionary]
                if (self.rankListarray.count > 0) {
                    for _ in 0...self.rankListarray.count-1
                    {
                        self.heightArray.append(105)
                    }
                    self.rankingTableVw.isHidden = false
                    self.nodataLabel.isHidden = true
                    self.rankingTableVw.reloadData()
                }
                else {
                    self.rankingTableVw.isHidden = true
                    self.nodataLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                    self.nodataLabel.isHidden = false
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fs")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flysportsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}


extension FSRankingListVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return rankListarray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = rankingTableVw.dequeueReusableCell(withIdentifier: "rankingsportcell") as! RankingSportTableCell
        
        cell.serialNoLabel.text = "\(indexPath.section+1)"
        if (indexPath.section % 2 == 0)
        {
            cell.serialnoView.backgroundColor = ColorCode.FSThemeColor
        }
        else {
            cell.serialnoView.backgroundColor = ColorCode.FSViewBG
        }
        
        let dict = rankListarray[indexPath.section]
        
        cell.nameLabel.text = dict["name"] as? String
        cell.usernameLabel.text = dict["team"] as? String
        cell.votesLabel.text = "\(dict["votes"] as! NSNumber)"
        
        if (indexPath.section == 0) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "gold-medal")
        }
        else if (indexPath.section == 1) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "FB_silvermedal")
        }
        else if (indexPath.section == 2) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "FB_bronzemedal")
        }
        else {
            cell.goldImageVw.image = nil
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: rankingTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
}

class RankingSportTableCell : UITableViewCell
{
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var usernameLabel : UILabel!
    @IBOutlet weak var votesLabel : UILabel!
    @IBOutlet weak var serialNoLabel : UILabel!

    @IBOutlet weak var goldImageVw : UIImageView!
    @IBOutlet weak var serialnoView : CustomUIView!
}
