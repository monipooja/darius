//
//  FSCountryVC.swift
//  Darious
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import DropDown
import FirebaseDynamicLinks

class FSCountryVC: UIViewController {
    
    @IBOutlet weak var dropdwonImg : UIImageView!
    @IBOutlet weak var continueBtn : KGHighLightedButton!
    @IBOutlet weak var flagImg : UIImageView!
    @IBOutlet weak var countryLabel : UILabel!
    
    let dropDown = DropDown()
    let searchBar : UISearchBar = UISearchBar.init()
    
    var nameArray : [String] = []
    var isoArray : [String] = []
    var idArray : [String] = []
    var dataFiltered: [String] = []
    var filterIsoArray : [String] = []
    var filterIdArray : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FSThemeColor
        
        dropdwonImg.tintColor = ColorCode.FSDDColor
        
        flagImg.image = nil
        countryLabel.text = ""
        
        searchBarIntegration()
        
        continueBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "continue"), for: .normal)
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
            let params2 = "api_key=\(api_key)"
            self.callService(urlstr: Api.STDCode_URL, parameters: params2, check: "std")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.searchBar.isHidden = true
    }
    
    // MARK : Search bar create
    func searchBarIntegration()
    {
        searchBar.frame = CGRect(x: 20, y: 40, width: self.view.frame.width-40, height:50)
        searchBar.delegate = self
        self.view.addSubview(searchBar)
        searchBar.isHidden = true
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flysport&screen_name=country&country_name=&flag=&country=&province_id=&province=&category_id=&category=&position=&vote_date=") else { return }
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flysport")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flysport")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func onContinue(_ sender: Any)
    {
        if (countryLabel.text == "") {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_country"), controller: self)
        }
        else {
            let homevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    @IBAction func onGoBack(_ sender: Any)
    {
        view.endEditing(true)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
    }
    
    @IBAction func chooseCountry(_ sender: Any)
    {
        if (nameArray != [])
        {
            showDropDown()
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
            let params2 = "api_key=\(api_key)"
            self.callService(urlstr: Api.STDCode_URL, parameters: params2, check: "show")
        }
    }
    
    func showDropDown()
    {
        searchBar.isHidden = false
        searchBar.text = ""
        
        dropDown.anchorView = searchBar
        dropDown.dataSource = nameArray
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            let codestr = (self.isoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
            cell.codeLabel.text = ""
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            FS_SelectedCountry = self.nameArray[index]
            FS_CountryID = self.idArray[index]
            FS_Flag_iso = (self.isoArray[index]).lowercased()
            
            self.countryLabel.text = self.nameArray[index]
            let codestr = (self.isoArray[index]).lowercased()
            self.flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)

            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
        }
        dropDown.width = searchBar.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
        
        searchBar.becomeFirstResponder()
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                //   print("country = \(result)")
                self.nameArray = []
                self.isoArray = []
                self.idArray = []
                let data = result["data"] as! [NSDictionary]
                for dict in data
                {
                    self.nameArray.append(dict["nicename"] as! String)
                    self.isoArray.append(dict["iso"] as! String)
                    self.idArray.append("\(dict["id"] as! NSNumber)")
                }
                FS_SelectedCountry = self.nameArray[0]
                FS_CountryID = self.idArray[0]
                FS_Flag_iso = (self.isoArray[0]).lowercased()
                
                self.countryLabel.text = self.nameArray[0]
                self.countryLabel.textColor = ColorCode.FSDDColor
                
                let codestr = (self.isoArray[0]).lowercased()
                self.flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
                
                if (check == "show") {
                    self.showDropDown()
                }
                customLoader.hideIndicator()
            }
        }
    }
    
}

extension FSCountryVC : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dataFiltered = searchText.isEmpty ? nameArray : nameArray.filter({ (dat) -> Bool in
            dat.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        dropDown.dataSource = dataFiltered
        filterIsoArray = []
        filterIdArray = []
        
        for str in dataFiltered {
            var i : Int = 0
            for str1 in nameArray {
                if (str == str1) {
                    filterIsoArray.append(isoArray[i])
                    filterIdArray.append(idArray[i])
                    break
                }
                i += 1
            }
        }
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            let codestr = (self.filterIsoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png"))
            cell.codeLabel.text = ""
        }
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            FS_SelectedCountry = self.dataFiltered[index]
            FS_CountryID = self.filterIdArray[index]
            FS_Flag_iso = (self.filterIsoArray[index]).lowercased()
            
            self.countryLabel.text = self.dataFiltered[index]
            let codestr = (self.filterIsoArray[index]).lowercased()
            self.flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)

            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
        }
        
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        dataFiltered = nameArray
        filterIsoArray = isoArray
        filterIdArray = idArray
        searchBar.isHidden = true
        dropDown.hide()
    }
}
