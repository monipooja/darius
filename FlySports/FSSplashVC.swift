//
//  FSSplashVC.swift
//  Darious
//
//  Created by Apple on 31/10/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

var FS_SelectedCountry : String = ""
var FS_CountryID : String = ""
var FS_Flag_iso : String = ""

class FSSplashVC: UIViewController {

    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = true
//        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FSThemeColor
//        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(gotoNext), userInfo: nil, repeats: false)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func gotoNext()
    {
        timer.invalidate()
        let countryvc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSCountryVC") as! FSCountryVC
        present(countryvc, animated: false, completion: nil)
    }
}



public func flysportsBottomViewAction(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 1:
        // Ranking
        let rankingvc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSRankingVC") as! FSRankingVC
        controller.present(rankingvc, animated: false, completion: nil)
        break
    case 2:
        // My Votes
        let myvotevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSMyVotesVC") as! FSMyVotesVC
        controller.present(myvotevc, animated: false, completion: nil)
        break
    case 3:
        // Darious
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
        break
    case 4:
        // Wallet
        let walletvc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSWalletVC") as! FSWalletVC
        controller.present(walletvc, animated: false, completion: nil)
        break
    case 5:
        // Notification
        let notifyvc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSNotificationVC") as! FSNotificationVC
        controller.present(notifyvc, animated: false, completion: nil)
        break
    default:
        break
    }
}
