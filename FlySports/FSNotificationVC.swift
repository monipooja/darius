//
//  FSNotificationVC.swift
//  Darious
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FSNotificationVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
   
    @IBOutlet weak var notificationTableVw: UITableView!
    @IBOutlet weak var centerLbl: UILabel!
    
    var dictArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        notificationTableVw.rowHeight = UITableView.automaticDimension
        notificationTableVw.estimatedRowHeight = 55
        centerLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_notification_found")
        setBottomView()
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.Notification_URL, parameters: params)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    func callService(urlstr : String, parameters : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            //  print("notification result = \(result)")
            OperationQueue.main.addOperation {
                
                if let _ = result["notificationData"] as? [NSDictionary]
                {
                    self.dictArray = result["notificationData"] as! [NSDictionary]
                    if(self.dictArray.count > 0) {
                        self.notificationTableVw.reloadData()
                    } else {
                        self.centerLbl.isHidden = false
                    }
                }
                else {
                    self.centerLbl.isHidden = false
                }
                notification_count = 0
                self.notifyImageView.image = #imageLiteral(resourceName: "nonotification")
                customLoader.hideIndicator()
            }
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flysportsBottomViewAction(tagValue: sender.tag, controller: self)
    }
}

extension FSNotificationVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = notificationTableVw.dequeueReusableCell(withIdentifier: "fsnotifycell") as! FSNotifyTableCell
        
        let dict = dictArray[indexPath.row]
        
        let descStr = dict["messages"] as? String
        if (descStr?.contains("_") == true) && (ApiResponse.getLanguageFromUserDefaults(inputString: descStr!) != "")
        {
            cell.msgLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: descStr!)
        } else
        {
            cell.msgLbl.text = dict["messages"] as? String
        }
        
        if let _ = dict["created_on"] as? String
        {
            let datestr = dict["created_on"] as! String
            if let str = datestr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM yyyy | HH:mm a")
            {
                cell.dateLbl.text = str
            }
        }
        
        return cell
    }
    
}

class FSNotifyTableCell: UITableViewCell {
    // notifycell
    @IBOutlet weak var msgLbl : UILabel!
    @IBOutlet weak var descLbl : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var imgvw : UIImageView!
}
