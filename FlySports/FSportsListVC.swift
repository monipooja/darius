//
//  FSportsListVC.swift
//  Darious
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks

class FSportsListVC: UIViewController, UITextFieldDelegate{

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet weak var flagImg : UIImageView!
    @IBOutlet weak var positionLabel : UILabel!
    @IBOutlet weak var categoryLabel : UILabel!
    @IBOutlet weak var provinceLabel : UILabel!
    @IBOutlet weak var nodataLabel : UILabel!
    @IBOutlet weak var voteDateLbl : UILabel!
    
    @IBOutlet weak var searchTF : UITextField!
    
    @IBOutlet weak var sportListTableVw : UITableView!
    
    @IBOutlet weak var blurView : UIView!
    @IBOutlet weak var centerView : CustomUIView!
    @IBOutlet weak var voteMsgLbl : UILabel!
    @IBOutlet weak var closeBtn : KGHighLightedButton!
    
    var passDict : NSMutableDictionary = [:]
    
    var selectedCell : SportListTableCell!
    var selected5sCell : SportList5sTableCell!
    var selectDict : NSDictionary = [:]
    var selectIndex : Int = 0
    var listArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FSThemeColor
        
        if let _ = passDict["passname"] as? String
        {
            FS_Flag_iso = passDict["flag"] as! String
            FS_SelectedCountry =  passDict["passname"] as! String
            FS_CountryID = passDict["country"] as! String
            FS_Vote_end_date = (passDict["vote_date"] as! String).replacingOccurrences(of: "2", with: ", 2")
        }
        flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FS_Flag_iso).png"))
        positionLabel.text = passDict["pos"] as? String
        categoryLabel.text = passDict["catg_name"] as? String
        provinceLabel.text = passDict["prov_name"] as? String
        
        voteDateLbl.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fb_voting_month")) - \(FS_Vote_end_date) \n \(ApiResponse.getLanguageFromUserDefaults(inputString: "fb_vote_once_day"))"
        voteMsgLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "vote_success_msg")
        closeBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "close"), for: .normal)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&type=football&list_type=people&country=\(FS_CountryID)&province=\(self.passDict["prov_id"] as! String)&cat_id=\(self.passDict["catg_id"] as! String)&pos_id=\(self.passDict["pos"] as! String)&ledges=\(self.passDict["league"] as! String)"
            self.callService(urlstr: Api.FB_List_URL, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func closeCenterView(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if let _ = passDict["passname"] as? String
        {
            let homevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
            present(homevc, animated: false, completion: nil)
        }
        else {
            dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flysport&screen_name=list&country_name=\(FS_SelectedCountry)&flag=\(FB_Flag_iso)&country=\(FS_CountryID)&province_id=\(passDict["province_id"] as! String)&province=\(passDict["prov_name"] as! String)&category_id=\(passDict["catg_id"] as! String)&category=\(passDict["catg_name"] as! String)&position=\(passDict["pos"] as! String)&league=\(passDict["league"] as! String)&vote_date=\(FS_Vote_end_date.replacingOccurrences(of: ", ", with: ""))") else { return }
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flysport")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flysport")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    // MARK : Service call and fetching data
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("sport list = \(result)")
                if (check == "get") {
                    self.listArray = result["data"] as! [NSDictionary]
                    if (self.listArray.count > 0) {
                        self.sportListTableVw.reloadData()
                    }
                    else {
                        self.nodataLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                        self.nodataLabel.isHidden = false
                        self.sportListTableVw.isHidden = true
                    }
                    customLoader.hideIndicator()
                }
                else {
                    let editdict = NSMutableDictionary.init(dictionary: self.selectDict)
                    editdict["voted"] = NSNumber.init(value: 1)
                    var count = Int.init(exactly: editdict["votes"] as! NSNumber)!
                    count += 1
                    editdict["votes"] = NSNumber.init(value: count)
                    self.listArray[self.selectIndex] = editdict
                    if (self.view.frame.width == 320) && (self.view.frame.height == 568)
                    {
                        self.selected5sCell.votedView.backgroundColor = ColorCode.FSThemeColor
                        self.selected5sCell?.votesLabel.text = "\(count)"
                    }
                    else{
                        self.selectedCell.votedView.backgroundColor = ColorCode.FSThemeColor
                        self.selectedCell?.votesLabel.text = "\(count)"
                    }
                    customLoader.hideIndicator()
                    
                    self.blurView.isHidden = false
                    self.centerView.isHidden = false
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fs")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flysportsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FSportsListVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (view.frame.width == 320) && (view.frame.height == 568)
        {
            let cell = sportListTableVw.dequeueReusableCell(withIdentifier: "sportlist5scell") as! SportList5sTableCell
            
            cell.serialNoLabel.text = "\(indexPath.section+1)"
            if (indexPath.section % 2 == 0)
            {
                cell.serialnoView.backgroundColor = ColorCode.FSThemeColor
            }
            else {
                cell.serialnoView.backgroundColor = ColorCode.FSViewBG
            }
            
            let dict = listArray[indexPath.section]
            
            cell.nameLabel.text = dict["name"] as? String
            cell.usernameLabel.text = dict["team"] as? String
            cell.votesLabel.text = "\(dict["votes"] as! NSNumber)"
            
            if (dict["voted"] as! NSNumber == 0) {
                cell.votedView.backgroundColor = ColorCode.FBGrayVote
            }
            else {
                cell.votedView.backgroundColor = ColorCode.FSThemeColor
            }
            
            if (indexPath.section == 0) {
                cell.goldImageVw.image = #imageLiteral(resourceName: "gold-medal")
            }
            else if (indexPath.section == 1) {
                cell.goldImageVw.image = #imageLiteral(resourceName: "FB_silvermedal")
            }
            else if (indexPath.section == 2) {
                cell.goldImageVw.image = #imageLiteral(resourceName: "FB_bronzemedal")
            }
            else {
                cell.goldImageVw.image = nil
            }
            cell.voteButton.addTarget(self, action: #selector(voteTheUser(_:)), for: .touchUpInside)
            
            return cell
        }
        else {
            let cell = sportListTableVw.dequeueReusableCell(withIdentifier: "sportlistcell") as! SportListTableCell
            
            cell.serialNoLabel.text = "\(indexPath.section+1)"
            if (indexPath.section % 2 == 0)
            {
                cell.serialnoView.backgroundColor = ColorCode.FSThemeColor
            }
            else {
                cell.serialnoView.backgroundColor = ColorCode.FSViewBG
            }
            
            let dict = listArray[indexPath.section]
            
            cell.nameLabel.text = dict["name"] as? String
            cell.usernameLabel.text = dict["team"] as? String
            cell.votesLabel.text = "\(dict["votes"] as! NSNumber)"
            
            if (dict["voted"] as! NSNumber == 0) {
                cell.votedView.backgroundColor = ColorCode.FBGrayVote
            }
            else {
                cell.votedView.backgroundColor = ColorCode.FSThemeColor
            }
            
            if (indexPath.section == 0) {
                cell.goldImageVw.image = #imageLiteral(resourceName: "gold-medal")
            }
            else if (indexPath.section == 1) {
                cell.goldImageVw.image = #imageLiteral(resourceName: "FB_silvermedal")
            }
            else if (indexPath.section == 2) {
                cell.goldImageVw.image = #imageLiteral(resourceName: "FB_bronzemedal")
            }
            else {
                cell.goldImageVw.image = nil
            }
            cell.voteButton.addTarget(self, action: #selector(voteTheUser(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (view.frame.width == 320) && (view.frame.height == 568)
        {
            return 75
        }
        else {
            return 95
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: sportListTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func voteTheUser(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: sportListTableVw)
        let idxpath = sportListTableVw.indexPathForRow(at: point)
        
        let dict = listArray[idxpath!.section]
        selectDict = dict
        selectIndex = idxpath!.section
        
        var vtVw : CustomUIView!
        if (view.frame.width == 320) && (view.frame.height == 568)
        {
            selected5sCell = sportListTableVw.cellForRow(at: idxpath!) as? SportList5sTableCell
            vtVw = selected5sCell.votedView
        }
        else {
            selectedCell = sportListTableVw.cellForRow(at: idxpath!) as? SportListTableCell
            vtVw = selectedCell.votedView
        }
        
        if (vtVw.backgroundColor == ColorCode.FSThemeColor)
        {
            ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_already_voted"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_you_vote_once_day"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
            var params : String = ""
            params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&vote_session=\(dict["voting_session"] as! NSNumber)&post_id=0&user_id=\(dict["id"] as! NSNumber)&type=football"
            self.callService(urlstr: Api.FB_VoteUser_URL, parameters: params, check: "vote")
        }
    }
}

class SportListTableCell : UITableViewCell
{
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var usernameLabel : UILabel!
    @IBOutlet weak var votesLabel : UILabel!
    @IBOutlet weak var serialNoLabel : UILabel!
    
    @IBOutlet weak var votedView : CustomUIView!
    @IBOutlet weak var voteButton : UIButton!
    
    @IBOutlet weak var goldImageVw : UIImageView!
    @IBOutlet weak var serialnoView : CustomUIView!
}

class SportList5sTableCell : UITableViewCell
{
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var usernameLabel : UILabel!
    @IBOutlet weak var votesLabel : UILabel!
    @IBOutlet weak var serialNoLabel : UILabel!
    
    @IBOutlet weak var votedView : CustomUIView!
    @IBOutlet weak var voteButton : UIButton!
    
    @IBOutlet weak var goldImageVw : UIImageView!
    @IBOutlet weak var serialnoView : CustomUIView!
}
