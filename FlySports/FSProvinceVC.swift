//
//  FSProvinceVC.swift
//  Darious
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks
import DropDown

class FSProvinceVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet weak var flagImg : UIImageView!
    
    @IBOutlet var dropdwonImg : [UIImageView]!
    @IBOutlet weak var provView : CustomUIView!
    @IBOutlet weak var provinceLabel : UILabel!
    @IBOutlet weak var catgView : CustomUIView!
    @IBOutlet weak var categoryLabel : UILabel!
    @IBOutlet weak var posView : CustomUIView!
    @IBOutlet weak var positionLabel : UILabel!
    @IBOutlet weak var leagueView : CustomUIView!
    @IBOutlet weak var leagueLabel : UILabel!
    
    @IBOutlet weak var sendBtn : KGHighLightedButton!
    
    @IBOutlet weak var provHt : NSLayoutConstraint!
    @IBOutlet weak var leagueHt : NSLayoutConstraint!
    @IBOutlet weak var leagueBtm : NSLayoutConstraint!
    
    let dropDown = DropDown()
    var passName : String = ""
    var passid : String = ""
    var flagiso : String = ""
    var votedate : String = ""
    
    var provNameArray : [String] = []
    var provIdArray : [String] = []
    var catgNameArray : [String] = []
    var catgIdArray : [String] = []
    var posNameArray : [String] = []
    var leaguesArray : [String] = []
    
    var dataDict : NSMutableDictionary = ["prov_id":"", "prov_name":"", "catg_id":"", "catg_name":"", "pos":"", "league" : ""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FSThemeColor
        
        leagueHt.constant = 0
        leagueBtm.constant = 0
        leagueView.isHidden = true
        
        for img in dropdwonImg
        {
            img.tintColor = ColorCode.FSDDColor
        }
        if (passName != "")
        {
            FS_SelectedCountry = passName
            FS_CountryID = passid
            FS_Flag_iso = flagiso
            FS_Vote_end_date = votedate.replacingOccurrences(of: "2", with: ", 2")
        }
        flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FS_Flag_iso).png"))
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        setBottomView()
        setLanguage()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&country_id=\(FS_CountryID)"
            self.callService(urlstr: Api.FS_Sports_List, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    func setLanguage()
    {
        provinceLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_province")
        categoryLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_category")
        positionLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_position")
        leagueLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_league")
        
        sendBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "send"), for: .normal)
    }
    
    @IBAction func onSend(_ sender: Any)
    {
        if (provinceLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_province")) && (provView.isHidden == false) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_select_province"), controller: self)
        }
        else if (categoryLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_category")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_select_category"), controller: self)
        }
        else if (leagueView.isHidden == false) && (leagueLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_league")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_select_league"), controller: self)
        }
        else if (positionLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fs_choose_position")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_select_position"), controller: self)
        }
        else {
            let listvc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSportsListVC") as! FSportsListVC
            listvc.passDict = dataDict
            present(listvc, animated: false, completion: nil)
        }
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flysport&screen_name=area&country_name=\(FS_SelectedCountry)&flag=\(FS_Flag_iso)&country=\(FS_CountryID)&province_id=&province=&category_id=&category=&position=&vote_date=\(FS_Vote_end_date.replacingOccurrences(of: ", ", with: ""))") else { return }
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flysport")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flysport")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (passName == "")
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            let homevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    @IBAction func chooseProvince(_ sender: Any)
    {
        if (posNameArray == [])
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&country_id=\(FS_CountryID)"
            self.callService(urlstr: Api.FS_Sports_List, parameters: params, check: "get_1")
        }
        else {
            if (provNameArray == [])
            {
                ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_no_province_available"), controller: self)
            }
            else {
                showDropDown(anchorVw: provView, passarr: provNameArray, passLabel: provinceLabel)
            }
        }
    }
    
    @IBAction func chooseCategory(_ sender: Any)
    {
        if (catgNameArray == [])
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_no_category_available"), controller: self)
        }
        else {
            showDropDown(anchorVw: catgView, passarr: catgNameArray, passLabel: categoryLabel)
        }
    }
    
    @IBAction func chooseLeague(_ sender: Any)
    {
        if (leaguesArray == [])
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_no_league_available"), controller: self)
        }
        else {
            showDropDown(anchorVw: leagueView, passarr: leaguesArray, passLabel: leagueLabel)
        }
    }
    
    @IBAction func choosePosition(_ sender: Any)
    {
        if (provNameArray == [])
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_no_position_available"), controller: self)
        }
        else {
            showDropDown(anchorVw: posView, passarr: posNameArray, passLabel: positionLabel)
        }
    }
    
    func showDropDown(anchorVw : CustomUIView, passarr : [String], passLabel : UILabel)
    {
        dropDown.anchorView = anchorVw
        dropDown.dataSource = passarr
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            cell.flagImage.image = nil
            cell.codeLabel.text = ""
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            passLabel.text = passarr[index]
            
            if (passLabel == self.provinceLabel) {
                self.dataDict["prov_id"] = self.provIdArray[index]
                self.dataDict["prov_name"] = passarr[index]
            }
            else if (passLabel == self.categoryLabel) {
                self.dataDict["catg_id"] = self.catgIdArray[index]
                self.dataDict["catg_name"] = passarr[index]
                if (passarr[index] == "Senior")
                {
                    self.provHt.constant = 0
                    self.provView.isHidden = true
                    self.leagueHt.constant = 55
                    self.leagueView.isHidden = false
                    self.leagueBtm.constant = 15
                }
                else {
                    self.dataDict["league"] = ""
                    self.leagueHt.constant = 0
                    self.leagueView.isHidden = true
                    self.provHt.constant = 55
                    self.provView.isHidden = false
                    self.leagueBtm.constant = 0
                }
            }
            else if (passLabel == self.positionLabel) {
                self.dataDict["pos"] = passarr[index]
            }
            else if (passLabel == self.leagueLabel) {
                self.dataDict["league"] = passarr[index]
            }
            else {}
            passLabel.textColor = ColorCode.FSDDColor
        }
        dropDown.width = anchorVw.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("area list = \(result)")
                self.provNameArray = []
                self.provIdArray = []
                self.catgNameArray = []
                self.catgIdArray = []
                self.posNameArray = []
                self.leaguesArray = []
                
                let provArr = result["province"] as! [NSDictionary]
                for dict in provArr
                {
                    self.provNameArray.append(dict["name"] as! String)
                    self.provIdArray.append("\(dict["id"] as! NSNumber)")
                }
                let catgArr = result["category"] as! [NSDictionary]
                for dict in catgArr
                {
                    self.catgNameArray.append(dict["name"] as! String)
                    self.catgIdArray.append("\(dict["id"] as! NSNumber)")
                }
                let strArr = result["playerPosition"] as! [String]
                for str in strArr
                {
                    let convertStr = ApiResponse.getLanguageFromUserDefaults(inputString: str)
                    self.posNameArray.append(convertStr)
                }
                let leagArr = result["leagues"] as! [String]
                for str in leagArr
                {
                    let convertStr = ApiResponse.getLanguageFromUserDefaults(inputString: str)
                    self.leaguesArray.append(convertStr)
                }
                
                if (check == "get_1") {
                    if (self.provNameArray == [])
                    {
                        ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fs_no_province_available"), controller: self)
                    }
                    else {
                        self.showDropDown(anchorVw: self.provView, passarr: self.provNameArray, passLabel: self.provinceLabel)
                    }
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fs")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flysportsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}
