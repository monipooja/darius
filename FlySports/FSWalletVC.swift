//
//  FSWalletVC.swift
//  Darious
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FSWalletVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    
    @IBOutlet weak var centerLbl: UILabel!
    @IBOutlet weak var setMyBalLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var addBtn: KGHighLightedButton!
    @IBOutlet weak var withdrawBtn: KGHighLightedButton!
    @IBOutlet weak var balanceTableView: UITableView!
    
    // Withdraw Money View Outlets
    @IBOutlet weak var  blurView: UIView!
    @IBOutlet weak var withdrawView: CustomUIView!
    @IBOutlet weak var transferLbl: UILabel!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var ibanTF: UITextField!
    @IBOutlet weak var bicCodeTF: UITextField!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var continueBtn: KGHighLightedButton!
    @IBOutlet weak var withdrawVwTop: NSLayoutConstraint!
    
    var walletArray : [[NSDictionary]] = []
    var pageNo : Int = 1
    var myBalance : NSNumber = 0
    var fromScreen : String = ""
    var originalYvalue : CGFloat = 0
    var textFArray : [UITextField] = []
    
    var matchDateArr : [String] = []
    var insertDateArr : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        balanceTableView.rowHeight = UITableView.automaticDimension
        balanceTableView.estimatedRowHeight = 70
        
        setLanguage()
        setWithdrawViewData()
        if (myBalance == 0)
        {
            setBottomView()
        }
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&page_no=\(self.pageNo)"
            self.callService(urlstr: Api.Wallet_History_URL, parameters: params, check: "get")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (myBalance != 0)
        {
            walletArray = []
            textFArray = []
            insertDateArr = []
            matchDateArr = []
            viewDidLoad()
        }
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        centerLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_wallet_found")
        setMyBalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "my_balance")
        transferLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "transfer_money")
        
        firstNameTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "first_name")
        lastNameTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "last_name")
        ibanTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "iban")
        bicCodeTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "bic_code")
        amountTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "amount")
        
        addBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "add_money")), for: .normal)
        withdrawBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "withdraw_money_button")), for: .normal)
        continueBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    // Set Withdraw View
    func setWithdrawViewData()
    {
        firstNameTF.text = userData["first_name"] as? String
        lastNameTF.text = userData["last_name"] as? String
        ibanTF.text = userData["bank_iban"] as? String
        bicCodeTF.text = userData["bank_bic"] as? String
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        blurView.addGestureRecognizer(gesture)
        
        originalYvalue = withdrawVwTop.constant
        
        textFArray = [firstNameTF, lastNameTF, ibanTF, bicCodeTF, amountTF]
        for tf in textFArray
        {
            addBottomLayerWithColor(bgcolor: UIColor.lightGray, textF: tf)
        }
        addBottomLayerWithColor(bgcolor: UIColor.init(hexString: themeColor), textF: firstNameTF)
    }
    
    // Mark : Gesture Action
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        self.view.endEditing(true)
        blurView.isHidden = true
        withdrawView.isHidden = true
        withdrawVwTop.constant = originalYvalue
    }
    
    func setBottomView()
    {
        if (self.view.frame.width == 375) && (self.view.frame.height == 667) {
            addBtn.titleLabel?.font = UIFont(name: "SegoeScript-Bold", size: 10)
            withdrawBtn.titleLabel?.font = UIFont(name: "SegoeScript-Bold", size: 10)
        }
        else if (self.view.frame.width == 320) && (self.view.frame.height == 568) {
            addBtn.titleLabel?.font = UIFont(name: "SegoeScript-Bold", size: 9)
            withdrawBtn.titleLabel?.font = UIFont(name: "SegoeScript-Bold", size: 9)
        }
        else {
            addBtn.titleLabel?.font = UIFont(name: "SegoeScript-Bold", size: 14)
            withdrawBtn.titleLabel?.font = UIFont(name: "SegoeScript-Bold", size: 14)
        }
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func addMoney(_ sender: Any)
    {
        DispatchQueue.main.async {
            if let link = URL(string: "\(Api.Base_URL)addMoney?uid=\(user_id)&lang=\(tag_Lang)&curr=\(Currency_Code)") {
                UIApplication.shared.open(link)
            }
        }
    }
    
    @IBAction func withdrawMoney(_ sender: Any)
    {
        if (Int(truncating: myBalance) < 1)
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "invalid_wallet_amount"), controller: self)
        }
        else {
            blurView.isHidden = false
            withdrawView.isHidden = false
        }
    }
    
    @IBAction func hideWithdrawView(_ sender: Any)
    {
        self.view.endEditing(true)
        blurView.isHidden = true
        withdrawView.isHidden = true
    }
    
    @IBAction func withdrawContinue(_ sender: Any)
    {
        self.view.endEditing(true)
        blurView.isHidden = true
        withdrawView.isHidden = true
        
        let enterAmount = amountTF.text
        
        if (amountTF.text == "") {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_fill_the_amount"), controller: self)
        }
        else if (Int(enterAmount!)! > Int(truncating: myBalance)) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "invalid_wallet_amount"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&amount=\(amountTF.text!)"
            self.callService(urlstr: Api.Withdraw_Money_URL, parameters: params, check: "withdraw")
        }
    }
    
    // Mark : Add Bottom Layer
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:1)
        }
        textF.layer.addSublayer(border)
    }
    
    // Mark : Arrange data according to date
    func arrangeDataWithDate(dataArr : [NSDictionary])
    {
        for dict in dataArr
        {
            var arr : [NSDictionary] = []
            
            let startStr = dict["created_on"] as? String
            if let _ = startStr!.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd MMM yyyy")
            {
                let matchDate = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "yyyy-MM-dd")
                let insertDate = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM yyyy")
                if (self.matchDateArr.count == 0) {
                    self.matchDateArr.append(matchDate!)
                    self.insertDateArr.append(insertDate!)
                    arr.append(dict)
                    self.walletArray.append(arr)
                } else {
                    if (self.matchDateArr.contains(matchDate!)) {
                        let idx = self.matchDateArr.index(of: matchDate!)
                        self.walletArray[idx!].append(dict)
                    } else {
                        self.matchDateArr.append(matchDate!)
                        self.insertDateArr.append(insertDate!)
                        arr.append(dict)
                        self.walletArray.append(arr)
                    }
                }
            }
        }
        balanceTableView.isHidden = false
        balanceTableView.reloadData()
    }
    
    // Mark : Call service
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            //   print("wallet result = \(result)")
            OperationQueue.main.addOperation {
                
                if(check == "get") {
                    
                    self.myBalance = result["userWalletAmount"] as! NSNumber
                    wallet_amount = result["userWalletAmount"] as! NSNumber
                    
                    let numberFormatter = NumberFormatter.init()
                    numberFormatter.numberStyle = .decimal
                    numberFormatter.maximumFractionDigits = 2
                    let formattedNumber = numberFormatter.string(from: wallet_amount)
                    
                    self.balanceLabel.text = "\((formattedNumber)!) \(Current_Currency)"
                    
                    userDefault.set(wallet_amount, forKey: "walletAmt")
                    userDefault.synchronize()
                    
                    if let _  = result["walletData"] as? [NSDictionary] {
                        let dataArray = result["walletData"] as! [NSDictionary]
                        self.arrangeDataWithDate(dataArr: dataArray)
                    }
                    if(self.walletArray.count > 0) {
                        self.centerLbl.isHidden = true
                    } else {
                        self.centerLbl.isHidden = false
                    }
                    customLoader.hideIndicator()
                }
                else if (check == "withdraw") {
                    customLoader.hideIndicator()
                    self.viewDidLoad()
                }
                else {
                    customLoader.hideIndicator()
                    if let _  = result["walletData"] as? [NSDictionary] {
                        let dataArray = result["walletData"] as! [NSDictionary]
                        self.arrangeDataWithDate(dataArr: dataArray)
                    }
                }
            }
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flysportsBottomViewAction(tagValue: sender.tag, controller: self)
    }
}

extension FSWalletVC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        withdrawVwTop.constant = originalYvalue - 120
        
        for tf in textFArray {
            if(tf == textField) {
                for sublyr in tf.layer.sublayers!
                {
                    if(sublyr.backgroundColor == UIColor.lightGray.cgColor){
                        sublyr.backgroundColor = UIColor.init(hexString: themeColor).cgColor
                        sublyr.frame.size.height = 1.5
                    }
                }
            }
            else {
                for sublyr in tf.layer.sublayers!
                {
                    if(sublyr.backgroundColor == UIColor.lightGray.cgColor) || (sublyr.backgroundColor == UIColor.init(hexString: themeColor).cgColor) {
                        sublyr.backgroundColor = UIColor.lightGray.cgColor
                        sublyr.frame.size.height = 0.5
                    }
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        withdrawVwTop.constant = originalYvalue
        return true
    }
}

extension FSWalletVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return insertDateArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (walletArray.count > 0) {
            return walletArray[section].count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = balanceTableView.dequeueReusableCell(withIdentifier: "fswalletcell") as! FSWalletTableCell
        
        let dict = (walletArray[indexPath.section])[indexPath.row]
        let descStr = dict["description"] as? String
        if (descStr?.contains("_") == true)
        {
            cell.descLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: descStr!)
        } else
        {
            cell.descLabel.text = dict["description"] as? String
        }
        
        let titleStr = dict["title"] as! String
        if (titleStr != "")
        {
            if (titleStr == "[testingtwo test] send_money_you")
            {
                let arr = titleStr.split(separator: "]")
                if (arr[1].replacingOccurrences(of: " ", with: "") == "send_money_you")
                {
                    cell.titleLabel.text = "\(arr[0]) \(ApiResponse.getLanguageFromUserDefaults(inputString: "send_money_you"))".replacingOccurrences(of: "[", with: "")
                }
            }
            else {
                let arr = titleStr.split(separator: "[")
                if (arr[0].replacingOccurrences(of: " ", with: "") == "you_send_money")
                {
                    cell.titleLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "you_send_money")) \(arr[1])".replacingOccurrences(of: "]", with: "")
                }
            }
        }
        else {
            cell.titleHt.constant = 0
            cell.titleLabel.text = ""
        }
        
        if let _ = dict["amount"] as? NSNumber
        {
            if let _ = dict["currency"] as? String
            {
                cell.totalPrcLabel.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", Double("\(dict["amount"] as! NSNumber)")!)) + " \(dict["currency"] as! String)"
            }
            else {
                cell.totalPrcLabel.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", Double("\(dict["amount"] as! NSNumber)")!)) + " \(Current_Currency)"
            }
        }
        
        if let _ = dict["type"] as? String
        {
            let type = dict["type"] as! String
            if (type == "credit") {
                cell.arrowImageVw.image = #imageLiteral(resourceName: "GreenArrow")
            } else {
                cell.arrowImageVw.image = #imageLiteral(resourceName: "OrangeArrow")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
        vw.backgroundColor = UIColor.white
        
        let dateLabel = UILabel.init(frame: CGRect(x: 20, y: 5, width: vw.frame.width-20, height: 25))
        
        dateLabel.text = insertDateArr[section]
        dateLabel.backgroundColor = UIColor.white
        dateLabel.textColor = UIColor.init(hexString: "999999")
        dateLabel.font = UIFont.init(name: "SegoeScript-Bold", size: 13)
        vw.addSubview(dateLabel)
        
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.row == walletArray[indexPath.section].count-1 && (walletArray[indexPath.section].count > 9){
            // Last cell is visible
            pageNo = pageNo + 1
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&page_no=\(self.pageNo)"
            self.callService(urlstr: Api.Wallet_History_URL, parameters: params, check: "load")
        }
    }
}


class FSWalletTableCell : UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var totalPrcLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var arrowImageVw: UIImageView!
    
    @IBOutlet weak var titleHt: NSLayoutConstraint!
}
