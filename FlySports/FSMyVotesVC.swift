//
//  FSMyVotesVC.swift
//  Darious
//
//  Created by Apple on 14/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks

class FSMyVotesVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet weak var voteTableVw : UITableView!
    @IBOutlet weak var flagImg : UIImageView!
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var nodataLabel : UILabel!
    
    @IBOutlet weak var provinceLabel : UILabel!
    @IBOutlet weak var categoryLabel : UILabel!
    @IBOutlet weak var positionLabel : UILabel!
    
    var passDict : NSMutableDictionary = [:]
    var voteListarray : [NSDictionary] = []
    var heightArray : [CGFloat] = []
    
    var passProv : String = ""
    var passCatg : String = ""
    var passPos : String = ""
    var passid : String = ""
    var passname : String = ""
    var flagiso : String = ""
    var votedate : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FSThemeColor
        
        if (passname != "")
        {
            FS_SelectedCountry = passname
            FS_CountryID = passid
            FS_Flag_iso = flagiso
            FS_Vote_end_date = votedate.replacingOccurrences(of: "2", with: ", 2")
        }
        flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FS_Flag_iso).png"))
        positionLabel.text = passPos
        categoryLabel.text = passCatg
        provinceLabel.text = passProv
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "my_votes")
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FSThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&type=football&list_type=people&country=\(FS_CountryID)"
            self.callService(urlstr: Api.FB_MyVotes_URL, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flysport&screen_name=fsvote&country_name=\(FS_SelectedCountry)&flag=\(FS_Flag_iso)&country=\(FS_CountryID)&province_id=&province=&category_id=&category=&position=&vote_date=\(FS_Vote_end_date.replacingOccurrences(of: ", ", with: ""))") else { return }
        
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flysport")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flysport")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (passname == "")
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            let homevc = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    // MARK : Service call and fetching data
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
              //  print("sport votes = \(result)")
                self.voteListarray = result["data"] as! [NSDictionary]
                if (self.voteListarray.count > 0) {
                    self.voteTableVw.isHidden = false
                    self.voteTableVw.reloadData()
                }
                else {
                    self.nodataLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                    self.nodataLabel.isHidden = false
                    self.voteTableVw.isHidden = true
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fs")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flysportsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}


extension FSMyVotesVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return voteListarray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = voteTableVw.dequeueReusableCell(withIdentifier: "sportvotecell") as! SportVoteTableCell
        
        let dict = voteListarray[indexPath.section]
        
        cell.serialNoLabel.text = "\(dict["rank"] as! NSNumber)"
        if (indexPath.section % 2 == 0)
        {
            cell.serialnoView.backgroundColor = ColorCode.FSThemeColor
        }
        else {
            cell.serialnoView.backgroundColor = ColorCode.FSViewBG
        }
        
        cell.nameLabel.text = dict["name"] as? String
        cell.usernameLabel.text = dict["team"] as? String
        cell.votesLabel.text = "\(dict["votes"] as! NSNumber)"
        
        // Set Medals
        if (dict["rank"] as! NSNumber == 1) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "gold-medal")
        }
        else if (dict["rank"] as! NSNumber == 2) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "FB_silvermedal")
        }
        else if (dict["rank"] as! NSNumber == 3) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "FB_bronzemedal")
        }
        else {
            cell.goldImageVw.image = nil
        }
        
        cell.thirdLabel.text = "\(dict["voting_session"] as! String)".replacingOccurrences(of: "##", with: "/")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: voteTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
}

class SportVoteTableCell : UITableViewCell
{
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var usernameLabel : UILabel!
    @IBOutlet weak var thirdLabel : UILabel!
    @IBOutlet weak var votesLabel : UILabel!
    @IBOutlet weak var serialNoLabel : UILabel!
    
    @IBOutlet weak var goldImageVw : UIImageView!
    @IBOutlet weak var serialnoView : CustomUIView!
}
