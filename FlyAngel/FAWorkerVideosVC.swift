//
//  FAWorkerVideosVC.swift
//  Darious
//
//  Created by Dario Carrasco on 01/07/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit
import youtube_ios_player_helper_swift
import WebKit

class FAWorkerVideosVC: UIViewController, YTPlayerViewDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet var centerLabel : UILabel!
    @IBOutlet var workerNameLbl : UILabel!
    @IBOutlet var workerImageVw : UIImageView!
    @IBOutlet var videoTableView : UITableView!
    
    var videoList : [NSDictionary] = []
    var passUserid : NSNumber = 0
    var passVid : String = ""
    var selectedSection : Int?
    var visibleIndex : [Int] = []
    var loaderarr : [customLoader] = []
    
    private var playerView: PlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FAThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        videoTableView.rowHeight = UITableView.automaticDimension
        videoTableView.estimatedRowHeight = 200
        
        workerImageVw.layer.cornerRadius = workerImageVw.frame.width/2
        
        centerLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_nodata_found")
        
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FAThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.passUserid)"
            self.callService(urlstr: Api.FA_Videos_URL, params: params)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if (passVid != "")
        {
            playerView.ytPlayerView.delegate = nil
            playerView.ytPlayerView.stopVideo()
        }
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fa")
    }
    
    // Mark : Open Angle List
    @IBAction func onGoBack(_ sender: Any)
    {
        if (passVid != "") {
            playerView.playStop(sender: playerView.btnPlayPause)
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onFAHome(_ sender: Any)
    {
        let homevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAHomeVC") as! FAHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    // MARK : Call Service
    func callService(urlstr : String, params : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: params, controller: self) { (result) in
            
            //  print("videos list data = \(result)")
            OperationQueue.main.addOperation {
                
                let dict = (result["allData"] as! [NSDictionary])[0]
                self.workerNameLbl.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
                self.workerImageVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                
                self.videoList = dict["videosList"] as! [NSDictionary]
                self.videoTableView.reloadData()
                if(self.videoList == []) {
                    self.centerLabel.isHidden = false
                } else {
                    self.centerLabel.isHidden = true
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Workers List
            let workervc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkersVC") as! FAWorkersVC
            present(workervc, animated: false, completion: nil)
            break
        case 2:
            // My Donation
            let mydonationvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAMyDonationVC") as! FAMyDonationVC
            present(mydonationvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWalletVC") as! FAWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FANotificationVC") as! FANotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
    
    // Mark : Get video id from You tube URL
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
    
    func setMMPlayer(passUrl : String, videoid : String, mainview : UIView, videoVw : CustomUIView)
    {
        passVid = videoid
        playerView = UINib(nibName: "PlayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PlayerView
        playerView.videoId = videoid
        addPlayerView(contentVW: mainview, videoVw: videoVw)
    }
    
    private func addPlayerView(contentVW : UIView, videoVw : CustomUIView){
        contentVW.addSubview(playerView)
        playerView.frame = videoVw.frame
        playerView.autoresizingMask = .flexibleWidth

        playerView.btnFullScreen.addTarget(self, action: #selector(enterFullScreen(_:)), for: .touchUpInside)
    }
    
    @objc func enterFullScreen(_ sender : UIButton)
    {
        if (playerView.isFullScreen == false)
        {
            let landscapevc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "LandscapeVC") as! LandscapeVC
            landscapevc.vId = passVid
            landscapevc.sliderCurrentVlaue = self.playerView.timeSlider.value
            self.present(landscapevc, animated: false, completion: nil)
        }
    }
}


extension FAWorkerVideosVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return videoList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = videoTableView.dequeueReusableCell(withIdentifier: "videocell", for: indexPath) as! VideoTableCell
        
        let dict = videoList[indexPath.section]
        cell.textLbl.text = dict["title"] as? String
        
        let str = (dict["created_on"] as! String).retreiveTimeInterval(check: "full")
        cell.dayLbl.text = "\(str) \(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_ago"))"
//        cell.dayLbl.text = "\(str) ago"
        
        let vid = self.getYoutubeId(youtubeUrl: dict["video_link"] as! String)
        cell.videoImg.sd_setImage(with: URL(string: "http://img.youtube.com/vi/\(vid!)/0.jpg"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        if let _ = selectedSection
        {
            if (indexPath.section != selectedSection)
            {
                cell.playBtn.isHidden = false
                cell.videoImg.isHidden = false
            }
        }
        cell.playBtn.isHidden = true
        cell.videoImg.isHidden = true
        
        if (visibleIndex.contains(indexPath.section) == false)
        {
           // customLoader.showActivityIndicator(showColor: ColorCode.FAThemeColor, controller: cell.contentView)
            let webvw = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
            webvw.frame.size = CGSize(width: cell.videoView.frame.width, height: cell.videoView.frame.height)
            let xValue = ((videoTableView.frame.width - 30) - webvw.frame.width)/2
            let yValue = cell.videoView.frame.origin.y
            webvw.frame.origin = CGPoint(x: xValue, y: yValue)
            //let url = URL(string: "https://www.youtube.com/embed/\(vid!)")
            let url = URL(string: "https://www.youtube.com/embed/HjCT9EmSF-Q")
            webvw.load(URLRequest(url: url!))
            webvw.navigationDelegate = self
            cell.mainView.addSubview(webvw)
        }
      //  cell.playBtn.addTarget(self, action: #selector(playvideo(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 3))
        vw.backgroundColor = UIColor.clear
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 3
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       // print("index = \(indexPath.section)")
        
        if (visibleIndex.count > 0) && (visibleIndex.contains(indexPath.section))
        {}
        else {
            visibleIndex.append(indexPath.section)
        }
    }
    
    @objc func playvideo(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: videoTableView)
        let idxpath = videoTableView.indexPathForRow(at: point)
        let cell = videoTableView.cellForRow(at: idxpath!) as! VideoTableCell
        let dict = videoList[idxpath!.section]
        
        if let _ = selectedSection
        {
            let prevCell = videoTableView.cellForRow(at: IndexPath(row: 0, section: selectedSection!)) as! VideoTableCell
            
            if #available(iOS 11.0, *) {
                if prevCell.mainView.contains(playerView)
                {
                    playerView.removeFromSuperview()
                }
            } else {
                // Fallback on earlier versions
            }
        }
        
        selectedSection = idxpath?.section
        cell.playBtn.isHidden = true
        cell.videoImg.isHidden = true
        
        videoTableView.reloadData()
        
        let webvw = WKWebView.init()
        webvw.frame = cell.videoView.frame

        let url = URL(string: "https://www.youtube.com/embed/HjCT9EmSF-Q")
        webvw.load(URLRequest(url: url!))

        cell.mainView.addSubview(webvw)
        
//        let vid = self.getYoutubeId(youtubeUrl: dict["video_link"] as! String)
//        setMMPlayer(passUrl: dict["video_link"] as! String, videoid: vid!, mainview: cell.mainView, videoVw: cell.videoView)
       // print("linkkkk = \(dict["video_link"] as! String)")
        
//        let vid = self.getYoutubeId(youtubeUrl: "https://www.youtube.com/watch?v=_KhQT-LGb-4")
//        setMMPlayer(passUrl: "https://www.youtube.com/watch?v=_KhQT-LGb-4", videoid: vid!, mainview: cell.mainView, videoVw: cell.videoView)
    }
}

class VideoTableCell: UITableViewCell {
    
    @IBOutlet var textLbl : UILabel!
    @IBOutlet var dayLbl : UILabel!
    @IBOutlet var playBtn : UIButton!
    @IBOutlet var videoImg : UIImageView!
    @IBOutlet var videoView : CustomUIView!
    @IBOutlet var mainView : CustomUIView!
}

extension FAWorkerVideosVC : WKNavigationDelegate
{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
       // print("finish web view loading")
       // customLoader.hideIndicator()
    }
}



