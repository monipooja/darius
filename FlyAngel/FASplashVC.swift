//
//  FASplashVC.swift
//  Darious
//
//  Created by Dario Carrasco on 27/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FASplashVC: UIViewController {

    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FAThemeColor)
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(gotoNext), userInfo: nil, repeats: false)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func gotoNext()
    {
        timer.invalidate()
        let homevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAHomeVC") as! FAHomeVC
        present(homevc, animated: false, completion: nil)
    }
}
