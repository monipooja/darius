//
//  FAAngleListVC.swift
//  Darious
//
//  Created by Dario Carrasco on 28/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FAAngleListVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet var centerLabel : UILabel!
    @IBOutlet weak var angleLabel : UILabel!
    @IBOutlet weak var beAnAngel : KGHighLightedButton!
    @IBOutlet weak var angelTableView : UITableView!
    
    var angelList : [NSDictionary] = []
    var passuid : NSNumber = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FAThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        setLanguage()
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FAThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&worker_id=\(self.passuid)"
            self.callService(urlstr: Api.FA_AllAngels_URL, params: params)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    func setLanguage()
    {
        angleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_angels")
        beAnAngel.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_be_an_angel"), for: .normal)
        centerLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_nodata_found")
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fa")
    }
    
    // Mark : Open Donate Screen
    @IBAction func beAnAngel(_ sender: Any)
    {
        let donatevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FADonateVC") as! FADonateVC
        donatevc.passuid = passuid
        present(donatevc, animated: false, completion: nil)
    }
    
    @IBAction func onGoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onFAHome(_ sender: Any)
    {
        let homevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAHomeVC") as! FAHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    // MARK : Call Service
    func callService(urlstr : String, params : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: params, controller: self) { (result) in
            
            // print("all angel = \(result)")
            OperationQueue.main.addOperation {
                
                if let _ = result["data"] as? [NSDictionary]
                {
                    self.angelList = result["data"] as! [NSDictionary]
                    self.angelTableView.isHidden = false
                    self.angelTableView.reloadData()
                    if(self.angelList == []) {
                        self.centerLabel.isHidden = false
                    } else {
                        self.centerLabel.isHidden = true
                    }
                }
                else {
                    self.angelTableView.isHidden = true
                    self.centerLabel.isHidden = false
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Workers List
            let workervc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkersVC") as! FAWorkersVC
            present(workervc, animated: false, completion: nil)
            break
        case 2:
            // My Donation
            let mydonationvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAMyDonationVC") as! FAMyDonationVC
            present(mydonationvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWalletVC") as! FAWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FANotificationVC") as! FANotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }

}

extension FAAngleListVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return angelList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = angelTableView.dequeueReusableCell(withIdentifier: "angelscell") as! AngelsTableCell
        
        let dict = angelList[indexPath.row]
        
        cell.nameLabel.text = "\(dict["fullname"] as! String)"
        cell.permonthLabel.text = "\(dict["donation_amount"] as! String) \(Current_Currency)/\(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_month"))"
//        cell.permonthLabel.text = "\(dict["donation_amount"] as! String) €/month"
        
        if let _ = dict["created_on"] as? String
        {
            let daysStr = (dict["created_on"] as! String).retreiveTimeInterval(check: "full")
            cell.dayLabel.text = "\(daysStr) \(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_ago"))"
//            cell.dayLabel.text = "\(daysStr) ago"
            
        } else {
            cell.dayLabel.isHidden = true
        }
        
        if(indexPath.row == angelList.count-1)
        {
            cell.btmLabel.isHidden = true
        }
        
        cell.profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        cell.profileImgVw.layer.cornerRadius = cell.profileImgVw.frame.width/2
        
        return cell
    }
}

class AngelsTableCell: UITableViewCell {
    
    @IBOutlet weak var profileImgVw : customImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var permonthLabel : UILabel!
    @IBOutlet weak var dayLabel : UILabel!
    @IBOutlet weak var btmLabel : UILabel!
}
