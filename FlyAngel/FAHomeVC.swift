//
//  FAHomeVC.swift
//  Darious
//
//  Created by Dario Carrasco on 28/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FAHomeVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var centreImageHt : NSLayoutConstraint!
    @IBOutlet var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var jobsTableView : UITableView!
    @IBOutlet weak var headerTableview : UIView!
    
    @IBOutlet weak var jobLabel : UILabel!
    @IBOutlet weak var jobCountLabel : UILabel!
    @IBOutlet weak var angelLabel : UILabel!
    @IBOutlet weak var angelCountLabel : UILabel!
    @IBOutlet weak var creatingLabel : UILabel!
    @IBOutlet weak var msgLabel : UILabel!
    @IBOutlet weak var helpLabel : UILabel!
    @IBOutlet weak var searchTF : KGHighLightedField!
    @IBOutlet weak var advantageBtn : KGHighLightedButton!
    
    var workerList : [NSDictionary] = []
    var searchStr : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FAThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        jobsTableView.rowHeight = UITableView.automaticDimension
        jobsTableView.estimatedRowHeight = 315
        
        jobCountLabel.layer.masksToBounds = true
        jobCountLabel.layer.cornerRadius = jobCountLabel.frame.width/2
        angelCountLabel.layer.masksToBounds = true
        angelCountLabel.layer.cornerRadius = angelCountLabel.frame.width/2
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        headerTableview.addGestureRecognizer(gesture)
        
        setLangue()
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FAThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&worker_search_text="
            self.callService(urlstr: Api.FA_WorkerList_URL, params: params)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        headerTableview.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    func setLangue()
    {
        jobLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_jobs")
        angelLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_angels")
        creatingLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_create_jobs")
        helpLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_help_create_job")
        searchTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_search")
        advantageBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_advantages"), for: .normal)
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fa")
    }
    
    // Mark : Open Angle List
    @IBAction func openAngelList(_ sender: Any)
    {
    }
    
    // Mark : Open Angle List
    @IBAction func openAdvantageMore(_ sender: Any)
    {
        let readvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAReadMoreVC") as! FAReadMoreVC
        present(readvc, animated: true, completion: nil)
    }
    
    // Mark : Open Angle List
    @IBAction func onGotoShare(_ sender: Any)
    {
        let objectsToShare = ["\(ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flyangel"))\n\(Api.Base_URL)flyangel/installApplication"] as [Any]
        
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

        activityVC.popoverPresentationController?.sourceView = sender as! UIButton
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @IBAction func onFAHome(_ sender: Any)
    {
        let homevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAHomeVC") as! FAHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
    }
    
    // MARK : Call Service
    func callService(urlstr : String, params : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: params, controller: self) { (result) in
            
            // print("all post angel = \(result)")
            OperationQueue.main.addOperation {
                if let _ = result["allData"] as? [NSDictionary]
                {
                    self.workerList = result["allData"] as! [NSDictionary]
                    self.jobsTableView.reloadData()
                    if(self.workerList == [])
                    {
                        if (self.jobsTableView.frame.origin.y == 88)
                        {
                            self.jobsTableView.frame.origin.y -= 200
                        }
                    }
                    else {
                        self.jobsTableView.frame.origin.y = 88
                    }
                }
                customLoader.hideIndicator()
            }
        }
    }

    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Workers List
            let workervc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkersVC") as! FAWorkersVC
            present(workervc, animated: false, completion: nil)
            break
        case 2:
            // My Donation
            let mydonationvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAMyDonationVC") as! FAMyDonationVC
            present(mydonationvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWalletVC") as! FAWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FANotificationVC") as! FANotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FAHomeVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(string != "") {
            searchStr = searchStr.appending(string)
        } else {
            searchStr.removeLast()
        }
      //  print("search = \(searchStr)")
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&worker_search_text=\(searchStr)"
        self.callService(urlstr: Api.FA_WorkerList_URL, params: params)
        
        return true
    }
}

extension FAHomeVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return workerList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = jobsTableView.dequeueReusableCell(withIdentifier: "jobscell") as! JobsTableCell
        
        let dict = workerList[indexPath.section]
        
        cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        
        let totAmt = "\((dict["total_amount"] as? NSNumber)!)"
        let description = (dict["description"] as? String)!
        let percentage : Float = (Float(totAmt)!) / (Float(description)!)
        
        cell.progressBar.setProgress(percentage, animated: false)
        cell.progressView.layer.borderWidth = 1
        cell.progressView.layer.borderColor = UIColor.init(hexString: "EEEEEE").cgColor
        cell.progressView.layer.cornerRadius = 6
        cell.progressBar.layer.cornerRadius = 4
        cell.progressBar.clipsToBounds = true
        
        cell.raisedLabel.text = "\(String(format: "%.2f", percentage))% \(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_raised"))"
//        cell.raisedLabel.text = "\(String(format: "%.2f", percentage))% raised"
        cell.ofLabel.text = ""
        
        cell.angleBtn.setTitle("\(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_angels")) : \(dict["angels"] as! NSNumber)", for: .normal)
//        cell.angleBtn.setTitle("Angels : \(dict["angels"] as! NSNumber)", for: .normal)
        
        cell.profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        cell.profileImgVw.layer.cornerRadius = cell.profileImgVw.frame.width/2
        
        cell.seeProfile.addTarget(self, action: #selector(gotoProfile(_:)), for: .touchUpInside)
        cell.beAnAngel.addTarget(self, action: #selector(gotoDonate(_:)), for: .touchUpInside)
        cell.angleBtn.addTarget(self, action: #selector(gotoAngelList(_:)), for: .touchUpInside)
        
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            cell.locationTopConst.constant = -2
        }
        cell.setLanguage()
        
        return cell
    }
    
    @objc func gotoProfile(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: jobsTableView)
        let idxpath = jobsTableView.indexPathForRow(at: point)
        let dict = workerList[idxpath!.section]
        
        let profilevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkerProfileVC") as! FAWorkerProfileVC
        profilevc.passUid = dict["id"] as! NSNumber
        present(profilevc, animated: false, completion: nil)
    }
    
    @objc func gotoDonate(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: jobsTableView)
        let idxpath = jobsTableView.indexPathForRow(at: point)
        let dict = workerList[idxpath!.section]
        
        let donatevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FADonateVC") as! FADonateVC
        donatevc.passuid = dict["id"] as! NSNumber
        present(donatevc, animated: false, completion: nil)
    }
    
    @objc func gotoAngelList(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: jobsTableView)
        let idxpath = jobsTableView.indexPathForRow(at: point)
        let dict = workerList[idxpath!.section]
        
        let angelvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAAngleListVC") as! FAAngleListVC
        angelvc.passuid = dict["id"] as! NSNumber
        present(angelvc, animated: false, completion: nil)
    }
}

class JobsTableCell: UITableViewCell {
    
    @IBOutlet weak var profileImgVw : customImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var raisedLabel : UILabel!
    @IBOutlet weak var ofLabel : UILabel!
    @IBOutlet weak var progressBar : UIProgressView!
    @IBOutlet var progressView : UIView!
    @IBOutlet weak var locationTopConst : NSLayoutConstraint!
    
    @IBOutlet weak var seeProfile : KGHighLightedButton!
    @IBOutlet weak var angleBtn : KGHighLightedButton!
    @IBOutlet weak var beAnAngel : KGHighLightedButton!
    
    func setLanguage()
    {
        seeProfile.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_see_profile"), for: .normal)
        beAnAngel.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_angel_for_him"), for: .normal)
    }
}
