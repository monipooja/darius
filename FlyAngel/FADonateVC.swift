//
//  FADonateVC.swift
//  Darious
//
//  Created by Apple on 03/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FADonateVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var contentVwHt : NSLayoutConstraint!
    @IBOutlet var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet var profileImageView : UIImageView!
    @IBOutlet var contentView : UIView!
    @IBOutlet var progressView : UIView!
    
    @IBOutlet weak var donateBtn: KGHighLightedButton!
    
    @IBOutlet var nameLabel : UIButton!
    @IBOutlet var shareLabel : UILabel!
    @IBOutlet var raisedLabel : UILabel!
    @IBOutlet var amountLabel : UILabel!
    @IBOutlet var timeagoLabel : UILabel!
    @IBOutlet var helpLabel : UILabel!
    @IBOutlet var profileLabel : UILabel!
    @IBOutlet var progressBar : UIProgressView!
    
    var contentFinalHt : CGFloat = 0
    var passuid : NSNumber = 0
    var visible : String = "yes"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FAThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        profileImageView.layer.cornerRadius = profileImageView.frame.width/2
        contentFinalHt = contentVwHt.constant
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        contentView.addGestureRecognizer(gesture)
        
        setLangue()
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FAThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.passuid)"
            self.callService(urlstr: Api.FA_DonationAngel_URL, params: params)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        contentView.endEditing(true)
        contentVwHt.constant = contentFinalHt
    }
    
    func setLangue()
    {
        helpLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_help_create_job")
        profileLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_see_profile")
        donateBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_donate"), for: .normal)
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fa")
    }
    
    // Mark : Doante submit
    @IBAction func onDonate(_ sender: Any)
    {
        DispatchQueue.main.async {
            if let link = URL(string: "\(Api.Donate_URL)?uid=\(user_id)&petition_id=\(self.passuid)&type=flyangel") {
               // print("donate link = \(link)")
                UIApplication.shared.open(link)
            }
        }
    }
    
    // Mark : Share
    @IBAction func onShare(_ sender: Any)
    {
        let objectsToShare = ["\(ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flyangel"))\n\(Api.Base_URL)flyangel/installApplication"] as [Any]
        
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = sender as! UIButton
        self.present(activityVC, animated: true, completion: nil)
    }
    
    // Mark : See profile
    @IBAction func onGotoProfile(_ sender: Any)
    {
        let profilevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkerProfileVC") as! FAWorkerProfileVC
        profilevc.passUid = passuid
        present(profilevc, animated: false, completion: nil)
    }
    
    // Mark : Open Angle List
    @IBAction func onGoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onFAHome(_ sender: Any)
    {
        let homevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAHomeVC") as! FAHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    func setData(passDict : NSDictionary)
    {
        let nameStr = "\(passDict["first_name"] as! String) \(passDict["last_name"] as! String)"
        nameLabel.setTitle(nameStr, for: .normal)
        nameLabel.layer.shadowColor = UIColor.lightGray.cgColor
        nameLabel.layer.shadowRadius = 2
        nameLabel.layer.shadowOpacity = 0.6
        nameLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        nameLabel.layer.cornerRadius = 12
        nameLabel.titleEdgeInsets = UIEdgeInsets(top: 0, left: 3, bottom: 0, right: 3)
        
        profileImageView.sd_setImage(with: URL(string: "\(Image_URL)\(passDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        
        if let _ = passDict["lastDonationDate"] as? String
        {
            let daysStr = (passDict["lastDonationDate"] as! String).retreiveTimeInterval(check: "half")
            timeagoLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_last_donation")) \(daysStr) \(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_ago"))"
//            timeagoLabel.text = "Last donation \(daysStr) ago"
            
        } else {
            timeagoLabel.isHidden = true
        }
        
        let totAmt = "\((passDict["totalDonationAmount"] as? NSNumber)!)"
        let description = (passDict["description"] as? String)!
        let percentage : Float = (Float(totAmt)!) / (Float(description)!)
        
        progressBar.setProgress(percentage, animated: false)
        progressView.layer.borderWidth = 1
        progressView.layer.borderColor = UIColor.init(hexString: "EEEEEE").cgColor
        progressView.layer.cornerRadius = 6
        progressBar.layer.cornerRadius = 4
        progressBar.clipsToBounds = true
        
        raisedLabel.text = "\(String(format: "%.2f", percentage))% \(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_raised"))"
//        raisedLabel.text = "\(String(format: "%.2f", percentage))% raised"
        amountLabel.text = ""
        shareLabel.text = "0 \(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_share"))"
//        shareLabel.text = "0 share"
    }
    
    // MARK : Call Service
    func callService(urlstr : String, params : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: params, controller: self) { (result) in
            
            //  print("donation = \(result)")
            OperationQueue.main.addOperation {
                let dict = result["workerDetails"] as! NSDictionary
                self.setData(passDict: dict)
                customLoader.hideIndicator()
            }
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Workers List
            let workervc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkersVC") as! FAWorkersVC
            present(workervc, animated: false, completion: nil)
            break
        case 2:
            // My Donation
            let mydonationvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAMyDonationVC") as! FAMyDonationVC
            present(mydonationvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWalletVC") as! FAWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FANotificationVC") as! FANotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FADonateVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        contentVwHt.constant = contentFinalHt
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        contentVwHt.constant = contentFinalHt + 100
        contentView.accessibilityScroll(.up)
    }
}
