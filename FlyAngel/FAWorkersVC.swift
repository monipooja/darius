//
//  FAWorkersVC.swift
//  Darious
//
//  Created by Dario Carrasco on 28/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FAWorkersVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet var centerLabel : UILabel!
    @IBOutlet var searchTf : KGHighLightedField!
    @IBOutlet var workersCollectionView : UICollectionView!
    
    var workerList : [NSDictionary] = []
    var searchStr : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FAThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        searchTf.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_search")
        centerLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_nodata_found")
        
        setBottomView()
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        workersCollectionView.addGestureRecognizer(gesture)
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FAThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&worker_search_text="
            self.callService(urlstr: Api.FA_WorkerList_URL, params: params)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        workersCollectionView.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fa")
    }
    
    // Mark : Open Angle List
    @IBAction func onGoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onFAHome(_ sender: Any)
    {
        let homevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAHomeVC") as! FAHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    // MARK : Call Service
    func callService(urlstr : String, params : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: params, controller: self) { (result) in
            
            // print("all worker = \(result)")
            OperationQueue.main.addOperation {
                if let _ = result["allData"] as? [NSDictionary]
                {
                    self.workerList = result["allData"] as! [NSDictionary]
                    self.workersCollectionView.reloadData()
                    if(self.workerList == []) {
                        self.centerLabel.isHidden = false
                    } else {
                        self.centerLabel.isHidden = true
                    }
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Workers List
            let workervc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkersVC") as! FAWorkersVC
            present(workervc, animated: false, completion: nil)
            break
        case 2:
            // My Donation
            let mydonationvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAMyDonationVC") as! FAMyDonationVC
            present(mydonationvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWalletVC") as! FAWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FANotificationVC") as! FANotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FAWorkersVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(string != "") {
            searchStr = searchStr.appending(string)
        } else {
            searchStr.removeLast()
        }
     //   print("search = \(searchStr)")
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&worker_search_text=\(searchStr)"
        self.callService(urlstr: Api.FA_WorkerList_URL, params: params)
        
        return true
    }
}

extension FAWorkersVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return workerList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = workersCollectionView.dequeueReusableCell(withReuseIdentifier: "workercell", for: indexPath) as! WorkersCollectionCell
        
        let dict = workerList[indexPath.item]
        
        cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        
        cell.profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        cell.profileImgVw.layer.cornerRadius = cell.profileImgVw.frame.width/2
        
       cell.seeProfile.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_see_profile"), for: .normal)
        cell.seeProfile.addTarget(self, action: #selector(gotoProfile(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: workersCollectionView.frame.width, height: 191)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            let headerView = workersCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "wrokerheader", for: indexPath as IndexPath) as! WorkerHeaderView
            
             headerView.workerLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_worker")
            
            return headerView
        default:
            fatalError("Unexpected element kind")
        }
    }
    
    @objc func gotoProfile(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: workersCollectionView)
        let idxpath = workersCollectionView.indexPathForItem(at: point)
        let dict = workerList[idxpath!.item]
        
        let profilevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkerProfileVC") as! FAWorkerProfileVC
        profilevc.passUid = dict["id"] as! NSNumber
        present(profilevc, animated: false, completion: nil)
    }
}

class WorkersCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var profileImgVw : customImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var seeProfile : KGHighLightedButton!
}

class WorkerHeaderView: UICollectionReusableView {
    
    @IBOutlet var workerLabel : UILabel!
}
