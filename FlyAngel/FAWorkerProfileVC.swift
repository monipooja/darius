//
//  FAWorkerProfileVC.swift
//  Darious
//
//  Created by Dario Carrasco on 01/07/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FAWorkerProfileVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet var workerNameLbl : UILabel!
    @IBOutlet var workerImageVw : UIImageView!
    @IBOutlet var countryLbl : UILabel!
    @IBOutlet var ageLbl : UILabel!
    @IBOutlet var withusLbl : UILabel!
    @IBOutlet var knowledgeLbl : UILabel!
    @IBOutlet var knowledgeTxtVw : UITextView!
    @IBOutlet var functionsLbl : UILabel!
    @IBOutlet var functionTxtVw : UITextView!
    @IBOutlet var angelsLbl : UILabel!
    
    @IBOutlet var readAboutBtn : UIButton!
    @IBOutlet var videoBtn : UIButton!
    @IBOutlet var pictureBtn : UIButton!
    @IBOutlet weak var beAnAngel : KGHighLightedButton!
    
    @IBOutlet var leftLblCollection : [customLabelView]!
    @IBOutlet var rightLblCollection : [customLabelView]!
    
    var passUid : NSNumber = 0
    var proifleDict : NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FAThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        let lblarr = [ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_country"), ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_age"), ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_with_us_since"), ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_knowledge"), ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_actual_functions"), ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_number_of_angels")]
        for ii in 0...leftLblCollection.count-1
        {
            leftLblCollection[ii].text = "\(lblarr[ii]):"
            leftLblCollection[ii].layer.cornerRadius = 12
            rightLblCollection[ii].layer.cornerRadius = 12
            if #available(iOS 11.0, *) {
                leftLblCollection[ii].layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
                rightLblCollection[ii].layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
                knowledgeTxtVw.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
                functionTxtVw.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            } else {
                // Fallback on earlier versions
                setMarkCorner(bounds: leftLblCollection[ii].bounds, corners: [.topLeft, .topRight], lab: leftLblCollection[ii], textVw: nil)
                setMarkCorner(bounds: rightLblCollection[ii].bounds, corners: [.topRight, .bottomRight], lab: rightLblCollection[ii], textVw: nil)
                setMarkCorner(bounds: knowledgeTxtVw.bounds, corners: [.topRight, .bottomRight], lab: nil, textVw: knowledgeTxtVw)
                setMarkCorner(bounds: functionTxtVw.bounds, corners: [.topRight, .bottomRight], lab: nil, textVw: functionTxtVw)
            }
        }
        workerImageVw.layer.cornerRadius = workerImageVw.frame.width/2
        
        setLanguage()
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FAThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.passUid)"
            self.callService(urlstr: Api.FA_Profile_URL, params: params)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    // MARK : mask corner
    func setMarkCorner(bounds : CGRect, corners : UIRectCorner, lab : UILabel?, textVw : UITextView?)
    {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: 12, height: 12))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        
        if let _ = lab
        {
            lab!.layer.mask = mask
            lab!.layer.masksToBounds = true
        }
        else {
            textVw!.layer.mask = mask
            textVw!.layer.masksToBounds = true
        }
    }
    
    func setLanguage()
    {
        readAboutBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_read_about"), for: .normal)
        videoBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_videos"), for: .normal)
        pictureBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_pictures"), for: .normal)
        beAnAngel.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_be_an_angel"), for: .normal)
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fa")
    }
    
    // Mark : Open Donate Screen
    @IBAction func beAnAngel(_ sender: Any)
    {
        let donatevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FADonateVC") as! FADonateVC
        donatevc.passuid = passUid
        present(donatevc, animated: false, completion: nil)
    }
    
    @IBAction func onGotoVideosList(_ sender: Any)
    {
        let videovc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkerVideosVC") as! FAWorkerVideosVC
        videovc.passUserid = passUid
        present(videovc, animated: false, completion: nil)
    }
    
    @IBAction func onGotoPictureList(_ sender: Any)
    {
        let picturevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkerPictureVC") as! FAWorkerPictureVC
        picturevc.passUserid = passUid
        picturevc.dataPass = self.proifleDict
        present(picturevc, animated: false, completion: nil)
    }
    
    @IBAction func onGotoReadList(_ sender: Any)
    {
        let readvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkerReadVC") as! FAWorkerReadVC
        readvc.passUserid = passUid
        present(readvc, animated: false, completion: nil)
    }
    
    // Mark : Open Angle List
    @IBAction func onGoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onFAHome(_ sender: Any)
    {
        let homevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAHomeVC") as! FAHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    func setData(passDict : NSDictionary)
    {
        proifleDict = passDict
        workerNameLbl.text = "\(passDict["first_name"] as! String) \(passDict["last_name"] as! String)"
        workerImageVw.sd_setImage(with: URL(string: "\(Image_URL)\(passDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        
        if let _ = passDict["created_on"] as? String
        {
            withusLbl.text = (passDict["created_on"] as! String).retreiveTimeInterval(check: "full")
        }
        
        countryLbl.text = ""
        ageLbl.text = ""
        knowledgeTxtVw.text = passDict["knowledge"] as? String
        functionTxtVw.text = passDict["actual_functions"] as? String
        angelsLbl.text = "\(passDict["totalCountAngels"] as! NSNumber)"
        
        knowledgeTxtVw.contentInset = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 0);
        functionTxtVw.contentInset = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 0);
    }
    
    // MARK : Call Service
    func callService(urlstr : String, params : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: params, controller: self) { (result) in
            
            //  print("profile data = \(result)")
            OperationQueue.main.addOperation {
                self.setData(passDict: result["allData"] as! NSDictionary)
                customLoader.hideIndicator()
            }
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Workers List
            let workervc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkersVC") as! FAWorkersVC
            present(workervc, animated: false, completion: nil)
            break
        case 2:
            // My Donation
            let mydonationvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAMyDonationVC") as! FAMyDonationVC
            present(mydonationvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWalletVC") as! FAWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FANotificationVC") as! FANotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}
