//
//  FAWorkerPictureVC.swift
//  Darious
//
//  Created by Dario Carrasco on 01/07/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FAWorkerPictureVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet var centerLabel : UILabel!
    @IBOutlet var workerNameLbl : UILabel!
    @IBOutlet var workerImageVw : UIImageView!
    @IBOutlet var pictureCollectionView : UICollectionView!
    
    var picsArray : [NSDictionary] = []
    var passUserid : NSNumber = 0
    var dataPass : NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FAThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        workerImageVw.layer.cornerRadius = workerImageVw.frame.width/2
        workerNameLbl.text = "\(dataPass["first_name"] as! String) \(dataPass["last_name"] as! String)"
        workerImageVw.sd_setImage(with: URL(string: "\(Image_URL)\(dataPass["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        
        centerLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_nodata_found")
        
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FAThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.passUserid)"
            self.callService(urlstr: Api.FA_Picture_URL, params: params)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fa")
    }
    
    // Mark : Open Angle List
    @IBAction func onGoBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onFAHome(_ sender: Any)
    {
        let homevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAHomeVC") as! FAHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    // MARK : Call Service
    func callService(urlstr : String, params : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: params, controller: self) { (result) in
            
            // print("picture list data = \(result)")
            OperationQueue.main.addOperation {
                if let dictArr = result["allData"] as? [NSDictionary]
                {
                    self.centerLabel.isHidden = true
                    self.picsArray = dictArr
                }
                else {
                    self.centerLabel.isHidden = false
                }
                self.pictureCollectionView.reloadData()
                customLoader.hideIndicator()
            }
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Workers List
            let workervc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWorkersVC") as! FAWorkersVC
            present(workervc, animated: false, completion: nil)
            break
        case 2:
            // My Donation
            let mydonationvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAMyDonationVC") as! FAMyDonationVC
            present(mydonationvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAWalletVC") as! FAWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FANotificationVC") as! FANotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FAWorkerPictureVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = pictureCollectionView.dequeueReusableCell(withReuseIdentifier: "picturecell", for: indexPath) as! PictureCollectionCell
        
        let dict = picsArray[indexPath.item]
        
        cell.pictureImgVw.layer.borderWidth = 0.5
        cell.pictureImgVw.layer.borderColor = UIColor.lightGray.cgColor
        cell.pictureImgVw.sd_setImage(with: URL(string: "\(Api.FA_WorkerPic_URL)\(dict["worker_images"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let wd = (pictureCollectionView.frame.width-2)/2
        return CGSize(width: wd, height: wd)
    }
}

class PictureCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var pictureImgVw : UIImageView!
}
