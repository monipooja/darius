//
//  FAReadMoreVC.swift
//  Darious
//
//  Created by Apple on 17/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FAReadMoreVC: UIViewController {

    @IBOutlet weak var readTextView : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        var headingOne = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_para1_heading")
        var paraOne = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_para1_bullets")
        var headingTwo = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_para2_heading")
        var paraTwo = ApiResponse.getLanguageFromUserDefaults(inputString: "flyangel_para2_bullets")
        
        headingOne = headingOne.stringByDecodingHTMLEntities.replacingOccurrences(of: "<br/>", with: "\n")
        headingTwo = headingTwo.stringByDecodingHTMLEntities.replacingOccurrences(of: "<br/>", with: "\n")
        paraOne = paraOne.stringByDecodingHTMLEntities.replacingOccurrences(of: "<br/>", with: "\n     ")
        paraTwo = paraTwo.stringByDecodingHTMLEntities.replacingOccurrences(of: "<br/>", with: "\n     ")
        
        readTextView.text = "\(headingOne)\n\n     \(paraOne)\n\n\(headingTwo)\n\n     \(paraTwo)"
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func onFAHome(_ sender: Any)
    {
        let homevc = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FAHomeVC") as! FAHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
}
