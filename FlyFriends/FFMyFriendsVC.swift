//
//  FFMyFriendsVC.swift
//  Darious
//
//  Created by Apple on 07/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FFMyFriendsVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var toplabel : UILabel!
    @IBOutlet weak var followersbtn : UIButton!
    @IBOutlet weak var followingbtn : UIButton!
    
    @IBOutlet weak var btnWidth : NSLayoutConstraint!
    @IBOutlet weak var btnHeight : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)

        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
            imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            toplabel.font = UIFont(name: "SegoeScript-Bold", size: 18)
            followersbtn.titleLabel?.font = UIFont(name: "SegoeScript-Bold", size: 11)
            followingbtn.titleLabel?.font = UIFont(name: "SegoeScript-Bold", size: 11)
            
            btnWidth.constant = 100
            btnHeight.constant = 100
        }
        
        toplabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_friends")
        followersbtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_followers"), for: .normal)
        followingbtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_people_i_follow"), for: .normal)
        
        setBottomView()
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func myFollowers(_ sender: Any)
    {
        let followersVC = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyFollowersVC") as! FFMyFollowersVC
        followersVC.passStr = "followers"
        present(followersVC, animated: false, completion: nil)
    }
    
    @IBAction func peopleFollowByMe(_ sender: Any)
    {
        let followersVC = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyFollowersVC") as! FFMyFollowersVC
        followersVC.passStr = "following"
        present(followersVC, animated: false, completion: nil)
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }
}
