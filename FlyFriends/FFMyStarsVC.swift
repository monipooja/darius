//
//  FFMyStarsVC.swift
//  Darious
//
//  Created by Apple on 21/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FFMyStarsVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var myRatingTablevw : UITableView!
    @IBOutlet weak var profileImageVW : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var ratingLabel : UILabel!
    @IBOutlet weak var followersCountLabel : UILabel!
    @IBOutlet weak var followersImgVW : UIImageView!
    
    var rateArr : [Double] = []
    var topLblarr : [Int] = [17,0,0,0,0,0,0,0,0,0]
    var lblToparr : [Int] = [15,0,0,0,0,0,0,0,0,0]
    var cornerRadius : [CGFloat] = [40,0,0,0,0,0,0,0,0,40]
    var topSpacing : [CGFloat] = [4,-2,-2,-2,-2,-2,-2,-2,-2,-2]
    var btmSpacing : [CGFloat] = [-2,-2,-2,-2,-2,-2,-2,-2,-2,4]
    var heightArray : [CGFloat] = [70,30,30,30,30,30,30,30,30,50]
    var sectionArray : [Int] = []
    
    var passUser_id : String = ""
    
    var passTag : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        followersImgVW.tintColor = UIColor.init(hexString: ColorCode.FFIconsGray)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        for btn in btnCollection
        {
            if (btn.tag == passTag) {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
               // imgCollection[btn.tag].image = imgCollection[btn.tag].image?.withRenderingMode(.alwaysTemplate)
                imgCollection[btn.tag].tintColor = UIColor.white
            }
            else {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
                imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.passUser_id)"
            self.callService(urlStr: Api.FF_MyRating_URL, params: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
              //  print("star data = \(result)")
                if (check == "get")
                {
                    let dict = result["data"] as! NSDictionary
                    
                    if (dict["nickname"] as! String != "") {
                        self.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String) (@\(dict["nickname"] as! String))"
                    }
                    else {
                        self.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
                    }
                    self.profileImageVW.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                    self.profileImageVW.layer.cornerRadius = (self.profileImageVW.frame.width)/2
                    self.profileImageVW.layer.masksToBounds = true
                    self.ratingLabel.text = StaticFunctions.ratingFormatSet(rateValue: String(format: "%.1f", Double("\(dict["rating"] as! NSNumber)")!))
                    self.followersCountLabel.text = "\(dict["followers"] as! NSNumber)"
                    
                    self.rateArr.append(Double.init(truncating: dict["rat_1"] as! NSNumber))
                    self.rateArr.append(Double.init(truncating: dict["rat_2"] as! NSNumber))
                    self.rateArr.append(Double.init(truncating: dict["rat_3"] as! NSNumber))
                    self.rateArr.append(Double.init(truncating: dict["rat_4"] as! NSNumber))
                    self.rateArr.append(Double.init(truncating: dict["rat_5"] as! NSNumber))
                    self.rateArr.append(Double.init(truncating: dict["rat_6"] as! NSNumber))
                    self.rateArr.append(Double.init(truncating: dict["rat_7"] as! NSNumber))
                    self.rateArr.append(Double.init(truncating: dict["rat_8"] as! NSNumber))
                    self.rateArr.append(Double.init(truncating: dict["rat_9"] as! NSNumber))
                    self.rateArr.append(Double.init(truncating: dict["rat_10"] as! NSNumber))
                    self.myRatingTablevw.reloadData()
                    customLoader.hideIndicator()
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    func callRating(_ ratingValue : Double, starRating : [UIImageView])
    {
        var passvalue : String = ""
        var dcml : String = ""
        if ("\(ratingValue)".contains(".") == true)
        {
            let split = String(format: "%.2f", ratingValue).split(separator: ".")
            passvalue = "\(split[0])"
            let dcmlsplit = "\(Int(split[1])!/10)".split(separator: ".")
            dcml = "\(dcmlsplit[0])"
        }
        else {
            passvalue = "\(ratingValue)"
            dcml = "\(0)"
        }
        
        switch passvalue {
        case "0":
            decimalRating(decimalValue: dcml, starImg: starRating[0])
            starRating[1].image = #imageLiteral(resourceName: "Graystar")
            starRating[2].image = #imageLiteral(resourceName: "Graystar")
            starRating[3].image = #imageLiteral(resourceName: "Graystar")
            starRating[4].image = #imageLiteral(resourceName: "Graystar")
            break
        case "1":
            starRating[0].image = #imageLiteral(resourceName: "FF_yellowStar")
            decimalRating(decimalValue: dcml, starImg: starRating[1])
            starRating[2].image = #imageLiteral(resourceName: "Graystar")
            starRating[3].image = #imageLiteral(resourceName: "Graystar")
            starRating[4].image = #imageLiteral(resourceName: "Graystar")
            break
        case "2":
            starRating[0].image = #imageLiteral(resourceName: "FF_yellowStar")
            starRating[1].image = #imageLiteral(resourceName: "FF_yellowStar")
            decimalRating(decimalValue: dcml, starImg: starRating[2])
            starRating[3].image = #imageLiteral(resourceName: "Graystar")
            starRating[4].image = #imageLiteral(resourceName: "Graystar")
            break
        case "3":
            starRating[0].image = #imageLiteral(resourceName: "FF_yellowStar")
            starRating[1].image = #imageLiteral(resourceName: "FF_yellowStar")
            starRating[2].image = #imageLiteral(resourceName: "FF_yellowStar")
            decimalRating(decimalValue: dcml, starImg: starRating[3])
            starRating[4].image = #imageLiteral(resourceName: "Graystar")
            break
        case "4":
            starRating[0].image = #imageLiteral(resourceName: "FF_yellowStar")
            starRating[1].image = #imageLiteral(resourceName: "FF_yellowStar")
            starRating[2].image = #imageLiteral(resourceName: "FF_yellowStar")
            starRating[3].image = #imageLiteral(resourceName: "FF_yellowStar")
            decimalRating(decimalValue: dcml, starImg: starRating[4])
            break
        case "5":
            starRating[0].image = #imageLiteral(resourceName: "FF_yellowStar")
            starRating[1].image = #imageLiteral(resourceName: "FF_yellowStar")
            starRating[2].image = #imageLiteral(resourceName: "FF_yellowStar")
            starRating[3].image = #imageLiteral(resourceName: "FF_yellowStar")
            starRating[4].image = #imageLiteral(resourceName: "FF_yellowStar")
            break
        default:
            break
        }
    }
    
    func decimalRating(decimalValue : String, starImg : UIImageView)
    {
        switch decimalValue {
        case "0":
            starImg.image = #imageLiteral(resourceName: "Graystar")
            break
        case "1":
            starImg.image = UIImage.init(named: "Star 0.1")
            break
        case "2":
            starImg.image = UIImage.init(named: "Star 0.2")
            break
        case "3":
            starImg.image = UIImage.init(named: "Star 0.3")
            break
        case "4":
            starImg.image = UIImage.init(named: "Star 0.4")
            break
        case "5":
            starImg.image = UIImage.init(named: "Star 0.5")
            break
        case "6":
            starImg.image = UIImage.init(named: "Star 0.6")
            break
        case "7":
            starImg.image = UIImage.init(named: "Star 0.7")
            break
        case "8":
            starImg.image = UIImage.init(named: "Star 0.8")
            break
        case "9":
            starImg.image = UIImage.init(named: "Star 0.9")
            break
        default:
            break
        }
    }
}

extension FFMyStarsVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rateArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = myRatingTablevw.dequeueReusableCell(withIdentifier: "mystarcell") as! FFMyStarTableCell
        
        cell.topLblHt.constant = CGFloat(topLblarr[indexPath.row])
        cell.lblTop.constant = CGFloat(lblToparr[indexPath.row])
       // cell.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: Const.FF_StarRating[indexPath.row])
        cell.topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_aspects_value")
        
        cell.avgRateLabel.text = StaticFunctions.ratingFormatSet(rateValue: String(format: "%.1f", rateArr[indexPath.row]))
        
        if (sectionArray == [])
        {
            callRating(rateArr[indexPath.row], starRating: cell.starCollection)
        }
        else {
            if (sectionArray.contains(indexPath.row) == false)
            {
                callRating(rateArr[indexPath.row], starRating: cell.starCollection)
            }
        }
        
        cell.mainVw.layer.cornerRadius = cornerRadius[indexPath.row]
        cell.mainvwTop.constant = topSpacing[indexPath.row]
        cell.mainvwBtm.constant = btmSpacing[indexPath.row]
        
        if (indexPath.row == 0) {
    
            if #available(iOS 11.0, *) {
                cell.mainVw.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            } else {
                // Fallback on earlier versions
                let path = UIBezierPath(roundedRect: cell.mainVw.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius[indexPath.row], height: cornerRadius[indexPath.row]))
                let mask = CAShapeLayer()
                mask.path = path.cgPath
                cell.mainVw.layer.mask = mask
            }
            cell.topVw.isHidden = true
            cell.btmVw.isHidden = true
        }
        else if (indexPath.row == rateArr.count-1)
        {
            if #available(iOS 11.0, *) {
                cell.mainVw.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            } else {
                // Fallback on earlier versions
                let path = UIBezierPath(roundedRect: cell.mainVw.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius[indexPath.row], height: cornerRadius[indexPath.row]))
                let mask = CAShapeLayer()
                mask.path = path.cgPath
                cell.mainVw.layer.mask = mask
            }
            cell.topVw.isHidden = true
            cell.btmVw.isHidden = true
        }
        else {
            cell.topVw.isHidden = true
            cell.btmVw.isHidden = true
        }
        cell.mainVw.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        cell.mainVw.layer.shadowColor = UIColor.lightGray.cgColor
        cell.mainVw.layer.shadowOpacity = 0.8
        cell.mainVw.layer.shadowRadius = 4
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightArray[indexPath.row]
    }
}

class FFMyStarTableCell: UITableViewCell {
    
    @IBOutlet weak var mainVw : UIView!
    @IBOutlet weak var topVw : UIView!
    @IBOutlet weak var btmVw : UIView!
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var avgRateLabel : UILabel!
    @IBOutlet weak var avgstar : UIImageView!
    
    @IBOutlet var starCollection : [UIImageView]!
    
    @IBOutlet weak var topLblHt : NSLayoutConstraint!
    @IBOutlet weak var lblTop : NSLayoutConstraint!
    @IBOutlet weak var titleCenter : NSLayoutConstraint!
    @IBOutlet weak var mainvwTop : NSLayoutConstraint!
    @IBOutlet weak var mainvwBtm : NSLayoutConstraint!
}

