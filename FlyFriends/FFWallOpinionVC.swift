//
//  FFWallOpinionVC.swift
//  Darious
//
//  Created by Apple on 19/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds
import SwiftyStoreKit
import StoreKit

class FFWallOpinionVC: UIViewController, GADUnifiedNativeAdLoaderDelegate, GADUnifiedNativeAdDelegate {
    
    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var myWallLbl : UILabel!
    @IBOutlet weak var peopleLbl : UILabel!
    @IBOutlet weak var nodataLbl : UILabel!
    @IBOutlet weak var wallOpinionTableVw : UITableView!
    
    // Center report view outlets
    @IBOutlet weak var centerBlkVw : UIView!
    @IBOutlet weak var reportView : CustomUIView!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var reportRmvLbl : UILabel!
    @IBOutlet weak var reportRmvblkLbl : UILabel!
    @IBOutlet var optionLbls : [UILabel]!
    @IBOutlet var optionBtns : [KGHighLightedButton]!
    
    var refreshControl = UIRefreshControl()
    
    var opinionArray : [NSDictionary] = []
    var heightArray : [CGFloat] = []
    var linesArray : [Int] = []
    
    var selectRsn : String = ""
    var rating_id : NSNumber = 0
    var rmvblk_userid : NSNumber = 0
    var transactionId : String = ""
    var total : String = ""
    var currencyType : String = ""
    var selectStr : String = ""
    var selectedCell : WallOpinionTableCell!
    var selectDict : NSDictionary = [:]
    var optionLblText : [String] = ["ff_insults", "ff_hate_message", "ff_abusive", "ff_other"]
    var selectRow : Int = 0
    var setBtm : Bool = false
    var fromDelegate : Bool = false
    
    var adLoader: GADAdLoader!
    var adViewCell : AdsViewCell!
    
    var purchaseCreditAlert: UIAlertController!
    var txtPurchaseCreditAlert: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 1) {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                imgCollection[btn.tag].tintColor = UIColor.white
                imgCollection[btn.tag].image = imgCollection[btn.tag].image?.withRenderingMode(.alwaysTemplate)
            }
            else {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
                imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        for btn in optionBtns
        {
            btn.addTarget(self, action: #selector(selectReasonToReport(_:)), for: .touchUpInside)
            optionLbls[btn.tag-1].text = ApiResponse.getLanguageFromUserDefaults(inputString: optionLblText[btn.tag-1])
        }
        if (setBtm == false)
        {
            setBottomView()
        }
        myWallLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_wall")
        peopleLbl.text = "(\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_people_opinion")))"
        reportRmvLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_remove")
        reportRmvblkLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_remove_block")
        titleLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_report_this_opinion")
        
        /// MARK : UIRefreshControl
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        if #available(iOS 10.0, *) {
            wallOpinionTableVw.refreshControl = refreshControl
        } else {
            wallOpinionTableVw.addSubview(refreshControl)
        }
        
        let headerNib = UINib.init(nibName: "AdsViewCell", bundle: Bundle.main)
        wallOpinionTableVw.register(headerNib, forCellReuseIdentifier: "AdsViewCell")
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&page_no=0"
            self.callService(urlStr: Api.FF_PeopleOpinions_URL, params: params, check: "get")
        } 
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
//        NotificationCenter.default.addObserver(self, selector: #selector(openMollie(notification:)), name:NSNotification.Name(rawValue: "isDismissViewController"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(onMolliesuccess(notification:)), name:NSNotification.Name(rawValue: "dismissAfterSuccess"), object: nil)
    }
    
    ///  MARK : Pull To Refresh TableView
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&page_no=0"
        self.callService(urlStr: Api.FF_PeopleOpinions_URL, params: params, check: "refresh")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
   
    // Report View Actions
    @IBAction func hideReportView(_ sender: Any)
    {
        centerBlkVw.isHidden = true
        reportView.isHidden = true
        for btn in optionBtns
        {
            btn.backgroundColor = UIColor.white
        }
    }
    
    @objc func selectReasonToReport(_ sender : UIButton)
    {
        for btn in optionBtns
        {
            if (btn.tag == sender.tag)
            {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                selectRsn = optionLblText[btn.tag-1]
            }
            else {
                btn.backgroundColor = UIColor.white
            }
        }
    }
    
    @IBAction func reportRemove(_ sender: Any)
    {
        if (selectRsn == "")
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_select_reason"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rat_id=\(rating_id)&msg=\(selectRsn)&type=remove"
            self.callService(urlStr: Api.FF_Report_URL, params: params, check: "report")
        }
    }
    
    @IBAction func reportRemoveBlock(_ sender: Any)
    {
        if (selectRsn == "")
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_select_reason"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rat_id=\(rating_id)&msg=\(selectRsn)&type=remove_block"
            self.callService(urlStr: Api.FF_Report_URL, params: params, check: "report")
        }
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func shareWall(_ sender : Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flyfriends") else { return }
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ""
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flyfriends")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        if (fromDelegate == false)
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
              //  print("people's opinions = \(result)")
                if (check == "get") || (check == "refresh")
                {
                    if let dataarr = result["data"] as? [NSDictionary]
                    {
                        self.opinionArray = dataarr
                    }
                    if(self.opinionArray != []) {
                        self.wallOpinionTableVw.reloadData()
                        self.wallOpinionTableVw.isHidden = false
                        self.heightArray = []
                        self.linesArray = []
                        for dict in self.opinionArray
                        {
                            self.heightArray.append(100)
                            self.linesArray.append(2)
                        }
                    }
                    else {
                        self.wallOpinionTableVw.isHidden = true
                        self.nodataLbl.isHidden = false
                        self.nodataLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                    }
                    if (check == "refresh") {
                        self.refreshControl.endRefreshing()
                    }
                    customLoader.hideIndicator()
                }
                else if (check == "report") {
                    let msgstr = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msgstr, actions: [OkText], controller: self, completion: { (str) in
                        
                        self.centerBlkVw.isHidden = true
                        self.reportView.isHidden = true
                        self.selectRsn = ""
                        for btn in self.optionBtns
                        {
                            btn.backgroundColor = UIColor.white
                        }
                        self.heightArray = []
                        self.linesArray = []
                        self.viewDidLoad()
                    })
                }
                else if (check == "reveal") || (check == "freereveal") {

                    let datadict = result["data"] as! NSDictionary
                    let editdict = NSMutableDictionary.init(dictionary: self.selectDict)
                    
                    if (self.selectDict["status"] as! String == "active")
                    {
                        editdict["name"] = "\(datadict["first_name"] as! String) \(datadict["last_name"] as! String)"
                        editdict["view_status"] = "show"
                        if (self.selectDict["nickname"] as! String != "")
                        {
                            self.selectedCell.nameLabel.text = "\(editdict["name"] as! String) (@\(self.selectDict["nickname"] as! String))"
                        }
                        else {
                            self.selectedCell.nameLabel.text = editdict["name"] as? String
                        }
                        if (self.selectDict["profile_pic"] as! String != "") {
                            self.selectedCell.profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(self.selectDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                            self.selectedCell.profileImgVw.isHidden = false
                        }
                        self.opinionArray[self.selectRow] = NSDictionary.init(dictionary: editdict)
                    }
                    self.selectedCell.revealVwHt.constant = 0
                    customLoader.hideIndicator()
                    
                    if (check == "freereveal") {
                        Free_Reveal = result["free_reveal"] as! Bool
                        let errorvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFErrorPopupVC") as! FFErrorPopupVC
                        errorvc.statusCheck = "freereveal"
                        errorvc.modalPresentationStyle = .overCurrentContext
                        self.present(errorvc, animated: false, completion: nil)
                    }
                }
                else if (check == "rmblk"){
                    
                    let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msg, actions: [OkText], controller: self, completion: { (str) in
                        self.heightArray = []
                        self.linesArray = []
                        self.viewDidLoad()
                    })
                    customLoader.hideIndicator()
                }
                else if (check == "remove") {
                    customLoader.hideIndicator()
                    self.heightArray = []
                    self.linesArray = []
                    self.viewDidLoad()
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        setBtm = true
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    // Ads Function call
    func loadAds()
    {
        let multipleAdsOptions = GADMultipleAdsAdLoaderOptions()
        multipleAdsOptions.numberOfAds = 5
        
        adLoader = GADAdLoader(adUnitID: AdUnitId, rootViewController: self,
                               adTypes: [GADAdLoaderAdType.unifiedNative],
                               options: [multipleAdsOptions])
        adLoader.delegate = self
        adLoader.load(GADRequest())
    }
    
    func adLoader(_ adLoader: GADAdLoader,
                  didReceive nativeAd: GADUnifiedNativeAd) {
        // A unified native ad has loaded, and can be displayed.
        
        adViewCell.nativeViewAd.nativeAd = nativeAd
        nativeAd.delegate = self
        
        // Populate the native ad view with the native ad assets.
        // The headline and mediaContent are guaranteed to be present in every native ad.
        adViewCell.headlineLbl.text = nativeAd.headline
        adViewCell.descLabel.text = nativeAd.body
        adViewCell.advImageVW.image = nativeAd.icon?.image
        adViewCell.visitSiteBtn.setTitle(nativeAd.callToAction, for: .normal)
        adViewCell.visitSiteBtn.isUserInteractionEnabled = false
        adViewCell.nativeViewAd.callToActionView = adViewCell.visitSiteBtn
        
        nativeAd.register(adViewCell.visitSiteBtn, clickableAssetViews: [GADUnifiedNativeAssetIdentifier.callToActionAsset : adViewCell.nativeViewAd.callToActionView!], nonclickableAssetViews: [:])
    }
    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        // The adLoader has finished loading ads, and a new request can be sent.
    }
    public func adLoader(_ adLoader: GADAdLoader,
                         didFailToReceiveAdWithError error: GADRequestError)
    {
    }
    func register(_ adView: UIView, clickableAssetViews: [GADUnifiedNativeAssetIdentifier : UIView], nonclickableAssetViews: [GADUnifiedNativeAssetIdentifier : UIView])
    {
    }
    
    func getProduct(_ purchase: ServicePurchase)
    {
        SwiftyStoreKit.retrieveProductsInfo([appBundleId + "." + purchase.rawValue]) { result in
            if let product = result.retrievedProducts.first {
                self.total = "\(product.price)"
                self.currencyType = product.priceLocale.currencySymbol!
            }
        }
    }
    
    func purchase(_ purchase: ServicePurchase, atomically: Bool) {
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.purchaseProduct(appBundleId + "." + purchase.rawValue, atomically: atomically) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            if case .success(let purchaseObj) = result {
                self.verifyPurchase(purchase)
                
                let downloads = purchaseObj.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                
                // Deliver content from server, then:
                if purchaseObj.needsFinishTransaction {
                    customLoader.hideIndicator()
                    SwiftyStoreKit.finishTransaction(purchaseObj.transaction)
                }
            }
            else {
                customLoader.hideIndicator()
            }
        }
    }
    
    func verifyPurchase(_ purchase: ServicePurchase) {
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                let productId = appBundleId + "." + purchase.rawValue
                let purchaseResult = SwiftyStoreKit.verifyPurchase(productId: productId, inReceipt: receipt)
                switch purchaseResult {
                case .purchased:
                    StaticFunctions.showAlert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_payment_done"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "thanks_for_purchasing"), actions: [OkText], controller: self, completion: { (str) in
                        self.addPurchaseCreditToAccount(RegisteredPurchase: appBundleId + "." + purchase.rawValue)
                    })
                    break
                    
                case .notPurchased:
                    self.showAlert(self.alertForVerifyReceipt(result))
                    break
                }
            case .error:
                self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: AdSharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }
    
    func addPurchaseCreditToAccount(RegisteredPurchase: String) {

        if (self.selectStr == "remove")
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&amount=\(total)&msg=paid_for_remove_comment&type=remove&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_RemoveComment_URL, params: params, check: "remove")
        }
        else if (self.selectStr == "show_user")
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&trans_id=\(self.transactionId)&amount=\(total)&type=inapp&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_PeopleReveal_URL, params: params, check: "reveal")
        }
        else
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&trans_id=\(self.transactionId)&amount=\(total)&user_id=\(rmvblk_userid)&type=inapp&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_RemoveBlock_URL, params: params, check: "rmblk")
        }
    }
}

// MARK : TableView Delegates
extension FFWallOpinionVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return opinionArray.count + (opinionArray.count/3)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            let cell = wallOpinionTableVw.dequeueReusableCell(withIdentifier: "AdsViewCell") as! AdsViewCell
            
            cell.nativeViewAd.layer.cornerRadius = 44
            cell.nativeViewAd.layer.shadowColor = UIColor.lightGray.cgColor
            cell.nativeViewAd.layer.shadowOpacity = 0.6
            cell.nativeViewAd.layer.shadowRadius = 3
            cell.nativeViewAd.layer.shadowOffset = CGSize(width: 0, height: 0)
            
            return cell
        }
        else {
            let cell = wallOpinionTableVw.dequeueReusableCell(withIdentifier: "wallopncell") as! WallOpinionTableCell
            
            var sectionValue : Int!
            if (indexPath.section > 3) {
                sectionValue = indexPath.section-(indexPath.section/3)
            }
            else {
                sectionValue = indexPath.section
            }
            
            let dict = opinionArray[sectionValue]
            
            cell.bgImgVw.layer.cornerRadius = cell.bgImgVw.frame.width/2
            cell.bgImgVw.layer.masksToBounds = true
            
            cell.descLabel.text = dict["rating_desc"] as? String
            cell.descLabel.numberOfLines = linesArray[sectionValue]
            
            //Verified or not
            if (dict["face_status"] as! String == "yes")
            {
                cell.verifiedImg.isHidden = false
            }
            else {
                cell.verifiedImg.isHidden = true
            }
            
            // name & profilepic set
            if (dict["status"] as! String == "hidden") && (dict["view_status"] as! String == "active")
            {
                cell.nameLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_anonymous")) \(indexPath.section+1) \(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_hidden"))"
                cell.bgImgVw.backgroundColor = ColorCode.FFHiddenBG
                cell.anonymsImgVw.alpha = 0.3
                cell.profileImgVw.isHidden = true
                cell.anonymsImgVw.isHidden = false
            }
            else if (dict["status"] as! String == "active") && (dict["view_status"] as! String == "active")
            {
                cell.nameLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_anonymous")) \(indexPath.section+1)"
                cell.profileImgVw.isHidden = true
                cell.anonymsImgVw.isHidden = false
            }
            else if (dict["status"] as! String == "active") && (dict["view_status"] as! String == "show")
            {
                if (dict["nickname"] as! String != "")
                {
                    cell.nameLabel.text = "\(dict["name"] as! String) (@\(dict["nickname"] as! String))"
                }
                else {
                    cell.nameLabel.text = dict["name"] as? String
                }
                cell.profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                cell.profileImgVw.isHidden = false
                cell.anonymsImgVw.isHidden = true
            }
            else {
                cell.nameLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_anonymous")) \(indexPath.section+1) \(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_hidden"))"
                cell.bgImgVw.backgroundColor = ColorCode.FFHiddenBG
                cell.anonymsImgVw.alpha = 0.3
                cell.profileImgVw.isHidden = true
                cell.anonymsImgVw.isHidden = false
            }
            
            //Edit Set
            if (Int.init(exactly: dict["me_block"] as! NSNumber)! > 0) || (Int.init(exactly: dict["user_block"] as! NSNumber)! > 0) || (dict["status"] as! String == "hidden") || (dict["view_status"] as! String == "show")
            {
                cell.revealVwHt.constant = 0
            }
            else {
                cell.revealVwHt.constant = 35
            }
            // user block
            if (Int.init(exactly: dict["user_block"] as! NSNumber)! > 0)
            {
                cell.anonymsImgVw.isHidden = false
                cell.profileImgVw.isHidden = false
                cell.profileImgVw.image = #imageLiteral(resourceName: "FF_RedBlock")
                cell.profileImgVw.tintColor = ColorCode.FFRemoveBG
            }
            else {
            }
            
            if(heightArray[sectionValue] > 100) {
                cell.openBtn.isHidden = true
            }
            else {
                cell.openBtn.isHidden = false
            }
            
            if (self.view.frame.width == 320) && (self.view.frame.height == 568)
            {
                cell.blockTextLbl.font = UIFont(name: "SegoeScript-Bold", size: 11)
            }
            else {
                cell.blockTextLbl.font = UIFont(name: "SegoeScript-Bold", size: 13)
            }
            
            if (Free_Reveal == false) {
                cell.revealPrcLbl.text = Reveal_price
            }
            else {
                cell.revealPrcLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_free")
            }
            cell.revealBtn.addTarget(self, action: #selector(revealUser(_:)), for: .touchUpInside)
            cell.revealTextLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_reveal")
            cell.removeBtn.addTarget(self, action: #selector(removeComment(_:)), for: .touchUpInside)
            cell.removeTextLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_remove")
            cell.blockBtn.addTarget(self, action: #selector(removeAndBlock(_:)), for: .touchUpInside)
            cell.blockTextLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_remove_block")
            
            cell.openBtn.addTarget(self, action: #selector(openCell(_:)), for: .touchUpInside)
            cell.closeBtn.addTarget(self, action: #selector(closeCell(_:)), for: .touchUpInside)
            cell.profButton.addTarget(self, action: #selector(goTouserProfile(_:)), for: .touchUpInside)
            cell.nameButton.addTarget(self, action: #selector(goTouserProfile(_:)), for: .touchUpInside)
            cell.reportBtn.addTarget(self, action: #selector(reportComment(_:)), for: .touchUpInside)
            cell.reportBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_report"), for: .normal)
            
            addShadow(passVw: cell.bgView)
            
            return cell
        }
    }
    
    @objc func goTouserProfile(_ sender : UIButton) {
        
        let point = sender.convert(CGPoint.zero, to: wallOpinionTableVw)
        let idxpath = wallOpinionTableVw.indexPathForRow(at: point)
        var dict : NSDictionary = [:]
        if (idxpath!.section > 3) {
            dict = opinionArray[idxpath!.section-(idxpath!.section/3)]
        }
        else {
            dict = opinionArray[idxpath!.section]
        }
        if (dict["status"] as! String == "active") && (dict["view_status"] as! String == "show")
        {
            let profilevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFfriendProfileVC") as! FFfriendProfileVC
            profilevc.passid = dict["from_id"] as! NSNumber
            profilevc.passTag = 0
            present(profilevc, animated: false, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            adViewCell = (cell as! AdsViewCell)
            loadAds()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            return 98
        }
        else {
            if (indexPath.section > 3) {
                return heightArray[indexPath.section-(indexPath.section/3)]
            }
            else {
                return heightArray[indexPath.section]
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: wallOpinionTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }

    @objc func openCell(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: wallOpinionTableVw)
        let idxpath = wallOpinionTableVw.indexPathForRow(at: point)
        
        var sectionValue : Int!
        if (idxpath!.section > 3) {
            sectionValue = idxpath!.section-(idxpath!.section/3)
        }
        else {
            sectionValue = idxpath!.section
        }
        
        let dict = opinionArray[sectionValue]
        let cell = wallOpinionTableVw.cellForRow(at: idxpath!) as! WallOpinionTableCell
        let ht = ApiResponse.calculateSizeForString(dict["rating_desc"] as! String, sz: 10, maxWidth: cell.descLabel.frame.width, controller: self).height
        if (ht > 30) {
            heightArray[sectionValue] = wallOpinionTableVw.estimatedRowHeight + (ht-30)
        }
        else {
            heightArray[sectionValue] = wallOpinionTableVw.estimatedRowHeight
        }
        
        if (cell.revealVwHt.constant == 0)
        {
            heightArray[sectionValue] = heightArray[sectionValue] - 35
        }
        else {}
        
        linesArray[sectionValue] = 0
        wallOpinionTableVw.reloadRows(at: [idxpath!], with: .none)
    }
    
    @objc func closeCell(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: wallOpinionTableVw)
        let idxpath = wallOpinionTableVw.indexPathForRow(at: point)

        var sectionValue : Int!
        if (idxpath!.section > 3) {
            sectionValue = idxpath!.section-(idxpath!.section/3)
        }
        else {
            sectionValue = idxpath!.section
        }
        
        heightArray[sectionValue] = 100
        linesArray[sectionValue] = 2
        wallOpinionTableVw.reloadRows(at: [idxpath!], with: .none)
    }
    
    @objc func revealUser(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: wallOpinionTableVw)
        let idxpath = wallOpinionTableVw.indexPathForRow(at: point)
        
        var sectionValue : Int!
        if (idxpath!.section > 3) {
            sectionValue = idxpath!.section-(idxpath!.section/3)
        }
        else {
            sectionValue = idxpath!.section
        }
        
        let dict = opinionArray[sectionValue]
        let cell = wallOpinionTableVw.cellForRow(at: idxpath!) as! WallOpinionTableCell
        selectedCell = cell
        
        selectStr = "show_user"
        rating_id = dict["id"] as! NSNumber
        selectRow = sectionValue
        selectDict = dict
        
        if (Free_Reveal == false) {
            getProduct(.reveal)
            purchase(.reveal, atomically: true)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(rating_id)"
            
            callService(urlStr: Api.FF_FreeReveal_URL, params: params, check: "freereveal")
        }
    }
    
    @objc func removeComment(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: wallOpinionTableVw)
        let idxpath = wallOpinionTableVw.indexPathForRow(at: point)
        var dict : NSDictionary = [:]
        if (idxpath!.section > 3) {
            dict = opinionArray[idxpath!.section-(idxpath!.section/3)]
        }
        else {
            dict = opinionArray[idxpath!.section]
        }
        let cell = wallOpinionTableVw.cellForRow(at: idxpath!) as! WallOpinionTableCell
        selectedCell = cell
       
        selectStr = "remove"
        rating_id = dict["id"] as! NSNumber
        
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&amount=0&msg=paid_for_remove_comment&type=remove&curr_type=\(currencyType)"
        self.callService(urlStr: Api.FF_RemoveComment_URL, params: params, check: "remove")
    }
    
    @objc func removeAndBlock(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: wallOpinionTableVw)
        let idxpath = wallOpinionTableVw.indexPathForRow(at: point)
        var dict : NSDictionary = [:]
        if (idxpath!.section > 3) {
            dict = opinionArray[idxpath!.section-(idxpath!.section/3)]
        }
        else {
            dict = opinionArray[idxpath!.section]
        }
        let cell = wallOpinionTableVw.cellForRow(at: idxpath!) as! WallOpinionTableCell
        selectedCell = cell
       
        selectStr = "remove_block"
        rating_id = dict["id"] as! NSNumber
        rmvblk_userid = dict["from_id"] as! NSNumber
        
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&trans_id=\(self.transactionId)&amount=0&user_id=\(rmvblk_userid)&type=inapp&curr_type=\(currencyType)"
        self.callService(urlStr: Api.FF_RemoveBlock_URL, params: params, check: "rmblk")
    }
    
    @objc func reportComment(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: wallOpinionTableVw)
        let idxpath = wallOpinionTableVw.indexPathForRow(at: point)
        var dict : NSDictionary = [:]
        if (idxpath!.section > 3) {
            dict = opinionArray[idxpath!.section-(idxpath!.section/3)]
        }
        else {
            dict = opinionArray[idxpath!.section]
        }
        rating_id = dict["id"] as! NSNumber
        centerBlkVw.isHidden = false
        reportView.isHidden = false
    }
    
    func addShadow(passVw: UIView)
    {
        passVw.layer.cornerRadius = 45
        passVw.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        passVw.layer.shadowColor = UIColor.lightGray.cgColor
        passVw.layer.shadowOpacity = 0.8
        passVw.layer.shadowRadius = 4
    }
}

class WallOpinionTableCell : UITableViewCell
{
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var firstView : UIView!
    @IBOutlet weak var bottomView : UIView!

    @IBOutlet weak var profileImgVw : UIImageView!
    @IBOutlet weak var anonymsImgVw : UIImageView!
    @IBOutlet weak var bgImgVw : UIView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var descLabel : UILabel!
   
    @IBOutlet weak var reamMoreBtn : UIButton!
    @IBOutlet weak var openBtn : UIButton!
    @IBOutlet weak var closeBtn : UIButton!
    @IBOutlet weak var verifiedImg : UIButton!
    @IBOutlet weak var profButton : UIButton!
    @IBOutlet weak var nameButton : UIButton!
    
    @IBOutlet weak var reportBtn : UIButton!
    
    @IBOutlet weak var revealBtn : UIButton!
    @IBOutlet weak var removeBtn : UIButton!
    @IBOutlet weak var blockBtn : UIButton!
    @IBOutlet weak var revealPrcLbl : UILabel!
    @IBOutlet weak var revealTextLbl : UILabel!
    @IBOutlet weak var removeTextLbl : UILabel!
    @IBOutlet weak var blockTextLbl : UILabel!
    
    @IBOutlet weak var revealVwHt : NSLayoutConstraint!
    @IBOutlet weak var removeVwHt : NSLayoutConstraint!
    @IBOutlet weak var blockVwHt : NSLayoutConstraint!
}

// MARK: User facing alerts
extension FFWallOpinionVC {
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        customLoader.hideIndicator()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func showAlert(_ alert: UIAlertController)
    {
        customLoader.hideIndicator()
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {
        
        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
            return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        } else if let _ = result.invalidProductIDs.first {
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: "")
        } else {
            let errorString = result.error?.localizedDescription ?? "\(ApiResponse.getLanguageFromUserDefaults(inputString: "unknown_error"))"
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: errorString)
        }
    }
    
    func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
        
        switch result {
        case .success( _):
            //print("Verify receipt Success: \(receipt)")
            return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "receipt_verified"))
        case .error(let error):
            // print("Verify receipt Failed: \(error)")
            switch error {
            case .noReceiptData:
                return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "no_receipt"))
            case .networkError(let error):
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verifying_error")): \(error)")
            default:
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verification_failed")): \(error)")
            }
        }
    }
}
