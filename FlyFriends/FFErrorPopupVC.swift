//
//  FFErrorPopupVC.swift
//  Darious
//
//  Created by Apple on 30/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FFErrorPopupVC: UIViewController {
    
    @IBOutlet weak var resultView: CustomUIView!
    @IBOutlet weak var imageVw : UIImageView!
    @IBOutlet weak var msgTitleLabel : UILabel!
    @IBOutlet weak var messageLabel : UILabel!
    @IBOutlet weak var addMoneyBtn: KGHighLightedButton!
    @IBOutlet weak var imgWdConstraint: NSLayoutConstraint!
    
    // CARD Payment View
    @IBOutlet weak var paymentView: CustomUIView!
    @IBOutlet weak var paymentTitleLbl: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var commissionLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var subtotalValue: UILabel!
    @IBOutlet weak var commissionValue: UILabel!
    @IBOutlet weak var totalValue: UILabel!
    @IBOutlet weak var continueBtn: KGHighLightedButton!
    @IBOutlet weak var cardViewHeight: NSLayoutConstraint!
    
    // CARD-Wallet View
    @IBOutlet weak var cardWalletView: CustomUIView!
    @IBOutlet weak var walletBtn: UIButton!
    @IBOutlet weak var creditBtn: UIButton!
    @IBOutlet weak var chooseLabel: UILabel!
    @IBOutlet weak var walletLabel: UILabel!
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var cardRevealView: CustomUIView!
    @IBOutlet weak var msgLabel: UILabel!
    
    // Reveal view
    @IBOutlet weak var revealView: CustomUIView!
    @IBOutlet weak var revealTitleLabel : UILabel!
    
    var commisionValue : Float = 0
    var totalPrc : Float = 0
    var goback : Bool = false
    var statusCheck : String = ""
    var payReason : String = ""
    var ratingId : String = ""
    var addMoney : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        chooseLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "choose_payment_method")
        // msgLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_you_used_free_reveal")
        paymentTitleLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_credit_card_payment")
        revealTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_get_a_free_reveal")
        subtotalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "subtotal")
        commissionLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_card_commission")
        totalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "total")
        addMoneyBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "add_money"), for: .normal)
        continueBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "continue"), for: .normal)
        
        if (statusCheck == "reveal")
        {
            revealView.isHidden = false
        }
        else {
            revealView.isHidden = true
            resultView.isHidden = false
            
            self.msgTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_free_reveal_used")
            self.msgTitleLabel.textColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            messageLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_you_used_free_reveal")
            
            self.imageVw.image = #imageLiteral(resourceName: "Free_Reveal")
            self.imgWdConstraint.constant = 60
            self.statusCheck = "freereveal"
            
            //            if (totalPrc == 0)
            //            {
            //                if (statusCheck == "failure") {
            //                    msgTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "payment_not_done")
            //                    msgTitleLabel.textColor = ColorCode.FFRemoveBG
            //                    messageLabel.text = ""
            //                    imageVw.image = #imageLiteral(resourceName: "FF_Creditcard")
            //                    imgWdConstraint.constant = 70
            //                }
            //                else {
            //                    msgTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_payment_done")
            //                    msgTitleLabel.textColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            //                    messageLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_money_from_card")
            //                    imageVw.image = #imageLiteral(resourceName: "FF_Creditcard")
            //                    imgWdConstraint.constant = 70
            //                }
            //                resultView.isHidden = false
            //            }
            //            else {
            //                cardWalletView.isHidden = false
            //            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (addMoney == true)
        {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&page_no=0"
            self.callService(urlStr: Api.Wallet_History_URL, params: params, check: "wallet")
        }
    }
    
    @IBAction func closePaymentView(_ sender: Any)
    {
        cardWalletView.isHidden = false
        paymentView.isHidden = true
    }
    
    @IBAction func closeCardView(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func closeRevealView(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func goToMyProfile(_ sender: Any)
    {
        let profilevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyProfileVC") as! FFMyProfileVC
        self.present(profilevc, animated: false, completion: nil)
    }
    
    @IBAction func closeResultView(_ sender: Any)
    {
        if (statusCheck == "walletF")
        {
            resultView.isHidden = true
            cardWalletView.isHidden = false
        }
        else if (statusCheck == "failure") || (statusCheck == "reveal") || (statusCheck == "freereveal")
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            self.dismiss(animated: false, completion: {
                NotificationCenter.default.post(name: Notification.Name("dismissAfterSuccess"), object: nil, userInfo: nil)
            })
        }
    }
    
    @IBAction func onContinue(_ sender: Any)
    {
        cardWalletView.isHidden = true
        paymentView.isHidden = true
        self.dismiss(animated: false, completion: {
            NotificationCenter.default.post(name: Notification.Name("isDismissViewController"), object: nil, userInfo: nil)
        })
    }
    
    @IBAction func usingWallet(_ sender: Any)
    {
        if (Int(truncating: wallet_amount) < 1) || (Float(truncating: wallet_amount) < (totalPrc-commisionValue)) {
            
            msgTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_not_enough_money_need_add_money")
            messageLabel.isHidden = true
            imageVw.image = #imageLiteral(resourceName: "FF_Creditcard")
            imgWdConstraint.constant = 70
            addMoneyBtn.isHidden = false
            resultView.isHidden = false
            cardWalletView.isHidden = true
            statusCheck = "walletF"
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rat_id=\(ratingId)&msg=\(payReason)&amount=\(totalPrc-commisionValue)"
            callService(urlStr: Api.FF_Wallet_URL, params: params, check: "get")
        }
    }
    
    @IBAction func usingCard(_ sender: Any)
    {
        subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", totalPrc-commisionValue)) + " \(Current_Currency)"
        commissionValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", commisionValue)) + " \(Current_Currency)"
        totalValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", totalPrc)) + " \(Current_Currency)"
        cardWalletView.isHidden = true
        paymentView.isHidden = false
    }
    
    @IBAction func usingFreeReveal(_ sender: Any)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(ratingId)"
        
        callService(urlStr: Api.FF_FreeReveal_URL, params: params, check: "reveal")
    }
    
    @IBAction func addMoney(_ sender: Any)
    {
        resultView.isHidden = true
        cardWalletView.isHidden = false
        addMoney = true
        let addvc = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "AddMoneyVC") as! AddMoneyVC
        present(addvc, animated: false, completion: nil)
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
                //  print("wallet transaction = \(result)")
                if (check == "get")
                {
                    customLoader.hideIndicator()
                    self.msgTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_payment_done")
                    self.msgTitleLabel.textColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                    self.messageLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_money_from_wallet")
                    self.imageVw.image = #imageLiteral(resourceName: "FF_wallet")
                    self.imgWdConstraint.constant = 60
                    self.statusCheck = "walletS"
                    self.resultView.isHidden = false
                    self.cardWalletView.isHidden = true
                    
                    let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&page_no=0"
                    self.callService(urlStr: Api.Wallet_History_URL, params: params, check: "wallet")
                }
                else if (check == "reveal")
                {
                    customLoader.hideIndicator()
                    self.msgTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_payment_done")
                    self.msgTitleLabel.textColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                    
                    self.imageVw.image = #imageLiteral(resourceName: "Free_Reveal")
                    self.imgWdConstraint.constant = 60
                    self.statusCheck = "reveal"
                    self.resultView.isHidden = false
                    self.cardWalletView.isHidden = true
                }
                else {
                    customLoader.hideIndicator()
                    
                    self.addMoney = false
                    wallet_amount = result["userWalletAmount"] as! NSNumber
                    userDefault.set(wallet_amount, forKey: "walletAmt")
                    userDefault.synchronize()
                }
            }
        }
    }
}

