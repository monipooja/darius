//
//  WallHeaderView.swift
//  Darious
//
//  Created by Apple on 08/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class WallHeaderView: UITableViewHeaderFooterView {

    // My opinion & My wall
    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var nameLbl : UILabel!
    @IBOutlet weak var descLabel : UILabel!
    @IBOutlet weak var imageVw : UIImageView!
    @IBOutlet weak var dropdownBtn : UIButton!
    @IBOutlet weak var readMoreBtn : UIButton!
    

    // My wall
    @IBOutlet weak var ratingLabel : UILabel!
    @IBOutlet weak var editBtn : UIButton!
    @IBOutlet weak var starImgVw : UIImageView!
    
    @IBOutlet weak var ratingWd : NSLayoutConstraint!
    @IBOutlet weak var starWd : NSLayoutConstraint!
    @IBOutlet weak var editBtnWd : NSLayoutConstraint!
}
