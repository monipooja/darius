//
//  AdsViewCell.swift
//  Darious
//
//  Created by Apple on 22/10/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdsViewCell: UITableViewCell {

    @IBOutlet weak var nativeViewAd : GADUnifiedNativeAdView!
    @IBOutlet weak var advImageVW : UIImageView!
    @IBOutlet weak var headlineLbl : UILabel!
    @IBOutlet weak var descLabel : UILabel!
    
    @IBOutlet weak var visitSiteBtn : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
