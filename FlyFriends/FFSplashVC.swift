//
//  FFSplashVC.swift
//  Darious
//
//  Created by Apple on 06/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FFSplashVC: UIViewController {
    
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = true
//        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(gotoNext), userInfo: nil, repeats: false)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func gotoNext()
    {
        timer.invalidate()
        let myopinionvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(myopinionvc, animated: false, completion: nil)
    }
}

public func flyfriendstopButtonAction(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 0:
        // My Opinions
        let myopinionvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        controller.present(myopinionvc, animated: false, completion: nil)
        break
    case 1:
        // My Wall
        let wallopinionvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFWallOpinionVC") as! FFWallOpinionVC
        controller.present(wallopinionvc, animated: false, completion: nil)
        break
    case 2:
        // Information
        let infovc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFInfoVC") as! FFInfoVC
        controller.present(infovc, animated: false, completion: nil)
        break
    case 3:
        // Search
        let searchvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFSearchVC") as! FFSearchVC
        controller.present(searchvc, animated: false, completion: nil)
        break
    case 4:
        // Profile
        let profilevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyProfileVC") as! FFMyProfileVC
        controller.present(profilevc, animated: false, completion: nil)
        break
    default:
        break
    }
}

public func flyfriendsBottomViewAction(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 1:
        // Share
        let sharevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFShareVC") as! FFShareVC
        controller.present(sharevc, animated: false, completion: nil)
        break
    case 2:
        // My Friend
        let myfrndvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyFriendsVC") as! FFMyFriendsVC
        controller.present(myfrndvc, animated: false, completion: nil)
        break
    case 3:
        // Darious
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
        break
    case 4:
        // Wallet
        let walletvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFWalletVC") as! FFWalletVC
        controller.present(walletvc, animated: false, completion: nil)
        break
    case 5:
        // Notification
        let notifyvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFNotificationVC") as! FFNotificationVC
        controller.present(notifyvc, animated: false, completion: nil)
        break
    default:
        break
    }
}
