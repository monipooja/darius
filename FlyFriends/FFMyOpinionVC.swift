//
//  FFMyOpinionVC.swift
//  Darious
//
//  Created by Apple on 19/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SwiftyStoreKit

var editStr : String = "no"

class FFMyOpinionVC: UIViewController, GADUnifiedNativeAdLoaderDelegate, GADUnifiedNativeAdDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var myopinionLbl : UILabel!
    @IBOutlet weak var nodataLbl : UILabel!
    @IBOutlet weak var myopinionTableVw : UITableView!
    
    var refreshControl = UIRefreshControl()
    var selfView : UIView!
    
    var opinionArray : [NSDictionary] = []
    var heightArray : [CGFloat] = []
    var linesArray : [Int] = []
    
    var transactionId : String = ""
    var total : String = ""
    var currencyType : String = ""
    var rating_id : NSNumber = 0
    var selectStr : String = ""
    var setBtm : Bool = false
    var selectedCell : MyOpinionTableCell!
    
    var adLoader: GADAdLoader!
    var adViewCell : AdsViewCell!
    weak var rootViewController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        selfView = self.view
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        
        editStr = "no"
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 0) {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                imgCollection[btn.tag].image = imgCollection[btn.tag].image?.withRenderingMode(.alwaysTemplate)
                imgCollection[btn.tag].tintColor = UIColor.white
            }
            else {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
                imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        myopinionLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_opinions")
        
        if (setBtm == false)
        {
            setBottomView()
        }
        
        // MARK : UIRefreshControl
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        if #available(iOS 10.0, *) {
            myopinionTableVw.refreshControl = refreshControl
        } else {
            myopinionTableVw.addSubview(refreshControl)
        }
        
        let headerNib = UINib.init(nibName: "AdsViewCell", bundle: Bundle.main)
        myopinionTableVw.register(headerNib, forCellReuseIdentifier: "AdsViewCell")
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlStr: Api.FF_MyOpinions_URL, params: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
//        NotificationCenter.default.addObserver(self, selector: #selector(openMollie(notification:)), name:NSNotification.Name(rawValue: "isDismissViewController"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(onMolliesuccess(notification:)), name:NSNotification.Name(rawValue: "dismissAfterSuccess"), object: nil)
    }
    
    ///  MARK : Pull To Refresh TableView
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
        self.callService(urlStr: Api.FF_MyOpinions_URL, params: params, check: "refresh")
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
        
        if (editStr == "yes")
        {
            editStr = "no"
            self.heightArray = []
            self.linesArray = []
            self.viewDidLoad()
        }
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
               // print("my opinions = \(result)")
                if (check == "get") || (check == "refresh")
                {
                    if let dataarr = result["data"] as? [NSDictionary]
                    {
                        self.opinionArray = dataarr
                    }
                    if(self.opinionArray != []) {
                        self.myopinionTableVw.reloadData()
                        self.myopinionTableVw.isHidden = false
                        self.heightArray = []
                        self.linesArray = []
                        
                        for _ in self.opinionArray
                        {
                            self.heightArray.append(100)
                            self.linesArray.append(2)
                        }
                    }
                    else {
                        self.myopinionTableVw.isHidden = true
                        self.nodataLbl.isHidden = false
                        self.nodataLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                    }
                    if (check == "refresh")
                    {
                        self.refreshControl.endRefreshing()
                    }
                    customLoader.hideIndicator()
                }
                else if (check == "remove") || (check == "hidden") {
                    customLoader.hideIndicator()
                    self.heightArray = []
                    self.linesArray = []
                    self.viewDidLoad()
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        setBtm = true
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    // Ads Function call
    func loadAds()
    {
        let multipleAdsOptions = GADMultipleAdsAdLoaderOptions()
        multipleAdsOptions.numberOfAds = 5
        
        adLoader = GADAdLoader(adUnitID: AdUnitId, rootViewController: self,
                               adTypes: [GADAdLoaderAdType.unifiedNative],
                               options: [multipleAdsOptions])
        adLoader.delegate = self
        adLoader.load(GADRequest())
        
        rootViewController = self
    }
    
    func adLoader(_ adLoader: GADAdLoader,
                  didReceive nativeAd: GADUnifiedNativeAd)
    {
        // A unified native ad has loaded, and can be displayed.
        
        nativeAd.delegate = self
        adViewCell.nativeViewAd.nativeAd = nativeAd
        
        adViewCell.headlineLbl.text = nativeAd.headline
        adViewCell.descLabel.text = nativeAd.body
        adViewCell.advImageVW.image = nativeAd.icon?.image
        adViewCell.visitSiteBtn.setTitle(nativeAd.callToAction, for: .normal)
        adViewCell.visitSiteBtn.isUserInteractionEnabled = false
        
        adViewCell.nativeViewAd.callToActionView = adViewCell.visitSiteBtn
        
        nativeAd.register(adViewCell.visitSiteBtn, clickableAssetViews: [GADUnifiedNativeAssetIdentifier.callToActionAsset : adViewCell.nativeViewAd.callToActionView!], nonclickableAssetViews: [:])
    }
    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        // The adLoader has finished loading ads, and a new request can be sent.
    }
    public func adLoader(_ adLoader: GADAdLoader,
                         didFailToReceiveAdWithError error: GADRequestError)
    {
    }
    func register(_ adView: UIView, clickableAssetViews: [GADUnifiedNativeAssetIdentifier : UIView], nonclickableAssetViews: [GADUnifiedNativeAssetIdentifier : UIView])
    {
    }
    func nativeAdDidRecordClick(_ nativeAd: GADUnifiedNativeAd) {
       // print("\(#function) called")
    }
    func nativeAdDidRecordImpression(_ nativeAd: GADUnifiedNativeAd) {
       // print("\(#function) called")
    }
    func nativeAdWillPresentScreen(_ nativeAd: GADUnifiedNativeAd) {
       // print("\(#function) called")
    }
    func nativeAdWillDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
       // print("\(#function) called")
    }
    func nativeAdDidDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
       // print("\(#function) called")
    }
    func nativeAdWillLeaveApplication(_ nativeAd: GADUnifiedNativeAd) {
       // print("\(#function) called")
    }
    
    func getProduct(_ purchase: ServicePurchase)
    {
        SwiftyStoreKit.retrieveProductsInfo([appBundleId + "." + purchase.rawValue]) { result in
            if let product = result.retrievedProducts.first {
                self.total = "\(product.price)"
                self.currencyType = product.priceLocale.currencySymbol!
            }
        }
    }
    
    func purchase(_ purchase: ServicePurchase, atomically: Bool) {
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.purchaseProduct(appBundleId + "." + purchase.rawValue, atomically: atomically) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            if case .success(let purchaseObj) = result {
                self.verifyPurchase(purchase)
            
                let downloads = purchaseObj.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                
                // Deliver content from server, then:
                if purchaseObj.needsFinishTransaction {
                    customLoader.hideIndicator()
                    SwiftyStoreKit.finishTransaction(purchaseObj.transaction)
                }
            }
            else {
                customLoader.hideIndicator()
            }
        }
    }
    
    
    
    func verifyPurchase(_ purchase: ServicePurchase) {
        NetworkActivityIndicatorManager.networkOperationStarted()
        
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                let productId = appBundleId + "." + purchase.rawValue
                let purchaseResult = SwiftyStoreKit.verifyPurchase(productId: productId, inReceipt: receipt)
                switch purchaseResult {
                case .purchased:
                    StaticFunctions.showAlert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_payment_done"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "thanks_for_purchasing"), actions: [OkText], controller: self, completion: { (str) in
                        self.addPurchaseCreditToAccount(RegisteredPurchase: appBundleId + "." + purchase.rawValue)
                    })
                    break
                    
                case .notPurchased:
                    self.showAlert(self.alertForVerifyReceipt(result))
                    break
                }
                
            case .error:
                self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: AdSharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
        
    }
    
    func addPurchaseCreditToAccount(RegisteredPurchase: String) {
        
        if (self.selectStr == "remove")
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&amount=\(total)&msg=paid_for_remove_comment&type=remove&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_RemoveComment_URL, params: params, check: "remove")
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&amount=\(total)&msg=pay_for_hidden&type=hidden&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_RemoveComment_URL, params: params, check: "hidden")
        }
    }
}

// MARK : TableView Delegates
extension FFMyOpinionVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return opinionArray.count + (opinionArray.count/3)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            let cell = myopinionTableVw.dequeueReusableCell(withIdentifier: "AdsViewCell") as! AdsViewCell
            
            cell.nativeViewAd.layer.cornerRadius = 44
            cell.nativeViewAd.layer.shadowColor = UIColor.lightGray.cgColor
            cell.nativeViewAd.layer.shadowOpacity = 0.6
            cell.nativeViewAd.layer.shadowRadius = 3
            cell.nativeViewAd.layer.shadowOffset = CGSize(width: 0, height: 0)
            
            return cell
        }
        else {
            let cell = myopinionTableVw.dequeueReusableCell(withIdentifier: "myopncell") as! MyOpinionTableCell
            
            var sectionValue : Int!
            if (indexPath.section > 3) {
                sectionValue = indexPath.section-(indexPath.section/3)
            }
            else {
                sectionValue = indexPath.section
            }
            let dict = opinionArray[sectionValue]
            cell.bgImgVw.layer.cornerRadius = cell.bgImgVw.frame.width/2
            cell.bgImgVw.layer.masksToBounds = true
            
            cell.descLabel.text = dict["rating_desc"] as? String
            cell.descLabel.numberOfLines = linesArray[sectionValue]
            
            // Name set
            if (dict["nickname"] as! String != "")
            {
                cell.nameLabel.text = "\(dict["fullname"] as! String) (@\(dict["nickname"] as! String))"
            }
            else {
                cell.nameLabel.text = dict["fullname"] as? String
            }
            // Profile Pic
            if (dict["profile_pic"] as! String != "") {
                cell.profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                cell.profileImgVw.isHidden = false
                cell.anonymsImgVw.isHidden = true
            }
            //Verified or not
            if (dict["face_status"] as! String == "yes")
            {
                cell.verifiedImg.isHidden = false
            }
            else {
                cell.verifiedImg.isHidden = true
            }
            
            if (Int.init(exactly: dict["user_block"] as! NSNumber)! > 0) || (Int.init(exactly: dict["me_block"] as! NSNumber)! > 0)
            {
                cell.editVwHt.constant = 0
                if (Int.init(exactly: dict["user_block"] as! NSNumber)! > 0) {
                    cell.blockImgVw.isHidden = false
                    cell.blockImgVw.tintColor = ColorCode.FFRemoveBG
                    cell.blockImgVw.image = #imageLiteral(resourceName: "FF_RedBlock")
                }
                else {
                    cell.blockImgVw.isHidden = true
                }
            }
            else {
                cell.editVwHt.constant = 35
                cell.blockImgVw.isHidden = true
            }
            
            // Bottom View Button set
            if (dict["status"] as! String == "hidden")
            {
                cell.anonymsVwHt.constant = 0
            }
            else {
                cell.anonymsVwHt.constant = 35
            }
            
            // Height set
            if(heightArray[sectionValue] > 100) {
                cell.openBtn.isHidden = true
            }
            else {
                cell.openBtn.isHidden = false
            }

            cell.editBtn.addTarget(self, action: #selector(editOpinion(_:)), for: .touchUpInside)
            cell.editTextLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "edit_opinion_mollie")
            cell.anonymsPrcLbl.text = Anonymous_price
            cell.anonymousBtn.addTarget(self, action: #selector(alwaysAnonymous(_:)), for: .touchUpInside)
            cell.anonymsTextLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_always_anonymous")
            cell.removeBtn.addTarget(self, action: #selector(removeComment(_:)), for: .touchUpInside)
            cell.removeTextLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_remove")
            
            cell.openBtn.addTarget(self, action: #selector(openCell(_:)), for: .touchUpInside)
            cell.closeBtn.addTarget(self, action: #selector(closeCell(_:)), for: .touchUpInside)
            cell.profButton.addTarget(self, action: #selector(goTouserProfile(_:)), for: .touchUpInside)
            cell.nameButton.addTarget(self, action: #selector(goTouserProfile(_:)), for: .touchUpInside)
            
            cell.bgView.layer.cornerRadius = 45
            cell.bgView.layer.shadowOffset = CGSize.init(width: 0, height: 0)
            cell.bgView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.bgView.layer.shadowOpacity = 0.8
            cell.bgView.layer.shadowRadius = 4
            
            return cell
        }
    }
    
    @objc func goTouserProfile(_ sender : UIButton) {
        
        let point = sender.convert(CGPoint.zero, to: myopinionTableVw)
        let idxpath = myopinionTableVw.indexPathForRow(at: point)
        var dict : NSDictionary = [:]
        if (idxpath!.section > 3) {
            dict = opinionArray[idxpath!.section-(idxpath!.section/3)]
        }
        else {
            dict = opinionArray[idxpath!.section]
        }
        let profilevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFfriendProfileVC") as! FFfriendProfileVC
        profilevc.passid = dict["to_id"] as! NSNumber
        profilevc.passTag = 0
        present(profilevc, animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            adViewCell = (cell as! AdsViewCell)
            loadAds()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            return 98
        }
        else {
            if (indexPath.section > 3) {
                return heightArray[indexPath.section-(indexPath.section/3)]
            }
            else {
                return heightArray[indexPath.section]
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: myopinionTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func openCell(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: myopinionTableVw)
        let idxpath = myopinionTableVw.indexPathForRow(at: point)
        
        var sectionValue : Int!
        if (idxpath!.section > 3) {
            sectionValue = idxpath!.section-(idxpath!.section/3)
        }
        else {
            sectionValue = idxpath!.section
        }
        let dict = opinionArray[sectionValue]
        let cell = myopinionTableVw.cellForRow(at: idxpath!) as! MyOpinionTableCell
        let ht = ApiResponse.calculateSizeForString(dict["rating_desc"] as! String, sz: 10, maxWidth: cell.descLabel.frame.width, controller: self).height
        if (ht > 30) {
            heightArray[sectionValue] = myopinionTableVw.estimatedRowHeight + (ht-30)
        }
        else {
            heightArray[sectionValue] = myopinionTableVw.estimatedRowHeight
        }
        
        if (cell.anonymsVwHt.constant == 0) && (cell.editVwHt.constant == 0)
        {
            heightArray[sectionValue] = heightArray[sectionValue] - 70
        }
        else if (cell.anonymsVwHt.constant == 0) || (cell.editVwHt.constant == 0){
            heightArray[sectionValue] = heightArray[sectionValue] - 35
        }else {}
        
        linesArray[sectionValue] = 0
        myopinionTableVw.reloadRows(at: [idxpath!], with: .none)
    }
    
    @objc func closeCell(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: myopinionTableVw)
        let idxpath = myopinionTableVw.indexPathForRow(at: point)
   
        if (idxpath!.section > 3) {
            heightArray[idxpath!.section-(idxpath!.section/3)] = 100
            linesArray[idxpath!.section-(idxpath!.section/3)] = 2
        }
        else {
            heightArray[(idxpath?.section)!] = 100
            linesArray[(idxpath?.section)!] = 2
        }
        myopinionTableVw.reloadRows(at: [idxpath!], with: .none)
    }
    
    @objc func alwaysAnonymous(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: myopinionTableVw)
        let idxpath = myopinionTableVw.indexPathForRow(at: point)
        
        var dict : NSDictionary = [:]
        if (idxpath!.section > 3) {
            dict = opinionArray[idxpath!.section-(idxpath!.section/3)]
        }
        else {
            dict = opinionArray[idxpath!.section]
        }
        
        let cell = myopinionTableVw.cellForRow(at: idxpath!) as! MyOpinionTableCell
        selectedCell = cell
        
        getProduct(.anonymous)
        
        selectStr = "hidden"
        rating_id = dict["id"] as! NSNumber
        purchase(.anonymous, atomically: true)
    }
    
    @objc func removeComment(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: myopinionTableVw)
        let idxpath = myopinionTableVw.indexPathForRow(at: point)
        
        var dict : NSDictionary = [:]
        if (idxpath!.section > 3) {
            dict = opinionArray[idxpath!.section-(idxpath!.section/3)]
        }
        else {
            dict = opinionArray[idxpath!.section]
        }
        
        let cell = myopinionTableVw.cellForRow(at: idxpath!) as! MyOpinionTableCell
        selectedCell = cell
        
        selectStr = "remove"
        rating_id = dict["id"] as! NSNumber
        
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&amount=0&msg=paid_for_remove_comment&type=remove&curr_type=\(currencyType)"
        self.callService(urlStr: Api.FF_RemoveComment_URL, params: params, check: "remove")
    }
    
    @objc func editOpinion(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: myopinionTableVw)
        let idxpath = myopinionTableVw.indexPathForRow(at: point)
        
        let cell = self.myopinionTableVw.cellForRow(at: idxpath!) as! MyOpinionTableCell
        let editvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFEditOpinionVC") as! FFEditOpinionVC
        
        if (idxpath!.section > 3) {
            editvc.passDict = self.opinionArray[idxpath!.section-(idxpath!.section/3)]
        }
        else {
            editvc.passDict = self.opinionArray[idxpath!.section]
        }
        editvc.usrname = cell.nameLabel.text!
        self.present(editvc, animated: false, completion: nil)
    }
}

class MyOpinionTableCell : UITableViewCell
{
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var firstView : UIView!
    @IBOutlet weak var bottomView : UIView!
    
    @IBOutlet weak var profileImgVw : UIImageView!
    @IBOutlet weak var blockImgVw : UIImageView!
    @IBOutlet weak var anonymsImgVw : UIImageView!
    @IBOutlet weak var bgImgVw : UIView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var descLabel : UILabel!
    
    @IBOutlet weak var openBtn : UIButton!
    @IBOutlet weak var closeBtn : UIButton!
    @IBOutlet weak var verifiedImg : UIButton!
    @IBOutlet weak var profButton : UIButton!
    @IBOutlet weak var nameButton : UIButton!
    
    @IBOutlet weak var descHt : NSLayoutConstraint!
   
    @IBOutlet weak var editBtn : UIButton!
    @IBOutlet weak var anonymousBtn : UIButton!
    @IBOutlet weak var removeBtn : UIButton!
    @IBOutlet weak var anonymsPrcLbl : UILabel!
    @IBOutlet weak var editTextLbl : UILabel!
    @IBOutlet weak var anonymsTextLbl : UILabel!
    @IBOutlet weak var removeTextLbl : UILabel!
    
    @IBOutlet weak var editVwHt : NSLayoutConstraint!
    @IBOutlet weak var anonymsVwHt : NSLayoutConstraint!
    @IBOutlet weak var removeVwHt : NSLayoutConstraint!
}

// MARK: User facing alerts
extension FFMyOpinionVC {
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        customLoader.hideIndicator()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func showAlert(_ alert: UIAlertController) {
        customLoader.hideIndicator()
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {
        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
            return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        } else if let _ = result.invalidProductIDs.first {
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: "")
        } else {
            let errorString = result.error?.localizedDescription ?? "\(ApiResponse.getLanguageFromUserDefaults(inputString: "unknown_error"))"
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: errorString)
        }
    }
    
    func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
        
        switch result {
        case .success( _):
            //print("Verify receipt Success: \(receipt)")
            return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "receipt_verified"))
        case .error(let error):
            // print("Verify receipt Failed: \(error)")
            switch error {
            case .noReceiptData:
                return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "no_receipt"))
            case .networkError(let error):
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verifying_error")): \(error)")
            default:
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verification_failed")): \(error)")
            }
        }
    }

}


extension SKProduct {
    var localizedPrice123: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = priceLocale
        return formatter.string(from: price)!
    }
}
