//
//  FFfriendProfileVC.swift
//  Darious
//
//  Created by Apple on 09/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds
import SwiftyStoreKit

var profEditStr : String = "no"

class FFfriendProfileVC: UIViewController, GADRewardBasedVideoAdDelegate {
    
    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var upperButton : KGHighLightedButton!
    @IBOutlet weak var upperLabel : UILabel!
    @IBOutlet weak var lowerButton : KGHighLightedButton!
    @IBOutlet weak var lowerLabel : UILabel!
    @IBOutlet weak var blockButton : KGHighLightedButton!
    @IBOutlet weak var blockLabel : UILabel!
    @IBOutlet weak var supportLabel : UILabel!
    @IBOutlet weak var profileImageVW : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var followersCountLabel : UILabel!
    @IBOutlet weak var followersImgVW : UIImageView!
    @IBOutlet weak var blockImgVW : UIImageView!
    
    @IBOutlet weak var userCommenTblVw : UITableView!
    
    // Center report view outlets
    @IBOutlet weak var centerBlkVw : UIView!
    @IBOutlet weak var reportView : CustomUIView!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var reportRmvLbl : UILabel!
    @IBOutlet weak var reportRmvblkLbl : UILabel!
    @IBOutlet var optionLbls : [UILabel]!
    @IBOutlet var optionBtns : [KGHighLightedButton]!
    
    var optionTextArr = ["ff_insults", "ff_hate_message", "ff_abusive", "ff_other"]
    var opinionArray : [NSDictionary] = []
    var heightArray : [CGFloat] = []
    var linesArray : [Int] = []
    
    var passid : NSNumber = 0
    var transactionId : String = ""
    var total : String = ""
    var currencyType : String = ""
    var rating_id : NSNumber = 0
    var selectedCell : FProfOpinionTableCell!
    var selectedDict : NSDictionary = [:]
    var selectRsn : String = ""
    var selectStr : String = ""
    var passTag : Int = 3
    var checkScreen : String = ""
    var selectRow : Int = 0
    var setBtm : Bool = false
    
    /// The reward-based video ad.
    var rewardBasedVideo: GADRewardBasedVideoAd?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        blockImgVW.tintColor = UIColor.white
        
        profEditStr = "no"
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        for btn in btnCollection
        {
            if (btn.tag == passTag) {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                imgCollection[btn.tag].tintColor = UIColor.white
            }
            else {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
                imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        for btn in optionBtns
        {
            btn.addTarget(self, action: #selector(selectReasonToReport(_:)), for: .touchUpInside)
            optionLbls[btn.tag-1].text = ApiResponse.getLanguageFromUserDefaults(inputString: optionTextArr[btn.tag-1])
        }
        //  print("setbtm = \(setBtm)")
        if (setBtm == false)
        {
            setBottomView()
        }
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.passid)"
            self.callService(urlStr: Api.FF_FriendProfile_URL, params: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        //        NotificationCenter.default.addObserver(self, selector: #selector(openMollie(notification:)), name:NSNotification.Name(rawValue: "isDismissViewController"), object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(onMolliesuccess(notification:)), name:NSNotification.Name(rawValue: "dismissAfterReveal"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
        
        if (profEditStr == "yes") {
            profEditStr = "no"
            self.heightArray = []
            self.linesArray = []
            viewDidLoad()
        }
    }
    
    // MARK : Set data on Load
    func setDataOnLoad(dict : NSDictionary)
    {
        titleLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_report_this_opinion")
        reportRmvLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_remove")
        reportRmvblkLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_remove_block")
        supportLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_support_me")
        
        if (dict["nickname"] as! String != ""){
            nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String) (@\(dict["nickname"] as! String))"
        }
        else {
            nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        }
        profileImageVW.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        
        followersCountLabel.text = "\(dict["followers"] as! NSNumber)"
        
        followersImgVW.tintColor = UIColor.init(hexString: ColorCode.FFIconsGray)
        
        if (dict["commented"] as! NSNumber == 0)
        {
            lowerButton.backgroundColor = UIColor.init(hexString: ColorCode.FFBluecomment)
            lowerLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_write")
            lowerButton.isEnabled = true
        }
        else
        {
            lowerButton.backgroundColor = UIColor.init(hexString: ColorCode.FFPinkcomment)
            lowerLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_written")
            lowerButton.isEnabled = true
        }
        
        if (dict["following"] as! NSNumber == 0) {
            upperButton.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            upperLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_follow")
            lowerButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray)
            lowerButton.isEnabled = false
        }
        else {
            upperButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray)
            upperLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_unfollow")
        }
        
        blockImgVW.tintColor = UIColor.init(hexString: "ffffff")
        if (Int.init(exactly: dict["user_block"] as! NSNumber)! > 0)
        {
            upperButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray)
            upperButton.isEnabled = false
            lowerButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray)
            lowerButton.isEnabled = false
            
            blockButton.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            
            self.blockLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_unblock"))"
        }
        else {
            blockButton.backgroundColor = UIColor.init(hexString: "000000")
            self.blockLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_block"))"
        }
    }
    
    @IBAction func followUnfollowFreind(_ sender: Any)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(passid)"
        self.callService(urlStr: Api.FF_FollowUser_URL, params: params, check: "follow")
    }
    
    @IBAction func shareProfile(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(passid)&app_name=flyfriends") else { return }
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ""
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flyfriends")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func writeComment(_ sender: Any)
    {
        let writevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFWriteCommentVC") as! FFWriteCommentVC
        writevc.idpass = passid
        writevc.passTag = self.passTag
        present(writevc, animated: false, completion: nil)
    }
    
    @IBAction func blockUser(_ sender: Any)
    {
        if (blockButton.backgroundColor == UIColor.init(hexString: ColorCode.FFThemeColor))
        {
            selectStr = "unblock"
            
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.passid)&trans_id=\(self.transactionId)&amount=0&type=inapp&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_UnblockUser_URL, params: params, check: "unblock")
        }
        else {
            selectStr = "block"
            
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.passid)&trans_id=\(self.transactionId)&amount=0&type=inapp&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_BlockUser_URL, params: params, check: "block")
        }
        rating_id = 0
    }
    
    @IBAction func reportUser(_ sender: Any)
    {
        let alertcont = UIAlertController.init(title: ApiResponse.getLanguageFromUserDefaults(inputString: "report").capitalized, message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_provide_reason"), preferredStyle: .alert)
        alertcont.addTextField { (msgTextField) in
            msgTextField.placeholder = ""
        }
        let yesaction = UIAlertAction.init(title: ApiResponse.getLanguageFromUserDefaults(inputString: "report"), style: .default) { (action) in
            
            let reportMsg = (alertcont.textFields?[0])?.text
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rat_id=\(self.passid)&msg=\(reportMsg!)&type=user"
            self.callService(urlStr: Api.FF_Report_URL, params: params, check: "report_user")
        }
        let noaction = UIAlertAction.init(title: ApiResponse.getLanguageFromUserDefaults(inputString: "cancel"), style: .cancel, handler: nil)
        alertcont.addAction(yesaction)
        alertcont.addAction(noaction)
        present(alertcont, animated: true, completion: nil)
    }
    
    // Report View Actions
    @IBAction func hideReportView(_ sender: Any)
    {
        centerBlkVw.isHidden = true
        reportView.isHidden = true
        for btn in optionBtns
        {
            btn.backgroundColor = UIColor.white
        }
    }
    
    @objc func selectReasonToReport(_ sender : UIButton)
    {
        for btn in optionBtns
        {
            if (btn.tag == sender.tag)
            {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                selectRsn = optionTextArr[btn.tag-1]
            }
            else {
                btn.backgroundColor = UIColor.white
            }
        }
    }
    
    @IBAction func reportRemove(_ sender: Any)
    {
        if (selectRsn == "")
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_select_reason"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rat_id=\(rating_id)&msg=\(selectRsn)&type=remove"
            self.callService(urlStr: Api.FF_Report_URL, params: params, check: "report")
        }
    }
    
    @IBAction func reportRemoveBlock(_ sender: Any)
    {
        if (selectRsn == "")
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_select_reason"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rat_id=\(rating_id)&msg=\(selectRsn)&type=remove_block"
            self.callService(urlStr: Api.FF_Report_URL, params: params, check: "report")
        }
    }
    
    @IBAction func userRating(_ sender: Any)
    {
//        let starvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyStarsVC") as! FFMyStarsVC
//        starvc.passUser_id = "\(passid)"
//        starvc.passTag = self.passTag
//        present(starvc, animated: false, completion: nil)
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        if (checkScreen == "")
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        setBtm = true
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
                // print("friend profile = \(result)")
                if (check == "get")
                {
                    let dict = result["data"] as! NSDictionary
                    self.setDataOnLoad(dict: dict)
                    self.userCommenTblVw.isHidden = false
                    if (dict["me_block"] as! NSNumber != 0)
                    {
                        self.opinionArray = []
                        self.userCommenTblVw.reloadData()
                    }
                    else
                    {
                        if let dataarr = dict["opinions"] as? [NSDictionary]
                        {
                            self.opinionArray = dataarr
                        }
                        if(self.opinionArray != []) {
                            self.userCommenTblVw.reloadData()
                            
                            for dict in self.opinionArray
                            {
                                self.heightArray.append(105)
                                self.linesArray.append(2)
                            }
                        }
                        else {}
                    }
                    customLoader.hideIndicator()
                }
                else if (check == "follow")
                {
                    if (self.upperButton.backgroundColor == UIColor.init(hexString: ColorCode.FFThemeColor))
                    {
                        var followcount = Int(self.followersCountLabel.text!)!
                        followcount += 1
                        self.followersCountLabel.text = "\(followcount)"
                        self.upperLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_unfollow")
                        self.upperButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray)
                        if (self.lowerLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "ff_write")) {
                            self.lowerButton.backgroundColor = UIColor.init(hexString: ColorCode.FFBluecomment)
                        }
                        else {
                            self.lowerButton.backgroundColor = UIColor.init(hexString: ColorCode.FFPinkcomment)
                        }
                        self.lowerButton.isEnabled = true
                    }
                    else {
                        var followcount = Int(self.followersCountLabel.text!)!
                        followcount -= 1
                        self.followersCountLabel.text = "\(followcount)"
                        self.upperLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_follow")
                        self.upperButton.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                        self.lowerButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray)
                        self.lowerButton.isEnabled = false
                    }
                    customLoader.hideIndicator()
                }
                else if (check == "report") {
                    let msgstr = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msgstr, actions: [OkText], controller: self, completion: { (str) in
                        
                        self.centerBlkVw.isHidden = true
                        self.reportView.isHidden = true
                        self.selectRsn = ""
                        for btn in self.optionBtns
                        {
                            btn.backgroundColor = UIColor.white
                        }
                        self.heightArray = []
                        self.linesArray = []
                        self.viewDidLoad()
                    })
                }
                else if (check == "block") || (check == "unblock") {
                    customLoader.hideIndicator()
                    self.viewDidLoad()
                    self.transactionId = ""
                }
                else if (check == "reward") {
                    //  print("reward result = \(result)")
                    Free_Reveal = result["free_reveal"] as! Bool
                }
                else if (check == "remove") || (check == "hidden") {
                    customLoader.hideIndicator()
                    self.heightArray = []
                    self.linesArray = []
                    self.viewDidLoad()
                }
                else if (check == "report_user") {
                    let msgstr = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msgstr, actions: [OkText], controller: self, completion: { (str) in
                    })
                }
                else
                {
                    let datadict = result["data"] as! NSDictionary
                    let editdict = NSMutableDictionary.init(dictionary: self.selectedDict)
                    
                    editdict["name"] = "\(datadict["first_name"] as! String) \(datadict["last_name"] as! String)"
                    editdict["view_status"] = "show"
                    if (self.selectedDict["nickname"] as! String != "")
                    {
                        self.selectedCell.nameLabel.text = "\(editdict["name"] as! String) (@\(self.selectedDict["nickname"] as! String))"
                    }
                    else {
                        self.selectedCell.nameLabel.text = editdict["name"] as? String
                    }
                    if (self.selectedDict["profile_pic"] as! String != "") {
                        self.selectedCell.profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(self.selectedDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                        self.selectedCell.profileImgVw.isHidden = false
                    }
                    self.opinionArray[self.selectRow] = NSDictionary.init(dictionary: editdict)
                    self.selectedCell.revealVwHt.constant = 0
                    customLoader.hideIndicator()
                    
                    if (check == "freereveal") {
                        Free_Reveal = result["free_reveal"] as! Bool
                        let errorvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFErrorPopupVC") as! FFErrorPopupVC
                        errorvc.statusCheck = "freereveal"
                        errorvc.modalPresentationStyle = .overCurrentContext
                        self.present(errorvc, animated: false, completion: nil)
                    }
                }
            }
        }
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    @IBAction func supportMeWatchingVideo(_ sender: Any)
    {
        loadRewardVideoAds()
    }
    
    /// Reward AdVideo Work
    func loadRewardVideoAds()
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        
        rewardBasedVideo = GADRewardBasedVideoAd.sharedInstance()
        rewardBasedVideo?.delegate = self
        
        if rewardBasedVideo?.isReady == false
        {
            rewardBasedVideo?.load(GADRequest(),
                                   withAdUnitID: "ca-app-pub-7225491090572339/4225285890")
            // test - "ca-app-pub-3940256099942544/1712485313")
            //adRequestInProgress = true
        }
    }
    
    // MARK: GADRewardBasedVideoAdDelegate implementation
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didFailToLoadWithError error: Error) {
        //  adRequestInProgress = false
        // print("Reward based video ad failed to load: \(error.localizedDescription)")
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        // print("Reward based video ad is received.")
        rewardBasedVideo?.present(fromRootViewController: self)
    }
    
    func rewardBasedVideoAdDidStartPlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        // print("Reward based video ad started playing.")
        customLoader.hideIndicator()
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        // print("Reward based video ad is closed.")
        if (Free_Reveal == true)
        {
            let errorvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFErrorPopupVC") as! FFErrorPopupVC
            errorvc.statusCheck = "reveal"
            errorvc.modalPresentationStyle = .overCurrentContext
            self.present(errorvc, animated: false, completion: nil)
        }
    }
    
    //    func rewardBasedVideoAdDidOpen(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
    //        print("Opened reward based video ad.")
    //    }
    //    func rewardBasedVideoAdWillLeaveApplication(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
    //        print("Reward based video ad will leave application.")
    //    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        //print("Reward received with currency: \(reward.type), amount \(reward.amount).")
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(passid)"
        self.callService(urlStr: Api.FF_RewardVideo_URL, params: params, check: "reward")
    }
    
    func getProduct(_ purchase: ServicePurchase)
    {
        SwiftyStoreKit.retrieveProductsInfo([appBundleId + "." + purchase.rawValue]) { result in
            if let product = result.retrievedProducts.first {
                self.total = "\(product.price)"
                self.currencyType = product.priceLocale.currencySymbol!
            }
        }
    }
    
    func purchase(_ purchase: ServicePurchase, atomically: Bool) {
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.purchaseProduct(appBundleId + "." + purchase.rawValue, atomically: atomically) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            if case .success(let purchaseObj) = result {
                self.verifyPurchase(purchase)
                
                let downloads = purchaseObj.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                
                // Deliver content from server, then:
                if purchaseObj.needsFinishTransaction {
                    customLoader.hideIndicator()
                    SwiftyStoreKit.finishTransaction(purchaseObj.transaction)
                }
            }
            else {
                customLoader.hideIndicator()
            }
        }
    }
    
    func verifyPurchase(_ purchase: ServicePurchase) {
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                let productId = appBundleId + "." + purchase.rawValue
                let purchaseResult = SwiftyStoreKit.verifyPurchase(productId: productId, inReceipt: receipt)
                switch purchaseResult {
                case .purchased:
                    StaticFunctions.showAlert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_payment_done"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "thanks_for_purchasing"), actions: [OkText], controller: self, completion: { (str) in
                        self.addPurchaseCreditToAccount(RegisteredPurchase: appBundleId + "." + purchase.rawValue)
                    })
                    break
                    
                case .notPurchased:
                    self.showAlert(self.alertForVerifyReceipt(result))
                    break
                }
            case .error:
                self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: AdSharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }
    
    func addPurchaseCreditToAccount(RegisteredPurchase: String) {
        
        if (self.selectStr == "block")
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.passid)&trans_id=\(self.transactionId)&amount=\(total)&type=inapp&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_BlockUser_URL, params: params, check: "block")
        }
        else if (self.selectStr == "unblock")
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.passid)&trans_id=\(self.transactionId)&amount=\(total)&type=inapp&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_UnblockUser_URL, params: params, check: "unblock")
        }
        else if (self.selectStr == "show_user") {
            
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&trans_id=\(self.transactionId)&amount=\(total)&type=inapp&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_PeopleReveal_URL, params: params, check: "reveal")
        }
        else if (self.selectStr == "remove")
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&amount=\(total)&msg=paid_for_remove_comment&type=remove&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_RemoveComment_URL, params: params, check: "remove")
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&amount=\(total)&msg=pay_for_hidden&type=hidden&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_RemoveComment_URL, params: params, check: "hidden")
        }
    }
}

// MARK : TableView Delegates
extension FFfriendProfileVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return opinionArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = userCommenTblVw.dequeueReusableCell(withIdentifier: "fprofopncell") as! FProfOpinionTableCell
        
        let dict = opinionArray[indexPath.section]
        
        cell.bgImgVw.layer.cornerRadius = cell.bgImgVw.frame.width/2
        cell.bgImgVw.layer.masksToBounds = true
        
        cell.descLabel.text = dict["rating_desc"] as? String
        cell.descLabel.numberOfLines = linesArray[indexPath.section]
        
        if (Free_Reveal == false) {
            cell.revealPrcLbl.text = Reveal_price
        }
        else {
            cell.revealPrcLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_free")
        }
        cell.anonymsPrcLbl.text = Anonymous_price
        
        if (dict["from_id"] as! NSNumber == user_id)
        {
            if (dict["profile_pic"] as! String != "") {
                cell.profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                cell.profileImgVw.isHidden = false
                cell.anonymsImgVw.isHidden = true
            }
            if (dict["nickname"] as! String != "")
            {
                cell.nameLabel.text = "\(userData["first_name"] as! String) \(userData["last_name"] as! String) (@\(dict["nickname"] as! String))"
            }
            else {
                cell.nameLabel.text = "\(userData["first_name"] as! String) \(userData["last_name"] as! String)"
            }
            cell.reportBtn.isHidden = true
            cell.editView.isHidden = false
            
            if (dict["status"] as! String == "hidden")
            {
                cell.anonymsVwHt.constant = 0
            }
            else {
                cell.anonymsVwHt.constant = 35
            }
            cell.removeVwHt.constant = 35
        }
        else {
            cell.reportBtn.isHidden = false
            cell.editView.isHidden = true
            cell.anonymsVwHt.constant = 0
            cell.removeVwHt.constant = 0
            
            // name & profilepic set
            if (dict["status"] as! String == "hidden") && (dict["view_status"] as! String == "active")
            {
                cell.nameLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_anonymous")) \(indexPath.section+1) \(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_hidden"))"
                cell.bgImgVw.backgroundColor = ColorCode.FFHiddenBG
                cell.anonymsImgVw.alpha = 0.3
                cell.profileImgVw.isHidden = true
                cell.anonymsImgVw.isHidden = false
            }
            else if (dict["status"] as! String == "active") && (dict["view_status"] as! String == "active")
            {
                cell.nameLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_anonymous")) \(indexPath.section+1)"
                cell.bgImgVw.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                cell.anonymsImgVw.alpha = 1
                cell.profileImgVw.isHidden = true
                cell.anonymsImgVw.isHidden = false
            }
            else if (dict["status"] as! String == "active") && (dict["view_status"] as! String == "show")
            {
                if (dict["nickname"] as! String != "")
                {
                    cell.nameLabel.text = "\(dict["name"] as! String) (@\(dict["nickname"] as! String))"
                }
                else {
                    cell.nameLabel.text = dict["name"] as? String
                }
                cell.profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                cell.profileImgVw.isHidden = false
                cell.anonymsImgVw.isHidden = true
                
            }
            else {
                cell.nameLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_anonymous")) \(indexPath.section+1) \(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_hidden"))"
                cell.bgImgVw.backgroundColor = ColorCode.FFHiddenBG
                cell.anonymsImgVw.alpha = 0.3
                cell.profileImgVw.isHidden = true
                cell.anonymsImgVw.isHidden = false
            }
            // user block
            if (dict["view_status"] as! String == "show")
            {
                cell.revealVwHt.constant = 0
            }
            else {
                if (Int.init(exactly: dict["user_block"] as! NSNumber)! > 0) || (Int.init(exactly: dict["me_block"] as! NSNumber)! > 0) || (dict["status"] as! String == "hidden")
                {
                    cell.revealVwHt.constant = 0
                }
                else {
                    cell.revealVwHt.constant = 35
                }
            }
        }
        //Verified or not
        if (dict["face_status"] as! String == "yes")
        {
            cell.verifiedImg.isHidden = false
        }
        else {
            cell.verifiedImg.isHidden = true
        }
        // cell open/close set
        if(heightArray[indexPath.section] > 105)
        {
            cell.openBtn.isHidden = true
        }
        else {
            cell.openBtn.isHidden = false
        }
        
        cell.openBtn.addTarget(self, action: #selector(openCell(_:)), for: .touchUpInside)
        cell.closeBtn.addTarget(self, action: #selector(closeCell(_:)), for: .touchUpInside)
        cell.reportBtn.addTarget(self, action: #selector(reportComment(_:)), for: .touchUpInside)
        cell.reportBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_report"), for: .normal)
        cell.revealBtn.addTarget(self, action: #selector(revealUser(_:)), for: .touchUpInside)
        cell.revealTextLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_reveal")
        cell.editBtn.addTarget(self, action: #selector(editOpinion(_:)), for: .touchUpInside)
        cell.editTextLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_edit_opinion")
        cell.anonymousBtn.addTarget(self, action: #selector(alwaysAnonymous(_:)), for: .touchUpInside)
        cell.anonymsTextLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_always_anonymous")
        cell.removeBtn.addTarget(self, action: #selector(removeComment(_:)), for: .touchUpInside)
        cell.removeTextLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_remove")
        
        addShadow(passVw: cell.bgView)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightArray[indexPath.section]
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: userCommenTblVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func openCell(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: userCommenTblVw)
        let idxpath = userCommenTblVw.indexPathForRow(at: point)
        let dict = opinionArray[(idxpath?.section)!]
        
        let cell = userCommenTblVw.cellForRow(at: idxpath!) as! FProfOpinionTableCell
        let ht = ApiResponse.calculateSizeForString(dict["rating_desc"] as! String, sz: 10, maxWidth: cell.descLabel.frame.width, controller: self).height
        if (ht > 30) {
            heightArray[(idxpath?.section)!] = userCommenTblVw.estimatedRowHeight + (ht-30)
        }
        else {
            heightArray[(idxpath?.section)!] = userCommenTblVw.estimatedRowHeight
        }
        
        if (dict["from_id"] as! NSNumber == user_id) {
            if (cell.anonymsVwHt.constant == 0)
            {
                heightArray[(idxpath?.section)!] = heightArray[(idxpath?.section)!] - 35
            }
        }
        else {
            if (dict["view_status"] as! String == "show")
            {
                heightArray[(idxpath?.section)!] = heightArray[(idxpath?.section)!] - 105
            }
            else {
                heightArray[(idxpath?.section)!] = heightArray[(idxpath?.section)!] - 70
            }
            
            if (dict["view_status"] as! String != "show")
            {
                if (Int.init(exactly: dict["user_block"] as! NSNumber)! > 0) || (Int.init(exactly: dict["me_block"] as! NSNumber)! > 0) || (dict["status"] as! String == "hidden")
                {
                    heightArray[(idxpath?.section)!] = heightArray[(idxpath?.section)!] - 35
                }
            }
        }
        linesArray[(idxpath?.section)!] = 0
        userCommenTblVw.reloadRows(at: [idxpath!], with: .none)
    }
    
    @objc func closeCell(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: userCommenTblVw)
        let idxpath = userCommenTblVw.indexPathForRow(at: point)
        
        heightArray[(idxpath?.section)!] = 105
        linesArray[(idxpath?.section)!] = 2
        userCommenTblVw.reloadRows(at: [idxpath!], with: .none)
    }
    
    @objc func revealUser(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: userCommenTblVw)
        let idxpath = userCommenTblVw.indexPathForRow(at: point)
        let dict = opinionArray[(idxpath?.section)!]
        
        let cell = userCommenTblVw.cellForRow(at: idxpath!) as! FProfOpinionTableCell
        selectedCell = cell
        
        selectStr = "show_user"
        rating_id = dict["id"] as! NSNumber
        selectRow = (idxpath?.section)!
        selectedDict = dict
        
        if (Free_Reveal == false)
        {
            getProduct(.reveal)
            purchase(.reveal, atomically: true)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(rating_id)"
            
            callService(urlStr: Api.FF_FreeReveal_URL, params: params, check: "freereveal")
        }
    }
    
    @objc func alwaysAnonymous(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: userCommenTblVw)
        let idxpath = userCommenTblVw.indexPathForRow(at: point)
        let dict = opinionArray[(idxpath?.section)!]
        let cell = userCommenTblVw.cellForRow(at: idxpath!) as! FProfOpinionTableCell
        selectedCell = cell
        
        getProduct(.anonymous)
        
        selectStr = "hidden"
        rating_id = dict["id"] as! NSNumber
        purchase(.anonymous, atomically: true)
    }
    
    @objc func removeComment(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: userCommenTblVw)
        let idxpath = userCommenTblVw.indexPathForRow(at: point)
        let dict = opinionArray[(idxpath?.section)!]
        let cell = userCommenTblVw.cellForRow(at: idxpath!) as! FProfOpinionTableCell
        selectedCell = cell
       
        selectStr = "remove"
        rating_id = dict["id"] as! NSNumber
        
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&rating_id=\(self.rating_id)&amount=0&msg=paid_for_remove_comment&type=remove&curr_type=\(currencyType)"
        self.callService(urlStr: Api.FF_RemoveComment_URL, params: params, check: "remove")
    }
    
    @objc func editOpinion(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: userCommenTblVw)
        let idxpath = userCommenTblVw.indexPathForRow(at: point)
        let dict = opinionArray[(idxpath?.section)!]
        let cell = userCommenTblVw.cellForRow(at: idxpath!) as! FProfOpinionTableCell
        
        let editvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFEditOpinionVC") as! FFEditOpinionVC
        editvc.passDict = dict
        editvc.usrname = cell.nameLabel.text!
        self.present(editvc, animated: false, completion: nil)
    }
    
    @objc func reportComment(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: userCommenTblVw)
        let idxpath = userCommenTblVw.indexPathForRow(at: point)
        let dict = opinionArray[(idxpath?.section)!]
        rating_id = dict["id"] as! NSNumber
        centerBlkVw.isHidden = false
        reportView.isHidden = false
    }
    
    func addShadow(passVw: UIView)
    {
        passVw.layer.cornerRadius = 45
        passVw.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        passVw.layer.shadowColor = UIColor.lightGray.cgColor
        passVw.layer.shadowOpacity = 0.8
        passVw.layer.shadowRadius = 4
    }
}

class FProfOpinionTableCell : UITableViewCell
{
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var firstView : UIView!
    @IBOutlet weak var bottomView : UIView!
    
    @IBOutlet weak var profileImgVw : UIImageView!
    @IBOutlet weak var anonymsImgVw : UIImageView!
    @IBOutlet weak var bgImgVw : UIView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var descLabel : UILabel!
    
    @IBOutlet weak var reamMoreBtn : UIButton!
    @IBOutlet weak var openBtn : UIButton!
    @IBOutlet weak var closeBtn : UIButton!
    
    @IBOutlet weak var reportBtn : UIButton!
    @IBOutlet weak var verifiedImg : UIButton!
    
    @IBOutlet weak var revealBtn : UIButton!
    @IBOutlet weak var revealPrcLbl : UILabel!
    @IBOutlet weak var revealTextLbl : UILabel!
    @IBOutlet weak var anonymousBtn : UIButton!
    @IBOutlet weak var anonymsPrcLbl : UILabel!
    @IBOutlet weak var anonymsTextLbl : UILabel!
    @IBOutlet weak var removeBtn : UIButton!
    @IBOutlet weak var removeTextLbl : UILabel!
    @IBOutlet weak var editBtn : UIButton!
    @IBOutlet weak var editTextLbl : UILabel!
    @IBOutlet weak var editView : UIView!
    
    @IBOutlet weak var anonymsVwHt : NSLayoutConstraint!
    @IBOutlet weak var removeVwHt : NSLayoutConstraint!
    @IBOutlet weak var revealVwHt : NSLayoutConstraint!
}

// MARK: User facing alerts
extension FFfriendProfileVC {
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        customLoader.hideIndicator()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func showAlert(_ alert: UIAlertController) {
        customLoader.hideIndicator()
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {
        
        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
            return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        } else if let _ = result.invalidProductIDs.first {
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: "")
        } else {
            let errorString = result.error?.localizedDescription ?? "\(ApiResponse.getLanguageFromUserDefaults(inputString: "unknown_error"))"
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: errorString)
        }
    }
    
    func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
        
        switch result {
        case .success( _):
            //print("Verify receipt Success: \(receipt)")
            return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "receipt_verified"))
        case .error(let error):
            // print("Verify receipt Failed: \(error)")
            switch error {
            case .noReceiptData:
                return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "no_receipt"))
            case .networkError(let error):
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verifying_error")): \(error)")
            default:
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verification_failed")): \(error)")
            }
        }
    }
}

