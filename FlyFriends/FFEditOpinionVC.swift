//
//  FFEditOpinionVC.swift
//  Darious
//
//  Created by Apple on 07/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import SwiftyStoreKit

class FFEditOpinionVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var commentTableVw : UITableView!
    @IBOutlet weak var commentTV : customTextView!
    @IBOutlet weak var profileImgVw : UIImageView!
    @IBOutlet weak var blackVw : UIView!
    @IBOutlet weak var btmBlackVw : UIView!
    @IBOutlet weak var saveBlackVw : UIView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var editLabel : UILabel!
    @IBOutlet weak var cmntLabel : UILabel!
    @IBOutlet weak var textCountLbl : UILabel!
    
    @IBOutlet weak var editImgVw : UIImageView!
    @IBOutlet weak var editBtn : KGHighLightedButton!
    @IBOutlet weak var saveBtn : KGHighLightedButton!
    
    var passDict : NSDictionary = [:]
    var transactionId : String = ""
    var usrname : String = ""
    var total : String = ""
    var currencyType : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)

        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 0) {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                imgCollection[btn.tag].tintColor = UIColor.white
                imgCollection[btn.tag].image = imgCollection[btn.tag].image?.withRenderingMode(.alwaysTemplate)
            }
            else {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
                imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        setDataOnLoad()
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.commentTableVw.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
//        NotificationCenter.default.addObserver(self, selector: #selector(openMollie(notification:)), name:NSNotification.Name(rawValue: "isDismissViewController"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(onMolliesuccess(notification:)), name:NSNotification.Name(rawValue: "dismissAfterSuccess"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    func setDataOnLoad()
    {
        editLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_edit_opinion")
        cmntLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_comment")
        saveBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "save"), for: .normal)
        
//        let edit_inapp = userDefault.value(forKey: "edit_inapp") as? Bool
//        if (edit_inapp == nil) || (edit_inapp == false)
//        {
//            //editBtn.setTitle("", for: .normal)
//            editBtn.isHidden = false
//            saveBtn.isHidden = true
//        }
//        else {
//           // getProduct(.editopn)
//            self.editBtn.isHidden = true
//            self.editImgVw.isHidden = true
//            self.blackVw.isHidden = true
//            self.btmBlackVw.isHidden = true
//            self.saveBlackVw.isHidden = true
//            self.saveBtn.isHidden = false
//            self.commentTableVw.reloadData()
//        }
        
        editBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 3)
        
        if (passDict["profile_pic"] as! String != "") {
            profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(passDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        }
        profileImgVw.layer.cornerRadius = profileImgVw.frame.width/2
        profileImgVw.layer.masksToBounds = true
        
        nameLabel.text = usrname
        commentTV.text = passDict["rating_desc"] as? String
        textCountLbl.text = "\((passDict["rating_desc"] as! String).count)/500"
    }
    
    @IBAction func submitData(_ sender: Any)
    {
        if (commentTV.text == "")
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_here"), controller: self)
        }
        else {
            let commentStr = (commentTV.text)!.replacingOccurrences(of: "'", with: "\\'")
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(passDict["to_id"] as! NSNumber)&rat_1=0&rat_2=0&rat_3=0&rat_4=0&rat_5=0&rat_6=0&rat_7=0&rat_8=0&rat_9=0&rat_10=0&rat_comment=\(commentStr)&payId=\(self.transactionId)&rating_id=\(passDict["id"] as! NSNumber)&type=inapp"
            //  print("paaaa = \(params)")
            self.callService(urlStr: Api.FF_WriteComment_URL, params: params, check: "write")
        }
    }
    
    @IBAction func editopnPrc(_ sender: Any)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&amount=0&msg=paid_for_edit_opinion&curr_type=\(currencyType)"
        self.callService(urlStr: Api.FF_InsertWallet_URL, params: params, check: "edit")
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
               //  print("edittttt = \(result)")
                if (check == "write")
                {
                    self.transactionId = ""
                    editStr = "yes"
                    profEditStr = "yes"
                    let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msg, actions: [OkText], controller: self, completion: { (str) in
                        self.dismiss(animated: false, completion: nil)
                    })
                    userDefault.set(false, forKey: "edit_inapp")
                    userDefault.synchronize()
                    customLoader.hideIndicator()
                }
                else {
                    // after edit
                    customLoader.hideIndicator()
                    self.editBtn.isHidden = true
                    self.editImgVw.isHidden = true
                    self.blackVw.isHidden = true
                    self.btmBlackVw.isHidden = true
                    self.saveBlackVw.isHidden = true
                    self.saveBtn.isHidden = false
                    self.commentTableVw.reloadData()
                    
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }

    func getProduct(_ purchase: ServicePurchase)
    {
        SwiftyStoreKit.retrieveProductsInfo([appBundleId + "." + purchase.rawValue]) { result in
            if let product = result.retrievedProducts.first {
                self.total = "\(product.price)"
                self.currencyType = product.priceLocale.currencySymbol!
            }
        }
    }
    
    func purchase(_ purchase: ServicePurchase, atomically: Bool)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.purchaseProduct(appBundleId + "." + purchase.rawValue, atomically: atomically) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            if case .success(let purchaseObj) = result {
            
                self.verifyPurchase(purchase)
                
                let downloads = purchaseObj.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                
                // Deliver content from server, then:
                if purchaseObj.needsFinishTransaction {
                    customLoader.hideIndicator()
                    SwiftyStoreKit.finishTransaction(purchaseObj.transaction)
                }
            }
            else {
                customLoader.hideIndicator()
            }
        }
    }
    
    func verifyPurchase(_ purchase: ServicePurchase) {
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                let productId = appBundleId + "." + purchase.rawValue
                let purchaseResult = SwiftyStoreKit.verifyPurchase(productId: productId, inReceipt: receipt)
                switch purchaseResult {
                case .purchased:
                    StaticFunctions.showAlert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_payment_done"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "thanks_for_purchasing"),actions: [OkText], controller: self, completion: { (str) in
                        self.addPurchaseCreditToAccount(RegisteredPurchase: appBundleId + "." + purchase.rawValue)
                    })
                    break
                    
                case .notPurchased:
                    self.showAlert(self.alertForVerifyReceipt(result))
                    break
                }
            case .error:
                self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: AdSharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }
    
    func addPurchaseCreditToAccount(RegisteredPurchase: String) {
        
        userDefault.set(true, forKey: "edit_inapp")
        userDefault.synchronize()
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&amount=\(total)&msg=paid_for_edit_opinion&curr_type=\(currencyType)"
        self.callService(urlStr: Api.FF_InsertWallet_URL, params: params, check: "edit")
    }
}

extension FFEditOpinionVC : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if (textView.text.count+1 <= 500)
        {
            if (text == "") {
                textCountLbl.text = "\(textView.text.count-1)/500"
            }
            else {
                textCountLbl.text = "\(textView.text.count+1)/500"
            }
            return true
        }
        else {
            if (text == "")
            {
                textCountLbl.textColor = UIColor.init(hexString: ColorCode.FFIconsGray)
                textCountLbl.text = "\(textView.text.count-1)/500"
                return true
            }
            else {
                textCountLbl.textColor = ColorCode.FFRemoveBG
                if (textView.text.count > 500)
                {
                    let range = textView.text.index(textView.text.endIndex, offsetBy: -(textView.text.count-500))..<textView.text.endIndex
                    textView.text.removeSubrange(range)
                    textCountLbl.text = "500/500"
                    return true
                }
                else {
                    textCountLbl.text = "\(textView.text.count)/500"
                    return false
                }
            }
        }
    }
}


// MARK: User facing alerts
extension FFEditOpinionVC {
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        customLoader.hideIndicator()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func showAlert(_ alert: UIAlertController) {
        customLoader.hideIndicator()
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {
        
        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
            return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        } else if let _ = result.invalidProductIDs.first {
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: "")
        } else {
            let errorString = result.error?.localizedDescription ?? "\(ApiResponse.getLanguageFromUserDefaults(inputString: "unknown_error"))"
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: errorString)
        }
    }
    
    func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
        
        switch result {
        case .success( _):
            //print("Verify receipt Success: \(receipt)")
            return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "receipt_verified"))
        case .error(let error):
            // print("Verify receipt Failed: \(error)")
            switch error {
            case .noReceiptData:
                return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "no_receipt"))
            case .networkError(let error):
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verifying_error")): \(error)")
            default:
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verification_failed")): \(error)")
            }
        }
    }
}
