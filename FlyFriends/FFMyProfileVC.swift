//
//  FFMyProfileVC.swift
//  Darious
//
//  Created by Apple on 22/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import GoogleMobileAds

class FFMyProfileVC: UIViewController, GADRewardBasedVideoAdDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    @IBOutlet weak var topLabel : UILabel!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    @IBOutlet var LblCollection : [UILabel]!
    
    @IBOutlet weak var profileImageVW : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var followersCountLabel : UILabel!
    @IBOutlet weak var followersImgVW : UIImageView!
    
    @IBOutlet weak var rewardLabel : UILabel!
    @IBOutlet weak var byYouLabel : UILabel!
    @IBOutlet weak var actionLabel : UILabel!
    @IBOutlet var actionCollection : [KGHighLightedButton]!
    
    @IBOutlet weak var totalVideoWatchedLbl : UILabel!
    @IBOutlet weak var totalFreeRevealLbl : UILabel!
    @IBOutlet weak var freeRevealUsedLbl : UILabel!
    @IBOutlet weak var freeRevealAvailable : UILabel!
    @IBOutlet weak var nextfreeReveal : UILabel!
    
    var didAppearCall : Bool = false
    var actionText = ["ff_opinions", "ff_always_anonymous", "ff_revealed", "ff_removed", "ff_blocked"]
    var lbltextArray : [String] = ["ff_max_free_reveal", "ff_total_video_watched", "ff_free_reveal", "ff_free_reveal_used", "ff_reveal_available", "ff_next_free_reveal"]
    
    /// The reward-based video ad.
    var rewardBasedVideo: GADRewardBasedVideoAd?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        followersImgVW.tintColor = UIColor.init(hexString: ColorCode.FFIconsGray)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 4) {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                imgCollection[btn.tag].tintColor = UIColor.white
                imgCollection[btn.tag].image = imgCollection[btn.tag].image?.withRenderingMode(.alwaysTemplate)
            }
            else {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
                imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        var j = 0
        for lbl in LblCollection
        {
            lbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: lbltextArray[j])
            j += 1
        }
        setBottomView()
        
        rewardLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "your_reward")
        byYouLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "by_you")
        actionLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_actions")
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_profile")
        
        var i = 0
        for action in actionCollection
        {
            action.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: actionText[i]), for: .normal)
            i += 1
        }
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlStr: Api.FF_MyProfile_URL, params: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
        if (didAppearCall == true)
        {
            DispatchQueue.main.async {
                customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
                self.callService(urlStr: Api.FF_MyProfile_URL, params: params, check: "get")
            }
        }
    }
    
    @IBAction func opinionList(_ sender: Any)
    {
        let opinionvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(opinionvc, animated: true, completion: nil)
    }
    
    @IBAction func anonymousList(_ sender: Any)
    {
        let anymsvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFAnonymousListVC") as! FFAnonymousListVC
        present(anymsvc, animated: true, completion: nil)
    }
    
    @IBAction func revealedList(_ sender: Any)
    {
        let revealvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFRevealedVC") as! FFRevealedVC
        present(revealvc, animated: true, completion: nil)
    }
    
    @IBAction func removedList(_ sender: Any)
    {
        let removevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFRemovedVC") as! FFRemovedVC
        present(removevc, animated: true, completion: nil)
    }
    
    @IBAction func blockedList(_ sender: Any)
    {
        let blockedvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFBlockedListVC") as! FFBlockedListVC
        present(blockedvc, animated: true, completion: nil)
    }
    
    @IBAction func watchVideoForFreeReveal(_ sender: Any)
    {
        loadRewardVideoAds()
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
              //   print("my profile = \(result)")
                if (check == "get")
                {
                    let datadict = result["data"] as! NSDictionary
                    self.profileImageVW.sd_setImage(with: URL(string: "\(Image_URL)\(datadict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                    self.profileImageVW.layer.cornerRadius = self.profileImageVW.frame.width/2
                    self.profileImageVW.clipsToBounds = true
                    if (datadict["nickname"] as! String != "") {
                        self.nameLabel.text = "\(datadict["first_name"] as! String) \(datadict["last_name"] as! String) (@\(datadict["nickname"] as! String))"
                    }
                    else {
                        self.nameLabel.text = "\(datadict["first_name"] as! String) \(datadict["last_name"] as! String)"
                    }
                    self.followersCountLabel.text = "\(datadict["followers"] as! NSNumber)"
                    
                    if let _ = datadict["videosWatched"] as? NSNumber
                    {
                        self.totalVideoWatchedLbl.text = "\(datadict["videosWatched"] as! NSNumber)"
                    }
                    if let _ = datadict["totalReveal"] as? NSNumber
                    {
                        self.totalFreeRevealLbl.text = "\(datadict["totalReveal"] as! NSNumber)"
                    }
                    if let _ = datadict["totalRevealUsed"] as? NSNumber
                    {
                        self.freeRevealUsedLbl.text = "\(datadict["totalRevealUsed"] as! NSNumber)"
                    }
                    if let _ = datadict["videosWatched"] as? NSNumber, let _ = datadict["totalRevealUsed"] as? NSNumber
                    {
                        self.freeRevealAvailable.text = "\(Int.init(exactly: datadict["totalReveal"] as! NSNumber)! - Int.init(exactly: datadict["totalRevealUsed"] as! NSNumber)!)"
                    }
                    if let _ = datadict["nextReveal"] as? NSNumber
                    {
                        self.nextfreeReveal.text = "\(datadict["nextReveal"] as! NSNumber)/10"
                    }
                    else {
                        self.nextfreeReveal.text = "0/10"
                    }
                    customLoader.hideIndicator()
                }
                else {
                   // print("reward result = \(result)")
                    Free_Reveal = result["free_reveal"] as! Bool
                    let str1 = self.nextfreeReveal.text?.split(separator: "/")
                   // print("str1 = \(str1)")
                    if let _ = Int(str1![0])
                    {
                        var value = Int(str1![0])!
                       // print("value = \(value)")
                        value += 1
                        self.nextfreeReveal.text = "\(value)/10"
                    }
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    /// Reward AdVideo Work
    func loadRewardVideoAds()
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        
        rewardBasedVideo = GADRewardBasedVideoAd.sharedInstance()
        rewardBasedVideo?.delegate = self
        
        if rewardBasedVideo?.isReady == false
        {
            rewardBasedVideo?.load(GADRequest(),
                                   withAdUnitID: "ca-app-pub-7225491090572339/4225285890")
            // test - "ca-app-pub-3940256099942544/1712485313")
        }
        else {
            print("notttt readyyyyy")
            customLoader.hideIndicator()
        }
    }
    
    // MARK: GADRewardBasedVideoAdDelegate implementation
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didFailToLoadWithError error: Error) {
        //  adRequestInProgress = false
        //  print("Reward based video ad failed to load: \(error.localizedDescription)")
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        // print("Reward based video ad is received.")
        rewardBasedVideo?.present(fromRootViewController: self)
    }
    
    func rewardBasedVideoAdDidOpen(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
       // print("Opened reward based video ad.")
    }
    func rewardBasedVideoAdDidStartPlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
       // print("Reward based video ad started playing.")
        customLoader.hideIndicator()
    }
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
      //  print("Reward based video ad is closed.")
        
        if (Free_Reveal == true)
        {
            let errorvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFErrorPopupVC") as! FFErrorPopupVC
            errorvc.statusCheck = "reveal"
            errorvc.modalPresentationStyle = .overCurrentContext
            self.present(errorvc, animated: false, completion: nil)
        }
    }
    func rewardBasedVideoAdWillLeaveApplication(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
       // print("Reward based video ad will leave application.")
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
      //  print("Reward received with currency: \(reward.type), amount \(reward.amount).")
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(user_id)"
        self.callService(urlStr: Api.FF_RewardVideo_URL, params: params, check: "reward")
    }
}
