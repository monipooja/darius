//
//  FFMyFollowersVC.swift
//  Darious
//
//  Created by Apple on 07/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import GoogleMobileAds

class FFMyFollowersVC: UIViewController, GADUnifiedNativeAdLoaderDelegate, GADUnifiedNativeAdDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    @IBOutlet weak var searchVwHt : NSLayoutConstraint!
    @IBOutlet weak var srchIconWd : NSLayoutConstraint!
    @IBOutlet weak var searchView : UIView!
    
    @IBOutlet weak var myfollowersTableVw : UITableView!
    @IBOutlet weak var searchTF : UITextField!
    @IBOutlet weak var topTitleLbl : UILabel!
    @IBOutlet weak var nodataLbl : UILabel!
    @IBOutlet weak var toplabel : UILabel!
    
    var passStr : String = ""
    var pageno : Int = 0
    var selectedCell : FollowersTableCell!
    var followersArray : [NSDictionary] = []
    var selectDict : NSDictionary = [:]
    var selectIndex : Int = 0
    var layercount : Int = 0
    var setBtm : Bool = false
    var fromDelegate : Bool = false
    
    var adLoader: GADAdLoader!
    var adViewCell : FollowersAdTableCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)

        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
            imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setDataOnLaod()
        toplabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_friends")
        
        if (setBtm == false)
        {
            setBottomView()
        }
    }

    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
        
        socket.on("search_flyfriends_emit") { (dataArray, socketAck) in
            
            let jsonDict = dataArray[0] as? NSDictionary
           // print("json data = \(jsonDict!)")
            
            let datarr = jsonDict!["data"] as? [NSDictionary]
            self.followersArray = datarr!
            self.myfollowersTableVw.isHidden = false
            self.myfollowersTableVw.reloadData()
        }
    }
    override func viewDidLayoutSubviews()
    {
        if (layercount == 1)
        {
            StaticFunctions.addBorderOnLayer(bgColor: UIColor.init(hexString: ColorCode.FMenuGray), vwFrame: CGRect(x: 0, y: searchTF.frame.height-0.5, width: searchTF.frame.width, height: 0.6), layer: searchTF.layer)
        }
        layercount += 1
    }
    
    // View Did load data set
    func setDataOnLaod()
    {
        if (view.frame.width == 320) && (view.frame.height == 568)
        {
            searchVwHt.constant = 36
            searchView.layer.cornerRadius = 18
            srchIconWd.constant = 30
        }
        
        DispatchQueue.main.async {
            
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&page_no=\(self.pageno)&search=\(self.searchTF.text!)"
            
            if (self.passStr == "followers") {
                self.topTitleLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_followers")
                self.callService(urlStr: Api.FF_MyFollowers_URL, params: params, check: "get")
            }
            else {
                self.topTitleLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_people_i_follow")
                self.callService(urlStr: Api.FF_MyFollowing_URL, params: params, check: "get")
            }
        }
    }
    
    @IBAction func onSearch(_ sender: Any)
    {
//        view.endEditing(true)
//        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
//        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&page_no=\(pageno)&search=\(searchTF.text!)"
//        if (self.passStr == "followers") {
//            self.callService(urlStr: Api.FF_MyFollowers_URL, params: params, check: "get")
//        }
//        else {
//            self.callService(urlStr: Api.FF_MyFollowing_URL, params: params, check: "get")
//        }
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        if (fromDelegate == false)
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        setBtm = true
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
             //   print("my followers = \(result)")
                if (check == "get")
                {
                    if let dictArr = result["data"] as? [NSDictionary]
                    {
                        self.followersArray = dictArr
                        self.myfollowersTableVw.reloadData()
                        if (self.followersArray.count == 0)
                        {
                            self.myfollowersTableVw.isHidden = true
                            self.nodataLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                            self.nodataLbl.isHidden = false
                        }
                        else {
                            self.myfollowersTableVw.isHidden = false
                        }
                    }
                    else {
                        self.myfollowersTableVw.isHidden = true
                        self.nodataLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                        self.nodataLbl.isHidden = false
                    }
                    customLoader.hideIndicator()
                }
                else
                {
//                    customLoader.hideIndicator()
                    if (self.passStr == "following") {
                        self.viewDidLoad()
                    }
                    else {
                        let editdict = NSMutableDictionary.init(dictionary: self.selectDict)
                        
                        if (self.selectedCell.followButton.backgroundColor == UIColor.init(hexString: ColorCode.FFThemeColor))
                        {
                            var followcount = Int(self.selectedCell.followersCountLabel.text!)!
                            followcount += 1
                            editdict["followers"] = NSNumber.init(value: followcount)
                            self.selectedCell.followersCountLabel.text = "\(followcount)"
                            self.selectedCell.followButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_unfollow"), for: .normal)
                            var followValue = Int(truncating: self.selectDict["following"] as! NSNumber)
                            followValue += 1
                            editdict["following"] = NSNumber.init(value: followValue)
                            self.followersArray[self.selectIndex] = editdict
                            self.selectedCell.followButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray)
                            self.selectedCell.commentButton.isHidden = false
                            if (editdict["commented"] as! NSNumber == 0) {
                                self.selectedCell.rightSideVw.backgroundColor = UIColor.init(hexString: ColorCode.FFBluecomment)
                            }
                            else {
                                self.selectedCell.rightSideVw.backgroundColor = UIColor.init(hexString: ColorCode.FFPinkcomment)
                            }
                        }
                        else {
                            var followcount = Int(self.selectedCell.followersCountLabel.text!)!
                            followcount -= 1
                            editdict["followers"] = NSNumber.init(value: followcount)
                            self.selectedCell.followersCountLabel.text = "\(followcount)"
                            self.selectedCell.followButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_follow"), for: .normal)
                            var followValue = Int(truncating: self.selectDict["following"] as! NSNumber)
                            followValue -= 1
                            editdict["following"] = NSNumber.init(value: followValue)
                            self.followersArray[self.selectIndex] = editdict
                            self.selectedCell.followButton.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                            self.selectedCell.commentButton.isHidden = true
                            self.selectedCell.rightSideVw.backgroundColor = UIColor.init(hexString: ColorCode.FFGraycomment)
                        }
                    }
                }
            }
        }
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    // Ads Function call
    func loadAds()
    {
        let multipleAdsOptions = GADMultipleAdsAdLoaderOptions()
        multipleAdsOptions.numberOfAds = 5
        
        adLoader = GADAdLoader(adUnitID: AdUnitId, rootViewController: self,
                               adTypes: [GADAdLoaderAdType.unifiedNative],
                               options: [multipleAdsOptions])
        adLoader.delegate = self
        adLoader.load(GADRequest())
    }
    
    func adLoader(_ adLoader: GADAdLoader,
                  didReceive nativeAd: GADUnifiedNativeAd) {
        // A unified native ad has loaded, and can be displayed.
        
        adViewCell.nativeViewAd.nativeAd = nativeAd
        
        // Set ourselves as the native ad delegate to be notified of native ad events.
        nativeAd.delegate = self
        
        // Populate the native ad view with the native ad assets.
        // The headline and mediaContent are guaranteed to be present in every native ad.
        adViewCell.headlineLbl.text = nativeAd.headline
        adViewCell.descLabel.text = nativeAd.body
        adViewCell.advImageVW.image = nativeAd.icon?.image
        adViewCell.visitSiteBtn.setTitle(nativeAd.callToAction, for: .normal)
        adViewCell.visitSiteBtn.isUserInteractionEnabled = false
        adViewCell.nativeViewAd.callToActionView = adViewCell.visitSiteBtn
        
        nativeAd.register(adViewCell.visitSiteBtn, clickableAssetViews: [GADUnifiedNativeAssetIdentifier.callToActionAsset : adViewCell.nativeViewAd.callToActionView!], nonclickableAssetViews: [:])
    }
    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        // The adLoader has finished loading ads, and a new request can be sent.
    }
    public func adLoader(_ adLoader: GADAdLoader,
                         didFailToReceiveAdWithError error: GADRequestError)
    {
    }
    func register(_ adView: UIView, clickableAssetViews: [GADUnifiedNativeAssetIdentifier : UIView], nonclickableAssetViews: [GADUnifiedNativeAssetIdentifier : UIView])
    {
    }

}

extension FFMyFollowersVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str = "\(textField.text!)\(string)"
        if (string == "")
        {
            str.removeLast()
        }
        
        var typeStr = ""
        if (self.passStr == "followers") {
            typeStr = "my_followers"
        }
        else {
            typeStr = "my_following"
        }
        let data_json : NSDictionary = ["search" : "\(str)", "uid" : user_id, "type" : "\(typeStr)"]
        socket.emit("search_flyfriends_user", data_json)
        
        return true
    }
}

extension FFMyFollowersVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return followersArray.count + (followersArray.count/3)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            let cell = myfollowersTableVw.dequeueReusableCell(withIdentifier: "followersadsCell") as! FollowersAdTableCell
            
            cell.nativeViewAd.layer.cornerRadius = 44
            cell.nativeViewAd.layer.shadowColor = UIColor.lightGray.cgColor
            cell.nativeViewAd.layer.shadowOpacity = 0.6
            cell.nativeViewAd.layer.shadowRadius = 3
            cell.nativeViewAd.layer.shadowOffset = CGSize(width: 0, height: 0)
            
            return cell
        }
        else {
            let cell = myfollowersTableVw.dequeueReusableCell(withIdentifier: "followersCell") as! FollowersTableCell
            
            var dict : NSDictionary = [:]
            if (indexPath.section > 3) {
                dict = followersArray[indexPath.section-(indexPath.section/3)]
            }
            else {
                dict = followersArray[indexPath.section]
            }
            
            if (dict["nickname"] as! String != ""){
                cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String) (@\(dict["nickname"] as! String))"
            }
            else {
                cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
            }
            cell.profileImageVW.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
            cell.profileImageVW.layer.cornerRadius = (cell.profileImageVW.frame.width)/2
            cell.profileImageVW.layer.masksToBounds = true
            cell.followersCountLabel.text = "\(dict["followers"] as! NSNumber)"
            cell.rightSideVw.layer.cornerRadius = (cell.rightSideVw.frame.width)/2
            cell.rightSideVw.layer.masksToBounds = true
            
            if (self.passStr == "following")
            {
                cell.followButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray)
                cell.followButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_unfollow"), for: .normal)
                cell.commentButton.isHidden = false
                
                if (dict["commented"] as! NSNumber == 0)
                {
                    cell.rightSideVw.backgroundColor = UIColor.init(hexString: ColorCode.FFBluecomment)
                }
                else {
                    cell.rightSideVw.backgroundColor = UIColor.init(hexString: ColorCode.FFPinkcomment)
                }
            }
            else {
                if (Int(truncating: dict["following"] as! NSNumber) > 0)
                {
                    cell.followButton.backgroundColor = UIColor.init(hexString: ColorCode.FMenuGray)
                    cell.followButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_unfollow"), for: .normal)
                    cell.commentButton.isHidden = false
                    
                    if (dict["commented"] as! NSNumber == 0) {
                        cell.rightSideVw.backgroundColor = UIColor.init(hexString: ColorCode.FFBluecomment)
                    }
                    else {
                        cell.rightSideVw.backgroundColor = UIColor.init(hexString: ColorCode.FFPinkcomment)
                    }
                }
                else {
                    cell.commentButton.isHidden = true
                    cell.rightSideVw.backgroundColor = UIColor.init(hexString: ColorCode.FFGraycomment)
                    cell.followButton.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                    cell.followButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_follow"), for: .normal)
                }
            }
            
            cell.followersImgVW.tintColor = UIColor.init(hexString: ColorCode.FFIconsGray)
            cell.followButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 28)
            cell.followButton.addTarget(self, action: #selector(followUser(_:)), for: .touchUpInside)
            cell.commentButton.addTarget(self, action: #selector(writeComment(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == followersArray.count-1 && (followersArray.count > 9){
            // Last cell is visible
            pageno = pageno + 1
        //    customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&page_no=\(self.pageno)&search=\(self.searchTF.text!)"
            if (self.passStr == "followers") {
                self.callService(urlStr: Api.FF_MyFollowers_URL, params: params, check: "load")
            }
            else {
                self.callService(urlStr: Api.FF_MyFollowing_URL, params: params, check: "load")
            }
        }
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            adViewCell = (cell as! FollowersAdTableCell)
            loadAds()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dict : NSDictionary = [:]
        let profilevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFfriendProfileVC") as! FFfriendProfileVC
        if (indexPath.section > 3) {
            dict = followersArray[indexPath.section-(indexPath.section/3)]
        }
        else {
            dict = followersArray[indexPath.section]
        }
        profilevc.passid = dict["id"] as! NSNumber
        profilevc.passTag = 5
        present(profilevc, animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func followUser(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: myfollowersTableVw)
        let idxPath = myfollowersTableVw.indexPathForRow(at: point)
        selectedCell = myfollowersTableVw.cellForRow(at: idxPath!) as? FollowersTableCell
        var dict : NSDictionary = [:]
        if (idxPath!.section > 3) {
            dict = followersArray[idxPath!.section-(idxPath!.section/3)]
        }
        else {
            dict = followersArray[idxPath!.section]
        }
        selectDict = dict
        selectIndex = (idxPath?.section)!
        
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(dict["id"] as! NSNumber)"
        self.callService(urlStr: Api.FF_FollowUser_URL, params: params, check: "follow")
    }
    
    @objc func writeComment(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: myfollowersTableVw)
        let idxPath = myfollowersTableVw.indexPathForRow(at: point)
        selectedCell = myfollowersTableVw.cellForRow(at: idxPath!) as? FollowersTableCell
        
        var dict : NSDictionary = [:]
        if (idxPath!.section > 3) {
            dict = followersArray[idxPath!.section-(idxPath!.section/3)]
        }
        else {
            dict = followersArray[idxPath!.section]
        }
        
        let writevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFWriteCommentVC") as! FFWriteCommentVC
        writevc.idpass = dict["id"] as! NSNumber
        writevc.passTag = 5
        present(writevc, animated: false, completion: nil)
    }
}

class FollowersTableCell: UITableViewCell {
    
    @IBOutlet weak var profileImageVW : UIImageView!
    @IBOutlet weak var rightSideVw : UIView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var followersCountLabel : UILabel!
    @IBOutlet weak var followersImgVW : UIImageView!
    
    @IBOutlet weak var followButton : KGHighLightedButton!
    @IBOutlet weak var commentButton : UIButton!
}

class FollowersAdTableCell: UITableViewCell
{
    @IBOutlet weak var nativeViewAd : GADUnifiedNativeAdView!
    @IBOutlet weak var advImageVW : UIImageView!
    @IBOutlet weak var headlineLbl : UILabel!
    @IBOutlet weak var descLabel : UILabel!
    
    @IBOutlet weak var visitSiteBtn : KGHighLightedButton!
}
