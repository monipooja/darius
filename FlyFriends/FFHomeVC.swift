//
//  FFHomeVC.swift
//  Darious
//
//  Created by Apple on 06/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FFHomeVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var myopinionBtn : UIButton!
    @IBOutlet weak var wallopinionBtn : UIButton!
    @IBOutlet weak var toplabel : UILabel!
    @IBOutlet weak var myTextbtn : UIButton!
    @IBOutlet weak var wallTextbtn : UILabel!
    
    @IBOutlet weak var btnWidth : NSLayoutConstraint!
    @IBOutlet weak var btnHeight : NSLayoutConstraint!
    @IBOutlet weak var centerImgWd : NSLayoutConstraint!
    @IBOutlet weak var centerImgHt : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 0) {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                imgCollection[btn.tag].image = imgCollection[btn.tag].image?.withRenderingMode(.alwaysTemplate)
                imgCollection[btn.tag].tintColor = UIColor.white
            }
            else {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
                imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            toplabel.font = UIFont(name: "SegoeScript-Bold", size: 18)
            myTextbtn.titleLabel?.font = UIFont(name: "SegoeScript-Bold", size: 11)
            wallTextbtn.font = UIFont(name: "SegoeScript-Bold", size: 11)
            
            btnWidth.constant = 80
            btnHeight.constant = 80
            centerImgHt.constant = 40
            centerImgWd.constant = 40
        }
        myopinionBtn.layer.cornerRadius = btnWidth.constant/2
        wallopinionBtn.layer.cornerRadius = btnWidth.constant/2
        
        toplabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_opinions")
        myTextbtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_opinions"), for: .normal)
        let wallstr = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_wall"))\n(\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_people_opinion")))"
        wallTextbtn.text = wallstr
    
        setBottomView()
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func myOpinions(_ sender: Any)
    {
        let myopinionvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(myopinionvc, animated: false, completion: nil)
    }
    
    @IBAction func myWallOpinions(_ sender: Any)
    {
        let wallopinionvc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFWallOpinionVC") as! FFWallOpinionVC
        present(wallopinionvc, animated: false, completion: nil)
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFHomeVC") as! FFHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func ongoBackToDarius(_ sender: Any)
    {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }
}
