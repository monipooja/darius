//
//  MyWallModel.swift
//  Darious
//
//  Created by Apple on 03/09/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation



class MyWall {
    
    var name : String?
    var profilepic : String?
    var viewstatus : String?
    var status : String?
    
    init(nm : String, profpic : String, vwstatus : String, sts : String)
    {
        name = nm
        profilepic = profpic
        viewstatus = vwstatus
        status = sts
    }
}
