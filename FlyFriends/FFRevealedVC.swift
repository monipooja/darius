//
//  FFRevealedVC.swift
//  Darious
//
//  Created by Apple on 23/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import GoogleMobileAds

class FFRevealedVC: UIViewController, GADUnifiedNativeAdLoaderDelegate, GADUnifiedNativeAdDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var revealedTableVw : UITableView!
    @IBOutlet weak var alwaysLabel : UILabel!
    @IBOutlet weak var nodataLbl : UILabel!
    @IBOutlet weak var topLabel : UILabel!
    
    var opinionArray : [NSDictionary] = []
    var heightArray : [CGFloat] = []
    var linesArray : [Int] = []
    
    var adLoader: GADAdLoader!
    var adViewCell : AdsViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)

        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 4) {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                imgCollection[btn.tag].tintColor = UIColor.white
                imgCollection[btn.tag].image = imgCollection[btn.tag].image?.withRenderingMode(.alwaysTemplate)
            }
            else {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
                imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        alwaysLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_revealed")
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_profile")
        
        let headerNib = UINib.init(nibName: "AdsViewCell", bundle: Bundle.main)
        revealedTableVw.register(headerNib, forCellReuseIdentifier: "AdsViewCell")
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlStr: Api.FF_RevealedList_URL, params: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
              //  print("revealed list = \(result)")
                if (check == "get")
                {
                    if let dataarr = result["data"] as? [NSDictionary]
                    {
                        self.opinionArray = dataarr
                    }
                    if(self.opinionArray != []) {
                        self.revealedTableVw.reloadData()
                        self.revealedTableVw.isHidden = false
                        
                        for dict in self.opinionArray
                        {
                            self.heightArray.append(102)
                            self.linesArray.append(2)
                        }
                    }
                    else {
                        self.revealedTableVw.isHidden = true
                        self.nodataLbl.isHidden = false
                        self.nodataLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                    }
                    customLoader.hideIndicator()
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    // Ads Function call
    func loadAds()
    {
        let multipleAdsOptions = GADMultipleAdsAdLoaderOptions()
        multipleAdsOptions.numberOfAds = 5
        
        adLoader = GADAdLoader(adUnitID: AdUnitId, rootViewController: self,
                               adTypes: [GADAdLoaderAdType.unifiedNative],
                               options: [multipleAdsOptions])
        adLoader.delegate = self
        adLoader.load(GADRequest())
    }
    
    func adLoader(_ adLoader: GADAdLoader,
                  didReceive nativeAd: GADUnifiedNativeAd) {
        // A unified native ad has loaded, and can be displayed.
        
        adViewCell.nativeViewAd.nativeAd = nativeAd
        
        // Set ourselves as the native ad delegate to be notified of native ad events.
        nativeAd.delegate = self
        
        // Populate the native ad view with the native ad assets.
        // The headline and mediaContent are guaranteed to be present in every native ad.
        adViewCell.headlineLbl.text = nativeAd.headline
        adViewCell.descLabel.text = nativeAd.body
        adViewCell.advImageVW.image = nativeAd.icon?.image
        adViewCell.visitSiteBtn.setTitle(nativeAd.callToAction, for: .normal)
        adViewCell.visitSiteBtn.isUserInteractionEnabled = false
        adViewCell.nativeViewAd.callToActionView = adViewCell.visitSiteBtn
        
        nativeAd.register(adViewCell.visitSiteBtn, clickableAssetViews: [GADUnifiedNativeAssetIdentifier.callToActionAsset : adViewCell.nativeViewAd.callToActionView!], nonclickableAssetViews: [:])
    }
    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        // The adLoader has finished loading ads, and a new request can be sent.
    }
    public func adLoader(_ adLoader: GADAdLoader,
                         didFailToReceiveAdWithError error: GADRequestError)
    {
    }
    func register(_ adView: UIView, clickableAssetViews: [GADUnifiedNativeAssetIdentifier : UIView], nonclickableAssetViews: [GADUnifiedNativeAssetIdentifier : UIView])
    {
    }
}

// MARK : TableView Delegates
extension FFRevealedVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return opinionArray.count + (opinionArray.count/3)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            let cell = revealedTableVw.dequeueReusableCell(withIdentifier: "AdsViewCell") as! AdsViewCell
            
            cell.nativeViewAd.layer.cornerRadius = 44
            cell.nativeViewAd.layer.shadowColor = UIColor.lightGray.cgColor
            cell.nativeViewAd.layer.shadowOpacity = 0.6
            cell.nativeViewAd.layer.shadowRadius = 3
            cell.nativeViewAd.layer.shadowOffset = CGSize(width: 0, height: 0)
            
            return cell
        }
        else {
            let cell = revealedTableVw.dequeueReusableCell(withIdentifier: "revealcmntcell") as! RevealCommentTableCell
            
            var sectionValue : Int!
            if (indexPath.section > 3) {
                sectionValue = indexPath.section-(indexPath.section/3)
            }
            else {
                sectionValue = indexPath.section
            }
            
            let dict = opinionArray[sectionValue]
            
            if (dict["to_profile"] as! String != "") {
                cell.leftProfileImg.sd_setImage(with: URL(string: "\(Image_URL)\(dict["to_profile"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            }
            if (dict["from_profile"] as! String != "") {
                cell.rightProfileImg.sd_setImage(with: URL(string: "\(Image_URL)\(dict["from_profile"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            }
            cell.bgImgVw.layer.cornerRadius = cell.bgImgVw.frame.width/2
            cell.bgImgVw.layer.masksToBounds = true
            
            if (dict["to_nickname"] as! String != "") {
                cell.nameLabel.text = "\(dict["to_fullname"] as! String) (@\(dict["to_nickname"] as! String))"
            }else {
                cell.nameLabel.text = "\(dict["to_fullname"] as! String)"
            }
            
            if (dict["from_nickname"] as! String != "") {
                cell.opinionLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_opinion_by")) \(dict["from_fullname"] as! String) (@\(dict["from_nickname"] as! String)"
            }else {
                cell.opinionLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_opinion_by")) \(dict["from_fullname"] as! String)"
            }
            
            cell.descLabel.text =  dict["rating_desc"] as? String
            cell.descLabel.numberOfLines = linesArray[sectionValue]
        
            //Verified or not
            if (dict["face_status"] as! String == "yes")
            {
                cell.verifiedImg.isHidden = false
            }
            else {
                cell.verifiedImg.isHidden = true
            }
            // Height Set
            if(heightArray[sectionValue] > 102) {
                cell.openBtn.isHidden = true
            }
            else {
                cell.openBtn.isHidden = false
            }
            
            cell.openBtn.addTarget(self, action: #selector(openCell(_:)), for: .touchUpInside)
            cell.closeBtn.addTarget(self, action: #selector(closeCell(_:)), for: .touchUpInside)
            cell.leftprofBtn.addTarget(self, action: #selector(leftProfile(_:)), for: .touchUpInside)
            cell.rightProfBtn.addTarget(self, action: #selector(rightProfile(_:)), for: .touchUpInside)
            
            cell.bgView.layer.cornerRadius = 45
            cell.bgView.layer.shadowOffset = CGSize.init(width: 0, height: 0)
            cell.bgView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.bgView.layer.shadowOpacity = 0.8
            cell.bgView.layer.shadowRadius = 4
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let profilevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFfriendProfileVC") as! FFfriendProfileVC
//        profilevc.passid = dict["to_id"] as! NSNumber
//        profilevc.passTag = 4
//        present(profilevc, animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            adViewCell = (cell as! AdsViewCell)
            loadAds()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            return 98
        }
        else {
            if (indexPath.section > 3) {
                return heightArray[indexPath.section-(indexPath.section/3)]
            }
            else {
                return heightArray[indexPath.section]
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: revealedTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func openCell(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: revealedTableVw)
        let idxpath = revealedTableVw.indexPathForRow(at: point)
        
        var sectionValue : Int!
        if (idxpath!.section > 3) {
            sectionValue = idxpath!.section-(idxpath!.section/3)
        }
        else {
            sectionValue = idxpath!.section
        }
        
        let dict = opinionArray[sectionValue]
        let cell = revealedTableVw.cellForRow(at: idxpath!) as! RevealCommentTableCell
        let ht = ApiResponse.calculateSizeForString(dict["rating_desc"] as! String, sz: 10, maxWidth: cell.descLabel.frame.width, controller: self).height
        heightArray[sectionValue] = revealedTableVw.estimatedRowHeight + (ht-20)
        linesArray[sectionValue] = 0
        revealedTableVw.reloadRows(at: [idxpath!], with: .none)
    }
    
    @objc func closeCell(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: revealedTableVw)
        let idxpath = revealedTableVw.indexPathForRow(at: point)
        
        var sectionValue : Int!
        if (idxpath!.section > 3) {
            sectionValue = idxpath!.section-(idxpath!.section/3)
        }
        else {
            sectionValue = idxpath!.section
        }
        
        heightArray[sectionValue] = 102
        linesArray[sectionValue] = 2
        revealedTableVw.reloadRows(at: [idxpath!], with: .none)
    }
    
    @objc func leftProfile(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: revealedTableVw)
        let idxpath = revealedTableVw.indexPathForRow(at: point)
        var dict : NSDictionary = [:]
        if (idxpath!.section > 3) {
            dict = opinionArray[idxpath!.section-(idxpath!.section/3)]
        }
        else {
            dict = opinionArray[idxpath!.section]
        }
        
        if (dict["to_id"] as! NSNumber != user_id)
        {
            let profilevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFfriendProfileVC") as! FFfriendProfileVC
            profilevc.passid = dict["to_id"] as! NSNumber
            present(profilevc, animated: false, completion: nil)
        }
        else {
            let profilevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyProfileVC") as! FFMyProfileVC
            present(profilevc, animated: false, completion: nil)
        }
    }
    
    @objc func rightProfile(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: revealedTableVw)
        let idxpath = revealedTableVw.indexPathForRow(at: point)
        var dict : NSDictionary = [:]
        if (idxpath!.section > 3) {
            dict = opinionArray[idxpath!.section-(idxpath!.section/3)]
        }
        else {
            dict = opinionArray[idxpath!.section]
        }
        
        if (dict["from_id"] as! NSNumber != user_id)
        {
            let profilevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFfriendProfileVC") as! FFfriendProfileVC
            profilevc.passid = dict["from_id"] as! NSNumber
            present(profilevc, animated: false, completion: nil)
        }
        else {
            let profilevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyProfileVC") as! FFMyProfileVC
            present(profilevc, animated: false, completion: nil)
        }
    }
}

class RevealCommentTableCell : UITableViewCell
{
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var firstView : UIView!
    @IBOutlet weak var bottomView : UIView!
   
    @IBOutlet weak var leftProfileImg : UIImageView!
    @IBOutlet weak var rightProfileImg : UIImageView!
    @IBOutlet weak var bgImgVw : UIView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var opinionLabel : UILabel!
    @IBOutlet weak var descLabel : UILabel!
   
    @IBOutlet weak var reamMoreBtn : UIButton!
    @IBOutlet weak var openBtn : UIButton!
    @IBOutlet weak var closeBtn : UIButton!
    @IBOutlet weak var verifiedImg : UIButton!
    @IBOutlet weak var leftprofBtn : UIButton!
    @IBOutlet weak var rightProfBtn : UIButton!
    
    @IBOutlet weak var descHt : NSLayoutConstraint!
}
