//
//  FFBlockedListVC.swift
//  Darious
//
//  Created by Apple on 23/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SwiftyStoreKit

class FFBlockedListVC: UIViewController, GADUnifiedNativeAdLoaderDelegate, GADUnifiedNativeAdDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var blockedLbl : UILabel!
    @IBOutlet weak var nodataLbl : UILabel!
    @IBOutlet weak var BlockedTblVw: UITableView!
    
    var opinionArray : [NSDictionary] = []
    
    var transactionId : String = ""
    var total : String = ""
    var currencyType : String = ""
    var usr_id : NSNumber = 0
    var setBtm : Bool = false
    var selectedCell : BlockedCommentTableCell!
    
    var adLoader: GADAdLoader!
    var adViewCell : AdsViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 4) {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                imgCollection[btn.tag].image = imgCollection[btn.tag].image?.withRenderingMode(.alwaysTemplate)
                imgCollection[btn.tag].tintColor = UIColor.white
            }
            else {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
                imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        if (setBtm == false)
        {
            setBottomView()
        }
        
        blockedLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_blocked")
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_profile")
        
        let headerNib = UINib.init(nibName: "AdsViewCell", bundle: Bundle.main)
        BlockedTblVw.register(headerNib, forCellReuseIdentifier: "AdsViewCell")
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlStr: Api.FF_BlockedList_URL, params: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
//        NotificationCenter.default.addObserver(self, selector: #selector(openMollie(notification:)), name:NSNotification.Name(rawValue: "isDismissViewController"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(onMolliesuccess(notification:)), name:NSNotification.Name(rawValue: "dismissAfterSuccess"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
              //  print("blocked list = \(result)")
                if (check == "get")
                {
                    if let dataarr = result["data"] as? [NSDictionary]
                    {
                        self.opinionArray = dataarr
                    }
                    if(self.opinionArray != []) {
                        self.BlockedTblVw.reloadData()
                        self.BlockedTblVw.isHidden = false
                    }
                    else {
                        self.BlockedTblVw.isHidden = true
                        self.nodataLbl.isHidden = false
                        self.nodataLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                    }
                    customLoader.hideIndicator()
                }
                else {
                    let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msg, actions: [OkText], controller: self, completion: { (str) in
                        
                        self.viewDidLoad()
                        self.transactionId = ""
                    })
                    customLoader.hideIndicator()
                    //self.viewDidLoad()
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        setBtm = true
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        for btn in btnCollection
        {
            if (btn.tag == sender.tag) {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            }
            else {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
            }
        }
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    // Ads Function call
    func loadAds()
    {
        let multipleAdsOptions = GADMultipleAdsAdLoaderOptions()
        multipleAdsOptions.numberOfAds = 5
        
        adLoader = GADAdLoader(adUnitID: AdUnitId, rootViewController: self,
                               adTypes: [GADAdLoaderAdType.unifiedNative],
                               options: [multipleAdsOptions])
        adLoader.delegate = self
        adLoader.load(GADRequest())
    }
    
    func adLoader(_ adLoader: GADAdLoader,
                  didReceive nativeAd: GADUnifiedNativeAd) {
        // A unified native ad has loaded, and can be displayed.
        
        adViewCell.nativeViewAd.nativeAd = nativeAd
        
        // Set ourselves as the native ad delegate to be notified of native ad events.
        nativeAd.delegate = self
        
        // Populate the native ad view with the native ad assets.
        // The headline and mediaContent are guaranteed to be present in every native ad.
        adViewCell.headlineLbl.text = nativeAd.headline
        adViewCell.descLabel.text = nativeAd.body
        adViewCell.advImageVW.image = nativeAd.icon?.image
        adViewCell.visitSiteBtn.setTitle(nativeAd.callToAction, for: .normal)
        adViewCell.visitSiteBtn.isUserInteractionEnabled = false
        adViewCell.nativeViewAd.callToActionView = adViewCell.visitSiteBtn
        
        nativeAd.register(adViewCell.visitSiteBtn, clickableAssetViews: [GADUnifiedNativeAssetIdentifier.callToActionAsset : adViewCell.nativeViewAd.callToActionView!], nonclickableAssetViews: [:])
    }
    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        // The adLoader has finished loading ads, and a new request can be sent.
    }
    public func adLoader(_ adLoader: GADAdLoader,
                         didFailToReceiveAdWithError error: GADRequestError)
    {
    }
    func register(_ adView: UIView, clickableAssetViews: [GADUnifiedNativeAssetIdentifier : UIView], nonclickableAssetViews: [GADUnifiedNativeAssetIdentifier : UIView])
    {
    }
    
    func getProduct(_ purchase: ServicePurchase)
    {
        SwiftyStoreKit.retrieveProductsInfo([appBundleId + "." + purchase.rawValue]) { result in
            if let product = result.retrievedProducts.first {
                self.total = "\(product.price)"
                self.currencyType = product.priceLocale.currencySymbol!
            }
        }
    }
    
    func purchase(_ purchase: ServicePurchase, atomically: Bool) {
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.purchaseProduct(appBundleId + "." + purchase.rawValue, atomically: atomically) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            if case .success(let purchaseObj) = result {
                self.verifyPurchase(purchase)
                
                let downloads = purchaseObj.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                
                // Deliver content from server, then:
                if purchaseObj.needsFinishTransaction {
                    customLoader.hideIndicator()
                    SwiftyStoreKit.finishTransaction(purchaseObj.transaction)
                }
            }
            else {
                customLoader.hideIndicator()
            }
        }
    }
    
    func verifyPurchase(_ purchase: ServicePurchase) {
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                let productId = appBundleId + "." + purchase.rawValue
                let purchaseResult = SwiftyStoreKit.verifyPurchase(productId: productId, inReceipt: receipt)
                switch purchaseResult {
                case .purchased:
                   // self.showAlert(self.alertWithTitle("Payment done", message: "Thank you for purchasing."))
                    StaticFunctions.showAlert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_payment_done"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "thanks_for_purchasing"), actions: [OkText], controller: self, completion: { (str) in
                        self.addPurchaseCreditToAccount(RegisteredPurchase: appBundleId + "." + purchase.rawValue)
                    })
                    break
                    
                case .notPurchased:
                    self.showAlert(self.alertForVerifyReceipt(result))
                    break
                }
            case .error:
                self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: AdSharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }
    
    func addPurchaseCreditToAccount(RegisteredPurchase: String) {
        
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.usr_id)&trans_id=\(self.transactionId)&amount=\(total)&type=inapp&curr_type=\(currencyType)"
        self.callService(urlStr: Api.FF_UnblockUser_URL, params: params, check: "unblock")
    }
}

// MARK : TableView Delegates
extension FFBlockedListVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return opinionArray.count + (opinionArray.count/3)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            let cell = BlockedTblVw.dequeueReusableCell(withIdentifier: "AdsViewCell") as! AdsViewCell
            
            cell.nativeViewAd.layer.cornerRadius = 44
            cell.nativeViewAd.layer.shadowColor = UIColor.lightGray.cgColor
            cell.nativeViewAd.layer.shadowOpacity = 0.6
            cell.nativeViewAd.layer.shadowRadius = 3
            cell.nativeViewAd.layer.shadowOffset = CGSize(width: 0, height: 0)
            
            return cell
        }
        else {
            let cell = BlockedTblVw.dequeueReusableCell(withIdentifier: "blockedcmntcell") as! BlockedCommentTableCell
            
            var sectionValue : Int!
            if (indexPath.section > 3) {
                sectionValue = indexPath.section-(indexPath.section/3)
            }
            else {
                sectionValue = indexPath.section
            }
            
            let dict = opinionArray[sectionValue]
            
            if (dict["profile_pic"] as! String != "") {
                cell.profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                cell.profileImgVw.isHidden = false
                cell.anonymsImgVw.isHidden = true
            }
            else {
                cell.profileImgVw.isHidden = true
                cell.anonymsImgVw.isHidden = false
            }
            cell.bgImgVw.layer.cornerRadius = cell.bgImgVw.frame.width/2
            cell.bgImgVw.layer.masksToBounds = true
           
            if (dict["nickname"] as! String != "") {
                cell.nameLabel.text = "\(dict["fullname"] as! String) (@\(dict["nickname"] as! String))"
            }
            else {
                cell.nameLabel.text = "\(dict["fullname"] as! String)"
            }
            
            //Verified or not
            if (dict["face_status"] as! String == "yes")
            {
                cell.verifiedImg.isHidden = false
            }
            else {
                cell.verifiedImg.isHidden = true
            }
            
            cell.unblockBtn.addTarget(self, action: #selector(leftBtnAction(_:)), for: .touchUpInside)
            cell.unblockBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_unblock"), for: .normal)
            
            cell.bgView.layer.cornerRadius = 45
            cell.bgView.layer.shadowOffset = CGSize.init(width: 0, height: 0)
            cell.bgView.layer.shadowColor = UIColor.lightGray.cgColor
            cell.bgView.layer.shadowOpacity = 0.8
            cell.bgView.layer.shadowRadius = 4
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        StaticFunctions.showAlert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_not_able_to_show_profile"), actions: [OkText], controller: self) { (str) in
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            adViewCell = (cell as! AdsViewCell)
            loadAds()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section % 3 == 0) && (indexPath.section > 2)
        {
            return 98
        }
        else {
            return 105
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: BlockedTblVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func leftBtnAction(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: BlockedTblVw)
        let idxpath = BlockedTblVw.indexPathForRow(at: point)
        var dict : NSDictionary = [:]
        if (idxpath!.section > 3) {
            dict = opinionArray[idxpath!.section-(idxpath!.section/3)]
        }
        else {
            dict = opinionArray[idxpath!.section]
        }
        let cell = BlockedTblVw.cellForRow(at: idxpath!) as! BlockedCommentTableCell
        selectedCell = cell
    
        usr_id = dict["id"] as! NSNumber
        
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.usr_id)&trans_id=\(self.transactionId)&amount=0&type=inapp&curr_type="
        self.callService(urlStr: Api.FF_UnblockUser_URL, params: params, check: "unblock")
    }
}

class BlockedCommentTableCell : UITableViewCell
{
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var firstView : UIView!
    @IBOutlet weak var verifiedImg : UIButton!
    
    @IBOutlet weak var profileImgVw : UIImageView!
    @IBOutlet weak var anonymsImgVw : UIImageView!
    @IBOutlet weak var bgImgVw : UIView!
    @IBOutlet weak var nameLabel : UILabel!
    
    @IBOutlet weak var unblockBtn : KGHighLightedButton!
}

// MARK: User facing alerts
extension FFBlockedListVC {
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        customLoader.hideIndicator()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func showAlert(_ alert: UIAlertController) {
        customLoader.hideIndicator()
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {
        
        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
            return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        } else if let _ = result.invalidProductIDs.first {
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: "")
        } else {
            let errorString = result.error?.localizedDescription ?? "\(ApiResponse.getLanguageFromUserDefaults(inputString: "unknown_error"))"
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: errorString)
        }
    }
    
    func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
        
        switch result {
        case .success( _):
            //print("Verify receipt Success: \(receipt)")
            return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "receipt_verified"))
        case .error(let error):
            // print("Verify receipt Failed: \(error)")
            switch error {
            case .noReceiptData:
                return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "no_receipt"))
            case .networkError(let error):
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verifying_error")): \(error)")
            default:
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verification_failed")): \(error)")
            }
        }
    }
}
