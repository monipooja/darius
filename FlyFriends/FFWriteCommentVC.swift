//
//  FFWriteCommentVC.swift
//  Darious
//
//  Created by Apple on 09/08/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SwiftyStoreKit

class FFWriteCommentVC: UIViewController, GADInterstitialDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    @IBOutlet weak var anotherHt : NSLayoutConstraint!
    
    @IBOutlet weak var commentTableVw : UITableView!
    @IBOutlet weak var profileImageVW : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var opinionLabel : UILabel!
    @IBOutlet weak var writeOpnLbl : UILabel!
    @IBOutlet weak var saveButton : KGHighLightedButton!
    
    var idpass : NSNumber = 0
    var stararr : [Int] = [0,0,0,0,0,0,0,0,0,0]
    
    var passTag : Int = 3
    var transactionId : String = ""
    var total : String = ""
    var currencyType : String = ""
    var comntCellHt : CGFloat = 280
    var selectStr : String = ""
    var cmntType : String = "active"
    var typeValue : String = " "
    var commentedValue : NSNumber = 0
    var fromDelegate : Bool = false
    
    var selectedCell : WriteCommentTableCell!
    var commentCell : WriteCommentTableCell!
    
    var interstitial: GADInterstitial!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        topHeaderImgVw.tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for btn in btnCollection
        {
            if (btn.tag == 3) {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                imgCollection[btn.tag].tintColor = UIColor.white
                imgCollection[btn.tag].image = imgCollection[btn.tag].image?.withRenderingMode(.alwaysTemplate)
            }
            else {
                btn.backgroundColor = UIColor.init(hexString: ColorCode.FFLightColor)
                imgCollection[btn.tag].tintColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            }
            btn.addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        writeOpnLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_write_opinion")
        opinionLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "another_comment_mollie")
        saveButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "save"), for: .normal)
        
        // Interstitial Ads
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-7225491090572339/7111782614")
        // test - "ca-app-pub-3940256099942544/4411468910")
        interstitial.delegate = self
        let request = GADRequest()
        interstitial.load(request)
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.idpass)"
            self.callService(urlStr: Api.FF_FriendProfile_URL, params: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.commentTableVw.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
//        NotificationCenter.default.addObserver(self, selector: #selector(openMollie(notification:)), name:NSNotification.Name(rawValue: "isDismissViewController"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(onMolliesuccess(notification:)), name:NSNotification.Name(rawValue: "dismissAfterSuccess"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    func setDataOnLoad(passdict : NSDictionary)
    {
        if (passdict["nickname"] as! String != ""){
            nameLabel.text = "\(passdict["first_name"] as! String) \(passdict["last_name"] as! String) (@\(passdict["nickname"] as! String))"
        }
        else {
            nameLabel.text = "\(passdict["first_name"] as! String) \(passdict["last_name"] as! String)"
        }
        profileImageVW.sd_setImage(with: URL(string: "\(Image_URL)\(passdict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        profileImageVW.layer.cornerRadius = (profileImageVW.frame.width)/2
        profileImageVW.layer.masksToBounds = true
        
        saveButton.isHidden = false
        
//        if (passdict["commented"] as! NSNumber == 0)
//        {
//            anotherHt.constant = 0
//            saveButton.isHidden = false
//        }
//        else {
//            saveButton.isHidden = true
//        }
    }
    
    @IBAction func anotherOpinion(_ sender: Any)
    {
//        selectStr = "another_comment"
//
//        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
//        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&amount=0&msg=pay_for_another_comment&curr_type=\(currencyType)"
//        self.callService(urlStr: Api.FF_InsertWallet_URL, params: params, check: "another")
    }
    
    @IBAction func saveWritecomment(_ sender: Any)
    {
        if (commentCell?.comentTV.text == "")
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_here"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let commentStr = (commentCell?.comentTV.text)!.replacingOccurrences(of: "'", with: "\\'").trimmingCharacters(in: .whitespacesAndNewlines)
            if (commentedValue == 0)
            {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(idpass)&rat_1=\(stararr[0])&rat_2=\(stararr[1])&rat_3=\(stararr[2])&rat_4=\(stararr[3])&rat_5=\(stararr[4])&rat_6=\(stararr[5])&rat_7=\(stararr[6])&rat_8=\(stararr[7])&rat_9=\(stararr[8])&rat_10=\(stararr[9])&rat_comment=\(commentStr)&payId=&type=\(typeValue)"
                self.callService(urlStr: Api.FF_WriteComment_URL, params: params, check: "write")
            }
            else {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(idpass)&rat_1=\(stararr[0])&rat_2=\(stararr[1])&rat_3=\(stararr[2])&rat_4=\(stararr[3])&rat_5=\(stararr[4])&rat_6=\(stararr[5])&rat_7=\(stararr[6])&rat_8=\(stararr[7])&rat_9=\(stararr[8])&rat_10=\(stararr[9])&rat_comment=\(commentStr)&type=\(cmntType)&payId="
                self.callService(urlStr: Api.FF_AnotherCmnt_URL, params: params, check: "write")
            }
            self.transactionId = ""
        }
    }
    
    @IBAction func onFMenuHome(_ sender: Any)
    {
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func ongoBack(_ sender: Any)
    {
        if (fromDelegate == false)
        {
            if (transactionId == "")
            {
                self.dismiss(animated: false, completion: nil)
            }
            else {
                StaticFunctions.showAlert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_submit_comment"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_exit_without_comment"), actions: [YesText,NoText], controller: self) { (str) in
                    
                    if (str == YesText) {
                        self.dismiss(animated: false, completion: nil)
                    }
                    else {
                    }
                }
            }
        }
        else {
            let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
            present(homevc, animated: false, completion: nil)
        }
    }
   
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
              //  print("write profile = \(result)")
                if (check == "get")
                {
                    let dict = result["data"] as! NSDictionary
                    self.commentedValue = dict["commented"] as! NSNumber
                    self.setDataOnLoad(passdict: dict)
                    customLoader.hideIndicator()
                    if (Int.init(exactly: dict["me_block"] as! NSNumber)! > 0)
                    {
                        StaticFunctions.showAlert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_not_able_to_send_comment"), actions: [OkText], controller: self, completion: { (str) in
                            
                            self.dismiss(animated: false, completion: nil)
                        })
                    }
                }
                else if (check == "write")
                {
                    customLoader.hideIndicator()
                    var msg = result["message"] as! String
                    msg = ApiResponse.getLanguageFromUserDefaults(inputString: msg)
                    StaticFunctions.showAlert(title: "", message: msg, actions: [OkText], controller: self, completion: { (str) in
                        
                        userDefault.set(false, forKey: "another_inapp")
                        userDefault.set(false, forKey: "anonymous_inapp")
                        userDefault.synchronize()
                        self.showInterstitialAd()
                    })
                }
                else if (check == "another") {
                    customLoader.hideIndicator()
                    self.anotherHt.constant = 0
                    self.saveButton.isHidden = false
                }
                else if (check == "hidden") {
                    customLoader.hideIndicator()
                    self.cmntType = "hidden"
                    self.typeValue = "inapp"
                    self.selectedCell.btmVw.isHidden = true
                    self.selectedCell.btmVwHt.constant = 0
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "ff")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flyfriendstopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyfriendsBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    // Interstitial ad show
    func showInterstitialAd()
    {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
    }
    
    // GADInterstitial Delegate
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
       // print("will dismisss")
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
      //  print("did dismiss")
        let homevc = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
        self.present(homevc, animated: false, completion: nil)
    }
    
    func getProduct(_ purchase: ServicePurchase)
    {
        SwiftyStoreKit.retrieveProductsInfo([appBundleId + "." + purchase.rawValue]) { result in
            if let product = result.retrievedProducts.first {
                self.total = "\(product.price)"
                self.currencyType = product.priceLocale.currencySymbol!
            }
        }
    }
    
    func purchase(_ purchase: ServicePurchase, atomically: Bool)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
        NetworkActivityIndicatorManager.networkOperationStarted()
        SwiftyStoreKit.purchaseProduct(appBundleId + "." + purchase.rawValue, atomically: atomically) { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            if case .success(let purchaseObj) = result {
                self.verifyPurchase(purchase)
                
                let downloads = purchaseObj.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                
                // Deliver content from server, then:
                if purchaseObj.needsFinishTransaction {
                    customLoader.hideIndicator()
                    SwiftyStoreKit.finishTransaction(purchaseObj.transaction)
                }
            }
            else {
                customLoader.hideIndicator()
            }
        }
    }
    
    func verifyPurchase(_ purchase: ServicePurchase) {
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                let productId = appBundleId + "." + purchase.rawValue
                let purchaseResult = SwiftyStoreKit.verifyPurchase(productId: productId, inReceipt: receipt)
                switch purchaseResult {
                case .purchased:
                    StaticFunctions.showAlert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "ff_payment_done"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "thanks_for_purchasing"), actions: [OkText], controller: self, completion: { (str) in
                        self.addPurchaseCreditToAccount(RegisteredPurchase: appBundleId + "." + purchase.rawValue)
                    })
                    break
                    
                case .notPurchased:
                    self.showAlert(self.alertForVerifyReceipt(result))
                    break
                }
            case .error:
                self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: AdSharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }
    
    func addPurchaseCreditToAccount(RegisteredPurchase: String) {
        
        if (self.selectStr == "hidden") {
            userDefault.set(true, forKey: "anonymous_inapp")
            userDefault.synchronize()
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&amount=\(total)&msg=pay_for_hidden&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_InsertWallet_URL, params: params, check: "hidden")
        }
        else {
            
            userDefault.set(true, forKey: "another_inapp")
            userDefault.synchronize()
            customLoader.showActivityIndicator(showColor: ColorCode.FFThemeColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&amount=\(total)&msg=pay_for_another_comment&curr_type=\(currencyType)"
            self.callService(urlStr: Api.FF_InsertWallet_URL, params: params, check: "another")
        }
    }
    
}

extension FFWriteCommentVC : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        comntCellHt = 320
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        comntCellHt = 280
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
     //   print("count == \(textView.text.count)-------\(text.count)---\(text)")
        
        if (textView.text.count+1 <= 500)
        {
            if (text == "") {
                commentCell.textCountLbl.text = "\(textView.text.count-1)/500"
            }
            else {
                commentCell.textCountLbl.text = "\(textView.text.count+1)/500"
            }
            return true
        }
        else {
            if (text == "")
            {
                commentCell.textCountLbl.textColor = UIColor.init(hexString: ColorCode.FFIconsGray)
                commentCell.textCountLbl.text = "\(textView.text.count-1)/500"
                return true
            }
            else {
                commentCell.textCountLbl.textColor = ColorCode.FFRemoveBG
                if (textView.text.count > 500)
                {
                    let range = textView.text.index(textView.text.endIndex, offsetBy: -(textView.text.count-500))..<textView.text.endIndex
                    textView.text.removeSubrange(range)
                    commentCell.textCountLbl.text = "500/500"
                    return true
                }
                else {
                    commentCell.textCountLbl.text = "\(textView.text.count)/500"
                    return false
                }
            }
        }
    }
}

extension FFWriteCommentVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = commentTableVw.dequeueReusableCell(withIdentifier: "writecommentcell") as! WriteCommentTableCell
        
        cell.commentLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_comment")
        cell.anonymousLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_always_anonymous")
        
        cell.priceLabel.text = Anonymous_price
        cell.anonymousBtn.addTarget(self, action: #selector(alwaysAnonymous(_:)), for: .touchUpInside)
        
        commentCell = cell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return comntCellHt
    }
    
    @objc func alwaysAnonymous(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: commentTableVw)
        let idxpath = commentTableVw.indexPathForRow(at: point)
        let cell = commentTableVw.cellForRow(at: idxpath!) as! WriteCommentTableCell
        selectedCell = cell
        
        selectStr = "hidden"
       
        let anonymous_inapp = userDefault.value(forKey: "anonymous_inapp") as? Bool
        if (anonymous_inapp == nil) || (anonymous_inapp == false) {
            getProduct(.anonymous)
            purchase(.anonymous, atomically: true)
        }
        else {
            StaticFunctions.showAlert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "hidden_paid_msg"), actions: ["OK"], controller: self) { (str) in
                
                self.getProduct(.anonymous)
                self.cmntType = "hidden"
                self.typeValue = "inapp"
                self.selectedCell.btmVw.isHidden = true
                self.selectedCell.btmVwHt.constant = 0
            }
        }
    }
}

class WriteCommentTableCell: UITableViewCell {
    
    @IBOutlet weak var commentLabel : UILabel!
    @IBOutlet weak var textCountLbl : UILabel!
    @IBOutlet weak var comentTV : customTextView!
    
    @IBOutlet weak var anonymousLabel : UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var btmVw : CustomUIView!
    
    @IBOutlet weak var anonymousBtn : UIButton!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
}

// MARK: User facing alerts
extension FFWriteCommentVC {
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        customLoader.hideIndicator()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func showAlert(_ alert: UIAlertController) {
        customLoader.hideIndicator()
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {
        
        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
            return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        } else if let _ = result.invalidProductIDs.first {
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: "")
        } else {
            let errorString = result.error?.localizedDescription ?? "\(ApiResponse.getLanguageFromUserDefaults(inputString: "unknown_error"))"
            return alertWithTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no_product_info"), message: errorString)
        }
    }
    
    func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
        
        switch result {
        case .success( _):
            //print("Verify receipt Success: \(receipt)")
            return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "receipt_verified"))
        case .error(let error):
            // print("Verify receipt Failed: \(error)")
            switch error {
            case .noReceiptData:
                return alertWithTitle("", message: ApiResponse.getLanguageFromUserDefaults(inputString: "no_receipt"))
            case .networkError(let error):
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verifying_error")): \(error)")
            default:
                return alertWithTitle("", message: "\(ApiResponse.getLanguageFromUserDefaults(inputString: "verification_failed")): \(error)")
            }
        }
    }
}
