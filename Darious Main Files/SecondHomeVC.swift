//
//  SecondHomeVC.swift
//  Darious
//
//  Created by Apple on 22/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import WebKit

class SecondHomeVC: UIViewController, WKNavigationDelegate {

    static let identifier = String("SecondHomeVC")
    
    @IBOutlet weak var appListTableVw: UITableView!
    @IBOutlet weak var sideVw: CustomUIView!
   
    var policyWebview : WKWebView!
    var bgview : UIView!
    var titleLabel : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.setWebView()
        }
    }
    
    // Mark : Set WebView for Privacy & Policy
    func setWebView()
    {
        if openWebview
        {
            bgview = UIView.init()
            bgview.frame = view.frame
            bgview.backgroundColor = UIColor.black
            bgview.alpha = 0.5
            view.addSubview(bgview)
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
            gesture.numberOfTapsRequired = 1
            bgview.addGestureRecognizer(gesture)
            
            titleLabel = UILabel.init(frame: CGRect(x: 20, y: 20, width: view.frame.width-40, height: 30))
            titleLabel.backgroundColor = UIColor.white
            titleLabel.font = UIFont(name: "Proximanova-Semibold", size: 18)
            view.addSubview(titleLabel)
            
            policyWebview = WKWebView.init(frame: CGRect(x: 20, y: 50, width: view.frame.width-40, height: view.frame.height-50))
            
            if (policyOrTerms == "terms")
            {
                let url = URL(string: "http://flydarius.com/term-and-condition")!
                policyWebview.load(URLRequest(url: url))
                titleLabel.text = "     \(ApiResponse.getLanguageFromUserDefaults(inputString: "fly_claim_footer_terms_and_conditions"))"
            }
            else {
                let url = URL(string: "http://flydarius.com/privacy-policy")!
                policyWebview.load(URLRequest(url: url))
                titleLabel.text = "     \(ApiResponse.getLanguageFromUserDefaults(inputString: "privacy_policy"))"
            }
            policyWebview.navigationDelegate = self
            view.addSubview(policyWebview)
            policyWebview.allowsBackForwardNavigationGestures = true
            
            customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: policyWebview)
            
            openWebview = false
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // customLoader.hideIndicator()
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        customLoader.hideIndicator()
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer)
    {
        openWebview = false
        bgview.isHidden = true
        policyWebview.isHidden = true
        titleLabel.removeFromSuperview()
        bgview.removeFromSuperview()
        policyWebview.removeFromSuperview()
    }
    
    @IBAction func closeAppList(_ sender: Any)
    {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
    }

    @IBAction func goToWallet(_ sender: Any)
    {
        let mainController = storyboard!.instantiateViewController(withIdentifier: SecondHomeVC.identifier)
        let rightController = storyboard!.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController

        let slideMenuController = SlideMenuController(mainViewController: mainController, rightMenuViewController: rightController)
        present(slideMenuController, animated: false, completion: nil)
        
        slideMenuController.openRight()
    }
    
    @IBAction func goToNotification(_ sender: Any)
    {
        let mainController = storyboard!.instantiateViewController(withIdentifier: SecondHomeVC.identifier)
        let rightController = storyboard!.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        
        let slideMenuController = SlideMenuController(mainViewController: mainController, rightMenuViewController: rightController)
        present(slideMenuController, animated: false, completion: nil)
        
        slideMenuController.openRight()
    }
    
    @IBAction func goToProfile(_ sender: Any)
    {
        let mainController = storyboard!.instantiateViewController(withIdentifier: SecondHomeVC.identifier)
        let rightController = storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        
        let slideMenuController = SlideMenuController(mainViewController: mainController, rightMenuViewController: rightController)
        present(slideMenuController, animated: false, completion: nil)
        
        slideMenuController.openRight()
    }
    
    @IBAction func openRightSideBar(_ sender: Any)
    {
        let mainController = storyboard!.instantiateViewController(withIdentifier: SecondHomeVC.identifier)
        let rightController = storyboard!.instantiateViewController(withIdentifier: SideTableViewController.identifier) as! SideTableViewController
        
        let slideMenuController = SlideMenuController(mainViewController: mainController, rightMenuViewController: rightController)
        present(slideMenuController, animated: false, completion: nil)
        
        slideMenuController.openRight()
    }
    
    func goToApps(str : String)
    {
        switch str {
        case "Fly Claim":
            let flyclaim = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCSplashVC") as! FCSplashVC
            present(flyclaim, animated: true, completion: nil)
            break
        case "Fly Money":
            let flymoney = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSplashVC") as! FMSplashVC
            present(flymoney, animated: true, completion: nil)
            break
        case "Fly Angel":
            let flyangel = StoryboardType.fa_storyboard.instantiateViewController(withIdentifier: "FASplashVC") as! FASplashVC
            present(flyangel, animated: true, completion: nil)
            break
        case "Fly Menu":
            let flyangel = StoryboardType.fmenu_storyboard.instantiateViewController(withIdentifier: "FMenuSplashVC") as! FMenuSplashVC
            present(flyangel, animated: true, completion: nil)
            break
        default:
            break
        }
    }
}

extension SecondHomeVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Const.appNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = appListTableVw.dequeueReusableCell(withIdentifier: "appcell") as! AppTableViewCell
        
        if (indexPath.row == 0)
        {
            cell.titleLabel.text = Const.appNames[indexPath.row]
            cell.descLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: Const.appDescription[indexPath.row])
            cell.logoImage.image = UIImage(named: Const.appColorLogo[indexPath.row])
            
            if (Const.appNames[indexPath.row] == "Fly Claim") || (Const.appNames[indexPath.row] == "Fly Money") || (Const.appNames[indexPath.row] == "Fly Angel") || (Const.appNames[indexPath.row] == "Fly Menu") {
                cell.blackVw.isHidden = true
                cell.comingSoon.isHidden = true
              //  cell.bgImage.image = UIImage(named: Const.appColorBgImages[indexPath.row])
            }
            else {
                cell.blackVw.isHidden = false
                cell.comingSoon.isHidden = false
              //  cell.bgImage.image = UIImage(named: Const.appBgImages[indexPath.row])
            }
        }
        else {
            cell.titleLabel.text = Const.appNames[Const.appNames.count-indexPath.row]
            cell.descLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: Const.appDescription[Const.appNames.count-indexPath.row])
            cell.logoImage.image = UIImage(named: Const.appColorLogo[Const.appNames.count-indexPath.row])
            
            if (Const.appNames[Const.appNames.count-indexPath.row] == "Fly Claim") || (Const.appNames[Const.appNames.count-indexPath.row] == "Fly Money") || (Const.appNames[Const.appNames.count-indexPath.row] == "Fly Angel") || (Const.appNames[Const.appNames.count-indexPath.row] == "Fly Menu") {
                cell.blackVw.isHidden = true
                cell.comingSoon.isHidden = true
             //   cell.bgImage.image = UIImage(named: Const.appColorBgImages[Const.appNames.count-indexPath.row])
            }
            else {
                cell.blackVw.isHidden = false
                cell.comingSoon.isHidden = false
               // cell.bgImage.image = UIImage(named: Const.appBgImages[Const.appNames.count-indexPath.row])
            }
        }
        cell.blackVw.layer.cornerRadius = 8
        cell.bgImage.layer.cornerRadius = 8
        cell.bgImage.layer.masksToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0)
        {
            goToApps(str: Const.appNames[indexPath.row])
        }
        else {
            goToApps(str: Const.appNames[Const.appNames.count-indexPath.row])
        }
    }
}

class AppTableViewCell : UITableViewCell {
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var comingSoon: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var blackVw: UIView!
}
