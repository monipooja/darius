//
//  NotificationViewController.swift
//  Darious
//
//  Created by Apple on 14/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class NotificationViewController: ViewControllerPannable {
    
    @IBOutlet weak var notificationTableVw: UITableView!
    @IBOutlet weak var centerLbl: UILabel!
    
    @IBOutlet weak var newvw: CustomUIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var newvwHt: NSLayoutConstraint!
    
    var fromScreen : String = ""
    var dictArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        centerLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_notification_found")
        notificationTableVw.rowHeight = UITableView.automaticDimension
        notificationTableVw.estimatedRowHeight = 55
        
        if (fromScreen == "flyclaim")
        {
            newvwHt.constant = 0
            newvw.isHidden = true
            backBtn.isHidden = false
            UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FCBlackColor)
        }
        else {
            UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.themeColor)
        }
    
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.Notification_URL, parameters: params)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (fromScreen == "flyclaim")
        {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func goToHome(_ sender: Any)
    {
        let homevc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        view.window!.layer.add(transition, forKey: kCATransition)
        present(homevc, animated: false, completion: nil)
    }
    
    func callService(urlstr : String, parameters : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation
                {
                    if let _ = result["notificationData"] as? [NSDictionary]
                    {
                        self.dictArray = result["notificationData"] as! [NSDictionary]
                        if(self.dictArray.count > 0) {
                            self.notificationTableVw.reloadData()
                        } else {
                            self.centerLbl.isHidden = false
                        }
                    }
                    else {
                        self.centerLbl.isHidden = false
                    }
                    notification_count = 0
                    customLoader.hideIndicator()
            }
        }
    }
    
    // MARK : SIDE VIEW ACTIONS
    @IBAction func goToWallet(_ sender: Any)
    {
        let walletVC = storyboard!.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
        present(walletVC, animated: true, completion: nil)
    }
    @IBAction func goToNotification(_ sender: Any)
    {
        let homevc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        view.window!.layer.add(transition, forKey: kCATransition)
        present(homevc, animated: false, completion: nil)
    }
    @IBAction func goToProfile(_ sender: Any)
    {
        let profileVC = storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        present(profileVC, animated: true, completion: nil)
    }
    @IBAction func openRightSideBar(_ sender: Any)
    {
        let sidebarVC = storyboard!.instantiateViewController(withIdentifier: SideTableViewController.identifier) as! SideTableViewController
        present(sidebarVC, animated: true, completion: nil)
    }
}

extension NotificationViewController : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = notificationTableVw.dequeueReusableCell(withIdentifier: "notifycell") as! NotificationTableCell
        
        let dict = dictArray[indexPath.row]
        
        let descStr = dict["messages"] as? String
        if (descStr?.contains("_") == true) && (ApiResponse.getLanguageFromUserDefaults(inputString: descStr!) != "")
        {
            cell.msgLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: descStr!)
        } else
        {
            cell.msgLbl.text = dict["messages"] as? String
        }
        
        let datestr = dict["created_on"] as! String
        if let str = datestr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM yyyy | HH:mm a")
        {
            cell.dateLbl.text = str
        }
        
        return cell
    }
    
}

class NotificationTableCell: UITableViewCell {
    // notifycell
    
    @IBOutlet weak var msgLbl : UILabel!
    @IBOutlet weak var descLbl : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var imgvw : UIImageView!
}
