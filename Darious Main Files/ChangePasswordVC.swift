//
//  ChangePasswordVC.swift
//  Darious
//
//  Created by Apple on 21/06/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var oldPwdTextField: UITextField!
    @IBOutlet weak var newPwdTextField: UITextField!
    @IBOutlet weak var confirmPwdTextField: UITextField!
    
    @IBOutlet weak var oldPwdHt: NSLayoutConstraint!
    
    @IBOutlet weak var saveBtn: KGHighLightedButton!
    
    var screenStr : String = ""
    var mblNo : String = ""
    var stdcode : NSNumber = 0
    var otpStr : String = ""
    var userId : NSNumber = 0
    var textFArray : [UITextField] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        textFArray = [oldPwdTextField, newPwdTextField, confirmPwdTextField]
        
        if (screenStr == "otp") {
            oldPwdTextField.isHidden = true
            oldPwdHt.constant = 0
        }
        else {
            let data = userDefault.data(forKey: "UserDetails")
            let unarchData = NSKeyedUnarchiver.unarchiveObject(with: data!) as! NSDictionary
            userId = unarchData["id"] as! NSNumber
        }
        setLanguage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "change_password")
        oldPwdTextField.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "old_password")
        newPwdTextField.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "new_password")
        confirmPwdTextField.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "confirm_password")
        if (screenStr == "otp")
        {
            saveBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "update_password")), for: .normal)
        }
        else {
            saveBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "change_password")), for: .normal)
        }
    }
    
    @IBAction func onSave(_ sender: Any)
    {
        if(oldPwdTextField.text == "") && (screenStr == "sidebar")
        {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_old_password")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else if(newPwdTextField.text == "")
        {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_new_password")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else if(confirmPwdTextField.text == "")
        {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_confirm_password")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else if(newPwdTextField.text != confirmPwdTextField.text)
        {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "confirm_pass_new_pass_not_match")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else {
            newPwdTextField.resignFirstResponder()
            confirmPwdTextField.resignFirstResponder()
            
            if (screenStr == "otp")
            {
                oldPwdTextField.resignFirstResponder()
                
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&email=\(mblNo)&otp=\(otpStr)&password=\(newPwdTextField.text!)&cpassword=\(confirmPwdTextField.text!)&role=\(role)"
                callService(urlstr: Api.Upadte_Pwd_URL, parameters: params, check: "otp")
            }
            else {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(userId)&old_password=\(oldPwdTextField.text!)&password=\(newPwdTextField.text!)&cpassword=\(confirmPwdTextField.text!)"
                callService(urlstr: Api.ChangePwd_URL, parameters: params, check: "change")
            }
        }
    }
    
    @IBAction func onGoBack(_ sender: Any)
    {
        oldPwdTextField.resignFirstResponder()
        newPwdTextField.resignFirstResponder()
        confirmPwdTextField.resignFirstResponder()
        
        if (screenStr == "otp") {
            self.dismiss(animated: false, completion: nil)
        }
        else {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
        }
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation
                {
                    if (check == "change") {
                        self.oldPwdTextField.text = ""
                        self.newPwdTextField.text = ""
                        self.confirmPwdTextField.text = ""
                        
                        var finalMsg : String = ""
                        if let str = result["message"] {
                            let msgStr = str as! String
                            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                            if(showMsg != "") {
                                finalMsg = showMsg
                            }
                            else {
                                finalMsg = msgStr
                            }
                        }
                        else {
                            customLoader.hideIndicator()
                        }
                        StaticFunctions.showAlert(title: "", message: finalMsg, actions: [OkText], controller: self, completion: { (title) in
                            
                            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&user_id=\(user_id)"
                            self.callService(urlstr: Api.Logout_URL, parameters: params, check: "logout")
                        })
                    }
                    else if(check == "logout") {
                        
                        customLoader.hideIndicator()
                        userDefault.set(false, forKey: "session")
                        userDefault.set(nil, forKey: "UserDetails")
                        userDefault.synchronize()
                        Access_token = ""
                        userDefault.set(Access_token, forKey: "token")
                        userDefault.synchronize()
                        let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.present(VC, animated: true, completion: nil)
                    }
                    else {
                        if let str = result["message"] {
                            let msgStr = str as! String
                            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                            if(showMsg != "") {
                                StaticFunctions.showAlert(title: "", message: showMsg, actions: ["ok"], controller: self, completion: { (title) in
                                    
                                    userDefault.set(false, forKey: "session")
                                    userDefault.set(nil, forKey: "UserDetails")
                                    userDefault.synchronize()
                                    let loginVC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                    self.present(loginVC, animated: true, completion: nil)
                                })
                            }
                            else {
                                StaticFunctions.showAlert(title: "", message: msgStr, actions: ["ok"], controller: self, completion: { (title) in
                                    
                                    userDefault.set(false, forKey: "session")
                                    userDefault.set(nil, forKey: "UserDetails")
                                    userDefault.synchronize()
                                    let loginVC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                    self.present(loginVC, animated: true, completion: nil)
                                })
                            }
                        }
                        else {
                            customLoader.hideIndicator()
                        }
                    }
            }
        }
    }
    
}

extension ChangePasswordVC : UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //  print("did begin..........")
        
//        for tf in textFArray {
//            if(tf == textField) {
//                tf.layer.shadowOpacity = 0.5
//                tf.layer.shadowRadius = 3
//                tf.layer.shadowOffset = CGSize(width: 0, height: 0.2)
//                tf.layer.shadowColor = UIColor.lightGray.cgColor
//            }
//            else {
//                tf.layer.shadowOpacity = 0
//                tf.layer.shadowRadius = 0
//                tf.layer.shadowOffset = CGSize(width: 0, height: 0)
//                tf.layer.shadowColor = UIColor.clear.cgColor
//            }
//        }
    }
}
