//
//  ProfileViewController.swift
//  Darious
//
//  Created by Apple on 15/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import SlideMenuControllerSwift
import DropDown

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var contentVw: UIView!
    @IBOutlet weak var customVw: CustomUIView!
    @IBOutlet weak var contentHt: NSLayoutConstraint!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var bgImage: UIImageView!
    
    @IBOutlet weak var firstnameTF: UITextField!
    @IBOutlet weak var lastnameTF: UITextField!
    @IBOutlet weak var nicknameTF: UITextField!
    @IBOutlet weak var mobileNoTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var streetTF: UITextField!
    @IBOutlet weak var numberTF: UITextField!
    @IBOutlet weak var zipcodeTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var provinceTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var saveBtn: KGHighLightedButton!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var vwDismissBtn: UIButton!
    @IBOutlet weak var stdCodeBtn : UIButton!
    
    @IBOutlet var labelCollection: [UILabel]!
    
    @IBOutlet weak var newvw: CustomUIView!
    
    var datePicker : UIDatePicker?
    var doneButton : UIButton = UIButton()
    var textFArray : [UITextField] = []
    
    var presentFrom : String = ""
    var imageBase64 : String = ""
    var currentAddress : String = ""
    var dobStr : String = ""
    var lattitude : Double = 0
    var longitude : Double = 0
    var contentFinalHt : CGFloat = 0
    var age_str : Int = 0
    
    let dropDown = DropDown()
    var countryCode : NSNumber = 0
    var isoStr : String = ""
    let searchBar : UISearchBar = UISearchBar.init()
    var dataFiltered: [String] = []
    var filterCodeArray : [NSNumber] = []
    var filterIsoArray : [String] = []
    var nameArray : [String] = []
    var codeArray : [NSNumber] = []
    var isoArray : [String] = []
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        contentFinalHt = contentHt.constant
        
        let border = CALayer()
        border.backgroundColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x:0, y:newvw.frame.height, width:1, height:self.bgImage.frame.height-(newvw.frame.height))
        bgImage.layer.addSublayer(border)
        
        addDatePicker()
        searchBarIntegration()
        
        codeArray = userDefault.value(forKey: "code") as! [NSNumber]
        isoArray = userDefault.value(forKey: "iso") as! [String]
        nameArray = userDefault.value(forKey: "cname") as! [String]

        textFArray = [firstnameTF,lastnameTF,nicknameTF,mobileNoTF,emailTF, dobTF, streetTF,numberTF,zipcodeTF,cityTF,provinceTF,countryTF]
        var ii = 0
        for tf in textFArray {
            tf.delegate = self
            tf.layer.cornerRadius = 12
            if #available(iOS 11.0, *) {
                tf.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner]
            } else {
                // Fallback on earlier versions
                setMarkCorner(bounds: tf.bounds, corners: [.topRight, .topLeft, .bottomLeft, .bottomRight], textF: tf, lab: nil)
            }
            ii += 1
        }
        setLanguage()
        
       // userImage.layer.masksToBounds = false
        userImage.layer.cornerRadius = userImage.frame.width/2
        userImage.clipsToBounds = true
        logoImage.layer.cornerRadius = logoImage.frame.width/2
        logoImage.layer.masksToBounds = true
        cameraBtn.layer.cornerRadius = cameraBtn.frame.width/2
        cameraBtn.layer.masksToBounds = true
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction(sender:)))
        contentVw.addGestureRecognizer(gesture)
        
        if (presentFrom != "")
        {
            vwDismissBtn.isHidden = true
            newvw.isHidden = true
        }
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&type=get"
            self.callService(urlstr: Api.GetEdit_Profile_URL, parameters: params, check: "get")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        contentHt.constant = contentFinalHt
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        saveBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "save")), for: .normal)
        
        let tagArray = ["first_name","last_name","nick_name","mobile_no","email","dob","street","number","zipcode","city","province","country"]
        
        for i in 0...textFArray.count-1
        {
            if (ApiResponse.getLanguageFromUserDefaults(inputString: tagArray[i]) != "")
            {
                if (i == 4) || (i == 12) || (i == 13)
                {
                    textFArray[i].placeholder = "\(ApiResponse.getLanguageFromUserDefaults(inputString: tagArray[i]))"
                }
                else {
                    if (i == 0) || (i == 1) || (i == 2) || (i == 3)
                    {
                        textFArray[i].placeholder = "\(ApiResponse.getLanguageFromUserDefaults(inputString: tagArray[i]))*"
                    }
                    else {
                        textFArray[i].placeholder = "\(ApiResponse.getLanguageFromUserDefaults(inputString: tagArray[i]))"
                    }
                }
            }
        }
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        searchBar.resignFirstResponder()
        searchBar.isHidden = true
        contentVw.endEditing(true)
        contentHt.constant = contentFinalHt
    }
    
    // MARK : mask corner
    func setMarkCorner(bounds : CGRect, corners : UIRectCorner, textF : UITextField?, lab : UILabel?)
    {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: 12, height: 12))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        if (textF != nil) {
            textF!.layer.mask = mask
        } else {
            lab!.layer.mask = mask
            lab?.layer.masksToBounds = true
        }
    }
    
    @IBAction func onSaveData(_ sender: Any)
    {
        if(mobileNoTF.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_mobile_no")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else if(nicknameTF.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_nick_name")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else if (age_str < 17)
        {
            ApiResponse.alert(title: "", message: "Age must be 17 years or greater", controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&type=edit&first_name=\(self.firstnameTF.text!)&last_name=\(self.lastnameTF.text!)&email=\(self.emailTF.text!)&nickname=\(self.nicknameTF.text!)&mobile=\(self.mobileNoTF.text!)&std_code=\(self.countryCode)&con_code=\(self.isoStr)&address=&street=\(self.streetTF.text!)&number=\(self.numberTF.text!)&city=\(self.cityTF.text!)&province=\(self.provinceTF.text!)&country=\(self.countryTF.text!)&zipcode=\(self.zipcodeTF.text!)&latitude=\(self.lattitude)&longitude=\(self.longitude)&dob=\(self.dobStr)&profile_pic=\(self.imageBase64)&bank_bic=&bank_iban="
           // print("paraaaaa = \(params)")
            self.callService(urlstr: Api.GetEdit_Profile_URL, parameters: params, check: "edit")
        }
    }
    
    // Camera Action
    @IBAction func openCameraGallery(_ sender: Any)
    {
        let imgPick = UIImagePickerController.init()
        imgPick.delegate = self
        ApiResponse.CameraGallery(controller: self, imagePicker: imgPick)
    }
    
    // Image picker delegate
    func imagePickerController(_ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        var image = info[.originalImage] as! UIImage
        if let _ = fixedOrientation(passImg: image)
        {
            image = fixedOrientation(passImg: image)!
        }
        userImage.image = image
        let imageData = image.jpegData(compressionQuality: 0.2)
        imageBase64 = imageData!.base64EncodedString(options: .lineLength64Characters)
       // print("base 64 = \(imageBase64)")
        imageBase64 = imageBase64.replacingOccurrences(of: "+", with: "%2B")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectDOB(_ sender: Any)
    {
        self.view.endEditing(true)
        contentHt.constant = contentFinalHt + 180
        datePicker?.isHidden = false
        doneButton.isHidden = false
    }
    
    @IBAction func getStdCode(_ sender: Any)
    {
        searchBar.isHidden = false
        
        searchBar.text = ""
        mobileNoTF.resignFirstResponder()
        
        dropDown.anchorView = searchBar
        dropDown.dataSource = nameArray
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            let codestr = (self.isoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
            cell.codeLabel.text = "+\(self.codeArray[index])"
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            self.stdCodeBtn.setTitle("+\(self.codeArray[index])", for: .normal)
            self.countryCode = self.codeArray[index]
            self.isoStr = self.isoArray[index]
            self.mobileNoTF.becomeFirstResponder()
            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
        }
        dropDown.width = self.searchBar.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
        
        searchBar.becomeFirstResponder()
    }
    
    // MARK : Search bar create
    func searchBarIntegration()
    {
        searchBar.frame = CGRect(x: 20, y: 40, width: self.view.frame.width-40, height:50)
        searchBar.delegate = self
        self.view.addSubview(searchBar)
        searchBar.isHidden = true
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (presentFrom == "home") {
            dismiss(animated: false, completion: nil)
        }
        else {
        }
    }
    
    @IBAction func goToHome(_ sender: Any)
    {
        let homevc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        view.window!.layer.add(transition, forKey: kCATransition)
        present(homevc, animated: false, completion: nil)
    }
    
    // MARK : Added DatePicker To View
    func addDatePicker()
    {
        datePicker = UIDatePicker.init(frame: CGRect(x: -30, y: self.view.frame.height-200, width: self.view.frame.width+30, height: 200))
        datePicker?.datePickerMode = .date
        datePicker?.backgroundColor = UIColor.white
        datePicker?.tintColor = UIColor.black
        datePicker?.maximumDate = Date()
        self.view.addSubview(datePicker!)
        datePicker?.isHidden = true
        
        datePicker?.locale = Locale.init(identifier: Lang_Locale)
        
        //Done Button
        doneButton = UIButton.init(frame: CGRect(x: -30, y: self.view.frame.height-240, width: (datePicker?.frame.width)!, height: 40))
        doneButton.titleLabel?.textAlignment = .center
        doneButton.setTitleColor(UIColor.black, for: .normal)
        doneButton.backgroundColor = UIColor.white
        doneButton.titleLabel?.font = UIFont.init(name: "ProximaNova-Regular", size: 15)
        doneButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "common_done_ios"), for: .normal)
        
        doneButton.addTarget(self, action: #selector(donedatePicker(_:)), for: .touchUpInside)
        self.view.addSubview(doneButton)
        doneButton.isHidden = true
        
        let topBorder = CALayer()
        topBorder.backgroundColor = UIColor.lightGray.cgColor
        topBorder.frame = CGRect(x:0, y:0, width:doneButton.frame.width, height:0.5)
        doneButton.layer.addSublayer(topBorder)
    }
    
    @objc func donedatePicker(_ sender : UIButton)
    {
        datePicker?.isHidden = true
        sender.isHidden = true
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dobTF.text = formatter.string(from: (datePicker?.date)!)
        dobStr = formatter.string(from: (datePicker?.date)!).toDateString(inputFormat: "dd/MM/yyyy", outputFormat: "yyyy-MM-dd")!
        self.view.endEditing(true)
        contentHt.constant = contentFinalHt
        
        let components = Set<Calendar.Component>([.year])
        let ageComponents = Calendar.current.dateComponents(components, from: (datePicker?.date)!, to: Date())
        age_str = ageComponents.year!
    }
    
    // Mark : Add Bottom Layer
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:customVw.frame.width-13, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width-8, height:1)
        }
        textF.layer.addSublayer(border)
    }
    
    // Set Data
    func setData(dict : NSDictionary)
    {
        if(dict["profile_pic"] as! String != "") {
        
            let urlstr = "\(Image_URL)\(dict["profile_pic"] as! String)"
            DispatchQueue.main.async {
                self.userImage.sd_setImage(with: URL(string: urlstr), placeholderImage: UIImage.init(named: "demo_profile.png"))
            }
        }
        
        firstnameTF.text = dict["first_name"] as? String
        lastnameTF.text = dict["last_name"] as? String
        emailTF.text = dict["email"] as? String
        nicknameTF.text = dict["nickname"] as? String
        if (dict["nickname"] as! String == "")
        {
            nicknameTF.isUserInteractionEnabled = true
        }
        else {
            nicknameTF.isUserInteractionEnabled = false
        }
        
        if (dict["mobile"] as! String != "")
        {
             isoStr = dict["country_code"] as! String
             countryCode = dict["stdcode"] as! NSNumber
             stdCodeBtn.setTitle(" +\(dict["stdcode"] as! NSNumber)", for: .normal)
             mobileNoTF.text = "\(dict["mobile"] as! String)"
        }
        else {
            if(isoArray.contains(Locale.current.regionCode!)) {
                let indx = self.isoArray.index(of: Locale.current.regionCode!)
                stdCodeBtn.setTitle(" +\(codeArray[indx!])", for: .normal)
                countryCode = codeArray[indx!]
                isoStr = isoArray[indx!]
            }
        }
        
        if (dict["date_of_birth"] as! String != "0000-00-00") {
            dobTF.text = (dict["date_of_birth"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd/MM/yyyy")!
            
            dobStr = (dict["date_of_birth"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "yyyy-MM-dd")!
        }
        streetTF.text = dict["street"] as? String
        numberTF.text = dict["number"] as? String
        zipcodeTF.text = dict["zipcode"] as? String
        cityTF.text = dict["city"] as? String
        provinceTF.text = dict["province"] as? String
        countryTF.text = dict["country"] as? String
    }
    
    // MARK : Reverse Geocoding (Coordinate from Address)
    func getAddressFromCoordinate(location : CLLocation)
    {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location, completionHandler:
            {
                placemarks, error -> Void in
                // Place details
                if let placeMark = placemarks?.first {
                    
                    var completeAddress = ""
                    if let _ = placeMark.subThoroughfare {
                        completeAddress = "\(placeMark.subThoroughfare!)"
                    }
                    if placeMark.subLocality != nil {
                        completeAddress = "\(completeAddress) \(placeMark.subLocality!)"
                    }
                    if placeMark.locality != nil {
                        completeAddress = "\(completeAddress),\(placeMark.locality!)"
                    }
                    if placeMark.administrativeArea != nil {
                        completeAddress = "\(completeAddress),\(placeMark.administrativeArea!)"
                    }
                    if placeMark.postalCode != nil {
                        completeAddress = "\(completeAddress) \(placeMark.postalCode!)"
                    }
                    if placeMark.country != nil {
                        completeAddress = "\(completeAddress),\(placeMark.country!)"
                    }
                    
                    self.currentAddress = completeAddress
                    let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&type=edit&first_name=\(self.firstnameTF.text!)&last_name=\(self.lastnameTF.text!)&email=\(self.emailTF.text!)&address=\(self.currentAddress)&street=\(self.streetTF.text!)&number=\(self.numberTF.text!)&std_code=\(self.countryCode)&city=\(self.cityTF.text!)&province=\(self.provinceTF.text!)&country=\(self.countryTF.text!)&zipcode=\(self.zipcodeTF.text!)&latitude=\(self.lattitude)&longitude=\(self.longitude)&dob=\(self.dobStr)&profile_pic=\(self.imageBase64)&bank_bic=&bank_iban="
                   // print("paraaaaa = \(params)")
                    self.callService(urlstr: Api.GetEdit_Profile_URL, parameters: params, check: "edit")
                }
                else {
                    let showmsg = ApiResponse.getLanguageFromUserDefaults(inputString: "invalid_address")
                    ApiResponse.alert(title: "", message: showmsg, controller: self)
                }
        })
    }
    // MARK : Geocoding (address to coordinate)
    func getCoordinateFromAddress(address : String)
    {
       // let add = "Bholaram Ustad Marg, Indra Puri Colony, Indrapuri Colony, Bhanwar Kuwa, Indore, Madhya Pradesh 452014"
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            if let placemk = placemarks?.first {
                let location = placemk.location
                self.lattitude = (location?.coordinate.latitude)!
                self.longitude = (location?.coordinate.longitude)!
                //  print("location = \((location?.coordinate)!)")
                
                self.getAddressFromCoordinate(location: location!)
            }
            else {
                // print("location = \(error)")
                let showmsg = ApiResponse.getLanguageFromUserDefaults(inputString: "invalid_address")
                ApiResponse.alert(title: "", message: showmsg, controller: self)
            }
        }
    }
    
    // MARK : Service Call
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation
                {
                    if (check == "get") || (check == "save") {
                      //  print("get profile = \(result)")
                        
                        let userddata = result["userDetails"] as! NSDictionary
                        userData = userddata
                        
                        if ((userData["profile_pic"] as! String).contains("graph.facebook.com") == true) || ((userData["profile_pic"] as! String).contains("googleusercontent") == true)
                        {
                            Image_URL = ""
                        }
                        else {
                            Image_URL = Api.Profile_BaseURL
                        }
                        userDefault.set(Image_URL, forKey: "profileBaseURL")
                        
                        self.setData(dict: userddata)
                        
                        let archUserDetails = NSKeyedArchiver.archivedData(withRootObject: userddata)
                        userDefault.set(archUserDetails, forKey: "UserDetails")
                        userDefault.synchronize()
                        
                        customLoader.hideIndicator()
                        if (self.presentFrom != "") && (check == "save")
                        {
                            let appdelegate = UIApplication.shared.delegate as! AppDelegate
                            appdelegate.addSideController()
                        }
                    }
                    else {
                        customLoader.hideIndicator()
                        let msgStr = result["message"] as! String
                        let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                        StaticFunctions.showAlert(title: "", message: showMsg, actions: [OkText], controller: self, completion: { (titlestr) in
                            
                            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&type=get"
                            self.callService(urlstr: Api.GetEdit_Profile_URL, parameters: params, check: "save")
                        })
                    }
            }
        }
    }
    
    // MARK : SIDE VIEW ACTIONS
    @IBAction func goToWallet(_ sender: Any)
    {
        let walletVC = storyboard!.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
        present(walletVC, animated: true, completion: nil)
    }
    
    @IBAction func goToNotification(_ sender: Any)
    {
        let notifyVC = storyboard!.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        present(notifyVC, animated: true, completion: nil)
    }
    
    @IBAction func goToProfile(_ sender: Any)
    {
        let homevc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        view.window!.layer.add(transition, forKey: kCATransition)
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func openRightSideBar(_ sender: Any)
    {
        let sidebarVC = storyboard!.instantiateViewController(withIdentifier: SideTableViewController.identifier) as! SideTableViewController
        present(sidebarVC, animated: true, completion: nil)
    }
    
    /////////////
    func fixedOrientation(passImg : UIImage) -> UIImage? {
       
            guard passImg.imageOrientation != UIImage.Orientation.up else {
                //This is default orientation, don't need to do anything
                return passImg.copy() as? UIImage
            }
            
            guard let cgImage = passImg.cgImage else {
                //CGImage is not available
                return nil
            }
        // let colorSpace = cgImage.colorSpace,
            guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(passImg.size.width), height: Int(passImg.size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {

                return nil //Not able to create CGContext
            }
            var transform: CGAffineTransform = CGAffineTransform.identity
           
            switch passImg.imageOrientation {
                
            case .down, .downMirrored:
                
                transform = transform.translatedBy(x: passImg.size.width, y: passImg.size.height)
                
                transform = transform.rotated(by: CGFloat.pi)
                
                break
                
            case .left, .leftMirrored:
                
                transform = transform.translatedBy(x: passImg.size.width, y: 0)
                
                transform = transform.rotated(by: CGFloat.pi / 2.0)
                
                break
                
            case .right, .rightMirrored:
                
                transform = transform.translatedBy(x: 0, y: passImg.size.height)
                
                transform = transform.rotated(by: CGFloat.pi / -2.0)
                
                break
                
            case .up, .upMirrored:
                
                break
                
            }
            
            //Flip image one more time if needed to, this is to prevent flipped image
            
            switch passImg.imageOrientation {
                
            case .upMirrored, .downMirrored:
                
                transform.translatedBy(x: passImg.size.width, y: 0)
                
                transform.scaledBy(x: -1, y: 1)
                
                break
                
            case .leftMirrored, .rightMirrored:
                
                transform.translatedBy(x: passImg.size.height, y: 0)
                
                transform.scaledBy(x: -1, y: 1)
                
            case .up, .down, .left, .right:
                
                break
            }
            
            ctx.concatenate(transform)
        
            switch passImg.imageOrientation {
                
            case .left, .leftMirrored, .right, .rightMirrored:
                
                ctx.draw(passImg.cgImage!, in: CGRect(x: 0, y: 0, width: passImg.size.height, height: passImg.size.width))
                
            default:
                
                ctx.draw(passImg.cgImage!, in: CGRect(x: 0, y: 0, width: passImg.size.width, height: passImg.size.height))
                
                break
            }
            
            guard let newCGImage = ctx.makeImage() else { return nil }
            
            return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}


extension ProfileViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        contentHt.constant = contentFinalHt
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        contentHt.constant = contentFinalHt + 180
        
        if (textField == dobTF) {
            datePicker?.isHidden = false
            doneButton.isHidden = false
        }
        else {
            datePicker?.isHidden = true
            doneButton.isHidden = true
        }
    }
}

extension ProfileViewController : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dataFiltered = searchText.isEmpty ? nameArray : nameArray.filter({ (dat) -> Bool in
            dat.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        dropDown.dataSource = dataFiltered
        filterCodeArray = []
        filterIsoArray = []
        
        for str in dataFiltered {
            var i : Int = 0
            for str1 in nameArray {
                if (str == str1) {
                    filterCodeArray.append(codeArray[i])
                    filterIsoArray.append(isoArray[i])
                    break
                }
                i += 1
            }
        }
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            let codestr = (self.filterIsoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png"))
            cell.codeLabel.text = "+\(self.filterCodeArray[index])"
        }
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            self.stdCodeBtn.setTitle("+\(self.filterCodeArray[index])", for: .normal)
            self.countryCode = self.filterCodeArray[index]
            self.mobileNoTF.becomeFirstResponder()
            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
        }
        
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        dataFiltered = nameArray
        filterCodeArray = codeArray
        filterIsoArray = isoArray
        searchBar.isHidden = true
        dropDown.hide()
    }
}
