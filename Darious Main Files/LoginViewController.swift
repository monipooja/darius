//
//  LoginViewController.swift
//  Darious
//
//  Created by Apple on 07/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage
import FirebaseMessaging

class LoginViewController: UIViewController {

    @IBOutlet weak var loginBtn: KGHighLightedButton!
    @IBOutlet weak var accLabel: UILabel!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var forgotBtn: UIButton!
   // @IBOutlet weak var stdCodeBtn: UIButton!
    
    @IBOutlet weak var createdLabel: UILabel!
    
    @IBOutlet weak var mobileTextField: KGHighLightedField!
    @IBOutlet weak var passwordTextField: KGHighLightedField!
    
    let dropDown = DropDown()
    var countryCode : NSNumber = 0
    var isoStr : String = ""
    
    let searchBar : UISearchBar = UISearchBar.init()
    var dataFiltered: [String] = []
    var filterCodeArray : [NSNumber] = []
    var filterIsoArray : [String] = []
    var nameArray : [String] = []
    var codeArray : [NSNumber] = []
    var isoArray : [String] = []
    
    var textFArray : [UITextField] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        searchBarIntegration()
        
        codeArray = userDefault.value(forKey: "code") as! [NSNumber]
        isoArray = userDefault.value(forKey: "iso") as! [String]
        nameArray = userDefault.value(forKey: "cname") as! [String]
        
        if(isoArray.contains(Locale.current.regionCode!)) {
            let indx = self.isoArray.index(of: Locale.current.regionCode!)
           // stdCodeBtn.setTitle(" +\(codeArray[indx!])", for: .normal)
            countryCode = codeArray[indx!]
        }
       // stdCodeBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
    
        setLanguage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.searchBar.isHidden = true
        self.view.frame.origin.y = 0
    }
    
    // MARK : Search bar create
    func searchBarIntegration()
    {
        searchBar.frame = CGRect(x: 20, y: 40, width: self.view.frame.width-40, height:50)
        searchBar.delegate = self
        self.view.addSubview(searchBar)
        searchBar.isHidden = true
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        createdLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "this_is_social_platform")
        accLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "dont_have_account")
        mobileTextField.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "email")
        passwordTextField.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "password")
        loginBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "login")), for: .normal)
        signupBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "signup")), for: .normal)
        forgotBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "forget_password") + "?"), for: .normal)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onLogin(_ sender: Any)
    {
        if(mobileTextField.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_email")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else if (ApiResponse.validateEmail(mobileTextField.text!) == false) {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_valid_email")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else if(passwordTextField.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_password")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else {
            self.view.frame.origin.y = 0
            mobileTextField.resignFirstResponder()
            passwordTextField.resignFirstResponder()
            customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&email=\(mobileTextField.text!)&password=\(passwordTextField.text!)&role=\(role)"
            callService(urlstr: Api.Login_URL, parameters: params, check: "login")
        }
    }
    
    @IBAction func getStdCode(_ sender: Any)
    {
        searchBar.isHidden = false
        
        searchBar.text = ""
        mobileTextField.resignFirstResponder()
        
        dropDown.anchorView = searchBar
        dropDown.dataSource = nameArray
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            let codestr = (self.isoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
            cell.codeLabel.text = "+\(self.codeArray[index])"
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
//            self.stdCodeBtn.setTitle("+\(self.codeArray[index])", for: .normal)
//            self.countryCode = self.codeArray[index]
//            self.mobileTextField.becomeFirstResponder()
//            self.searchBar.isHidden = true
//            self.searchBar.resignFirstResponder()
        }
        dropDown.width = self.searchBar.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
        
        searchBar.becomeFirstResponder()
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation
            {
              //  print("login result === \(result)")
                customLoader.hideIndicator()
                let userDetails = result["userDetails"] as! NSDictionary
                let archUserDetails = NSKeyedArchiver.archivedData(withRootObject: userDetails)
                userDefault.set(archUserDetails, forKey: "UserDetails")
                userDefault.set(true, forKey: "session")
                
                userData = userDetails
                user_id = userDetails["id"] as! NSNumber
                wallet_amount = userDetails["wallet_amount"] as! NSNumber
                userDefault.set(wallet_amount, forKey: "walletAmt")
                Free_Reveal = result["free_reveal"] as! Bool
                
                if ((userData["profile_pic"] as! String).contains("graph.facebook.com") == true) || ((userData["profile_pic"] as! String).contains("googleusercontent") == true)
                {
                    Image_URL = ""
                }
                else {
                    Image_URL = Api.Profile_BaseURL
                }
                userDefault.set(Image_URL, forKey: "profileBaseURL")
                userDefault.synchronize()
                
                let subscribeTopic = "\(userDetails["id"] as! NSNumber)"
                Messaging.messaging().subscribe(toTopic: subscribeTopic)
                
                if let petid = userDefault.value(forKey: "share_petition") as? String
                {
                    UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FCBlackColor)
                    userDefault.set(nil, forKey: "share_petition")
                    let controller = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "PetitionDetailsVC") as! PetitionDetailsVC
                    controller.petitionId = NSNumber(value: Int(petid)!)
                    // controller.checkScreen = "login"
                    self.present(controller, animated: false, completion: nil)
                }
                else if let shareid = userDefault.value(forKey: "share_ff") as? String {
                    UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                    userDefault.set(nil, forKey: "share_ff")
                    if (shareid == "\(user_id)")
                    {
                        let controller = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFHomeVC") as! FFHomeVC
                        self.present(controller, animated: false, completion: nil)
                    }
                    else {
                        let controller = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFfriendProfileVC") as! FFfriendProfileVC
                        controller.passid = NSNumber(value: Int(shareid)!)
                        controller.checkScreen = "login"
                        self.present(controller, animated: false, completion: nil)
                    }
                }
                else {
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    appdelegate.addSideController()
                }
            }
        }
    }
}

extension LoginViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.frame.origin.y = 0
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //  print("did begin..........")
        if (self.view.frame.origin.y == 0){
            self.view.frame.origin.y -= 60
        }
    }
}

extension LoginViewController : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dataFiltered = searchText.isEmpty ? nameArray : nameArray.filter({ (dat) -> Bool in
            dat.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        dropDown.dataSource = dataFiltered
        filterCodeArray = []
        filterIsoArray = []
        
        for str in dataFiltered {
            var i : Int = 0
            for str1 in nameArray {
                if (str == str1) {
                    filterCodeArray.append(codeArray[i])
                    filterIsoArray.append(isoArray[i])
                    break
                }
                i += 1
            }
        }
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            let codestr = (self.filterIsoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png"))
            cell.codeLabel.text = "+\(self.filterCodeArray[index])"
        }
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
//            self.stdCodeBtn.setTitle("+\(self.filterCodeArray[index])", for: .normal)
//            self.countryCode = self.filterCodeArray[index]
//            self.mobileTextField.becomeFirstResponder()
//            self.searchBar.isHidden = true
//            self.searchBar.resignFirstResponder()
        }
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        dataFiltered = nameArray
        filterCodeArray = codeArray
        filterIsoArray = isoArray
        searchBar.isHidden = true
        dropDown.hide()
    }
}
