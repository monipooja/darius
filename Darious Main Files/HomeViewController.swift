//
//  HomeViewController.swift
//  Darious
//
//  Created by Apple on 08/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import WebKit
import SlideMenuControllerSwift
import Contacts

var openBool : Bool! = false
var openWebview : Bool! = false
var policyOrTerms : String = ""

class HomeViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    static let identifier = String("HomeViewController")
    
    @IBOutlet weak var createdLabel: UILabel!
    @IBOutlet weak var appTitleBtn: KGHighLightedButton!
    @IBOutlet weak var appImage: UIImageView!
    @IBOutlet weak var appDescription: UILabel!
    @IBOutlet weak var sideVw: CustomUIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var pagecontrol: UIPageControl!
    
    @IBOutlet weak var appCollectionView: UICollectionView!
    @IBOutlet weak var newViewBtm: NSLayoutConstraint!
    @IBOutlet weak var collVwHt: NSLayoutConstraint!
    
    @IBOutlet weak var logoWd: NSLayoutConstraint!
    @IBOutlet weak var logoHt: NSLayoutConstraint!
    @IBOutlet weak var centerIconWd: NSLayoutConstraint!
    @IBOutlet weak var centerIconHt: NSLayoutConstraint!
    @IBOutlet weak var descHt: NSLayoutConstraint!
    @IBOutlet weak var titleBtnHt: NSLayoutConstraint!
    
    var xvalue : CGFloat = 0
    var items: [Int] = []
    var CenterLogoImg : UIImageView!
    var policyWebview : WKWebView!
    var bgview : UIView!
    var closeWebVwButton : UIButton!
    
    var indexValue : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        view.backgroundColor = UIColor.white
        view.endEditing(true)
        
        pagecontrol.isHidden = true
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.themeColor)
        
        let data : NSDictionary = ["id" : user_id]
        print("idddd = \(user_id)")
        socket.emit("join", data)
        
        setLanguage()
        
        backButton.layer.cornerRadius = backButton.frame.width/2
        backButton.layer.masksToBounds = true
        nextButton.layer.cornerRadius = nextButton.frame.width/2
        nextButton.layer.masksToBounds = true
        
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor.init(hexString: "EB4507").cgColor
        
        appDescription.layer.cornerRadius = 8
        appDescription.layer.masksToBounds = true
        
        backButton.isEnabled = false
        appImage.image = UIImage(named: Const.appColorLogo[indexValue])
        appTitleBtn.setTitle(Const.appNames[indexValue], for: .normal)
        appTitleBtn.backgroundColor = Const.appThemsColor[indexValue]
        appDescription.text = ApiResponse.getLanguageFromUserDefaults(inputString: Const.appDescription[indexValue])
        
        if (self.view.frame.height >=  812)
        {
            newViewBtm.constant = 20
        }
        
        if (self.view.frame.width == 414) {
           collVwHt.constant = collVwHt.constant + 5
        }
        else if (self.view.frame.width == 320) {
            collVwHt.constant = collVwHt.constant - 30
            centerIconHt.constant = 50
            centerIconWd.constant = 50
            descHt.constant = 30
            titleBtnHt.constant = 35
            appDescription.font = UIFont.init(name: "Arial", size: 8)
            appTitleBtn.titleLabel?.font = UIFont.init(name: "Pristina-Regular", size: 18)
        } else {
        }
        
        let check = userDefault.value(forKey: "onoff") as? [String]
        if (check == nil)
        {
            var arr : [String] = []
            for _ in Const.appNames
            {
                arr.append("on")
            }
            userDefault.setValue(arr, forKey: "onoff")
            userDefault.synchronize()
        }
       
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.Order_Count_URL, parameters: params)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.setWebView()
        }
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        createdLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "this_is_social_platform")
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // Mark : Set WebView for Privacy & Policy
    func setWebView()
    {
        if openWebview
        {
            bgview = UIView.init()
            bgview.frame = view.frame
            bgview.backgroundColor = UIColor.black
            bgview.alpha = 0.5
            view.addSubview(bgview)
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
            gesture.numberOfTapsRequired = 1
            bgview.addGestureRecognizer(gesture)
            
            policyWebview = WKWebView.init(frame: CGRect(x: 20, y: 50, width: view.frame.width-40, height: view.frame.height-50))
            customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: policyWebview)
            
            if (policyOrTerms == "terms")
            {
                // http://flydarius.com/term-and-condition
                let url = URL(string: "https://flydarius.com/term-and-condition")!
                policyWebview.load(URLRequest(url: url))
            }
            else {
                // http://flydarius.com/privacy-policy
                let url = URL(string: "https://flydarius.com/privacy-policy")!
                policyWebview.load(URLRequest(url: url))
            }
            policyWebview.navigationDelegate = self
            policyWebview.autoresizingMask = [.flexibleWidth,.flexibleHeight]
            view.addSubview(policyWebview)
            policyWebview.allowsBackForwardNavigationGestures = true
            
            closeWebVwButton = UIButton.init(frame: CGRect(x: policyWebview.frame.width-40, y: 10, width: 30, height: 30))
            closeWebVwButton.setImage(#imageLiteral(resourceName: "theme_cross"), for: .normal)
            closeWebVwButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            closeWebVwButton.backgroundColor = UIColor.white
            closeWebVwButton.layer.cornerRadius = closeWebVwButton.frame.width/2
            closeWebVwButton.layer.masksToBounds = true
            closeWebVwButton.tintColor = UIColor.init(hexString: ColorCode.themeColor)
            closeWebVwButton.addTarget(self, action: #selector(closeWebview(sender:)), for: .touchUpInside)
            policyWebview.addSubview(closeWebVwButton)
            
            openWebview = false
        }
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        customLoader.hideIndicator()
    }
    
    @objc func closeWebview(sender : UIButton)
    {
        openWebview = false
        bgview.isHidden = true
        policyWebview.isHidden = true
        closeWebVwButton.removeFromSuperview()
        bgview.removeFromSuperview()
        policyWebview.removeFromSuperview()
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer)
    {
        openWebview = false
        bgview.isHidden = true
        policyWebview.isHidden = true
        closeWebVwButton.removeFromSuperview()
        bgview.removeFromSuperview()
        policyWebview.removeFromSuperview()
    }
    
    @IBAction func nextApp(_ sender: Any)
    {
        if (indexValue != 1)
        {
            backButton.isEnabled = true
            indexValue += 2
        }
        else {
            nextButton.isEnabled = false
        }
        
//        else {
//            indexValue += 2
//            if (indexValue == 10) {
//                appCollectionView.scrollToItem(at: IndexPath(item: 19, section: 0), at: .right, animated: true)
//            }
//        }

        if (Const.appNames[indexValue] == "FlyFriends") || (Const.appNames[indexValue] == "FlySport") || (Const.appNames[indexValue] == "FlyBest") || (Const.appNames[indexValue] == "FlyClaim") || (Const.appNames[indexValue] == "FlyPic") || (Const.appNames[indexValue] == "FlyAds") || (Const.appNames[indexValue] == "FlyMeet")
        {
            appImage.image = UIImage(named: Const.appColorLogo[indexValue])
            if (Const.appNames[indexValue] == "FlyMeet")
            {
                appImage.tintColor = ColorCode.FMeetDarkTheme
            }
        }
        else {
            appImage.image = UIImage(named: Const.appGrayLogo[indexValue])
        }
        appTitleBtn.setTitle(Const.appNames[indexValue], for: .normal)
        appTitleBtn.backgroundColor = Const.appThemsColor[indexValue]
        appDescription.text = ApiResponse.getLanguageFromUserDefaults(inputString: Const.appDescription[indexValue])
        appCollectionView.reloadData()
        
        if (indexValue == 8) {
            indexValue = 1
        }
    }
    
    @IBAction func previousApp(_ sender: Any)
    {
        if (indexValue > 0) {
            if (indexValue == 1) {
                nextButton.isEnabled = true
                indexValue = 8
            } else {
                indexValue -= 2
                if (indexValue == 0)
                {
                    backButton.isEnabled = false
                }
            }
            
//            if (indexValue == 12) || (indexValue == 14) || (indexValue == 16)  {
//                indexValue -= 2
//            }
//            else {
//                if (indexValue == 9) {
//                    appCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: true)
//                }
//            }
            
            if (Const.appNames[indexValue] == "FlyFriends") || (Const.appNames[indexValue] == "FlySport") || (Const.appNames[indexValue] == "FlyBest") || (Const.appNames[indexValue] == "FlyClaim") || (Const.appNames[indexValue] == "FlyPic") || (Const.appNames[indexValue] == "FlyAds") || (Const.appNames[indexValue] == "FlyMeet")
            {
                appImage.image = UIImage(named: Const.appColorLogo[indexValue])
                if (Const.appNames[indexValue] == "FlyMeet")
                {
                    appImage.tintColor = ColorCode.FMeetDarkTheme
                }
            }
            else {
                appImage.image = UIImage(named: Const.appGrayLogo[indexValue])
            }
            appTitleBtn.setTitle(Const.appNames[indexValue], for: .normal)
            appTitleBtn.backgroundColor = Const.appThemsColor[indexValue]
            appDescription.text = ApiResponse.getLanguageFromUserDefaults(inputString: Const.appDescription[indexValue])
            appCollectionView.reloadData()
        }
    }
    
    @IBAction func selectApp(_ sender: Any)
    {
        goToApps(str: ((sender as! UIButton).titleLabel?.text)!)
    }
    
    @IBAction func goToApp(_ sender: Any)
    {
        if (appImage.image == #imageLiteral(resourceName: "C_FlyFriend"))
        {
            goToApps(str: "FlyFriends")
        }
        else if (appImage.image == #imageLiteral(resourceName: "C_FlyClaim"))
        {
            goToApps(str: "FlyClaim")
        }
        else if (appImage.image == #imageLiteral(resourceName: "C_FlyBest"))
        {
            goToApps(str: "FlyBest")
        }
        else if (appImage.image == #imageLiteral(resourceName: "C_FlySports"))
        {
            goToApps(str: "FlySport")
        }
        else if (appImage.image == #imageLiteral(resourceName: "C_FlyPic"))
        {
            goToApps(str: "FlyPic")
        }
        else if (appImage.image == #imageLiteral(resourceName: "C_FlyAds"))
        {
            goToApps(str: "FlyAds")
        }
        else if (appImage.image == #imageLiteral(resourceName: "fmeet_topIcon"))
        {
            goToApps(str: "FlyMeet")
        }
        else {}
    }

    @IBAction func goToWallet(_ sender: Any)
    {
        let walletVC = storyboard!.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
        present(walletVC, animated: true, completion: nil)
    }
    
    @IBAction func goToNotification(_ sender: Any)
    {
        let notifyVC = storyboard!.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        present(notifyVC, animated: true, completion: nil)
    }
    
    @IBAction func goToProfile(_ sender: Any)
    {
        let profileVC = storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        present(profileVC, animated: true, completion: nil)
    }
    
    @IBAction func openRightSideBar(_ sender: Any)
    {
        let sidebarVC = storyboard!.instantiateViewController(withIdentifier: SideTableViewController.identifier) as! SideTableViewController
        present(sidebarVC, animated: true, completion: nil)
    }
    
    func goToApps(str : String)
    {
        let check = checkUnlockApp(index: indexValue)
        if (check == false) {
            switch str {
            case "FlyClaim":
                let flyclaim = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "FCSplashVC") as! FCSplashVC
                present(flyclaim, animated: true, completion: nil)
                break
            case "FlyFriends":
                let flyfriends = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFSplashVC") as! FFSplashVC
                present(flyfriends, animated: true, completion: nil)
                break
            case "FlyBest":
                let flybest = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBSplashVC") as! FBSplashVC
                present(flybest, animated: true, completion: nil)
                break
            case "FlySport":
                let flysport = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSSplashVC") as! FSSplashVC
                present(flysport, animated: true, completion: nil)
                break
            case "FlyPic":
                let flypic = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FlyPicSplashVC") as! FlyPicSplashVC
                present(flypic, animated: true, completion: nil)
                break
            case "FlyAds":
                let flypic = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FlyadSplashVC") as! FlyadSplashVC
                present(flypic, animated: true, completion: nil)
                break
            case "FlyMeet":
                let flymeet = StoryboardType.fmeet_storyboard.instantiateViewController(withIdentifier: "FMeetSplashVC") as! FMeetSplashVC
                let nav = UINavigationController.init(rootViewController: flymeet)
                nav.isNavigationBarHidden = true
                present(nav, animated: true, completion: nil)
                break
            default:
                break
            }
        }
    }
    
    func callService(urlstr : String, parameters : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
           // print("notification count result = \(result)")
            OperationQueue.main.addOperation {
                let dict = result["data"] as! NSDictionary
                notification_count = dict["notificationCount"] as! NSNumber
                wallet_amount = NSNumber.init(value: Float(String(format: "%.2f", Double(truncating: dict["wallet_amount"] as! NSNumber)))!)
                Free_Reveal = dict["free_reveal"] as! Bool
                customLoader.hideIndicator()
                
                //self.fetchContacts()
            }
        }
    }
    
    func checkUnlockApp(index : Int) -> Bool
    {
        let arr = userDefault.value(forKey: "onoff") as! [String]
        if (arr[index] == "off"){
            ApiResponse.alert(title: "", message: "Application is lock", controller: self)
            return true
        }
        else {
            return false
        }
    }
}

extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Const.appNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = appCollectionView.dequeueReusableCell(withReuseIdentifier: "appcell", for: indexPath) as! AppCollectionCell
        
        if (indexPath.item <= 2) || (indexPath.item == 4) || (indexPath.item == 6) || (indexPath.item == 8)
        {
            cell.nameLbl.text = Const.appNames[indexPath.item]
            
            if (self.view.frame.width == 320) && (self.view.frame.height == 568)
            {
                cell.nameLbl.font = UIFont.init(name: "Pristina-Regular", size: 11)
            }
            else if (self.view.frame.width == 375)
            {
                cell.nameLbl.font = UIFont.init(name: "Pristina-Regular", size: 13)
            }
            else {
                cell.nameLbl.font = UIFont.init(name: "Pristina-Regular", size: 15)
            }
            
            if (Const.appNames[indexPath.item] == "FlyFriends") || (Const.appNames[indexPath.item] == "FlyBest") || (Const.appNames[indexPath.item] == "FlySport") || (Const.appNames[indexPath.item] == "FlyClaim") || (Const.appNames[indexPath.item] == "FlyPic") || (Const.appNames[indexPath.item] == "FlyAds") || (Const.appNames[indexPath.item] == "FlyMeet")
            {
                cell.imageVw.image = UIImage(named: Const.appColorLogo[indexPath.item])
                if (Const.appNames[indexPath.item] == "FlyMeet")
                {
                    cell.imageVw.tintColor = ColorCode.FMeetDarkTheme
                }
            }
            else {
                cell.imageVw.image = UIImage(named: Const.appGrayLogo[indexPath.item])
            }
            cell.imageVw.tag = indexPath.item
        }
        else {
            cell.nameLbl.text = ""
            cell.imageVw.image = nil
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            return CGSize(width: 50, height: 70)
        }
        else if (self.view.frame.width == 375)
        {
            return CGSize(width: 55, height: 80)
        }
        else {
            return CGSize(width: 60, height: 85)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            return 11
        }
        else if (self.view.frame.width == 375)
        {
            return 15
        }
        else {
            return 18
        }
    }
    
 /*   func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if (indexPath.item > 10)
        {
            pagecontrol.currentPage = 1
        }
        else {
            pagecontrol.currentPage = 0
        }
    }
 */
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = appCollectionView.cellForItem(at: indexPath) as! AppCollectionCell
       
        if (indexPath.item <= 2) || (indexPath.item == 4) || (indexPath.item == 6) || (indexPath.item == 8)
        {
            if (cell.imageVw.image == UIImage(named: Const.appColorLogo[indexPath.item]))
            {
                indexValue = indexPath.item
                if (appTitleBtn.titleLabel?.text == Const.appNames[indexValue]) {
                    goToApps(str: (appTitleBtn.titleLabel?.text)!)
                }
                
                appImage.image = UIImage(named: Const.appColorLogo[indexValue])
                if (indexPath.item == 1)
                {
                    appImage.tintColor = ColorCode.FMeetDarkTheme
                }
                appTitleBtn.setTitle(Const.appNames[indexValue], for: .normal)
                appTitleBtn.backgroundColor = Const.appThemsColor[indexValue]
                appDescription.text = ApiResponse.getLanguageFromUserDefaults(inputString: Const.appDescription[indexValue])
                
                if (indexValue == 1) {
                    backButton.isEnabled = true
                    nextButton.isEnabled = false
                }
                else if (indexValue == 0) {
                    nextButton.isEnabled = true
                    backButton.isEnabled = false
                }
                else {
                    backButton.isEnabled = true
                    nextButton.isEnabled = true
                }
            }
            else {
//                indexValue = indexPath.item
//                appImage.image = UIImage(named: Const.appGrayLogo[indexValue])
//                appTitleBtn.setTitle(Const.appNames[indexValue], for: .normal)
//                appTitleBtn.backgroundColor = Const.appThemsColor[indexValue]
//                appDescription.text = ApiResponse.getLanguageFromUserDefaults(inputString: Const.appDescription[indexValue])
//
//                if (indexValue == 0) {
//                    backButton.isEnabled = false
//                    nextButton.isEnabled = true
//                }
//                else if (indexValue == Const.appNames.count-1) {
//                    nextButton.isEnabled = false
//                    backButton.isEnabled = true
//                }
//                else {
//                    backButton.isEnabled = true
//                    nextButton.isEnabled = true
//                }
            }
        }
    }
    
    func animationOnImageview(imgvw : UIImageView)
    {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
        transition.type = CATransitionType.fade
        imgvw.layer.add(transition, forKey: kCATransition)
    }
}

class AppCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageVw : UIImageView!
    @IBOutlet weak var nameLbl : UILabel!
}
