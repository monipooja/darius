

import UIKit
import SDWebImage
import SlideMenuControllerSwift
import FirebaseMessaging
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import MessageUI

class SideTableViewController : UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate  {
   
   static let identifier = String("hamburger")
    
   @IBOutlet weak var newvw: CustomUIView!
   @IBOutlet var tablevw: UITableView!
    
    var arr1 : [String] = []
    var imageArr1 : [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        
        let border = CALayer()
        border.backgroundColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x:0, y:newvw.frame.height, width:1, height:self.view.frame.height-newvw.frame.height)
        self.tablevw.layer.addSublayer(border)
        
        tablevw.separatorStyle = .none
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let tnc = ApiResponse.getLanguageFromUserDefaults(inputString: "fly_claim_footer_terms_and_conditions")
        let privacy = ApiResponse.getLanguageFromUserDefaults(inputString: "privacy_policy")
        let language = ApiResponse.getLanguageFromUserDefaults(inputString: "language")
        let setting = ApiResponse.getLanguageFromUserDefaults(inputString: "nav_setting")
        let changePwd = ApiResponse.getLanguageFromUserDefaults(inputString: "change_password")
        let contactus = ApiResponse.getLanguageFromUserDefaults(inputString: "nav_contact_us")
        let logout = ApiResponse.getLanguageFromUserDefaults(inputString: "logout")
        
        arr1 = [tnc, privacy, language, setting, changePwd, contactus, logout]
        imageArr1 = [ #imageLiteral(resourceName: "Terms"), #imageLiteral(resourceName: "Policy"), #imageLiteral(resourceName: "language"), #imageLiteral(resourceName: "settings"), #imageLiteral(resourceName: "ChangeKey"), #imageLiteral(resourceName: "contact-us"), #imageLiteral(resourceName: "Logout")]
        tablevw.reloadData()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr1.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tablevw.dequeueReusableCell(withIdentifier: "sidecell", for: indexPath)
        
        cell.textLabel?.textColor = UIColor.init(hexString: "686868")
        cell.textLabel?.font = UIFont(name: "ProximaNova-Semibold", size: 16)
        
        cell.textLabel?.text = arr1[indexPath.row]
        
        cell.imageView?.image = imageArr1[indexPath.row]
        var itemSize : CGSize
        var imageRect : CGRect
        if(indexPath.row == 0) {
            itemSize = CGSize(width : 25, height : 30);
            imageRect = CGRect(x : 3.0, y : 0.0, width : 22, height : itemSize.height);
        }
        else if (indexPath.row == 1)
        {
            itemSize = CGSize(width : 25, height : 27);
            imageRect = CGRect(x : 0.0, y : 0.0, width : itemSize.width, height : itemSize.height);
        }
        else if (indexPath.row == 2) || (indexPath.row == 3)
        {
            itemSize = CGSize(width : 27, height : 25);
            imageRect = CGRect(x : 2.0, y : 0.0, width : 25, height : itemSize.height);
        }
        else if (indexPath.row == 4)
        {
            itemSize = CGSize(width : 27, height : 25);
            imageRect = CGRect(x : 2.0, y : 0.0, width : 25, height : itemSize.height);
        }
        else if (indexPath.row == 5)
        {
            itemSize = CGSize(width : 27, height : 28);
            imageRect = CGRect(x : 2.0, y : 0.0, width : 25, height : itemSize.height);
        }
        else {
            itemSize = CGSize(width : 30, height : 21);
            imageRect = CGRect(x : 3.0, y : 0.0, width : 27, height : itemSize.height);
        }
        UIGraphicsBeginImageContextWithOptions(itemSize, false, 0.0)
        cell.imageView?.image!.draw(in: imageRect)
        cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext();
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch indexPath.row {
            
        case 0:
            openWebview = true
            policyOrTerms = "terms"
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            newvw.isHidden = true
            break
            
        case 1:
            openWebview = true
            policyOrTerms = "policy"
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            newvw.isHidden = true
            break
            
        case 2:
            let langvc = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "LanguageCurrencyVC") as! LanguageCurrencyVC
            langvc.checkStr = "language"
            present(langvc, animated: false, completion: nil)
            break
            
        case 3:
            let settingvc = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
            present(settingvc, animated: false, completion: nil)
            break
            
        case 4:
            let chnagevc = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            present(chnagevc, animated: false, completion: nil)
            break
            
        case 5:
            openMailTosend()
            break
            
        case 6:
            callLogoutService()
            break
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func callLogoutService()
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&user_id=\(user_id)"
        ApiResponse.onPostPhp(url: Api.Logout_URL, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation
            {
                customLoader.hideIndicator()
                userDefault.set(false, forKey: "session")
                userDefault.set(nil, forKey: "UserDetails")
                Access_token = ""
                userDefault.set("", forKey: "token")
                userDefault.set([], forKey: "unlock_albums")
                userDefault.setValue(nil, forKey: "buy_photos")
                userDefault.setValue(nil, forKey: "user_dict")
                userDefault.setValue(nil, forKey: "filter_dict")
                userDefault.synchronize()
                
                Messaging.messaging().unsubscribe(fromTopic: "\(user_id)")
                
                if let _  = AccessToken.current
                {
                    let fbLoginManager = LoginManager()
                    fbLoginManager.logOut()
                    AccessToken.current = nil
                    let cookies = HTTPCookieStorage.shared
                    let facebookCookies = cookies.cookies(for: URL(string: "https://facebook.com/")!)
                    for cookie in facebookCookies! {
                        cookies.deleteCookie(cookie)
                    }
                    let deletepermission = GraphRequest(graphPath: "me/permissions/", parameters: [:], httpMethod: HTTPMethod(rawValue: "DELETE"))
                    deletepermission.start(completionHandler: {(connection,result,error)-> Void in
                        print("the delete permission is \(String(describing: result))")
                    })
                }
                else {
                    GIDSignIn.sharedInstance()?.signOut()
                }
                
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.present(VC, animated: true, completion: nil)
            }
        }
    }
    
    func openMailTosend()
    {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
        else
        {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["info@dakefly.com"])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        StaticFunctions.showAlert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "could_not_send_mail"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "mail_not_send_msg"), actions: [OkText], controller: self, completion: { (str) in
            })
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    
        self.dismiss(animated: true, completion: nil)
        
        switch result {
            
        case MFMailComposeResult.sent:
            StaticFunctions.showAlert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "mail_sent"), actions: [OkText], controller: self, completion: { (str) in
            })
            break
            
        case MFMailComposeResult.cancelled:
            break
            
        case MFMailComposeResult.failed:
            StaticFunctions.showAlert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "mail_failed"), actions: [OkText], controller: self, completion: { (str) in
            })
            break
            
        case MFMailComposeResult.saved:
            StaticFunctions.showAlert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "mail_saved"), actions: [OkText], controller: self, completion: { (str) in
            })
            break
            
        default:
            break
        }
    }
    
    @IBAction func goToHome(_ sender: Any)
    {
        let homevc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        view.window!.layer.add(transition, forKey: kCATransition)
        present(homevc, animated: false, completion: nil)
    }
    
    // MARK : SIDE VIEW ACTIONS
    @IBAction func goToWallet(_ sender: Any)
    {
        let walletVC = storyboard!.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
        present(walletVC, animated: true, completion: nil)
    }
    @IBAction func goToNotification(_ sender: Any)
    {
        let notifyVC = storyboard!.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        present(notifyVC, animated: true, completion: nil)
    }
    @IBAction func goToProfile(_ sender: Any)
    {
        let profileVC = storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        present(profileVC, animated: true, completion: nil)
    }
    @IBAction func openRightSideBar(_ sender: Any)
    {
        let homevc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        view.window!.layer.add(transition, forKey: kCATransition)
        present(homevc, animated: false, completion: nil)
    }
}
