//
//  MyDDCell.swift
//  FlyDeli
//
//  Created by Apple on 05/02/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import DropDown

class MyDDCell: DropDownCell{

    @IBOutlet weak var flagImage: UIImageView!
  //  @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    
    @IBOutlet weak var flagWidth: NSLayoutConstraint!
    @IBOutlet weak var flagHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
