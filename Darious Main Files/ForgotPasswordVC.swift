//
//  ForgotPasswordVC.swift
//  Darious
//
//  Created by Apple on 08/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import DropDown

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var forgetLabel: UILabel!
    
    @IBOutlet weak var submitBtn: KGHighLightedButton!
    
    @IBOutlet weak var mobileNo: KGHighLightedField!
//    @IBOutlet weak var stdcodeBtn: UIButton!
    
    let dropDown = DropDown()
    var countryCode : NSNumber = 0
    
    let searchBar : UISearchBar = UISearchBar.init()
    var dataFiltered: [String] = []
    var filterCodeArray : [NSNumber] = []
    var filterIsoArray : [String] = []
    var nameArray : [String] = []
    var codeArray : [NSNumber] = []
    var isoArray : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        codeArray = userDefault.value(forKey: "code") as! [NSNumber]
        isoArray = userDefault.value(forKey: "iso") as! [String]
        
//        if(isoArray.contains(Locale.current.regionCode!)) {
//            let indx = self.isoArray.index(of: Locale.current.regionCode!)
//            stdcodeBtn.setTitle(" +\(codeArray[indx!])", for: .normal)
//            countryCode = codeArray[indx!]
//        }
//        stdcodeBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        
        nameArray = userDefault.value(forKey: "cname") as! [String]
        searchBarIntegration()
       
        setLanguage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        mobileNo.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "email")
        forgetLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "forget_password")
        submitBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "send_otp")), for: .normal)
    }
    
    // MARK : Search bar create
    func searchBarIntegration()
    {
        searchBar.frame = CGRect(x: 20, y: 40, width: self.view.frame.width-40, height:50)
        searchBar.delegate = self
        self.view.addSubview(searchBar)
        searchBar.isHidden = true
    }
    
    @IBAction func submitOTP(_ sender: Any)
    {
        if (mobileNo.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_email")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else if (ApiResponse.validateEmail(mobileNo.text!) == false) {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_valid_email")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&role=\(role)&email=\(mobileNo.text!)"
            forgotPassword(urlstr: Api.ForgotPwd_URL, parameters: params, check: "")
        }
    }
    
    @IBAction func getStdcode(_ sender: Any)
    {
        searchBar.isHidden = false
        
        mobileNo.resignFirstResponder()
        
        dropDown.anchorView = searchBar
        dropDown.dataSource = nameArray
        dropDown.direction = .bottom
        dropDown.dismissMode = .manual
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            let codestr = (self.isoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png"))
            cell.codeLabel.text = "+\(self.codeArray[index])"
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
//            self.stdcodeBtn.setTitle("+\(self.codeArray[index])", for: .normal)
//            self.countryCode = self.codeArray[index]
//            self.mobileNo.becomeFirstResponder()
//            self.searchBar.isHidden = true
//            self.searchBar.resignFirstResponder()
        }
        dropDown.width = searchBar.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
        
        searchBar.becomeFirstResponder()
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    func forgotPassword(urlstr : String, parameters : String, check : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation
                {
                    customLoader.hideIndicator()
                    let otpvc = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                    otpvc.mblNo = self.mobileNo.text!
                    otpvc.screenStr = "pwd"
                    self.present(otpvc, animated: true, completion: nil)
            }
        }
    }
}

extension ForgotPasswordVC : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dataFiltered = searchText.isEmpty ? nameArray : nameArray.filter({ (dat) -> Bool in
            dat.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        dropDown.dataSource = dataFiltered
        filterCodeArray = []
        filterIsoArray = []
        
        for str in dataFiltered {
            var i : Int = 0
            for str1 in nameArray {
                if (str == str1) {
                    filterCodeArray.append(codeArray[i])
                    filterIsoArray.append(isoArray[i])
                    break
                }
                i += 1
            }
        }
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            let codestr = (self.filterIsoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png"))
            cell.codeLabel.text = "+\(self.filterCodeArray[index])"
        }
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
//            self.stdcodeBtn.setTitle("+\(self.filterCodeArray[index])", for: .normal)
//            self.countryCode = self.filterCodeArray[index]
//            self.mobileNo.becomeFirstResponder()
//            self.searchBar.isHidden = true
//            self.searchBar.resignFirstResponder()
        }
        
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        dataFiltered = nameArray
        filterCodeArray = codeArray
        filterIsoArray = isoArray
        searchBar.isHidden = true
        dropDown.hide()
    }
}
