//
//  OTPViewController.swift
//  Darious
//
//  Created by Apple on 08/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController {

    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var codeLabel: UIButton!
    @IBOutlet weak var resendBtn: UIButton!
    @IBOutlet weak var confrimBtn: KGHighLightedButton!
    
    // TextFields
    @IBOutlet weak var textField_1: KGHighLightedField!
    @IBOutlet weak var textField_2: KGHighLightedField!
    @IBOutlet weak var textField_3: KGHighLightedField!
    @IBOutlet weak var textField_4: KGHighLightedField!
    
    var passDict : NSDictionary = [:]
    var otpStr : String = ""
    var mblNo : String = ""
    var stdcode : NSNumber = 0
    var screenStr : String = ""
    var mbl_data : String = ""
    
    var load : Bool = false
    var textFArray : [UITextField] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        load = true
        textField_1.becomeFirstResponder()
        
        textFArray = [textField_1, textField_2, textField_3, textField_4]
        for tf in textFArray {
            addBottomLayerWithColor(bgcolor: UIColor.lightGray, textF: tf)
        }
        addBottomLayerWithColor(bgcolor: UIColor.init(hexString: themeColor), textF: textField_1)
        
        setLanguage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(load == true) {
            load = false
        }else {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        msgLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "we_have_sent_otp")
        codeLabel.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "didn’t_receive_code")), for: .normal)
        resendBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "resend_otp")), for: .normal)
        confrimBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "confirm_otp")), for: .normal)
    }
    
    @IBAction func confirmOTP(_ sender: Any)
    {
        otpStr = "\(textField_1.text!)\(textField_2.text!)\(textField_3.text!)\(textField_4.text!)"
        if(otpStr == "") || (otpStr.count < 4) {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "invalid_otp")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else {
            if(screenStr == "reg")
            {
                let params = "device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&mobile=\(passDict["mobile"] as! String)&stdcode=\(passDict["stdcode"] as! String)&first_name=\(passDict["first_name"] as! String)&last_name=\(passDict["last_name"] as! String)&country_code=\(passDict["countrycode"] as! String)&password=\(passDict["password"] as! String)&cpassword=\(passDict["cpwd"] as! String)&otp=\(otpStr)&role=\(role)&nickname=\(passDict["nick_name"] as! String)&email=\(passDict["email"] as! String)&mobile_data=\(mbl_data)"
                callService(urlstr: Api.Registration_URL, parameters: params, check: "signup")
            }
            else {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&role=\(role)&otp=\(otpStr)&email=\(mblNo)"
                callService(urlstr: Api.VerifyOTP_URL, parameters: params, check: "verify")
            }
        }
    }
    
    @IBAction func resendOTP(_ sender: Any)
    {
        if(screenStr == "reg")
        {
            let params = "email=\(passDict["email"] as! String)&role=\(role)"
            callService(urlstr: Api.resent_otp, parameters: params, check: "resend")
        }
        else {
            let params = "email=\(mblNo)&role=\(role)"
            callService(urlstr: Api.forgot_resent_otp, parameters: params, check: "resend")
        }
    }
    
    @IBAction func onGoBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    //MARK : Add bottom layer to TextFields
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:1.5)
        }
        textF.layer.addSublayer(border)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation
                {
                    if(check == "resend") {
                        let msgStr = result["message"] as! String
                        let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                        ApiResponse.alert(title: "", message: showMsg, controller: self)
                    }
                    else if(check == "signup") {
                        customLoader.hideIndicator()
                        let loginVC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        self.present(loginVC, animated: true, completion: nil)
                    }
                    else {
                        customLoader.hideIndicator()
                        let changePwd = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                        changePwd.screenStr = "otp"
                        changePwd.mblNo = self.mblNo
                      //  changePwd.stdcode = self.stdcode
                        changePwd.otpStr = self.otpStr
                        self.present(changePwd, animated: true, completion: nil)
                    }
            }
        }
    }
}

extension OTPViewController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField_1.text?.count)! < 2) {
            
            if (textField_1.text != "") {
                if (textField_2.text != "") {
                    if (textField_3.text != "") {
                        if (string == "") {
                            if (textField_4.text == "") {
                                textField_3.text = ""
                                textField_2.becomeFirstResponder()
                            }else {
                                textField_4.text = ""
                                textField_3.becomeFirstResponder()
                            }
                        }
                        else {
                            textField_4.text = string
                            textField_4.becomeFirstResponder()
                        }
                    } else {
                        if (string == "") {
                            if (textField_3.text == "") {
                                textField_2.text = ""
                                textField_1.becomeFirstResponder()
                            }else {
                                textField_3.text = ""
                                textField_2.becomeFirstResponder()
                            }
                        }
                        else {
                            textField_3.text = string
                            textField_3.becomeFirstResponder()
                        }
                    }
                } else {
                    if (string == "") {
                        if (textField_2.text == "") {
                            textField_1.text = ""
                            textField_1.becomeFirstResponder()
                        }else {
                            textField_2.text = ""
                            textField_1.becomeFirstResponder()
                        }
                    }
                    else {
                        textField_2.text = string
                        textField_2.becomeFirstResponder()
                    }
                }
            }
            else {
                return true
            }
        }
        else {
            // print("texxxxx = \(string)")
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //  print("did begin..........")
        
        for tf in textFArray {
            if(tf == textField) {
                for sublyr in tf.layer.sublayers!
                {
                    if(sublyr.backgroundColor == UIColor.lightGray.cgColor){
                        sublyr.backgroundColor = UIColor.init(hexString: themeColor).cgColor
                        sublyr.frame.size.height = 1.5
                    }
                }
            }
            else {
                for sublyr in tf.layer.sublayers!
                {
                    if(sublyr.backgroundColor == UIColor.lightGray.cgColor) || (sublyr.backgroundColor == UIColor.init(hexString: themeColor).cgColor) {
                        sublyr.backgroundColor = UIColor.lightGray.cgColor
                        sublyr.frame.size.height = 0.5
                    }
                }
            }
        }
    }
}
