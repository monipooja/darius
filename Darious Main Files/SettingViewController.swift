//
//  SettingViewController.swift
//  Darious
//
//  Created by Apple on 23/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import LocalAuthentication

class SettingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var newvw: CustomUIView!
    @IBOutlet var tablevw: UITableView!
    
    var switchArr : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        
        let border = CALayer()
        border.backgroundColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x:0, y:newvw.frame.height, width:1, height:self.view.frame.height-newvw.frame.height)
        self.tablevw.layer.addSublayer(border)
        
        tablevw.separatorStyle = .none
        
        switchArr = userDefault.value(forKey: "onoff") as! [String]
        tablevw.dataSource = self
        tablevw.delegate = self
        tablevw.reloadData()
        
        newvw.isHidden = true
        tablevw.isHidden = true
        
        DispatchQueue.main.async {
            self.authenticateUser()
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func goToHome(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK : SIDE VIEW ACTIONS
    @IBAction func goToWallet(_ sender: Any)
    {
        let walletVC = storyboard!.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
        present(walletVC, animated: true, completion: nil)
    }
    @IBAction func goToNotification(_ sender: Any)
    {
        let notifyVC = storyboard!.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        present(notifyVC, animated: true, completion: nil)
    }
    @IBAction func goToProfile(_ sender: Any)
    {
        let profileVC = storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        present(profileVC, animated: true, completion: nil)
    }
    @IBAction func openRightSideBar(_ sender: Any)
    {
        let homevc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        view.window!.layer.add(transition, forKey: kCATransition)
        present(homevc, animated: false, completion: nil)
    }
    
    // MAKR : Touch ID Authentication
    func authenticateUser()
    {
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [unowned self] success, authenticationError in
                
                DispatchQueue.main.async {
                    if success {
                        self.newvw.isHidden = false
                        self.tablevw.isHidden = false
                    } else {
                        StaticFunctions.showAlert(title: "Authentication failed", message: "", actions: ["OK"], controller: self) { (str) in
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
        } else {
            
            if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error)
            {
                let reason = "Use Passcode"
                
                context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) {
                    [unowned self] success, authenticationError in
                    
                    DispatchQueue.main.async {
                        if success {
                            self.newvw.isHidden = false
                            self.tablevw.isHidden = false
                        } else {
                            StaticFunctions.showAlert(title: "Authentication failed", message: "", actions: ["OK"], controller: self) { (str) in
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK : TableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return Const.appNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tablevw.dequeueReusableCell(withIdentifier: "settingcell", for: indexPath) as! SettingTableCell
        
        cell.imgView.image = UIImage(named: Const.appColorLogo[indexPath.section])
        cell.nameLabel.text = Const.appNames[indexPath.section]
        
        if (switchArr[indexPath.section] == "on")
        {
            cell.onOffSwitch.isOn = true
        }
        else {
            cell.onOffSwitch.isOn = false
        }
        cell.onOffSwitch.addTarget(self, action: #selector(onOffApps(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 77
    }
    
    @objc func onOffApps(_ sender : UISwitch)
    {
        let point = sender.convert(CGPoint.zero, to: tablevw)
        let idxpath = tablevw.indexPathForRow(at: point)!
        
        if (sender.isOn == true){
            switchArr[idxpath.section] = "on"
        }
        else {
            switchArr[idxpath.section] = "off"
        }
        userDefault.setValue(switchArr, forKey: "onoff")
        userDefault.synchronize()
    }
}

class SettingTableCell: UITableViewCell {
    
    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var onOffSwitch : UISwitch!
}
