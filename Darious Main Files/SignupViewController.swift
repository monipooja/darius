//
//  SignupViewController.swift
//  Darious
//
//  Created by Apple on 07/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage
import CoreTelephony
import Contacts

class SignupViewController: UIViewController {

    @IBOutlet weak var signupBtn: KGHighLightedButton!
    @IBOutlet weak var accLabel: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var createdLabel: UILabel!
    
    // TextFields
    @IBOutlet weak var firstnameTF: KGHighLightedField!
    @IBOutlet weak var lastnameTF: KGHighLightedField!
    @IBOutlet weak var nicknameTF: KGHighLightedField!
    @IBOutlet weak var emailTF: KGHighLightedField!
    @IBOutlet weak var mobileNoTF: KGHighLightedField!
    @IBOutlet weak var passwordTF: KGHighLightedField!
    @IBOutlet weak var confirmPwdTF: KGHighLightedField!
    
    @IBOutlet weak var contentVw: UIView!
    @IBOutlet weak var contentHt: NSLayoutConstraint!
    
    @IBOutlet weak var stdCodeBtn: UIButton!

    var err_imgvw : UIImageView?
    let dropDown = DropDown()
    var countryCode : NSNumber = 0
    var isoStr : String = ""
    var dataDict : NSDictionary = [:]
    var keybaordOpen : String = ""
    var checkAgree : Bool = false
    
    let searchBar : UISearchBar = UISearchBar.init()
    var dataFiltered: [String] = []
    var filterCodeArray : [NSNumber] = []
    var filterIsoArray : [String] = []
    var nameArray : [String] = []
    var codeArray : [NSNumber] = []
    var isoArray : [String] = []
    var dataarr : [NSDictionary] = []
    var userType : [String] = []
    var textFArray : [UITextField] = []
    
    var sendstr = "[name] "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        searchBarIntegration()
        
        textFArray = [firstnameTF, lastnameTF, nicknameTF, mobileNoTF, passwordTF, confirmPwdTF]

        codeArray = userDefault.value(forKey: "code") as! [NSNumber]
        isoArray = userDefault.value(forKey: "iso") as! [String]
        nameArray = userDefault.value(forKey: "cname") as! [String]
        
        if(isoArray.contains(Locale.current.regionCode!)) {
            let indx = self.isoArray.index(of: Locale.current.regionCode!)
            stdCodeBtn.setTitle(" +\(codeArray[indx!])", for: .normal)
            stdCodeBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 2, bottom: 0, right: 5)
            countryCode = codeArray[indx!]
            isoStr = isoArray[indx!]
        }
        setLanguage()
        fetchContacts()
        
        err_imgvw = UIImageView.init()
        err_imgvw?.frame.size = CGSize(width: 20, height: 20)
        err_imgvw?.image = #imageLiteral(resourceName: "Error-Icon")
        contentVw.addSubview(err_imgvw!)
        err_imgvw?.isHidden = true
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction(sender:)))
        contentVw.addGestureRecognizer(gesture)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        contentHt.constant = 680
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        contentVw.endEditing(true)
        contentHt.constant = 680
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        createdLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "this_is_social_platform")
        accLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "have_an_account") + "?"
        firstnameTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "first_name")
        lastnameTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "last_name")
        nicknameTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "nick_name")
        emailTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "email")
        mobileNoTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "mobile_no")
        passwordTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "password")
        confirmPwdTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "confirm_password")
        
        loginBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "login")), for: .normal)
        signupBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "signup")), for: .normal)
    }
    
    // MARK : Search bar create
    func searchBarIntegration()
    {
        searchBar.frame = CGRect(x: 20, y: 40, width: self.view.frame.width-40, height:50)
        searchBar.delegate = self
        searchBar.tag = 100
        self.view.addSubview(searchBar)
        searchBar.isHidden = true
        keybaordOpen = "no"
    }
    
    @IBAction func onSignup(_ sender: Any)
    {
        if(firstnameTF.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_first_name")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
            err_imgvw?.frame.origin.x = (firstnameTF.frame.origin.x + firstnameTF.bounds.width) - 30
            err_imgvw?.frame.origin.y = firstnameTF.frame.origin.y + 10
            err_imgvw?.isHidden = false
        }
        else if(lastnameTF.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_last_name")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
            err_imgvw?.frame.origin.x = (lastnameTF.frame.origin.x + lastnameTF.bounds.width) - 30
            err_imgvw?.frame.origin.y = lastnameTF.frame.origin.y + 10
            err_imgvw?.isHidden = false
        }
        else if(nicknameTF.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_nick_name")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
            err_imgvw?.frame.origin.x = (nicknameTF.frame.origin.x + nicknameTF.bounds.width) - 30
            err_imgvw?.frame.origin.y = nicknameTF.frame.origin.y + 10
            err_imgvw?.isHidden = false
        }
        else if(emailTF.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_email")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
            err_imgvw?.frame.origin.x = (emailTF.frame.origin.x + emailTF.bounds.width) - 30
            err_imgvw?.frame.origin.y = emailTF.frame.origin.y + 10
            err_imgvw?.isHidden = false
        }
        else if (ApiResponse.validateEmail(emailTF.text!) == false) {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_valid_email")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else  if(mobileNoTF.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_mobile_no")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
            err_imgvw?.frame.origin.x = (mobileNoTF.frame.origin.x + mobileNoTF.bounds.width) - 30
            err_imgvw?.frame.origin.y = mobileNoTF.frame.origin.y + 10
            err_imgvw?.isHidden = false
        }
        else if(passwordTF.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_password")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
            err_imgvw?.frame.origin.x = (passwordTF.frame.origin.x + passwordTF.bounds.width) - 30
            err_imgvw?.frame.origin.y = passwordTF.frame.origin.y + 10
            err_imgvw?.isHidden = false
        }
        else if(confirmPwdTF.text == "") {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_confirm_password")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
            err_imgvw?.frame.origin.x = (confirmPwdTF.frame.origin.x + confirmPwdTF.bounds.width) - 30
            err_imgvw?.frame.origin.y = confirmPwdTF.frame.origin.y + 10
            err_imgvw?.isHidden = false
        }
        else if(passwordTF.text != confirmPwdTF.text) {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "confirm_pass_new_pass_not_match")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
//        else if(checkAgree == false) {
//            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_select_terms_and_condition")
//            ApiResponse.alert(title: "", message: showMsg, controller: self)
//        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
            let mblno = (mobileNoTF.text)!.stringByStrippingHTMLTags
            dataDict = ["first_name" :"\(firstnameTF.text!)","last_name" : "\(lastnameTF.text!)","nick_name" : "\(nicknameTF.text!)", "mobile" : mblno,"stdcode" : "\(countryCode)", "countrycode" : isoStr, "password" : passwordTF.text!, "cpwd" : confirmPwdTF.text!, "email" : emailTF.text!]
            let params = "email=\(emailTF.text!)&role=\(role)&nickname=\(nicknameTF.text!)"
            callService(urlstr: Api.OTP_Registration_URL, parameters: params, check: "signup")
        }
    }
    
    @IBAction func openStdCode(_ sender: Any)
    {
        searchBar.isHidden = false
    
        dropDown.anchorView = searchBar
        dropDown.dataSource = nameArray
        dropDown.direction = .bottom
        dropDown.dismissMode = .manual
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            let codestr = (self.isoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png"))
            cell.codeLabel.text = "+\(self.codeArray[index])"
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            self.stdCodeBtn.setTitle("+\(self.codeArray[index])", for: .normal)
            self.countryCode = self.codeArray[index]
            self.isoStr = self.isoArray[index]
            self.mobileNoTF.becomeFirstResponder()
            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
            self.keybaordOpen = ""
        }
        dropDown.width = mobileNoTF.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
        
        searchBar.becomeFirstResponder()
    }
    
    @IBAction func onGoBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation
            {
                if (check == "signup") {
                    let otpvc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                    otpvc.passDict = self.dataDict
                    otpvc.screenStr = "reg"
                    otpvc.mbl_data = self.sendstr
                    self.present(otpvc, animated: true, completion: nil)
                }
                else {
                }
            }
        }
    }
    
    // Fetch Contacts
    func fetchContacts()
    {
        let contactStore = CNContactStore()
        var contacts = [CNContact]()
        var phonenumbers = [String]()
        let keys = [CNContactPhoneNumbersKey]
        let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
        
        do {
            try contactStore.enumerateContacts(with: request) {
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                contacts.append(contact)
                for ContctNumVar: CNLabeledValue in contact.phoneNumbers {
                    if let fulMobNumVar  = ContctNumVar.value as? CNPhoneNumber {
                        if let MccNamVar = fulMobNumVar.value(forKey: "digits") as? String {
                            phonenumbers.append(MccNamVar.replacingOccurrences(of: "+", with: ""))
                        }
                    }
                }
            }
        }
        catch {
            // print("unable to fetch contacts")
            sendstr = ""
        }
        for nm in phonenumbers
        {
            if (sendstr == "[name] ") {
                sendstr = sendstr + "'%\(nm)'"
            }
            else {
                sendstr = sendstr + " or " + "[name] '%\(nm)'"
            }
        }
    }
}

extension SignupViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        contentHt.constant = 680
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (contentHt.constant == 680) {
            contentHt.constant = 800
        }
        err_imgvw?.isHidden = true
    }
}

extension SignupViewController : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dataFiltered = searchText.isEmpty ? nameArray : nameArray.filter({ (dat) -> Bool in
            dat.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        dropDown.dataSource = dataFiltered
        filterCodeArray = []
        filterIsoArray = []
        
        for str in dataFiltered {
            var i : Int = 0
            for str1 in nameArray {
                if (str == str1) {
                    filterCodeArray.append(codeArray[i])
                    filterIsoArray.append(isoArray[i])
                    break
                }
                i += 1
            }
        }
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            let codestr = (self.filterIsoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png"))
            cell.codeLabel.text = "+\(self.filterCodeArray[index])"
        }
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            self.stdCodeBtn.setTitle("+\(self.filterCodeArray[index])", for: .normal)
            self.countryCode = self.filterCodeArray[index]
            self.isoStr = self.filterIsoArray[index]
            self.mobileNoTF.becomeFirstResponder()
            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
            self.keybaordOpen = ""
        }
        
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        dataFiltered = nameArray
        filterCodeArray = codeArray
        filterIsoArray = isoArray
        searchBar.isHidden = true
        dropDown.hide()
    }
}

