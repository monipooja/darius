//
//  ViewControllerPannable.swift
//  Darious
//
//  Created by Apple on 31/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class ViewControllerPannable: UIViewController {
    var panGestureRecognizer: UIPanGestureRecognizer?
    var originalPosition: CGPoint?
    var currentPositionTouched: CGPoint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(_:)))
        view.addGestureRecognizer(panGestureRecognizer!)
    }
    
    // MARK : Pan gesture to dismiss the view
    @objc func panGestureAction(_ panGesture: UIPanGestureRecognizer) {
        let translation = panGesture.translation(in: view)
        
        if panGesture.state == .began {
            originalPosition = view.center
            currentPositionTouched = panGesture.location(in: view)
        } else if panGesture.state == .changed {
            //            view.frame.origin = CGPoint(
            //                x: translation.x,
            //                y: translation.y
            //            )
            view.frame.origin.y = translation.y
            view.window?.backgroundColor = UIColor.white
        } else if panGesture.state == .ended {
            let velocity = panGesture.velocity(in: view)
            
            if velocity.y >= 200 {
                UIView.animate(withDuration: 0.8
                    , animations: {
                        //                        self.view.frame.origin = CGPoint(
                        //                            x: self.view.frame.origin.x,
                        //                            y: self.view.frame.size.height
                        //                        )
                        self.view.frame.origin.y = translation.y
                }, completion: { (isCompleted) in
                    if isCompleted {
                        self.dismiss(animated: false, completion: nil)
                    }
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.center = self.originalPosition!
                })
            }
        }
    }
}
