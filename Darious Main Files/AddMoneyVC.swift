//
//  AddMoneyVC.swift
//  Darious
//
//  Created by Apple on 24/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class AddMoneyVC: UIViewController {

    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var addBtn: KGHighLightedButton!
    
    // Center view outlets
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var centerView: CustomUIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var commissionLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var subtotalValue: UILabel!
    @IBOutlet weak var commissionValue: UILabel!
    @IBOutlet weak var totalValue: UILabel!
    @IBOutlet weak var continueBtn: KGHighLightedButton!
    @IBOutlet weak var currencyLbl: UILabel!
    
    @IBOutlet weak var centerErrorVw: CustomUIView!
    @IBOutlet weak var failedLabel: UILabel!
    @IBOutlet weak var successLabel: UILabel!
    @IBOutlet weak var statusTitleLabel: UILabel!
    @IBOutlet weak var handImage: UIImageView!
    
    @IBOutlet var priceBtnCollection: [KGHighLightedButton]!
    
    var total : Float = 0
    var transactionId : String = ""
    var total_comsn : Float = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        setLanguage()
        currencyLbl.text = Current_Currency
        
        let border = CALayer()
        border.backgroundColor = UIColor.init(hexString: themeColor).cgColor
        border.frame = CGRect(x:0, y:amountTF.frame.height+1, width:amountTF.frame.width, height:1)
        amountTF.layer.addSublayer(border)
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        blurView.addGestureRecognizer(gesture)

        for btn in priceBtnCollection
        {
            btn.setTitle((btn.titleLabel?.text)! + " \(Current_Currency)", for: .normal)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.view.frame.origin.y = 0
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "add_money")
        amount.text = ApiResponse.getLanguageFromUserDefaults(inputString: "amount")
        
        titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "card_method")
        subtotalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "subtotal")
        totalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "total")
        commissionLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "commission")
        successLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "money_has_been_added")
        failedLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "payment_not_done")
        
        addBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "add_money")), for: .normal)
        continueBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (transactionId != ""){
            ApiResponse.onResponseGetMollie(url: "https://api.mollie.com/v2/payments/\(transactionId)") { (result, error) in
                OperationQueue.main.addOperation {
                    customLoader.hideIndicator()
                    if (error == "")
                    {
                        //print("mollie trans result = \(result)")
                        let status = "\(result["status"] ?? "")"
                        self.blurView.alpha = 0.9
                        self.blurView.backgroundColor = UIColor.init(hexString: "919191")
                        self.transactionId = ""
                        if status == "canceled" || status == "failed" || status == "open" {
                            self.blurView.isHidden = false
                            self.statusTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "error")
                            self.failedLabel.isHidden = false
                            self.successLabel.isHidden = true
                            self.handImage.image = #imageLiteral(resourceName: "CancelCross")
                            self.centerErrorVw.isHidden = false
                        } else {
                            if (status == "paid") {
                                self.blurView.isHidden = false
                                self.statusTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "common_done_ios")
                                self.failedLabel.isHidden = true
                                self.successLabel.isHidden = false
                                self.handImage.image = #imageLiteral(resourceName: "SuccessChecked")
                                self.centerErrorVw.isHidden = false
                            }
                        }
                    }
                    else{}
                }
            }
        }
    }
    
    // Mark : Gesture Action
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        self.view.endEditing(true)
        self.view.frame.origin.y = 0
        blurView.isHidden = true
        centerView.isHidden = true
        if (centerErrorVw.isHidden == false)
        {
            centerErrorVw.isHidden = true
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func selectTenPrice(_ sender: Any)
    {
        amountTF.text = "10.00"
        setButtonsViews(selectedBtn: (sender as! UIButton))
    }
    
    @IBAction func selectTwentyfivePrice(_ sender: Any)
    {
        amountTF.text = "25.00"
        setButtonsViews(selectedBtn: (sender as! UIButton))
    }
    
    @IBAction func selectFiftyPrice(_ sender: Any)
    {
        amountTF.text = "50.00"
        setButtonsViews(selectedBtn: (sender as! UIButton))
    }
    
    @IBAction func selectHundreadPrice(_ sender: Any)
    {
        amountTF.text = "100.00"
        setButtonsViews(selectedBtn: (sender as! UIButton))
    }
    
    @IBAction func selectTwohundreadPrice(_ sender: Any)
    {
        amountTF.text = "200.00"
        setButtonsViews(selectedBtn: (sender as! UIButton))
    }
    
    func setButtonsViews(selectedBtn : UIButton)
    {
        for btn in priceBtnCollection
        {
            btn.layer.borderColor = UIColor.init(hexString: ColorCode.progressColor).cgColor
            btn.layer.borderWidth = 1
            btn.backgroundColor = UIColor.white
            btn.setTitleColor(UIColor.init(hexString: ColorCode.progressColor), for: .normal)
        }
        selectedBtn.backgroundColor = UIColor.init(hexString: ColorCode.progressColor)
        selectedBtn.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func onAddMoney(_ sender: Any)
    {
        if (amountTF.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "enter_amount_here"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else
        {
            view.endEditing(true)
            blurView.isHidden = false
            centerView.isHidden = false
            
            if (amountTF.text!.contains(".") == false) && (amountTF.text!.contains(",") == false)
            {
                subtotalValue.text = "\(amountTF.text!).0 \(Current_Currency)"
            }
            else {
                if (amountTF.text!.contains(".") == true)
                {
                    let sp = amountTF.text?.split(separator: ".")
                    if (sp?.count == 1)
                    {
                        subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: amountTF.text!) + "0 \(Current_Currency)"
                    }
                    else {
                        subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: amountTF.text!) + " \(Current_Currency)"
                    }
                }
                else {
                    let sp = amountTF.text?.split(separator: ",")
                    if (sp?.count == 1)
                    {
                        subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: amountTF.text!) + "0 \(Current_Currency)"
                    }
                    else {
                        subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: amountTF.text!) + " \(Current_Currency)"
                    }
                }
            }

            if ((amountTF.text?.contains(","))!) {
                let amtstr = amountTF.text?.replacingOccurrences(of: ",", with: ".")
                total_comsn = ((Float(amtstr!)!) * (Float(commission_value)!/100)) + Float(commission_Card)!
                total = Float(amtstr!)! + total_comsn
                total = Float(String(format: "%.2f", total))!
            }
            else {
                let amountStr = "\(self.amountTF.text!)"
                total_comsn = ((Float(amountStr)!) * (Float(commission_value)!/100)) + Float(commission_Card)!
                total = Float(amountStr)! + total_comsn
                total = Float(String(format: "%.2f", total))!
            }
            commissionValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", total_comsn)) + " \(Current_Currency)"
            totalValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", total)) + " \(Current_Currency)"
        }
    }
    
    @IBAction func onContinue(_ sender: Any)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
        let aa = "\(total)".split(separator: ".")
       
        if (aa[1].count == 1) {
            self.molliePayment(payment: "\(total)0", cid: "")
        }
        else {
            self.molliePayment(payment: "\(total)", cid: "")
        }
    }
    
    // Mollie Payment Service Call
    func molliePayment(payment : String, cid : String)
    {
        let amountDict : NSDictionary = ["currency" : "EUR", "value" : payment]
        let date = Date()
        let formatter = DateFormatter.init()
        formatter.dateFormat = "yyyyMMdd"
        let datestr = formatter.string(from: date)
        let metaDict : NSDictionary = ["uinique_id" : "\(datestr)\(user_id)", "commission" : "\(total_comsn)"]
        
        let params : [String : Any] = ["metadata" : metaDict, "amount" : amountDict, "description" : "\(ApiResponse.getLanguageFromUserDefaults(inputString: "subscription_business"))", "customerId" : "\(cid)", "redirectUrl" : "http://www.myapp.com/", "webhookUrl" : "\(Api.Base_URL)flycliam/add_money_wallet_webhook/\(user_id)"]
       
        ApiResponse.onResponsePostMollie2(url: "https://api.mollie.com/v2/payments", parms: params) { (result, error) in
            // print("mollie payment result = \(result)")
            
            OperationQueue.main.addOperation {
                if let _ = result["_links"] as? NSDictionary {
                    
                    let checkoutUrl = ((result["_links"] as! NSDictionary)["checkout"] as! NSDictionary)["href"] as! String
                    self.transactionId = result["id"] as! String
                    
                    customLoader.hideIndicator()
                    self.centerView.isHidden = true
                    let webvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "MollieWebviewVC") as! MollieWebviewVC
                    webvc.checkouturl = checkoutUrl
                    webvc.transid = result["id"] as! String
                    webvc.comeFrom = "addmoney"
                    self.present(webvc, animated: false, completion: nil)
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    @IBAction func onhideView(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
    }
    
    @IBAction func onHideErrorView(_ sender: Any)
    {
        blurView.isHidden = true
        centerErrorVw.isHidden = true
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
}

extension AddMoneyVC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            self.view.frame.origin.y -= 100
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        for btn in priceBtnCollection
        {
            btn.layer.borderColor = UIColor.init(hexString: ColorCode.progressColor).cgColor
            btn.layer.borderWidth = 1
            btn.backgroundColor = UIColor.white
            btn.setTitleColor(UIColor.init(hexString: ColorCode.progressColor), for: .normal)
        }
        return true
    }
}
