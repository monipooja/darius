//
//  TermsConditionVC.swift
//  Darious
//
//  Created by Apple on 24/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import WebKit

class TermsConditionVC: UIViewController, WKNavigationDelegate, WKUIDelegate {

    @IBOutlet weak var navBar : UINavigationBar!
    
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
      //  navBar.topItem?.title = ApiResponse.getLanguageFromUserDefaults(inputString: "fly_claim_footer_terms_and_conditions")
        
        LoadWkWebView()
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func LoadWkWebView()
    {
        webView = WKWebView.init(frame: CGRect(x: 0, y: 64, width: view.bounds.width, height: view.bounds.height-64))
        
        customLoader.showActivityIndicator(showColor: ColorCode.themeColor, controller: webView)
        
        let url = URL(string: "https://flydarius.com/term-and-condition")!
        webView.load(URLRequest(url: url))
        
        webView.navigationDelegate = self
        webView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        view.addSubview(webView)
        webView.allowsBackForwardNavigationGestures = true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        customLoader.hideIndicator()
    }

    @IBAction func onBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
}
