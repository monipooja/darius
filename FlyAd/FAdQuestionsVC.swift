//
//  FAdQuestionsVC.swift
//  Darious
//
//  Created by Apple on 07/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class FAdQuestionsVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var qsnTableView : UITableView!
    
    var qsnDict : NSDictionary = [:]
    var select_ans : String = "option_one"
    var adid : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FAdThemeColor
        btmImageView.tintColor = ColorCode.FAdThemeColor
        topHeaderImgVw.tintColor = ColorCode.FAdThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&ad_id=\(self.adid)"
            self.callService(urlstr: Api.Fad_Question, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickToEarn(_ sender: Any)
    {
        if (select_ans == "")
        {
            ApiResponse.alert(title: "", message: "Please select answer", controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&id=\(qsnDict["id"] as! NSNumber)&ad_id=\(adid)&answer=\(select_ans)"
            self.callService(urlstr: Api.Fad_Answer, parameters: params, check: "ans")
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdHomeVC") as! FAdHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                // print("search result = \(result)")
                if (check == "get") {
                    let data = result["data"] as! NSDictionary
                    self.qsnDict = data
                    self.qsnTableView.reloadData()
                    customLoader.hideIndicator()
                }
                else {
                    customLoader.hideIndicator()
                    let nextvc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdSuccessVC") as! FAdSuccessVC
                    nextvc.earnPrice = "0"
                    self.present(nextvc, animated: false, completion: nil)
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyadBottomButtonAction(tagValue: sender.tag, controller: self)
    }
    
}

/// MAKR : CollectionView Methods
extension FAdQuestionsVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = qsnTableView.dequeueReusableCell(withIdentifier: "fadquestioncell") as! FAdQuestionCell
        
        cell.qsnLabel.text = qsnDict["question"] as? String
        cell.firstLabel.text = qsnDict["option_one"] as? String
        cell.secondLabel.text = qsnDict["option_two"] as? String
        cell.thirdLabel.text = qsnDict["option_three"] as? String
        cell.fourthLabel.text = qsnDict["option_four"] as? String
        
        for btn in cell.selectBtns
        {
            btn.addTarget(self, action: #selector(selectAnswer(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: qsnTableView.frame.width, height: 10))
        vw.backgroundColor = UIColor.clear
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func selectAnswer(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: qsnTableView)
        let idxPath = qsnTableView.indexPathForRow(at: point)!
        let cell = qsnTableView.cellForRow(at: idxPath) as! FAdQuestionCell
     
        switch sender.tag
        {
            case 1:
                select_ans = "option_one"
            break;
            
            case 2:
                select_ans = "option_two"
            break;
            
            case 3:
                select_ans = "option_three"
            break;
            
            case 4:
                select_ans = "option_four"
            break;
            
            default:
            break
        }
        
        for btn in cell.selectBtns
        {
            if (btn.tag == sender.tag)
            {
                btn.setImage(#imageLiteral(resourceName: "Fad_select_circle"), for: .normal)
            }
            else {
                btn.setImage(nil, for: .normal)
            }
        }
    }
}

class FAdQuestionCell: UITableViewCell {
    
    @IBOutlet weak var qsnLabel : UILabel!
    @IBOutlet weak var firstLabel : UILabel!
    @IBOutlet weak var secondLabel : UILabel!
    @IBOutlet weak var thirdLabel : UILabel!
    @IBOutlet weak var fourthLabel : UILabel!
    @IBOutlet var selectBtns : [UIButton]!
}
