//
//  FAdVideoVC.swift
//  Darious
//
//  Created by Apple on 21/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class FAdVideoVC: UIViewController {

    var playerVw : PlayerView!
    var vId : String = ""
    var label : UILabel!
    var sliderCurrentVlaue : Float = 0
    var videoLoader : UIActivityIndicatorView!
    var adid : String = ""
    var totalTime : String = ""
    var service : Bool = false
    
    var timer : Timer!
    
    override func viewDidLoad() {
        UIApplication.shared.statusBarView?.isHidden = false
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .landscape
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(callViewAdService), userInfo: nil, repeats: true)
        
        setVideoLoader()
        setMMPlayer(videoid: vId)
    }
    
    func setMMPlayer(videoid : String)
    {
        playerVw = UINib(nibName: "PlayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PlayerView
        playerVw.videoId = videoid
        addPlayerView()
    }
    
    private func addPlayerView(){
        self.view.addSubview(playerVw)
        playerVw.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        //   playerVw.autoresizingMask = .flexibleWidth
        videoLoader.stopAnimating()
        playerVw.timeSlider.value = sliderCurrentVlaue
        playerVw.updateTime()
        playerVw.btnFullScreen.addTarget(self, action: #selector(exitFullScreen(_:)), for: .touchUpInside)
    }
    
    func setVideoLoader()
    {
        videoLoader = UIActivityIndicatorView.init()
        videoLoader.frame.size = CGSize(width: 25, height: 25)
        videoLoader.center = self.view.center
        videoLoader.hidesWhenStopped = true
        self.view.addSubview(videoLoader)
        videoLoader.isHidden = true
    }
    
    @objc func exitFullScreen(_ sender : UIButton)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .portrait
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func callViewAdService()
    {
        let duration = playerVw.ytPlayerView.duration
        let value = playerVw.ytk_secondsToCounter(Int(duration))
       
        if (totalTime == "") && (value != "00:00"){
            let ff = Float.init(value.replacingOccurrences(of: ":", with: "."))
            totalTime = String(format: "%.2f", (ff! - 0.01))
        }
        
        if (playerVw.ytPlayerView.currentTime > 10) && (adid != "")
        {
            let aa = playerVw.ytk_secondsToCounter(Int(playerVw.ytPlayerView.currentTime))

          //  print("float = \(Float.init(totalTime.replacingOccurrences(of: ":", with: ".")))----------\(Float.init(aa.replacingOccurrences(of: ":", with: ".")))")
            if (Float.init(aa.replacingOccurrences(of: ":", with: "."))! >= Float.init(totalTime.replacingOccurrences(of: ":", with: "."))!)
            {
                self.timer.invalidate()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.myOrientation = .portrait
                let qsnvc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdQuestionsVC") as! FAdQuestionsVC
                qsnvc.adid = self.adid
                present(qsnvc, animated: false, completion: nil)
            }
            
            if (service == false)
            {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&ad_id=\(adid)&ad_type=view"
                self.callService(urlstr: Api.Fad_View, parameters: params, check: "get")
            }
        }
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                self.service = true
            }
        }
    }
}
