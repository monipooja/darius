//
//  FlyadSplashVC.swift
//  Darious
//
//  Created by Apple on 06/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class FlyadSplashVC: UIViewController {
    
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = true
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(gotoNext), userInfo: nil, repeats: false)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func gotoNext()
    {
        timer.invalidate()
        let homevc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdHomeVC") as! FAdHomeVC
        present(homevc, animated: false, completion: nil)
    }
}

// Mark : Get video id from You tube URL
public func getYoutubeId(youtubeUrl: String) -> String? {
    return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
}

    
public func flyadBottomButtonAction(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 1:
        // New Ads- home
        let nextvc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdHomeVC") as! FAdHomeVC
        controller.present(nextvc, animated: false, completion: nil)
        break
    case 2:
        // Search Ads
        let nextvc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdSearchVC") as! FAdSearchVC
        controller.present(nextvc, animated: false, completion: nil)
        break
    case 3:
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
        break
    case 4:
        // Viewed Ads
        let nextvc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdViewedAdsVC") as! FAdViewedAdsVC
        controller.present(nextvc, animated: false, completion: nil)
        break
    case 5:
        // Total Earnings
        let nextvc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdEarningsVC") as! FAdEarningsVC
        controller.present(nextvc, animated: false, completion: nil)
        break
    default:
        break
    }
}
    

