//
//  FAdFilterVC.swift
//  Darious
//
//  Created by Apple on 07/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class FAdFilterVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!

    @IBOutlet weak var categoryTableView : UITableView!
    
    var dataArray : [NSDictionary] = []

    var pass_catgid : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FAdThemeColor
        btmImageView.tintColor = ColorCode.FAdThemeColor
        topHeaderImgVw.tintColor = ColorCode.FAdThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.Fad_Category, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func onApply(_ sender: Any)
    {
        let searchvc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdSearchVC") as! FAdSearchVC
        searchvc.category_id = pass_catgid
        present(searchvc, animated: false, completion: nil)
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdHomeVC") as! FAdHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                // print("search result = \(result)")
                if (check == "get") {
                    let data = result["category"] as! [NSDictionary]
                    if (data.count > 0)
                    {
                        self.dataArray = data
                        self.categoryTableView.reloadData()
                    }
                    else {
                        self.categoryTableView.isHidden = true
                    }
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyadBottomButtonAction(tagValue: sender.tag, controller: self)
    }
    
}

/// MAKR : CollectionView Methods
extension FAdFilterVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = categoryTableView.dequeueReusableCell(withIdentifier: "fadfiltercell") as! FAdFilterCell
        
        let dict = dataArray[indexPath.section]
        cell.titleLabel.text = dict["name"] as? String
        
        if (pass_catgid == "\(dict["id"] as! NSNumber)") {
            cell.checkBtn.setImage(#imageLiteral(resourceName: "Right_checked"), for: .normal)
        } else {
            cell.checkBtn.setImage(nil, for: .normal)
        }
        
        cell.titleLabel.layer.cornerRadius = 12
        cell.titleLabel.layer.masksToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = dataArray[indexPath.section]
        let cell = tableView.cellForRow(at: indexPath) as! FAdFilterCell
        
        if (cell.checkBtn.image(for: .normal) == nil) {
            cell.checkBtn.setImage(#imageLiteral(resourceName: "Right_checked"), for: .normal)
            pass_catgid = "\(dict["id"] as! NSNumber)"
        } else {
            cell.checkBtn.setImage(nil, for: .normal)
            pass_catgid = ""
        }
        categoryTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: categoryTableView.frame.width, height: 10))
        vw.backgroundColor = UIColor.clear
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
}

class FAdFilterCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var checkBtn : UIButton!
}
