//
//  FAdSearchVC.swift
//  Darious
//
//  Created by Apple on 07/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class FAdSearchVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var nodataLabel: UILabel!
    @IBOutlet weak var searchTF: UITextField!
    
    @IBOutlet weak var homeCollectionView : UICollectionView!
    
    var dataArray : [NSDictionary] = []

    var category_id : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FAdThemeColor
        btmImageView.tintColor = ColorCode.FAdThemeColor
        topHeaderImgVw.tintColor = ColorCode.FAdThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FAdThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&cat_id=\(self.category_id)"
            self.callService(urlstr: Api.Fad_Viewed_List, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func onFilter(_ sender: Any)
    {
        let filtervc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdFilterVC") as! FAdFilterVC
        present(filtervc, animated: false, completion: nil)
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdHomeVC") as! FAdHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
              //  print("search result = \(result)")
                if (check == "get") {
                    let data = result["data"] as! [NSDictionary]
                    if (data.count > 0)
                    {
                        self.dataArray = data
                        self.homeCollectionView.reloadData()
                    }
                    else {
                        self.homeCollectionView.isHidden = true
                        self.nodataLabel.isHidden = false
                    }
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyadBottomButtonAction(tagValue: sender.tag, controller: self)
    }
    
}

/// MAKR : CollectionView Methods
extension FAdSearchVC : UICollectionViewDataSource, UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "fadsearchcell", for: indexPath) as! FAdSearchCollCell
        
        let dict = dataArray[indexPath.item]
        
        if (dict["type"] as! String == "image")
        {
            let imgUrl = URL(string: "\(Api.FP_Pic_Url)\(dict["name"] as! String)")
            cell.bgImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "FBest_other"))
        }
        else {
            let vid = getYoutubeId(youtubeUrl: dict["name"] as! String)
            cell.bgImageVw.sd_setImage(with: URL(string: "http://img.youtube.com/vi/\(vid!)/0.jpg"), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        }
        
        cell.titleLabel.text = dict["cat_name"] as? String
        cell.sharePrice.text = "\(dict["share_price"] as! String) \(Current_Currency)"
        
        cell.shareBtn.addTarget(self, action: #selector(shareAd(_:)), for: .touchUpInside)
        
        cell.bgImageVw.layer.cornerRadius = 12
        cell.bgImageVw.clipsToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            return 5
        }
        else if (self.view.frame.width == 375)
        {
            return 10
        }
        else {
            return 15
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10
    }
    
    @objc func shareAd(_ sender : UIButton)
    {
    }
}

class FAdSearchCollCell: UICollectionViewCell {
    
    @IBOutlet weak var bgImageVw : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var sharePrice : UILabel!
    
    @IBOutlet weak var shareBtn : UIButton!
}
