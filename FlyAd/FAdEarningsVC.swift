//
//  FAdEarningsVC.swift
//  Darious
//
//  Created by Apple on 07/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class FAdEarningsVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var nodataLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var toalEarningLbl: UILabel!
    @IBOutlet weak var earningAmtLabel: UILabel!
    
    @IBOutlet weak var earningTableView : UITableView!
    
    var dataArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FAdThemeColor
        btmImageView.tintColor = ColorCode.FAdThemeColor
        topHeaderImgVw.tintColor = ColorCode.FAdThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.Fad_Earnings, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdHomeVC") as! FAdHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                // print("search result = \(result)")
                if (check == "get") {
                    let data = result["data"] as! [NSDictionary]
                    if (data.count > 0)
                    {
                        self.dataArray = data
                        self.earningTableView.reloadData()
                        
                        let timeStr = (self.dataArray[0]["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM, yyyy")
                        self.timeLabel.text = "Last Updated " + timeStr!
                        
                        var total : Float = 0
                        for dict in self.dataArray
                        {
                            total = total + Float.init(dict["price"] as! String)!
                        }
                        self.earningAmtLabel.text = "\(total) \(Current_Currency)"
                    }
                    else {
                        self.earningTableView.isHidden = true
                        self.nodataLabel.isHidden = false
                    }
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyadBottomButtonAction(tagValue: sender.tag, controller: self)
    }
    
}

/// MAKR : CollectionView Methods
extension FAdEarningsVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = earningTableView.dequeueReusableCell(withIdentifier: "fadearningcell") as! FAdEarningCell
        
        let dict = dataArray[indexPath.item]
        
        let imgUrl = URL(string: "\(Api.FP_Pic_Url)\(dict["name"] as! String)")
        cell.bgImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "FBest_other"))
        
        cell.titleLabel.text = dict["title"] as? String
        cell.receivedLabel.text = "Recieved \(dict["price"] as! String) \(Current_Currency)"
        
        if let _ = dict["created_on"] as? String
        {
            let datestr = dict["created_on"] as! String
            if let str = datestr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM yyyy | HH:mm a")
            {
                cell.dateLabel.text = str
            }
        }
        
        return cell
    }
    
}

class FAdEarningCell: UITableViewCell {
    
    @IBOutlet weak var bgImageVw : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var receivedLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
}
