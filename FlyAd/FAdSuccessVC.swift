//
//  FAdSuccessVC.swift
//  Darious
//
//  Created by Apple on 07/01/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class FAdSuccessVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var topHeaderImgVw : UIImageView!
    
    @IBOutlet weak var congLabel : UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    
    @IBOutlet weak var shareBtn : KGHighLightedButton!
    
    var earnPrice : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FAdThemeColor
        btmImageView.tintColor = ColorCode.FAdThemeColor
        topHeaderImgVw.tintColor = ColorCode.FAdThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        priceLabel.text = earnPrice
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func shareEarning(_ sender: Any)
    {
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fad_storyboard.instantiateViewController(withIdentifier: "FAdHomeVC") as! FAdHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flyadBottomButtonAction(tagValue: sender.tag, controller: self)
    }
    
}
