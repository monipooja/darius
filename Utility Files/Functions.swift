//
//  Functions.swift
//  Darious
//
//  Created by Apple on 10/07/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
import UIKit

class StaticFunctions
{
    static func setBottomView(view : UIView, layoutConstraint : [NSLayoutConstraint], passWdCollection: [NSLayoutConstraint], passHtCollection: [NSLayoutConstraint], passTopSpaceCollection: [NSLayoutConstraint], passBottomBtnCollection: [UIButton], appstr : String)
    {
        if (view.frame.width == 320) && (view.frame.height == 568)
        {
            layoutConstraint[0].constant = 83
            layoutConstraint[1].constant = 5
            layoutConstraint[2].constant = 60
            layoutConstraint[3].constant = 60
            
            for wd in passWdCollection {
                wd.constant = wd.constant - 5
            }
            for ht in passHtCollection {
                ht.constant = ht.constant - 5
            }
            for vwtop in passTopSpaceCollection {
                vwtop.constant = vwtop.constant + 10
            }
            for btn in passBottomBtnCollection {
                if (btn.tag == 3) {
                    btn.titleLabel?.font =  UIFont(name: "HousegrindPersonalUseOnly", size: 12)
                    //btn.titleLabel?.font =  UIFont(name: "OpenSans-Semibold", size: 10)
                }
                else {
                    btn.titleLabel?.font =  UIFont(name: "OpenSans-Semibold", size: 8)
                }
                btn.titleEdgeInsets = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
            }
        }
        else if (view.frame.width == 375)
        {
            layoutConstraint[0].constant = 100
            for wd in passWdCollection {
                wd.constant = wd.constant + 0
            }
            for ht in passHtCollection {
                ht.constant = ht.constant + 0
            }
            for btn in passBottomBtnCollection {
                if (btn.tag == 3) {
                    btn.titleLabel?.font =  UIFont(name: "HousegrindPersonalUseOnly", size: 14)
                    //btn.titleLabel?.font =  UIFont(name: "OpenSans-Semibold", size: 13)
                }
                else {
                    btn.titleLabel?.font =  UIFont(name: "OpenSans-Semibold", size: 10)
                }
            }
        }
        else {
            layoutConstraint[0].constant = 110
            layoutConstraint[2].constant = 75
            layoutConstraint[3].constant = 75
            
            for wd in passWdCollection {
                wd.constant = wd.constant + 3
            }
            for ht in passHtCollection {
                ht.constant = ht.constant + 3
            }
            for vwtop in passTopSpaceCollection {
                vwtop.constant = vwtop.constant - 3
            }
            for btn in passBottomBtnCollection {
                if (btn.tag == 3) {
                    btn.titleLabel?.font =  UIFont(name: "HousegrindPersonalUseOnly", size: 14)
                    //btn.titleLabel?.font =  UIFont(name: "OpenSans-Semibold", size: 15)
                }
                else {
                    btn.titleLabel?.font =  UIFont(name: "OpenSans-Semibold", size: 12)
                }
            }
        }
    }
    
    static func showAlert(title: String?, message: String?, actions:[String], controller: UIViewController, completion:@escaping ((String) -> Void)) {
        customLoader.hideIndicator()
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        for aTitle in actions {
            // add an action (button)
            alert.addAction(UIAlertAction(title: aTitle, style: .default, handler: { (action) in
                completion(action.title!)
            }))
        }
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func addBorderOnLayer(bgColor : UIColor, vwFrame : CGRect, layer : CALayer)
    {
        let topBorder = CALayer()
        topBorder.backgroundColor = bgColor.cgColor
        topBorder.frame = vwFrame
        topBorder.opacity = 0.3
        layer.addSublayer(topBorder)
    }
    
    static func priceFormatSet(prcValue : String) -> String
    {
        if (Current_Currency == "€") {
            let changedValue = prcValue.replacingOccurrences(of: ".", with: ".")
            return changedValue
        }
        else {
            return prcValue
        }
    }
    
    static func ratingFormatSet(rateValue : String) -> String
    {
        if (useLang == "spanish") {
            let changedValue = rateValue.replacingOccurrences(of: ".", with: ".")
            return changedValue
        }
        else {
            return rateValue
        }
    }
    
    static func priceSetWithoutDecimal(prcValue : String) -> String
    {
        if (Current_Currency == "€") {
            let changedValue = "\(prcValue).00"
            return changedValue
        }
        else {
            let changedValue = "\(prcValue).00"
            return changedValue
        }
    }
    
    static func callRating(_ ratingValue : Double, starRating : [UIImageView], fullStar : UIImage, halfStar : UIImage, grayStar : UIImage)
    {
        if(ratingValue == 0)
        {
            starRating[0].image = grayStar
            starRating[1].image = grayStar
            starRating[2].image = grayStar
            starRating[3].image = grayStar
            starRating[4].image = grayStar
        }
        else if((ratingValue >= 0.5) && (ratingValue < 1.0))
        {
            starRating[0].image = halfStar
            starRating[1].image = grayStar
            starRating[2].image = grayStar
            starRating[3].image = grayStar
            starRating[4].image = grayStar
        }
        else if((ratingValue >= 1.0) && (ratingValue < 1.5))
        {
            starRating[0].image = fullStar
            starRating[1].image = grayStar
            starRating[2].image = grayStar
            starRating[3].image = grayStar
            starRating[4].image = grayStar
        }
        else if((ratingValue >= 1.5) && (ratingValue < 2.0))
        {
            starRating[0].image = fullStar
            starRating[1].image = halfStar
            starRating[2].image = grayStar
            starRating[3].image = grayStar
            starRating[4].image = grayStar
        }
        else if((ratingValue >= 2.0) && (ratingValue < 2.5))
        {
            starRating[0].image = fullStar
            starRating[1].image = fullStar
            starRating[2].image = grayStar
            starRating[3].image = grayStar
            starRating[4].image = grayStar
        }
        else if((ratingValue >= 2.5) && (ratingValue < 3.0))
        {
            starRating[0].image = fullStar
            starRating[1].image = fullStar
            starRating[2].image = halfStar
            starRating[3].image = grayStar
            starRating[4].image = grayStar
        }
        else if((ratingValue >= 3.0) && (ratingValue < 3.5))
        {
            starRating[0].image = fullStar
            starRating[1].image = fullStar
            starRating[2].image = fullStar
            starRating[3].image = grayStar
            starRating[4].image = grayStar
        }
        else if((ratingValue >= 3.5) && (ratingValue < 4.0))
        {
            starRating[0].image = fullStar
            starRating[1].image = fullStar
            starRating[2].image = fullStar
            starRating[3].image = halfStar
            starRating[4].image = grayStar
        }
        else if((ratingValue >= 4.0) && (ratingValue < 4.5))
        {
            starRating[0].image = fullStar
            starRating[1].image = fullStar
            starRating[2].image = fullStar
            starRating[3].image = fullStar
            starRating[4].image = grayStar
        }
        else if((ratingValue >= 4.5) && (ratingValue < 5.0))
        {
            starRating[0].image = fullStar
            starRating[1].image = fullStar
            starRating[2].image = fullStar
            starRating[3].image = fullStar
            starRating[4].image = halfStar
        }
        else if((ratingValue == 5.0))
        {
            for i in 0...4
            {
                starRating[i].image = fullStar
            }
        }
    }
    
    static func toJsonFromObject(passObj : Any) -> String?
    {
        guard let data = try? JSONSerialization.data(withJSONObject: passObj, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
  /*  static func alert(title: String, message : String , controller: UIViewController)
    {
        OperationQueue.main.addOperation
            {
                customLoader.hideIndicator()
                let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                controller.present(alert, animated: true, completion: nil)
        }
    }
    
    static func bottomCustomAlert(message : String , controller: UIViewController)
    {
        OperationQueue.main.addOperation {
            bottomCustomView = UILabel.init(frame: CGRect(x: 0, y : controller.view.frame.height - 120, width : controller.view.frame.width, height : 60))
            bottomCustomView.backgroundColor = UIColor.init(hexString: "919191")
            bottomCustomView.text = message
            bottomCustomView.textColor = UIColor.white
            bottomCustomView.textAlignment = .center
            bottomCustomView.numberOfLines = 2
            bottomCustomView.layer.cornerRadius = 18
            bottomCustomView.alpha = 0.8
            bottomCustomView.font = UIFont(name: "ProximaNova-Regular", size: 14)
            controller.view.addSubview(bottomCustomView)
            
            timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(hideBottomCustomView), userInfo: nil, repeats: true)
        }
    }
    
    @objc static func hideBottomCustomView() {
        
        UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
            if (Float(bottomCustomView.alpha) > 0) {
                bottomCustomView.alpha = bottomCustomView.alpha - 0.2
            }
            else {
                timer.invalidate()
                bottomCustomView.removeFromSuperview()
            } // Here you will get the animation you want
        }, completion: { _ in
            timer.invalidate()
            bottomCustomView.removeFromSuperview() // Here you hide it when animation done
        })
    }
    
    static func CameraGallery(controller: UIViewController, imagePicker : UIImagePickerController)
    {
        let actionSheetController : UIAlertController = UIAlertController(title: "por favor selecciona una opcion", message: "", preferredStyle: .actionSheet)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: "Cámara", style: .default)
        { action -> Void in
            // print("Camera")
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
            {
                imagePicker.sourceType = UIImagePickerController.SourceType.camera;
                imagePicker.allowsEditing = false
                controller.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(cameraActionButton)
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: "Galería", style: .default)
        { action -> Void in
            //  print("Gallery")
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)
            {
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                imagePicker.allowsEditing = false
                controller.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(galleryActionButton)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancelar", style: .cancel) { action -> Void in
            // print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        controller.present(actionSheetController, animated: true, completion: nil)
    }
    
    static func validateEmail(_ emailStr : String) -> Bool
    {
        let a = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" as String
        let emailTest = NSPredicate(format: "SELF MATCHES %@", a)
        return emailTest.evaluate(with: emailStr)
    }
    
    static func calculateSizeForString(_ inString:String, sz : Float, maxWidth : CGFloat, controller: UIViewController) -> CGRect
    {
        let messageString = inString
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: CGFloat(sz))]
        let attrString:NSAttributedString? = NSAttributedString(string: messageString, attributes: attributes)
        
        // let maxWidth = controller.view.frame.width - (controller.view.frame.width - 245)
        let rect:CGRect = attrString!.boundingRect(with: CGSize(width : maxWidth, height : CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context:nil )   //hear u will get nearer height not the exact value
        let requredSize:CGRect = rect
        return requredSize         //requredSize.height  //to include button's in your tableview
    }
    
    // MARK : Conversion Of JSON To Object
    static func toObjectFromJson(jsonString : String) -> AnyObject
    {
        let data = jsonString.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if let jsonData = data {
            // Will return an object or nil if JSON decoding fails
            do {
                let objectData = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as AnyObject
                
                return objectData
            }
            catch {
                return "error" as AnyObject
            }
        } else {
            // Lossless conversion of the string was not possible
            return "error" as AnyObject
        }
    }
    
    static func useJsonFile(fileName : String) -> Any
    {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject> {
                    // do stuff
                    return jsonResult
                } else {
                    return ""
                }
            } catch let error {
                // handle error
                print("json reading error = \(error.localizedDescription)")
                return ""
            }
        } else {
            return ""
        }
    }
    
    static func loadSVGToWeb(wbvw : UIWebView, resource : String)
    {
        let path: String = Bundle.main.path(forResource: resource, ofType: "svg")!
        let url: NSURL = NSURL.fileURL(withPath: path) as NSURL  //Creating a URL which points towards our path
        //Creating a page request which will load our URL (Which points to our path)
        let request: URLRequest = URLRequest.init(url: url as URL)
        wbvw.loadRequest(request as URLRequest)
    }
    
    static func getLanguageFromUserDefaults(inputString : String) -> String
    {
        var returnStr : String = ""
        let txt = userDefault.data(forKey: "language")
        if(txt != nil) {
            let txtData = NSKeyedUnarchiver.unarchiveObject(with: txt!) as! [NSDictionary]
            for dict in txtData {
                let wI = NSMutableString( string: dict["tag_name"] as! String )
                CFStringTransform( wI, nil, "Any-Hex/Java" as NSString, true )
                let zz = wI as String
                
                if (zz == inputString) {
                    returnStr = dict[useLang] as! String
                    break
                }
                else {
                }
            }
        }
        if (returnStr == ""){
            returnStr = inputString
        }
        return returnStr
    }
    
    //    static func AESEncrypt(appKey : String)
    //    {
    //        var key : String = Constant.SECRET_KEY
    //
    //        if (key.count < Constant.CIPHER_KEY_LEN) {
    //            let numPad : Int = Constant.CIPHER_KEY_LEN - key.count
    //            for _ in 0...numPad {
    //                key += "0"
    //            }
    //        } else if (key.count > Constant.CIPHER_KEY_LEN) {
    //            // let range = 0..<Constant.CIPHER_KEY_LEN // If you have a range
    //            //key = key.substring(with: range) // Swift 3
    //            // key = key.index(key.startIndex, offsetBy: Constant.CIPHER_KEY_LEN)
    //        }
    //    }
 } */
    
//    static func proceedToNewController(controller : UIViewController, storyboard : UIStoryboard, identifier : String, newvc : UIViewController)
//    {
//        let contVc = storyboard.instantiateViewController(withIdentifier: identifier) as! contName
//        controller.present(contVc, animated: false, completion: nil)
//    }
}

