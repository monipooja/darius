//
//  Constant.swift
//  Darious
//
//  Created by Apple on 07/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
import UIKit

let device_id : String = (UIDevice.current.identifierForVendor?.uuidString)!
let api_key : String = "123456"
let device_type : String = "2"
var Access_token : String = ""
let role : String = "user"
var user_id : NSNumber = 0
var userData : NSDictionary = [:]
let userDefault : UserDefaults = UserDefaults.standard
var mollie_key : String = ""
var commission_value : String = ""
var commission_Card : String = ""
var wallet_amount : NSNumber = 0
var notification_count : NSNumber = 0
var Anonymous_price : String = ""
var Reveal_price : String = ""
var AnotherComment_price : String = ""
var Current_Currency : String = "€"
var Currency_Code : String = "EUR"
var Currency_Name : String = "euro"
var Image_URL : String = ""
var AdUnitId : String = "ca-app-pub-3940256099942544/3986624511"
var AdSharedSecret : String = "69fe7e2e5e524c399376a0616a29a3d2"

var YesText : String = "Yes"
var NoText: String = "No"
var OkText: String = "Ok"

class ColorCode {
    // Dakefly
    static let themeColor : String = "CF3C26" //"F27242"
    static let progressColor : String = "F24248"
    static let TrackColor : String = "ECECEC"
    // Flyclaim
    static let FCBlackColor : String = "262626"
    static let AppGrayColor : String = "C1C1C1"
    // FlyMoney
    static let FMThemeColor : String = "143A66"
    // FlySports
    static let FSPThemeColor : String = "FF0A0A"
    // FlyAngel
    static let FAThemeColor : String = "3ECBFF"
    // FlyMenu
    static let FMenuThemeColor : String = "AC3B10"
    static let FMenuIndividColor : String = "03930E"
    static let FMenuCoupleColor : String = "D98200"
    static let FMenuSmallColor : String = "D94402"
    static let FMenuMediumColor : String = "661414"
    static let FMenuBigColor : String = "143A66"
    static let AddBgColor : String = "F9F8F8"
    static let FMenuGray : String = "707070"
    static let FMenuLightGreen : String = "DEEFDF"
    static let FMenuLightYellow : String = "E9B254"
    static let FMenuLightRed : String = "BF4F4F"
    static let FMenuPauseGreen : String = "6BA26F"
    static let FMenuBorderGreen : String = "007009"
    // FlyFriends
    static let FFThemeColor : String = "009782"
    static let FFLightColor : String = "EEFAF8"
    static let FFIconsGray : String = "565656"
    static let FFGraycomment : String = "D6D6D6"
    static let FFBluecomment : String = "00539B"
    static let FFPinkcomment : String = "96009B"
    static let FFRemoveBG : UIColor = UIColor.init(hexString: "9B001A")
    static let FFHiddenBG : UIColor = UIColor.init(hexString: "474747")
    // FlyBest
    static let FBThemeColor : UIColor = UIColor.init(hexString: "2B55A8")
    static let FBGrayVote : UIColor = UIColor.init(hexString: "C9C8C8")
    // FlySport
    static let FSThemeColor : UIColor = UIColor.init(hexString: "E80531")
    static let FSDDColor : UIColor = UIColor.init(hexString: "565454")
    static let FSViewBG : UIColor = UIColor.init(hexString: "040404")
    // FlyPic
    static let FPicThemeColor : UIColor = UIColor.init(hexString: "0B607B")
    static let FPicReadyColor : UIColor = UIColor.init(hexString: "3BAB59")
    static let FPEditBgGray : UIColor = UIColor.init(hexString: "D3D3D3")
    static let FPEditBgGreen : UIColor = UIColor.init(hexString: "5AD1BD")
    static let FPOrderCell : UIColor = UIColor.init(hexString: "DAE7EB")
    // FlyPic
    static let FAdThemeColor : UIColor = UIColor.init(hexString: "EC1C62")
    // FlyMeet
    static let FMeetThemeClr : UIColor = UIColor.init(hexString: "FEF8F8")
    static let FMeetDarkTheme : UIColor = UIColor.init(hexString: "AE0404")
}

class StoryboardType {
    static let main_storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    static let fc_storyboard = UIStoryboard.init(name: "Flyclaim", bundle: nil)
    static let fm_storyboard = UIStoryboard.init(name: "Flymoney", bundle: nil)
    static let fa_storyboard = UIStoryboard.init(name: "Flyangel", bundle: nil)
    static let fmenu_storyboard = UIStoryboard.init(name: "Flymenu", bundle: nil)
    static let ff_storyboard = UIStoryboard.init(name: "Flyfriends", bundle: nil)
    static let fbest_storyboard = UIStoryboard.init(name: "Flybest", bundle: nil)
    static let fs_storyboard = UIStoryboard.init(name: "Flysports", bundle: nil)
    static let fpic_storyboard = UIStoryboard.init(name: "Flypic", bundle: nil)
    static let fad_storyboard = UIStoryboard.init(name: "Flyad", bundle: nil)
    static let fmeet_storyboard = UIStoryboard.init(name: "Flymeet", bundle: nil)
}

enum Const {
    static let appColorLogo = ["C_FlyFriend",
                               "fmeet_topIcon",
                               "C_FlyBest",
                               "",
                               "C_FlySports",
                               "",
                               "C_FlyPic",
                               "",
                               "C_FlyClaim"]
//                              "C_FlyAd"]
//                               "C_Flyencers",
//                               "C_FlyHealth",
//                               "C_FlyBed",
//                               "C_FlyDeli",
//                               "C_FlyCar",
//                               "C_FlyGame",
//                               "C_FlyAngel",
//                               "C_FlyFood"]
    
    static let appGrayLogo = ["Gray_Flyfriend",
                              "Gray_Flybest",
                              "Gray_Flysports",
                              "Gray_Flypic",
                              "Gray_Flyclaim"]
//                              "Gray_Flyads"]
//                              "Gray_Flyencers",
//                              "Gray_Flyhealth",
//                              "Gray_Flybed",
//                              "Gray_Flydeli",
//                              "Gray_Flycar",
//                              "Gray_Flygame",
//                              "Gray_Flyangel",
//                              "Gray_Flymenu"]
    
    static let appNames = ["FlyFriends",
                           "FlyMeet",
                           "FlyBest",
                           "",
                           "FlySport",
                           "",
                           "FlyPic",
                           "",
                           "FlyClaim"]
//     "FlyAds",
//    ["FlyFriends",
//    "inFlyencers",
//    "FlyClaim",
//    "FlyHealth",
//    "FlyBest",
//    "FlyBed",
//    "FlySport",
//    "FlyPic",
//    "FlyDeli",
//    "FlyCar",
//    "",
//    "FlyGame",
//    "",
//    "FlyAngel",
//    "",
//    "FlyMenu"]
    
    static let appDescription = ["flyfriends_text",
                                 "flymeet_text",
                                 "flyBest_text",
                                 "",
                                 "flysport_text",
                                 "",
                                 "flyPics_text",
                                 "",
                                 "flyClaim_text"]
//                                 "flyAd_text",
//                                 "inFlyencer_text",
//                                 "flyHealth_text",
//                                 "flyBed_text",
//                                 "flyDeli_text",
//                                 "flyCar_text",
//                                 "flyGames_text",
//                                 "flyAngel_text",
//                                 "flyMenu_text"]
    
    static let appThemsColor = [UIColor.init(hexString:ColorCode.FFThemeColor),
                                ColorCode.FMeetDarkTheme,
                                ColorCode.FBThemeColor,
                                nil,
                                ColorCode.FSThemeColor,
                                nil,
                                ColorCode.FPicThemeColor,
                                nil,
                                UIColor.init(hexString: ColorCode.FCBlackColor)]
//                                ColorCode.FAdThemeColor,
//                                UIColor.init(hexString: ColorCode.AppGrayColor),
//                                UIColor.init(hexString: ColorCode.AppGrayColor),
//                                UIColor.init(hexString: ColorCode.AppGrayColor),
//                                UIColor.init(hexString: ColorCode.AppGrayColor),
//                                UIColor.init(hexString: ColorCode.AppGrayColor),
//                                UIColor.init(hexString: ColorCode.AppGrayColor),
//                                UIColor.init(hexString: ColorCode.AppGrayColor)]
}

let appBundleId = "com.dakefly"

/// in-app Purchase
enum ServicePurchase: String {
    
    case reveal
    case editopn
    case block
    case unblock
    case anonymous
    case remove
    case removeblock
    case writeanother
    
}

struct GlobalConstant {
    // MARK: - Screen Size
    static let SCREENWIDTH = UIScreen.main.bounds.size.width
    static let SCREENHEIGHT = UIScreen.main.bounds.size.height
    static let SCREENMAXLENGTH = max(GlobalConstant.SCREENWIDTH, GlobalConstant.SCREENHEIGHT)
    static let SCREENMINLENGTH = min(GlobalConstant.SCREENWIDTH, GlobalConstant.SCREENHEIGHT)
}

struct DeviceType1 {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH < 568.0
    static let IS_IPHONE_5_SE          = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 568.0
    static let IS_IPHONE_6_6plus_8_7   = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 736.0
    static let IS_IPHONE_8         = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 736.0
    static let IS_IPHONE_X_12mini     = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 812
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && GlobalConstant.SCREENMAXLENGTH  == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && GlobalConstant.SCREENMAXLENGTH   == 1366.0
    static let IS_IPHONE_XSMAX      = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 896
    static let IS_IPHONE_XR         = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 1792
}
