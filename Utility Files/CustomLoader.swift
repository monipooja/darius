

import Foundation
import UIKit

class customLoader {
    
    static var currentOverlay : UIView?
    static var indicator : UIActivityIndicatorView?
    
    static func show(showColor1 : UIColor) {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            
            return
        }
        show(currentMainWindow, showColor2 : showColor1)
    }
    
    static func show(_ loadingText: String, showColor : UIColor) {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            
            return
        }
        show(currentMainWindow, loadingText: loadingText, showColor3 : showColor)
    }
    
    static func show(_ overlayTarget : UIView, showColor2 : UIColor) {
        show(overlayTarget, loadingText: nil, showColor3 : showColor2)
    }
    
    static func show(_ overlayTarget : UIView, loadingText: String?, showColor3 : UIColor) {
        // Clear it first in case it was already shown
        hide()
        
        // Create the overlay
        let overlay = UIView(frame: overlayTarget.frame)
        overlay.center = overlayTarget.center
        overlay.alpha = 0
        overlay.backgroundColor = UIColor.black
        overlayTarget.addSubview(overlay)
        overlayTarget.bringSubviewToFront(overlay)
        
        // Create and animate the activity indicator
        let indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        indicator.center = overlay.center
        indicator.color =  showColor3
        indicator.startAnimating()
        overlay.addSubview(indicator)
        
        // Create label
        if let textString = loadingText {
            let label = UILabel()
            label.text = textString
            label.textColor = UIColor.white
            label.sizeToFit()
            label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 30)
            overlay.addSubview(label)
        }
        
        // Animate the overlay to show
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        overlay.alpha = overlay.alpha > 0 ? 0 : 0.5
        UIView.commitAnimations()
        
        currentOverlay = overlay
    }
    
    static func hide() {
        if currentOverlay != nil {
            currentOverlay?.removeFromSuperview()
            currentOverlay =  nil
        }
    }
    
    static func showActivityIndicator(showColor : String, controller : UIView)
    {
        indicator = UIActivityIndicatorView.init(frame: CGRect(x: 0, y: 0, width : 50, height : 50))
        indicator?.center = controller.center
        indicator?.style = .whiteLarge
        indicator?.color = UIColor.init(hexString: showColor)
        indicator?.hidesWhenStopped = true
        controller.addSubview(indicator!)
        indicator?.startAnimating()
    }
    
    static func hideIndicator() {
        indicator?.stopAnimating()
        indicator?.removeFromSuperview()
    }
}
