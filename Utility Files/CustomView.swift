
import UIKit

@IBDesignable
open class KGHighLightedField: UITextField {
    
    @IBInspectable
    public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable
    public var shadowColor: UIColor = UIColor.clear {
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }
    
    @IBInspectable
    public var shadowRadius: CGFloat = 0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable
    public var opacity: Float = 0.0 {
        didSet {
            layer.shadowOpacity = opacity
        }
    }
    
    @IBInspectable
    public var offSet: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            layer.shadowOffset = offSet
        }
    }
    
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var padingRight: CGFloat = 0
    @IBInspectable var padingTop: CGFloat = 0
    @IBInspectable var padingBottom : CGFloat = 0
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y, width: bounds.width-padingRight, height: bounds.height)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    @IBInspectable var placeHolderColor : UIColor = UIColor(red:0.64, green:0.64, blue:0.64, alpha:1){
        didSet {
            setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
        }
    }
    
//    @IBInspectable var bottomLayer: Bool {
//        get{
//            return self.bottomLayer
//        }
//        set (hasDone) {
//            if hasDone{
//                addBottomLayer()
//            }
//        }
//    }
    
    @IBInspectable
    public var bgColor: UIColor = UIColor.clear {
        didSet {
            addBottomLayerWithColor(bgcolor: bgColor)
        }
    }
    
    func addBottomLayerWithColor(bgcolor : UIColor)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        border.frame = CGRect(x:0, y:bounds.height+1, width:bounds.width, height:1.5)
        layer.addSublayer(border)
    }
    
    func addBottomLayer()
    {
        let border = CALayer()
        border.backgroundColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x:0, y:bounds.height+1, width:bounds.width, height:0.6)
        layer.addSublayer(border)
    }
    
//    @IBInspectable var doneAccessory: Bool{
//        get{
//            return self.doneAccessory
//        }
//        set (hasDone) {
//            if hasDone{
//                addDoneButtonOnKeyboard()
//            }
//        }
//    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: ApiResponse.getLanguageFromUserDefaults(inputString: "common_done_ios"), style: .done, target: self, action: #selector(self.doneButtonAction))
        done.tintColor = UIColor.init(hexString: themeColor)
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}

@IBDesignable
open class KGHighLightedButton: UIButton {
    
    @IBInspectable
    public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    //
    @IBInspectable
    public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable
    public var shadowColor: UIColor = UIColor.clear {
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }
    
    @IBInspectable
    public var shadowRadius: CGFloat = 0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable
    public var opacity: Float = 0.0 {
        didSet {
            layer.shadowOpacity = opacity
        }
    }
    
    @IBInspectable
    public var offSet: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            layer.shadowOffset = offSet
        }
    }
}

@IBDesignable
open class CustomUIView: UIView {
    
    @IBInspectable
    public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable
    public var shadowColor: UIColor = UIColor.clear {
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }
    
    @IBInspectable
    public var shadowRadius: CGFloat = 0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable
    public var opacity: Float = 0.0 {
        didSet {
            layer.shadowOpacity = opacity
        }
    }
    
    @IBInspectable
    public var offSet: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            layer.shadowOffset = offSet
        }
    }
}

@IBDesignable
open class GradientView: UIView {
    @IBInspectable
    public var startColor: UIColor = .white {
        didSet {
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    public var endColor: UIColor = .white {
        didSet {
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            setNeedsDisplay()
        }
    }
    
    private lazy var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [self.startColor.cgColor, self.endColor.cgColor]
        return gradientLayer
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    open override func layoutSubviews() {
        gradientLayer.frame = bounds
    }
}

@IBDesignable
open class customLabelView: UILabel {
    
    @IBInspectable
    public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable
    public var shadowColr : UIColor = UIColor.clear {
        didSet {
            layer.shadowColor = shadowColr.cgColor
        }
    }
    
    @IBInspectable
    public var shadowRadius: CGFloat = 0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable
    public var opacity: Float = 0.0 {
        didSet {
            layer.shadowOpacity = opacity
        }
    }
    
    @IBInspectable
    public var offSet: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            layer.shadowOffset = offSet
        }
    }
    
    @IBInspectable
    public var maskToBounds: Bool = false {
        didSet {
            layer.masksToBounds = maskToBounds
        }
    }
    
    @IBInspectable var padingLeft: CGFloat = 0
    @IBInspectable var padingRight: CGFloat = 0
    @IBInspectable var padingTop: CGFloat = 0
    @IBInspectable var padingBottom: CGFloat = 0
    
    override open func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: padingTop, left: padingLeft, bottom: padingBottom, right: padingRight)
        super.drawText(in: rect.inset(by: insets))
    }
}

@IBDesignable
class MySlide: UISlider {
    
    @IBInspectable var height: CGFloat = 3
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: bounds.origin, size: CGSize(width: bounds.width, height: height))
    }
    
    @IBInspectable var maxImage : UIImage? {
        
        didSet{
            setMaximumTrackImage(maxImage, for: .normal)
        }
    }
}

@IBDesignable
open class customImageView: UIImageView {
    
    @IBInspectable
    public var cornerRad: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRad
        }
    }
    
    @IBInspectable
    public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    
    @IBInspectable
    public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    public var maskToBounds: Bool = false {
        didSet {
            layer.masksToBounds = maskToBounds
        }
    }
    
    @IBInspectable
    public var clipBounds: Bool = false {
        didSet {
            self.clipsToBounds = clipBounds
        }
    }
    
    @IBInspectable
    public var Shadow: UIColor = UIColor.clear {
        didSet {
            layer.shadowColor = Shadow.cgColor
        }
    }
    
    @IBInspectable
    public var shadowRadius: CGFloat = 0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable
    public var opacity: Float = 0.0 {
        didSet {
            layer.shadowOpacity = opacity
        }
    }
    
    @IBInspectable
    public var offSet: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            layer.shadowOffset = offSet
        }
    }
    
}

@IBDesignable
open class customTextView: UITextView {
    
    @IBInspectable
    public var cornerRad: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRad
        }
    }
    
    @IBInspectable
    public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    
    @IBInspectable
    public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: ApiResponse.getLanguageFromUserDefaults(inputString: "common_done_ios"), style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
    
    override open func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        if (action == #selector(UIResponderStandardEditActions.paste(_:)) || action == #selector(UIResponderStandardEditActions.cut(_:)) || action == #selector(UIResponderStandardEditActions.copy(_:)) || action == #selector(UIResponderStandardEditActions.select(_:)) || action == #selector(UIResponderStandardEditActions.selectAll(_:))) {
            
            return false
            
        }
        return super.canPerformAction(action, withSender: sender)
    }
}

@IBDesignable
open class CustomNaviagtionBar : UINavigationBar {
//    @IBInspectable var bottomLayer: Bool {
//        get{
//            return self.bottomLayer
//        }
//        set (hasDone) {
//            if hasDone{
//                addBottomLayer()
//            }
//        }
//    }
    
    func addBottomLayer()
    {
        let border = CALayer()
        border.backgroundColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x:0, y:43, width:UIScreen.main.bounds.width, height:1)
        layer.addSublayer(border)
    }
    
    @IBInspectable
    public var Shadow: UIColor = UIColor.clear {
        didSet {
            layer.shadowColor = Shadow.cgColor
        }
    }
    
    @IBInspectable
    public var shadowRadius: CGFloat = 0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable
    public var opacity: Float = 0.0 {
        didSet {
            layer.shadowOpacity = opacity
        }
    }
    
    @IBInspectable
    public var offSet: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            layer.shadowOffset = offSet
        }
    }
    
    @IBInspectable
    public var maskedToBounds: Bool = false {
        didSet {
            layer.masksToBounds = maskedToBounds
        }
    }
}

