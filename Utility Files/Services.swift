

import Foundation
import UIKit
import StringExtensionHTML

var customView : UILabel!
var bottomCustomView : UILabel!
var timer : Timer!
var reqCount : Int = 0

class ApiResponse {
    
    static func onResponsePost(url: String,parms: NSDictionary, controller: UIViewController, completion: @escaping (_ res:NSDictionary) -> Void) {
        
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let url = NSURL(string:"\(url)")
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parms, options: JSONSerialization.WritingOptions())
                
                let task = session.dataTask(with: request as URLRequest) {
                    data, response, error in
                    
                    guard let data = data, error == nil else
                    {
                        ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "time_out"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "try_again"), controller: controller)
                        return
                    }
                    
                    if let httpStatus = response as? HTTPURLResponse,
                        httpStatus.statusCode != 200 { // check for httperrors
                         print("respppp = \(httpStatus)")
                        ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "something_wrong"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "try_again"), controller: controller)
                        return
                    }
                    
                    if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any]
                    {
                        // print("parsed data = \(parsedData)")
                        let status = parsedData["status"] as! NSNumber
                        if (status == 1){
                            completion(parsedData as NSDictionary)
                        }
                        else if(status == 3){
                            OperationQueue.main.addOperation {
                                customLoader.hideIndicator()
                                Access_token = ""
                                userDefault.set(Access_token, forKey: "token")
                                userDefault.synchronize()
                                let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                controller.present(VC, animated: true, completion: nil)
                            }
                        }
                        else {
                            // print("parsed data = \(parsedData)")
                            if let str = parsedData["message"] as? String {
                                let msgStr = str
                                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                                if(showMsg != "") {
                                    ApiResponse.alert(title: "", message: showMsg, controller: controller)
                                }
                                else {
                                    ApiResponse.alert(title: "", message: msgStr, controller: controller)
                                }
                            }
                            else {
                                OperationQueue.main.addOperation {
                                    customLoader.hideIndicator()
                                }
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            customLoader.hideIndicator()
                        }
                    }
                }
                task.resume()
            }
            catch
            {
            }
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: controller)
        }
    }
    
    static func onResponsePostMollie2(url: String,parms: [String : Any], completion: @escaping (_ res:NSDictionary , _ error : String) -> Void) {
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let url = NSURL(string:"\(url)")
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(mollie_key)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parms, options: .prettyPrinted)
            
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 201 {
                        print("response was not 200: \(String(describing: response))")
                        completion(["":""], Api.Status_Not_200)
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    completion(["":""], "\(String(describing: error))")
                    return
                }
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                    //  print("result = \(String(describing: result))")
                    completion(result! , "")
                }
                catch
                {
                }
            }
            task.resume()
        }
        catch
        {
        }
    }
    static func onResponseGetMollie(url: String, completion: @escaping (_ res:NSDictionary , _ error : String) -> Void) {
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        
        let url = NSURL(string:"\(url)")
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(mollie_key)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        do {
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        completion(["":""], Api.Status_Not_200)
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    completion(["":""], "\(String(describing: error))")
                    return
                }
                
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                    //print("result = \(String(describing: result))")
                    completion(result! , "")
                }
                catch
                {
                }
            }
            task.resume()
        }
    }
    
    static func onResponseGet(url: String, completion: @escaping (_ res:NSDictionary , _ error : String) -> Void) {
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        
        let url = NSURL(string:"\(url)")
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        
        do {
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
//                        print("response was not 200: \(String(describing: response))")
                        completion(["":""], Api.Status_Not_200)
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    completion(["":""], "\(String(describing: error))")
                    return
                }
                
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                   // print("result = \(String(describing: result))")
                    completion(result! , "")
                }
                catch
                {
                }
            }
            task.resume()
        }
    }
    
    static func onResponsePostPhp(url: String,parms: String, completion: @escaping (_ res:NSDictionary , _ error : String) -> Void) {
        
        var request = URLRequest(url: URL(string: "\(url)")!)
        request.httpMethod = "POST"
        let postString = parms
        //print("post string = \(postString)")
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            
            guard let data = data, error == nil else {
                // check for fundamental networking error
//                print("error=\(String(describing: error))")
                completion(["":""], "\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
//                print("statusCode should be 200, but is \(httpStatus.statusCode)")
//                print("response = \(String(describing: response))")
                completion(["":""], Api.Status_Not_200)
                return
            }
            
            if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any]
            {
                //print("parsed data = \(parsedData)")
                completion(parsedData as NSDictionary , "")
            }
            else
            {
                OperationQueue.main.addOperation {
                    customLoader.hideIndicator()
                    print("false")
                }
            }
        }
        task.resume()
    }
    
    static func onPostPhp(url: String,parms: String, controller: UIViewController, completion: @escaping (_ res:NSDictionary) -> Void) {
        
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            var request = URLRequest(url: URL(string: "\(url)")!)
            request.httpMethod = "POST"
            let postString = parms
            request.httpBody = postString.data(using: .utf8)
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                
                guard let data = data, error == nil else
                {
                    if (reqCount <= 2)
                    {
                        // print("reqqqq = \(reqCount)")
                        onPostPhp(url: url, parms: parms, controller: controller, completion: { (result) in
                            completion(result as NSDictionary)
                        })
                    }
                    else {
                        // print("time out")
                        ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "time_out"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "try_again"), controller: controller)
                    }
                    reqCount += 1
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse,
                    httpStatus.statusCode != 200 { // check for httperrors
                   // print("respppp = \(httpStatus)")
                    ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "something_wrong"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "try_again"), controller: controller)
                    return
                }
                
                if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any]
                {
                   // print("parsed data = \(parsedData)")
                    let status = parsedData["status"] as! NSNumber
                    if (status == 1){
                        completion(parsedData as NSDictionary)
                    }
                    else if(status == 3){
                        OperationQueue.main.addOperation {
                            customLoader.hideIndicator()
                            Access_token = ""
                            userDefault.set(Access_token, forKey: "token")
                            userDefault.synchronize()
                            let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            controller.present(VC, animated: true, completion: nil)
                        }
                    }
                    else {
                       // print("parsed data = \(parsedData)")
                        if let str = parsedData["message"] as? String {
                            let msgStr = str
                            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                            if(showMsg != "") {
                                ApiResponse.alert(title: "", message: showMsg, controller: controller)
                            }
                            else {
                                ApiResponse.alert(title: "", message: msgStr, controller: controller)
                            }
                        }
                        else {
                            OperationQueue.main.addOperation {
                                customLoader.hideIndicator()
                            }
                        }
                    }
                }
                else
                {
                   // print("parsed data = \(error)")
                    OperationQueue.main.addOperation {
                        customLoader.hideIndicator()
                    }
                }
            }
            task.resume()
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: controller)
        }
    }
    
    static func onGetPhp(url: String,parms: String, controller: UIViewController, completion: @escaping (_ res:NSDictionary) -> Void) {
        
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let url1 = NSURL(string:"\(url)")
            let request = NSMutableURLRequest(url: url1! as URL)
            request.httpMethod = "GET"
            
            do {
                let task = session.dataTask(with: request as URLRequest) {
                    data, response, error in
                    
                    guard let data = data, error == nil else
                    {
                        ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "time_out"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "try_again"), controller: controller)
                        if (url == Api.FMenu_Splash_URL) {
                            controller.dismiss(animated: false, completion: nil)
                        }
                        return
                    }
                    
                    if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode != 200 {
                          //  print("response was not 200: \(String(describing: response))")
                            ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "something_wrong"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "try_again"), controller: controller)
                            return
                        }
                    }
                
                    do
                    {
                        if let result = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                        {
                            let status = result["status"] as! NSNumber
                            
                            if (status == 1){
                                completion(result as NSDictionary)
                            }
                            else if(status == 3){
                                OperationQueue.main.addOperation {
                                    customLoader.hideIndicator()
                                    Access_token = ""
                                    userDefault.set(Access_token, forKey: "token")
                                    userDefault.synchronize()
                                    let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    controller.present(VC, animated: true, completion: nil)
                                }
                            }
                            else {
                                //  print("parsed data = \(parsedData)")
                                if let str = result["message"] as? String {
                                    let msgStr = str
                                    let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                                    if(showMsg != "") {
                                        ApiResponse.alert(title: "", message: showMsg, controller: controller)
                                    }
                                    else {
                                        ApiResponse.alert(title: "", message: msgStr, controller: controller)
                                    }
                                }
                                else {
                                    OperationQueue.main.addOperation {
                                        customLoader.hideIndicator()
                                    }
                                }
                            }
                        }
                        else {
                            OperationQueue.main.addOperation {
                                customLoader.hideIndicator()
                            }
                        }
                    }
                    catch
                    {
                    }
                }
                task.resume()
            }
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: controller)
        }
    }
    
    static func alert(title: String, message : String , controller: UIViewController)
    {
        OperationQueue.main.addOperation
        {
            customLoader.hideIndicator()
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: OkText, style: UIAlertAction.Style.default, handler: nil))
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    static func bottomCustomAlert(message : String , controller: UIViewController, fontFamliy : UIFont, bgcolor: UIColor, txtcolor: UIColor, wd: CGFloat, ht:CGFloat)
    {
        OperationQueue.main.addOperation {
            bottomCustomView = UILabel.init(frame: CGRect(x: (controller.view.frame.width-wd)/2, y : controller.view.frame.height - 70, width : wd, height : ht))
            bottomCustomView.backgroundColor = bgcolor
            bottomCustomView.text = message
            bottomCustomView.textColor = txtcolor
            bottomCustomView.textAlignment = .center
            if (ht == 40){
                bottomCustomView.numberOfLines = 1
                bottomCustomView.layer.cornerRadius = 18
            } else {
                bottomCustomView.numberOfLines = 2
                bottomCustomView.layer.cornerRadius = 22
            }
            bottomCustomView.alpha = 1
            bottomCustomView.layer.masksToBounds = true
            bottomCustomView.layer.shadowColor = UIColor.lightGray.cgColor
            bottomCustomView.layer.shadowRadius = 3
            bottomCustomView.layer.shadowOpacity = 0.4
            bottomCustomView.layer.shadowOffset = CGSize(width: 0, height: 0)
            
            bottomCustomView.font = fontFamliy
                //UIFont(name: "SegoeScript-Bold", size: 15)
            
            UIView.transition(with: bottomCustomView, duration: 1, options: .transitionCurlUp, animations: {
                controller.view.addSubview(bottomCustomView)
            }, completion: nil)
            
            timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(hideBottomCustomView), userInfo: nil, repeats: true)
        }
    }
    
    @objc static func hideBottomCustomView() {
        
        UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
            if (Float(bottomCustomView.alpha) > 0) {
                bottomCustomView.alpha = bottomCustomView.alpha - 0.2
            }
            else {
                timer.invalidate()
                bottomCustomView.removeFromSuperview()
            } // Here you will get the animation you want
        }, completion: { _ in
            timer.invalidate()
            bottomCustomView.removeFromSuperview() // Here you hide it when animation done
        })
    }
    
    static func CameraGallery(controller: UIViewController, imagePicker : UIImagePickerController)
    {
        let actionSheetController : UIAlertController = UIAlertController(title: ApiResponse.getLanguageFromUserDefaults(inputString: "select_options"), message: "", preferredStyle: .actionSheet)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: ApiResponse.getLanguageFromUserDefaults(inputString: "camera"), style: .default)
        { action -> Void in
            // print("Camera")
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
            {
                imagePicker.sourceType = UIImagePickerController.SourceType.camera;
                imagePicker.allowsEditing = false
                controller.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(cameraActionButton)
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: ApiResponse.getLanguageFromUserDefaults(inputString: "gallery"), style: .default)
        { action -> Void in
            //  print("Gallery")
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)
            {
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                imagePicker.allowsEditing = false
                controller.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(galleryActionButton)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: ApiResponse.getLanguageFromUserDefaults(inputString: "cancel"), style: .cancel) { action -> Void in
            // print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        controller.present(actionSheetController, animated: true, completion: nil)
    }
    
    static func validateEmail(_ emailStr : String) -> Bool
    {
        let a = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" as String
        let emailTest = NSPredicate(format: "SELF MATCHES %@", a)
        return emailTest.evaluate(with: emailStr)
    }
    
    static func calculateSizeForString(_ inString:String, sz : Float, maxWidth : CGFloat, controller: UIViewController) -> CGRect
    {
        let messageString = inString
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: CGFloat(sz))]
        let attrString:NSAttributedString? = NSAttributedString(string: messageString, attributes: attributes)
        
       // let maxWidth = controller.view.frame.width - (controller.view.frame.width - 245)
        let rect:CGRect = attrString!.boundingRect(with: CGSize(width : maxWidth, height : CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context:nil )   //hear u will get nearer height not the exact value
        let requredSize:CGRect = rect
        return requredSize         //requredSize.height  //to include button's in your tableview
    }
    
    // MARK : Conversion Of JSON To Object
    static func toObjectFromJson(jsonString : String) -> AnyObject
    {
        let data = jsonString.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if let jsonData = data {
            // Will return an object or nil if JSON decoding fails
            do {
                let objectData = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as AnyObject
                
                return objectData
            }
            catch {
                return "error" as AnyObject
            }
        } else {
            // Lossless conversion of the string was not possible
            return "error" as AnyObject
        }
    }
    
    static func useJsonFile(fileName : String) -> Any
    {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject> {
                    // do stuff
                    return jsonResult
                } else {
                    return ""
                }
            } catch let error {
                // handle error
                print("json reading error = \(error.localizedDescription)")
                return ""
            }
        } else {
            return ""
        }
    }
    
    static func loadSVGToWeb(wbvw : UIWebView, resource : String)
    {
        let path: String = Bundle.main.path(forResource: resource, ofType: "svg")!
        let url: NSURL = NSURL.fileURL(withPath: path) as NSURL  //Creating a URL which points towards our path
        //Creating a page request which will load our URL (Which points to our path)
        let request: URLRequest = URLRequest.init(url: url as URL)
        wbvw.loadRequest(request as URLRequest)
    }
    
    static func getLanguageFromUserDefaults(inputString : String) -> String
    {
        var returnStr : String = ""
        let txt = userDefault.data(forKey: "language")
        if(txt != nil) {
            let txtData = NSKeyedUnarchiver.unarchiveObject(with: txt!) as! [NSDictionary]
            for dict in txtData {
                let wI = NSMutableString( string: dict["tag_name"] as! String )
                CFStringTransform( wI, nil, "Any-Hex/Java" as NSString, true )
                let zz = wI as String
                
                if (zz == inputString) {
                    if let _ = dict[tag_Lang] as? String
                    {
                        returnStr = dict[tag_Lang] as! String
                    }
                    break
                }
                else {
                }
            }
        }
        if (returnStr == ""){
            returnStr = inputString
        }
        return returnStr
    }
    
//    static func AESEncrypt(appKey : String)
//    {
//        var key : String = Constant.SECRET_KEY
//
//        if (key.count < Constant.CIPHER_KEY_LEN) {
//            let numPad : Int = Constant.CIPHER_KEY_LEN - key.count
//            for _ in 0...numPad {
//                key += "0"
//            }
//        } else if (key.count > Constant.CIPHER_KEY_LEN) {
//            // let range = 0..<Constant.CIPHER_KEY_LEN // If you have a range
//            //key = key.substring(with: range) // Swift 3
//            // key = key.index(key.startIndex, offsetBy: Constant.CIPHER_KEY_LEN)
//        }
//    }
    
}


