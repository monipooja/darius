//
//  FPMyProfileVC.swift
//  Darious
//
//  Created by Apple on 02/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks

var Photographer_Info : NSDictionary = [:]

class FPMyProfileVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var profileIcon : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var profileImgVw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var likeLabel : UILabel!
    @IBOutlet weak var picsLabel : UILabel!
    @IBOutlet weak var albumLabel : UILabel!
    @IBOutlet weak var ratingLabel : UILabel!
    
    @IBOutlet weak var albumBtn : KGHighLightedButton!
    @IBOutlet weak var picsBtn : KGHighLightedButton!
    
    var fromDelegate = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        profileIcon.tintColor = UIColor.white
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            btnCollection[idx].alpha = 0.15
            imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_my_profile")
        albumBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_albums"), for: .normal)
        picsBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_pics"), for: .normal)
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FP_My_Profile, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    func setDataOnLoad(passDict : NSDictionary)
    {
        profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(passDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        nameLabel.text = "\(passDict["name"] as! String) (@\(passDict["nickname"] as! String))"
        placeLabel.text = passDict["address"] as? String
        albumLabel.text = "\(passDict["album_count"] as! NSNumber)"
        
        let arr1 = (passDict["pic_count"] as! String).split(separator: ",")
        let piccount = arr1[0].split(separator: "=")
        let like = arr1[1].split(separator: "=")
        
        picsLabel.text = "\(piccount[1])"
        likeLabel.text = "\(like[1])"
        
        if let _ = passDict["rating"] as? NSNumber
        {
            ratingLabel.text = String(format: "%.1f", Double.init("\(passDict["rating"] as! NSNumber)")!)
        }
    }
    
    @IBAction func myAlbums(_ sender: Any)
    {
        let albumvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPMyAlbumsVC") as! FPMyAlbumsVC
        navigationController?.pushViewController(albumvc, animated: false)
    }
    
    @IBAction func myPics(_ sender: Any)
    {
        let picsvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPMyPicsVC") as! FPMyPicsVC
        picsvc.passDict = Photographer_Info
        navigationController?.pushViewController(picsvc, animated: false)
    }
    
    @IBAction func shareFlyPicProfile(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flypic&screen_name=album") else { return }
        
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flypic")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flypic")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (fromDelegate == false)
        {
             navigationController?.popViewController(animated: false)
        }
        else {
            let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
            navigationController?.pushViewController(homevc, animated: false)
        }
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("fp my profile result = \(result)")
                Photographer_Info = result["data"] as! NSDictionary
                self.setDataOnLoad(passDict: result["data"] as! NSDictionary)
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypictopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}
