//
//  FPEditAlbumVC.swift
//  Darious
//
//  Created by Apple on 25/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPEditAlbumVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var profileIcon : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var albmNameLbl : UILabel!
    @IBOutlet weak var nameTF : UITextField!
    @IBOutlet weak var locationLbl : UILabel!
    @IBOutlet weak var locationTF : UITextField!
    @IBOutlet weak var codeLbl : UILabel!
    @IBOutlet var codeTF : [UITextField]!
    
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var notFound : UILabel!
    @IBOutlet weak var saveButton : KGHighLightedButton!
    
    @IBOutlet weak var albumTableContent : UIView!
    @IBOutlet weak var albumTableView : UITableView!
    
    var passDict : NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        profileIcon.tintColor = UIColor.white
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            if (btnCollection[idx].tag == 0) {
                btnCollection[idx].alpha = 1
                imgCollection[idx].tintColor = UIColor.white
            }
            else {
                btnCollection[idx].alpha = 0.15
                imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            }
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_edit_album")
        albmNameLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_album_name")
        locationLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_location")
        codeLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_code")
        saveButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "save"), for: .normal)
        
        for tf in codeTF
        {
            addBottomLayerWithColor(bgcolor: UIColor.gray, textF: tf)
        }
        
        nameTF.text = passDict["name"] as? String
        locationTF.text = passDict["location"] as? String
        if (passDict["code"] as! String != ""){
            let arr = Array(passDict["code"] as! String)
            var i : Int = 0
            for tf in codeTF
            {
                tf.text = "\(arr[i])"
                i += 1
            }
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    @IBAction func saveAlbum(_ sender: Any)
    {
        let codeStr = "\(codeTF[0].text!)\(codeTF[1].text!)\(codeTF[2].text!)\(codeTF[3].text!)"
        if (nameTF.text == "") {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_enter_album"), controller: self)
        }
        else if (locationTF.text == "") {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_enter_location"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&album_id=\(passDict["id"] as! NSNumber)&name=\(nameTF.text!)&location=\(locationTF.text!)&code=\(codeStr)"
            self.callService(urlstr: Api.FP_Create_Album, parameters: params, check: "save")
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("edit album result = \(result)")
                let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                StaticFunctions.showAlert(title: "", message: msg, actions: ["OK"], controller: self, completion: { (str) in
                    
                })
                customLoader.hideIndicator()
            }
        }
    }
    
    //MARK : Add bottom layer to TextFields
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:1.5)
        }
        textF.layer.addSublayer(border)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypictopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}
