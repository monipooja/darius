//
//  FPSeePhotosVC.swift
//  Darious
//
//  Created by Apple on 29/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPSeePhotosVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var profileIcon : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var nameTF : UITextField!
    @IBOutlet weak var locationTF : UITextField!
    @IBOutlet weak var notFound : UILabel!
    @IBOutlet weak var listBtn : UIButton!
    @IBOutlet weak var gridBtn : UIButton!
    
    @IBOutlet weak var albumTableContent : UIView!
    @IBOutlet weak var albumTableView : UITableView!
    
    var picsArray : [NSDictionary] = []
    var passDict : NSDictionary = [:]
    
    var yesString : String = ""
    var formString : String = "list"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        profileIcon.tintColor = UIColor.white
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            if (btnCollection[idx].tag == 0) {
                btnCollection[idx].alpha = 1
                imgCollection[idx].tintColor = UIColor.white
            }
            else {
                btnCollection[idx].alpha = 0.15
                imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            }
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        nameTF.text = passDict["name"] as? String
        locationTF.text = passDict["location"] as? String
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&album_id=\(self.passDict["id"] as! NSNumber)"
            self.callService(urlstr: Api.FP_Album_Pics, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        albumTableView.endEditing(true)
    }
    
    @IBAction func viewInList(_ sender: Any)
    {
        listBtn.setImage(#imageLiteral(resourceName: "FP_listview"), for: .normal)
        gridBtn.setImage(#imageLiteral(resourceName: "FP_gridview"), for: .normal)
        formString = "list"
    
        UIView.transition(with: albumTableView, duration: 1.0, options: .transitionFlipFromLeft, animations: {self.albumTableView.reloadData()}, completion: nil)
    }
    
    @IBAction func viewInGrid(_ sender: Any)
    {
        listBtn.setImage(#imageLiteral(resourceName: "FP_WhiteListVw"), for: .normal)
        gridBtn.setImage(#imageLiteral(resourceName: "FP_FillGridVw"), for: .normal)
        formString = "grid"
     
        UIView.transition(with: albumTableView, duration: 1.0, options: .transitionFlipFromRight, animations: {self.albumTableView.reloadData()}, completion: nil)
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
//                print("edit album result = \(result)")
                self.picsArray = result["data"] as! [NSDictionary]
                if (self.picsArray.count > 0) {
                    self.albumTableView.isHidden = false
                    self.albumTableView.reloadData()
                } else {
                    self.albumTableView.isHidden = false
                    self.notFound.isHidden = false
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    //MARK : Add bottom layer to TextFields
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:1.5)
        }
        textF.layer.addSublayer(border)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypictopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FPSeePhotosVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if (formString == "list"){
            return picsArray.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (formString == "list") {
            let cell = albumTableView.dequeueReusableCell(withIdentifier: "seephotocell") as! FPSeePhotoTabelCell
            
            let dict = picsArray[indexPath.section]
            
            let imgUrl = URL(string: "\(Api.FP_Pic_Url)\(dict["pic_name"] as! String)")
            cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "FBest_other"))
            
            cell.albumNameLbl.text = passDict["name"] as? String
            cell.dateLbl.text = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd/MM/yyyy")
            cell.likesLbl.text = "\(dict["likes"] as! NSNumber)"
            
            if (dict["likes"] as! NSNumber == 0){
                cell.likesImgVw.image = #imageLiteral(resourceName: "FP_grayheart")
            } else {
                cell.likesImgVw.image = #imageLiteral(resourceName: "FP_redheart")
            }
            
            cell.priceTF.text = dict["price"] as? String
            
            return cell
        }
        else {
            let cell = albumTableView.dequeueReusableCell(withIdentifier: "seecollectioncell") as! FPSeeCollectionCell
            
            cell.picCollectionVw.dataSource = self
            cell.picCollectionVw.delegate = self
            cell.picCollectionVw.reloadData()
        
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (formString == "list"){
            return 375
        }
        else {
            if (picsArray.count <= 3){
                return 130
            } else {
                let cnt = (picsArray.count/3) + (picsArray.count % 3)
              //  print("cntttt = \(picsArray.count/3)----\(picsArray.count % 3)----\(cnt)")
                return (CGFloat(15 + ((cnt) * 119)))
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: albumTableView.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
}

class FPSeePhotoTabelCell: UITableViewCell {
    
    @IBOutlet weak var picImageVw : UIImageView!
    @IBOutlet weak var albumNameLbl : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var likesLbl : UILabel!
    @IBOutlet weak var priceLbl : UILabel!
    @IBOutlet weak var priceTF : UITextField!
    @IBOutlet weak var likesImgVw : UIImageView!
}

class FPSeeCollectionCell: UITableViewCell {
    
    @IBOutlet weak var picCollectionVw : UICollectionView!
}

/// MAKR : CollectionView Methods
extension FPSeePhotosVC : UICollectionViewDataSource, UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "seegridcollcell", for: indexPath) as! FPSeeGridCollCell
        
        let dict = picsArray[indexPath.item]
        let imgUrl = URL(string: "\(Api.FP_Pic_Url)\(dict["pic_name"] as! String)")
        cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "FBest_other"))
        
        if (dict["price"] as! String == "") {
            cell.priceLbl.text = "free"
        }
        else {
            cell.priceLbl.text = dict["price"] as? String
        }
        cell.priceLbl.layer.cornerRadius = 4
        cell.priceLbl.layer.masksToBounds = true
        
        return cell
    }
}

class FPSeeGridCollCell: UICollectionViewCell {
    
    @IBOutlet weak var picImageVw : UIImageView!
    @IBOutlet weak var priceLbl : UILabel!
}
