//
//  FPUploadPhotoVC.swift
//  Darious
//
//  Created by Apple on 26/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import OpalImagePicker
import Photos
import DropDown

class FPUploadPhotoVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, URLSessionDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    @IBOutlet weak var topView : CustomUIView!
    
     @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var fromPC : UIButton!
    @IBOutlet weak var fromPhone : UIButton!
    
    @IBOutlet weak var topFirstView : CustomUIView!
    @IBOutlet weak var topSecondView : CustomUIView!
    @IBOutlet weak var topFirstBtn : UIButton!
    @IBOutlet weak var topSecondBtn : UIButton!
    @IBOutlet weak var topImgWd : NSLayoutConstraint!
    @IBOutlet weak var topImgHt : NSLayoutConstraint!
    
    var progressbar : UIProgressView!

    var passAlbumid : String = ""
    var selectAlbumId : String = ""
    let dropDown = DropDown()
    
    var selectImgArray : [UIImage] = []
    var albumNameArr : [String] = []
    var albumIdArr : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            if (btnCollection[idx].tag == 2) {
                btnCollection[idx].alpha = 1
                imgCollection[idx].image = #imageLiteral(resourceName: "FP_comp_white")
            }
            else {
                btnCollection[idx].alpha = 0.15
                imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            }
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            topImgWd.constant = 80
            topImgHt.constant = 80
            topFirstView.layer.cornerRadius = 40
            topSecondView.layer.cornerRadius = 40
            topFirstBtn.layer.cornerRadius = 34
            topSecondBtn.layer.cornerRadius = 34
        }
        else if (self.view.frame.width == 375) && (self.view.frame.height == 667)
        {
            topImgWd.constant = 100
            topImgHt.constant = 100
            topFirstView.layer.cornerRadius = 50
            topSecondView.layer.cornerRadius = 50
            topFirstBtn.layer.cornerRadius = 44
            topSecondBtn.layer.cornerRadius = 44
        }else {
            topImgWd.constant = 130
            topImgHt.constant = 130
            topFirstView.layer.cornerRadius = 65
            topSecondView.layer.cornerRadius = 65
            topFirstBtn.layer.cornerRadius = 59
            topSecondBtn.layer.cornerRadius = 59
        }
        
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_upload_pics")
        fromPC.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_from_pc"), for: .normal)
        fromPhone.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_from_smartphone"), for: .normal)
        
        if (passAlbumid == "") {
            topView.isHidden = false
            DispatchQueue.main.async {
                customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
                self.callService(urlstr: Api.FP_My_Album, parameters: params, check: "get")
            }
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func fromPC(_ sender: Any)
    {
        if (passAlbumid == "") {
            showDropDown(check: "pc")
        }
        else {
            pcOrPhone(passStr: "pc")
        }
    }
    
    @IBAction func fromPhone(_ sender: Any)
    {
        if (passAlbumid == "") {
            showDropDown(check: "phone")
        }
        else {
            pcOrPhone(passStr: "phone")
        }
    }
    
    func pcOrPhone(passStr : String)
    {
        if (passStr == "pc") {
            let nextvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPUploadPcVC") as! FPUploadPcVC
            navigationController?.pushViewController(nextvc, animated: false)
        }
        else {
            let actionSheet = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
            
            let subview = actionSheet.view.subviews.first! as UIView
            let alertContentView = subview.subviews.first! as UIView
            alertContentView.backgroundColor = UIColor.init(hexString: "212121")
            actionSheet.view.tintColor = UIColor.init(hexString: "212121")
            alertContentView.layer.cornerRadius = 15
            
            //to change font of title and message.
            let titleFont = [NSAttributedString.Key.font: UIFont(name: "OpenSans-Semibold", size: 20.0)!]
            let titleAttrString = NSMutableAttributedString(string: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_upload_photos"), attributes: titleFont)
            actionSheet.setValue(titleAttrString, forKey: "attributedTitle")
            
            let action1 = UIAlertAction.init(title: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_choose_photos"), style: .default) { (action) in
                
                let imagePicker = OpalImagePickerController()
                imagePicker.imagePickerDelegate = self
                //Only allow image media type assets
                imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
                imagePicker.selectionImageTintColor = UIColor.white
                self.present(imagePicker, animated: true, completion: nil)
            }
            let icon1 = #imageLiteral(resourceName: "fp_gallery").imageWithSize(scaledToSize: CGSize(width: 28, height: 28))
            action1.setValue(icon1, forKey: "image")
            
            let action2 = UIAlertAction.init(title: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_take_photo"), style: .default) { (action) in
                
                let imagePicker = UIImagePickerController()
                imagePicker.sourceType = UIImagePickerController.SourceType.camera;
                imagePicker.allowsEditing = false
                imagePicker.delegate = self
                self.present(imagePicker, animated: true, completion: nil)
            }
            let icon2 = #imageLiteral(resourceName: "fp_graycamera").imageWithSize(scaledToSize: CGSize(width: 32, height: 32))
            action2.setValue(icon2, forKey: "image")
            
            let action3 = UIAlertAction.init(title: ApiResponse.getLanguageFromUserDefaults(inputString: "cancel"), style: .default) { (action) in
            }
            action1.setValue(UIColor.init(hexString: "212121"), forKey: "titleTextColor")
            action2.setValue(UIColor.init(hexString: "212121"), forKey: "titleTextColor")
            //  action2.setValue(#imageLiteral(resourceName: "SelectCircle").withRenderingMode(.alwaysOriginal), forKey: "image")
            actionSheet.addAction(action1)
            actionSheet.addAction(action2)
            actionSheet.addAction(action3)
            present(actionSheet, animated: true, completion: nil)
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var image = info[.originalImage] as! UIImage
        if let _ = fixedOrientation(passImg: image)
        {
            image = fixedOrientation(passImg: image)!
        }
        selectImgArray = [image]
        
        self.dismiss(animated: true, completion: nil)
       
        let nextvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPSelectedPhotoVC") as! FPSelectedPhotoVC
        nextvc.passImgarray = selectImgArray
        if (passAlbumid != ""){
            nextvc.album_id = passAlbumid
        }
        else {
            nextvc.album_id = selectAlbumId
        }
        navigationController?.pushViewController(nextvc, animated: false)
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                if (check == "get") {
                    let albumArray = result["data"] as! [NSDictionary]
                    if (albumArray.count > 0) {
                        for dict in albumArray
                        {
                            self.albumNameArr.append(dict["name"] as! String)
                            self.albumIdArr.append("\(dict["id"] as! NSNumber)")
                        }
                    }
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypictopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
    // Show DropDwon
    func showDropDown(check : String)
    {
        if (check == "phone") {
            dropDown.anchorView = fromPhone
        } else {
            dropDown.anchorView = fromPC
        }
        dropDown.dataSource = albumNameArr
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 25, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            cell.flagImage.image = nil
            cell.codeLabel.text = ""
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            self.selectAlbumId = self.albumIdArr[index]
            if (check == "phone") {
                self.pcOrPhone(passStr: "phone")
            } else {
                self.pcOrPhone(passStr: "pc")
            }
        }
        dropDown.width = self.view.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    /////////////
    func fixedOrientation(passImg : UIImage) -> UIImage? {
        
        guard passImg.imageOrientation != UIImage.Orientation.up else {
            //This is default orientation, don't need to do anything
            return passImg.copy() as? UIImage
        }
        
        guard let cgImage = passImg.cgImage else {
            //CGImage is not available
            return nil
        }
        // let colorSpace = cgImage.colorSpace,
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(passImg.size.width), height: Int(passImg.size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            
            return nil //Not able to create CGContext
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch passImg.imageOrientation {
            
        case .down, .downMirrored:
            
            transform = transform.translatedBy(x: passImg.size.width, y: passImg.size.height)
            
            transform = transform.rotated(by: CGFloat.pi)
            
            break
            
        case .left, .leftMirrored:
            
            transform = transform.translatedBy(x: passImg.size.width, y: 0)
            
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            
            break
            
        case .right, .rightMirrored:
            
            transform = transform.translatedBy(x: 0, y: passImg.size.height)
            
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            
            break
            
        case .up, .upMirrored:
            
            break
            
        }
        
        //Flip image one more time if needed to, this is to prevent flipped image
        
        switch passImg.imageOrientation {
            
        case .upMirrored, .downMirrored:
            
            transform.translatedBy(x: passImg.size.width, y: 0)
            
            transform.scaledBy(x: -1, y: 1)
            
            break
            
        case .leftMirrored, .rightMirrored:
            
            transform.translatedBy(x: passImg.size.height, y: 0)
            
            transform.scaledBy(x: -1, y: 1)
            
        case .up, .down, .left, .right:
            
            break
        }
        
        ctx.concatenate(transform)
        
        switch passImg.imageOrientation {
            
        case .left, .leftMirrored, .right, .rightMirrored:
            
            ctx.draw(passImg.cgImage!, in: CGRect(x: 0, y: 0, width: passImg.size.height, height: passImg.size.width))
            
        default:
            
            ctx.draw(passImg.cgImage!, in: CGRect(x: 0, y: 0, width: passImg.size.width, height: passImg.size.height))
            
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}

extension FPUploadPhotoVC : OpalImagePickerControllerDelegate
{
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    {
        selectImgArray = images
        picker.dismiss(animated: true, completion: nil)
        
        let nextvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPSelectedPhotoVC") as! FPSelectedPhotoVC
        nextvc.passImgarray = selectImgArray
        if (passAlbumid != ""){
            nextvc.album_id = passAlbumid
        }
        else {
            nextvc.album_id = selectAlbumId
        }
        navigationController?.pushViewController(nextvc, animated: false)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController)
    {
    }
}

extension UIImage {
    
    func imageWithSize(scaledToSize newSize: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}
