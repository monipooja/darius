//
//  FPEditPhotoVC.swift
//  Darious
//
//  Created by Apple on 25/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks

class FPEditPhotoVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var profileIcon : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var listBtn : UIButton!
    @IBOutlet weak var gridBtn : UIButton!
    
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var albmNameLbl : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var placeLbl : UILabel!
    @IBOutlet var codeLbls : [UILabel]!
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var currencyLbl : UILabel!
    @IBOutlet weak var notFound : UILabel!
    @IBOutlet weak var allLbl : UILabel!
    @IBOutlet weak var priceLbl : UILabel!
    
    // Grid View Outlets
    @IBOutlet weak var editGridBtn : UIButton!
    @IBOutlet weak var rightGridBtn : UIButton!
    @IBOutlet weak var checkBtn : UIButton!
    @IBOutlet weak var priceGridTF : UITextField!
    @IBOutlet weak var gridEditVw : UIView!
    @IBOutlet weak var gridPriceVw : UIView!
    
    @IBOutlet weak var albumTableContent : UIView!
    @IBOutlet weak var albumTableView : UITableView!
    
    @IBOutlet weak var blurView : UIView!
    @IBOutlet weak var centerView : CustomUIView!
    @IBOutlet weak var centerMsgLbl : UILabel!
    
    @IBOutlet weak var noBtn : KGHighLightedButton!
    @IBOutlet weak var yesBtn : KGHighLightedButton!
    
    var picsArray : [NSDictionary] = []
    var priceEnable : [Bool] = []
    var picId_array : [String] = []
    var picPrc_array : [String] = []
    var gridSelectIndex : [Int] = []
    var free_checkbox : [Int] = []
    
    var passDict : NSDictionary = [:]
    
    var selectedCell : FPEditPhotoTabelCell!
    var selectDict : NSDictionary = [:]
    var selectIndex : Int = 0
    
    var yesString : String = ""
    var formString : String = "list"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        profileIcon.tintColor = UIColor.white
        currencyLbl.text = "\(Current_Currency)"
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            if (btnCollection[idx].tag == 0) {
                btnCollection[idx].alpha = 1
                imgCollection[idx].tintColor = UIColor.white
            }
            else {
                btnCollection[idx].alpha = 0.15
                imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            }
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        setDataOnLoad()
        
        if (formString == "list"){
            gridEditVw.isHidden = true
            albumTableContent.frame.size = CGSize(width: albumTableContent.frame.width, height: 300)
        } else {
            gridEditVw.isHidden = false
            albumTableContent.frame.size = CGSize(width: albumTableContent.frame.width, height: 428)
        }
        
        notFound.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_albums")
        allLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_all")
        priceLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_price")
        noBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no"), for: .normal)
        yesBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "yes"), for: .normal)
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&album_id=\(self.passDict["id"] as! NSNumber)"
            self.callService(urlstr: Api.FP_Album_Pics, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        albumTableView.endEditing(true)
    }
    
    func setDataOnLoad()
    {
        albmNameLbl.text = "\(passDict["name"] as! String)"
        dateLbl.text = "\((passDict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd/MM/yyyy")!)"
        placeLbl.text = "\(passDict["location"] as! String)"
        
        for lbl in codeLbls
        {
            addBottomLayerWithColor(bgcolor: UIColor.gray, lbl: lbl)
        }
    }
    
    @IBAction func showHideCode(_ sender: Any)
    {
        if (codeLbls[0].text == "*") {
            if (passDict["code"] as! String != ""){
                let arr = Array(passDict["code"] as! String)
                var i : Int = 0
                for lbl in codeLbls
                {
                    lbl.font = UIFont.systemFont(ofSize: 15)
                    lbl.text = "\(arr[i])"
                    i += 1
                }
            }
            else {
                for lbl in codeLbls
                {
                    lbl.text = "0"
                    lbl.font = UIFont.systemFont(ofSize: 15)
                }
            }
        }
        else {
            for lbl in codeLbls
            {
                lbl.font = UIFont.init(name: "SegoeScript-Bold", size: 16)
                lbl.text = "*"
            }
        }
    }
    
    @IBAction func viewInList(_ sender: Any)
    {
        gridEditVw.isHidden = true
        albumTableContent.frame.size = CGSize(width: albumTableContent.frame.width, height: 300)
        listBtn.setImage(#imageLiteral(resourceName: "FP_listview"), for: .normal)
        gridBtn.setImage(#imageLiteral(resourceName: "FP_gridview"), for: .normal)
        formString = "list"
        
        UIView.transition(with: albumTableView, duration: 1.0, options: .transitionFlipFromLeft, animations: {self.albumTableView.reloadData()}, completion: nil)
    }
    
    @IBAction func viewInGrid(_ sender: Any)
    {
        gridEditVw.isHidden = false
        albumTableContent.frame.size = CGSize(width: albumTableContent.frame.width, height: 428)
        listBtn.setImage(#imageLiteral(resourceName: "FP_WhiteListVw"), for: .normal)
        gridBtn.setImage(#imageLiteral(resourceName: "FP_FillGridVw"), for: .normal)
        formString = "grid"
        
        UIView.transition(with: albumTableView, duration: 1.0, options: .transitionFlipFromRight, animations: {self.albumTableView.reloadData()}, completion: nil)
    }
   
    @IBAction func editGridView(_ sender: Any)
    {
        editGridBtn.backgroundColor = ColorCode.FPEditBgGray
        rightGridBtn.setImage(#imageLiteral(resourceName: "FP_greenright"), for: .normal)
        gridPriceVw.isHidden = false
        priceGridTF.becomeFirstResponder()
        priceGridTF.text = ""
    }
    
    @IBAction func checkAll(_ sender: Any)
    {
        if (picsArray.count > 0)
        {
            picId_array = []
            gridSelectIndex = []
            if ((sender as! UIButton).image(for: .normal) == nil)
            {
                (sender as! UIButton).setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
                (sender as! UIButton).backgroundColor = ColorCode.FBThemeColor
                var ii = 0
                for dict in picsArray
                {
                    picId_array.append("\(dict["id"] as! NSNumber)")
                    gridSelectIndex.append(ii)
                    ii += 1
                }
            }
            else {
                (sender as! UIButton).setImage(nil, for: .normal)
                (sender as! UIButton).backgroundColor = UIColor.white
            }
            albumTableView.reloadData()
        }
    }
    
    @IBAction func saveGridView(_ sender: Any)
    {
        if (picId_array.count == 0) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_select_photos_to_update"), controller: self)
        }
        else if (gridPriceVw.isHidden == true) {
           // ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_enter_price"), controller: self)
        }
        else if (priceGridTF.text?.trimmingCharacters(in: CharacterSet.whitespaces) == "") {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_enter_price"), controller: self)
        }
        else {
            for _ in picId_array
            {
                picPrc_array.append(priceGridTF.text!)
            }
            
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = ["access_token":"\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "pic_id": picId_array, "price": picPrc_array] as [String : Any]
            
            ApiResponse.onResponsePost(url: Api.FP_Update_Pic, parms: params as NSDictionary, controller: self) { (result) in
                
                OperationQueue.main.addOperation {
                    customLoader.hideIndicator()
                    
                    for idx in self.gridSelectIndex
                    {
                        let editdict = NSMutableDictionary.init(dictionary: self.picsArray[idx])
                        editdict["price"] = self.priceGridTF.text
                        self.picsArray[idx] = editdict
                    }
                    self.picId_array = []
                    self.gridSelectIndex = []
                    self.priceGridTF.isEnabled = true
                    self.editGridBtn.backgroundColor = ColorCode.FPEditBgGreen
                    self.rightGridBtn.setImage(#imageLiteral(resourceName: "FP_grayright"), for: .normal)
                    self.priceGridTF.resignFirstResponder()
                    self.gridPriceVw.isHidden = true
                    self.priceGridTF.text = ""
                    self.albumTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
                }
            }
        }
    }
    
    @IBAction func deleteGridView(_ sender: Any)
    {
        if (picId_array.count == 0) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_select_photos_to_delete"), controller: self)
        }
        else {
            blurView.isHidden = false
            centerView.isHidden = false
        }
    }
    
    @IBAction func onSelectNo(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
        
        if (formString == "list") {
            picId_array = []
        }else {
        }
    }
    
    @IBAction func onSelectYes(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
        
        if (formString == "list") {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = ["access_token":"\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "pic_id": picId_array] as [String : Any]
            
            ApiResponse.onResponsePost(url: Api.FP_Delete_Pic, parms: params as NSDictionary, controller: self) { (result) in
                
                OperationQueue.main.addOperation {
                    let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msg, actions: ["OK"], controller: self, completion: { (str) in
                        
                        self.viewDidLoad()
                    })
                }
            }
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = ["access_token":"\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "pic_id": picId_array] as [String : Any]
            
            ApiResponse.onResponsePost(url: Api.FP_Delete_Pic, parms: params as NSDictionary, controller: self) { (result) in
                
                OperationQueue.main.addOperation {
                    let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msg, actions: ["OK"], controller: self, completion: { (str) in
                        
                        self.picId_array = []
                        self.gridSelectIndex = []
                        self.checkBtn.setImage(nil, for: .normal)
                        self.checkBtn.backgroundColor = UIColor.white
                        self.viewDidLoad()
                    })
                }
            }
        }
    }
    
    @IBAction func shareFlyPicProfile(_ sender: Any)
    {
        let name = albmNameLbl.text!.replacingOccurrences(of: " ", with: "%20")
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flypic&screen_name=album&albumId=\(passDict["id"] as! NSNumber)&albumName=\(name)") else { return }
        
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flypic")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flypic")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    //MARK : Add bottom layer to TextFields
    func addBottomLayerWithColor(bgcolor : UIColor, lbl : UILabel)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:lbl.frame.height+1, width:lbl.frame.width, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:lbl.frame.height+1, width:lbl.frame.width, height:1.5)
        }
        lbl.layer.addSublayer(border)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                //print("album photos result = \(result)")
                if (check == "get") {
                    self.picsArray = result["data"] as! [NSDictionary]
                    if (self.picsArray.count > 0) {
                        for _ in 0...self.picsArray.count-1
                        {
                            self.priceEnable.append(false)
                        }
                        self.albumTableView.isHidden = false
                        self.albumTableView.reloadData()
                    } else {
                        self.albumTableView.isHidden = false
                        self.albumTableView.reloadData()
                        self.listBtn.isHidden = true
                        self.gridBtn.isHidden = true
                        self.gridEditVw.isHidden = true
                        self.notFound.isHidden = false
                    }
                }
                else if (check == "delete") {
                    let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msg, actions: ["OK"], controller: self, completion: { (str) in
                        
                        self.viewDidLoad()
                    })
                }
                else if (check == "update") {
                    
                    let editdict = NSMutableDictionary.init(dictionary: self.selectDict)
                    editdict["price"] = self.selectedCell?.priceTF.text
                    self.picsArray[self.selectIndex] = editdict
                    self.selectedCell?.priceTF.isEnabled = false
                    self.selectedCell?.editButton.backgroundColor = ColorCode.FPEditBgGreen
                    self.selectedCell?.saveButton.setImage(#imageLiteral(resourceName: "FP_grayright"), for: .normal)
                    self.selectedCell?.priceTF.resignFirstResponder()
                }
                else {
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    //MARK : Add bottom layer to TextFields
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:1.5)
        }
        textF.layer.addSublayer(border)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypictopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FPEditPhotoVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if (formString == "list"){
            return picsArray.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (formString == "list") {
            let cell = albumTableView.dequeueReusableCell(withIdentifier: "editphotocell") as! FPEditPhotoTabelCell
            
            let dict = picsArray[indexPath.section]
            
            let imgUrl = URL(string: "\(Api.FP_Original_Url)\(dict["original_pic"] as! String)")
            cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            
            cell.albumNameLbl.text = passDict["name"] as? String
            cell.dateLbl.text = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd/MM/yyyy")! + " \(passDict["location"] as! String)"
            cell.likesLbl.text = "\(dict["likes"] as! NSNumber)"
            
            
            cell.priceLbl.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_price")) :"
            
            if (dict["price"] as! String == "free")
            {
                cell.priceTF.text = ApiResponse.getLanguageFromUserDefaults(inputString: "\(dict["price"] as! String)")
                cell.currencyLbl.text = ""
            } else {
                cell.currencyLbl.text = "\(Current_Currency)"
                cell.priceTF.text = dict["price"] as? String
            }
            
            cell.saveButton.addTarget(self, action: #selector(updatePhotoPrice(_:)), for: .touchUpInside)
            cell.deleteButton.addTarget(self, action: #selector(deletePhoto(_:)), for: .touchUpInside)
            cell.editButton.addTarget(self, action: #selector(editPhotoPrice(_:)), for: .touchUpInside)
            
            if (free_checkbox.contains(indexPath.section))
            {
                cell.currencyLbl.text = ""
                cell.priceTF.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_free"))"
                cell.freeButton.layer.borderWidth = 0
                cell.freeButton.backgroundColor = ColorCode.FPicThemeColor
                cell.freeButton.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
            }
            else {
                cell.freeButton.layer.borderWidth = 1
                cell.freeButton.layer.borderColor = UIColor.lightGray.cgColor
                cell.freeButton.backgroundColor = UIColor.white
                cell.freeButton.setImage(nil, for: .normal)
                
                cell.priceTF.isEnabled = priceEnable[indexPath.section]
            }
            cell.freeLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_free"))"
            cell.freeButton.layer.cornerRadius = 4
            cell.freeButton.addTarget(self, action: #selector(setFree(_:)), for: .touchUpInside)
            
            return cell
        }
        else {
            let cell = albumTableView.dequeueReusableCell(withIdentifier: "collectioncell") as! FPEditCollectionCell
            
            if (picsArray.count > 0)
            {
                cell.picCollectionVw.isHidden = false
            }
            else {
                cell.picCollectionVw.isHidden = true
            }
            cell.picCollectionVw.dataSource = self
            cell.picCollectionVw.delegate = self
            cell.picCollectionVw.reloadData()
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (formString == "list"){
            return 375
        }
        else {
            if (picsArray.count <= 3){
                return 130
            } else {
                let cnt = (picsArray.count/3) + (picsArray.count % 3)
                return (CGFloat(15 + ((cnt) * 119)))
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: albumTableView.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func editPhotoPrice(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        let cell = albumTableView.cellForRow(at: idxpath) as? FPEditPhotoTabelCell
        
        cell?.priceUnderline.isHidden = false
        cell?.priceTF.isEnabled = true
        cell?.editButton.isHidden = true
        cell?.saveButton.isHidden = false
        cell?.priceTF.becomeFirstResponder()
        if (idxpath.section == picsArray.count-1)
        {
            albumTableView.contentSize = CGSize(width: albumTableView.contentSize.width, height: albumTableView.contentSize.height + 100)
        }
    }
    
    @objc func updatePhotoPrice(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        let dict = picsArray[idxpath.section]
        
        let cell = albumTableView.cellForRow(at: idxpath) as? FPEditPhotoTabelCell
        selectedCell = cell
        selectDict = dict
        selectIndex = idxpath.section
        
        if (idxpath.section == picsArray.count-1)
        {
            albumTableView.contentSize = CGSize(width: albumTableView.contentSize.width, height: albumTableView.contentSize.height - 100)
        }
        cell?.priceTF.resignFirstResponder()
        
        picId_array = ["\(dict["id"] as! NSNumber)"]
        picPrc_array = ["\((cell?.priceTF.text)!)"]
        
        customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
        let params = ["access_token":"\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "pic_id": picId_array, "price": picPrc_array] as [String : Any]
        
        ApiResponse.onResponsePost(url: Api.FP_Update_Pic, parms: params as NSDictionary, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
                customLoader.hideIndicator()
                let editdict = NSMutableDictionary.init(dictionary: self.selectDict)
                editdict["price"] = self.selectedCell?.priceTF.text
                self.picsArray[self.selectIndex] = editdict
                self.selectedCell?.priceTF.isEnabled = false
                self.selectedCell?.editButton.isHidden = false
                self.selectedCell?.saveButton.isHidden = true
                cell?.priceUnderline.isHidden = true
                self.selectedCell?.priceTF.resignFirstResponder()
                self.picId_array = []
                self.picPrc_array = []
                if (cell?.freeButton.backgroundColor != ColorCode.FPicThemeColor)
                {
                    cell?.currencyLbl.text = "\(Current_Currency)"
                }
            }
        }
    }
    
    @objc func deletePhoto(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        let dict = picsArray[idxpath.section]
        
        picId_array = ["\(dict["id"] as! NSNumber)"]
        
        blurView.isHidden = false
        centerView.isHidden = false
    }
    
    @objc func setFree(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        
        let cell = albumTableView.cellForRow(at: idxpath) as? FPEditPhotoTabelCell
        
        if (cell?.freeButton.image(for: .normal) == #imageLiteral(resourceName: "FP_imgchecked"))
        {
            if (free_checkbox.contains(idxpath.section) == true){
                let arr = NSArray.init(array: free_checkbox)
                let ii = arr.index(of: idxpath.section)
                free_checkbox.remove(at: ii)
            }
            cell?.priceTF.text = ""
            cell?.freeButton.layer.borderWidth = 1
            cell?.freeButton.backgroundColor = UIColor.white
            cell?.freeButton.setImage(nil, for: .normal)
            
            cell?.currencyLbl.text = "\(Current_Currency)"
        }
        else {
            free_checkbox.append(idxpath.section)
            cell?.priceTF.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_free"))"
            cell?.freeButton.layer.borderWidth = 0
            cell?.freeButton.backgroundColor = ColorCode.FPicThemeColor
            cell?.freeButton.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
            
            cell?.currencyLbl.text = ""
        }
    }
}

class FPEditPhotoTabelCell: UITableViewCell {
    
    @IBOutlet weak var picImageVw : UIImageView!
    @IBOutlet weak var albumNameLbl : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var likesLbl : UILabel!
    @IBOutlet weak var priceLbl : UILabel!
    @IBOutlet weak var priceTF : UITextField!
    @IBOutlet weak var likesImgVw : UIImageView!
    @IBOutlet weak var currencyLbl : UILabel!
    @IBOutlet weak var priceUnderline : UILabel!
    
    @IBOutlet weak var saveButton : KGHighLightedButton!
    @IBOutlet weak var deleteButton : KGHighLightedButton!
    @IBOutlet weak var editButton : UIButton!
    @IBOutlet weak var freeButton : UIButton!
    @IBOutlet weak var freeLabel : UILabel!
}

class FPEditCollectionCell: UITableViewCell {
    
    @IBOutlet weak var picCollectionVw : UICollectionView!
}

/// MAKR : CollectionView Methods
extension FPEditPhotoVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridcollcell", for: indexPath) as! FPGridCollectionCell
        
        let dict = picsArray[indexPath.item]
        let imgUrl = URL(string: "\(Api.FP_Original_Url)\(dict["original_pic"] as! String)")
        cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        
        if (dict["price"] as! String == "") {
            cell.priceLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_free")
        }
        else {
            cell.priceLbl.text = dict["price"] as? String
        }
        cell.priceLbl.layer.cornerRadius = 4
        cell.priceLbl.layer.masksToBounds = true
        
        if (picId_array.contains("\(dict["id"] as! NSNumber)") == true) {
            cell.selectButton.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
        }
        else {
            cell.selectButton.setImage(nil, for: .normal)
        }
        cell.selectButton.tag = indexPath.item
        cell.selectButton.addTarget(self, action: #selector(selectPic(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func selectPic(_ sender : UIButton)
    {
        let dict = picsArray[sender.tag]
     //   selectDict = dict
        if (sender.image(for: .normal) == nil)
        {
            sender.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
            picId_array.append("\(dict["id"] as! NSNumber)")
            gridSelectIndex.append(sender.tag)
        }
        else {
            sender.setImage(nil, for: .normal)
            if (picId_array.contains("\(dict["id"] as! NSNumber)"))
            {
                let arr = NSArray.init(array: picId_array)
                let index = arr.index(of: "\(dict["id"] as! NSNumber)")
                picId_array.remove(at: index)
                gridSelectIndex.remove(at: index)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            return CGSize(width: 100, height: 100)
        }
        else if (self.view.frame.width == 375)
        {
            return CGSize(width: 110, height: 110)
        }
        else if(self.view.frame.width == 414) {
            return CGSize(width: 120, height: 120)
        }else {
            return CGSize(width: 130, height: 130)
        }
    }
    
}

class FPGridCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var picImageVw : UIImageView!
    @IBOutlet weak var priceLbl : UILabel!
    @IBOutlet weak var selectButton : UIButton!
}
