//
//  FPSelectedPhotoVC.swift
//  Darious
//
//  Created by Apple on 28/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import Alamofire

class FPSelectedPhotoVC: UIViewController, URLSessionDelegate {

    @IBOutlet weak var pictTableView : UITableView!
    @IBOutlet weak var contentTableVw : UIView!
    
    @IBOutlet weak var topTitleLbl : UILabel!
    @IBOutlet weak var saveBtn : KGHighLightedButton!
    
    var passImgarray : [UIImage] = []
    var htAarray : [CGFloat] = []
    var priceArray : [String] = []
    
    var album_id : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        for _ in passImgarray
        {
            htAarray.append(325)
            priceArray.append("free")
        }
        
        topTitleLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_upload_photos")
        saveBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "save"), for: .normal)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func onSavePhotos3(_ sender: Any)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
        uploadMultiplePhotos()
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    /// MARK : Multipart (file format)
    func uploadMultiplePhotos()
    {
        let urlString = Api.FP_Upload_Photos
        
        var prcStr : String = ""
        for prc in priceArray
        {
            if (prcStr == "") {
                prcStr = prcStr + prc
            }
            else {
                prcStr = prcStr + ",\(prc)"
            }
        }
        
        var imagesData : [Data] = []
        for img in passImgarray
        {
            let image_data = img.jpegData(compressionQuality: 0.2)
            imagesData.append(image_data!)
        }
        
        let parameters : [String : String] = ["access_token":"\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "album_id":"\(album_id)", "price" : prcStr]
       
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            for imageData in imagesData {
                multipartFormData.append(imageData, withName: "img[", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: urlString,
           
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    let ddict = try! JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! NSDictionary
                    
                    let status = ddict["status"] as! NSNumber
                    
                    if (status == 1) {
                        customLoader.hideIndicator()
                        let albumvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPMyAlbumsVC") as! FPMyAlbumsVC
                        self.navigationController?.pushViewController(albumvc, animated: false)
                    }
                    else if(status == 3){
                        customLoader.hideIndicator()
                        Access_token = ""
                        userDefault.set(Access_token, forKey: "token")
                        userDefault.synchronize()
                        let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.navigationController?.pushViewController(VC, animated: true)
                    }
                    else {
                        if let str = ddict["message"] {
                            let msgStr = str as! String
                            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                            if(showMsg != "") {
                                ApiResponse.alert(title: "", message: showMsg, controller: self)
                            }
                            else {
                                ApiResponse.alert(title: "", message: msgStr, controller: self)
                            }
                        }
                        else {
                            customLoader.hideIndicator()
                        }
                    }
                    
                }
            case .failure(let error):
                //print(error.localizedDescription)
                ApiResponse.alert(title: "", message: error.localizedDescription, controller: self)
                customLoader.hideIndicator()
            }
        })
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        
        // let size = Float(totalBytesSent/totalBytesExpectedToSend)
        // print("sizeee = \(size)")
    }

}

extension FPSelectedPhotoVC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if (textField.tag == passImgarray.count-1) && (passImgarray.count > 1){
            view.frame.origin.y = -200
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string != ""){
            priceArray[textField.tag] = "\(textField.text!)\(string)"
        } else {
            let _ = textField.text?.removeLast()
            priceArray[textField.tag] = "\(textField.text!)"
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
     //   priceArray[textField.tag] = textField.text!
        
        if (textField.tag == passImgarray.count-1) {
            view.frame.origin.y = 0
        }
    }
}

extension FPSelectedPhotoVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return passImgarray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = pictTableView.dequeueReusableCell(withIdentifier: "showpiccell") as! SelectedPhotoCell
        
        cell.picImageVw.image = passImgarray[indexPath.section]
        cell.picImageVw.backgroundColor = UIColor.white
       // cell.picImageVw.roundCornersForAspectFit(radius: 20)
        
        cell.priceTF.tag = indexPath.section
        if (priceArray[indexPath.section] == "free") {
            cell.priceTF.text = ""
        } else {
            cell.priceTF.text = priceArray[indexPath.section]
        }
        
        let rect = CGRect(x:0, y:cell.priceTF.frame.height-2, width:cell.priceTF.frame.width, height:0.8)
        StaticFunctions.addBorderOnLayer(bgColor: UIColor.gray, vwFrame: rect, layer: cell.priceTF.layer)
        
        if (htAarray[indexPath.section] == 325)
        {
            cell.freeBtn.backgroundColor = ColorCode.FPicThemeColor
            cell.freeBtn.setTitleColor(UIColor.white, for: .normal)
            cell.priceBtn.backgroundColor = UIColor.white
            cell.priceBtn.setTitleColor(UIColor.black, for: .normal)
            
            cell.priceLabel.isHidden = true
            cell.priceTF.isHidden = true
        }
        else {
            cell.priceBtn.backgroundColor = ColorCode.FPicThemeColor
            cell.priceBtn.setTitleColor(UIColor.white, for: .normal)
            cell.freeBtn.backgroundColor = UIColor.white
            cell.freeBtn.setTitleColor(UIColor.black, for: .normal)
            
            cell.priceLabel.isHidden = false
            cell.priceTF.isHidden = false
        }
        cell.freeBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "ff_free"), for: .normal)
        cell.priceBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_price"), for: .normal)
        
        cell.freeBtn.addTarget(self, action: #selector(freePic(_:)), for: .touchUpInside)
        cell.priceBtn.addTarget(self, action: #selector(priceSet(_:)), for: .touchUpInside)
        cell.deleteBtn.addTarget(self, action: #selector(deletePic(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return htAarray[indexPath.section]
    }
    
    @objc func freePic(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: pictTableView)
        let idxPath = pictTableView.indexPathForRow(at: point)!
        let cell = pictTableView.cellForRow(at: idxPath) as? SelectedPhotoCell
        
        cell?.freeBtn.backgroundColor = ColorCode.FPicThemeColor
        cell?.freeBtn.setTitleColor(UIColor.white, for: .normal)
        cell?.priceBtn.backgroundColor = UIColor.white
        cell?.priceBtn.setTitleColor(UIColor.black, for: .normal)
        htAarray[idxPath.section] = 325
        pictTableView.reloadRows(at: [idxPath], with: .none)
        
        priceArray[idxPath.section] = "free"
    }
    
    @objc func priceSet(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: pictTableView)
        let idxPath = pictTableView.indexPathForRow(at: point)!
        let cell = pictTableView.cellForRow(at: idxPath) as? SelectedPhotoCell

        cell?.priceBtn.backgroundColor = ColorCode.FPicThemeColor
        cell?.priceBtn.setTitleColor(UIColor.white, for: .normal)
        cell?.freeBtn.backgroundColor = UIColor.white
        cell?.freeBtn.setTitleColor(UIColor.black, for: .normal)
        htAarray[idxPath.section] = 360
        pictTableView.reloadRows(at: [idxPath], with: .none)
        
        priceArray[idxPath.section] = "0"
    }
    
    @objc func deletePic(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: pictTableView)
        let idxPath = pictTableView.indexPathForRow(at: point)!
        
        passImgarray.remove(at: idxPath.section)
        htAarray.remove(at: idxPath.section)
        priceArray.remove(at: idxPath.section)
        pictTableView.reloadData()
    }
}

class SelectedPhotoCell: UITableViewCell {
    
    @IBOutlet weak var picImageVw : UIImageView!
    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var priceTF : UILabel!
    
    @IBOutlet weak var freeBtn : KGHighLightedButton!
    @IBOutlet weak var priceBtn : KGHighLightedButton!
    @IBOutlet weak var deleteBtn : KGHighLightedButton!
    
}
