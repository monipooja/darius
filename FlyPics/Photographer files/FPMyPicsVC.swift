//
//  FPMyPicsVC.swift
//  Darious
//
//  Created by Apple on 24/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPMyPicsVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var profileIcon : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var profileImgVw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var likeLabel : UILabel!
    @IBOutlet weak var picsLabel : UILabel!
    @IBOutlet weak var albumLabel : UILabel!
    @IBOutlet weak var ratingLabel : UILabel!
    
    @IBOutlet weak var checkBtn : UIButton!
    @IBOutlet weak var allLbl : UILabel!
    @IBOutlet weak var topTitle : UILabel!
    
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var noFound : UILabel!
    
    @IBOutlet weak var picsCollVw : UICollectionView!
    
    @IBOutlet weak var blurView : UIView!
    @IBOutlet weak var centerView : CustomUIView!
    @IBOutlet weak var centerMsgLbl : UILabel!
    
    @IBOutlet weak var noBtn : KGHighLightedButton!
    @IBOutlet weak var yesBtn : KGHighLightedButton!
    
    var dataArray : [NSDictionary] = []
    var picId_array : [String] = []
    
    var passDict : NSDictionary = [:]
    
    var delete_check : String = ""
    var selected_index : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        profileIcon.tintColor = UIColor.white
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            btnCollection[idx].alpha = 0.15
            imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        setDataOnLoad()
        
        noFound.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_pics")
        allLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_all")
        noBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no"), for: .normal)
        yesBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "yes"), for: .normal)
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FP_User_My_Pics, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func setDataOnLoad()
    {
        profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(passDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        nameLabel.text = "\(passDict["name"] as! String) (@\(passDict["nickname"] as! String))"
        placeLabel.text = passDict["address"] as? String
        albumLabel.text = "\(passDict["album_count"] as! NSNumber)"
        
        let arr1 = (passDict["pic_count"] as! String).split(separator: ",")
        let piccount = arr1[0].split(separator: "=")
        let like = arr1[1].split(separator: "=")
        
        picsLabel.text = "\(piccount[1])"
        likeLabel.text = "\(like[1])"
        
        if let _ = passDict["rating"] as? NSNumber
        {
            ratingLabel.text = String(format: "%.1f", Double.init("\(passDict["rating"] as! NSNumber)")!)
        }
    }
    
    @IBAction func deleteMultiplePics(_ sender: Any)
    {
        if (picId_array.count > 0)
        {
            blurView.isHidden = false
            centerView.isHidden = false
            
            delete_check = "multi"
        }
        else{
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_select_photos_to_delete"), controller: self)
        }
    }
    
    @IBAction func checkAll(_ sender: Any)
    {
        if (dataArray.count > 0)
        {
            picId_array = []
            if (checkBtn.image(for: .normal) == nil)
            {
                checkBtn.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
                checkBtn.backgroundColor = ColorCode.FBThemeColor
                for dict in dataArray
                {
                    picId_array.append("\(dict["id"] as! NSNumber)")
                }
            }
            else {
                checkBtn.setImage(nil, for: .normal)
                checkBtn.backgroundColor = UIColor.white
                picId_array = []
            }
            picsCollVw.reloadData()
        }
    }
    
    @IBAction func onSelectNo(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
    }
    
    @IBAction func onSelectYes(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
        
        if (picId_array.count > 0)
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = ["access_token":"\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "pic_id": picId_array] as [String : Any]
            
            ApiResponse.onResponsePost(url: Api.FP_Delete_Pic, parms: params as NSDictionary, controller: self) { (result) in
                
                OperationQueue.main.addOperation {
                    
                    if (self.delete_check == "multi")
                    {
                        customLoader.hideIndicator()
                        let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                        StaticFunctions.showAlert(title: "", message: msg, actions: ["OK"], controller: self, completion: { (str) in
                            self.viewDidLoad()
                        })
                    }
                    else {
                        customLoader.hideIndicator()
                        self.picId_array = []
                        self.dataArray.remove(at: self.selected_index!)
                        self.picsCollVw.reloadData()
                    }
                }
            }
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
             //   print("photo pic list result = \(result)")
                let data = result["imagesData"] as! [NSDictionary]
                if (check == "get")
                {
                    if (data.count > 0){
                        self.dataArray = result["imagesData"] as! [NSDictionary]
                        self.picsCollVw.reloadData()
                        self.picsCollVw.isHidden = false
                    } else {
                        self.noFound.isHidden = false
                        self.picsCollVw.isHidden = true
                    }
                    customLoader.hideIndicator()
                }
                else
                {
                    customLoader.hideIndicator()
                    self.picId_array = []
                    self.viewDidLoad()
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypictopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

/// MAKR : CollectionView Methods
extension FPMyPicsVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = picsCollVw.dequeueReusableCell(withReuseIdentifier: "mypicscell", for: indexPath) as! FPMyPicsCollCell
        
        let dict = dataArray[indexPath.item]
        let imgUrl = URL(string: "\(Api.FP_Original_Url)\(dict["original_pic"] as! String)")
        cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        
        if (picId_array.contains("\(dict["id"] as! NSNumber)") == true){
            cell.checkBtn.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
        }
        else {
            cell.checkBtn.setImage(nil, for: .normal)
        }
        cell.checkBtn.addTarget(self, action: #selector(selectImage(_:)), for: .touchUpInside)
        cell.deleteBtn.addTarget(self, action: #selector(deleteImageLocally(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func deleteImageLocally(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: picsCollVw)
        let idxpath = picsCollVw.indexPathForItem(at: point)!
        let dict = dataArray[idxpath.item]
        
        selected_index = idxpath.item
        picId_array = ["\(dict["id"] as! NSNumber)"]
        delete_check = "single"

        blurView.isHidden = false
        centerView.isHidden = false
    }
    
    @objc func selectImage(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: picsCollVw)
        let idxpath = picsCollVw.indexPathForItem(at: point)!
        
        let dict = dataArray[idxpath.item]
        let picId = "\(dict["id"] as! NSNumber)"
        
        if (sender.image(for: .normal) == nil)
        {
            sender.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
            if (picId_array.contains(picId) == false)
            {
                picId_array.append("\(dict["id"] as! NSNumber)")
            }
        }
        else {
            sender.setImage(nil, for: .normal)
            let arr = NSArray.init(array: picId_array)
            let index = arr.index(of: "\(dict["id"] as! NSNumber)")
            picId_array.remove(at: index)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            return CGSize(width: 85, height: 95)
        }
        else if (self.view.frame.width == 375)
        {
            return CGSize(width: 100, height: 110)
        }
        else {
            return CGSize(width: 110, height: 120)
        }
    }
}

class FPMyPicsCollCell: UICollectionViewCell {
    
    @IBOutlet weak var picImageVw : UIImageView!
    @IBOutlet weak var checkBtn : UIButton!
    @IBOutlet weak var deleteBtn : UIButton!
}
