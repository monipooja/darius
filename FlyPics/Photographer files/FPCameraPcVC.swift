//
//  FPCameraPcVC.swift
//  Darious
//
//  Created by Apple on 23/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPCameraPcVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var profileIcon : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var noFound : UILabel!
    @IBOutlet weak var topTitle : UILabel!
    
    @IBOutlet weak var topFirstView : CustomUIView!
    @IBOutlet weak var topSecondView : CustomUIView!
    @IBOutlet weak var topFirstBtn : UIButton!
    @IBOutlet weak var topSecondBtn : UIButton!
    @IBOutlet weak var topImgWd : NSLayoutConstraint!
    @IBOutlet weak var topImgHt : NSLayoutConstraint!
    
    @IBOutlet weak var uploadBtn : KGHighLightedButton!
    
    @IBOutlet weak var tableCustomView : CustomUIView!
    @IBOutlet weak var albumTableView : UITableView!
    
    var albumArray : [NSDictionary] = []
    
    var albumId : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        profileIcon.tintColor = UIColor.white
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            if (btnCollection[idx].tag == 2) {
                btnCollection[idx].alpha = 1
                imgCollection[idx].image = #imageLiteral(resourceName: "FP_comp_white")
            }
            else {
                btnCollection[idx].alpha = 0.15
                imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            }
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            topImgWd.constant = 80
            topImgHt.constant = 80
            topFirstView.layer.cornerRadius = 40
            topSecondView.layer.cornerRadius = 40
            topFirstBtn.layer.cornerRadius = 34
            topSecondBtn.layer.cornerRadius = 34
        }
        else if (self.view.frame.width == 375) && (self.view.frame.height == 667)
        {
            topImgWd.constant = 100
            topImgHt.constant = 100
            topFirstView.layer.cornerRadius = 50
            topSecondView.layer.cornerRadius = 50
            topFirstBtn.layer.cornerRadius = 44
            topSecondBtn.layer.cornerRadius = 44
        }else {
            topImgWd.constant = 110
            topImgHt.constant = 110
            topFirstView.layer.cornerRadius = 55
            topSecondView.layer.cornerRadius = 55
            topFirstBtn.layer.cornerRadius = 49
            topSecondBtn.layer.cornerRadius = 49
        }
        
        noFound.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_upload_pics")
        uploadBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_upload_pictures"), for: .normal)
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FP_My_Album, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        tableCustomView.isHidden = true
    }
    
    @IBAction func openAlbumTable(_ sender: Any)
    {
        tableCustomView.isHidden = false
    }

    @IBAction func selectAlbumAndContinue(_ sender: Any)
    {
        self.tableCustomView.isHidden = false
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                //  print("album result = \(result)")
                self.albumArray = result["data"] as! [NSDictionary]
                self.albumTableView.reloadData()
                customLoader.hideIndicator()
            }
        }
    }
    
    //MARK : Add bottom layer to TextFields
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:1.5)
        }
        textF.layer.addSublayer(border)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypictopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FPCameraPcVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 1) {
            return albumArray.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == 0) {
            let cell = albumTableView.dequeueReusableCell(withIdentifier: "fpuploadcell1") as! FPUploadCell1
            
            cell.albumLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_album")
            
            return cell
        }
        else if (indexPath.section == 1){
            let cell = albumTableView.dequeueReusableCell(withIdentifier: "fpuploadcell2") as! FPUploadCell2
            
            let dict = albumArray[indexPath.row]
            
            cell.albumNameLbl.text = dict["name"] as? String
            
            return cell
        }
        else {
            let cell = albumTableView.dequeueReusableCell(withIdentifier: "fpuploadcell3") as! FPUploadCell3
            cell.saveButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "continue"), for: .normal)
            cell.saveButton.addTarget(self, action: #selector(onContinue(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.section == 1) {
            let dict = albumArray[indexPath.row]
            albumId = "\(dict["id"] as! NSNumber)"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 0){
            return 40
        }
        else if (indexPath.section == 1){
            return 30
        }
        else {
            return 65
        }
    }
    
    @objc func onContinue(_ sender : UIButton)
    {
        let uploadvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPUploadPhotoVC") as! FPUploadPhotoVC
        uploadvc.passAlbumid = albumId
        navigationController?.pushViewController(uploadvc, animated: false)
    }
}

class FPUploadCell1: UITableViewCell {
    
    @IBOutlet weak var albumLbl : UILabel!
}

class FPUploadCell2: UITableViewCell {
    
    @IBOutlet weak var albumNameLbl : UILabel!
}

class FPUploadCell3: UITableViewCell {
    
    @IBOutlet weak var saveButton : KGHighLightedButton!
}
