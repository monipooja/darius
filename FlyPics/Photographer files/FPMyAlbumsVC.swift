//
//  FPMyPhotosVC.swift
//  Darious
//
//  Created by Apple on 25/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks

class FPMyAlbumsVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var profileIcon : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var noFound : UILabel!
    @IBOutlet weak var blurView : UIView!
    @IBOutlet weak var centerView : CustomUIView!
    @IBOutlet weak var centerMsgLbl : UILabel!
    
    @IBOutlet weak var noBtn : KGHighLightedButton!
    @IBOutlet weak var yesBtn : KGHighLightedButton!
    
    @IBOutlet weak var profileImgVw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var likeLabel : UILabel!
    @IBOutlet weak var picsLabel : UILabel!
    @IBOutlet weak var albumLabel : UILabel!
    @IBOutlet weak var ratingLabel : UILabel!
    
    @IBOutlet weak var albumTableView : UITableView!
    
    var photosArray : [NSDictionary] = []
    
    var checkStr : String = ""
    var editStr : String = ""
    var selectedAlbumId : String = ""
    var selectedCell : FPMyPhotosTabelCell!
    var selectedDict : NSDictionary = [:]
    var selectIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        profileIcon.tintColor = UIColor.white
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            if (btnCollection[idx].tag == 0) {
                btnCollection[idx].alpha = 1
                imgCollection[idx].tintColor = UIColor.white
            }
            else {
                btnCollection[idx].alpha = 0.15
                imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            }
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        setDataOnLoad()
        
        noFound.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_albums")
        noBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "no"), for: .normal)
        yesBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "yes"), for: .normal)
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FP_My_Album, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
        
        if (editStr != "") {
            self.photosArray = []
            DispatchQueue.main.async {
                customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
                self.callService(urlstr: Api.FP_My_Album, parameters: params, check: "get")
            }
        }
    }
    
    func setDataOnLoad()
    {
        profileImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(Photographer_Info["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        nameLabel.text = "\(Photographer_Info["name"] as! String) (@\(Photographer_Info["nickname"] as! String))"
        placeLabel.text = Photographer_Info["address"] as? String
        albumLabel.text = "\(Photographer_Info["album_count"] as! NSNumber)"
        
        let arr1 = (Photographer_Info["pic_count"] as! String).split(separator: ",")
        let piccount = arr1[0].split(separator: "=")
        let like = arr1[1].split(separator: "=")
        
        picsLabel.text = "\(piccount[1])"
        likeLabel.text = "\(like[1])"
        
        if let _ = Photographer_Info["rating"] as? NSNumber
        {
            ratingLabel.text = String(format: "%.1f", Double.init("\(Photographer_Info["rating"] as! NSNumber)")!)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func hideView(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
    }
    
    @IBAction func onSelectNo(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
    }
    
    @IBAction func onSelectYes(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
        
        if (checkStr == "yesmark") {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&album_id=\(selectedAlbumId)&ready=yes"
            self.callService(urlstr: Api.FP_MarkReady_Album, parameters: params, check: "yesmark")
        }
        else if (checkStr == "nomark") {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&album_id=\(selectedAlbumId)&ready=no"
            self.callService(urlstr: Api.FP_MarkReady_Album, parameters: params, check: "nomark")
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&album_id=\(selectedAlbumId)"
            self.callService(urlstr: Api.FP_Delete_Album, parameters: params, check: "delete")
        }
    }
    
    @IBAction func shareFlyPicProfile(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flypic&screen_name=album") else { return }
        
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flypic")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flypic")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("my photos result = \(result)")
                if (check == "get") {
                    self.photosArray = result["data"] as! [NSDictionary]
                    if (self.photosArray.count > 0) {
                        self.albumTableView.isHidden = false
                        self.albumTableView.reloadData()
                    } else {
                        self.albumTableView.isHidden = true
                        self.noFound.isHidden = false
                    }
                }
                else if (check == "yesmark") {
                    let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msg, actions: ["Ok"], controller: self, completion: { (str) in
                        
                        let editdict = NSMutableDictionary.init(dictionary: self.selectedDict)
                        editdict["ready"] = "yes"
                        self.photosArray[self.selectIndex] = editdict
                        self.selectedCell.markReadyButton.backgroundColor = ColorCode.FPicReadyColor
                        self.selectedCell.markReadyButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_ready"), for: .normal)
                    })
                }
                else if (check == "nomark") {
                    let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msg, actions: ["Ok"], controller: self, completion: { (str) in
                        
                        let editdict = NSMutableDictionary.init(dictionary: self.selectedDict)
                        editdict["ready"] = "no"
                        self.photosArray[self.selectIndex] = editdict
                        self.selectedCell.markReadyButton.backgroundColor = ColorCode.FPicThemeColor
                        self.selectedCell.markReadyButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_mark_ready"), for: .normal)
                    })
                }
                else {
                    let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msg, actions: ["Ok"], controller: self, completion: { (str) in
                        self.viewDidLoad()
                    })
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    //MARK : Add bottom layer to TextFields
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:1.5)
        }
        textF.layer.addSublayer(border)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypictopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FPMyAlbumsVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return photosArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = albumTableView.dequeueReusableCell(withIdentifier: "myphotoscell") as! FPMyPhotosTabelCell
        
        let dict = photosArray[indexPath.section]
        
        cell.albumNameLbl.text = dict["name"] as? String
        cell.placeLbl.text = dict["location"] as? String
        cell.dateLbl.text = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd/MM/yyyy")
        
        if (dict["ready"] as! String == "no") {
            cell.markReadyButton.backgroundColor = ColorCode.FPicThemeColor
            cell.markReadyButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_mark_ready"), for: .normal)
        }
        else {
            cell.markReadyButton.backgroundColor = ColorCode.FPicReadyColor
            cell.markReadyButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_ready"), for: .normal)
        }
        cell.seeButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_see"), for: .normal)
        cell.markReadyButton.addTarget(self, action: #selector(markReadyAlbum(_:)), for: .touchUpInside)
        cell.deleteButton.addTarget(self, action: #selector(deleteAlbum(_:)), for: .touchUpInside)
        cell.editButton.addTarget(self, action: #selector(editAlbum(_:)), for: .touchUpInside)
        cell.seeButton.addTarget(self, action: #selector(seePhotos(_:)), for: .touchUpInside)
        
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            cell.editWd.constant = 30
            cell.editHt.constant = 30
            cell.editView.layer.cornerRadius = 15
        }
        else if (self.view.frame.width == 375) && (self.view.frame.height == 667)
        {
            cell.editWd.constant = 40
            cell.editHt.constant = 40
            cell.editView.layer.cornerRadius = 20
        }else {
            cell.editWd.constant = 50
            cell.editHt.constant = 50
            cell.editView.layer.cornerRadius = 25
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: albumTableView.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func markReadyAlbum(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        let dict = photosArray[idxpath.section]
        selectedDict = dict
        selectIndex = idxpath.section
        selectedAlbumId = "\(dict["id"] as! NSNumber)"
        selectedCell = albumTableView.cellForRow(at: idxpath) as? FPMyPhotosTabelCell
        
        yesBtn.backgroundColor = ColorCode.FPicReadyColor
        
        if (selectedCell.markReadyButton.backgroundColor == ColorCode.FPicThemeColor)
        {
            checkStr = "yesmark"
            centerMsgLbl.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_mark_ready"))?"
        }
        else {
            checkStr = "nomark"
            centerMsgLbl.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_mark_not_ready"))?"
        }
        blurView.isHidden = false
        centerView.isHidden = false
    }
    
    @objc func deleteAlbum(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        let dict = photosArray[idxpath.section]
        selectedAlbumId = "\(dict["id"] as! NSNumber)"
        
        checkStr = "delete"
        
        yesBtn.backgroundColor = UIColor.init(hexString: "C40606")
        
        centerMsgLbl.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_sure_to_delete_album"))?"
        blurView.isHidden = false
        centerView.isHidden = false
    }
    
    @objc func editAlbum(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        let dict = photosArray[idxpath.section]
        
        editStr = "edit"
        let editvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPEditAlbumVC") as! FPEditAlbumVC
        editvc.passDict = dict
        navigationController?.pushViewController(editvc, animated: false)
    }
    
    @objc func seePhotos(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        let dict = photosArray[idxpath.section]
        
        let nextvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPEditPhotoVC") as! FPEditPhotoVC
        nextvc.passDict = dict
        self.navigationController?.pushViewController(nextvc, animated: false)
    }
}

class FPMyPhotosTabelCell: UITableViewCell {
    
    @IBOutlet weak var albumNameLbl : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var placeLbl : UILabel!
    
    @IBOutlet weak var seeButton : UIButton!
    @IBOutlet weak var markReadyButton : UIButton!
    @IBOutlet weak var deleteButton : UIButton!
    @IBOutlet weak var editButton : UIButton!
    @IBOutlet weak var editView : CustomUIView!
    
    @IBOutlet weak var editHt : NSLayoutConstraint!
    @IBOutlet weak var editWd : NSLayoutConstraint!
}
