//
//  FPPhotographerSellsVC.swift
//  Darious
//
//  Created by Apple on 25/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPPhotographerSellsVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var profileIcon : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var nodataLabel : UILabel!
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var myorderLbl : UILabel!
    @IBOutlet weak var periodLbl : UILabel!
    @IBOutlet weak var fromLbl : UILabel!
    @IBOutlet weak var toLbl : UILabel!
    @IBOutlet weak var fromDateLbl : UILabel!
    @IBOutlet weak var toDateLbl : UILabel!
    
    @IBOutlet weak var allBtn : KGHighLightedButton!
    @IBOutlet weak var applyBtn : KGHighLightedButton!
    
    @IBOutlet weak var fromWidth : NSLayoutConstraint!
    @IBOutlet weak var toWidth : NSLayoutConstraint!
    
    @IBOutlet weak var orderTableVw : UITableView!
    
    var datePicker : UIDatePicker!
    var doneBtn : UIButton!
    var chooseStr : String = ""
    var fromDateStr : String = ""
    var toDateStr : String = ""
    var delegate : String = ""
    
    var dataArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        profileIcon.tintColor = UIColor.white
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            btnCollection[idx].alpha = 0.15
            imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        setDatePicker()
        
        if (self.view.frame.width == 320){
            fromWidth.constant = 130
            toWidth.constant = 130
        }
        
        nodataLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_sells")
        myorderLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_orders")
        periodLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_select_period")
        fromLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_from")
        toLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_to")
        allBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_all"), for: .normal)
        applyBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_apply"), for: .normal)
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&start_date=&end_date="
            self.callService(urlstr: Api.FP_Photo_Orders, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    // Set Date Picker
    func setDatePicker()
    {
        doneBtn = UIButton.init(type: .system)
        doneBtn.frame = CGRect(x: 0, y: view.frame.height-220, width: view.frame.width, height: 40)
        doneBtn.backgroundColor = ColorCode.FPicThemeColor
        doneBtn.setTitleColor(UIColor.white, for: .normal)
        doneBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "common_done_ios"), for: .normal)
        doneBtn.titleLabel?.textAlignment = .right
        doneBtn.addTarget(self, action: #selector(chooseDate(_:)), for: .touchUpInside)
        
        datePicker = UIDatePicker.init()
        datePicker.frame = CGRect(x: 0, y: view.frame.height-180, width: view.frame.width, height: 180)
        datePicker.backgroundColor = UIColor.white
        datePicker.datePickerMode = .date
//        let calendar = Calendar(identifier: .gregorian)
//        var comps = DateComponents()
//        comps.year = 30
//        let maxDate = calendar.date(byAdding: comps, to: Date())
        datePicker.maximumDate = Date()
      //  datePicker.minimumDate = Date()
        
        view.addSubview(doneBtn)
        view.addSubview(datePicker)
        doneBtn.isHidden = true
        datePicker.isHidden = true
    }
    
    @objc func chooseDate(_ sender : UIButton)
    {
        doneBtn.isHidden = true
        datePicker.isHidden = true
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        self.view.endEditing(true)
        
        if (chooseStr == "from") {
            fromDateLbl.text = formatter.string(from: (datePicker?.date)!).toDateString(inputFormat: "dd/MM/yyyy HH:mm:ss", outputFormat: "dd-MM-yyyy")!
            fromDateStr = formatter.string(from: (datePicker?.date)!).toDateString(inputFormat: "dd/MM/yyyy HH:mm:ss", outputFormat: "yyyy-MM-dd")!
        }
        else {
            toDateLbl.text = formatter.string(from: (datePicker?.date)!).toDateString(inputFormat: "dd/MM/yyyy HH:mm:ss", outputFormat: "dd-MM-yyyy")!
            toDateStr = formatter.string(from: (datePicker?.date)!).toDateString(inputFormat: "dd/MM/yyyy HH:mm:ss", outputFormat: "yyyy-MM-dd")!
        }
    }
    
    @IBAction func allPeriod(_ sender: Any)
    {
        fromDateLbl.text = "dd/mm/yyyy"
        toDateLbl.text = "dd/mm/yyyy"
        fromDateStr = ""
        toDateStr = ""
        customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&start_date=\(fromDateStr)&end_date=\(toDateStr)"
        self.callService(urlstr: Api.FP_Photo_Orders, parameters: params, check: "get")
    }
    
    @IBAction func selectFromDate(_ sender: Any)
    {
        chooseStr = "from"
        doneBtn.isHidden = false
        datePicker.isHidden = false
    }
    
    @IBAction func selectToDate(_ sender: Any)
    {
        chooseStr = "to"
        doneBtn.isHidden = false
        datePicker.isHidden = false
    }
    
    @IBAction func onApplyPeriod(_ sender: Any)
    {
        var result : ComparisonResult?
        if (fromDateStr != "") && (toDateStr != "")
        {
            let leftDate = fromDateStr.toDate(format: "yyyy-MM-dd")
            let rightDate = toDateStr.toDate(format: "yyyy-MM-dd")
            result = leftDate!.compare(rightDate!)
        }
        
        if (fromDateLbl.text == "dd/mm/yyyy") {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_choose_from_date"), controller: self)
        }
        else if (toDateLbl.text == "dd/mm/yyyy") {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_choose_to_date"), controller: self)
        }
        else if (result != nil) && (result == .orderedDescending)
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_from_greater_than_to"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&start_date=\(fromDateStr)&end_date=\(toDateStr)"
            self.callService(urlstr: Api.FP_Photo_Orders, parameters: params, check: "get")
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (delegate == "")
        {
            navigationController?.popViewController(animated: false)
        } else {
            let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
            navigationController?.pushViewController(homevc, animated: false)
        }
    }

    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
              //  print("photo orders result = \(result)")
                let data = result["data"] as! [NSDictionary]
                if (data.count > 0) {
                    self.dataArray = result["data"] as! [NSDictionary]
                    self.orderTableVw.reloadData()
                    self.orderTableVw.isHidden = false
                    self.nodataLabel.isHidden = true
                } else {
                    self.nodataLabel.isHidden = false
                    self.orderTableVw.isHidden = true
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypictopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FPPhotographerSellsVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = orderTableVw.dequeueReusableCell(withIdentifier: "photoellcell") as! FPPhotoSellCell
        
        let dict = dataArray[indexPath.section]
        
        cell.nameLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_order_no")) - \(dict["id"] as! NSNumber)"
        if (dict["amount"] as! String == "free"){
            cell.priceLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_price")) : \(dict["amount"] as! String)"
        }
        else {
            cell.priceLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_price")) : \(dict["amount"] as! String) \(Current_Currency)"
        }
        cell.dateLbl.text = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd", outputFormat: "dd/MM/yyyy")
        
        if (indexPath.section % 2 == 0) {
            cell.mainView.backgroundColor = ColorCode.FPOrderCell
        }
        else {
            cell.mainView.backgroundColor = UIColor.white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = dataArray[indexPath.section]
        
        let picsvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPOrderPicsVC") as! FPOrderPicsVC
        picsvc.order_id = dict["id"] as! NSNumber
        picsvc.screenName = "photo"
        navigationController?.pushViewController(picsvc, animated: false)
    }
}

class FPPhotoSellCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    
    @IBOutlet weak var mainView : UIView!
    
}
