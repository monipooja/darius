//
//  FPCreateAlbumVC.swift
//  Darious
//
//  Created by Apple on 22/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPCreateAlbumVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    @IBOutlet weak var profileIcon : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var albmNameLbl : UILabel!
    @IBOutlet weak var locationLbl : UILabel!
    @IBOutlet weak var codeLbl : UILabel!
    @IBOutlet weak var albumLbl : UILabel!
    
    @IBOutlet weak var saveBtn : KGHighLightedButton!
    @IBOutlet weak var createBtn : KGHighLightedButton!
    
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var nameTF : UITextField!
    @IBOutlet weak var locationTF : UITextField!
    @IBOutlet var codeTF : [UITextField]!
    @IBOutlet weak var albumView : CustomUIView!
    
    var albumId : String = ""
    var codeStr : String = ""
    var viewOriginY : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        profileIcon.tintColor = UIColor.white
        
        viewOriginY = view.frame.origin.y
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            if (btnCollection[idx].tag == 1) {
                btnCollection[idx].alpha = 1
                imgCollection[idx].image = #imageLiteral(resourceName: "FP_plus_white")
            }
            else {
                btnCollection[idx].alpha = 0.15
                imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            }
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        for tf in codeTF
        {
            addBottomLayerWithColor(bgcolor: UIColor.gray, textF: tf)
        }
        setBottomView()
        
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_create_album")
        albmNameLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_album_name")
        locationLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_location")
        codeLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_code")
        albumLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_album")
        saveBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "save"), for: .normal)
        createBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_create_album"), for: .normal)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func openCreateAlbumView(_ sender: Any)
    {
        albumView.isHidden = false
    }

    @IBAction func saveAlbumData(_ sender: Any)
    {
        codeStr = "\(codeTF[0].text!)\(codeTF[1].text!)\(codeTF[2].text!)\(codeTF[3].text!)"
        if (nameTF.text == "") {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_enter_album"), controller: self)
        }
        else if (locationTF.text == "") {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_enter_location"), controller: self)
        }
        else if (codeStr.count < 4) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_enter_correct_code"), controller: self)
        }
        else {
            view.endEditing(true)
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&name=\(nameTF.text!)&location=\(locationTF.text!)&code=\(codeStr)"
            self.callService(urlstr: Api.FP_Create_Album, parameters: params, check: "create")
        }
    }

    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
              //  print("album result = \(result)")
                // let data = result["data"] as! [NSDictionary]
                if (check == "create")
                {
                    let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msg, actions: ["OK"], controller: self, completion: { (str) in
                        
                        self.nameTF.text = ""
                        self.locationTF.text = ""
                        for tf in self.codeTF
                        {
                            tf.text = ""
                        }
                    })
                }
                else {
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    //MARK : Add bottom layer to TextFields
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:1.5)
        }
        textF.layer.addSublayer(border)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypictopButtonAction(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FPCreateAlbumVC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.view.frame.origin.y = viewOriginY - 100
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.frame.origin.y = 0
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((codeTF[0].text!.count) < 2) {
            
            if (codeTF[0].text != "") {
                if (codeTF[1].text != "") {
                    if (codeTF[2].text != "") {
                        if (string == "") {
                            if (codeTF[3].text == "") {
                                codeTF[2].text = ""
                                codeTF[1].becomeFirstResponder()
                            }else {
                                codeTF[3].text = ""
                                codeTF[2].becomeFirstResponder()
                            }
                        }
                        else {
                            codeTF[3].text = string
                            codeTF[3].becomeFirstResponder()
                        }
                    } else {
                        if (string == "") {
                            if (codeTF[2].text == "") {
                                codeTF[1].text = ""
                                codeTF[0].becomeFirstResponder()
                            }else {
                                codeTF[2].text = ""
                                codeTF[1].becomeFirstResponder()
                            }
                        }
                        else {
                            codeTF[2].text = string
                            codeTF[2].becomeFirstResponder()
                        }
                    }
                } else {
                    if (string == "") {
                        if (codeTF[1].text == "") {
                            codeTF[0].text = ""
                            codeTF[0].becomeFirstResponder()
                        }else {
                            codeTF[1].text = ""
                            codeTF[0].becomeFirstResponder()
                        }
                    }
                    else {
                        codeTF[1].text = string
                        codeTF[1].becomeFirstResponder()
                    }
                }
            }
            else {
                return true
            }
        }
        else {
            // print("texxxxx = \(string)")
        }
        return false
    }
}

