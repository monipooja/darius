//
//  FlyPicSplashVC.swift
//  Darious
//
//  Created by Apple on 05/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

var Cart_Count : Int = 0

class FlyPicSplashVC: UIViewController {

    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = true
//        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        
        if let _ = userDefault.value(forKey: "buy_photos") as? [NSDictionary]
        {
            Cart_Count = (userDefault.value(forKey: "buy_photos") as! [NSDictionary]).count
        }
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(gotoNext), userInfo: nil, repeats: false)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func gotoNext()
    {
        timer.invalidate()
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        self.navigationController?.pushViewController(homevc, animated: false)
    }
}

public func flypictopButtonAction(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 0:
        // My Albums
        let albumvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPMyAlbumsVC") as! FPMyAlbumsVC
        controller.navigationController?.pushViewController(albumvc, animated: false)
        break
    case 1:
        // Create Album
        let createvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPCreateAlbumVC") as! FPCreateAlbumVC
        controller.navigationController?.pushViewController(createvc, animated: false)
        break
    case 2:
        let uploadvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPCameraPcVC") as! FPCameraPcVC
        controller.navigationController?.pushViewController(uploadvc, animated: false)
        break
    default:
        break
    }
}

public func flypicUserTopActions(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 0:
        // Pics & Album
        let picalbumvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPMyPicAlbumVC") as! FPMyPicAlbumVC
        controller.navigationController?.pushViewController(picalbumvc, animated: false)
        break
    case 1:
        // My Subscription
        let subsvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPMySubscriptionVC") as! FPMySubscriptionVC
        controller.navigationController?.pushViewController(subsvc, animated: false)
        break
    case 2:
        // Cart
        let cartvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPAddCartVC") as! FPAddCartVC
        controller.navigationController?.pushViewController(cartvc, animated: false)
        break
    default:
        break
    }
}


public func flypicUserBtmAction(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 1:
        // Search
        let searchvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPicSearchVC") as! FPicSearchVC
        controller.navigationController?.pushViewController(searchvc, animated: false)
        break
    case 2:
        // My Sells
        let sellsvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPUserSellVC") as! FPUserSellVC
        controller.navigationController?.pushViewController(sellsvc, animated: false)
        break
    case 3:
        // Darious
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
        break
    case 4:
        // Wallet
        let walletvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPicWalletVC") as! FPicWalletVC
        walletvc.check = "user"
        controller.navigationController?.pushViewController(walletvc, animated: false)
        break
    case 5:
        // Notification
        let notifyvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPicNotificationVC") as! FPicNotificationVC
        notifyvc.check = "user"
        controller.navigationController?.pushViewController(notifyvc, animated: false)
        break
    default:
        break
    }
}

public func flypicBottomViewAction(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 1:
        // Cart
        let nextvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPPhotographerSellsVC") as! FPPhotographerSellsVC
        controller.navigationController?.pushViewController(nextvc, animated: false)
        break
    case 2:
        // My Profile
        let profilevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPMyProfileVC") as! FPMyProfileVC
        controller.navigationController?.pushViewController(profilevc, animated: false)
        break
    case 3:
        // Darious
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
        break
    case 4:
        // Wallet
        let walletvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPicWalletVC") as! FPicWalletVC
        walletvc.check = "photographer"
        controller.navigationController?.pushViewController(walletvc, animated: false)
        break
    case 5:
        // Notification
        let notifyvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPicNotificationVC") as! FPicNotificationVC
        notifyvc.check = "photographer"
        controller.navigationController?.pushViewController(notifyvc, animated: false)
        break
    default:
        break
    }
}
