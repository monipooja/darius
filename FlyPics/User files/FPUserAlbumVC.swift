//
//  FPUserAlbumVC.swift
//  Darious
//
//  Created by Apple on 02/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPUserAlbumVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var nodataLabel : UILabel!
    @IBOutlet weak var profileImgVw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var likeLabel : UILabel!
    @IBOutlet weak var picsLabel : UILabel!
    @IBOutlet weak var albumLabel : UILabel!
    @IBOutlet weak var ratingLabel : UILabel!

    @IBOutlet weak var albumTableView : UITableView!
    
    var dataArray : [NSDictionary] = []
    
    var passId : String = ""
    var selectedCell : FPUserAlbumTabelCell!
    var selectDict : NSDictionary = [:]
    var selectIndex : Int = 0
    var fromDelegate : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            btnCollection[idx].alpha = 0.15
            imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        nodataLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_album")
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.passId)"
            self.callService(urlstr: Api.FP_My_Album, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
        
        if (Cart_Count > 0)
        {
            cartCountLbl.isHidden = false
            cartCountLbl.text = "\(Cart_Count)"
        }
        else {
            cartCountLbl.isHidden = true
        }
    }
    
    func setDataOnLoad(passDict : NSDictionary)
    {
        if ((passDict["profile_pic"] as! String).contains("graph.facebook.com") == true) || ((passDict["profile_pic"] as! String).contains("googleusercontent") == true)
        {
            profileImgVw.sd_setImage(with: URL(string: "\(passDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        else {
            profileImgVw.sd_setImage(with: URL(string: "\(Api.Profile_BaseURL)\(passDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        nameLabel.text = "\(passDict["name"] as! String)(@\(passDict["nickname"] as! String))"
        placeLabel.text = passDict["address"] as? String
        albumLabel.text = "\(passDict["album_count"] as! NSNumber)"
        
        let arr1 = (passDict["pic_count"] as! String).split(separator: ",")
        let piccount = arr1[0].split(separator: "=")
        let like = arr1[1].split(separator: "=")
        
        picsLabel.text = "\(piccount[1])"
        likeLabel.text = "\(like[1])"
        
        if let _ = passDict["rating"] as? NSNumber
        {
            ratingLabel.text = String(format: "%.1f", Double.init("\(passDict["rating"] as! NSNumber)")!)
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPicSearchVC") as! FPicSearchVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("usr album result = \(result)")
                if (check == "get")
                {
                    self.setDataOnLoad(passDict: result["user_details"] as! NSDictionary)
                    let data = result["data"] as! [NSDictionary]
                    if (data.count > 0)
                    {
                        self.dataArray = data
                        self.albumTableView.reloadData()
                    }
                    else {
                        self.albumTableView.isHidden = true
                        self.nodataLabel.isHidden = false
                    }
                }
                else {
                    let msg = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msg, actions: ["Ok"], controller: self, completion: { (str) in
                        
                        let editdict = NSMutableDictionary.init(dictionary: self.selectDict)
                        
                        if (editdict["subscribe"] as! NSNumber == 1)
                        {
                            editdict["subscribe"] = 0
                            self.selectedCell.subscribeBtn.backgroundColor = ColorCode.FPicThemeColor
                            self.selectedCell.subscribeBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_subscribe"), for: .normal)
                        }
                        else {
                            editdict["subscribe"] = 1
                            self.selectedCell.subscribeBtn.backgroundColor = UIColor.init(hexString: "04AE42")
                            self.selectedCell.subscribeBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_subscribed"), for: .normal)
                        }
                        self.dataArray[self.selectIndex] = editdict
                    })
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypicUserTopActions(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicUserBtmAction(tagValue: sender.tag, controller: self)
    }

}

extension FPUserAlbumVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = albumTableView.dequeueReusableCell(withIdentifier: "useralbumcell") as! FPUserAlbumTabelCell
        
        let dict = dataArray[indexPath.section]
        
        cell.nameLabel.text = dict["name"] as? String
        cell.placeLabel.text = dict["location"] as? String
        cell.dateLbl.text = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd/MM/yyyy")
        
        // Subscribed
        if (dict["subscribe"] as! NSNumber == 0) {
            cell.subscribeBtn.backgroundColor = ColorCode.FPicThemeColor
            cell.subscribeBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_subscribe"), for: .normal)
        }
        else {
            cell.subscribeBtn.backgroundColor = UIColor.init(hexString: "04AE42")
            cell.subscribeBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_subscribed"), for: .normal)
        }
        // Mark ready
        if (dict["ready"] as! String == "no") {
            cell.cameraButton.alpha = 0.27
            cell.lockImgVw.isHidden = true
        }
        else {
            cell.cameraButton.alpha = 1
            cell.lockImgVw.isHidden = false
        }
        // Lock/Unlock album
        let arr = userDefault.value(forKey: "unlock_albums") as! [String]
        if (arr.count > 0) && (arr.contains("\(dict["id"] as! NSNumber)") == true)
        {
            cell.lockImgVw.image = #imageLiteral(resourceName: "FP_unlock")
        } else {
            cell.lockImgVw.image = #imageLiteral(resourceName: "FP_lock")
        }
    
        cell.cameraButton.addTarget(self, action: #selector(goToPics(_:)), for: .touchUpInside)
        cell.subscribeBtn.addTarget(self, action: #selector(subscribeTheAlbum(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: albumTableView.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func goToPics(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        let dict = dataArray[idxpath.section]
        
        if (dict["ready"] as! String == "no") {
            ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_album_not_ready"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_notify_album_ready"), controller: self)
        }
        else {
            let arr = userDefault.value(forKey: "unlock_albums") as! [String]
            if (arr.count > 0) && (arr.contains("\(dict["id"] as! NSNumber)") == true)
            {
                let photosvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPUserPhotosVC") as! FPUserPhotosVC
                photosvc.album_Id = "\(dict["id"] as! NSNumber)"
                photosvc.album_name = "\(dict["name"] as! String)"
                self.navigationController?.pushViewController(photosvc, animated: false)
            } else {
                let codevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPicInsertCodeVC") as! FPicInsertCodeVC
                codevc.album_Id = "\(dict["id"] as! NSNumber)"
                codevc.album_name = "\(dict["name"] as! String)"
                navigationController?.pushViewController(codevc, animated: false)
            }
        }
    }
    
    @objc func subscribeTheAlbum(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        let dict = dataArray[idxpath.section]
        selectDict = dict
        selectIndex = idxpath.section
        selectedCell = albumTableView.cellForRow(at: idxpath) as? FPUserAlbumTabelCell
        
        customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&album_id=\(dict["id"] as! NSNumber)"
        self.callService(urlstr: Api.FP_Subscribe_Album, parameters: params, check: "subs")
    }
}

class FPUserAlbumTabelCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var lockImgVw : UIImageView!
    
    @IBOutlet weak var rightView : CustomUIView!
    @IBOutlet weak var cameraButton : UIButton!
    
    @IBOutlet weak var subscribeBtn : KGHighLightedButton!
}
