//
//  FPicSearchVC.swift
//  Darious
//
//  Created by Apple on 19/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import DropDown

class FPicSearchVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var nodataLabel : UILabel!
    @IBOutlet weak var searchLabel : UILabel!
    
    @IBOutlet weak var secondView : UIView!
    @IBOutlet weak var countryLabel : UILabel!
    @IBOutlet weak var areaLabel : UILabel!
    @IBOutlet weak var searchTF : UITextField!
    @IBOutlet weak var firstDD : UIImageView!
    @IBOutlet weak var secondDD : UIImageView!
    @IBOutlet weak var searchButton : KGHighLightedButton!
    @IBOutlet weak var filterViewHt : NSLayoutConstraint!
    
    @IBOutlet weak var searchTableView : UITableView!
    
    let dropDown = DropDown()
    let searchBar : UISearchBar = UISearchBar.init()
    
    var dataArray : [NSDictionary] = []
    var nameArray : [String] = []
    var idArray : [String] = []
    var isoArray : [String] = []
    var dataFiltered: [String] = []
    var filterIdArray : [String] = []
    var filterIsoArray : [String] = []
    
    var userNameArr : [String] = []
    var userNameFlitered : [String] = []
    var userDataFlitered : [NSDictionary] = []
    
    var country_id : String = ""
    var area_id : String = ""
    var check : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        firstDD.tintColor = ColorCode.FPicThemeColor
        secondDD.tintColor = ColorCode.FPicThemeColor
        
        filterViewHt.constant = 0
        secondView.isHidden = true
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            btnCollection[idx].alpha = 0.15
            imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        searchBarIntegration()
        setLanguage()
        
        nodataLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "search")
        searchTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "search")
        
        if let _ = userDefault.value(forKey: "unlock_albums") as? [String]
        {}
        else {
            userDefault.set([], forKey: "unlock_albums")
            userDefault.synchronize()
        }
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FP_Search_List, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.searchBar.isHidden = true
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
        
        if (Cart_Count > 0)
        {
            cartCountLbl.isHidden = false
            cartCountLbl.text = "\(Cart_Count)"
        }
        else {
            cartCountLbl.isHidden = true
        }
    }
    
    func setLanguage()
    {
        countryLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_country")
        areaLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_area")
    }
    
    @IBAction func chooseCountry(_ sender: Any)
    {
        areaLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_area")
        nameArray = []
        idArray = []
        country_id = ""
        check = "country"
        customLoader.showActivityIndicator(showColor: ColorCode.FBThemeColor.toHexString(), controller: self.view)
        let params2 = "api_key=\(api_key)"
        self.callService(urlstr: Api.STDCode_URL, parameters: params2, check: "country")
    }
    
    @IBAction func chooseArea(_ sender: Any)
    {
        areaLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_area")
        nameArray = []
        idArray = []
        check = "area"
        if (country_id == "")
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_county"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FBThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&country_id=\(country_id)"
            self.callService(urlstr: Api.FB_AreaList_URL, parameters: params, check: "area")
        }
    }
    
    @IBAction func filterOn(_ sender: Any)
    {
        if(secondView.isHidden == true)
        {
            filterViewHt.constant = 170
            secondView.isHidden = false
        }
        else {
            countryLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_country")
            country_id = ""
            areaLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_area")
            area_id = ""
            filterViewHt.constant = 0
            secondView.isHidden = true
        }
    }
    
    @IBAction func searchOnfilter(_ sender: Any)
    {
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("search result = \(result)")
                if (check == "get") {
                    let data = result["data"] as! [NSDictionary]
                    if (data.count > 0)
                    {
                        self.dataArray = data
                        self.userDataFlitered = self.dataArray
                        self.searchTableView.reloadData()
                
                        for dict in self.dataArray
                        {
                            self.userNameArr.append(dict["name"] as! String)
                        }
                    }
                    else {
                        self.searchTableView.isHidden = true
                        self.nodataLabel.isHidden = false
                    }
                    customLoader.hideIndicator()
                }
                else if (check == "country"){
                    let data = result["data"] as! [NSDictionary]
                    for dict in data
                    {
                        self.nameArray.append(dict["nicename"] as! String)
                        self.isoArray.append(dict["iso"] as! String)
                        self.idArray.append("\(dict["id"] as! NSNumber)")
                    }
                    customLoader.hideIndicator()
                    self.showDropDown(passarr: self.nameArray)
                }
                else {
                    let dataArr = result["data"] as! [NSDictionary]
                    for dict in dataArr
                    {
                        self.nameArray.append(dict["name"] as! String)
                        self.idArray.append("\(dict["id"] as! NSNumber)")
                    }
                    customLoader.hideIndicator()
                    self.showDropDown(passarr: self.nameArray)
                }
            }
        }
    }
    
    // MARK : Search bar create
    func searchBarIntegration()
    {
        searchBar.frame = CGRect(x: 20, y: 40, width: self.view.frame.width-40, height:50)
        searchBar.delegate = self
        self.view.addSubview(searchBar)
        searchBar.isHidden = true
    }
    
    func showDropDown(passarr : [String])
    {
        searchBar.isHidden = false
        searchBar.text = ""
        
        dropDown.anchorView = searchBar
        dropDown.dataSource = passarr
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            if (self.check == "country"){
                let codestr = (self.isoArray[index]).lowercased()
                cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
            }
            else {
                cell.flagImage.image = nil
            }
            cell.codeLabel.text = ""
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            if (self.check == "country"){
                self.countryLabel.text = passarr[index]
                self.country_id = self.idArray[index]
            }
            else {
                self.areaLabel.text = passarr[index]
                self.area_id = self.idArray[index]
            }
            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
        }
        dropDown.width = searchBar.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
        
        searchBar.becomeFirstResponder()
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypicUserTopActions(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicUserBtmAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FPicSearchVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return userDataFlitered.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = searchTableView.dequeueReusableCell(withIdentifier: "fpsearchcell") as! FPSearchTabelCell
        
        let dict = userDataFlitered[indexPath.section]
        
        if ((dict["profile_pic"] as! String).contains("graph.facebook.com") == true) || ((dict["profile_pic"] as! String).contains("googleusercontent") == true)
        {
            cell.profileImage.sd_setImage(with: URL(string: "\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        else {
            cell.profileImage.sd_setImage(with: URL(string: "\(Api.Profile_BaseURL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        
        cell.nameLabel.text = "\(dict["name"] as! String)(@\(dict["nickname"] as! String))"
        cell.placeLabel.text = dict["address"] as? String
        cell.albumLabel.text = "\(dict["album_count"] as! NSNumber)"
        
        let arr1 = (dict["pic_count"] as! String).split(separator: ",")
        let piccount = arr1[0].split(separator: "=")
        let like = arr1[1].split(separator: "=")
        
        cell.picsLabel.text = "\(piccount[1])"
        cell.likeLabel.text = "\(like[1])"
        
        if let _ = dict["rating"] as? NSNumber
        {
            cell.ratingLabel.text = String(format: "%.1f", Double.init("\(dict["rating"] as! NSNumber)")!)
        }
        cell.cameraButton.addTarget(self, action: #selector(goToAlbums(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: searchTableView.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func goToAlbums(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: searchTableView)
        let idxPath = searchTableView.indexPathForRow(at: point)!
        let dict = userDataFlitered[idxPath.section]
        
        let albumvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPUserAlbumVC") as! FPUserAlbumVC
        albumvc.passId = "\(dict["id"] as! NSNumber)"
        navigationController?.pushViewController(albumvc, animated: false)
    }
}

class FPSearchTabelCell: UITableViewCell {
    
    @IBOutlet weak var profileImage : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var likeLabel : UILabel!
    @IBOutlet weak var picsLabel : UILabel!
    @IBOutlet weak var albumLabel : UILabel!
    @IBOutlet weak var ratingLabel : UILabel!
    
    @IBOutlet weak var rightView : CustomUIView!
    @IBOutlet weak var cameraButton : UIButton!
    
}

extension FPicSearchVC : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dataFiltered = searchText.isEmpty ? nameArray : nameArray.filter({ (dat) -> Bool in
            dat.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        dropDown.dataSource = dataFiltered
        filterIdArray = []
        filterIsoArray = []
        var localarr = nameArray
        
        for str in dataFiltered {
            var i : Int = 0
            for str1 in localarr {
                if (str == str1) {
                    filterIdArray.append(idArray[i])
                    filterIsoArray.append(isoArray[i])
                    localarr[i] = ""
                    break
                }
                i += 1
            }
        }
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            if (self.check == "country"){
                let codestr = (self.isoArray[index]).lowercased()
                cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
            }
            else {
                cell.flagImage.image = nil
            }
            cell.codeLabel.text = ""
        }
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            if (self.check == "country"){
                self.countryLabel.text = self.dataFiltered[index]
                self.country_id = self.filterIdArray[index]
            }
            else {
                self.areaLabel.text = self.dataFiltered[index]
                self.area_id = self.filterIdArray[index]
            }
            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
        }
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        dataFiltered = nameArray
        filterIdArray = idArray
        filterIsoArray = isoArray
        searchBar.isHidden = true
        dropDown.hide()
    }
}

extension FPicSearchVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str = "\(textField.text!)\(string)"
        if (string == "")
        {
            str.removeLast()
        }
        
        userNameFlitered = str.isEmpty ? userNameArr : userNameArr.filter({ (dat) -> Bool in
            dat.range(of: str, options: .caseInsensitive) != nil
        })
        
        userDataFlitered = []
        
        for str in userNameFlitered {
            var i : Int = 0
            for str1 in userNameArr {
                if (str == str1) {
                    userDataFlitered.append(self.dataArray[i])
                    break
                }
                i += 1
            }
        }
        searchTableView.reloadData()
        
        return true
    }
}
