//
//  FPMyPicAlbumVC.swift
//  Darious
//
//  Created by Apple on 25/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import Photos

class FPMyPicAlbumVC: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var listBtn : UIButton!
    @IBOutlet weak var gridBtn : UIButton!
    @IBOutlet weak var downloadBtn : UIButton!
    @IBOutlet weak var checkBtn : UIButton!
    @IBOutlet weak var allLbl : UILabel!
    @IBOutlet weak var upperViewHt : NSLayoutConstraint!
    
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var noFound : UILabel!
    @IBOutlet weak var topTitle : UILabel!
    
    @IBOutlet weak var downloadView : CustomUIView!
    @IBOutlet weak var dwnldLabel : UILabel!
    @IBOutlet weak var progressBar : UIProgressView!
    @IBOutlet weak var percentageLbl : UILabel!
    @IBOutlet weak var countLabel : UILabel!
    @IBOutlet weak var blurView : UIView!
    @IBOutlet weak var cancelBtn : KGHighLightedButton!
    
    @IBOutlet weak var downloadCollVw : UICollectionView!
    @IBOutlet weak var picsTableVw : UITableView!
    
    var dataArray : [NSDictionary] = []
    var heightArray : [CGFloat] = []
    var selectPics : [String] = []
    var selectIds : [String] = []
    
    var passName : String = ""
    var passid : String = ""
    var flagiso : String = ""
    var codeStr : String = ""
    var viewOriginY : CGFloat = 0
    var timer : Timer!
    var checkCancel : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            if (btnCollection[idx].tag == 0) {
                btnCollection[idx].alpha = 1
                imgCollection[idx].tintColor = UIColor.white
            }
            else {
                btnCollection[idx].alpha = 0.15
                imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            }
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        downloadBtn.isEnabled = false
        noFound.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_pics")
        dwnldLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_downloading")
        
        let tapgesture = UITapGestureRecognizer.init(target: self, action: #selector(touchOnBlurView(_:)))
        tapgesture.numberOfTapsRequired = 1
        blurView.addGestureRecognizer(tapgesture)
        
        setBottomView()
      //  setupLongPressGesture()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FP_Download_List, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
        
        if (Cart_Count > 0)
        {
            cartCountLbl.isHidden = false
            cartCountLbl.text = "\(Cart_Count)"
        }
        else {
            cartCountLbl.isHidden = true
        }
    }
    
    @objc func touchOnBlurView(_ gesture : UIGestureRecognizer)
    {
        if (percentageLbl.text == "100 %")
        {
            downloadView.isHidden = true
            blurView.isHidden = true
            percentageLbl.text = "0 %"
            countLabel.text = "0"
            progressBar.progress = 0
            selectIds = []
            selectPics = []
            downloadCollVw.reloadData()
        }
    }
    
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 0.3 // 1 second press
        longPressGesture.delegate = self
        self.downloadCollVw.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.downloadCollVw)
            if let indexPath = downloadCollVw.indexPathForItem(at: touchPoint)
            {
             //   print("press long......\(indexPath)")
                let cell = downloadCollVw.cellForItem(at: indexPath) as! FPAlbumPicCollCell
                cell.checkBtn.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
                cell.checkBtn.isHidden = false
                let dict = dataArray[indexPath.item]
                selectPics.append("\(dict["original_pic"] as! String)")
                selectIds.append("\(dict["id"] as! NSNumber)")
                downloadBtn.isEnabled = true
            }
        }
    }
    
    @IBAction func downloadsMultiplePics(_ sender: Any)
    {
        customLoader.show("\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_loading"))....", showColor: ColorCode.FPicThemeColor)
        timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(callfunction), userInfo: nil, repeats: false)
        timer.fire()
    }
    
    @objc func callfunction()
    {
        DispatchQueue.main.async {
            customLoader.hide()
            self.timer.invalidate()
            self.downloadView.isHidden = false
            self.blurView.isHidden = false
        }
        
        var ii = 0
        for str in self.selectPics
        {
            let imgstr = "\(Api.FP_Original_Url)\(str)"
            let url = URL(string: imgstr)
            DispatchQueue.main.async {
                if let data = try? Data(contentsOf: url!)
                {
                    let image: UIImage = UIImage(data: data)!
                    PHPhotoLibrary.saveImage(image: image, albumName: "DakeFly") { (result) in
                        
                        ii += 1
                        OperationQueue.main.addOperation {
                            self.countLabel.text = "\(ii)/\(self.selectPics.count)"
                            self.progressBar.progress = Float((Float(ii)/Float(self.selectPics.count)))
                            self.percentageLbl.text = "\(Int((Float(ii)/Float(self.selectPics.count)) * 100)) %"
                         //   print("ffffff = \(Float(ii))-----\n----\(Float(self.selectPics.count))---\n---\((Float(ii)/Float(self.selectPics.count)))---\n---\(Int((Float(ii)/Float(self.selectPics.count)) * 100))")
                            if (Int((Float(ii)/Float(self.selectPics.count)) * 100) == 100)
                            {
                                StaticFunctions.showAlert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_downloading_complete"), actions: [OkText], controller: self, completion: { (str) in
                                    
                                    self.downloadView.isHidden = true
                                    self.blurView.isHidden = true
                                    self.percentageLbl.text = "0 %"
                                    self.countLabel.text = "0"
                                    self.progressBar.progress = 0
                                    self.selectIds = []
                                    self.selectPics = []
                                    self.downloadCollVw.reloadData()
                                })
                            }
                        }
                        if (self.checkCancel == true)
                        {
                            return
                        }
                    }
                }
            }
           // break
        }
    }
    
    @IBAction func cancelDownloading(_ sender: Any)
    {
        checkCancel = true
        downloadView.isHidden = true
        blurView.isHidden = true
        percentageLbl.text = "0 %"
        countLabel.text = "0"
        progressBar.progress = 0
        selectIds = []
        selectPics = []
        downloadCollVw.reloadData()
    }
    
    @IBAction func checkAll(_ sender: Any)
    {
        if (dataArray.count > 0)
        {
            if (checkBtn.image(for: .normal) == nil)
            {
                checkBtn.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
                checkBtn.backgroundColor = ColorCode.FBThemeColor
                for dict in dataArray
                {
                    selectPics.append("\(dict["original_pic"] as! String)")
                }
                downloadBtn.isEnabled = true
            }
            else {
                checkBtn.setImage(nil, for: .normal)
                checkBtn.backgroundColor = UIColor.white
                selectPics = []
                downloadBtn.isEnabled = false
            }
            downloadCollVw.reloadData()
        }
    }
    
    @IBAction func viewInList(_ sender: Any)
    {
        listBtn.setImage(#imageLiteral(resourceName: "FP_listview"), for: .normal)
        gridBtn.setImage(#imageLiteral(resourceName: "FP_gridview"), for: .normal)
        downloadBtn.isEnabled = false
        upperViewHt.constant = 40
        downloadCollVw.isHidden = true
        checkBtn.isHidden = true
        checkBtn.setImage(nil, for: .normal)
        allLbl.isHidden = true
        picsTableVw.isHidden = false
        
        UIView.transition(with: picsTableVw, duration: 1.0, options: .transitionFlipFromLeft, animations: {self.picsTableVw.reloadData()}, completion: nil)
    }
    
    @IBAction func viewInGrid(_ sender: Any)
    {
        listBtn.setImage(#imageLiteral(resourceName: "FP_WhiteListVw"), for: .normal)
        gridBtn.setImage(#imageLiteral(resourceName: "FP_FillGridVw"), for: .normal)
        downloadBtn.isEnabled = false
        upperViewHt.constant = 80
        checkBtn.isHidden = false
        allLbl.isHidden = false
        downloadCollVw.isHidden = false
        picsTableVw.isHidden = true
        
        UIView.transition(with: downloadCollVw, duration: 1.0, options: .transitionFlipFromRight, animations: {self.downloadCollVw.reloadData()}, completion: nil)
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
             //   print("picsssssss = \(result)")
                let data = result["data"] as! [NSDictionary]
                if (data.count > 0) {
                    self.dataArray = result["data"] as! [NSDictionary]
                    self.viewInList(self.listBtn)
                } else {
                    self.noFound.isHidden = false
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypicUserTopActions(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicUserBtmAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FPMyPicAlbumVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = picsTableVw.dequeueReusableCell(withIdentifier: "picalbumcell") as! FPPicAlbumTabelCell
        
        let dict = dataArray[indexPath.section]
        let imgUrl = URL(string: "\(Api.FP_Original_Url)\(dict["original_pic"] as! String)")
        cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "Placeholder"))

        cell.downloadBtn.addTarget(self, action: #selector(downloadLocally(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: picsTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func downloadLocally(_ sender : UIButton)
    {
       // customLoader.show("\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_downloading"))....", showColor: ColorCode.FPicThemeColor)
        let point = sender.convert(CGPoint.zero, to: picsTableVw)
        let idxpath = picsTableVw.indexPathForRow(at: point)!
        let dict = dataArray[idxpath.section]
        
        self.downloadView.isHidden = false
        self.blurView.isHidden = false
        
        let imgstr = "\(Api.FP_Original_Url)\(dict["original_pic"] as! String)"
        let url = URL(string: imgstr)
        if let data = try? Data(contentsOf: url!)
        {
            let image: UIImage = UIImage(data: data)!
            PHPhotoLibrary.saveImage(image: image, albumName: "DakeFly") { (result) in
             //   print("result = \(result)")
                OperationQueue.main.addOperation {
                //    customLoader.hide()
                    if (result == false){
                        ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_no_permission"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_setting_gallery_permission"), controller: self)
                    }
                    else {
                        self.countLabel.text = "\(1)/\(1)"
                        self.progressBar.progress = Float((Float(1)/Float(1)))
                        self.percentageLbl.text = "\(Int((Float(1)/Float(1)) * 100)) %"
                        
                        if (Int((Float(1)/Float(1)) * 100) == 100)
                        {
                            StaticFunctions.showAlert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_downloading_complete"), actions: [OkText], controller: self, completion: { (str) in
                                
                                self.downloadView.isHidden = true
                                self.blurView.isHidden = true
                                self.percentageLbl.text = "0 %"
                                self.countLabel.text = "0"
                                self.progressBar.progress = 0
                                self.selectIds = []
                                self.selectPics = []
                                self.downloadCollVw.reloadData()
                            })
                        }
                    }
                }
            }
        }
        else {
            customLoader.hide()
        }
    }
}

class FPPicAlbumTabelCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var picImageVw : UIImageView!
    
    @IBOutlet weak var downloadBtn : UIButton!
    
}

/// MAKR : CollectionView Methods
extension FPMyPicAlbumVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = downloadCollVw.dequeueReusableCell(withReuseIdentifier: "albumpiccollcell", for: indexPath) as! FPAlbumPicCollCell
        
        let dict = dataArray[indexPath.item]
        let imgUrl = URL(string: "\(Api.FP_Original_Url)\(dict["original_pic"] as! String)")
        cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "FBest_other"))
        
        if (selectIds.contains("\(dict["id"] as! NSNumber)") == true){
            cell.checkBtn.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
        }
        else {
            cell.checkBtn.setImage(nil, for: .normal)
        }
        
        cell.checkBtn.addTarget(self, action: #selector(selectImages(_:)), for: .touchUpInside)
        cell.downloadBtn.addTarget(self, action: #selector(downloadImageLocally(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            return CGSize(width: 85, height: 85)
        }
        else if (self.view.frame.width == 375)
        {
            return CGSize(width: 90, height: 90)
        }
        else if(self.view.frame.width == 414) {
            return CGSize(width: 92, height: 92)
        }else {
            return CGSize(width: 95, height: 95)
        }
    }

    @objc func selectImages(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: downloadCollVw)
        let idxpath = downloadCollVw.indexPathForItem(at: point)!
        
        let cell = downloadCollVw.cellForItem(at: idxpath) as! FPAlbumPicCollCell
        let dict = dataArray[idxpath.item]
        
        if (selectIds.contains("\(dict["id"] as! NSNumber)"))
        {
            cell.checkBtn.setImage(nil, for: .normal)
            let arr : NSArray = NSArray.init(array: selectIds)
            let ii = arr.index(of: "\(dict["id"] as! NSNumber)")
            selectIds.remove(at: ii)
            selectPics.remove(at: ii)
            
            if (selectIds.count > 0){
                downloadBtn.isEnabled = true
            }
            else {
                downloadBtn.isEnabled = false
            }
        }
        else {
            cell.checkBtn.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
            selectPics.append("\(dict["original_pic"] as! String)")
            selectIds.append("\(dict["id"] as! NSNumber)")
            downloadBtn.isEnabled = true
        }
    }
    
    @objc func downloadImageLocally(_ sender : UIButton)
    {
        self.downloadView.isHidden = false
        self.blurView.isHidden = false

        let point = sender.convert(CGPoint.zero, to: downloadCollVw)
        let idxpath = downloadCollVw.indexPathForItem(at: point)!
        let dict = dataArray[idxpath.item]
    
        let imgstr = "\(Api.FP_Original_Url)\(dict["original_pic"] as! String)"
        let url = URL(string: imgstr)
        if let data = try? Data(contentsOf: url!)
        {
            let image: UIImage = UIImage(data: data)!
            PHPhotoLibrary.saveImage(image: image, albumName: "DakeFly") { (result) in
                OperationQueue.main.addOperation {
                    customLoader.hide()
                    if (result == false){
                        ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_no_permission"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_setting_gallery_permission"), controller: self)
                    }
                    else {
                        self.countLabel.text = "\(1)/\(1)"
                        self.progressBar.progress = Float((Float(1)/Float(1)))
                        self.percentageLbl.text = "\(Int((Float(1)/Float(1)) * 100)) %"
                        
                        if (Int((Float(1)/Float(1)) * 100) == 100)
                        {
                            StaticFunctions.showAlert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "downloading_complete"), actions: [OkText], controller: self, completion: { (str) in
                                
                                self.downloadView.isHidden = true
                                self.blurView.isHidden = true
                                self.percentageLbl.text = "0 %"
                                self.countLabel.text = "0"
                                self.progressBar.progress = 0
                                self.selectIds = []
                                self.selectPics = []
                                self.downloadCollVw.reloadData()
                            })
                        }
                    }

                }
            }
        }else {
            customLoader.hide()
        }
    }
}

class FPAlbumPicCollCell: UICollectionViewCell {
    
    @IBOutlet weak var picImageVw : UIImageView!
    @IBOutlet weak var checkBtn : UIButton!
    @IBOutlet weak var downloadBtn : UIButton!
}
