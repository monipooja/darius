//
//  FPAddCartVC.swift
//  Darious
//
//  Created by Apple on 02/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import Photos

class FPAddCartVC: UIViewController, MollieWebVCDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var buyView : UIView!
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var nodataLabel : UILabel!
    @IBOutlet weak var picCountLbl : UILabel!
    @IBOutlet weak var picTxtLbl : UILabel!
    @IBOutlet weak var totalTxtLbl : UILabel!
    @IBOutlet weak var totalLbl : UILabel!
    @IBOutlet weak var buyBtn : KGHighLightedButton!
    
    @IBOutlet weak var cartTableView : UITableView!
    
    // Center view outlets
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var centerView: CustomUIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var commissionLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var subtotalValue: UILabel!
    @IBOutlet weak var commissionValue: UILabel!
    @IBOutlet weak var totalValue: UILabel!
    @IBOutlet weak var continueBtn: KGHighLightedButton!
    
    @IBOutlet weak var centerErrorVw: CustomUIView!
    @IBOutlet weak var failedLabel: UILabel!
    @IBOutlet weak var successLabel: UILabel!
    @IBOutlet weak var statusTitleLabel: UILabel!
    @IBOutlet weak var handImage: UIImageView!
    
    @IBOutlet weak var paymentView: CustomUIView!
    @IBOutlet weak var walletBtn: UIButton!
    @IBOutlet weak var creditBtn: UIButton!
    @IBOutlet weak var selectLabel: UILabel!
    
    var dataArray : [NSDictionary] = []
    var user_array : [String] = []
    
    var mutabledict : NSMutableDictionary = [:]
    var total_amount : NSMutableDictionary = [:]
    
    var total_comsn : Float = 0
    var total : Float = 0
    var transactionId : String = ""
    var order_id : String = ""
    var payment_type : String = ""
    var photogrpher_id : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            if (btnCollection[idx].tag == 2) {
                btnCollection[idx].alpha = 1
                imgCollection[idx].tintColor = UIColor.white
            }
            else {
                btnCollection[idx].alpha = 0.15
                imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            }
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        blurView.addGestureRecognizer(gesture)
        
        nodataLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_cart")
        totalTxtLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "total")
        picTxtLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_total_pics")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_cart")
        titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "card_method")
        subtotalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "subtotal")
        totalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "total")
        commissionLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "commission")
        successLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_payment_done")
        failedLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "payment_not_done")
        continueBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
        buyBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "fp_buy")), for: .normal)
        
        selectLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "choose_payment_method")
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func callDidAppear() {
        viewDidAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (transactionId != ""){
            ApiResponse.onResponseGetMollie(url: "https://api.mollie.com/v2/payments/\(transactionId)") { (result, error) in
                OperationQueue.main.addOperation {
                    customLoader.hideIndicator()
                    if (error == "")
                    {
                      //  print("mollie trans result = \(result)")
                        let status = "\(result["status"] ?? "")"
                        self.blurView.alpha = 0.9
                        self.blurView.backgroundColor = UIColor.init(hexString: "919191")
                        self.transactionId = ""
                        if status == "canceled" || status == "failed" || status == "open" {
                            self.blurView.isHidden = false
                            self.statusTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "error")
                            self.failedLabel.isHidden = false
                            self.successLabel.isHidden = true
                            self.handImage.image = #imageLiteral(resourceName: "CancelCross")
                            self.centerErrorVw.isHidden = false
                        } else {
                            if (status == "paid") {
                                self.blurView.isHidden = false
                                self.statusTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "common_done_ios")
                                self.failedLabel.isHidden = true
                                self.successLabel.isHidden = false
                                self.handImage.image = #imageLiteral(resourceName: "SuccessChecked")
                                self.centerErrorVw.isHidden = false
                            }
                        }
                    }
                    else{}
                }
            }
        }
        else {
            if (Int(truncating: notification_count) > 0)
            {
                notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
            }
            setDataOnLoad()
        }
    }
    
    // Mark : Gesture Action
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        self.view.endEditing(true)
        self.view.frame.origin.y = 0
        blurView.isHidden = true
        centerView.isHidden = true
        centerErrorVw.isHidden = true
        if (self.handImage.image == #imageLiteral(resourceName: "SuccessChecked"))
        {
            centerErrorVw.isHidden = true
            Cart_Count = 0
            userDefault.setValue([], forKey: "buy_photos")
            userDefault.synchronize()
            let ratevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPRatingVC") as! FPRatingVC
            self.navigationController?.pushViewController(ratevc, animated: false)
        }
    }
    
    func setDataOnLoad()
    {
        if let _ = userDefault.value(forKey: "buy_photos")
        {
            let dict = userDefault.value(forKey: "user_dict") as! NSDictionary
            if (dict != [:])
            {
                photogrpher_id = "\(dict["id"] as! NSNumber)"
            }
            
            dataArray = userDefault.value(forKey: "buy_photos") as! [NSDictionary]
            
           // print("data array = \(dataArray)")
            
            if (dataArray.count > 0)
            {
                picCountLbl.text = "\(dataArray.count)"
                var totalAmt : Float = 0
                for dict in dataArray
                {
                    if (user_array == []) {
                        user_array.append("\(dict["user_id"] as! String)")
                    } else {
                        if (user_array.contains("\(dict["user_id"] as! String)") == false) {
                            user_array.append("\(dict["user_id"] as! String)")
                        }
                    }
                   
                    if let _ = dict["price"] as? NSNumber
                    {
                        let prc1 = dict["price"] as! NSNumber
                        totalAmt = totalAmt + Float(exactly: prc1)!
                    }
                    else {
                        if (dict["price"] as! String != "") && (dict["price"] as! String != "free")
                        {
                            let prc1 = dict["price"] as! String
                            if let _ = Float(prc1)
                            {
                                totalAmt = totalAmt + Float(prc1)!
                            }
                        }
                    }
                }
                totalLbl.text = "\(totalAmt) \(Current_Currency)"
                
                buyView.isHidden = false
                cartTableView.isHidden = false
                cartTableView.reloadData()
            }
            else {
                buyView.isHidden = true
                picCountLbl.text = "0"
                totalLbl.text = "0 \(Current_Currency)"
                cartTableView.isHidden = true
                nodataLabel.isHidden = false
            }
        }
        else {
            buyView.isHidden = true
            picCountLbl.text = "0"
            totalLbl.text = "0 \(Current_Currency)"
            cartTableView.isHidden = true
            nodataLabel.isHidden = false
        }
        
        var totalAmt : Float = 0
        for userID in user_array
        {
            var userAmt : Float = 0
            var dictarr : [NSDictionary] = []
            
            for dict in dataArray
            {
                var prcvalue = ""
                if let _ = dict["price"] as? NSNumber
                {
                    prcvalue = "\(dict["price"] as! NSNumber)"
                }
                else {
                    if (dict["price"] as! String != "") && (dict["price"] as! String != "free")
                    {
                        prcvalue = dict["price"] as! String
                    }
                    else {
                        prcvalue = "0"
                    }
                }
                
                if ("\(dict["user_id"] as! String)" == userID)
                {
                    let senddict = ["pic_id" : "\(dict["id"] as! NSNumber)", "album_id" : "\(dict["album_id"] as! String)", "amount" : "\(prcvalue)"]
                    dictarr.append(senddict as NSDictionary)
                    
                    if let _ = Float(prcvalue)
                    {
                        userAmt = userAmt + Float(prcvalue)!
                    }
                }
            }
            mutabledict.setValue(dictarr, forKey: "\(userID)")
            
            total_amount.setValue("\(userAmt)", forKey: "\(userID)")
            
            totalAmt = totalAmt + userAmt
        }
        total_amount.setValue("\(totalAmt)", forKey: "total_amount")
    }
    
    @IBAction func buyProducts(_ sender: Any)
    {
        let totalValue1 = (self.totalLbl.text?.replacingOccurrences(of: "\(Current_Currency)", with: ""))?.trimmingCharacters(in: .whitespaces)
        
        if (Float(totalValue1!)! > Float(0))
        {
            paymentView.isHidden = false
            blurView.isHidden = false
        }
        else {
            payment_type = "wallet"
            callServiceAfterPayment()
        }
    }
    
    @IBAction func onContinue(_ sender: Any)
    {
        self.callServiceAfterPayment()
    }
    
    @IBAction func onhideView(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
    }
    
    @IBAction func onHideErrorView(_ sender: Any)
    {
        blurView.isHidden = true
        centerErrorVw.isHidden = true
        if (self.handImage.image == #imageLiteral(resourceName: "SuccessChecked"))
        {
            centerErrorVw.isHidden = true
            Cart_Count = 0
            userDefault.setValue([], forKey: "buy_photos")
            userDefault.synchronize()
            let ratevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPRatingVC") as! FPRatingVC
            self.navigationController?.pushViewController(ratevc, animated: false)
        }
    }
    
    @IBAction func onHidePaymentView(_ sender: Any)
    {
        blurView.isHidden = true
        paymentView.isHidden = true
    }
    
    @IBAction func onSelectWallet(_ sender: Any)
    {
        paymentView.isHidden = true
        payment_type = "wallet"
        
        let totalValue1 = (self.totalLbl.text?.replacingOccurrences(of: "\(Current_Currency)", with: ""))?.trimmingCharacters(in: .whitespaces)
       
        if (Float(totalValue1!)! > Float(truncating: wallet_amount))
        {
            let titlStr = ApiResponse.getLanguageFromUserDefaults(inputString: "invalid_wallet_amount")
            StaticFunctions.showAlert(title: titlStr, message: ApiResponse.getLanguageFromUserDefaults(inputString: "fc_continue_with_card"), actions: [YesText, NoText], controller: self) { (str) in
                if (str == YesText)
                {
                    self.payment_type = "card"
                    self.calculationForCard()
                }else {
                    self.blurView.isHidden = true
                    self.paymentView.isHidden = true
                }
            }
        }
        else {
            callServiceAfterPayment()
        }
    }
    
    @IBAction func onSelectCard(_ sender: Any)
    {
        paymentView.isHidden = true
        payment_type = "card"
        
        calculationForCard()
    }
    
    func calculationForCard()
    {
        let totalValue1 = (totalLbl.text?.replacingOccurrences(of: "\(Current_Currency)", with: ""))?.trimmingCharacters(in: .whitespaces)
        
        if (totalValue1!.contains(".") == false) && (totalValue1!.contains(",") == false)
        {
            subtotalValue.text = "\(totalValue1!).0 \(Current_Currency)"
        }
        else {
            if (totalValue1!.contains(".") == true)
            {
                let sp = totalValue1?.split(separator: ".")
                if (sp?.count == 1)
                {
                    subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: totalValue1!) + "0 \(Current_Currency)"
                }
                else {
                    subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: totalValue1!) + " \(Current_Currency)"
                }
            }
            else {
                let sp = totalValue1?.split(separator: ",")
                if (sp?.count == 1)
                {
                    subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: totalValue1!) + "0 \(Current_Currency)"
                }
                else {
                    subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: totalValue1!) + " \(Current_Currency)"
                }
            }
        }
        
        if ((totalValue1?.contains(","))!) {
            let amtstr = totalValue1?.replacingOccurrences(of: ",", with: ".")
            total_comsn = ((Float(amtstr!)!) * (Float(commission_value)!/100)) + Float(commission_Card)!
            total = Float(amtstr!)! + total_comsn
            total = Float(String(format: "%.2f", total))!
        }
        else {
            let amountStr = "\(totalValue1!)"
            total_comsn = ((Float(amountStr)!) * (Float(commission_value)!/100)) + Float(commission_Card)!
            total = Float(amountStr)! + total_comsn
            total = Float(String(format: "%.2f", total))!
        }
        commissionValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", total_comsn)) + " \(Current_Currency)"
        totalValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", total)) + " \(Current_Currency)"
        
        centerView.isHidden = false
    }
    
    func callServiceAfterPayment()
    {
        if let _ = StaticFunctions.toJsonFromObject(passObj: mutabledict)
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            
            let jsonStr = StaticFunctions.toJsonFromObject(passObj: mutabledict)
            let amtStr = StaticFunctions.toJsonFromObject(passObj: total_amount)
            
            let params = ["access_token": "\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "amount" : "\(amtStr!)", "data" : "\(jsonStr!)", "type" : payment_type]
         //   print("params = \(params)")
            ApiResponse.onResponsePost(url: Api.FP_Download_Pic, parms: params as NSDictionary, controller: self) { (result) in
                
                OperationQueue.main.addOperation {
                    customLoader.hideIndicator()
                   
                    self.order_id = "\(result["order_id"] as! NSNumber)"
                    if (self.payment_type == "wallet") {
                        
                        let totalValue1 = (self.totalLbl.text?.replacingOccurrences(of: "\(Current_Currency)", with: ""))?.trimmingCharacters(in: .whitespaces)
                        if (Float(totalValue1!)! > Float(0))
                        {
                            customLoader.hideIndicator()
                            
                            wallet_amount = result["wallet_amount"] as! NSNumber
                            userDefault.set(wallet_amount, forKey: "walletAmt")
                            userDefault.synchronize()
                            
                            self.blurView.isHidden = false
                            self.statusTitleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "common_done_ios")
                            self.failedLabel.isHidden = true
                            self.successLabel.isHidden = false
                            self.handImage.image = #imageLiteral(resourceName: "SuccessChecked")
                            self.centerErrorVw.isHidden = false
                        }
                        else {
                            Cart_Count = 0
                            userDefault.setValue([], forKey: "buy_photos")
                            userDefault.synchronize()
                            let ratevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPRatingVC") as! FPRatingVC
                            self.navigationController?.pushViewController(ratevc, animated: false)
                        }
                    }
                    else {
                        let aa = "\(self.total)".split(separator: ".")

                        if (aa[1].count == 1) {
                            self.molliePayment(payment: "\(self.total)0", cid: "")
                        }
                        else {
                            self.molliePayment(payment: "\(self.total)", cid: "")
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    // Mollie Payment Service Call
    func molliePayment(payment : String, cid : String)
    {
        let amountDict : NSDictionary = ["currency" : "EUR", "value" : payment]
        let date = Date()
        let formatter = DateFormatter.init()
        formatter.dateFormat = "yyyyMMdd"
        let datestr = formatter.string(from: date)
        let metaDict : NSDictionary = ["uinique_id" : "\(datestr)\(user_id)", "commission" : "\(total_comsn)", "order_id" : "\(order_id)"]
        
        let params : [String : Any] = ["metadata" : metaDict, "amount" : amountDict, "description" : "\(ApiResponse.getLanguageFromUserDefaults(inputString: "subscription_business"))", "customerId" : "\(cid)", "redirectUrl" : "http://www.myapp.com/", "webhookUrl" : "\(Api.Base_URL)flypics/payment_for_pic/\(user_id)"]
       
        ApiResponse.onResponsePostMollie2(url: "https://api.mollie.com/v2/payments", parms: params) { (result, error) in
            // print("mollie payment result = \(result)")
            
            OperationQueue.main.addOperation {
                if let _ = result["_links"] as? NSDictionary {
                    
                    let checkoutUrl = ((result["_links"] as! NSDictionary)["checkout"] as! NSDictionary)["href"] as! String
                    self.transactionId = result["id"] as! String
                    customLoader.hideIndicator()
                    self.centerView.isHidden = true
                    self.blurView.isHidden = true
                    let webvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "MollieWebviewVC") as! MollieWebviewVC
                    webvc.delegate = self
                    webvc.checkouturl = checkoutUrl
                    webvc.transid = result["id"] as! String
                    webvc.comeFrom = "cartvc"
                    self.navigationController?.pushViewController(webvc, animated: false)
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    // MARK : Service Calling
    func callService(urlStr : String, params : String, check : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
        ApiResponse.onPostPhp(url: urlStr, parms: params, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
               // print("wallet transaction = \(result)")
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypicUserTopActions(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicUserBtmAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FPAddCartVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = cartTableView.dequeueReusableCell(withIdentifier: "fpcartcell") as! FPCartTabelCell
        
        let dict = dataArray[indexPath.section]
        
        let imgUrl = URL(string: "\(Api.FP_Pic_Url)\(dict["pic_name"] as! String)")
        cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        
        cell.photographerNameLbl.text = dict["name"] as? String
        cell.albumNameLbl.text = dict["album_name"] as? String
        cell.dateLabel.text = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd/MM/yyyy")
        
        if let _ = dict["price"] as? NSNumber
        {
            cell.priceLabel.text = "\(dict["price"] as! NSNumber) \(Current_Currency)"
        }
        else {
            if (dict["price"] as! String == "") && (dict["price"] as! String == "free") {
                cell.priceLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_free")
            }
            else {
                cell.priceLabel.text = (dict["price"] as? String)! + " \(Current_Currency)"
            }
        }
        
        cell.removeButton.addTarget(self, action: #selector(removePhoto(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: cartTableView.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func removePhoto(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: cartTableView)
        let idxPath = cartTableView.indexPathForRow(at: point)!
        
        dataArray.remove(at: idxPath.section)
        Cart_Count = dataArray.count
        userDefault.setValue(dataArray, forKey: "buy_photos")
        if (dataArray.count == 0)
        {
            userDefault.setValue([:], forKey: "user_dict")
        }
        userDefault.synchronize()
        viewDidLoad()
        setDataOnLoad()
    }
}

class FPCartTabelCell: UITableViewCell {
    
    @IBOutlet weak var picImageVw : UIImageView!
    @IBOutlet weak var albumNameLbl : UILabel!
    @IBOutlet weak var photographerNameLbl : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    
    @IBOutlet weak var removeButton : UIButton!
    
}
