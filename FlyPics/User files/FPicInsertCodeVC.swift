//
//  FPicInsertCodeVC.swift
//  Darious
//
//  Created by Apple on 09/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPicInsertCodeVC: UIViewController {

    @IBOutlet weak var firstTF : UITextField!
    @IBOutlet weak var secondTF : UITextField!
    @IBOutlet weak var thirdTF : UITextField!
    @IBOutlet weak var fourthTF : UITextField!
    @IBOutlet weak var insertLbl : UILabel!
    
    @IBOutlet weak var saveBtn : KGHighLightedButton!

    var album_Id : String = ""
    var album_name : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        
        firstTF.becomeFirstResponder()
        
        insertLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_insert_code")
        saveBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_unlock"), for: .normal)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func checkInsertCode(_ sender: Any)
    {
        if (firstTF.text == "") || (secondTF.text == "") || (thirdTF.text == "") || (fourthTF.text == "")
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_enter_correct_code"), controller: self)
        }
        else {
            let codeStr = "\(firstTF.text!)\(secondTF.text!)\(thirdTF.text!)\(fourthTF.text!)"
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&album_id=\(album_Id)&code=\(codeStr)"
            self.callService(urlstr: Api.FP_Unlock_Album, parameters: params, check: "get")
        }
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                //print("code unlock result = \(result)")
                var arr = userDefault.value(forKey: "unlock_albums") as! [String]
                arr.append(self.album_Id)
                userDefault.set(arr, forKey: "unlock_albums")
                userDefault.synchronize()
                
                let photosvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPUserPhotosVC") as! FPUserPhotosVC
                photosvc.album_Id = "\(self.album_Id)"
                photosvc.album_name = "\(self.album_name)"
                self.navigationController?.pushViewController(photosvc, animated: false)
                customLoader.hideIndicator()
            }
        }
    }
    
}

extension FPicInsertCodeVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((firstTF.text!.count) < 2) {
            
            if (firstTF.text != "") {
                if (secondTF.text != "") {
                    if (thirdTF.text != "") {
                        if (string == "") {
                            if (fourthTF.text == "") {
                                thirdTF.text = ""
                                secondTF.becomeFirstResponder()
                            }else {
                                fourthTF.text = ""
                                thirdTF.becomeFirstResponder()
                            }
                        }
                        else {
                            fourthTF.text = string
                            fourthTF.becomeFirstResponder()
                        }
                    } else {
                        if (string == "") {
                            if (thirdTF.text == "") {
                                secondTF.text = ""
                                firstTF.becomeFirstResponder()
                            }else {
                                thirdTF.text = ""
                                secondTF.becomeFirstResponder()
                            }
                        }
                        else {
                            thirdTF.text = string
                            thirdTF.becomeFirstResponder()
                        }
                    }
                } else {
                    if (string == "") {
                        if (secondTF.text == "") {
                            firstTF.text = ""
                            firstTF.becomeFirstResponder()
                        }else {
                            secondTF.text = ""
                            firstTF.becomeFirstResponder()
                        }
                    }
                    else {
                        secondTF.text = string
                        secondTF.becomeFirstResponder()
                    }
                }
            }
            else {
                return true
            }
        }
        else {
            // print("texxxxx = \(string)")
        }
        return false
    }
}
