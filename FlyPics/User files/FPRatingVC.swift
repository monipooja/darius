//
//  FPRatingVC.swift
//  Darious
//
//  Created by Apple on 19/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPRatingVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    @IBOutlet weak var saveButton : KGHighLightedButton!
    
    @IBOutlet weak var topTitle : UILabel!
    
    @IBOutlet weak var rateTableVw : UITableView!
    
    var dataArray : [NSDictionary] = []
    var heightArray : [CGFloat] = []
    var userRateArr : [NSMutableDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            if (btnCollection[idx].tag == 2) {
                btnCollection[idx].alpha = 1
                imgCollection[idx].tintColor = UIColor.white
            }
            else {
                btnCollection[idx].alpha = 0.15
                imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            }
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        setDataOnLoad()
      
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_rate")
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func setDataOnLoad()
    {
        let dict = userDefault.value(forKey: "user_dict") as! NSDictionary
        dataArray.append(dict)
        rateTableVw.reloadData()
        
        for dict in dataArray
        {
            let mutabledict : NSMutableDictionary = ["user_id" : "\(dict["id"] as! NSNumber)", "rat_1":"0",  "rat_2":"0", "rat_3":"0"]
            userRateArr.append(mutabledict)
        }
    }
    
    @IBAction func onSaveRating(_ sender: Any)
    {
        if let _ = StaticFunctions.toJsonFromObject(passObj: userRateArr)
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let jsonStr = StaticFunctions.toJsonFromObject(passObj: userRateArr)
            
            let params = ["access_token": "\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "user_list":"\(jsonStr!)"]
            
          //  print("params = \(params)")
            
            ApiResponse.onResponsePost(url: Api.FP_Rate_User, parms: params as NSDictionary, controller: self) { (result) in
                
                OperationQueue.main.addOperation {
                    
                   // print("rate result = \(result)")
                    let msgStr = ApiResponse.getLanguageFromUserDefaults(inputString: result["message"] as! String)
                    StaticFunctions.showAlert(title: "", message: msgStr, actions: [OkText], controller: self, completion: { (str) in
                        
                        customLoader.hideIndicator()
                        userDefault.setValue([:], forKey: "user_dict")
                        userDefault.synchronize()
                        let sellsvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPUserSellVC") as! FPUserSellVC
                        sellsvc.from_screen = "rating"
                        self.navigationController?.pushViewController(sellsvc, animated: false)
                    })
                }
            }
        }
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }

    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypicUserTopActions(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicUserBtmAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FPRatingVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = rateTableVw.dequeueReusableCell(withIdentifier: "picratecell") as! FPRateTableCell
        
        let dict = dataArray[indexPath.section]

        if ((dict["profile_pic"] as! String).contains("graph.facebook.com") == true) || ((dict["profile_pic"] as! String).contains("googleusercontent") == true)
        {
            cell.profileImgVw.sd_setImage(with: URL(string: "\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        else {
            cell.profileImgVw.sd_setImage(with: URL(string: "\(Api.Profile_BaseURL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        cell.nameLabel.text = "\(dict["name"] as! String) (@\(dict["nickname"] as! String))"
        cell.placeLabel.text = dict["address"] as? String
        cell.albumLabel.text = "\(dict["album_count"] as! NSNumber)"

        let arr1 = (dict["pic_count"] as! String).split(separator: ",")
        let piccount = arr1[0].split(separator: "=")
        let like = arr1[1].split(separator: "=")

        cell.picsLabel.text = "\(piccount[1])"
        cell.likeLabel.text = "\(like[1])"
        
        if let _ = dict["rating"] as? NSNumber
        {
            cell.ratingLabel.text = String(format: "%.1f", Double.init("\(dict["rating"] as! NSNumber)")!)
        }
        
        cell.qualityLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_quality_work")
        cell.workflowLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_work_flow")
        cell.waitingLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_waiting_time")
        
        for btn in cell.qualityRateBtns
        {
            btn.addTarget(self, action: #selector(qualityOfWork(_:)), for: .touchUpInside)
        }
        for btn in cell.flowRateBtns
        {
            btn.addTarget(self, action: #selector(workFlow(_:)), for: .touchUpInside)
        }
        for btn in cell.waitingRateBtns
        {
            btn.addTarget(self, action: #selector(waitingTime(_:)), for: .touchUpInside)
        }
    
        return cell
    }
    
    @objc func qualityOfWork(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: rateTableVw)
        let idxpath = rateTableVw.indexPathForRow(at: point)!
        
        let dict = userRateArr[idxpath.section]
        dict["rat_1"] = "\(sender.tag)"
        
        let cell = rateTableVw.cellForRow(at: idxpath) as! FPRateTableCell
        setStars(passBtnColl: cell.qualityRateBtns, tagValue: sender.tag)
    }
    
    @objc func workFlow(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: rateTableVw)
        let idxpath = rateTableVw.indexPathForRow(at: point)!
        
        let dict = userRateArr[idxpath.section]
        dict["rat_2"] = "\(sender.tag)"
        
        let cell = rateTableVw.cellForRow(at: idxpath) as! FPRateTableCell
        setStars(passBtnColl: cell.flowRateBtns, tagValue: sender.tag)
    }
    
    @objc func waitingTime(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: rateTableVw)
        let idxpath = rateTableVw.indexPathForRow(at: point)!
        
        let dict = userRateArr[idxpath.section]
        dict["rat_3"] = "\(sender.tag)"
        
        let cell = rateTableVw.cellForRow(at: idxpath) as! FPRateTableCell
        setStars(passBtnColl: cell.waitingRateBtns, tagValue: sender.tag)
    }
    
    func setStars(passBtnColl : [UIButton], tagValue : Int)
    {
        for btn in passBtnColl
        {
            if (btn.tag <= tagValue)
            {
                btn.setImage(#imageLiteral(resourceName: "Yellow_star"), for: .normal)
            }
            else {
                btn.setImage(#imageLiteral(resourceName: "Gray_star"), for: .normal)
            }
        }
    }
}

class FPRateTableCell: UITableViewCell {
    
    @IBOutlet weak var profileImgVw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var likeLabel : UILabel!
    @IBOutlet weak var picsLabel : UILabel!
    @IBOutlet weak var albumLabel : UILabel!
    @IBOutlet weak var ratingLabel : UILabel!
    
    @IBOutlet weak var qualityLabel : UILabel!
    @IBOutlet weak var workflowLbl : UILabel!
    @IBOutlet weak var waitingLabel : UILabel!

    @IBOutlet var qualityRateBtns : [UIButton]!
    @IBOutlet var flowRateBtns : [UIButton]!
    @IBOutlet var waitingRateBtns : [UIButton]!
    
}
