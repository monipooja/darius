//
//  FPMySubscriptionVC.swift
//  Darious
//
//  Created by Apple on 18/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPMySubscriptionVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var noFound : UILabel!
    @IBOutlet weak var topTitle : UILabel!
    
    @IBOutlet weak var subscriptionTableVw : UITableView!
    
    var dataArray : [NSDictionary] = []
    var heightArray : [CGFloat] = []
    var selectPics : [String] = []
    
    var passName : String = ""
    var passid : String = ""
    var flagiso : String = ""
    var codeStr : String = ""
    var viewOriginY : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            if (btnCollection[idx].tag == 1) {
                btnCollection[idx].alpha = 1
                imgCollection[idx].tintColor = UIColor.white
            }
            else {
                btnCollection[idx].alpha = 0.15
                imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            }
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        noFound.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_subscription")
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FP_Subscribe_List, parameters: params, check: "subs")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
        
        if (Cart_Count > 0)
        {
            cartCountLbl.isHidden = false
            cartCountLbl.text = "\(Cart_Count)"
        }
        else {
           cartCountLbl.isHidden = true
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("subs list result = \(result)")
                let data = result["data"] as! [NSDictionary]
                if (data.count > 0) {
                    self.dataArray = result["data"] as! [NSDictionary]
                    self.subscriptionTableVw.reloadData()
                    self.subscriptionTableVw.isHidden = false
                } else {
                    self.noFound.isHidden = false
                    self.subscriptionTableVw.isHidden = true
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypicUserTopActions(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicUserBtmAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FPMySubscriptionVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = subscriptionTableVw.dequeueReusableCell(withIdentifier: "picsubscell") as! FPPicSubsTableCell
        
        let dict = dataArray[indexPath.section]
        
        cell.nameLabel.text = dict["name"] as? String
        cell.placeLabel.text = dict["location"] as? String
        cell.dateLbl.text = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd/MM/yyyy")
        
        if (dict["ready"] as! String == "no") {
            cell.cameraButton.alpha = 0.27
            //cell.cameraButton.isEnabled = false
        }
        else {
            cell.cameraButton.alpha = 1
            //  cell.cameraButton.isEnabled = true
        }
      cell.subscribeBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_subscribed"), for: .normal)
        cell.cameraButton.addTarget(self, action: #selector(goToPics(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: subscriptionTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func goToPics(_ sender : UIButton)
    {
/*        let point = sender.convert(CGPoint.zero, to: subscriptionTableVw)
        let idxpath = subscriptionTableVw.indexPathForRow(at: point)!
        let dict = dataArray[idxpath.section]
        
        if (dict["ready"] as! String == "no") {
            ApiResponse.alert(title: "Album is not ready", message: "We will notify you when this album is ready", controller: self)
        }
        else {
            let photosvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPUserPhotosVC") as! FPUserPhotosVC
            photosvc.passDict = [:]
            photosvc.album_Id = "\(dict["id"] as! NSNumber)"
            photosvc.album_name = "\(dict["name"] as! String)"
            present(photosvc, animated: false, completion: nil)
        } */
    }
}

class FPPicSubsTableCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var lockImgVw : UIImageView!
    
    @IBOutlet weak var rightView : CustomUIView!
    @IBOutlet weak var cameraButton : UIButton!
    
    @IBOutlet weak var subscribeBtn : KGHighLightedButton!
    
}
