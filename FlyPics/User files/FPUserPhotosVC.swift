//
//  FPUserPhotosVC.swift
//  Darious
//
//  Created by Apple on 02/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import Photos

class FPUserPhotosVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImageView : UIImageView!
    
    @IBOutlet var btnCollection : [KGHighLightedButton]!
    @IBOutlet var imgCollection : [UIImageView]!
    
    @IBOutlet weak var topTitle : UILabel!
    @IBOutlet weak var cartCountLbl : UILabel!
    @IBOutlet weak var profileImgVw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var placeLabel : UILabel!
    @IBOutlet weak var likeLabel : UILabel!
    @IBOutlet weak var picsLabel : UILabel!
    @IBOutlet weak var albumLabel : UILabel!
    @IBOutlet weak var ratingLabel : UILabel!
    @IBOutlet weak var albumNameBtn : KGHighLightedButton!
    @IBOutlet weak var notFound : UILabel!
    
    @IBOutlet weak var listBtn : UIButton!
    @IBOutlet weak var gridBtn : UIButton!
    @IBOutlet weak var cartBtn : UIButton!
    
    @IBOutlet weak var albumTableContent : UIView!
    @IBOutlet weak var albumTableView : UITableView!
    
    var picsArray : [NSDictionary] = []
    var dictArray : [NSDictionary] = []
    var userDictCart : NSDictionary = [:]
    var indexArray : [Int] = []
    
    var selectedCell : FPUserPhotoTabelCell?
    var selectedGridCell : FPUserGridCollCell?
    var selectDict : NSDictionary = [:]
    var userInfoDict : NSDictionary = [:]
    var selectIndex : Int = 0
    
    var album_Id : String = ""
    var album_name : String = ""
    var formString : String = "list"
    var fromDelegate : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        btmImageView.tintColor = ColorCode.FPicThemeColor
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        for idx in 0...btnCollection.count-1
        {
            btnCollection[idx].alpha = 0.15
            imgCollection[idx].tintColor = ColorCode.FPicThemeColor
            btnCollection[idx].addTarget(self, action: #selector(topButtonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        notFound.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_photos")

        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&album_id=\(self.album_Id)"
            self.callService(urlstr: Api.FP_Album_Pics, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
        
        if (Cart_Count > 0)
        {
            cartCountLbl.isHidden = false
            cartCountLbl.text = "\(Cart_Count)"
        }
        else {
            cartCountLbl.isHidden = true
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        albumTableView.endEditing(true)
    }
    
    func setDataOnLoad(passDict : NSDictionary)
    {
        if ((passDict["profile_pic"] as! String).contains("graph.facebook.com") == true) || ((passDict["profile_pic"] as! String).contains("googleusercontent") == true)
        {
            profileImgVw.sd_setImage(with: URL(string: "\(passDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        else {
            profileImgVw.sd_setImage(with: URL(string: "\(Api.Profile_BaseURL)\(passDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        nameLabel.text = "\(passDict["name"] as! String)(@\(passDict["nickname"] as! String))"
        placeLabel.text = passDict["address"] as? String
        albumLabel.text = "\(passDict["album_count"] as! NSNumber)"
        albumNameBtn.setTitle(album_name, for: .normal)
        
        let arr1 = (passDict["pic_count"] as! String).split(separator: ",")
        let piccount = arr1[0].split(separator: "=")
        let like = arr1[1].split(separator: "=")
        
        picsLabel.text = "\(piccount[1])"
        likeLabel.text = "\(like[1])"
        
        if let _ = passDict["rating"] as? NSNumber
        {
            ratingLabel.text = String(format: "%.1f", Double.init("\(passDict["rating"] as! NSNumber)")!)
        }
    }
    
    @IBAction func viewInList(_ sender: Any)
    {
        listBtn.setImage(#imageLiteral(resourceName: "FP_listview"), for: .normal)
        gridBtn.setImage(#imageLiteral(resourceName: "FP_gridview"), for: .normal)
        formString = "list"
        cartBtn.isHidden = true
        
        UIView.transition(with: albumTableView, duration: 1.0, options: .transitionFlipFromLeft, animations: {self.albumTableView.reloadData()}, completion: nil)
    }
    
    @IBAction func viewInGrid(_ sender: Any)
    {
        listBtn.setImage(#imageLiteral(resourceName: "FP_WhiteListVw"), for: .normal)
        gridBtn.setImage(#imageLiteral(resourceName: "FP_FillGridVw"), for: .normal)
        formString = "grid"
        cartBtn.isHidden = false
        
        UIView.transition(with: albumTableView, duration: 1.0, options: .transitionFlipFromRight, animations: {self.albumTableView.reloadData()}, completion: nil)
    }
    
    @IBAction func addToCart(_ sender: Any)
    {
        if let _ = userDefault.value(forKey: "buy_photos")
        {
            let arr = userDefault.value(forKey: "buy_photos") as! [NSDictionary]
            dictArray = dictArray + arr
            
            let userdict = userDefault.value(forKey: "user_dict") as! NSDictionary
            if (userdict != [:])
            {
                userDictCart = userdict
            }else {
                userDictCart = userInfoDict
            }
            userDefault.setValue(userDictCart, forKey: "user_dict")
        }
        else {
            userDictCart = userInfoDict
            userDefault.setValue(userDictCart, forKey: "user_dict")
        }
        Cart_Count = dictArray.count
        userDefault.setValue(dictArray, forKey: "buy_photos")
        userDefault.synchronize()
        viewDidAppear(true)
        indexArray = []
        dictArray = []
        self.albumTableView.reloadData()
        
        ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_added_to_cart"), controller: self, fontFamliy: UIFont(name: "SegoeScript-Bold", size: 15)!,  bgcolor: UIColor.white, txtcolor: ColorCode.FPicThemeColor, wd: 150, ht: 40)
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (fromDelegate == false) {
            let albumvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPUserAlbumVC") as! FPUserAlbumVC
            albumvc.passId = "\(userInfoDict["id"] as! NSNumber)"
            navigationController?.pushViewController(albumvc, animated: false)
        }
        else {
            let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPicSearchVC") as! FPicSearchVC
            navigationController?.pushViewController(homevc, animated: false)
        }
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("user photo result = \(result)")
                if (check == "get")
                {
                    self.picsArray = result["data"] as! [NSDictionary]
                    self.userInfoDict = result["user_info"] as! NSDictionary
                    self.setDataOnLoad(passDict: self.userInfoDict)
                    self.albumTableView.isHidden = false
                    self.albumTableView.reloadData()
                    if (self.picsArray.count == 0) {
                        self.notFound.isHidden = false
                    }
                    else {
                        self.notFound.isHidden = true
                    }
                }
                else if (check == "like") {
                    let editdict = NSMutableDictionary.init(dictionary: self.selectDict)
                    if (editdict["isLiked"] as! NSNumber == 0)
                    {
                        editdict["isLiked"] = 1
                        if let _ = self.selectedCell
                        {
                            self.selectedCell!.likePhotoBtn.setImage(#imageLiteral(resourceName: "FP_redheart"), for: .normal)
                        }
                        else {
                            self.selectedGridCell!.likeBtn.setImage(#imageLiteral(resourceName: "FP_redheart"), for: .normal)
                        }
                        var count = Int.init(exactly: editdict["likes"] as! NSNumber)!
                        count += 1
                        editdict["likes"] = NSNumber.init(value: count)
                        self.selectedCell?.likesLbl.text = "\(count)"
                    }
                    else {
                        editdict["isLiked"] = 0
                        if let _ = self.selectedCell
                        {
                            self.selectedCell!.likePhotoBtn.setImage(#imageLiteral(resourceName: "FP_grayheart"), for: .normal)
                        }
                        else {
                            self.selectedGridCell!.likeBtn.setImage(#imageLiteral(resourceName: "FP_grayheart"), for: .normal)
                        }
                        var count = Int.init(exactly: editdict["likes"] as! NSNumber)!
                        count -= 1
                        editdict["likes"] = NSNumber.init(value: count)
                        self.selectedCell?.likesLbl.text = "\(count)"
                    }
                    self.picsArray[self.selectIndex] = editdict
                }
                else {}
                customLoader.hideIndicator()
            }
        }
    }
    
    //MARK : Add bottom layer to TextFields
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:1.5)
        }
        textF.layer.addSublayer(border)
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fp")
    }
    
    // Top View Button Actions
    @objc func topButtonAction(_ sender : UIButton)
    {
        flypicUserTopActions(tagValue: sender.tag, controller: self)
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flypicUserBtmAction(tagValue: sender.tag, controller: self)
    }
    
    // MARK : Download image locally
    func saveImage(image: UIImage) -> Bool {
       
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            customLoader.hideIndicator()
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            customLoader.hideIndicator()
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent("fileName.png")!)
            return true
        } catch {
            print(error.localizedDescription)
            customLoader.hideIndicator()
            return false
        }
    }
}

extension FPUserPhotosVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if (formString == "list"){
            return picsArray.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (formString == "list") {
            let cell = albumTableView.dequeueReusableCell(withIdentifier: "userphotocell") as! FPUserPhotoTabelCell
            
            let dict = picsArray[indexPath.section]
            
            let imgUrl = URL(string: "\(Api.FP_Pic_Url)\(dict["pic_name"] as! String)")
            cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            
            cell.albumNameLbl.text = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd/MM/yyyy")
            cell.dateLbl.text = dict["location"] as? String
            cell.likesLbl.text = "\(dict["likes"] as! NSNumber)"
            
            if (dict["isLiked"] as! NSNumber == 0){
                cell.likePhotoBtn.setImage(#imageLiteral(resourceName: "FP_grayheart"), for: .normal)
            } else {
                cell.likePhotoBtn.setImage(#imageLiteral(resourceName: "FP_redheart"), for: .normal)
            }
            
            if let _ = dict["price"] as? NSNumber
            {
                cell.priceLbl.text = "\(dict["price"] as! NSNumber) \(Current_Currency)"
            }
            else {
                if (dict["price"] as! String == "") || (dict["price"] as! String == "free") {
                    cell.priceLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_free")
                }
                else {
                    cell.priceLbl.text = "\(dict["price"] as! String) \(Current_Currency)"
                }
            }
            
            cell.downloadBtn.addTarget(self, action: #selector(addToCartFromList(_:)), for: .touchUpInside)
            cell.addCartBtn.addTarget(self, action: #selector(addToCartFromList(_:)), for: .touchUpInside)
            cell.likePhotoBtn.addTarget(self, action: #selector(likePhoto(_:)), for: .touchUpInside)
            
            return cell
        }
        else {
            let cell = albumTableView.dequeueReusableCell(withIdentifier: "usercollectioncell") as! FPUserCollectionCell
            
            cell.picCollectionVw.dataSource = self
            cell.picCollectionVw.delegate = self
            cell.picCollectionVw.reloadData()
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (formString == "list"){
            return 350
        }
        else {
            if (picsArray.count <= 3){
                return 120
            } else {
                let cnt = (picsArray.count/3) + (picsArray.count % 3)
                return (CGFloat(15 + ((cnt) * 106)))
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: albumTableView.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }

    @objc func likePhoto(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        let dict = picsArray[idxpath.section]
        selectDict = dict
        selectIndex = idxpath.section
        selectedCell = albumTableView.cellForRow(at: idxpath) as? FPUserPhotoTabelCell
        
        customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&pic_id=\(dict["id"] as! NSNumber)"
        self.callService(urlstr: Api.FP_Like_Pic, parameters: params, check: "like")
    }
    
    @objc func addToCartFromList(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: albumTableView)
        let idxpath = albumTableView.indexPathForRow(at: point)!
        let dict = picsArray[idxpath.section]
        
        let mutableDict = NSMutableDictionary.init(dictionary: dict)
        mutableDict.setValue(album_name, forKey: "album_name")
        mutableDict.setValue(album_Id, forKey: "album_id")
        mutableDict.setValue(userInfoDict["address"] as! String, forKey: "address")
        mutableDict.setValue(userInfoDict["name"] as! String, forKey: "name")
        mutableDict.setValue("\(userInfoDict["id"] as! NSNumber)", forKey: "user_id")
        
        var set : Bool = true
        if let _ = userDefault.value(forKey: "buy_photos")
        {
            var arr = userDefault.value(forKey: "buy_photos") as! [NSDictionary]
            if (arr.contains(mutableDict) == false)
            {
                let userdict = userDefault.value(forKey: "user_dict") as! NSDictionary
                
                if (userdict != [:])
                {
                    if (userdict["id"] as! NSNumber == userInfoDict["id"] as! NSNumber)
                    {
                        arr.append(mutableDict)
                        dictArray = arr
                        
                        userDictCart = userdict
                        userDefault.setValue(userDictCart, forKey: "user_dict")
                    }
                    else {
                        set = false
                        ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "cannot_select_another_photographer"), controller: self)
                    }
                }
                else {
                    arr.append(mutableDict)
                    dictArray = arr
                    
                    userDictCart = userInfoDict
                    userDefault.setValue(userDictCart, forKey: "user_dict")
                }
            }
            else {
                set = false
                ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "already_in_cart"), controller: self)
            }
        }
        else {
            var arr : [NSDictionary] = []
            arr.append(mutableDict)
            dictArray = arr
            userDictCart = userInfoDict
            userDefault.setValue(userDictCart, forKey: "user_dict")
        }
        if (set == true)
        {
            Cart_Count = dictArray.count
            userDefault.setValue(dictArray, forKey: "buy_photos")
            userDefault.synchronize()
            dictArray = []
            viewDidAppear(true)
            
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "fp_added_to_cart"), controller: self, fontFamliy: UIFont(name: "SegoeScript-Bold", size: 15)!,  bgcolor: UIColor.white, txtcolor: ColorCode.FPicThemeColor, wd: 150, ht: 40)
        }
    }
}

class FPUserPhotoTabelCell: UITableViewCell {
    
    @IBOutlet weak var picImageVw : UIImageView!
    @IBOutlet weak var albumNameLbl : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var likesLbl : UILabel!
    @IBOutlet weak var priceLbl : UILabel!
    @IBOutlet weak var likePhotoBtn : UIButton!
   
    @IBOutlet weak var downloadBtn : KGHighLightedButton!
    @IBOutlet weak var addCartBtn : UIButton!
}

class FPUserCollectionCell: UITableViewCell {
    
    @IBOutlet weak var picCollectionVw : UICollectionView!
}

/// MAKR : CollectionView Methods
extension FPUserPhotosVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "usergridcollcell", for: indexPath) as! FPUserGridCollCell
        
        let dict = picsArray[indexPath.item]
        let imgUrl = URL(string: "\(Api.FP_Pic_Url)\(dict["pic_name"] as! String)")
        cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "Placeholder"))
       
        if let _ = dict["price"] as? NSNumber
        {
            cell.priceLbl.text = "\(dict["price"] as! NSNumber) \(Current_Currency)"
        }
        else {
            if (dict["price"] as! String == "") {
                cell.priceLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "ff_free")
            }
            else {
                cell.priceLbl.text = dict["price"] as? String
            }
        }
        
        if (dict["isLiked"] as! NSNumber == 0){
            cell.likeBtn.setImage(#imageLiteral(resourceName: "FP_grayheart"), for: .normal)
        } else {
            cell.likeBtn.setImage(#imageLiteral(resourceName: "FP_redheart"), for: .normal)
        }
        
        if (indexArray.count > 0){
            if (indexArray.contains(indexPath.item)){
                cell.checkBtn.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
            }
            else {
               cell.checkBtn.setImage(nil, for: .normal)
            }
        }
        else {
            cell.checkBtn.setImage(nil, for: .normal)
        }
        
        cell.priceLbl.layer.cornerRadius = 4
        cell.priceLbl.layer.masksToBounds = true
        
        cell.likeBtn.addTarget(self, action: #selector(likePhotoOnColl(_:)), for: .touchUpInside)
        cell.checkBtn.addTarget(self, action: #selector(checkImage(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            return CGSize(width: 100, height: 100)
        }
        else if (self.view.frame.width == 375)
        {
            return CGSize(width: 110, height: 110)
        }
        else if(self.view.frame.width == 414) {
            return CGSize(width: 120, height: 120)
        }else {
            return CGSize(width: 130, height: 130)
        }
    }
    
    @objc func likePhotoOnColl(_ sender : UIButton)
    {
        let tablecell = albumTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! FPUserCollectionCell
        
        let point = sender.convert(CGPoint.zero, to: tablecell.picCollectionVw)
        let idxpath = tablecell.picCollectionVw.indexPathForItem(at: point)!
    
        let dict = picsArray[idxpath.item]
        selectDict = dict
        selectIndex = idxpath.item
        selectedGridCell = tablecell.picCollectionVw.cellForItem(at: idxpath) as? FPUserGridCollCell
        
        customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&pic_id=\(dict["id"] as! NSNumber)"
        self.callService(urlstr: Api.FP_Like_Pic, parameters: params, check: "like")
    }
    
    @objc func checkImage(_ sender : UIButton)
    {
        let tablecell = albumTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! FPUserCollectionCell
        
        let point = sender.convert(CGPoint.zero, to: tablecell.picCollectionVw)
        let idxpath = tablecell.picCollectionVw.indexPathForItem(at: point)!
        
        let dict = picsArray[idxpath.item]
        
        let mutableDict = NSMutableDictionary.init(dictionary: dict)
        mutableDict.setValue(album_name, forKey: "album_name")
        mutableDict.setValue(album_Id, forKey: "album_id")
        mutableDict.setValue(userInfoDict["address"] as! String, forKey: "address")
        mutableDict.setValue(userInfoDict["name"] as! String, forKey: "name")
        mutableDict.setValue("\(userInfoDict["id"] as! NSNumber)", forKey: "user_id")
        
        if let _ = userDefault.value(forKey: "buy_photos")
        {
            let arr = userDefault.value(forKey: "buy_photos") as! [NSDictionary]
            if (arr.contains(mutableDict) == false)
            {
                let userdict = userDefault.value(forKey: "user_dict") as! NSDictionary
                if (userdict != [:])
                {
                    if (userdict["id"] as! NSNumber == userInfoDict["id"] as! NSNumber)
                    {
                        if (sender.image(for: .normal) == #imageLiteral(resourceName: "FP_imgchecked")) {
                            sender.setImage(nil, for: .normal)
                            if (indexArray.contains(idxpath.item))
                            {
                                let arr = NSArray.init(array: indexArray)
                                let index = arr.index(of: idxpath.item)
                                indexArray.remove(at: index)
                                dictArray.remove(at: index)
                            }
                        }
                        else {
                            sender.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
                            indexArray.append(idxpath.item)
                            dictArray.append(mutableDict)
                        }
                    }
                    else {
                        ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "cannot_select_another_photographer"), controller: self)
                    }
                }
                else {
                    if (sender.image(for: .normal) == #imageLiteral(resourceName: "FP_imgchecked")) {
                        sender.setImage(nil, for: .normal)
                        if (indexArray.contains(idxpath.item))
                        {
                            let arr = NSArray.init(array: indexArray)
                            let index = arr.index(of: idxpath.item)
                            indexArray.remove(at: index)
                            dictArray.remove(at: index)
                        }
                    }
                    else {
                        sender.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
                        indexArray.append(idxpath.item)
                        dictArray.append(mutableDict)
                    }
                }
            }
            else {
                ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "already_in_cart"), controller: self)
            }
        }
        else {
            if (sender.image(for: .normal) == #imageLiteral(resourceName: "FP_imgchecked")) {
                sender.setImage(nil, for: .normal)
                if (indexArray.contains(idxpath.item))
                {
                    let arr = NSArray.init(array: indexArray)
                    let index = arr.index(of: idxpath.item)
                    indexArray.remove(at: index)
                    dictArray.remove(at: index)
                }
            }
            else {
                sender.setImage(#imageLiteral(resourceName: "FP_imgchecked"), for: .normal)
                indexArray.append(idxpath.item)
                dictArray.append(mutableDict)
            }
        }
    }
}

class FPUserGridCollCell: UICollectionViewCell {
    
    @IBOutlet weak var picImageVw : UIImageView!
    @IBOutlet weak var priceLbl : UILabel!
    @IBOutlet weak var checkBtn : UIButton!
    @IBOutlet weak var likeBtn : UIButton!
}
