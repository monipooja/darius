//
//  FPOrderPicsVC.swift
//  Darious
//
//  Created by Apple on 24/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPOrderPicsVC: UIViewController {

    @IBOutlet weak var picsTableVw : UITableView!
    
    @IBOutlet weak var topTitle : UILabel!
    
    var dataArray : [NSDictionary] = []
    
    var order_id : NSNumber = 0
    var screenName : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
    
        topTitle.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_my_sells")
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FPicThemeColor.toHexString(), controller: self.view)
            if (self.screenName == "user")
            {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&order_id=\(self.order_id)"
                self.callService(urlstr: Api.FP_Order_Pics, parameters: params, check: "")
            }
            else {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&order_id=\(self.order_id)"
                self.callService(urlstr: Api.FP_Photo_Order_Details, parameters: params, check: "")
            }
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
        navigationController?.pushViewController(homevc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: false)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("\(self.screenName) my pics result = \(result)")
                let data = result["data"] as! [NSDictionary]
                if (data.count > 0) {
                    self.dataArray = result["data"] as! [NSDictionary]
                    self.picsTableVw.reloadData()
                    self.picsTableVw.isHidden = false
                } else {
                    self.picsTableVw.isHidden = true
                }
                customLoader.hideIndicator()
            }
        }
    }
}

extension FPOrderPicsVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = picsTableVw.dequeueReusableCell(withIdentifier: "orderpicscell") as! FPOrderPicsCell
        
        let dict = dataArray[indexPath.section]

        if (screenName == "user") {
            let imgUrl = URL(string: "\(Api.FP_Original_Url)\(dict["original_pic"] as! String)")
            cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        }
        else {
            let imgUrl = URL(string: "\(Api.FP_Original_Url)\(dict["original_pic"] as! String)")
            cell.picImageVw.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        }
        cell.nameLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_album_name")) - \(dict["name"] as! String)"
        
        if (dict["price"] as! String == "free") {
            cell.priceLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_price")) : \(dict["price"] as! String)"
        }
        else {
            cell.priceLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fp_price")) : \(dict["price"] as! String) \(Current_Currency)"
        }
        cell.dateLbl.text = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd/MM/yyyy")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: picsTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
}

class FPOrderPicsCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    
    @IBOutlet weak var picImageVw : UIImageView!
}
