//
//  FPHomeVC.swift
//  Darious
//
//  Created by Apple on 20/12/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FPHomeVC: UIViewController {

    @IBOutlet weak var usersLabel : UILabel!
    @IBOutlet weak var photographerLabel : UILabel!
    
    @IBOutlet weak var firstBtnHt : NSLayoutConstraint!
    @IBOutlet weak var firstBtnWd : NSLayoutConstraint!
    @IBOutlet weak var secondBtnHt : NSLayoutConstraint!
    @IBOutlet weak var secondBtnWd : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FPicThemeColor
        
        if (self.view.frame.width == 320) && (self.view.frame.height == 568)
        {
            firstBtnHt.constant = 125
            firstBtnWd.constant = 125
            secondBtnHt.constant = 125
            secondBtnWd.constant = 125
        }
        else if (self.view.frame.width == 375) && (self.view.frame.height == 667)
        {
            firstBtnHt.constant = 150
            firstBtnWd.constant = 150
            secondBtnHt.constant = 150
            secondBtnWd.constant = 150
        }else {
            firstBtnHt.constant = 175
            firstBtnWd.constant = 175
            secondBtnHt.constant = 175
            secondBtnWd.constant = 175
        }
        
        usersLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_users")
        photographerLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fp_photographer")
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func goToUsers(_ sender: Any)
    {
        let searchvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPicSearchVC") as! FPicSearchVC
        navigationController?.pushViewController(searchvc, animated: false)
    }
    
    @IBAction func goToPhotographer(_ sender: Any)
    {
        let nextvc = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPMyProfileVC") as! FPMyProfileVC
        navigationController?.pushViewController(nextvc, animated: false)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
    }
    
}
