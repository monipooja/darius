//
//  FBRankingVC.swift
//  Darious
//
//  Created by Apple on 08/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import DropDown
import FirebaseDynamicLinks

class FBRankingVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    @IBOutlet weak var countryCenterX : NSLayoutConstraint!
    
    @IBOutlet var ddImgCollection: [UIImageView]!
    
    @IBOutlet weak var topFlagImg : UIImageView!
    @IBOutlet weak var flagImg : UIImageView!
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var tableContentVw : UIView!
    
    @IBOutlet weak var countryLabel : UILabel!
    @IBOutlet weak var sectionLabel : UILabel!
    @IBOutlet weak var yearLabel : UILabel!
    @IBOutlet weak var seasonLabel : UILabel!
    @IBOutlet weak var socialLabel : UILabel!
    @IBOutlet weak var areaLabel : UILabel!
    
    @IBOutlet weak var areaView : CustomUIView!
    @IBOutlet weak var yearView : CustomUIView!
    @IBOutlet weak var seasonView : CustomUIView!
    
    @IBOutlet weak var socialIcon : UIImageView!
    @IBOutlet weak var socialImg : UIImageView!
    @IBOutlet weak var socialImgWd : NSLayoutConstraint!
    
    @IBOutlet weak var socialView : CustomUIView!
    @IBOutlet weak var sectionView : CustomUIView!
    @IBOutlet weak var firstBtn : UIButton!
    @IBOutlet weak var secondBtn : UIButton!
    
    @IBOutlet weak var sendBtn : KGHighLightedButton!
    
    let dropDown = DropDown()
    let searchBar : UISearchBar = UISearchBar.init()
    
    var yearIndex : Int = 0
    var passName : String = ""
    var passid : String = ""
    var flagiso : String = ""
    var votedate : String = ""
    
    var nameArray : [String] = []
    var isoArray : [String] = []
    var idArray : [String] = []
    var dataFiltered: [String] = []
    var filterIsoArray : [String] = []
    var filterIdArray : [String] = []
    var yearArray : [String] = []
    var seasonArray : [[String]] = []
    var areaNameArray : [String] = []
    var areaIdArray : [String] = []
    
    var rankingDict : NSMutableDictionary = ["country":"", "c_id":"", "c_flag":"", "social":"", "list_type":"", "section":"", "year":"", "season":"", "area" :"", "area_id":""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FBThemeColor
        
        btmImgVw.tintColor = ColorCode.FBThemeColor
        if (passName != "")
        {
            FB_SelectedCountry = passName
            FB_CountryID = passid
            FB_Flag_iso = flagiso
            Vote_end_date = votedate.replacingOccurrences(of: "2", with: ", 2")
        }
        topFlagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FB_Flag_iso).png"))
        
        for img in ddImgCollection
        {
            img.tintColor = ColorCode.FBThemeColor
        }
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        rankingDict["social"] = SocialMedia.insta.rawValue
        
        let gesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(touchGesture(_:)))
        gesture.numberOfTapsRequired = 1
        tableContentVw.addGestureRecognizer(gesture)
        
        searchBarIntegration()
        setBottomView()
        SetLanguage()
        
        DispatchQueue.main.async {
            let params2 = "api_key=\(api_key)"
            self.callService(urlstr: Api.STDCode_URL, parameters: params2, check: "country")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    @objc func touchGesture(_ gesture : UIGestureRecognizer)
    {
        self.view.endEditing(true)
        self.searchBar.isHidden = true
    }
    
    // MARK : Search bar create
    func searchBarIntegration()
    {
        searchBar.frame = CGRect(x: 20, y: 40, width: self.view.frame.width-40, height:50)
        searchBar.delegate = self
        self.view.addSubview(searchBar)
        searchBar.isHidden = true
    }
    
    func SetLanguage()
    {
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_ranking")
        countryLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_country")
        sectionLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_section")
        yearLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_year")
        seasonLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_season")
        socialLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_network")
        areaLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_area")
        
        sendBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "send"), for: .normal)
    }
    
    @IBAction func chooseCountry(_ sender: Any)
    {
        areaLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_area")
        areaLabel.textColor = UIColor.init(hexString: "C9C8C8")
        areaNameArray = []
        areaIdArray = []
        rankingDict["area"] = ""
        rankingDict["area_id"] = ""
        if (nameArray == [])
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FBThemeColor.toHexString(), controller: self.view)
            let params2 = "api_key=\(api_key)"
            self.callService(urlstr: Api.STDCode_URL, parameters: params2, check: "country_dd")
        }
        else {
            showDropDown()
            socialView.isHidden = true
            sectionView.isHidden = true
        }
    }
    
    @IBAction func selectArea(_ sender : Any)
    {
        if (rankingDict["c_id"] as! String == "")
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_county"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FBThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&country_id=\(rankingDict["c_id"] as! String)"
            self.callService(urlstr: Api.FB_AreaList_URL, parameters: params, check: "area")
        }
    }
    
    @IBAction func chooseSocial(_ sender: Any)
    {
        socialView.isHidden = false
        sectionView.isHidden = true
    }
    @IBAction func hideSocial(_ sender: Any)
    {
        socialView.isHidden = true
    }
    
    @IBAction func selectInstagram(_ sender: Any)
    {
        socialLabel.isHidden = true
        socialIcon.image = #imageLiteral(resourceName: "FB_smalInsta")
        socialIcon.isHidden = false
        socialImg.image = #imageLiteral(resourceName: "FB_InstaIcon")
        socialImg.isHidden = false
        socialImgWd.constant = 75
        
        firstBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fb_best_post"), for: .normal)
        secondBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fb_best_instagramer"), for: .normal)
        socialView.isHidden = true
        
        rankingDict["social"] = SocialMedia.insta.rawValue
    }
    
    @IBAction func selectYoutube(_ sender: Any)
    {
        socialLabel.isHidden = true
        socialIcon.image = #imageLiteral(resourceName: "FBest_youtube")
        socialIcon.isHidden = false
        socialImg.image = #imageLiteral(resourceName: "Logo_of_YouTube")
        socialImg.isHidden = false
        socialImgWd.constant = 60
        
        firstBtn.setTitle("Best Video", for: .normal)
        secondBtn.setTitle("Best Youtuber", for: .normal)
        socialView.isHidden = true
        
        rankingDict["social"] = SocialMedia.youtube.rawValue
    }
    
    @IBAction func selectTwitter(_ sender: Any)
    {
        socialLabel.isHidden = true
        socialIcon.image = #imageLiteral(resourceName: "FBest_twitter")
        socialIcon.isHidden = false
        socialImg.image = #imageLiteral(resourceName: "logo_twitter")
        socialImg.isHidden = false
        socialImgWd.constant = 90
        
        firstBtn.setTitle("Best Tweet", for: .normal)
        secondBtn.setTitle("Best Twitter", for: .normal)
        socialView.isHidden = true
        
        rankingDict["social"] = SocialMedia.twitter.rawValue
    }
    
    @IBAction func chooseSection(_ sender: Any)
    {
        sectionView.isHidden = false
    }
    @IBAction func hideSection(_ sender: Any)
    {
        sectionView.isHidden = true
    }
    
    @IBAction func chooseFirstSection(_ sender: Any)
    {
        sectionView.isHidden = true
        sectionLabel.text = firstBtn.titleLabel?.text
        sectionLabel.textColor = ColorCode.FBThemeColor
        sectionLabel.font = UIFont(name: "CopperplateGothic-Bold", size: 13)
        
        rankingDict["list_type"] = ListType.post.rawValue
        rankingDict["section"] = firstBtn.titleLabel?.text
    }
    
    @IBAction func chooseSecondSection(_ sender: Any)
    {
        sectionView.isHidden = true
        sectionLabel.text = secondBtn.titleLabel?.text
        sectionLabel.textColor = ColorCode.FBThemeColor
        sectionLabel.font = UIFont(name: "CopperplateGothic-Bold", size: 13)
        
        rankingDict["list_type"] = ListType.people.rawValue
        rankingDict["section"] = secondBtn.titleLabel?.text
    }
    
    @IBAction func chooseYear(_ sender: Any)
    {
        yearArray = []
        seasonArray = []
        customLoader.showActivityIndicator(showColor: ColorCode.FBThemeColor.toHexString(), controller: self.view)
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&app_name=flybest"
        self.callService(urlstr: Api.FB_Session_URL, parameters: params, check: "session")
    }
    
    @IBAction func chooseSeason(_ sender: Any)
    {
        if (yearLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_year")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_year"), controller: self)
        }
        else {
            showSecondDropDown(anchorVw: seasonView, passarr: seasonArray[yearIndex])
        }
    }
    
    @IBAction func onSend(_ sender: Any)
    {
        if (countryLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_country")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_county"), controller: self)
        }
        else if (areaLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_area")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_area"), controller: self)
        }
        else if (socialLabel.isHidden == false) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_social"), controller: self)
        }
        else if (sectionLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_section")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_section"), controller: self)
        }
        else if (yearLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_year")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_year"), controller: self)
        }
        else if (seasonLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_season")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_season"), controller: self)
        }
        else {
            let rankinglist = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBRankingListVC") as! FBRankingListVC
            rankinglist.passDict = rankingDict
            present(rankinglist, animated: false, completion: nil)
        }
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flybest&screen_name=fbranking&country_name=\(FB_SelectedCountry)&flag=\(FB_Flag_iso)&type=&list_type=&country=&province=&vote_date=\(Vote_end_date.replacingOccurrences(of: ", ", with: ""))") else { return }
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (passName == "")
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    func showDropDown()
    {
        searchBar.isHidden = false
        searchBar.text = ""
        
        dropDown.anchorView = searchBar
        dropDown.dataSource = nameArray
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            let codestr = (self.isoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
            cell.codeLabel.text = ""
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            self.countryLabel.text = self.nameArray[index]
            
            let codestr = (self.isoArray[index]).lowercased()
            self.flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
            
            self.rankingDict["country"] = self.nameArray[index]
            self.rankingDict["c_id"] = self.idArray[index]
            self.rankingDict["c_flag"] = "\(Api.Flag_URL)\(codestr).png"
            self.countryCenterX.constant = 15
            
            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
        }
        dropDown.width = searchBar.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
        
        self.countryLabel.textColor = ColorCode.FBThemeColor
        self.countryLabel.font = UIFont(name: "CopperplateGothic-Bold", size: 13)
        searchBar.becomeFirstResponder()
    }
    
    func showSecondDropDown(anchorVw : CustomUIView, passarr : [String])
    {
        dropDown.anchorView = anchorVw
        dropDown.dataSource = passarr
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            cell.flagImage.image = nil
            cell.codeLabel.text = ""
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            if (anchorVw == self.yearView) {
                self.yearLabel.text = self.yearArray[index]
                self.yearIndex = index
                self.rankingDict["year"] = self.yearArray[index]
                self.yearLabel.textColor = ColorCode.FBThemeColor
                self.yearLabel.font = UIFont(name: "CopperplateGothic-Bold", size: 13)
            }
            else if (anchorVw == self.seasonView) {
                let arr = self.seasonArray[self.yearIndex]
                self.seasonLabel.text = arr[index]
                self.rankingDict["season"] = arr[index]
                self.seasonLabel.textColor = ColorCode.FBThemeColor
                self.seasonLabel.font = UIFont(name: "CopperplateGothic-Bold", size: 13)
            }
            else {
                self.areaLabel.text = passarr[index]
                self.rankingDict["area"] = passarr[index]
                self.rankingDict["area_id"] = self.areaIdArray[index]
                self.areaLabel.textColor = ColorCode.FBThemeColor
                self.areaLabel.font = UIFont(name: "CopperplateGothic-Bold", size: 13)
            }
        }
        dropDown.width = anchorVw.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    // MARK : Service call and fetching data
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("ranking \(check) = \(result)")
                if (check == "country") || (check == "country_dd")
                {
                    let data = result["data"] as! [NSDictionary]
                    self.nameArray = []
                    self.isoArray = []
                    self.idArray = []
                    for dict in data
                    {
                        self.nameArray.append(dict["nicename"] as! String)
                        self.isoArray.append(dict["iso"] as! String)
                        self.idArray.append("\(dict["id"] as! NSNumber)")
                        
                        if (check == "country") {
                            if ("\(dict["id"] as! NSNumber)" == FS_CountryID)
                            {
                                self.countryLabel.text = dict["nicename"] as? String
                                self.countryLabel.textColor = ColorCode.FBThemeColor
                                self.rankingDict["country"] = dict["nicename"] as! String
                                self.rankingDict["c_id"] = "\(dict["id"] as! NSNumber)"
                                self.countryCenterX.constant = 15
                                
                                let codestr = (dict["iso"] as! String).lowercased()
                                self.flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
                                self.rankingDict["c_flag"] = "\(Api.Flag_URL)\(codestr).png"
                            }
                        }
                    }
                    if (check == "country_dd") {
                        self.showDropDown()
                    }
                    customLoader.hideIndicator()
                }
                else if (check == "area") {
                    let dataArr = result["data"] as! [NSDictionary]
                    for dict in dataArr
                    {
                        self.areaNameArray.append(dict["name"] as! String)
                        self.areaIdArray.append("\(dict["id"] as! NSNumber)")
                    }
                    customLoader.hideIndicator()
                    self.showSecondDropDown(anchorVw: self.areaView, passarr: self.areaNameArray)
                }
                else {
                   // print("ranking \(check) = \(result)")
                    let dict = result["data"] as! NSDictionary
                    for (key,value) in dict
                    {
                        if ((key as! String) != "active_session") {
                            self.yearArray.append(key as! String)
                            var arr : [String] = []
                            for str in (value as! [String]) {
                                arr.append(ApiResponse.getLanguageFromUserDefaults(inputString: "\(str)_full"))
                            }
                            self.seasonArray.append(arr)
                        }
                    }
                    customLoader.hideIndicator()
                    self.showSecondDropDown(anchorVw: self.yearView, passarr: self.yearArray)
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fb")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flybestBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}


extension FBRankingVC : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dataFiltered = searchText.isEmpty ? nameArray : nameArray.filter({ (dat) -> Bool in
            dat.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        dropDown.dataSource = dataFiltered
        filterIsoArray = []
        filterIdArray = []
        
        for str in dataFiltered {
            var i : Int = 0
            for str1 in nameArray {
                if (str == str1) {
                    filterIsoArray.append(isoArray[i])
                    filterIdArray.append(idArray[i])
                    break
                }
                i += 1
            }
        }
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            let codestr = (self.filterIsoArray[index]).lowercased()
            cell.flagImage.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png"))
            cell.codeLabel.text = ""
        }
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
           
            self.countryLabel.text = self.dataFiltered[index]
            let codestr = (self.filterIsoArray[index]).lowercased()
            self.flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(codestr).png")!)
            
            self.rankingDict["country"] = self.dataFiltered[index]
            self.rankingDict["c_id"] = self.filterIdArray[index]
            self.rankingDict["c_flag"] = "\(Api.Flag_URL)\(codestr).png"
            self.countryCenterX.constant = 12
            
            self.searchBar.isHidden = true
            self.searchBar.resignFirstResponder()
        }
        
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        for ob: UIView in ((searchBar.subviews[0] )).subviews {
            if let z = ob as? UIButton {
                let btn: UIButton = z
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        dataFiltered = nameArray
        filterIsoArray = isoArray
        filterIdArray = idArray
        searchBar.isHidden = true
        dropDown.hide()
    }
}
