//
//  FBMyVotesVC.swift
//  Darious
//
//  Created by Apple on 08/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks

class FBMyVotesVC: UIViewController {
    
    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var flagImg : UIImageView!
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var nodataLabel : UILabel!
    @IBOutlet weak var votesTableVw : UITableView!
    
    var votesArray : [NSDictionary] = []
    var passName : String = ""
    var passid : String = ""
    var flagiso : String = ""
    var votedate : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FBThemeColor
        
        btmImgVw.tintColor = ColorCode.FBThemeColor
        if (passName != "")
        {
            FB_SelectedCountry = passName
            FB_CountryID = passid
            FB_Flag_iso = flagiso
            Vote_end_date = votedate.replacingOccurrences(of: "2", with: ", 2")
        }
        flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FB_Flag_iso).png"))
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "my_votes")
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&type=\(SocialMedia.insta.rawValue)&list_type=people&country=\(FB_CountryID)"
            self.callService(urlstr: Api.FB_MyVotes_URL, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flybest&screen_name=fbvote&country_name=\(FB_SelectedCountry)&flag=\(FB_Flag_iso)&type=&list_type=&country=&province=&vote_date=\(Vote_end_date.replacingOccurrences(of: ", ", with: ""))") else { return }
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (passName == "")
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    // MARK : Service call and fetching data
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("my votes = \(result)")
                self.votesArray = result["data"] as! [NSDictionary]
                if (self.votesArray.count > 0) {
                    self.votesTableVw.reloadData()
                }
                else {
                    self.nodataLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                    self.nodataLabel.isHidden = false
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fb")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flybestBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FBMyVotesVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return votesArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = votesTableVw.dequeueReusableCell(withIdentifier: "votescell") as! MyvotesTableCell
        
        let dict = votesArray[indexPath.section]
        
        cell.profileImageVw.sd_setImage(with: URL(string: dict["profile_pic"] as! String), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        
        cell.serialNoLabel.text = "\(dict["rank"] as! NSNumber)"
        cell.serialNoLabel.layer.cornerRadius = cell.serialNoLabel.frame.width/2
        cell.serialNoLabel.layer.masksToBounds = true
        
        cell.nameLabel.text = dict["name"] as? String
        cell.usernameLabel.text = dict["best_id"] as? String
        cell.votesLabel.text = "\(dict["votes"] as! NSNumber)"
        
        // Set Medals
        if (dict["rank"] as! NSNumber == 1) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "gold-medal")
        }
        else if (dict["rank"] as! NSNumber == 2) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "FB_silvermedal")
        }
        else if (dict["rank"] as! NSNumber == 3) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "FB_bronzemedal")
        }
        else {
            cell.goldImageVw.image = nil
        }
        
        let session = "\(dict["voting_session"] as! String)".replacingOccurrences(of: "##", with: "/")
        // Set social icon
        if (dict["type"] as! String == SocialMedia.insta.rawValue)
        {
            cell.socialIcon1Imgvw.image = #imageLiteral(resourceName: "FB_smalInsta")
            cell.socialIcon2Imgvw.image = #imageLiteral(resourceName: "FB_smalInsta")
            
            // Set type-year-season
            if (dict["vote_type"] as! String == ListType.post.rawValue)
            {
                cell.thirdLabel.text = "Best Post/\(session)"
            }
            else {
               cell.thirdLabel.text = "Best Instagramer/\(session)"
            }
        }
        else if (dict["type"] as! String == SocialMedia.youtube.rawValue)
        {
            cell.socialIcon1Imgvw.image = #imageLiteral(resourceName: "FBest_youtube")
            cell.socialIcon2Imgvw.image = #imageLiteral(resourceName: "FBest_youtube")
            
            // Set type-year-season
            if (dict["vote_type"] as! String == ListType.post.rawValue)
            {
                cell.thirdLabel.text = "Best Youtube/\(session)"
            }
            else {
                cell.thirdLabel.text = "Best Youtuber/\(session)"
            }
        }
        else
        {
            cell.socialIcon1Imgvw.image = #imageLiteral(resourceName: "FBest_twitter")
            cell.socialIcon2Imgvw.image = #imageLiteral(resourceName: "FBest_twitter")
            
            // Set type-year-season
            if (dict["vote_type"] as! String == ListType.post.rawValue)
            {
                cell.thirdLabel.text = "Best Tweet/\(session)"
            }
            else {
                cell.thirdLabel.text = "Best Twitter/\(session)"
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: votesTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }

}

class MyvotesTableCell : UITableViewCell
{
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var usernameLabel : UILabel!
    @IBOutlet weak var thirdLabel : UILabel!
    @IBOutlet weak var votesLabel : UILabel!
    @IBOutlet weak var serialNoLabel : UILabel!
    
    @IBOutlet weak var socialIcon1Imgvw : UIImageView!
    @IBOutlet weak var socialIcon2Imgvw : UIImageView!
    @IBOutlet weak var goldImageVw : UIImageView!
    @IBOutlet weak var profileImageVw : UIImageView!
}
