//
//  FBAreaVC.swift
//  Darious
//
//  Created by Apple on 04/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import DropDown
import FirebaseDynamicLinks

class FBAreaVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var dropdwonImg : UIImageView!
    @IBOutlet weak var flagImg : UIImageView!
    @IBOutlet weak var countryLabel : UILabel!
    @IBOutlet weak var areaBtn : UIButton!
    @IBOutlet weak var areaLabel : UILabel!
    @IBOutlet weak var sendBtn : KGHighLightedButton!
    
    let dropDown = DropDown()
    var passName : String = ""
    var passid : String = ""
    var flagiso : String = ""
    var votedate : String = ""
    
    var areaNameArray : [String] = []
    var areaIdArray : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FBThemeColor
        
        btmImgVw.tintColor = ColorCode.FBThemeColor
        dropdwonImg.tintColor = ColorCode.FBThemeColor
        if (passName != "")
        {
            FB_SelectedCountry = passName
            FB_CountryID = passid
            FB_Flag_iso = flagiso
            Vote_end_date = votedate.replacingOccurrences(of: "2", with: ", 2")
        }
        countryLabel.text = FB_SelectedCountry
        flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FB_Flag_iso).png"))
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        setBottomView()
        
        areaLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_area")
        sendBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "send"), for: .normal)
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FBThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&country_id=\(FB_CountryID)"
            self.callService(urlstr: Api.FB_AreaList_URL, parameters: params, check: "area")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func onSend(_ sender: Any)
    {
        if (areaLabel.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fb_choose_area")) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_select_area"), controller: self)
        }
        else {
            let socialvc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBSocialVC") as! FBSocialVC
            socialvc.socialName = SocialMedia.insta.rawValue
            present(socialvc, animated: false, completion: nil)
        }
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flybest&screen_name=area&country_name=\(FB_SelectedCountry)&flag=\(FB_Flag_iso)&type=&list_type=&country=\(FB_CountryID)&province=&vote_date=\(Vote_end_date.replacingOccurrences(of: ", ", with: ""))") else { return }
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (passName == "")
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    @IBAction func chooseArea(_ sender: Any)
    {
        if (areaNameArray == [])
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_no_area_found"), controller: self)
        }
        else {
            showDropDown()
        }
    }
    
    func showDropDown()
    {
        dropDown.anchorView = areaBtn
        dropDown.dataSource = areaNameArray
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            cell.flagImage.image = nil
            cell.codeLabel.text = ""
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            self.areaLabel.text = self.areaNameArray[index]
            self.areaLabel.textColor = ColorCode.FBThemeColor
            self.areaLabel.font = UIFont(name: "CopperplateGothic-Bold", size: 13)
            FB_ProvinceID = self.areaIdArray[index]
        }
        dropDown.width = areaBtn.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("area list = \(result)")
                let dataArr = result["data"] as! [NSDictionary]
                for dict in dataArr
                {
                    self.areaNameArray.append(dict["name"] as! String)
                    self.areaIdArray.append("\(dict["id"] as! NSNumber)")
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fb")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flybestBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

