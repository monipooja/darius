//
//  FBSocialVC.swift
//  Darious
//
//  Created by Apple on 04/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import  FirebaseDynamicLinks

class FBSocialVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var flagImg : UIImageView!
    
    @IBOutlet weak var socialIcon : UIImageView!
    @IBOutlet weak var socialImage : UIImageView!
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var topImgButton : UIButton!
    @IBOutlet weak var bottomLabel : UILabel!
    @IBOutlet weak var btmImgButton : UIButton!

    @IBOutlet weak var socialImgWd : NSLayoutConstraint!
    
    var socialName : String = ""
    var flagiso : String = ""
    var passName : String = ""
    var passid : String = ""
    var provid : String = ""
    var votedate : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FBThemeColor
        
        btmImgVw.tintColor = ColorCode.FBThemeColor
        if (passName != "") {
            FB_SelectedCountry = passName
            FB_ProvinceID = provid
            FB_CountryID = passid
            FB_Flag_iso = flagiso
            Vote_end_date = votedate.replacingOccurrences(of: "2", with: ", 2")
        }
        flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FB_Flag_iso).png"))
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        setDataOnLoad()
        setBottomView()
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    func setDataOnLoad()
    {
        if (socialName == SocialMedia.insta.rawValue)
        {
            socialIcon.image = #imageLiteral(resourceName: "FB_smalInsta")
            socialImage.image = #imageLiteral(resourceName: "FB_InstaIcon")
            socialImgWd.constant = 112
            
            topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_best_post").capitalized
            topImgButton.setImage(#imageLiteral(resourceName: "FB_BestPost"), for: .normal)
            bottomLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_best_instagramer").capitalized
            btmImgButton.setImage(#imageLiteral(resourceName: "FB_BestInstagramer"), for: .normal)
        }
        else if (socialName == SocialMedia.youtube.rawValue)
        {
            socialIcon.image = #imageLiteral(resourceName: "FBest_youtube")
            socialImage.image = #imageLiteral(resourceName: "Logo_of_YouTube")
            socialImgWd.constant = 80
            
            topLabel.text = "BEST VIDEO"
            topImgButton.setImage(#imageLiteral(resourceName: "FB_bestvideo"), for: .normal)
            bottomLabel.text = "BEST YOUTBER"
            btmImgButton.setImage(#imageLiteral(resourceName: "FB_bestYoutuber"), for: .normal)
        }
        else {
            socialIcon.image = #imageLiteral(resourceName: "FBest_twitter")
            socialImage.image = #imageLiteral(resourceName: "logo_twitter")
            socialImgWd.constant = 130
            
            topLabel.text = "BEST TWEET"
            topImgButton.setImage(#imageLiteral(resourceName: "FB_bestTweet"), for: .normal)
            bottomLabel.text = "BEST TWITTER"
            btmImgButton.setImage(#imageLiteral(resourceName: "FB_bestTwitter"), for: .normal)
        }
    }
    
    @IBAction func onTopButtonAction(_ sender: Any)
    {
        if (socialName == SocialMedia.insta.rawValue)
        {
            let bestvc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBInstragramBestVC") as! FBInstragramBestVC
            bestvc.list_type = ListType.post.rawValue
            present(bestvc, animated: false, completion: nil)
        }
        else if (socialName == SocialMedia.youtube.rawValue) {
        }
        else {
        }
    }
    
    @IBAction func onBottomButtonAction(_ sender: Any)
    {
        if (socialName == SocialMedia.insta.rawValue)
        {
            let bestvc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBInstragramBestVC") as! FBInstragramBestVC
            bestvc.list_type = "person"
            present(bestvc, animated: false, completion: nil)
        }
        else if (socialName == SocialMedia.youtube.rawValue)
        {
        }
        else {
        }
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flybest&screen_name=social&country_name=\(FB_SelectedCountry)&flag=\(FB_Flag_iso)&type=\(SocialMedia.insta.rawValue)&list_type=&country=\(FB_CountryID)&province=\(FB_ProvinceID)&vote_date=\(Vote_end_date.replacingOccurrences(of: ", ", with: ""))") else { return }
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (passName == "")
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    // MARK : Service call and fetching data
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fb")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flybestBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}
