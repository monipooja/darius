//
//  FBHomeVC.swift
//  Darious
//
//  Created by Apple on 04/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks

var Vote_end_date : String = ""

class FBHomeVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!

    @IBOutlet weak var flagImg : UIImageView!
    @IBOutlet weak var countryLabel : UILabel!
    
    var passName : String = ""
    var passid : String = ""
    var flagiso : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FBThemeColor
        
        btmImgVw.tintColor = ColorCode.FBThemeColor
        if (passName != "")
        {
            FB_SelectedCountry = passName
            FB_CountryID = passid
            FB_Flag_iso = flagiso
        }
        countryLabel.text = FB_SelectedCountry
        flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FB_Flag_iso).png"))
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        setBottomView()
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FB_VoteDates_URL, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flybest&screen_name=fbhome&country_name=\(FB_SelectedCountry)&flag=\(FB_Flag_iso)&type=&list_type=&country=\(FB_CountryID)&vote_date=\(Vote_end_date.replacingOccurrences(of: ", ", with: ""))&province=") else { return }
        
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    @IBAction func goToInstagram(_ sender: Any)
    {
        let areavc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBAreaVC") as! FBAreaVC
        present(areavc, animated: false, completion: nil)
    }
    
    @IBAction func goToYoutube(_ sender: Any)
    {
        let socialvc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBSocialVC") as! FBSocialVC
        socialvc.socialName = SocialMedia.youtube.rawValue
        present(socialvc, animated: false, completion: nil)
    }
    
    @IBAction func goToTwitter(_ sender: Any)
    {
        let socialvc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBSocialVC") as! FBSocialVC
        socialvc.socialName = SocialMedia.twitter.rawValue
        present(socialvc, animated: false, completion: nil)
    }
    
    @IBAction func goToOthers(_ sender: Any)
    {
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        let countryvc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBCountryVC") as! FBCountryVC
        present(countryvc, animated: false, completion: nil)
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                let data = result["data"] as! [NSDictionary]
                for dict in data
                {
                    if (dict["status"] as! String == "active") {
                        Vote_end_date = "\(dict["months"] as! String), \(dict["year"] as! NSNumber)"
                        break
                    }
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fb")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flybestBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}
