//
//  FBSplashVC.swift
//  Darious
//
//  Created by Apple on 28/10/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

var FB_SelectedCountry : String = ""
var FB_CountryID : String = ""
var FB_ProvinceID : String = ""
var FB_Flag_iso : String = ""

class FBSplashVC: UIViewController {

    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = true
//        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FBThemeColor
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(gotoNext), userInfo: nil, repeats: false)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func gotoNext()
    {
        timer.invalidate()
        let countryvc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBCountryVC") as! FBCountryVC
        present(countryvc, animated: false, completion: nil)
    }
}



public func flybestBottomViewAction(tagValue : Int, controller : UIViewController)
{
    switch tagValue {
    case 1:
        // Ranking
        let rankingvc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBRankingVC") as! FBRankingVC
        controller.present(rankingvc, animated: false, completion: nil)
        break
    case 2:
        // My Votes
        let myvotevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBMyVotesVC") as! FBMyVotesVC
        controller.present(myvotevc, animated: false, completion: nil)
        break
    case 3:
        // Darious
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
        break
    case 4:
        // Wallet
        let walletvc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBWalletVC") as! FBWalletVC
        controller.present(walletvc, animated: false, completion: nil)
        break
    case 5:
        // Notification
        let notifyvc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBNotificationVC") as! FBNotificationVC
        controller.present(notifyvc, animated: false, completion: nil)
        break
    default:
        break
    }
}

enum SocialMedia: String {
    
    case insta
    case youtube
    case twitter
}

enum ListType: String {
    
    case post
    case people
    
}
