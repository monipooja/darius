//
//  FBInstragramBestVC.swift
//  Darious
//
//  Created by Apple on 11/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import FirebaseDynamicLinks

class FBInstragramBestVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var flagImg : UIImageView!
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var nodataLabel : UILabel!
    @IBOutlet weak var voteDateLbl : UILabel!
    @IBOutlet weak var postTableVw : UITableView!
    
    @IBOutlet weak var blurView : UIView!
    @IBOutlet weak var centerView : CustomUIView!
    @IBOutlet weak var voteMsgLbl : UILabel!
    @IBOutlet weak var closeBtn : KGHighLightedButton!
    
    var list_type : String = ""
    var c_id : String = ""
    var prov_id : String = ""
    var votedate : String = ""
    var flagiso : String = ""
    var passName : String = ""
    
    var selectedCell : PostTableViewCell!
    var selectDict : NSDictionary = [:]
    var selectIndex : Int = 0
    var postArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FBThemeColor
        
        btmImgVw.tintColor = ColorCode.FBThemeColor
        if (passName != "") {
            FB_Flag_iso = flagiso
            FB_SelectedCountry =  passName
            FB_CountryID = c_id
            FB_ProvinceID = prov_id
            Vote_end_date = votedate.replacingOccurrences(of: "2", with: ", 2")
        }
        flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FB_Flag_iso).png"))
        
        if (list_type == ListType.post.rawValue) {
            topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_best_post")
        }
        else {
            topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_best_instagramer")
        }
        voteDateLbl.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "fb_voting_month")) - \(Vote_end_date) \n \(ApiResponse.getLanguageFromUserDefaults(inputString: "fb_vote_once_day"))"
        voteMsgLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "vote_success_msg")
        closeBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "close"), for: .normal)
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FBThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&type=\(SocialMedia.insta.rawValue)&list_type=\(self.list_type)&country=\(FB_CountryID)&province=\(FB_ProvinceID)"
            self.callService(urlstr: Api.FB_List_URL, parameters: params, check: "area")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    @IBAction func closeCenterView(_ sender: Any)
    {
        blurView.isHidden = true
        centerView.isHidden = true
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        if (passName == "")
        {
            dismiss(animated: false, completion: nil)
        }
        else {
            let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
            present(homevc, animated: false, completion: nil)
        }
    }
    
    @IBAction func onShare(_ sender: Any)
    {
        guard let link = URL(string: "\(Api.Share_URL)share_id=\(user_id)&app_name=flybest&screen_name=best&country_name=\(FB_SelectedCountry)&flag=\(FB_Flag_iso)&ype=\(SocialMedia.insta.rawValue)&list_type=\(self.list_type)&country=\(FB_CountryID)&province=\(FB_ProvinceID)&vote_date=\(Vote_end_date.replacingOccurrences(of: ", ", with: ""))") else { return }
        let dynamicLinksDomain = "https://flydarius.page.link"
        
        let linkBuilder = DynamicLinkComponents.init(link: link, domainURIPrefix: dynamicLinksDomain)
        
        linkBuilder!.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.flyapps.Darious")
        linkBuilder!.navigationInfoParameters?.isForcedRedirectEnabled = true
        
        linkBuilder!.iOSParameters?.appStoreID = "1469582226"
        linkBuilder!.iOSParameters?.fallbackURL = URL(string:"https://apps.apple.com/us/app/dakefly/id1469582226?ls=1")
        linkBuilder!.iOSParameters?.minimumAppVersion = "1.1"
        linkBuilder!.androidParameters = DynamicLinkAndroidParameters(packageName: "com.darius.universe")
        linkBuilder!.androidParameters?.fallbackURL = URL(string:"https://play.google.com/store/apps/details?id=com.darius.universe")
        
        linkBuilder!.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        linkBuilder!.socialMetaTagParameters?.title = "DakeFly"
        
        linkBuilder!.socialMetaTagParameters?.descriptionText = ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")
        linkBuilder!.socialMetaTagParameters?.imageURL = URL(string:"")
        
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        linkBuilder!.options = options
        
        //        guard let longDynamicLink = linkBuilder!.url else { return }
        //          print("The long URL is: \(longDynamicLink)")
        
        linkBuilder!.shorten(completion: { (shorturl, warnings, error) in
            // GlobalConstant.print( object: "The short URL is: \(url!)")
            
            if(shorturl != nil) {
                // Instantiate the interaction controller
                let ac = UIActivityViewController(activityItems: [shorturl as Any, ApiResponse.getLanguageFromUserDefaults(inputString: "share_text_flybest")], applicationActivities: nil)
                self.present(ac, animated: true)
            }
        })
    }
    
    // MARK : Service call and fetching data
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
               // print("best list = \(result)")
                if (check == "area") {
                    self.postArray = result["data"] as! [NSDictionary]
                    if (self.postArray.count > 0) {
                        self.postTableVw.reloadData()
                    }
                    else {
                        self.nodataLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                        self.nodataLabel.isHidden = false
                    }
                    customLoader.hideIndicator()
                }
                else {
                    let editdict = NSMutableDictionary.init(dictionary: self.selectDict)
                    self.selectedCell.votedView.backgroundColor = ColorCode.FBThemeColor
                    editdict["voted"] = NSNumber.init(value: 1)
                    var count = Int.init(exactly: editdict["votes"] as! NSNumber)!
                    count += 1
                    editdict["votes"] = NSNumber.init(value: count)
                    self.postArray[self.selectIndex] = editdict
                    self.selectedCell?.votesLabel.text = "\(count)"
                    customLoader.hideIndicator()
                    
                    self.blurView.isHidden = false
                    self.centerView.isHidden = false
                }
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fb")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flybestBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}

extension FBInstragramBestVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return postArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = postTableVw.dequeueReusableCell(withIdentifier: "bestpostcell") as! PostTableViewCell
        
        cell.serialNoLabel.text = "\(indexPath.section+1)"
        cell.serialNoLabel.layer.cornerRadius = cell.serialNoLabel.frame.width/2
        cell.serialNoLabel.layer.masksToBounds = true
        
        let dict = postArray[indexPath.section]
        if ((dict["profile_pic"] as! String).contains("http")) {
            cell.profileImageVw.sd_setImage(with: URL(string: dict["profile_pic"] as! String), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        else {
            cell.profileImageVw.sd_setImage(with: URL(string: "\(Api.FB_Profile_Url)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        
        if (list_type == ListType.post.rawValue)
        {
            cell.postImageVw.sd_setImage(with: URL(string: "\(Api.FB_Profile_Url)\(dict["image_video_name"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
            cell.bgView.layer.cornerRadius = 30
            cell.voteVwHt.constant = 50
            cell.voteVwWd.constant = 50
            cell.votedView.layer.cornerRadius = 50/2
        }
        else {
            cell.bgView.layer.cornerRadius = 44
            cell.voteVwHt.constant = 45
            cell.voteVwWd.constant = 45
            cell.votedView.layer.cornerRadius = 45/2
        }
        
        cell.nameLabel.text = dict["name"] as? String
        cell.usernameLabel.text = dict["best_id"] as? String
        cell.votesLabel.text = "\(dict["votes"] as! NSNumber)"
        
        if (dict["voted"] as! NSNumber == 0) {
            cell.votedView.backgroundColor = ColorCode.FBGrayVote
        }
        else {
            cell.votedView.backgroundColor = ColorCode.FBThemeColor
        }
        
        if (indexPath.section == 0) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "gold-medal")
        }
        else if (indexPath.section == 1) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "FB_silvermedal")
        }
        else if (indexPath.section == 2) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "FB_bronzemedal")
        }
        else {
            cell.goldImageVw.image = nil
        }
        cell.voteButton.addTarget(self, action: #selector(voteTheUser(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (list_type == ListType.post.rawValue){
            return 380
        }
        else {
            return 102
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: postTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func voteTheUser(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: postTableVw)
        let idxpath = postTableVw.indexPathForRow(at: point)
        
        selectedCell = postTableVw.cellForRow(at: idxpath!) as? PostTableViewCell
        
        let dict = postArray[idxpath!.section]
        selectDict = dict
        selectIndex = idxpath!.section
        
        if (selectedCell.votedView.backgroundColor == ColorCode.FBThemeColor)
        {
            ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_already_voted"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "fb_you_vote_once_day"), controller: self)
        }
        else {
            customLoader.showActivityIndicator(showColor: ColorCode.FBThemeColor.toHexString(), controller: self.view)
            var params : String = ""
            if (list_type == ListType.post.rawValue)
            {
                params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&type=\(list_type)&vote_session=\(dict["voting_session"] as! NSNumber)&post_id=\(dict["id"] as! NSNumber)&user_id=\(dict["uid"] as! NSNumber)"
            }
            else {
                params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&type=\(list_type)&vote_session=\(dict["voting_session"] as! NSNumber)&post_id=0&user_id=\(dict["id"] as! NSNumber)"
            }
            self.callService(urlstr: Api.FB_VoteUser_URL, parameters: params, check: "vote")
        }
    }
}

class PostTableViewCell : UITableViewCell
{
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var usernameLabel : UILabel!
    @IBOutlet weak var votesLabel : UILabel!
    @IBOutlet weak var serialNoLabel : UILabel!
    
    @IBOutlet weak var votedView : CustomUIView!
    @IBOutlet weak var bgView: CustomUIView!
    @IBOutlet weak var voteButton : UIButton!
    
    @IBOutlet weak var goldImageVw : UIImageView!
    @IBOutlet weak var profileImageVw : UIImageView!
    @IBOutlet weak var postImageVw : UIImageView!
    
    @IBOutlet weak var voteVwHt : NSLayoutConstraint!
    @IBOutlet weak var voteVwWd : NSLayoutConstraint!
}
