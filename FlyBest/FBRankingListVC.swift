//
//  FBRankingListVC.swift
//  Darious
//
//  Created by Apple on 11/11/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit

class FBRankingListVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var rankingTableVw : UITableView!
    @IBOutlet weak var flagImg : UIImageView!
    @IBOutlet weak var topLabel : UILabel!
    @IBOutlet weak var nodataLabel : UILabel!
    
    @IBOutlet weak var countryLabel : UILabel!
    @IBOutlet weak var areaLabel : UILabel!
    @IBOutlet weak var sectionLabel : UILabel!
    @IBOutlet weak var yearLabel : UILabel!
    @IBOutlet weak var seasonLabel : UILabel!
    
    @IBOutlet weak var countryflagImg : UIImageView!
    @IBOutlet weak var socialIcon : UIImageView!
    @IBOutlet weak var socialImg : UIImageView!
    @IBOutlet weak var socialImgWd : NSLayoutConstraint!
    
    var passDict : NSMutableDictionary = [:]
    var rankPostarray : [NSDictionary] = []
    var heightArray : [CGFloat] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.isHidden = false
        UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FBThemeColor
        
        btmImgVw.tintColor = ColorCode.FBThemeColor
        flagImg.sd_setImage(with: URL(string: "\(Api.Flag_URL)\(FB_Flag_iso).png"))
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        setBottomView()
        setDataOnLoad()
        
        topLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "fb_ranking")
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.FBThemeColor.toHexString(), controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&type=\(self.passDict["social"] as! String)&list_type=\(self.passDict["list_type"] as! String)&country=\(self.passDict["c_id"] as! String)&year=\(self.passDict["year"] as! String)&months=\(self.passDict["season"] as! String)&province=\(self.passDict["area_id"] as! String)"
            self.callService(urlstr: Api.FB_Ranking_URL, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "NotifyBubble")
        }
    }
    
    func setDataOnLoad()
    {
        if (passDict["social"] as! String == SocialMedia.insta.rawValue)
        {
            socialIcon.image = #imageLiteral(resourceName: "FB_smalInsta")
            socialImg.image = #imageLiteral(resourceName: "FB_InstaIcon")
            socialImgWd.constant = 75
        }
        else if (passDict["social"] as! String == SocialMedia.youtube.rawValue)
        {
            socialIcon.image = #imageLiteral(resourceName: "FBest_youtube")
            socialImg.image = #imageLiteral(resourceName: "Logo_of_YouTube")
            socialImgWd.constant = 60
        }
        else if (passDict["social"] as! String == SocialMedia.twitter.rawValue) {
            socialIcon.image = #imageLiteral(resourceName: "FBest_twitter")
            socialImg.image = #imageLiteral(resourceName: "logo_twitter")
            socialImgWd.constant = 90
        }
        else {
            socialIcon.image = nil
            socialImg.image = nil
        }
        
        countryLabel.text = (passDict["country"] as! String)
        areaLabel.text = (passDict["area"] as! String)
        countryflagImg.sd_setImage(with: URL(string: passDict["c_flag"] as! String))
        sectionLabel.text = (passDict["section"] as! String)
        yearLabel.text = (passDict["year"] as! String)
        seasonLabel.text = (passDict["season"] as! String)
    }
    
    @IBAction func backToHome(_ sender: Any)
    {
        let homevc = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        dismiss(animated:false, completion: nil)
    }
    
    // MARK : Service call and fetching data
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
              //  print("ranking list = \(result)")
                self.rankPostarray = result["data"] as! [NSDictionary]
                if (self.rankPostarray.count > 0) {
                    for _ in 0...self.rankPostarray.count-1
                    {
                        self.heightArray.append(105)
                    }
                    self.rankingTableVw.reloadData()
                }
                else {
                    self.nodataLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_data_found")
                    self.nodataLabel.isHidden = false
                }
                customLoader.hideIndicator()
            }
        }
    }
    
    // Mark : Set bottom view according to screen sizes
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fb")
    }
    
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        flybestBottomViewAction(tagValue: sender.tag, controller: self)
    }
    
}


extension FBRankingListVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return rankPostarray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = rankingTableVw.dequeueReusableCell(withIdentifier: "rankingpostcell") as! RankingPostTableCell
        
        cell.serialNoLabel.text = "\(indexPath.section+1)"
        cell.serialNoLabel.layer.cornerRadius = cell.serialNoLabel.frame.width/2
        cell.serialNoLabel.layer.masksToBounds = true
        
        let dict = rankPostarray[indexPath.section]
        
        if ((dict["profile_pic"] as! String).contains("http")) {
            cell.profileImageVw.sd_setImage(with: URL(string: dict["profile_pic"] as! String), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        else {
            cell.profileImageVw.sd_setImage(with: URL(string: "\(Api.FB_Profile_Url)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        cell.nameLabel.text = dict["name"] as? String
        cell.usernameLabel.text = dict["best_id"] as? String
        cell.votesLabel.text = "\(dict["votes"] as! NSNumber)"
        
        if (indexPath.section == 0) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "gold-medal")
        }
        else if (indexPath.section == 1) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "FB_silvermedal")
        }
        else if (indexPath.section == 2) {
            cell.goldImageVw.image = #imageLiteral(resourceName: "FB_bronzemedal")
        }
        else {
            cell.goldImageVw.image = nil
        }
        
        if (dict["type"] as! String == SocialMedia.insta.rawValue)
        {
            cell.socialIconImgvw.image = #imageLiteral(resourceName: "FB_smalInsta")
        }
        else if (dict["type"] as! String == SocialMedia.youtube.rawValue)
        {
            cell.socialIconImgvw.image = #imageLiteral(resourceName: "FBest_youtube")
        }
        else
        {
            cell.socialIconImgvw.image = #imageLiteral(resourceName: "FBest_twitter")
        }
        
        if (passDict["list_type"] as! String == ListType.post.rawValue)
        {
            cell.seePost.isHidden = false
            cell.postImageVw.sd_setImage(with: URL(string: dict["image_video_name"] as! String), placeholderImage: #imageLiteral(resourceName: "textImage"))
            if (heightArray[indexPath.section] > 105)
            {
                cell.seePost.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fb_hide_post"), for: .normal)
            }
            else {
                cell.seePost.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fb_see_post"), for: .normal)
            }
        }
        else {
            cell.seePost.isHidden = true
        }
        
        cell.seePost.addTarget(self, action: #selector(seePost(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightArray[indexPath.section]
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: rankingTableVw.frame.width, height: 10))
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    @objc func seePost(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: rankingTableVw)
        let idxpath = rankingTableVw.indexPathForRow(at: point)
        
        if (sender.titleLabel?.text == ApiResponse.getLanguageFromUserDefaults(inputString: "fb_see_post")) {
            heightArray[idxpath!.section] = 380
        }
        else {
            heightArray[idxpath!.section] = 105
        }
        rankingTableVw.reloadRows(at: [idxpath!], with: .none)
    }
}

class RankingPostTableCell : UITableViewCell
{
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var usernameLabel : UILabel!
    @IBOutlet weak var votesLabel : UILabel!
    @IBOutlet weak var serialNoLabel : UILabel!
    
    @IBOutlet weak var seePost : KGHighLightedButton!
    
    @IBOutlet weak var socialIconImgvw : UIImageView!
    @IBOutlet weak var goldImageVw : UIImageView!
    @IBOutlet weak var profileImageVw : UIImageView!
    @IBOutlet weak var postImageVw : UIImageView!
}
