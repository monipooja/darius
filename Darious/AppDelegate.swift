//
//  AppDelegate.swift
//  Darious
//
//  Created by Apple on 07/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import SystemConfiguration
import ReachabilitySwift
import SlideMenuControllerSwift
import FBSDKCoreKit
import FBSDKLoginKit
import SocketIO
import Firebase
import UserNotifications
import FirebaseMessaging
import GoogleSignIn
import GoogleMobileAds
import SwiftyStoreKit

let reach : Reachability = Reachability()!
var socket: SocketIOClient = SocketIOClient(socketURL: URL(string: "http://148.72.65.6:4001")!)
//var socket: SocketIOClient = SocketIOClient(socketURL: URL(string: "http://flydarius.com")!)
var current_screen : String = ""

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var myOrientation: UIInterfaceOrientationMask = .portrait
    var invoiceId : String = ""
    var titleStr : String = ""
    var appnameStr : String = ""

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.themeColor)
        
        GIDSignIn.sharedInstance().clientID = "284789267151-700q9ej6a6cguae4kb7h2mqrt03d9hi1.apps.googleusercontent.com"
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        let session = userDefault.value(forKey: "session") as? Bool
        if (session == nil) || (session == false) {
        }
        else {
        }
        
        let token = userDefault.value(forKey: "token") as? String
        if(token == nil) {
            Access_token = ""
        }
        else {
            Access_token = token!
        }
        userDefault.set(0, forKey: "pid")
        userDefault.set("", forKey: "trid")
        userDefault.set(nil, forKey: "popup")
        userDefault.synchronize()
        
        setupFirebaseNotification()
        
        setupIAP()
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        let session = userDefault.value(forKey: "session") as? Bool
        if (session == true) {
            let data : NSDictionary = ["sender_id" : user_id]
            socket.emit("disconnect_chat", data)
            socket.disconnect()
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        Messaging.messaging()
        AppEvents.activateApp()
        
        let session = userDefault.value(forKey: "session") as? Bool
        if (session == true) {
            socket.on(clientEvent: .connect) { (dataarray, ack) in
               // print("connect..................")
                if (current_screen == "chat"){
                }
            }
            socket.connect()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return myOrientation
    }
    
    func addSideController()
    {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryboard.instantiateViewController(withIdentifier: HomeViewController.identifier)
        let hamburgerController = mainStoryboard.instantiateViewController(withIdentifier: SideTableViewController.identifier)

        let slideMenuController = SlideMenuController(mainViewController: mainController, rightMenuViewController: hamburgerController)
        
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }
    
    // Deep linking - Universal link
    private func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
     
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            let _ = userActivity.webpageURL!
          //  print("url.absoluteString ------ \(url.absoluteString)")
            //handle url and open whatever page you want to open.
        }
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
      //  print("open link === \(url.absoluteString)")
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            // Handle the deep link. For example, show the deep-linked content or
            // apply a promotional offer to the user's account.
            //  print("open link = \(dynamicLink)")
            
            handleDynamicLink(dynamicLink)
            return true
        }
        else {
            let handled = ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[.sourceApplication] as? String, annotation: options[.annotation])
            
            let handle = GIDSignIn.sharedInstance()?.handle(url)
            
            return handled || handle!
        }
       // return false
    }
    
    // FBSDK Open url
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if (url.scheme == "fb1102433599936353")
        {
            
            return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        else
        {
            return (GIDSignIn.sharedInstance()?.handle(url))!
        }
    }

    //// IPV6 config code
    func ipv6Reachability() -> SCNetworkReachability? {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }
    
    // Firebase
    func setupFirebaseNotification()
    {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    OperationQueue.main.addOperation {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
                else{
                    //Do stuff if unsuccessful...
                }
            })
        } else {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound];
            let setting = UIUserNotificationSettings(types: type, categories: nil);
            UIApplication.shared.registerUserNotificationSettings(setting);
            UIApplication.shared.registerForRemoteNotifications();
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),name: Notification.Name.InstanceIDTokenRefresh, object: nil)
        FirebaseApp.configure()
    }
    
    @objc func tokenRefreshNotification(notification: NSNotification)
    {
        if Messaging.messaging().fcmToken != nil {
            // print("InstanceID token: \(Messaging.messaging().fcmToken)")
        }
        Messaging.messaging()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
         Messaging.messaging().setAPNSToken(deviceToken, type: .sandbox)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
       // print("1111 == \(notification.request.content.userInfo)")
        
        let dict = notification.request.content.userInfo as NSDictionary
        if let _ = dict["gcm.notification.app_name"] as? String
        {
            appnameStr = dict["gcm.notification.app_name"] as! String
            if let _ = dict["gcm.notification.id"] as? String
            {
                invoiceId = dict["gcm.notification.id"] as! String
            }
            else {
                invoiceId = dict["gcm.notification.user_id"] as! String
                titleStr = ""
            }
            
            if let _ = dict["gcm.notification.cat"] as? String
            {
                titleStr = dict["gcm.notification.cat"] as! String
            }
        }else {
        }
        completionHandler([.alert, .sound])
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
       // print("22222 == \(userInfo)")
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
      //  print("responseeeee == \(titleStr)")
        
        if (appnameStr == "flyclaim")
        {
            if (titleStr == "petition_victory")
            {
                UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FCBlackColor)
                let controller = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "PetitionDetailsVC") as? PetitionDetailsVC
                controller?.petitionId = NSNumber(value: Int(invoiceId)!)
                window?.rootViewController = controller
            }
            else {}
        }
        else if (appnameStr == "flyfriends")
        {
            UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
            if (titleStr == "follow")
            {
                let controller = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyFollowersVC") as? FFMyFollowersVC
                controller?.passStr = "followers"
                controller?.fromDelegate = true
                window?.rootViewController = controller
            }
            else if (titleStr == "write"){
                let controller = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFWallOpinionVC") as? FFWallOpinionVC
                controller?.fromDelegate = true
                window?.rootViewController = controller
            }
            else {
                let controller = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFWriteCommentVC") as? FFWriteCommentVC
                controller?.idpass = NSNumber.init(value: Int(invoiceId)!)
                controller?.fromDelegate = true
                window?.rootViewController = controller
            }
        }
        else if (appnameStr == "flymoney")
        {
            if (titleStr == "transfer_money") || (titleStr == "request_accept") || (titleStr == "transfer_money_request") || (titleStr == "transfer_request")
            {
                UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FMThemeColor)
                let controller = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSingleUserVC") as? FMSingleUserVC
                controller?.rcvrUserid = invoiceId
                window?.rootViewController = controller
            }
        }
        else{
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      //  print("33333 == \(data)")
        
        let dict = data as NSDictionary
//       print("33333 == \(dict)")
//        let aps = dict["aps"] as! NSDictionary
        if let _ = dict["gcm.notification.app_name"] as? String
        {
            appnameStr = dict["gcm.notification.app_name"] as! String
            if let _ = dict["gcm.notification.id"] as? String
            {
                invoiceId = dict["gcm.notification.id"] as! String
            }
            else {
                invoiceId = dict["gcm.notification.user_id"] as! String
                titleStr = ""
            }
            
            if let _ = dict["gcm.notification.cat"] as? String
            {
                titleStr = dict["gcm.notification.cat"] as! String
            }
        }else {
        }
    }
    
    func callLocalNotification(msgContent:String , userName : String , msgType:String)
    {
        if #available(iOS 10.0, *){
            //UNUserNotificationCenter.current().delegate = self
            let content = UNMutableNotificationContent()
            let requestIdentifier = "actionTap"
            content.categoryIdentifier = "CategoryIdentifier"
            content.sound = UNNotificationSound.default
            content.badge = 0
            content.title = userName
            content.body = msgContent
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 3.0, repeats: false)
            let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request) { (error:Error?) in
                
                if error != nil{
                    print(error?.localizedDescription ?? 0)
                }
                print("Notification Register Success")
            }
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        
        if let data = text.data(using: .utf8){
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    //// Handle Universal link when app installed
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            
            // print("dyamicn link == \(String(describing: dynamiclink?.url!))")
            if let _ = dynamiclink
            {
                self.handleDynamicLink(dynamiclink!)
            }
        }
        return handled
    }
    
    func handleDynamicLink(_ dynamicLink: DynamicLink) {
        
        let appUrl = dynamicLink.url
      //  print("dynamic link = \(appUrl)")
        
        if(appUrl == nil) {
        }else {
            if getQueryStringParameter(url: "\(appUrl!)", param: "app_name") == "flyclaim"
            {
                let session = userDefault.object(forKey: "session") as? Bool
                let petitionId = getQueryStringParameter(url: "\(appUrl!)", param: "share_id")
                
                if(session == false || session == nil){
                    userDefault.set(petitionId, forKey: "share_petition")
                    userDefault.synchronize()
                }else{
                    UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FCBlackColor)
                    userDefault.set(nil, forKey: "share_petition")
                    let controller = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "PetitionDetailsVC") as? PetitionDetailsVC
                    // controller!.checkScreen = "delegate"
                    controller?.petitionId = NSNumber(value: Int(petitionId!)!)
                    self.window?.rootViewController = controller
                }
            }
            else if getQueryStringParameter(url: "\(appUrl!)", param: "app_name") == "flyfriends"
            {
                if let userId = getQueryStringParameter(url: "\(appUrl!)", param: "share_id")
                {
                    let session = userDefault.object(forKey: "session") as? Bool
                    
                    if(session == false || session == nil){
                        userDefault.set(userId, forKey: "share_ff")
                        userDefault.synchronize()
                    }else{
                        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                        userDefault.set(nil, forKey: "share_ff")
                        if (userId == "\(user_id)")
                        {
                            let controller = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFMyOpinionVC") as! FFMyOpinionVC
                            self.window?.rootViewController = controller
                        }
                        else {
                            let controller = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFfriendProfileVC") as? FFfriendProfileVC
                            controller?.passid = NSNumber(value: Int(userId)!)
                            controller?.checkScreen = "delegate"
                            self.window?.rootViewController = controller
                        }
                    }
                }
            }
            else if getQueryStringParameter(url: "\(appUrl!)", param: "app_name") == "flybest"
            {
                let session = userDefault.object(forKey: "session") as? Bool
                
                let userId = getQueryStringParameter(url: "\(appUrl!)", param: "share_id")
                
                if(session == false || session == nil){
                    userDefault.set(userId, forKey: "share_fb")
                    userDefault.synchronize()
                }else{
                    UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FBThemeColor
                    userDefault.set(nil, forKey: "share_fb")
                }
                goToFlybestScreens(screen: getQueryStringParameter(url: "\(appUrl!)", param: "screen_name")!, passUrl: appUrl!)
            }
            else if getQueryStringParameter(url: "\(appUrl!)", param: "app_name") == "flysport"
            {
                let session = userDefault.object(forKey: "session") as? Bool
                
                let userId = getQueryStringParameter(url: "\(appUrl!)", param: "share_id")
                
                if(session == false || session == nil){
                    userDefault.set(userId, forKey: "share_fs")
                    userDefault.synchronize()
                }else{
                    UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FBThemeColor
                    userDefault.set(nil, forKey: "share_fs")
                }
                goToFlybestScreens(screen: getQueryStringParameter(url: "\(appUrl!)", param: "screen_name")!, passUrl: appUrl!)
            }
            else if getQueryStringParameter(url: "\(appUrl!)", param: "app_name") == "flypic"
            {
                let session = userDefault.object(forKey: "session") as? Bool
                
                let userId = getQueryStringParameter(url: "\(appUrl!)", param: "share_id")
                
                if(session == false || session == nil){
                    userDefault.set(userId, forKey: "share_fp")
                    userDefault.synchronize()
                }else{
                    UIApplication.shared.statusBarView?.backgroundColor = ColorCode.FBThemeColor
                    userDefault.set(nil, forKey: "share_fp")
                }
                goToFlypicScreens(screen: getQueryStringParameter(url: "\(appUrl!)", param: "screen_name")!, passUrl: appUrl!)
            }
            else {}
        }
    }
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        
        guard let url = URLComponents(string: url) else { return nil }
        
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    // in-app Purchase
    func setupIAP() {
        
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    let downloads = purchase.transaction.downloads
                    if !downloads.isEmpty {
                        SwiftyStoreKit.start(downloads)
                    } else if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    print("\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
        
        SwiftyStoreKit.updatedDownloadsHandler = { downloads in
            
            // contentURL is not nil if downloadState == .finished
            let contentURLs = downloads.compactMap { $0.contentURL }
            if contentURLs.count == downloads.count {
                print("Saving: \(contentURLs)")
                SwiftyStoreKit.finishTransaction(downloads[0].transaction)
            }
        }
    }
    
    func goToFlybestScreens(screen : String, passUrl : URL)
    {
        if (screen == "country") {
            let controller = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBCountryVC") as! FBCountryVC
            self.window?.rootViewController = controller
        }
        else if (screen == "fbhome") {
            let controller = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBHomeVC") as! FBHomeVC
            controller.passName = getQueryStringParameter(url: "\(passUrl)", param: "country_name")!
            controller.passid = getQueryStringParameter(url: "\(passUrl)", param: "country")!
            controller.flagiso = getQueryStringParameter(url: "\(passUrl)", param: "flag")!
            self.window?.rootViewController = controller
        }
        else if (screen == "area") {
            let controller = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBAreaVC") as! FBAreaVC
            controller.passName = getQueryStringParameter(url: "\(passUrl)", param: "country_name")!
            controller.passid = getQueryStringParameter(url: "\(passUrl)", param: "country")!
            controller.flagiso = getQueryStringParameter(url: "\(passUrl)", param: "flag")!
            controller.votedate = getQueryStringParameter(url: "\(passUrl)", param: "vote_date")!
            self.window?.rootViewController = controller
        }
        else if (screen == "social") {
            let controller = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBSocialVC") as! FBSocialVC
            controller.socialName = getQueryStringParameter(url: "\(passUrl)", param: "type")!
            controller.passName = getQueryStringParameter(url: "\(passUrl)", param: "country_name")!
            controller.passid = getQueryStringParameter(url: "\(passUrl)", param: "country")!
            controller.flagiso = getQueryStringParameter(url: "\(passUrl)", param: "flag")!
            controller.provid = getQueryStringParameter(url: "\(passUrl)", param: "province")!
            controller.votedate = getQueryStringParameter(url: "\(passUrl)", param: "vote_date")!
            self.window?.rootViewController = controller
        }
        else if (screen == "best") {
            let controller = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBInstragramBestVC") as! FBInstragramBestVC
            controller.list_type = getQueryStringParameter(url: "\(passUrl)", param: "list_type")!
            controller.passName = getQueryStringParameter(url: "\(passUrl)", param: "country_name")!
            controller.flagiso = getQueryStringParameter(url: "\(passUrl)", param: "flag")!
            controller.c_id = getQueryStringParameter(url: "\(passUrl)", param: "country")!
            controller.prov_id = getQueryStringParameter(url: "\(passUrl)", param: "province")!
            controller.votedate = getQueryStringParameter(url: "\(passUrl)", param: "vote_date")!
            self.window?.rootViewController = controller
        }
        else if (screen == "fbranking") {
            let controller = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSRankingVC") as! FSRankingVC
            controller.passname = getQueryStringParameter(url: "\(passUrl)", param: "country_name")!
            controller.flagiso = getQueryStringParameter(url: "\(passUrl)", param: "flag")!
            controller.passid = getQueryStringParameter(url: "\(passUrl)", param: "country")!
            controller.votedate = getQueryStringParameter(url: "\(passUrl)", param: "vote_date")!
            self.window?.rootViewController = controller
        }
        else if (screen == "fbvote") {
            let controller = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSMyVotesVC") as! FSMyVotesVC
            controller.passname = getQueryStringParameter(url: "\(passUrl)", param: "country_name")!
            controller.flagiso = getQueryStringParameter(url: "\(passUrl)", param: "flag")!
            controller.passid = getQueryStringParameter(url: "\(passUrl)", param: "country")!
            controller.votedate = getQueryStringParameter(url: "\(passUrl)", param: "vote_date")!
            self.window?.rootViewController = controller
        }
        else {}
    }
    
    func goToFlysportScreens(screen : String, passUrl : URL)
    {
        if (screen == "country") {
            let controller = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSCountryVC") as! FSCountryVC
            self.window?.rootViewController = controller
        }
        else if (screen == "fshome") {
            let controller = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSHomeVC") as! FSHomeVC
            controller.passName = getQueryStringParameter(url: "\(passUrl)", param: "country_name")!
            controller.passid = getQueryStringParameter(url: "\(passUrl)", param: "country")!
            controller.flagiso = getQueryStringParameter(url: "\(passUrl)", param: "flag")!
            self.window?.rootViewController = controller
        }
        else if (screen == "area") {
            let controller = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSProvinceVC") as! FSProvinceVC
            controller.passName = getQueryStringParameter(url: "\(passUrl)", param: "country_name")!
            controller.passid = getQueryStringParameter(url: "\(passUrl)", param: "country")!
            controller.flagiso = getQueryStringParameter(url: "\(passUrl)", param: "flag")!
            controller.votedate = getQueryStringParameter(url: "\(passUrl)", param: "vote_date")!
            self.window?.rootViewController = controller
        }
        else if (screen == "list") {
            let controller = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSportsListVC") as! FSportsListVC
            let dict = ["prov_id":"\(getQueryStringParameter(url: "\(passUrl)", param: "province_id")!)", "prov_name":"\(getQueryStringParameter(url: "\(passUrl)", param: "province")!)", "catg_id":"\(getQueryStringParameter(url: "\(passUrl)", param: "category_id")!)", "catg_name":"\(getQueryStringParameter(url: "\(passUrl)", param: "category")!)", "pos":"\(getQueryStringParameter(url: "\(passUrl)", param: "position")!)", "passname":"\(getQueryStringParameter(url: "\(passUrl)", param: "country_name")!)", "flag":"\(getQueryStringParameter(url: "\(passUrl)", param: "flag")!)", "country":"\(getQueryStringParameter(url: "\(passUrl)", param: "country")!)", "vote_date":"\(getQueryStringParameter(url: "\(passUrl)", param: "vote_date")!)", "league":"\(getQueryStringParameter(url: "\(passUrl)", param: "league")!)"]

            controller.passDict = dict as! NSMutableDictionary
            self.window?.rootViewController = controller
        }
        else if (screen == "fsranking") {
            let controller = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSRankingVC") as! FSRankingVC
            controller.passname = getQueryStringParameter(url: "\(passUrl)", param: "country_name")!
            controller.flagiso = getQueryStringParameter(url: "\(passUrl)", param: "flag")!
            controller.passid = getQueryStringParameter(url: "\(passUrl)", param: "country")!
            controller.votedate = getQueryStringParameter(url: "\(passUrl)", param: "vote_date")!
            self.window?.rootViewController = controller
        }
        else if (screen == "fsvote") {
            let controller = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSMyVotesVC") as! FSMyVotesVC
            controller.passname = getQueryStringParameter(url: "\(passUrl)", param: "country_name")!
            controller.flagiso = getQueryStringParameter(url: "\(passUrl)", param: "flag")!
            controller.passid = getQueryStringParameter(url: "\(passUrl)", param: "country")!
            controller.votedate = getQueryStringParameter(url: "\(passUrl)", param: "vote_date")!
            self.window?.rootViewController = controller
        }
        else {}
    }
    
    func goToFlypicScreens(screen : String, passUrl : URL)
    {
        if (screen == "album") {
            let controller = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPUserAlbumVC") as! FPUserAlbumVC
            controller.fromDelegate = true
            controller.passId = getQueryStringParameter(url: "\(passUrl)", param: "share_id")!
            self.window?.rootViewController = controller
        }
        else if (screen == "photo") {
            let controller = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPUserPhotosVC") as! FPUserPhotosVC
            controller.album_Id = getQueryStringParameter(url: "\(passUrl)", param: "albumId")!
            controller.album_name = getQueryStringParameter(url: "\(passUrl)", param: "albumName")!
            controller.fromDelegate = true
            self.window?.rootViewController = controller
        }
        else {}
    }
}

class NetworkActivityIndicatorManager: NSObject {
    
    private static var loadingCount = 0
    
    class func networkOperationStarted() {
        
        #if os(iOS)
        if loadingCount == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        loadingCount += 1
        #endif
    }
    
    class func networkOperationFinished() {
        #if os(iOS)
        if loadingCount > 0 {
            loadingCount -= 1
        }
        if loadingCount == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        #endif
    }
}
