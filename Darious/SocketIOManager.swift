//
//  SocketIOManager.swift
//  SocketChat
//
//  Created by Gabriel Theodoropoulos on 1/31/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit
import SocketIO

class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    
    var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string: "http://148.72.65.6:4001")! as URL)
    
    
    override init() {
        super.init()
    }
    
    func establishConnection() {
        socket.connect()
    }
    
    func closeConnection() {
        socket.disconnect()
    }
    
    func joinToServerWithMyId(data : NSDictionary)
    {
        socket.emit("join", data)
    }
    
    func joinToServerChat(pass_data: NSDictionary, completionHandler: @escaping (_ message: Bool?) -> Void) {
        establishConnection()
        
        socket.emit("join_flymeet", pass_data)
        
        socket.on("joined_flymeet") { ( dataArray, ack) -> Void in
            completionHandler(true)
        }
    }
    
    func exitChatWithId(nickname: String, completionHandler: () -> Void) {
        socket.emit("disconnect_chat", "")
        completionHandler()
    }
    
    func sendMessage(pass_data: NSDictionary) {
        socket.emit("chatMessage", pass_data)
    }
    
    func getChatMessage(completionHandler: @escaping (_ messageInfo: NSDictionary) -> Void) {
        socket.on("chat_recieve_flymeet") { (dataArray, socketAck) -> Void in
            let jsonDict = dataArray[0] as? NSDictionary
            
            completionHandler(jsonDict!)
        }
    }
    
    
//    private func listenForOtherMessages() {
//        socket.on("userConnectUpdate") { (dataArray, socketAck) -> Void in
//            NSNotificationCenter.defaultCenter().postNotificationName("userWasConnectedNotification", object: dataArray[0] as! [String: AnyObject])
//        }
//
//        socket.on("userExitUpdate") { (dataArray, socketAck) -> Void in
//            NSNotificationCenter.defaultCenter().postNotificationName("userWasDisconnectedNotification", object: dataArray[0] as! String)
//        }
//
//        socket.on("userTypingUpdate") { (dataArray, socketAck) -> Void in
//            NSNotificationCenter.defaultCenter().postNotificationName("userTypingNotification", object: dataArray[0] as? [String: AnyObject])
//        }
//    }
    
    
    func sendStartTypingMessage(nickname: String) {
        socket.emit("startType", nickname)
    }
    
    
    func sendStopTypingMessage(nickname: String) {
        socket.emit("stopType", nickname)
    }
}
