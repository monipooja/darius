//
//  SplashViewController.swift
//  Darious
//
//  Created by Apple on 07/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import SwiftyStoreKit

var useLang : String = ""
var tag_Lang : String = ""
var Lang_Locale : String = ""
var Free_Reveal : Bool = false

class SplashViewController: UIViewController {
    
    @IBOutlet weak var createdLabel: UILabel!
    
    var timer : Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = true
        //  fatalError()
        
        DispatchQueue.global(qos: .background).async {
            self.getInfo(.anonymous, passString: "anonymous")
            self.getInfo(.reveal, passString: "reveal")
        }
        
        let firstlaunch = userDefault.value(forKey: "firstlaunch") as? Bool
        if (firstlaunch == nil) || (firstlaunch == false)
        {}
        else {
            DispatchQueue.main.async {
                tag_Lang = userDefault.value(forKey: "tagLanguage") as! String
                let params1 = "api_key=\(api_key)&lang=\(tag_Lang)&uid=\(user_id)"
                self.callService(urlstr: Api.GetLanguage_URL, parameters: params1, check: "lang")
            }
        }
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.goToNext), userInfo: nil, repeats: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Go to next controller
    @objc func goToNext()
    {
        timer.invalidate()
        checkAppUpdatedVersion()
    }
    
    // MARK : Get Access Token Function
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
                if(check == "token") {
                    let data = result["data"] as! NSDictionary
                    Access_token = data["access_token"] as! String
                    userDefault.set(Access_token, forKey: "token")
                    userDefault.synchronize()
                    
                    self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.goToNext), userInfo: nil, repeats: false)
                }
                else {
                    let langData = result["languageData"] as! [NSDictionary]
                    
                    let archData = NSKeyedArchiver.archivedData(withRootObject: langData)
                    userDefault.set(archData, forKey: "language")
                    userDefault.synchronize()
                    
                    self.createdLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "this_is_social_platform")
                    
                    let consData = result["constantData"] as! [NSDictionary]
                    for dict in consData {
                        if (dict["tag_name"] as? String) == "mollie_key" {
                            mollie_key = (dict["tag_value"] as? String)!
                        }
                        if (dict["tag_name"] as? String) == "commission_in_percent" {
                            commission_value = (dict["tag_value"] as? String)!
                        }
                        if (dict["tag_name"] as? String) == "commission_card" {
                            commission_Card = (dict["tag_value"] as? String)!
                        }
                    }
                    YesText = ApiResponse.getLanguageFromUserDefaults(inputString: "yes")
                    NoText = ApiResponse.getLanguageFromUserDefaults(inputString: "no")
                    OkText = ApiResponse.getLanguageFromUserDefaults(inputString: "ok")
                }
            }
        }
    }
    
    // Check updates of app
    func checkAppUpdatedVersion()
    {
        let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        let infoDict = Bundle.main.infoDictionary
        let appId = infoDict?["CFBundleIdentifier"] as? String
        
        let url = NSURL.init(string: "http://itunes.apple.com/lookup?bundleId=\(appId!)")
        do {
            let data = NSData.init(contentsOf: url! as URL)
            let itunesVersionInfo = try JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? NSDictionary
            if(itunesVersionInfo?["resultCount"] as! Int == 1) {
                let appStoreVersion = ((itunesVersionInfo?["results"] as! [Any])[0] as! NSDictionary)["version"] as! String
                
              //  print("version ===== \(appStoreVersion) \n \(currentVersion!)")
                if(appStoreVersion.compare(currentVersion!) == ComparisonResult.orderedDescending)
                {
                   // print("apps to be updated")
                    let titlestr = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "great_news")) \(ApiResponse.getLanguageFromUserDefaults(inputString: "new_update_ava"))"
                    let msgStr = ApiResponse.getLanguageFromUserDefaults(inputString: "update_desc")
                    let updateBtnTitle = ApiResponse.getLanguageFromUserDefaults(inputString: "update_btn")
                    StaticFunctions.showAlert(title: titlestr, message: msgStr, actions: [updateBtnTitle], controller: self) { (actionstr) in
                        if(actionstr == updateBtnTitle) {
                            let urlStr = "itms://itunes.apple.com/us/app/dakefly/id1469582226?ls=1&mt=8"
                            // https://
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(URL(string: urlStr)!)
                            }
                        }
                    }
                }
                else {
                    self.checkAndUpdate()
                }
            }
            else {
                self.checkAndUpdate()
            }
        }
        catch {
        }
    }
    
    func checkAndUpdate()
    {
        let firstlaunch = userDefault.value(forKey: "firstlaunch") as? Bool
        if (firstlaunch == nil) || (firstlaunch == false)
        {
            let languagevc = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "LanguageCurrencyVC") as! LanguageCurrencyVC
            languagevc.checkStr = "firstLaunch"
            present(languagevc, animated: false, completion: nil)
        }
        else {
            useLang = userDefault.value(forKey: "appLanguage") as! String
            Current_Currency = userDefault.value(forKey: "appCurrency") as! String
            Currency_Name = userDefault.value(forKey: "appCurrencyName") as! String
            Currency_Code = userDefault.value(forKey: "currencyCode") as! String
            Lang_Locale = userDefault.value(forKey: "Lang_Locale") as! String
            
            self.saveDataAndGoToNext()
        }
    }
    
    func saveDataAndGoToNext()
    {
        let session = userDefault.value(forKey: "session") as? Bool
        if (session == nil) || (session == false)
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.present(vc, animated: true, completion: nil)
        }
        else {
            let data = userDefault.data(forKey: "UserDetails")
            let userdata = NSKeyedUnarchiver.unarchiveObject(with: data!) as! NSDictionary
            user_id = userdata["id"] as! NSNumber
            userData = userdata
            wallet_amount = userDefault.value(forKey: "walletAmt") as! NSNumber
            Image_URL = userDefault.value(forKey: "profileBaseURL") as! String
            
            let nickname = userData["nickname"] as! String
            if (nickname == "")
            {
                let profilevc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                profilevc.presentFrom = "login"
                self.present(profilevc, animated: true, completion: nil)
            }
            else {
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.addSideController()
            }
        }
    }
    
    //// MARK : in-app purchase
    func getInfo(_ purchase: ServicePurchase, passString : String){
        
        SwiftyStoreKit.retrieveProductsInfo([appBundleId + "." + purchase.rawValue]) { result in
            if let product = result.retrievedProducts.first
            {
                if (passString == "reveal")
                {
                    Reveal_price = product.localizedPrice!
                }
                else if (passString == "anonymous")
                {
                    Anonymous_price = product.localizedPrice!
                }
                else {}
            }
        }
    }
}
