//
//  ViewController.swift
//  Darious
//
//  Created by Apple on 07/05/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import Firebase

class ViewController: UIViewController, GIDSignInDelegate {

    @IBOutlet weak var createdLabel: UILabel!
    @IBOutlet weak var dakeflyLabel: UILabel!
    @IBOutlet weak var agreeTNCLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    @IBOutlet weak var fbWd : NSLayoutConstraint!
    @IBOutlet weak var fbHt : NSLayoutConstraint!
    @IBOutlet weak var gmailWd : NSLayoutConstraint!
    @IBOutlet weak var gmailHt : NSLayoutConstraint!
    @IBOutlet weak var emailWd : NSLayoutConstraint!
    @IBOutlet weak var emailHt : NSLayoutConstraint!
    
    var userName : String = ""
    var userEmail : String = ""
    var socialId : String = ""
    var profilePic : String = ""
    var checkAgree : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        UIApplication.shared.statusBarView?.isHidden = false
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.themeColor)
        
        agreeTNCLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "accept_terms_condition_darius")
        
        DispatchQueue.main.async {
            let params2 = "api_key=\(api_key)"
            self.callService(urlstr: Api.STDCode_URL, parameters: params2, check: "std")
        }
        
        setLanguage()
        
        if(Access_token == "")
        {
            DispatchQueue.main.async {
                let params = "device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)"
                self.callService(urlstr: Api.GetToken_URL, parameters: params, check: "token")
            }
            userDefault.set(false, forKey: "session")
            userDefault.set(nil, forKey: "UserDetails")
            userDefault.synchronize()
        }
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        if (view.frame.width == 320) && (view.frame.height == 568)
        {
            fbWd.constant = 75
            fbHt.constant = 75
            gmailWd.constant = 75
            gmailHt.constant = 75
            emailWd.constant = 75
            emailHt.constant = 75
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        dakeflyLabel.text = "akeFly"
        createdLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "this_is_social_platform")
    }
    
    @IBAction func agreeTC(_ sender: Any)
    {
        if (checkAgree)
        {
            checkImageView.image = nil
            checkAgree = false
        }
        else {
            checkImageView.image = #imageLiteral(resourceName: "Rightcheck")
            checkAgree = true
        }
    }
    
    @IBAction func openTC(_ sender: Any)
    {
        let termsvc = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "TermsConditionVC") as! TermsConditionVC
        present(termsvc, animated: false, completion: nil)
    }

    @IBAction func facebookLogin(_ sender : Any)
    {
        if(checkAgree == false) {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_select_terms_and_condition")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else {
            facebookSignup()
        }
    }
    
    @IBAction func googleLogin(_ sender : Any)
    {
        if(checkAgree == false) {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_select_terms_and_condition")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else {
            GIDSignIn.sharedInstance().signIn()
        }
    }
    
    @IBAction func emailLogin(_ sender : Any)
    {
        if(checkAgree == false) {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "please_select_terms_and_condition")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
        else {
            let loginvc = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            present(loginvc, animated: false, completion: nil)
        }
    }
    
    func facebookSignup()
    {
        let fbLoginManager : LoginManager = LoginManager()
        
        fbLoginManager.logIn(permissions: ["email"], from: self, handler: { (result, error) -> Void in

            if (error == nil) {
                let fbloginresult : LoginManagerLoginResult = result!
                if(fbloginresult.isCancelled) {
                    //Show Cancel alert
                } else if(fbloginresult.grantedPermissions.contains("email")) {
                    self.returnUserData()
                   // fbLoginManager.logOut()
                }
                else {
                   // print("fbbbbb = \(fbloginresult)")
                }
            }
        })
    }
    
    func returnUserData() {
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"id, name, picture.type(large), email"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil) {
                // Process error
               // print("\n\n Error: \(String(describing: error))")
            } else {
                let resultDic = result as! NSDictionary
              //  print("\n\n  fetched user: \(String(describing: result))")
                if (resultDic.value(forKey:"name") != nil) {
                    self.userName = resultDic.value(forKey:"name")! as! String
                    let wI = NSMutableString( string: self.userName)
                    CFStringTransform( wI, nil, "Any-Hex/Java" as NSString, true )
                    self.userName = wI as String
                }
                if (resultDic.value(forKey:"email") != nil) {
                    self.userEmail = resultDic.value(forKey:"email")! as! String
                }
                if (resultDic.value(forKey:"id") != nil) {
                    self.socialId = resultDic.value(forKey:"id")! as! String
                }
                if (resultDic.value(forKey:"picture") != nil) || (resultDic.value(forKey:"picture") as! NSDictionary != [:]) {
                    let picDict = resultDic.value(forKey:"picture")! as! NSDictionary
                    if let datadict = picDict["data"] as? NSDictionary
                    {
                        if (datadict != [:])
                        {
                           // self.profilePic = datadict["url"] as! String
                        }
                    }
                }
                self.profilePic = "https://graph.facebook.com/\(self.socialId)/picture?type=large"
                customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&role=\(role)&social_id=\(self.socialId)&social_type=facebook&social_name=\(self.userName)&profile_pic=\(self.profilePic)&email=\(self.userEmail)&lang=\(tag_Lang)"
                self.callService(urlstr: Api.SocialLogin_URL, parameters: params, check: "login")
            }
        })
    }
    
    // MARK : Google Sign in Method
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!)
    {
        if let _ = error {
            // print("errrrr = \(error.localizedDescription)")
        } else {
            customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&role=\(role)&social_id=\(user.userID!)&social_type=gmail&social_name=\(user.profile.!)&profile_pic=\(user.profile.imageURL(withDimension: 200)!)&email=\(user.profile.email!)&lang=\(tag_Lang)"
            self.callService(urlstr: Api.SocialLogin_URL, parameters: params, check: "login")
        }
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        // customLoader.hide()
        self.present(viewController, animated: false, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            
            OperationQueue.main.addOperation {
                
                if(check == "std") {
                    let data = result["data"] as! [NSDictionary]
                    var codeArray : [NSNumber] = []
                    var nameArray : [String] = []
                    var isoArray : [String] = []
                    for dict in data
                    {
                        codeArray.append(dict["phonecode"] as! NSNumber)
                        nameArray.append(dict["nicename"] as! String)
                        isoArray.append(dict["iso"] as! String)
                    }
                    let archData = NSKeyedArchiver.archivedData(withRootObject: data)
                    userDefault.set(archData, forKey: "stdcode")
                    userDefault.set(codeArray, forKey: "code")
                    userDefault.set(nameArray, forKey: "cname")
                    userDefault.set(isoArray, forKey: "iso")
                    userDefault.synchronize()
                }
                else if (check == "token") {
                    let data = result["data"] as! NSDictionary
                    Access_token = data["access_token"] as! String
                    userDefault.set(Access_token, forKey: "token")
                    userDefault.synchronize()
                }
                else {
                   // print("main login result === \(result)")
                    customLoader.hideIndicator()
                    let userDetails = result["userDetails"] as! NSDictionary
                    let archUserDetails = NSKeyedArchiver.archivedData(withRootObject: userDetails)
                    userDefault.set(archUserDetails, forKey: "UserDetails")
                    userDefault.set(true, forKey: "session")
                    
                    userData = userDetails
                    user_id = userDetails["id"] as! NSNumber
                    wallet_amount = userDetails["wallet_amount"] as! NSNumber
                    Free_Reveal = result["free_reveal"] as! Bool
                    userDefault.set(wallet_amount, forKey: "walletAmt")
                    
                    if ((userData["profile_pic"] as! String).contains("graph.facebook.com") == true) || ((userData["profile_pic"] as! String).contains("googleusercontent") == true)
                    {
                        Image_URL = ""
                    }
                    else {
                        Image_URL = Api.Profile_BaseURL
                    }
                    userDefault.set(Image_URL, forKey: "profileBaseURL")
                    userDefault.synchronize()
                    
                    let subscribeTopic = "\(userDetails["id"] as! NSNumber)"
                    Messaging.messaging().subscribe(toTopic: subscribeTopic)
                    
                    if let petid = userDefault.value(forKey: "share_petition") as? String
                    {
                        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FCBlackColor)
                        userDefault.set(nil, forKey: "share_petition")
                        let controller = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "PetitionDetailsVC") as! PetitionDetailsVC
                        controller.petitionId = NSNumber(value: Int(petid)!)
                        // controller.checkScreen = "login"
                        self.present(controller, animated: false, completion: nil)
                    }
                    else if let shareid = userDefault.value(forKey: "share_ff") as? String
                    {
                        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FFThemeColor)
                        userDefault.set(nil, forKey: "share_ff")
                        if (shareid == "\(user_id)")
                        {
                            let controller = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFHomeVC") as! FFHomeVC
                            self.present(controller, animated: false, completion: nil)
                        }
                        else {
                            let controller = StoryboardType.ff_storyboard.instantiateViewController(withIdentifier: "FFfriendProfileVC") as! FFfriendProfileVC
                            controller.passid = NSNumber(value: Int(shareid)!)
                            controller.checkScreen = "login"
                            self.present(controller, animated: false, completion: nil)
                        }
                    }
                    else if let _ = userDefault.value(forKey: "share_fb") as? String {
                        let controller = StoryboardType.fbest_storyboard.instantiateViewController(withIdentifier: "FBCountryVC") as! FBCountryVC
                        self.present(controller, animated: false, completion: nil)
                    }
                    else if let _ = userDefault.value(forKey: "share_fs") as? String {
                        let controller = StoryboardType.fs_storyboard.instantiateViewController(withIdentifier: "FSCountryVC") as! FSCountryVC
                        self.present(controller, animated: false, completion: nil)
                    }
                    else if let _ = userDefault.value(forKey: "share_fp") as? String {
                        let controller = StoryboardType.fpic_storyboard.instantiateViewController(withIdentifier: "FPHomeVC") as! FPHomeVC
                        self.present(controller, animated: false, completion: nil)
                    }
                    else {
                        let nickname = userDetails["nickname"] as! String
                        if (nickname == "")
                        {
                            let profilevc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                            profilevc.presentFrom = "login"
                            self.present(profilevc, animated: true, completion: nil)
                        }
                        else {
                            let appdelegate = UIApplication.shared.delegate as! AppDelegate
                            appdelegate.addSideController()
                        }
                    }
                }
            }
        }
    }
    
}
