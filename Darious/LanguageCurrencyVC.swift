//
//  LanguageCurrencyVC.swift
//  Darious
//
//  Created by Apple on 16/09/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage

class LanguageCurrencyVC: UIViewController {

    @IBOutlet weak var LangView: CustomUIView!
    @IBOutlet weak var currencyView: CustomUIView!
    
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var currencyImage: UIImageView!
    @IBOutlet weak var flagDDImage: UIImageView!
    @IBOutlet weak var currencyDDImage: UIImageView!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    
    @IBOutlet weak var langButton: UIButton!
    @IBOutlet weak var currencyButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var saveButton: KGHighLightedButton!
    
    @IBOutlet weak var flagWidth: NSLayoutConstraint!
    @IBOutlet weak var flagHeight: NSLayoutConstraint!
    @IBOutlet weak var currencyWidth: NSLayoutConstraint!
    @IBOutlet weak var currencyHeight: NSLayoutConstraint!
    @IBOutlet weak var langViewHt: NSLayoutConstraint!
    @IBOutlet weak var currencyViewHt: NSLayoutConstraint!
    
    let dropDown = DropDown()
    
    var checkStr : String = ""
    var flagisoarray : [String] = []
    var langNamearray : [String] = []
    var langTagName : [String] = []
    var currencySymbolArr : [String] = []
    var currencyCodeArr : [String] = []
    var currencyNameArr : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UIApplication.shared.statusBarView?.isHidden = false
        
        flagDDImage.tintColor = UIColor.init(hexString: themeColor)
        currencyDDImage.tintColor = UIColor.init(hexString: themeColor)
        
        if (checkStr == "firstLaunch")
        {
            backButton.isHidden = true
        }
        else {
            useLang = userDefault.value(forKey: "appLanguage") as! String
            Current_Currency = userDefault.value(forKey: "appCurrency") as! String
            Currency_Code = userDefault.value(forKey: "currencyCode") as! String
            tag_Lang = userDefault.value(forKey: "tagLanguage") as! String
            Lang_Locale = userDefault.value(forKey: "Lang_Locale") as! String
            
            backButton.isHidden = false
            saveButton.setTitle(OkText, for: .normal)
            
            createdLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "this_is_social_platform")
            if (checkStr == "language")
            {
                currencyViewHt.constant = 0
                currencyView.isHidden = true
            }
            else {
                langViewHt.constant = 0
                LangView.isHidden = true
            }
        }
        
        DispatchQueue.main.async {
            customLoader.showActivityIndicator(showColor: ColorCode.themeColor, controller: self.view)
            let params1 = "api_key=\(api_key)"
            self.callService(urlstr: Api.Language_Currency_URL, parameters: params1, check: "langcode")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func chooseLanguage(_ sender: Any)
    {
        showDropDown(textArray: langNamearray, isoArray: flagisoarray, selectvw: langButton)
    }
    
    @IBAction func ChooseCurrency(_ sender: Any)
    {
        showDropDown(textArray: currencyNameArr, isoArray: currencySymbolArr, selectvw: currencyButton)
    }
    
    @IBAction func onConinue(_ sender: Any)
    {
        userDefault.set(useLang, forKey: "appLanguage")
        userDefault.set(Current_Currency, forKey: "appCurrency")
        userDefault.set(Currency_Code, forKey: "currencyCode")
        userDefault.set(Currency_Name, forKey: "appCurrencyName")
        userDefault.set(tag_Lang, forKey: "tagLanguage")
        userDefault.set(Lang_Locale, forKey: "Lang_Locale")
        userDefault.synchronize()
        
        if (self.checkStr == "firstLaunch")
        {
            saveDataAndGoToNext()
        }
        else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func onGoBack(_ sender: Any)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func showDropDown(textArray : [String], isoArray : [String], selectvw : UIButton)
    {
        dropDown.anchorView = selectvw
        dropDown.dataSource = textArray
        dropDown.direction = .bottom
        dropDown.dismissMode = .automatic
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // Custom cell configuration
        dropDown.cellNib = UINib(nibName: "MyDDCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDDCell else { return }
            
            // Setup your custom UI components
            if (selectvw == self.langButton)
            {
//                cell.flagWidth.constant = 30
//                cell.flagHeight.constant = 28
                let codestr = (textArray[index]).lowercased()
                cell.flagImage.sd_setImage(with: URL(string: "\(Api.Lang_Flag_URL)\(codestr).png")!)
                cell.codeLabel.text = ""
            }
            else {
                cell.flagWidth.constant = 25
                cell.flagHeight.constant = 25
                let codestr = (textArray[index]).lowercased()
                cell.flagImage.sd_setImage(with: URL(string: "\(Api.Currency_icon_URL)\(codestr).png")!)
                cell.codeLabel.text = self.currencySymbolArr[index]
            }
        }
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //  print("Selected item: \(item) at index: \(index)")
            
            if (selectvw == self.langButton)
            {
                let codestr = (textArray[index]).lowercased()
                self.flagImage.sd_setImage(with: URL(string: "\(Api.Lang_Flag_URL)\(codestr).png")!)
                self.languageLabel.text = textArray[index].capitalized
                useLang = codestr
                Lang_Locale = self.flagisoarray[index]
                tag_Lang = self.langTagName[index]
                
                customLoader.showActivityIndicator(showColor: ColorCode.themeColor, controller: self.view)
                if (user_id == 0){
                    let params1 = "api_key=\(api_key)&lang=\(tag_Lang)&uid="
                    self.callService(urlstr: Api.GetLanguage_URL, parameters: params1, check: "lang")
                }
                else {
                    let params1 = "api_key=\(api_key)&lang=\(tag_Lang)&uid=\(user_id)"
                    self.callService(urlstr: Api.GetLanguage_URL, parameters: params1, check: "lang")
                }
            }
            else {
                let codestr = (textArray[index]).lowercased()
                self.currencyImage.sd_setImage(with: URL(string: "\(Api.Currency_icon_URL)\(codestr).png")!)
                self.currencyLabel.text = textArray[index].capitalized
                Current_Currency = self.currencySymbolArr[index]
                Currency_Code = self.currencyCodeArr[index]
                Currency_Name = (self.currencyNameArr[index]).lowercased()
            }
        }
        dropDown.width = selectvw.frame.width
        dropDown.reloadAllComponents()
        dropDown.show()
    }

    // MARK : Get Access Token Function
    func callService(urlstr : String, parameters : String, check : String)
    {
        ApiResponse.onPostPhp(url: urlstr, parms: parameters, controller: self) { (result) in
            OperationQueue.main.addOperation {
                
                if (check == "lang")
                {
                    let langData = result["languageData"] as! [NSDictionary]
                    
                    let archData = NSKeyedArchiver.archivedData(withRootObject: langData)
                    userDefault.set(archData, forKey: "language")
                    userDefault.synchronize()
                    
                    userDefault.set(useLang, forKey: "appLanguage")
                    userDefault.set(Current_Currency, forKey: "appCurrency")
                    userDefault.set(Currency_Code, forKey: "currencyCode")
                    userDefault.set(Currency_Name, forKey: "appCurrencyName")
                    userDefault.set(tag_Lang, forKey: "tagLanguage")
                    userDefault.set(Lang_Locale, forKey: "Lang_Locale")
                    userDefault.synchronize()
                    
                    let consData = result["constantData"] as! [NSDictionary]
                    for dict in consData {
                        if (dict["tag_name"] as? String) == "mollie_key" {
                            mollie_key = (dict["tag_value"] as? String)!
                        }
                        if (dict["tag_name"] as? String) == "commission_in_percent" {
                            commission_value = (dict["tag_value"] as? String)!
                        }
                        if (dict["tag_name"] as? String) == "commission_card" {
                            commission_Card = (dict["tag_value"] as? String)!
                        }
                    }
                    customLoader.hideIndicator()
               
                    YesText = ApiResponse.getLanguageFromUserDefaults(inputString: "yes")
                    NoText = ApiResponse.getLanguageFromUserDefaults(inputString: "no")
                    OkText = ApiResponse.getLanguageFromUserDefaults(inputString: "ok")
                    
                    if (self.checkStr == "firstLaunch") {
                        self.saveButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "continue"), for: .normal)
                    }
                    else {
                    self.saveButton.setTitle(OkText, for: .normal)
                    }
                    self.createdLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "this_is_social_platform")
                }
                else
                {
                     print("ressss = \(result)")
                    customLoader.hideIndicator()
                    let languageArr = result["languages"] as! [NSDictionary]
                    for dict in languageArr
                    {
                        let str = dict["COLUMN_NAME"] as! String
                        let split = str.split(separator: ":")
                        self.flagisoarray.append(String(split[0]))
                        self.langTagName.append(str)
                        self.langNamearray.append(String(split[1]).capitalized)
                        
                        if (useLang == String(split[1]).lowercased()) && (self.checkStr == "language")
                        {
                            self.flagImage.sd_setImage(with: URL(string: "\(Api.Lang_Flag_URL)\(String(split[1]).lowercased()).png")!)
                            self.flagWidth.constant = 35
                            self.flagHeight.constant = 28
                            self.languageLabel.text = String(split[1]).capitalized
                        }
                    }
                    
                    if (self.checkStr == "firstLaunch")
                    {
                        useLang = self.langNamearray[0].lowercased()
                        tag_Lang = self.langTagName[0].lowercased()
                        Lang_Locale = self.flagisoarray[0].lowercased()
                        
                        self.flagImage.sd_setImage(with: URL(string: "\(Api.Lang_Flag_URL)\(self.langNamearray[0].lowercased()).png")!)
                        self.flagWidth.constant = 35
                        self.flagHeight.constant = 28
                        self.languageLabel.text = self.langNamearray[0]
                    }
                    
                    let currencyArr = result["currency"] as! [NSDictionary]
                    for dict in currencyArr
                    {
                        self.currencyNameArr.append((dict["currency"] as! String).capitalized)
                        self.currencySymbolArr.append(dict["symbol"] as! String)
                        self.currencyCodeArr.append(dict["code"] as! String)
                        
                        if (Current_Currency == dict["symbol"] as! String) && (self.checkStr == "currency")
                        {
                            self.currencyImage.sd_setImage(with: URL(string: "\(Api.Currency_icon_URL)\((dict["currency"] as! String).lowercased()).png")!)
                            self.currencyWidth.constant = 28
                            self.currencyHeight.constant = 28
                            self.currencyLabel.text = (dict["currency"] as! String).capitalized
                            Currency_Name = (dict["currency"] as! String).lowercased()
                            Currency_Code = dict["code"] as! String
                        }
                    }
                    
                    if (self.checkStr == "firstLaunch")
                    {
                        self.currencyImage.sd_setImage(with: URL(string: "\(Api.Currency_icon_URL)\(self.currencyNameArr[0].lowercased()).png")!)
                        self.currencyWidth.constant = 28
                        self.currencyHeight.constant = 28
                        self.currencyLabel.text = self.currencyNameArr[0]
                        Current_Currency = self.currencySymbolArr[0]
                        Currency_Code = self.currencyCodeArr[0]
                        Currency_Name = (self.currencyNameArr[0]).lowercased()
                    }
                    
                    if (self.checkStr == "firstLaunch")
                    {
                        customLoader.showActivityIndicator(showColor: ColorCode.themeColor, controller: self.view)
                        let params1 = "api_key=\(api_key)&lang=\(tag_Lang)"
                        self.callService(urlstr: Api.GetLanguage_URL, parameters: params1, check: "lang")
                    }
                }
            }
        }
    }

    func saveDataAndGoToNext()
    {
        let session = userDefault.value(forKey: "session") as? Bool
        if (session == nil) || (session == false)
        {
            userDefault.set(true, forKey: "firstlaunch")
            userDefault.synchronize()
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.present(vc, animated: true, completion: nil)
        }
        else {
            let data = userDefault.data(forKey: "UserDetails")
            let userdata = NSKeyedUnarchiver.unarchiveObject(with: data!) as! NSDictionary
            user_id = userdata["id"] as! NSNumber
            userData = userdata
            wallet_amount = userDefault.value(forKey: "walletAmt") as! NSNumber
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
        }
    }
}
