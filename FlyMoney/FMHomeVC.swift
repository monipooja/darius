//
//  FMHomeVC.swift
//  Darious
//
//  Created by Dario Carrasco on 06/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FMHomeVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var notifyImageView : UIImageView!
    
    @IBOutlet weak var userCollectionView : UICollectionView!
    @IBOutlet weak var balanceLabel : UILabel!
    @IBOutlet weak var moneyLabel : UILabel!
    @IBOutlet weak var searchBtn : UIButton!
    @IBOutlet weak var groupBtn : KGHighLightedButton!
    
    var userArray : [NSDictionary] = []
    var nameArr : [String] = []
    var groupArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMThemeColor)
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setBottomView()
        setLanguage()

        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FM_HomeData_URL, parameters: params, check: "user")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setLanguage()
    {
        moneyLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "my_money")
        searchBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "search"), for: .normal)
        groupBtn.setTitle("\(ApiResponse.getLanguageFromUserDefaults(inputString: "create_group"))", for: .normal)
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fm")
    }
    
    @IBAction func onSearch(_ sender: Any)
    {
        let searchvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSearchVC") as! FMSearchVC
        searchvc.fromScreen = "single"
        present(searchvc, animated: false, completion: nil)
    }
    
    @IBAction func onGroup(_ sender: Any)
    {
        let groupvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMGroupVC") as! FMGroupVC
        present(groupvc, animated: false, completion: nil)
    }
    
    @IBAction func onFMHome(_ sender: Any)
    {
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.addSideController()
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FMThemeColor, controller: self.view)
            ApiResponse.onResponsePostPhp(url: urlstr, parms: parameters) { (result, error) in
                
                if (error == "") {
                 //   print("fm home result = \(result)")
                    let status = result["status"] as! NSNumber
                    
                    OperationQueue.main.addOperation {
                        if(status == 1)
                        {
                            self.userArray = result["allTransitions"] as! [NSDictionary]
                            self.groupArray = result["groupTransitions"] as! [NSDictionary]
                            if let _ = result["totalWalletAmount"] as? NSNumber
                            {
                                wallet_amount = result["totalWalletAmount"] as! NSNumber
                            }
                            else {
                                let ft = Float(result["totalWalletAmount"] as! String)
                                wallet_amount = NSNumber(value: ft!)
                            }
                            
                            let numberFormatter = NumberFormatter()
                            numberFormatter.numberStyle = .decimal
                            let formattedNumber = numberFormatter.string(from: wallet_amount)
                            self.balanceLabel.text = "\(formattedNumber!) \(Current_Currency)"
                            
                            userDefault.set(wallet_amount, forKey: "walletAmt")
                            userDefault.synchronize()
                            self.userCollectionView.reloadData()
                            for dict in self.userArray {
                                self.nameArr.append("\(dict["first_name"] as! String) \(dict["last_name"] as! String)")
                            }
                            customLoader.hideIndicator()
                        }
                        else if(status == 3){
                            customLoader.hideIndicator()
                            Access_token = ""
                            userDefault.set(Access_token, forKey: "token")
                            userDefault.synchronize()
                            let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(VC, animated: true, completion: nil)
                        }
                        else {
                            if let str = result["message"] {
                                let msgStr = str as! String
                                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                                if(showMsg != "") {
                                    ApiResponse.alert(title: "", message: showMsg, controller: self)
                                }
                                else {
                                    ApiResponse.alert(title: "", message: msgStr, controller: self)
                                }
                            }
                            else {
                                customLoader.hideIndicator()
                            }
                        }
                    }
                }
                else {
                    ApiResponse.alert(title: "Reuqest tiempo fuera", message: "Inténtalo de nuevo", controller: self)
                }
            }
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Group
            let searchvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSearchVC") as! FMSearchVC
            searchvc.fromScreen = "group"
            present(searchvc, animated: false, completion: nil)
            break
        case 2:
            // Transaction
            let transvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMTransactionVC") as! FMTransactionVC
            present(transvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMWalletVC") as! FMWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMNotificationVC") as! FMNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMHomeVC : UICollectionViewDataSource, UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (section == 0) {
            return groupArray.count
        } else {
            return userArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (indexPath.section == 0)
        {
            let cell = userCollectionView.dequeueReusableCell(withReuseIdentifier: "groupcell", for: indexPath) as! GroupCollectionCell
            
            let dict = groupArray[indexPath.item]
            cell.userImgvw.sd_setImage(with: URL(string: "\(Api.FM_GroupImage_URL)\(dict["group_image"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
            cell.nameLabel.text = "\(dict["group_name"] as! String)"
            
            cell.userImgvw.layer.cornerRadius = cell.userImgvw.bounds.width/2
            cell.userImgvw.layer.masksToBounds = true
            
            cell.grpCircleBtn.layer.cornerRadius = cell.grpCircleBtn.bounds.width/2
            
            return cell
        }
        else {
            let cell = userCollectionView.dequeueReusableCell(withReuseIdentifier: "usercell", for: indexPath) as! UserCollectionCell
            
            let dict = userArray[indexPath.item]
            cell.userImgvw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
            cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
            
            cell.userImgvw.layer.cornerRadius = cell.userImgvw.bounds.width/2
            cell.userImgvw.layer.masksToBounds = true
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (indexPath.section == 0)
        {
            let dict = groupArray[indexPath.row]
            let creatgrpvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMCreateGroupVC") as! FMCreateGroupVC
            creatgrpvc.groupDict = dict
            creatgrpvc.fromScreen = "group"
            present(creatgrpvc, animated: false, completion: nil)
        }
        else {
            let dict = userArray[indexPath.row]
            let singlevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSingleUserVC") as! FMSingleUserVC
            singlevc.passDict = userArray[indexPath.item]
            singlevc.rcvrUserid = "\(dict["receiver_id"] as! NSNumber)"
            singlevc.singelFromScreen = "home"
            present(singlevc, animated: true, completion: nil)
        }
    }
}


class UserCollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var userImgvw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
}

class GroupCollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var userImgvw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var grpCircleBtn : UIButton!
}
