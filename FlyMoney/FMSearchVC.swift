//
//  FMSearchVC.swift
//  Darious
//
//  Created by Dario Carrasco on 06/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FMSearchVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var peopleTableView : UITableView!
    @IBOutlet weak var searchTF : KGHighLightedField!
    @IBOutlet weak var centerMsgLbl : UILabel!
    
    var fromScreen : String = ""
    var nameArray : [String] = []
    var filterArray : [String] = []
    var dictArray : [NSDictionary] = []
    var filterDictArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMThemeColor)
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }

        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setDataOnLoad()
        
        DispatchQueue.main.async {
            if (self.fromScreen == "single") {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
                self.callService(urlstr: Api.FM_AllUser_URL, parameters: params, check: "user")
            }
            else {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
                self.callService(urlstr: Api.FM_AllGroup_URL, parameters: params, check: "group")
            }
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setDataOnLoad()
    {
        searchTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "search_people")
        centerMsgLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_listing_avaliable")
        
        peopleTableView.rowHeight = UITableView.automaticDimension
        peopleTableView.estimatedRowHeight = 60
        peopleTableView.layer.cornerRadius = 10
        peopleTableView.layer.masksToBounds = true
        
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fm")
    }

    @IBAction func onFMHome(_ sender: Any)
    {
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FMThemeColor, controller: self.view)
            ApiResponse.onResponsePostPhp(url: urlstr, parms: parameters) { (result, error) in
                
                if (error == "") {
                 //   print("all user/group result = \(result)")
                    let status = result["status"] as! NSNumber
                    
                    OperationQueue.main.addOperation {
                        if(status == 1)
                        {
                            if(check == "user")
                            {
                                if let _ = result["UserDetails"] as? [NSDictionary]
                                {
                                        self.dictArray = result["UserDetails"] as! [NSDictionary]
                                    if (self.dictArray.count > 0) {
                                        for dict in self.dictArray
                                        {
                                            self.nameArray.append("\(dict["first_name"] as! String) \(dict["last_name"] as! String)")
                                        }
                                        self.filterDictArray = self.dictArray
                                        self.peopleTableView.reloadData()
                                    }
                                    else {
                                        self.centerMsgLbl.isHidden = false
                                    }
                                }
                                else {
                                    self.centerMsgLbl.isHidden = false
                                }
                            }
                            else {
                                if let _ = result["groupTransitions"] as? [NSDictionary] {
                                    self.dictArray = result["groupTransitions"] as! [NSDictionary]
                                    if (self.dictArray.count > 0)
                                    {
                                        for dict in self.dictArray
                                        {
                                            self.nameArray.append(dict["group_name"] as! String)
                                        }
                                        self.filterDictArray = self.dictArray
                                        self.peopleTableView.reloadData()
                                    }
                                    else {
                                        self.centerMsgLbl.isHidden = false
                                    }
                                }
                                else {
                                    self.centerMsgLbl.isHidden = false
                                }
                            }
                            customLoader.hideIndicator()
                        }
                        else if(status == 3){
                            customLoader.hideIndicator()
                            Access_token = ""
                            userDefault.set(Access_token, forKey: "token")
                            userDefault.synchronize()
                            let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(VC, animated: true, completion: nil)
                        }
                        else {
                            if let str = result["message"] {
                                let msgStr = str as! String
                                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                                if(showMsg != "") {
                                    ApiResponse.alert(title: "", message: showMsg, controller: self)
                                }
                                else {
                                    ApiResponse.alert(title: "", message: msgStr, controller: self)
                                }
                            }
                            else {
                                customLoader.hideIndicator()
                            }
                        }
                    }
                }
                else {
                    ApiResponse.alert(title: "Reuqest tiempo fuera", message: "Inténtalo de nuevo", controller: self)
                }
            }
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Home
            let searchvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSearchVC") as! FMSearchVC
            searchvc.fromScreen = "group"
            present(searchvc, animated: false, completion: nil)
            break
        case 2:
            // Transaction
            let transvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMTransactionVC") as! FMTransactionVC
            present(transvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMWalletVC") as! FMWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMNotificationVC") as! FMNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMSearchVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterDictArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = peopleTableView.dequeueReusableCell(withIdentifier: "fmpeoplecell") as! FMPeopleTableCell
        
        let dict = filterDictArray[indexPath.row]
        if (fromScreen == "single") {
            cell.peopleImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
            cell.peopleName.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        }
        else {
            cell.peopleImgVw.sd_setImage(with: URL(string: "\(Api.FM_GroupImage_URL)\(dict["group_image"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
            cell.peopleName.text = "\(dict["group_name"] as! String)"
        }
        cell.peopleImgVw.layer.cornerRadius = cell.peopleImgVw.frame.width/2
        cell.peopleImgVw.layer.masksToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (fromScreen == "single") {
            let detailsvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMPaymentDetailsVC") as! FMPaymentDetailsVC
            detailsvc.finalScreen = fromScreen
            detailsvc.passDict = filterDictArray[indexPath.row]
            present(detailsvc, animated: true, completion: nil)
        }
        else {
            let dict = filterDictArray[indexPath.row]
            let creatgrpvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMCreateGroupVC") as! FMCreateGroupVC
            creatgrpvc.groupDict = dict
            creatgrpvc.fromScreen = "group"
            present(creatgrpvc, animated: false, completion: nil)
        }
    }
}

class FMPeopleTableCell : UITableViewCell {
    
    @IBOutlet weak var peopleImgVw : UIImageView!
    @IBOutlet weak var peopleName : UILabel!
}

/// MARK : TextField Delegate
extension FMSearchVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        filterArray = self.nameArray.filter({ (text) -> Bool in
            
            let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
            let tmp: NSString = text as NSString
            let range = tmp.range(of: updatedString!, options: NSString.CompareOptions.caseInsensitive)
            
            return range.location == 0
        })
        
        if (string == "") && (textField.text?.count == 1) {
            filterDictArray = dictArray
        }
        else {
            filterDictArray = []
            for str in filterArray {
                for dict in dictArray {
                    
                    if (fromScreen == "group")
                    {
                        if let _ = dict["group_name"] as? String
                        {
                            if (str == "\(dict["group_name"] as! String)") {
                                filterDictArray.append(dict)
                            }
                        }
                    }
                    else {
                        if let _ = dict["first_name"] as? String
                        {
                            if (str == "\(dict["first_name"] as! String) \(dict["last_name"] as! String)") {
                                filterDictArray.append(dict)
                            }
                        }
                    }
                }
            }
        }
        self.peopleTableView.reloadData()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
