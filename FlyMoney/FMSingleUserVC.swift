//
//  FMSingleUserVC.swift
//  Darious
//
//  Created by Dario Carrasco on 14/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FMSingleUserVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var contentVw : UIView!
    
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var messageTableView : UITableView!
    
    @IBOutlet weak var userProfile : UIImageView!
    @IBOutlet weak var userNameTF : UILabel!
    @IBOutlet weak var userNicknameTF : UILabel!
    
    @IBOutlet weak var sendButton : KGHighLightedButton!
    @IBOutlet weak var requestButton : KGHighLightedButton!
    
    var messageArray : [NSDictionary] = []
    var passDict : NSDictionary = [:]
    var userDict : NSDictionary = [:]
    var singelFromScreen : String = ""
    var rcvrUserid : String = ""
    var setBtm : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMThemeColor)
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        messageTableView.rowHeight = UITableView.automaticDimension
        messageTableView.estimatedRowHeight = 160
        
        setLanguage()
        if (setBtm == false) {
            setDataOnLoad()
        }
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(self.rcvrUserid)"
            self.callService(urlstr: Api.FM_MessageList_URL, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    // Mark : Gesture Action
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        contentVw.endEditing(true)
    }
    
    func setDataOnLoad()
    {
        setBtm = true
        
        userProfile.layer.cornerRadius = userProfile.bounds.width/2
        userProfile.layer.masksToBounds = true
        
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fm")
    }
    
    func setLanguage()
    {
        sendButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "send_enter_amount"), for: .normal)
        requestButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fly_money_meet_request_heading"), for: .normal)
    }
    
    @IBAction func onSend(_ sender: Any)
    {
        let detailvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMPaymentDetailsVC") as! FMPaymentDetailsVC
        detailvc.finalScreen = "singleuser"
        detailvc.fromOption = "tosend"
        detailvc.passDict = userDict
        detailvc.user_ID = NSNumber(value: Int(rcvrUserid)!)
        present(detailvc, animated: false, completion: nil)
    }
    
    @IBAction func onRequest(_ sender: Any)
    {
        let detailvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMPaymentDetailsVC") as! FMPaymentDetailsVC
        detailvc.finalScreen = "singleuser"
        detailvc.fromOption = "torequest"
        detailvc.passDict = userDict
        detailvc.user_ID = NSNumber(value: Int(rcvrUserid)!)
        present(detailvc, animated: false, completion: nil)
    }
    
    @IBAction func onFMHome(_ sender: Any)
    {
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FMThemeColor, controller: self.view)
            ApiResponse.onResponsePostPhp(url: urlstr, parms: parameters) { (result, error) in
                
                if (error == "") {
                  //  print("message result = \(result)")
                    let status = result["status"] as! NSNumber
                    
                    OperationQueue.main.addOperation {
                        if(status == 1)
                        {
                            if (check == "get")
                            {
                                self.messageArray = result["allTransitions"] as! [NSDictionary]
                                self.userDict = result["userDetails"] as! NSDictionary
                                if (self.userDict != [:])
                                {
                                    self.userProfile.sd_setImage(with: URL(string: "\(Image_URL)\(self.userDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                                    self.userNameTF.text = "\(self.userDict["first_name"] as! String) \(self.userDict["last_name"] as! String)"
                                    self.userNicknameTF.text = self.userDict["nickname"] as? String
                                }
                                if (self.messageArray.count > 0)
                                {
                                    self.messageTableView.reloadData()
                                    self.tableViewScrollToBottom(true)
                                }
                                else {
                                    self.messageTableView.isHidden = true
                                }
                                customLoader.hideIndicator()
                            }
                            else {
                                customLoader.hideIndicator()
                                self.viewDidLoad()
                            }
                        }
                        else if(status == 3){
                            customLoader.hideIndicator()
                            Access_token = ""
                            userDefault.set(Access_token, forKey: "token")
                            userDefault.synchronize()
                            let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(VC, animated: true, completion: nil)
                        }
                        else {
                            if let str = result["message"] {
                                let msgStr = str as! String
                                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                                if(showMsg != "") {
                                    ApiResponse.alert(title: "", message: showMsg, controller: self)
                                }
                                else {
                                    ApiResponse.alert(title: "", message: msgStr, controller: self)
                                }
                            }
                            else {
                                customLoader.hideIndicator()
                            }
                        }
                    }
                }
                else {
                    ApiResponse.alert(title: "Reuqest tiempo fuera", message: "Inténtalo de nuevo", controller: self)
                }
            }
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Home
            let searchvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSearchVC") as! FMSearchVC
            searchvc.fromScreen = "group"
            present(searchvc, animated: false, completion: nil)
            break
        case 2:
            // Transaction
            let transvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMTransactionVC") as! FMTransactionVC
            present(transvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMWalletVC") as! FMWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMNotificationVC") as! FMNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMSingleUserVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dict = messageArray[indexPath.row]
        if (dict["type"] as! String == "debit")
        {
            let cell = messageTableView.dequeueReusableCell(withIdentifier: "fmrightcell") as! FMRightTableCell
            
            cell.bgView.backgroundColor = UIColor.init(hexString: "146661")
            
            cell.amountLabel.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", dict["amount"] as! NSNumber)) + " \(Current_Currency)"
            cell.descLabel.text = dict["description"] as? String
            
            let datestr = dict["created_on"] as! String
            if let str = datestr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM yyyy HH:mm a")
            {
                cell.dateLabel.text = str
            }
            
            cell.reqSendImg.image = #imageLiteral(resourceName: "Black_send")
            if (dict["status"] as! String == "done")
            {
                cell.statusImageVw.image = #imageLiteral(resourceName: "Right_checked")
                cell.statusImgWd.constant = 20
            }
            else {
                cell.statusImageVw.image = #imageLiteral(resourceName: "CancelCross")
                cell.statusImgWd.constant = 20
            }
            return cell
        }
        else if (dict["type"] as! String == "credit") {
            
            let cell = messageTableView.dequeueReusableCell(withIdentifier: "fmleftcell") as! FMLeftTableCell
            
            cell.bgView.backgroundColor = UIColor.init(hexString: "D13535")
            
            cell.amountLabel.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", dict["amount"] as! NSNumber)) + " \(Current_Currency)"
            cell.descLabel.text = dict["description"] as? String
            
            let datestr = dict["created_on"] as! String
            if let str = datestr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM yyyy HH:mm a")
            {
                cell.dateLabel.text = str
            }
            cell.bottomVwHt.constant = 0
            cell.acceptBtn.isHidden = true
            cell.declineBtn.isHidden = true
            cell.upperVwHt.constant = cell.upperVwHt.constant - 35
            
            cell.reqSendImg.image = #imageLiteral(resourceName: "Black_request")
            if (dict["status"] as! String == "done")
            {
                cell.statusImageVw.image = #imageLiteral(resourceName: "Right_checked")
                cell.statusImgWd.constant = 20
            }
            else if (dict["status"] as! String == "cancel") {
                cell.statusImageVw.image = #imageLiteral(resourceName: "CancelCross")
                cell.statusImgWd.constant = 20
            }
            else {
                cell.statusImageVw.image = nil
                cell.statusImgWd.constant = 0
            }
             return cell
        }
        else if (dict["type"] as! String == "request") && (dict["created_by"] as! NSNumber == user_id)
        {
            let cell = messageTableView.dequeueReusableCell(withIdentifier: "fmrightcell") as! FMRightTableCell
            
            cell.bgView.backgroundColor = UIColor.init(hexString: "146661")
            
            cell.amountLabel.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", dict["amount"] as! NSNumber)) + " \(Current_Currency)"
            cell.descLabel.text = dict["description"] as? String
            
            let datestr = dict["created_on"] as! String
            if let str = datestr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM yyyy HH:mm a")
            {
                cell.dateLabel.text = str
            }
        
            cell.reqSendImg.image = #imageLiteral(resourceName: "Request_icon")
            if (dict["status"] as! String == "accept")
            {
                cell.statusImageVw.image = #imageLiteral(resourceName: "Right_checked")
                cell.statusImgWd.constant = 20
            }
            else if (dict["status"] as! String == "cancel") {
                cell.statusImageVw.image = #imageLiteral(resourceName: "CancelCross")
                cell.statusImgWd.constant = 20
            }
            else {
                cell.statusImageVw.image = nil
                cell.statusImgWd.constant = 0
            }
            return cell
        }
        else {
            let cell = messageTableView.dequeueReusableCell(withIdentifier: "fmleftcell") as! FMLeftTableCell
            
            cell.bgView.backgroundColor = UIColor.init(hexString: "D13535")
            
            cell.amountLabel.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", dict["amount"] as! NSNumber)) + " \(Current_Currency)"
            cell.descLabel.text = dict["description"] as? String
            
            let datestr = dict["created_on"] as! String
            if let str = datestr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM yyyy HH:mm a")
            {
                cell.dateLabel.text = str
            }
            
            cell.reqSendImg.image = #imageLiteral(resourceName: "Request_icon")
            if (dict["status"] as! String == "accept")
            {
                cell.statusImageVw.image = #imageLiteral(resourceName: "Right_checked")
                cell.statusImgWd.constant = 20
                cell.bottomVwHt.constant = 0
                cell.acceptBtn.isHidden = true
                cell.declineBtn.isHidden = true
                cell.upperVwHt.constant = cell.upperVwHt.constant - 35
            }
            else if (dict["status"] as! String == "cancel")
            {
                cell.statusImageVw.image = #imageLiteral(resourceName: "CancelCross")
                cell.statusImgWd.constant = 20
                cell.bottomVwHt.constant = 0
                cell.acceptBtn.isHidden = true
                cell.declineBtn.isHidden = true
                cell.upperVwHt.constant = cell.upperVwHt.constant - 35
            }
            else {
                cell.statusImageVw.image = nil
                cell.statusImgWd.constant = 0
                cell.bottomVwHt.constant = 35
                cell.acceptBtn.isHidden = false
                cell.declineBtn.isHidden = false
            }
            cell.acceptBtn.addTarget(self, action: #selector(acceptRequest(_:)), for: .touchUpInside)
            cell.declineBtn.addTarget(self, action: #selector(declineRequest(_:)), for: .touchUpInside)
         cell.acceptBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "accept"), for: .normal)
        cell.declineBtn.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "decline"), for: .normal)
            
            return cell
        }
    }
    
    // MARK : Scroll to bottom
    func tableViewScrollToBottom(_ animated: Bool)
    {
        let numberOfSections = self.messageTableView.numberOfSections
        if (numberOfSections > 0) {
            let numberOfRows = messageTableView.numberOfRows(inSection: numberOfSections-1)
            
            if (numberOfRows > 0) {
                let indexPath = IndexPath(row: numberOfRows-1 , section: numberOfSections-1)
                messageTableView.scrollToRow(at: indexPath,
                                             at: UITableView.ScrollPosition.bottom, animated: false)
            }
        }
    }
    
    @objc func acceptRequest(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: messageTableView)
        let idxpath = messageTableView.indexPathForRow(at: point)
        let dict = messageArray[idxpath!.row]
        
        let detailvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMPaymentDetailsVC") as! FMPaymentDetailsVC
        detailvc.finalScreen = "singleuser"
        detailvc.fromOption = "tosend"
        detailvc.passDict = userDict
        detailvc.dataDict = dict
        detailvc.user_ID = NSNumber(value: Int(rcvrUserid)!)
        present(detailvc, animated: false, completion: nil)
    }
    
    @objc func declineRequest(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: messageTableView)
        let idxpath = messageTableView.indexPathForRow(at: point)
        let dict = messageArray[idxpath!.row]
        
        let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&wallet_id=\(dict["id"] as! NSNumber)"
        self.callService(urlstr: Api.FM_DeclineRequest_URL, parameters: params, check: "decline")
    }
}

class FMLeftTableCell : UITableViewCell {
    
    @IBOutlet weak var bgView : UIView!
    
    @IBOutlet weak var statusImageVw : UIImageView!
    @IBOutlet weak var reqSendImg : UIImageView!
    @IBOutlet weak var amountLabel : UILabel!
    @IBOutlet weak var descLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    
    @IBOutlet weak var bottomVwHt : NSLayoutConstraint!
    @IBOutlet weak var upperVwHt : NSLayoutConstraint!
    @IBOutlet weak var statusImgWd : NSLayoutConstraint!
    
    @IBOutlet weak var acceptBtn : KGHighLightedButton!
    @IBOutlet weak var declineBtn : KGHighLightedButton!
}

class FMRightTableCell : UITableViewCell {
    
    @IBOutlet weak var bgView : UIView!
    
    @IBOutlet weak var statusImageVw : UIImageView!
    @IBOutlet weak var reqSendImg : UIImageView!
    @IBOutlet weak var amountLabel : UILabel!
    @IBOutlet weak var descLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    
    @IBOutlet weak var statusImgWd : NSLayoutConstraint!
}


//type - debit - right
//type - credit - left
//type - request --- create_by==loginuserid ---- right side.....status check--- accept hai to show right,,,, status = cancel---cross symbol,,,,,no right and no cross
//type - request --- create by!=loginuserid ---- left side...check--- accept hai to show right,,,, status = cancel---cross symbol,,,,,else case --- accept dcline
// send - green
// rcv - red
// reuqest kari he mene - send icon
/// reuqet get - reuqest icon
// debit - uper icon

// credit - inside icon
// rquest - dario given icon
