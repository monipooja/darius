//
//  FMTransactionVC.swift
//  Darious
//
//  Created by Dario Carrasco on 11/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FMTransactionVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var historyTableView : UITableView!
    @IBOutlet weak var balanceLabel : UILabel!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var transLabel : UILabel!
    @IBOutlet weak var centerMsgLbl : UILabel!
    
    var transArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMThemeColor)
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
       
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        transLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "transaction")
        centerMsgLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_listing_avaliable")
        historyTableView.rowHeight = UITableView.automaticDimension
        historyTableView.estimatedRowHeight = 80
        
        nameLabel.text = "\(userData["first_name"] as! String) \(userData["last_name"] as! String)"
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        let formattedNumber = numberFormatter.string(from: wallet_amount)
        balanceLabel.text = "\(formattedNumber!) \(Current_Currency)"
        
        setBottomView()
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FM_Transaction_URL, parameters: params, check: "get")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fm")
    }
    
    @IBAction func onFMHome(_ sender: Any)
    {
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FMThemeColor, controller: self.view)
            ApiResponse.onResponsePostPhp(url: urlstr, parms: parameters) { (result, error) in
                
                if (error == "") {
                   // print("transaction result = \(result)")
                    let status = result["status"] as! NSNumber
                    
                    OperationQueue.main.addOperation {
                        if(status == 1)
                        {
                            if let _ = result["allTransitions"] as? [NSDictionary]
                            {
                                self.transArray = result["allTransitions"] as! [NSDictionary]
                                if (self.transArray.count > 0)
                                {
                                    self.historyTableView.reloadData()
                                }
                                else {
                                   self.centerMsgLbl.isHidden = false
                                }
                            }
                            else {
                                self.centerMsgLbl.isHidden = false
                            }
                            customLoader.hideIndicator()
                        }
                        else if(status == 3){
                            customLoader.hideIndicator()
                            Access_token = ""
                            userDefault.set(Access_token, forKey: "token")
                            userDefault.synchronize()
                            let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(VC, animated: true, completion: nil)
                        }
                        else {
                            if let str = result["message"] {
                                let msgStr = str as! String
                                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                                if(showMsg != "") {
                                    ApiResponse.alert(title: "", message: showMsg, controller: self)
                                }
                                else {
                                    ApiResponse.alert(title: "", message: msgStr, controller: self)
                                }
                            }
                            else {
                                customLoader.hideIndicator()
                            }
                        }
                    }
                }
                else {
                    ApiResponse.alert(title: "Reuqest tiempo fuera", message: "Inténtalo de nuevo", controller: self)
                }
            }
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Group
            let searchvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSearchVC") as! FMSearchVC
            searchvc.fromScreen = "group"
            present(searchvc, animated: false, completion: nil)
            break
        case 2:
            // Transaction
            let transvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMTransactionVC") as! FMTransactionVC
            present(transvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMWalletVC") as! FMWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMNotificationVC") as! FMNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMTransactionVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = historyTableView.dequeueReusableCell(withIdentifier: "fmtranscell") as! FMTrasnsTableCell
        
        let dict = transArray[indexPath.row]
        if let _ = dict["group_name"] as? String
        {
            cell.peopleName.text = dict["group_name"] as? String
            cell.peopleImgVw.sd_setImage(with: URL(string: "\(Api.FM_GroupImage_URL)\(dict["group_image"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        else {
            cell.peopleName.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
            cell.peopleImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        cell.msgLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "pay_for")) \(dict["description"] as! String)"
        
        let amt = dict["amount"] as! NSNumber
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        let formattedNumber = numberFormatter.string(from: amt)
        cell.amountLabel.text = "\(formattedNumber!) \(Current_Currency)"
        
        cell.peopleImgVw.layer.cornerRadius = cell.peopleImgVw.frame.width/2
        cell.peopleImgVw.layer.masksToBounds = true
        
        if (dict["type"] as! String == "debit") {
            cell.sendRequestImg.image = #imageLiteral(resourceName: "Red_request")
        }
        else {
            cell.sendRequestImg.image = #imageLiteral(resourceName: "Green_rcv")
        }
        return cell
    }
}

class FMTrasnsTableCell : UITableViewCell {
    
    @IBOutlet weak var peopleImgVw : UIImageView!
    @IBOutlet weak var peopleName : UILabel!
    @IBOutlet weak var msgLabel : UILabel!
    @IBOutlet weak var amountLabel : UILabel!
    @IBOutlet weak var sendRequestImg : UIImageView!
}
