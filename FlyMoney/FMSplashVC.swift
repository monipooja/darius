//
//  FMSplashVC.swift
//  Darious
//
//  Created by Dario Carrasco on 06/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FMSplashVC: UIViewController {

    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(hexString: ColorCode.FMThemeColor)
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(gotoNext), userInfo: nil, repeats: false)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func gotoNext()
    {
        timer.invalidate()
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
}


/// user show in ios not in andriod
// moliie process
// send/request on create grp and open
// image must in both send and request

