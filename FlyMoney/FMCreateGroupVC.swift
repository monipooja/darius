//
//  FMCreateGroupVC.swift
//  Darious
//
//  Created by Dario Carrasco on 11/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit
import Photos

class FMCreateGroupVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var contentVw : UIView!
    
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var peopleTableView : UITableView!
    
    @IBOutlet weak var groupProfile : UIImageView!
    @IBOutlet weak var groupNameTF : UITextField!
    @IBOutlet weak var totalUserLabel : UILabel!
    @IBOutlet weak var participantLabel : UILabel!
    @IBOutlet weak var selectedLabel : UILabel!
    @IBOutlet weak var cameraBtn : UIButton!
    
    @IBOutlet weak var sendButton : KGHighLightedButton!
    @IBOutlet weak var requestButton : KGHighLightedButton!
    
    var userArray : [NSDictionary] = []
    var grpMembersId : String = ""
    var photoString : String = ""
    var asset : PHAsset!
    var groupDict : NSDictionary = [:]
    var fromScreen : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMThemeColor)
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        setLanguage()
        setDataOnLoad()
    
        if (userArray.count > 0) {
            peopleTableView.reloadData()
            for dict in userArray
            {
                if (grpMembersId == "")
                {
                    grpMembersId = "\(dict["id"] as! NSNumber)"
                }
                else {
                    grpMembersId = grpMembersId + ",\(dict["id"] as! NSNumber)"
                }
            }
        }
        else {
            DispatchQueue.main.async {
                self.grpMembersId = self.groupDict["group_members"] as! String
                self.groupProfile.sd_setImage(with: URL(string: "\(Api.FM_GroupImage_URL)\(self.groupDict["group_image"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
                self.groupNameTF.text = self.groupDict["group_name"] as? String
                self.photoString = self.groupDict["group_image"] as! String
                
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&group_member_id=\(self.groupDict["id"] as! NSNumber)"
                self.callService(urlstr: Api.FM_GroupMembers_URL, parameters: params, check: "members")
            }
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    // Mark : Gesture Action
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        contentVw.endEditing(true)
    }
    
    func setDataOnLoad()
    {
        groupProfile.layer.cornerRadius = groupProfile.bounds.width/2
        
        totalUserLabel.text = "\(userArray.count)"
        totalUserLabel.layer.cornerRadius = totalUserLabel.bounds.width/2
        totalUserLabel.layer.masksToBounds = true
        totalUserLabel.layer.borderWidth = 1
        totalUserLabel.layer.borderColor = UIColor.init(hexString: ColorCode.FMThemeColor).cgColor
        
        cameraBtn.layer.cornerRadius = cameraBtn.bounds.width/2
        cameraBtn.layer.masksToBounds = true
        cameraBtn.layer.borderWidth = 1
        cameraBtn.layer.borderColor = UIColor.init(hexString: ColorCode.FMThemeColor).cgColor
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        contentVw.addGestureRecognizer(gesture)
        
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fm")
    }
    
    func setLanguage()
    {
        groupNameTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "enter_group_name")
        participantLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "participants")
        selectedLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "selected")
        
        sendButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "send_enter_amount"), for: .normal)
       requestButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fly_money_meet_request_heading"), for: .normal)
    }
    
    @IBAction func onSend(_ sender: Any)
    {
        if (groupNameTF.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_group_name"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            let detailvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMPaymentDetailsVC") as! FMPaymentDetailsVC
            detailvc.memberArray = userArray
            detailvc.groupName = groupNameTF.text!
            detailvc.grpmemberIds = grpMembersId
            detailvc.grpProfileImage = groupProfile.image!
            detailvc.profileName = photoString
            detailvc.finalScreen = fromScreen
            detailvc.fromOption = "tosend"
            detailvc.passDict = groupDict
            present(detailvc, animated: false, completion: nil)
        }
    }
    
    @IBAction func onRequest(_ sender: Any)
    {
        if (groupNameTF.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_enter_group_name"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            let detailvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMPaymentDetailsVC") as! FMPaymentDetailsVC
            detailvc.memberArray = userArray
            detailvc.groupName = groupNameTF.text!
            detailvc.grpmemberIds = grpMembersId
            detailvc.grpProfileImage = groupProfile.image!
            detailvc.profileName = photoString
            detailvc.finalScreen = fromScreen
            detailvc.fromOption = "torequest"
            detailvc.passDict = groupDict
            present(detailvc, animated: false, completion: nil)
        }
    }
    
    @IBAction func uploadProfileImage(_ sender: Any)
    {
        let imgPick = UIImagePickerController.init()
        imgPick.delegate = self
        ApiResponse.CameraGallery(controller: self, imagePicker: imgPick)
    }
    
    // Image picker delegate
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let image = info[.originalImage] as! UIImage
        groupProfile.image = image
        if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            asset = result.firstObject
        }
        if(asset==nil){
            photoString="FirstImg.png"
        }
        else{
            let fname = (asset.value(forKey: "filename"))!
            let words = (fname as! String).components(separatedBy: ".")
            let fnm=words[0]
            photoString = "\(fnm).png"
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onFMHome(_ sender: Any)
    {
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FMThemeColor, controller: self.view)
            ApiResponse.onResponsePostPhp(url: urlstr, parms: parameters) { (result, error) in
                
                if (error == "") {
                    //  print("\(check) money result = \(result)")
                    let status = result["status"] as! NSNumber
                    
                    OperationQueue.main.addOperation {
                        if(status == 1)
                        {
                            if (check == "members")
                            {
                                let dict = result["groupMemberDetails"] as! NSDictionary
                                if let _ = dict["groupMemberList"] as? [NSDictionary]
                                {
                                    self.userArray = dict["groupMemberList"] as! [NSDictionary]
                                    self.peopleTableView.reloadData()
                                }
                            }
                            customLoader.hideIndicator()
                        }
                        else if(status == 3){
                            customLoader.hideIndicator()
                            Access_token = ""
                            userDefault.set(Access_token, forKey: "token")
                            userDefault.synchronize()
                            let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(VC, animated: true, completion: nil)
                        }
                        else {
                            if let str = result["message"] {
                                let msgStr = str as! String
                                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                                if(showMsg != "") {
                                    ApiResponse.alert(title: "", message: showMsg, controller: self)
                                }
                                else {
                                    ApiResponse.alert(title: "", message: msgStr, controller: self)
                                }
                            }
                            else {
                                customLoader.hideIndicator()
                            }
                        }
                    }
                }
                else {
                    ApiResponse.alert(title: "Reuqest tiempo fuera", message: "Inténtalo de nuevo", controller: self)
                }
            }
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Home
            let searchvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSearchVC") as! FMSearchVC
            searchvc.fromScreen = "group"
            present(searchvc, animated: false, completion: nil)
            break
        case 2:
            // Transaction
            let transvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMTransactionVC") as! FMTransactionVC
            present(transvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMWalletVC") as! FMWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMNotificationVC") as! FMNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMCreateGroupVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = peopleTableView.dequeueReusableCell(withIdentifier: "fmcreatecell") as! FMCreateTableCell
        
        let dict = userArray[indexPath.row]
        cell.peopleImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        cell.peopleImgVw.layer.cornerRadius = cell.peopleImgVw.frame.width/2
        cell.peopleImgVw.layer.masksToBounds = true
        
        cell.peopleName.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        
        cell.selectImg.layer.cornerRadius = cell.selectImg.frame.width/2
        cell.selectImg.layer.masksToBounds = true
        cell.selectImg.image = #imageLiteral(resourceName: "Green_check")
        
        return cell
    }
}

class FMCreateTableCell : UITableViewCell {
    
    @IBOutlet weak var peopleImgVw : UIImageView!
    @IBOutlet weak var peopleName : UILabel!
    @IBOutlet weak var selectImg : UIImageView!
}
