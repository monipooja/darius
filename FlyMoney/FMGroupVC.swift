//
//  FMGroupVC.swift
//  Darious
//
//  Created by Dario Carrasco on 07/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FMGroupVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var peopleTableView : UITableView!
    @IBOutlet weak var searchTF : KGHighLightedField!
    @IBOutlet weak var groupBtn : KGHighLightedButton!
    @IBOutlet weak var centerMsgLbl : UILabel!
    
    var selectIndex : NSMutableArray = []
    var selectedArray : [NSDictionary] = []
    var finalArray : [String] = []
    var nameArray : [String] = []
    var filterArray : [String] = []
    var dictArray : [NSDictionary] = []
    var filterDictArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMThemeColor)
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
       
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        peopleTableView.rowHeight = UITableView.automaticDimension
        peopleTableView.estimatedRowHeight = 60
        peopleTableView.delaysContentTouches = false
        peopleTableView.layer.cornerRadius = 10
        peopleTableView.layer.masksToBounds = true
        
        setLanguage()
        setBottomView()
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.FM_AllUser_URL, parameters: params, check: "user")
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setLanguage()
    {
        searchTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "search_people")
        centerMsgLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_listing_avaliable")
        groupBtn.setTitle("\(ApiResponse.getLanguageFromUserDefaults(inputString: "create_group"))", for: .normal)
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fm")
    }
    
    @IBAction func onGroup(_ sender: Any)
    {
        if (selectIndex.count < 2)
        {
            ApiResponse.alert(title: "", message: "Please select atleast two user", controller: self)
        }
        else {
            let creatgrpvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMCreateGroupVC") as! FMCreateGroupVC
            creatgrpvc.userArray = self.selectedArray
            creatgrpvc.fromScreen = "creategrp"
            present(creatgrpvc, animated: false, completion: nil)
        }
    }
    
    @IBAction func onFMHome(_ sender: Any)
    {
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FMThemeColor, controller: self.view)
            ApiResponse.onResponsePostPhp(url: urlstr, parms: parameters) { (result, error) in
                
                if (error == "") {
                    //   print("money home result = \(result)")
                    let status = result["status"] as! NSNumber
                    
                    OperationQueue.main.addOperation {
                        if(status == 1)
                        {
                            if let _ = result["UserDetails"] as? [NSDictionary]
                            {
                                self.dictArray = result["UserDetails"] as! [NSDictionary]
                                if (self.dictArray.count > 0)
                                {
                                    for dict in self.dictArray
                                    {
                                        self.nameArray.append("\(dict["first_name"] as! String) \(dict["last_name"] as! String)")
                                    }
                                    self.filterDictArray = self.dictArray
                                    self.peopleTableView.reloadData()
                                }
                                else {
                                    self.centerMsgLbl.isHidden = false
                                }
                            }
                            else {
                                self.centerMsgLbl.isHidden = false
                            }
                            customLoader.hideIndicator()
                        }
                        else if(status == 3){
                            customLoader.hideIndicator()
                            Access_token = ""
                            userDefault.set(Access_token, forKey: "token")
                            userDefault.synchronize()
                            let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(VC, animated: true, completion: nil)
                        }
                        else {
                            if let str = result["message"] {
                                let msgStr = str as! String
                                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                                if(showMsg != "") {
                                    ApiResponse.alert(title: "", message: showMsg, controller: self)
                                }
                                else {
                                    ApiResponse.alert(title: "", message: msgStr, controller: self)
                                }
                            }
                            else {
                                customLoader.hideIndicator()
                            }
                        }
                    }
                }
                else {
                    ApiResponse.alert(title: "Reuqest tiempo fuera", message: "Inténtalo de nuevo", controller: self)
                }
            }
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Home
            let searchvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSearchVC") as! FMSearchVC
            searchvc.fromScreen = "group"
            present(searchvc, animated: false, completion: nil)
            break
        case 2:
            // Transaction
            let transvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMTransactionVC") as! FMTransactionVC
            present(transvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMWalletVC") as! FMWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMNotificationVC") as! FMNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMGroupVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterDictArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = peopleTableView.dequeueReusableCell(withIdentifier: "fmgroupcell") as! FMGroupTableCell
        
        let dict = filterDictArray[indexPath.row]
        cell.peopleImgVw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        cell.peopleImgVw.layer.cornerRadius = cell.peopleImgVw.frame.width/2
        cell.peopleImgVw.layer.masksToBounds = true
        
        cell.peopleName.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        
        cell.selectImg.layer.cornerRadius = cell.selectImg.frame.width/2
        cell.selectImg.layer.masksToBounds = true
        
        if (selectIndex.count > 0) {
            if (selectIndex.contains(indexPath.row)) {
                cell.selectImg.image = #imageLiteral(resourceName: "Green_check")
                cell.selectImg.layer.borderColor = UIColor.clear.cgColor
                cell.selectImg.layer.borderWidth = 0
            }
            else {
                cell.selectImg.image = nil
                cell.selectImg.layer.borderColor = UIColor.lightGray.cgColor
                cell.selectImg.layer.borderWidth = 1
            }
        }
        else {
            cell.selectImg.image = nil
            cell.selectImg.layer.borderColor = UIColor.lightGray.cgColor
            cell.selectImg.layer.borderWidth = 1
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = peopleTableView.cellForRow(at: indexPath) as! FMGroupTableCell
        if (cell.selectImg.image == #imageLiteral(resourceName: "Green_check"))
        {
            cell.selectImg.image = nil
            cell.selectImg.layer.borderColor = UIColor.lightGray.cgColor
            cell.selectImg.layer.borderWidth = 1
            if (selectIndex.count > 0) {
                if (selectIndex.contains(indexPath.row)) {
                    selectedArray.remove(at: selectIndex.index(of: indexPath.row))
                    selectIndex.remove(indexPath.row)
                }
            }
        }
        else {
            cell.selectImg.image = #imageLiteral(resourceName: "Green_check")
            cell.selectImg.layer.borderColor = UIColor.clear.cgColor
            cell.selectImg.layer.borderWidth = 0
            selectIndex.add(indexPath.row)
            selectedArray.append(filterDictArray[indexPath.row])
        }
    }
}

class FMGroupTableCell : UITableViewCell {
    
    @IBOutlet weak var peopleImgVw : UIImageView!
    @IBOutlet weak var peopleName : UILabel!
    @IBOutlet weak var selectImg : UIImageView!
}

/// MARK : TextField Delegate
extension FMGroupVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        filterArray = self.nameArray.filter({ (text) -> Bool in
            
            let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
            let tmp: NSString = text as NSString
            let range = tmp.range(of: updatedString!, options: NSString.CompareOptions.caseInsensitive)
            
            return range.location == 0
        })
        
        if (string == "") && (textField.text?.count == 1) {
            filterDictArray = dictArray
        }
        else {
            filterDictArray = []
            for str in filterArray {
                for dict in dictArray {
                    if (str == "\(dict["first_name"] as! String) \(dict["last_name"] as! String)") {
                        filterDictArray.append(dict)
                    }
                }
            }
        }
        self.peopleTableView.reloadData()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
