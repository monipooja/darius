//
//  FMWalletVC.swift
//  Darious
//
//  Created by Dario Carrasco on 11/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FMWalletVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var centerLbl: UILabel!
    @IBOutlet weak var setMyBalLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var addBtn: KGHighLightedButton!
    @IBOutlet weak var withdrawBtn: KGHighLightedButton!
    @IBOutlet weak var balanceTableView: UITableView!
    
    // Withdraw Money View Outlets
    @IBOutlet weak var  blurView: UIView!
    @IBOutlet weak var withdrawView: CustomUIView!
    @IBOutlet weak var transferLbl: UILabel!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var ibanTF: UITextField!
    @IBOutlet weak var bicCodeTF: UITextField!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var continueBtn: KGHighLightedButton!
    @IBOutlet weak var withdrawVwTop: NSLayoutConstraint!
    
    var walletArray : [[NSDictionary]] = []
    var pageNo : Int = 1
    var myBalance : NSNumber = 0
    var fromScreen : String = ""
    var originalYvalue : CGFloat = 0
    var setBtm : Bool = false
    
    var textFArray : [UITextField] = []
    var matchDateArr : [String] = []
    var insertDateArr : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMThemeColor)
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        balanceTableView.rowHeight = UITableView.automaticDimension
        balanceTableView.estimatedRowHeight = 70
        
        setLanguage()
        setWithdrawViewData()
        if (setBtm == false)
        {
            setBottomView()
        }
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&page_no=\(self.pageNo)"
            self.callService(urlstr: Api.Wallet_History_URL, parameters: params, check: "get")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (myBalance != 0)
        {
            walletArray = []
            textFArray = []
            insertDateArr = []
            matchDateArr = []
            viewDidLoad()
        }
    }
    
    // MARK : Set Language
    func setLanguage()
    {
        centerLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_wallet_found")
        setMyBalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "my_balance")
        transferLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "transfer_money")
        
        firstNameTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "first_name")
        lastNameTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "last_name")
        ibanTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "iban")
        bicCodeTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "bic_code")
        amountTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "amount")
        
        addBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "add_money")), for: .normal)
        withdrawBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "withdraw_money_button")), for: .normal)
        continueBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // Set Withdraw View
    func setWithdrawViewData()
    {
        firstNameTF.text = userData["first_name"] as? String
        lastNameTF.text = userData["last_name"] as? String
        ibanTF.text = userData["bank_iban"] as? String
        bicCodeTF.text = userData["bank_bic"] as? String
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        blurView.addGestureRecognizer(gesture)
        
        originalYvalue = withdrawVwTop.constant
        
        textFArray = [firstNameTF, lastNameTF, ibanTF, bicCodeTF, amountTF]
        for tf in textFArray
        {
            addBottomLayerWithColor(bgcolor: UIColor.lightGray, textF: tf)
        }
        addBottomLayerWithColor(bgcolor: UIColor.init(hexString: themeColor), textF: firstNameTF)
    }
    
    func setBottomView()
    {
        setBtm = true
        
        if (self.view.frame.width == 375) && (self.view.frame.height == 667) {
            addBtn.titleLabel?.font = UIFont(name: "ProximaNova-Semibold", size: 12)
            withdrawBtn.titleLabel?.font = UIFont(name: "ProximaNova-Semibold", size: 12)
        }
        else {
            addBtn.titleLabel?.font = UIFont(name: "ProximaNova-Semibold", size: 15)
            withdrawBtn.titleLabel?.font = UIFont(name: "ProximaNova-Semibold", size: 15)
        }
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fm")
    }
    
    // Mark : Gesture Action
    @objc func checkAction(sender : UITapGestureRecognizer) {
        // Do what you want
        self.view.endEditing(true)
        blurView.isHidden = true
        withdrawView.isHidden = true
        withdrawVwTop.constant = originalYvalue
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func addMoney(_ sender: Any)
    {
        DispatchQueue.main.async {
            if let link = URL(string: "\(Api.Base_URL)addMoney?uid=\(user_id)&lang=\(tag_Lang)&curr=\(Currency_Code)") {
                //print("linkkkk = \(link)")
                UIApplication.shared.open(link)
            }
        }
    }
    
    @IBAction func withdrawMoney(_ sender: Any)
    {
        if (Int(truncating: myBalance) < 1)
        {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "invalid_wallet_amount"), controller: self)
        }
        else {
            blurView.isHidden = false
            withdrawView.isHidden = false
        }
    }
    
    @IBAction func hideWithdrawView(_ sender: Any)
    {
        self.view.endEditing(true)
        blurView.isHidden = true
        withdrawView.isHidden = true
    }
    
    @IBAction func withdrawContinue(_ sender: Any)
    {
        self.view.endEditing(true)
        blurView.isHidden = true
        withdrawView.isHidden = true
        
        let enterAmount = amountTF.text
        
        if (amountTF.text == "") {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "please_fill_the_amount"), controller: self)
        }
        else if (Int(enterAmount!)! > Int(truncating: myBalance)) {
            ApiResponse.alert(title: "", message: ApiResponse.getLanguageFromUserDefaults(inputString: "invalid_wallet_amount"), controller: self)
        }
        else {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&amount=\(amountTF.text!)"
            self.callService(urlstr: Api.Withdraw_Money_URL, parameters: params, check: "withdraw")
        }
    }
    
    // Mark : Add Bottom Layer
    func addBottomLayerWithColor(bgcolor : UIColor, textF : UITextField)
    {
        let border = CALayer()
        border.backgroundColor = bgcolor.cgColor
        if(bgcolor == UIColor.lightGray) {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:0.5)
        }
        else {
            border.frame = CGRect(x:0, y:textF.frame.height+1, width:textF.frame.width, height:1)
        }
        textF.layer.addSublayer(border)
    }
    
    // Mark : Arrange data according to date
    func arrangeDataWithDate(dataArr : [NSDictionary])
    {
        for dict in dataArr
        {
            var arr : [NSDictionary] = []
            
            let startStr = dict["created_on"] as? String
            if let _ = startStr!.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outputFormat: "dd MMM yyyy")
            {
                let matchDate = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "yyyy-MM-dd")
                let insertDate = (dict["created_on"] as! String).toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM yyyy")
                if (self.matchDateArr.count == 0) {
                    self.matchDateArr.append(matchDate!)
                    self.insertDateArr.append(insertDate!)
                    arr.append(dict)
                    self.walletArray.append(arr)
                } else {
                    if (self.matchDateArr.contains(matchDate!)) {
                        let idx = self.matchDateArr.index(of: matchDate!)
                        self.walletArray[idx!].append(dict)
                    } else {
                        self.matchDateArr.append(matchDate!)
                        self.insertDateArr.append(insertDate!)
                        arr.append(dict)
                        self.walletArray.append(arr)
                    }
                }
            }
        }
        balanceTableView.isHidden = false
        balanceTableView.reloadData()
    }
    
    // Mark : Call service
    func callService(urlstr : String, parameters : String, check : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FMThemeColor, controller: self.view)
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            ApiResponse.onResponsePostPhp(url: urlstr, parms: parameters) { (result, error) in
                
                if (error == "") {
                    //   print("wallet result = \(result)")
                    let status = result["status"] as! NSNumber
                    
                    OperationQueue.main.addOperation {
                        if(status == 1)
                        {
                            if(check == "get") {
                                //                                self.balanceLabel.text = "\(result["userWalletAmount"] as! NSNumber) \u{20ac}".replacingOccurrences(of: ".", with: ",")
                                self.myBalance = result["userWalletAmount"] as! NSNumber
                                wallet_amount = result["userWalletAmount"] as! NSNumber
                                
                                let numberFormatter = NumberFormatter()
                                numberFormatter.numberStyle = .decimal
                                numberFormatter.maximumFractionDigits = 2
                                let formattedNumber = numberFormatter.string(from: wallet_amount)
                                self.balanceLabel.text = "\(formattedNumber!) \(Current_Currency)"
                                
                                userDefault.set(wallet_amount, forKey: "walletAmt")
                                userDefault.synchronize()
                                
                                if let _  = result["walletData"] as? [NSDictionary] {
                                    let dataArray = result["walletData"] as! [NSDictionary]
                                    self.arrangeDataWithDate(dataArr: dataArray)
                                }
                                if(self.walletArray.count > 0) {
                                    self.centerLbl.isHidden = true
                                } else {
                                    self.centerLbl.isHidden = false
                                }
                                customLoader.hideIndicator()
                            }
                            else if (check == "withdraw") {
                                customLoader.hideIndicator()
                                self.viewDidLoad()
                            }
                            else {
                                if let _  = result["walletData"] as? [NSDictionary] {
                                    let dataArray = result["walletData"] as! [NSDictionary]
                                    self.arrangeDataWithDate(dataArr: dataArray)
                                }
                            }
                            customLoader.hideIndicator()
                        }
                        else if(status == 3){
                            customLoader.hideIndicator()
                            Access_token = ""
                            userDefault.set(Access_token, forKey: "token")
                            userDefault.synchronize()
                            let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(VC, animated: true, completion: nil)
                        }
                        else {
                            self.centerLbl.isHidden = false
                            if let str = result["message"] {
                                let msgStr = str as! String
                                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                                if(showMsg != "") {
                                    ApiResponse.alert(title: "", message: showMsg, controller: self)
                                }
                                else {
                                    ApiResponse.alert(title: "", message: msgStr, controller: self)
                                }
                            }
                            else {
                                customLoader.hideIndicator()
                            }
                        }
                    }
                }
                else {
                    ApiResponse.alert(title: "Reuqest tiempo fuera", message: "Inténtalo de nuevo", controller: self)
                }
            }
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Group
            let searchvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSearchVC") as! FMSearchVC
            searchvc.fromScreen = "group"
            present(searchvc, animated: false, completion: nil)
            break
        case 2:
            // Transaction
            let transvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMTransactionVC") as! FMTransactionVC
            present(transvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMWalletVC") as! FMWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMNotificationVC") as! FMNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMWalletVC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        withdrawVwTop.constant = originalYvalue - 120
        
        for tf in textFArray {
            if(tf == textField) {
                for sublyr in tf.layer.sublayers!
                {
                    if(sublyr.backgroundColor == UIColor.lightGray.cgColor){
                        sublyr.backgroundColor = UIColor.init(hexString: themeColor).cgColor
                        sublyr.frame.size.height = 1.5
                    }
                }
            }
            else {
                for sublyr in tf.layer.sublayers!
                {
                    if(sublyr.backgroundColor == UIColor.lightGray.cgColor) || (sublyr.backgroundColor == UIColor.init(hexString: themeColor).cgColor) {
                        sublyr.backgroundColor = UIColor.lightGray.cgColor
                        sublyr.frame.size.height = 0.5
                    }
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        withdrawVwTop.constant = originalYvalue
        return true
    }
}

extension FMWalletVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return insertDateArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (walletArray.count > 0) {
            return walletArray[section].count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = balanceTableView.dequeueReusableCell(withIdentifier: "fmwalletcell") as! FMWalletTableCell
        
        let dict = (walletArray[indexPath.section])[indexPath.row]
        let descStr = dict["description"] as? String
        if (descStr?.contains("_") == true)
        {
            cell.descLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: descStr!)
        } else
        {
            cell.descLabel.text = dict["description"] as? String
        }
        
        let titleStr = dict["title"] as! String
        if (titleStr != "")
        {
            if (titleStr == "[testingtwo test] send_money_you")
            {
                let arr = titleStr.split(separator: "]")
                if (arr[1].replacingOccurrences(of: " ", with: "") == "send_money_you")
                {
                    cell.titleLabel.text = "\(arr[0]) \(ApiResponse.getLanguageFromUserDefaults(inputString: "send_money_you"))".replacingOccurrences(of: "[", with: "")
                }
            }
            else {
                let arr = titleStr.split(separator: "[")
                if (arr[0].replacingOccurrences(of: " ", with: "") == "you_send_money")
                {
                    cell.titleLabel.text = "\(ApiResponse.getLanguageFromUserDefaults(inputString: "you_send_money")) \(arr[1])".replacingOccurrences(of: "]", with: "")
                }
            }
        }
        else {
            cell.titleHt.constant = 0
            cell.titleLabel.text = ""
        }
        
        if let _ = dict["amount"] as? NSNumber
        {
            if let _ = dict["currency"] as? String
            {
                cell.totalPrcLabel.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", Double("\(dict["amount"] as! NSNumber)")!)) + " \(dict["currency"] as! String)"
            }
            else {
                cell.totalPrcLabel.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", Double("\(dict["amount"] as! NSNumber)")!)) + " \(Current_Currency)"
            }
        }
        
        let type = dict["type"] as! String
        if (type == "credit") {
            cell.arrowImageVw.image = #imageLiteral(resourceName: "GreenArrow")
        } else {
            cell.arrowImageVw.image = #imageLiteral(resourceName: "OrangeArrow")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
        vw.backgroundColor = UIColor.white
        
        let dateLabel = UILabel.init(frame: CGRect(x: 20, y: 5, width: vw.frame.width-20, height: 25))
        
        dateLabel.text = insertDateArr[section]
        dateLabel.backgroundColor = UIColor.white
        dateLabel.textColor = UIColor.init(hexString: "999999")
        dateLabel.font = UIFont.init(name: "ProximaNova-Semibold", size: 13)
        vw.addSubview(dateLabel)
        
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.row == walletArray[indexPath.section].count-1 && (walletArray[indexPath.section].count > 9){
            // Last cell is visible
            pageNo = pageNo + 1
         //   customLoader.showActivityIndicator(showColor: ColorCode.FCBlackColor, controller: self.view)
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&page_no=\(self.pageNo)"
            self.callService(urlstr: Api.Wallet_History_URL, parameters: params, check: "load")
        }
    }
}


class FMWalletTableCell : UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var totalPrcLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var arrowImageVw: UIImageView!
    
    @IBOutlet weak var titleHt: NSLayoutConstraint!
}
