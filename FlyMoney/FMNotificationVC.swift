//
//  FMNotificationVC.swift
//  Darious
//
//  Created by Dario Carrasco on 11/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FMNotificationVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var notificationTableVw: UITableView!
    @IBOutlet weak var centerLbl: UILabel!
    
    var dictArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMThemeColor)
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
        
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        notificationTableVw.rowHeight = UITableView.automaticDimension
        notificationTableVw.estimatedRowHeight = 55
        centerLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "no_notification_found")
        
        setBottomView()
        
        DispatchQueue.main.async {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)"
            self.callService(urlstr: Api.Notification_URL, parameters: params)
        }
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fm")
    }
    
    @IBAction func onFMHome(_ sender: Any)
    {
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    func callService(urlstr : String, parameters : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FMThemeColor, controller: self.view)
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            ApiResponse.onResponsePostPhp(url: urlstr, parms: parameters) { (result, error) in
                
                if (error == "") {
                    //  print("notification result = \(result)")
                    let status = result["status"] as! NSNumber
                    
                    OperationQueue.main.addOperation {
                        if(status == 1)
                        {
                            if let _ = result["notificationData"] as? [NSDictionary]
                            {
                                self.dictArray = result["notificationData"] as! [NSDictionary]
                                if(self.dictArray.count > 0) {
                                    self.notificationTableVw.reloadData()
                                } else {
                                    self.centerLbl.isHidden = false
                                }
                            }
                            else {
                                self.centerLbl.isHidden = false
                            }
                            notification_count = 0
                            self.notifyImageView.image = #imageLiteral(resourceName: "nonotification")
                            customLoader.hideIndicator()
                        }
                        else if(status == 3){
                            customLoader.hideIndicator()
                            Access_token = ""
                            userDefault.set(Access_token, forKey: "token")
                            userDefault.synchronize()
                            let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(VC, animated: true, completion: nil)
                        }
                        else {
                            self.centerLbl.isHidden = false
                            if let str = result["message"] {
                                let msgStr = str as! String
                                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                                if(showMsg != "") {
                                    ApiResponse.alert(title: "", message: showMsg, controller: self)
                                }
                                else {
                                    ApiResponse.alert(title: "", message: msgStr, controller: self)
                                }
                            }
                            else {
                                customLoader.hideIndicator()
                            }
                        }
                    }
                }
                else {
                    ApiResponse.alert(title: "Reuqest tiempo fuera", message: "Inténtalo de nuevo", controller: self)
                }
            }
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Group
            let searchvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSearchVC") as! FMSearchVC
            searchvc.fromScreen = "group"
            present(searchvc, animated: false, completion: nil)
            break
        case 2:
            // My Petition
            let transvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMTransactionVC") as! FMTransactionVC
            present(transvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMWalletVC") as! FMWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMNotificationVC") as! FMNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMNotificationVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = notificationTableVw.dequeueReusableCell(withIdentifier: "fmnotifycell") as! FMNotificationTableCell
        
        let dict = dictArray[indexPath.row]
        
        let descStr = dict["messages"] as? String
        if (descStr?.contains("_") == true) && (ApiResponse.getLanguageFromUserDefaults(inputString: descStr!) != "")
        {
            cell.msgLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: descStr!)
        } else
        {
            cell.msgLbl.text = dict["messages"] as? String
        }
        
        let datestr = dict["created_on"] as! String
        if let str = datestr.toDateString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z", outputFormat: "dd MMM yyyy | HH:mm a")
        {
            cell.dateLbl.text = str
        }
        
        return cell
    }
    
}

class FMNotificationTableCell: UITableViewCell {
    // notifycell
    
    @IBOutlet weak var msgLbl : UILabel!
    @IBOutlet weak var descLbl : UILabel!
    @IBOutlet weak var dateLbl : UILabel!
    @IBOutlet weak var imgvw : UIImageView!
}
