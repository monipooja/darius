//
//  FMPaymentDetailsVC.swift
//  Darious
//
//  Created by Dario Carrasco on 10/06/19.
//  Copyright © 2019 Darius. All rights reserved.
//

import UIKit

class FMPaymentDetailsVC: UIViewController {

    @IBOutlet var bottombarBtnCollection: [UIButton]!
    @IBOutlet weak var contentVw : UIView!
    
    @IBOutlet weak var btmVwHt : NSLayoutConstraint!
    @IBOutlet weak var dariuBottom : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnWd : NSLayoutConstraint!
    @IBOutlet weak var dariuTopBtnHt : NSLayoutConstraint!
    @IBOutlet var wdCollection : [NSLayoutConstraint]!
    @IBOutlet var htCollection : [NSLayoutConstraint]!
    @IBOutlet var vwTopSpaceCollection : [NSLayoutConstraint]!
    @IBOutlet weak var notifyImageView : UIImageView!
    @IBOutlet weak var btmImgVw : UIImageView!
    
    @IBOutlet weak var userCollectionView : UICollectionView!
    
    @IBOutlet weak var amountTF : KGHighLightedField!
    @IBOutlet weak var addMessageTF : KGHighLightedField!
    @IBOutlet weak var sendButton : KGHighLightedButton!
    @IBOutlet weak var requestButton : KGHighLightedButton!
    @IBOutlet weak var sendLongButton : KGHighLightedButton!
    @IBOutlet weak var requestLongButton : KGHighLightedButton!
    @IBOutlet weak var sendImage : UIImageView!
    @IBOutlet weak var rquestImage : UIImageView!
    @IBOutlet weak var sendLongImage : UIImageView!
    @IBOutlet weak var rquestLongImage : UIImageView!
    
    @IBOutlet weak var userProfile : UIImageView!
    @IBOutlet weak var userName : UILabel!
    @IBOutlet weak var userNickname : UILabel!
    @IBOutlet weak var paymentLabel : UILabel!
    @IBOutlet weak var debitLabel : UILabel!
    @IBOutlet weak var walletButton : UIButton!
    @IBOutlet weak var cardButton : UIButton!
    @IBOutlet weak var walletImageVw : UIImageView!
    @IBOutlet weak var walletIcon : UIImageView!
    @IBOutlet weak var cardImageVw : UIImageView!
    
    // Center View outlets
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var centerSuccessVw: CustomUIView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var msgLabel : UILabel!
    @IBOutlet weak var handimage : UIImageView!
    
    // Credit payment view outlets
    @IBOutlet weak var paymentView: CustomUIView!
    @IBOutlet weak var paymentTitleLbl: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var commissionLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var subtotalValue: UILabel!
    @IBOutlet weak var commissionValue: UILabel!
    @IBOutlet weak var totalValue: UILabel!
    @IBOutlet weak var continueBtn: KGHighLightedButton!
    
    @IBOutlet weak var debitViewHt: NSLayoutConstraint!
    @IBOutlet weak var walletViewHt: NSLayoutConstraint!
    @IBOutlet weak var debitView: UIView!
    
    var finalScreen : String = ""
    var passDict : NSDictionary = [:]
    var dataDict : NSDictionary = [:]
    var grpArr : [NSDictionary] = []
    var paymentType : String = ""
    var total : Float = 0
    var transactionId : String = ""
    var total_comsn : Float = 0
    var user_ID : NSNumber = 0
    
    // create group var
    var memberArray : [NSDictionary] = []
    var grpmemberIds : String = ""
    var groupName : String = ""
    var grpProfileImage : UIImage!
    var profileName : String = ""
    var fromOption : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        btmImgVw.tintColor = UIColor.init(hexString: ColorCode.FMThemeColor)
        
        if (Int(truncating: notification_count) > 0)
        {
            notifyImageView.image = #imageLiteral(resourceName: "FM_Notification")
        }
     
        for btn in bottombarBtnCollection
        {
            btn.addTarget(self, action: #selector(bottomBarbuttonAction(_:)), for: .touchUpInside)
        }
        
        if (wallet_amount == 0)
        {
            debitViewHt.constant = debitViewHt.constant - walletViewHt.constant
            walletViewHt.constant = 0
            walletButton.isHidden = true
            walletImageVw.isHidden = true
            walletIcon.isHidden = true
            setImage(selectImg: cardImageVw, unselectImg: walletImageVw)
            paymentType = "card"
        }
        else {
            setImage(selectImg: walletImageVw, unselectImg: cardImageVw)
            paymentType = "wallet"
        }
        setLanguage()
        setBottomView()
        setDataOnLoad()
    }
    
    // MARK : Status bar content's color set to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    // Mark : Gesture Action
    @objc func checkAction(sender : UITapGestureRecognizer) {
        contentVw.endEditing(true)
    }
    
    // Mark : Gesture 2 Action
    @objc func hideview(sender : UITapGestureRecognizer) {
        blurView.isHidden = true
        centerSuccessVw.isHidden = true
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (transactionId != ""){
            ApiResponse.onResponseGetMollie(url: "https://api.mollie.com/v2/payments/\(transactionId)") { (result, error) in
                OperationQueue.main.addOperation {
                    customLoader.hideIndicator()
                    if (error == "")
                    {
                        //print("mollie trans result = \(result)")
                        let status = "\(result["status"] ?? "")"
                        self.transactionId = ""
                        if status == "canceled" || status == "failed" || status == "open" {
                            self.blurView.isHidden = false
                            self.centerSuccessVw.isHidden = false
                            self.handimage.image = #imageLiteral(resourceName: "CancelCross")
                            self.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "error")
                            self.msgLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "payment_not_done")
                        } else {
                            if (status == "paid") {
                                self.sendMoney()
                            }
                        }
                    }
                    else{}
                }
            }
        }
    }
    
    func setDataOnLoad()
    {
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
        gesture.numberOfTapsRequired = 1
        contentVw.addGestureRecognizer(gesture)
        
        let gesture2 = UITapGestureRecognizer(target: self, action:  #selector(hideview(sender:)))
        gesture2.numberOfTapsRequired = 1
        blurView.addGestureRecognizer(gesture2)
        
        if (finalScreen == "group") || (finalScreen == "creategrp")
        {
            userCollectionView.isHidden = false
            userCollectionView.reloadData()
            paymentLabel.text = groupName
            
            sendButton.isHidden = true
            sendImage.isHidden = true
            requestButton.isHidden = true
            rquestImage.isHidden = true
            
            if (fromOption == "tosend")
            {
                sendLongButton.isHidden = false
                sendLongImage.isHidden = false
            }
            else {
                requestLongButton.isHidden = false
                rquestLongImage.isHidden = false
                debitViewHt.constant = 0
                debitView.isHidden = true
            }
        }
        else {
            paymentLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "payment_details")
            
            userProfile.sd_setImage(with: URL(string: "\(Image_URL)\(passDict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
            userName.text = "\(passDict["first_name"] as! String) \(passDict["last_name"] as! String)"
            self.userNickname.text = passDict["nickname"] as? String
            
            userProfile.layer.cornerRadius = userProfile.frame.width/2
            userProfile.layer.masksToBounds = true
           
            if (dataDict != [:])
            {
                amountTF.text = "\(dataDict["amount"] as! NSNumber)"
                addMessageTF.text = dataDict["description"] as? String
            }
            if (finalScreen == "single")
            {
                user_ID = (passDict["id"] as? NSNumber)!
            }
        }
        
        if (finalScreen == "singleuser")
        {
            sendButton.isHidden = true
            sendImage.isHidden = true
            requestButton.isHidden = true
            rquestImage.isHidden = true
            
            if (fromOption == "tosend")
            {
                sendLongButton.isHidden = false
                sendLongImage.isHidden = false
            }
            else {
                requestLongButton.isHidden = false
                rquestLongImage.isHidden = false
                debitViewHt.constant = 0
                debitView.isHidden = true
            }
        }
    }
    
    func setLanguage()
    {
        amountTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "enter_amount") + "*"
        addMessageTF.placeholder = ApiResponse.getLanguageFromUserDefaults(inputString: "add_a_message") + "*"
        debitLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "debit_from")
    
        titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "payment_done")
        paymentTitleLbl.text = ApiResponse.getLanguageFromUserDefaults(inputString: "amount_details")
        subtotalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "subtotal")
        totalLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "total")
        commissionLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "commission")
        
        continueBtn.setTitle((ApiResponse.getLanguageFromUserDefaults(inputString: "continue")), for: .normal)
        walletButton.setTitle("     \(ApiResponse.getLanguageFromUserDefaults(inputString: "Wallet")) (\(wallet_amount) \(Current_Currency))", for: .normal)
        cardButton.setTitle("     \(ApiResponse.getLanguageFromUserDefaults(inputString: "debit_credit_card"))", for: .normal)
        sendButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "send_enter_amount"), for: .normal)
        requestButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fly_money_meet_request_heading"), for: .normal)
        sendLongButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "send_enter_amount"), for: .normal)
        requestLongButton.setTitle(ApiResponse.getLanguageFromUserDefaults(inputString: "fly_money_meet_request_heading"), for: .normal)
    }
    
    func setBottomView()
    {
        StaticFunctions.setBottomView(view: self.view, layoutConstraint: [btmVwHt,dariuBottom,dariuTopBtnHt,dariuTopBtnWd], passWdCollection: wdCollection, passHtCollection: htCollection, passTopSpaceCollection: vwTopSpaceCollection, passBottomBtnCollection: bottombarBtnCollection, appstr: "fm")
    }
    
    @IBAction func onSelectWallet(_ sender: Any)
    {
        paymentType = "wallet"
        setImage(selectImg: walletImageVw, unselectImg: cardImageVw)
    }
        
    @IBAction func onSelectCard(_ sender: Any)
    {
        paymentType = "card"
        setImage(selectImg: cardImageVw, unselectImg: walletImageVw)
    }
    
    func setImage(selectImg : UIImageView, unselectImg : UIImageView)
    {
        selectImg.image = #imageLiteral(resourceName: "Right_checked")
        selectImg.layer.borderWidth = 0
        selectImg.layer.cornerRadius = selectImg.frame.width/2
        selectImg.layer.masksToBounds = true
        
        unselectImg.layer.cornerRadius = unselectImg.frame.width/2
        unselectImg.layer.masksToBounds = true
        unselectImg.image = nil
        unselectImg.layer.borderColor = UIColor.lightGray.cgColor
        unselectImg.layer.borderWidth = 1
    }
    
    @IBAction func onSendMoney(_ sender: Any)
    {
        if (amountTF.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "enter_amount"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (addMessageTF.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "enter_message"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            if (paymentType == "wallet") {
                sendMoney()
            }
            else {
                blurView.isHidden = false
                paymentView.isHidden = false
                setBalanceWithComission()
            }
        }
    }
    
    // MARK : Send Money
    func sendMoney()
    {
        if (finalScreen == "single") || (finalScreen == "singleuser") {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(user_ID)&add_money=\(amountTF.text!)&message=\(addMessageTF.text!)"
           
            self.callService(urlstr: Api.FM_SendMoney_URL, parameters: params, check: "send")
        }
        else if (finalScreen == "group")
        {
            let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&group_id=\(passDict["id"] as! NSNumber)&add_money=\(amountTF.text!)&message=\(addMessageTF.text!)&group_name=\(passDict["group_name"] as! String)&group_members=\(passDict["group_members"] as! String)&group_image="
            self.callService(urlstr: Api.FM_SendToGroup_URL, parameters: params, check: "send")
        }
        else {
            if (profileName != "")
            {
                let params : [String : String] = ["access_token":"\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "add_money":"\(amountTF.text!)", "message":"\(addMessageTF.text!)", "group_name":"\(groupName)", "group_members":"\(grpmemberIds)"]
                serviceCallWithMultipart(parameters: params, apiString: Api.FM_SendToGroup_URL)
            }
            else {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&add_money=\(amountTF.text!)&message=\(addMessageTF.text!)&group_name=\(groupName)&group_members=\(grpmemberIds)&group_image="
                self.callService(urlstr: Api.FM_SendToGroup_URL, parameters: params, check: "send")
            }
        }
    }
    
    // MARK : Set commission
    func setBalanceWithComission()
    {
        if (amountTF.text!.contains(".") == false)
        {
            subtotalValue.text = StaticFunctions.priceSetWithoutDecimal(prcValue: amountTF.text!) + " \(Current_Currency)"
        }
        else {
            let sp = amountTF.text?.split(separator: ".")
            if (sp?.count == 1)
            {
                subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: amountTF.text!) + "0 \(Current_Currency)"
            }
            else {
                subtotalValue.text = StaticFunctions.priceFormatSet(prcValue: amountTF.text!) + " \(Current_Currency)"
            }
        }
        
        if ((amountTF.text?.contains(","))!) {
            let amtstr = StaticFunctions.priceFormatSet(prcValue: amountTF.text!)
            total_comsn = ((Float(amtstr)!) * (Float(commission_value)!/100)) + Float(commission_Card)!
            total = Float(amtstr)! + total_comsn
            total = Float(String(format: "%.2f", total))!
        }
        else {
            let amountStr = "\(self.amountTF.text!)"
            total_comsn = ((Float(amountStr)!) * (Float(commission_value)!/100)) + Float(commission_Card)!
            total = Float(amountStr)! + total_comsn
            total = Float(String(format: "%.2f", total))!
        }
        commissionValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", total_comsn)) + " \(Current_Currency)"
        totalValue.text = StaticFunctions.priceFormatSet(prcValue: String(format: "%.2f", total)) + " \(Current_Currency)"
    }

    @IBAction func onRequestMoney(_ sender: Any)
    {
        if (amountTF.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "enter_amount"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else if (addMessageTF.text == "")
        {
            ApiResponse.bottomCustomAlert(message: ApiResponse.getLanguageFromUserDefaults(inputString: "enter_message"), controller: self, fontFamliy: UIFont(name: "ProximaNova-Regular", size: 14)!,  bgcolor: UIColor.init(hexString: "919191"), txtcolor: UIColor.white, wd: view.frame.width, ht: 60)
        }
        else {
            if (finalScreen == "single") || (finalScreen == "singleuser"){
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&user_id=\(user_ID)&add_money=\(amountTF.text!)&message=\(addMessageTF.text!)"
                self.callService(urlstr: Api.FM_RequestMoney_URL, parameters: params, check: "request")
            }
            else if(finalScreen == "group") {
                let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&group_id=\(passDict["id"] as! NSNumber)&add_money=\(amountTF.text!)&message=\(addMessageTF.text!)&group_name=\(passDict["group_name"] as! String)&group_members=\(passDict["group_members"] as! String)&group_image="
                self.callService(urlstr: Api.FM_RequestToGroup_URL, parameters: params, check: "send")
            }
            else {
                if (profileName != "")
                {
                    let params : [String : String] = ["access_token":"\(Access_token)", "device_id":"\(device_id)", "api_key":"\(api_key)", "device_type":"\(device_type)", "uid":"\(user_id)", "role":"\(role)", "add_money":"\(amountTF.text!)", "message":"\(addMessageTF.text!)", "group_name":"\(groupName)", "group_members":"\(grpmemberIds)"]
                    serviceCallWithMultipart(parameters: params, apiString: Api.FM_RequestToGroup_URL)
                }
                else {
                    let params = "access_token=\(Access_token)&device_id=\(device_id)&api_key=\(api_key)&device_type=\(device_type)&uid=\(user_id)&role=\(role)&add_money=\(amountTF.text!)&message=\(addMessageTF.text!)&group_name=\(groupName)&group_members=\(grpmemberIds)&group_image="
                    self.callService(urlstr: Api.FM_RequestToGroup_URL, parameters: params, check: "send")
                }
            }
        }
    }
    
    @IBAction func onFMHome(_ sender: Any)
    {
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onContinue(_ sender: Any)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FMThemeColor, controller: self.view)
        let aa = "\(total)".split(separator: ".")
        
        if (aa[1].count == 1) {
            self.molliePayment(payment: "\(total)0", cid: "")
        }
        else {
            self.molliePayment(payment: "\(total)", cid: "")
        }
    }
    
    @IBAction func onHidePaymentView(_ sender: Any)
    {
        blurView.isHidden = true
        paymentView.isHidden = true
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    @IBAction func onHideCenterView(_ sender: Any)
    {
        blurView.isHidden = true
        centerSuccessVw.isHidden = true
        let homevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMHomeVC") as! FMHomeVC
        present(homevc, animated: false, completion: nil)
    }
    
    // MARK : Service call
    func callService(urlstr : String, parameters : String, check : String)
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            customLoader.showActivityIndicator(showColor: ColorCode.FMThemeColor, controller: self.view)
            ApiResponse.onResponsePostPhp(url: urlstr, parms: parameters) { (result, error) in
                
                if (error == "") {
                  //  print("\(check) money result = \(result)")
                    let status = result["status"] as! NSNumber
                    
                    OperationQueue.main.addOperation {
                        if(status == 1)
                        {
                            if (check == "members")
                            {
                                let dict = result["groupMemberDetails"] as! NSDictionary
                                if let _ = dict["groupMemberList"] as? [NSDictionary]
                                {
                                    self.memberArray = dict["groupMemberList"] as! [NSDictionary]
                                    self.userCollectionView.reloadData()
                                }
                                customLoader.hideIndicator()
                            }
                            else {
                                self.blurView.isHidden = false
                                self.centerSuccessVw.isHidden = false
                                self.handimage.image = #imageLiteral(resourceName: "SuccessChecked")
                                self.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "payment_done")
                                self.msgLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "sucessful")
                                customLoader.hideIndicator()
                            }
                        }
                        else if(status == 3){
                            customLoader.hideIndicator()
                            Access_token = ""
                            userDefault.set(Access_token, forKey: "token")
                            userDefault.synchronize()
                            let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(VC, animated: true, completion: nil)
                        }
                        else {
                            if let str = result["message"] {
                                let msgStr = str as! String
                                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                                if(showMsg != "") {
                                    ApiResponse.alert(title: "", message: showMsg, controller: self)
                                }
                                else {
                                    ApiResponse.alert(title: "", message: msgStr, controller: self)
                                }
                            }
                            else {
                                customLoader.hideIndicator()
                            }
                        }
                    }
                }
                else {
                    ApiResponse.alert(title: "Reuqest tiempo fuera", message: "Inténtalo de nuevo", controller: self)
                }
            }
        }
        else {
            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
            ApiResponse.alert(title: "", message: showMsg, controller: self)
        }
    }
    
    // Mollie Payment Service Call
    func molliePayment(payment : String, cid : String)
    {
        let amountDict : NSDictionary = ["currency" : "EUR", "value" : payment]
        let date = Date()
        let formatter = DateFormatter.init()
        formatter.dateFormat = "yyyyMMdd"
        let datestr = formatter.string(from: date)
        let metaDict : NSDictionary = ["uinique_id" : "\(datestr)\(user_id)", "commission" : "\(total_comsn)"]
        
        let params : [String : Any] = ["metadata" : metaDict, "amount" : amountDict, "description" : "\(ApiResponse.getLanguageFromUserDefaults(inputString: "subscription_business"))", "customerId" : "\(cid)", "redirectUrl" : "http://www.myapp.com/", "webhookUrl" : "\(Api.Base_URL)flycliam/add_money_wallet_webhook/\(user_id)"]
        
        ApiResponse.onResponsePostMollie2(url: "https://api.mollie.com/v2/payments", parms: params) { (result, error) in
            // print("mollie payment result = \(result)")
            
            OperationQueue.main.addOperation {
                if let _ = result["_links"] as? NSDictionary {
                    
                    let checkoutUrl = ((result["_links"] as! NSDictionary)["checkout"] as! NSDictionary)["href"] as! String
                    self.transactionId = result["id"] as! String
                    
                    customLoader.hideIndicator()
                    self.blurView.isHidden = true
                    self.paymentView.isHidden = true
                    let webvc = StoryboardType.fc_storyboard.instantiateViewController(withIdentifier: "MollieWebviewVC") as! MollieWebviewVC
                    webvc.checkouturl = checkoutUrl
                    webvc.transid = result["id"] as! String
                    webvc.comeFrom = "fmdetail"
                    self.present(webvc, animated: false, completion: nil)
                }
                else {
                    customLoader.hideIndicator()
                }
            }
        }
    }
    
    /// MARK : Multipart (file format)
    func serviceCallWithMultipart(parameters : [String:String], apiString : String)
    {
        customLoader.showActivityIndicator(showColor: ColorCode.FMThemeColor, controller: self.view)
        let url = NSURL(string: apiString)
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var body = Data()
        
        let mimetype = "image/png"
        
        for (key, value) in parameters
        {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        let image_data = grpProfileImage.jpegData(compressionQuality: 0.2)
        if(image_data == nil)
        {
            return
        }
        //define the data post parameter
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"group_image\"; filename=\"\(profileName)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(image_data!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.httpBody = body as Data
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            DispatchQueue.main.async
                {
                    guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                        ApiResponse.alert(title: "Something Went Wrong", message: "Please try again", controller: self)
                        return
                    }
                    _ = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    
                    let dd = try! JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    
                    let status = dd["status"] as! NSNumber
                    
                    if (status == 1) {
                        self.blurView.isHidden = false
                        self.centerSuccessVw.isHidden = false
                        self.handimage.image = #imageLiteral(resourceName: "SuccessChecked")
                        self.titleLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "Payment Done")
                        self.msgLabel.text = ApiResponse.getLanguageFromUserDefaults(inputString: "sucessful")
                        customLoader.hideIndicator()
                    }
                    else if(status == 3){
                        customLoader.hideIndicator()
                        Access_token = ""
                        userDefault.set(Access_token, forKey: "token")
                        userDefault.synchronize()
                        let VC = StoryboardType.main_storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.present(VC, animated: true, completion: nil)
                    }
                    else {
                        if let str = dd["message"] {
                            let msgStr = str as! String
                            let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: msgStr)
                            if(showMsg != "") {
                                ApiResponse.alert(title: "", message: showMsg, controller: self)
                            }
                            else {
                                ApiResponse.alert(title: "", message: msgStr, controller: self)
                            }
                        }
                        else {
                            customLoader.hideIndicator()
                        }
                    }
            }
        }
        task.resume()
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    // Bottom View Actions
    @objc func bottomBarbuttonAction(_ sender : UIButton)
    {
        switch sender.tag {
        case 1:
            // Home
            let searchvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSearchVC") as! FMSearchVC
            searchvc.fromScreen = "group"
            present(searchvc, animated: false, completion: nil)
            break
        case 2:
            // Transaction
            let transvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMTransactionVC") as! FMTransactionVC
            present(transvc, animated: false, completion: nil)
            break
        case 3:
            // Darious
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.addSideController()
            break
        case 4:
            // Wallet
            let walletvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMWalletVC") as! FMWalletVC
            present(walletvc, animated: false, completion: nil)
            break
        case 5:
            // Notification
            let notifyvc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMNotificationVC") as! FMNotificationVC
            present(notifyvc, animated: false, completion: nil)
            break
        default:
            break
        }
    }
}

extension FMPaymentDetailsVC : UICollectionViewDataSource, UICollectionViewDelegate
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return memberArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = userCollectionView.dequeueReusableCell(withReuseIdentifier: "detailcell", for: indexPath) as! DetailCollectionCell
        
        let dict = memberArray[indexPath.item]
        if let _ = dict["profile_pic"] as? String
        {
            cell.userImgvw.sd_setImage(with: URL(string: "\(Image_URL)\(dict["profile_pic"] as! String)"), placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        else {
            cell.userImgvw.sd_setImage(with: nil, placeholderImage: #imageLiteral(resourceName: "Fm_Placeholder"))
        }
        cell.userImgvw.layer.cornerRadius = cell.userImgvw.bounds.width/2
        cell.userImgvw.layer.masksToBounds = true
        
        if let _ = dict["first_name"] as? String
        {
            cell.nameLabel.text = "\(dict["first_name"] as! String) \(dict["last_name"] as! String)"
        }
        cell.touchButton.addTarget(self, action: #selector(goToList(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func goToList(_ sender : UIButton)
    {
        let point = sender.convert(CGPoint.zero, to: userCollectionView)
        let idxpath = userCollectionView.indexPathForItem(at: point)
        let dict = memberArray[(idxpath?.item)!]
        let singlevc = StoryboardType.fm_storyboard.instantiateViewController(withIdentifier: "FMSingleUserVC") as! FMSingleUserVC
        singlevc.passDict = memberArray[(idxpath?.item)!]
        singlevc.rcvrUserid = "\(dict["id"] as! NSNumber)"
        singlevc.singelFromScreen = "details"
        present(singlevc, animated: true, completion: nil)
    }
}


class DetailCollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var userImgvw : UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var touchButton : UIButton!
}
